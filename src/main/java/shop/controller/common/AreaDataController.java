package shop.controller.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;
import shop.core.common.oscache.SystemManager;

/**
 * @Description 获取区域数据
 * @Author semstouch
 * @Date 2017/8/22
 **/
@Controller
@ControllerAdvice
@RequestMapping("/manage/area")
public class AreaDataController extends AbstractJsonpResponseBodyAdvice {
    @Autowired
    private SystemManager systemManager;

    public AreaDataController() {
        super("callback");
    }

    @RequestMapping(value = "/getAllData", method = RequestMethod.GET,produces="text/html;charset=UTF-8")
    @ResponseBody
    public String getAllData(String callback) {
        String rs = callback + "(" + systemManager.getAreaData() + ")";
        return rs;
    }

}

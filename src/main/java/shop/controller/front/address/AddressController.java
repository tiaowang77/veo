/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.address;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.address.AddressService;
import shop.services.front.address.bean.Address;
import shop.services.manage.system.bean.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @类名称：AddressController
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:33:04
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/front/address/")
public class AddressController extends FrontBaseController<Address> {
    private final  static  String ADDRESS_DEFAULT_YES="y";
    private final  static  String ADDRESS_DEFAULT_NO="n";
    @Resource
    private AddressService addressService;
    @Override
    public Services<Address> getService() {
        return addressService;
    }


    /**
     * 跳转到地址列表页面
     *
     * @return
     */
    @RequestMapping("toAddressList")
    public String toAddressList() {return "/front/address/addressList";}

    /**
     * 跳转到地址编辑页面
     *
     * @return
     */
    @RequestMapping("toAddressEdit")
    public String toAddressEdit() {return "/front/address/addressEdit";}

    /**
     * 跳转到地址添加页面
     *
     * @return
     */
    @RequestMapping("toAddressAdd")
    public String toAddressAdd() {return "/front/address/addressAdd";}

    /**
     * 查询用户地址列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectAddressList")
    @ResponseBody
    public JSONResult selectAddressList(HttpServletRequest request,Address address) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        address.setUserID(user.getId());
        List<Address> rsList = addressService.selectList(address);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 新增地址
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertAddress", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertAddress(HttpServletRequest request,Address address) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        Address rs=new Address();
        rs.setUserID(user.getId());
        List<Address> rsList = addressService.selectList(rs);
        jsonResult = new JSONResult();
        if(rsList.size()==0){
            address.setIsdefault(ADDRESS_DEFAULT_YES);
        }
        address.setUserID(user.getId());
        addressService.insert(address);
        return jsonResult;
    }

    /**
     * 设置默认地址
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "setDefault", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult setDefault(HttpServletRequest request,Address address) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        Address rs=new Address();
        rs.setUserID(user.getId());
        List<Address> rsList=addressService.selectList(rs);
        for(Address addressInfo : rsList){
            if(ADDRESS_DEFAULT_YES.equals(addressInfo.getIsdefault())){
                addressInfo.setIsdefault(ADDRESS_DEFAULT_NO);
                addressService.update(addressInfo);
            }
        }
        jsonResult = new JSONResult();
        address.setIsdefault(ADDRESS_DEFAULT_YES);
        addressService.update(address);
        return jsonResult;
    }

    /**
     * 查询用户默认地址
     * @param address
     * @return
     */
    @RequestMapping(value = "selectDefault", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDefault(HttpServletRequest request,Address address) {
        User user = (User) request.getSession().getAttribute("userInfo");
        address.setUserID(user.getId());
        address.setIsdefault(ADDRESS_DEFAULT_YES);
        address =  addressService.selectOne(address);
        jsonResult = new JSONResult();
        jsonResult.setData(address);
        return jsonResult;
    }
}

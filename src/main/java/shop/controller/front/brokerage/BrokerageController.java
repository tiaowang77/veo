/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.brokerage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.services.front.brokerage.BrokerageService;
import shop.services.front.brokerage.bean.Brokerage;

import javax.annotation.Resource;

/**   
 * @类名称：BrokerageController
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:32      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/brokerage/")
public class BrokerageController extends FrontBaseController<Brokerage> {
    @Resource
    private BrokerageService brokerageService;
    @Override
    public Services<Brokerage> getService() {
        return brokerageService;
    }

}

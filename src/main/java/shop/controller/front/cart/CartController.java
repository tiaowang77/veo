/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.front.cart;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.cart.CartService;
import shop.services.front.cart.bean.Cart;
import shop.services.front.spec.SpecService;
import shop.services.front.spec.bean.Spec;
import shop.services.manage.system.bean.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @类名称：CartController
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:33:52
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/front/cart/")
public class CartController extends FrontBaseController<Cart> {
    @Resource
    private CartService cartService;
    @Resource
    private SpecService specService;

    @Override
    public Services<Cart> getService() {
        return cartService;
    }

    /**
     * 跳转到用户购物车页面
     *
     * @return
     */
    @RequestMapping("toCartList")
    public String toCartList() {
        return "/front/cart/cartList";
    }


    /**
     * 查询用户购物车
     * @param cart
     * @return
     */
    @RequestMapping(value = "selectCartList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectCartList(HttpServletRequest request, Cart cart) {
        JSONResult jsonResult = new JSONResult();
       User user = (User) request.getSession().getAttribute("userInfo");
        cart.setUserID(user.getId());
        List<Cart> list = cartService.selectList(cart);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 加入购物车
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertCart", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertCart(HttpServletRequest request, Cart cart) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        Spec spec = new Spec();
        spec.setProductID(cart.getProductID());
        spec.setSpecCombination(cart.getSpecName());
        spec = specService.selectOne(spec);//获取商品规格
        cart.setUserID(user.getId());
        cart.setSpecID(spec.getId());
        Cart rs = cart;
        rs = cartService.selectOne(rs);//查询该条购物车
        jsonResult = new JSONResult();
        if (rs != null) {
            rs.setCartNum(rs.getCartNum() + cart.getCartNum());
            cartService.update(rs);//修改商品数量
            return jsonResult;
        } else {
            cartService.insert(cart);
            return jsonResult;
        }
    }

    /**
     * 编辑购物车
     * @param cart
     * @return
     */
    @RequestMapping(value = "updateCart", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateCart(@RequestBody List<Cart> cart) {
        jsonResult = new JSONResult();
        Cart rs = new Cart();
        if (cart != null && cart.size() > 0) {//判断对象是否为空
            for (Cart cartInfo : cart) {//进行遍历并赋值
                rs.setId(cartInfo.getId());
                rs.setCartNum(cartInfo.getCartNum());
                cartService.update(rs);
            }
        }
        return jsonResult;
    }

    /**
     * 查询购物车数量
     * @param cart
     * @return
     */
    @RequestMapping(value = "selectAmount", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectAmount(HttpServletRequest request, Cart cart) {
        User user = (User) request.getSession().getAttribute("userInfo");
        cart.setUserID(user.getId());
        List<Cart> rsList = cartService.selectList(cart);
        int amount = 0;
        if (rsList.size() != 0) {
            for (int i = 0; i < rsList.size(); i++) {
                amount += rsList.get(i).getCartNum();
            }
        } else {
        }
        jsonResult = new JSONResult();
        jsonResult.setData(amount);
        return jsonResult;
    }


    /**
     * 删除购物车
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "deleteCart", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteCart(HttpServletRequest request, @RequestParam(value = "ids[]") String[] ids) throws Exception {
        Cart cart = new Cart();
        for (int i = 0; i < ids.length; i++) {
            cart.setId(ids[i]);
            cartService.delete(cart);
        }
        jsonResult = new JSONResult();
        return jsonResult;
    }

    /**
     * 查询订单详情（购物车入口）
     * @param cartIDArr
     * @return
     */
    @RequestMapping(value = "selectOrderDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectOrderDetail(@RequestParam(value = "cartIDArr[]") String[] cartIDArr) {
        JSONResult rs = new JSONResult();
        List<Cart> cartList = new ArrayList<>();//创建集合对象
        for (int i = 0; i < cartIDArr.length; i++) {
            cartList.add(cartService.selectById(cartIDArr[i]));
        }
        rs.setData(cartList);
        return rs;
    }
}

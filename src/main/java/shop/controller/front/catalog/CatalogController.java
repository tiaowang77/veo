/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.catalog;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.services.front.catalog.CatalogService;
import shop.services.front.catalog.bean.Catalog;

import javax.annotation.Resource;

/**   
 * @类名称：CatalogController
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:34:37      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/catalog/")
public class CatalogController extends FrontBaseController<Catalog> {
    @Resource
    private CatalogService catalogService;
    @Override
    public Services<Catalog> getService() {
        return catalogService;
    }
}

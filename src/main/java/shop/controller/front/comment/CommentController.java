/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.comment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.comment.CommentService;
import shop.services.front.comment.bean.Comment;
import shop.services.front.orderdetail.OrderdetailService;
import shop.services.front.orderdetail.bean.Orderdetail;
import shop.services.manage.system.bean.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @类名称：CommentController
 * @创建人：Wzl
 * @创建时间：2017-12-14 下午15:27:40
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/front/comment/")
public class CommentController extends FrontBaseController<Comment> {
    private final static String ORDERDETAIL_COMMENT_YES = "y";
    private final static String ORDERDETAIL_COMMENT_NO = "n";
    @Resource
    private CommentService commentService;
    @Resource
    private OrderdetailService orderdetailService;

    @Override
    public Services<Comment> getService() {
        return commentService;
    }


    /**
     * 跳转到评价列表页面
     *
     * @return
     */
    @RequestMapping("toCommentList")
    public String toCommentList() {
        return "/front/comment/commentList";
    }
    /**
     * 跳转到添加评价页面
     *
     * @return
     */
    @RequestMapping("toCommentAdd")
    public String toCommentAdd() {
        return "/front/comment/addComment";
    }

    /**
     * 跳转到查看评价页面
     *
     * @return
     */
    @RequestMapping("toCommentDetail")
    public String toCommentDetail() {
        return "/front/comment/commentDetail";
    }

    /**
     * 查询商品评价列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectCommentList")
    @ResponseBody
    public JSONResult selectCommentList(Comment comment) throws Exception {
        String rank=comment.getRank();
        switch(rank){
            case "1":
                rank="好评";
                break;
            case "2":
                rank="中评";
                break;
            case "3":
                rank="差评";
                break;
        }
        comment.setRank(rank);
        List<Comment> commentList =commentService.selectList(comment);
        jsonResult = new JSONResult();
        jsonResult.setData(commentList);
        return jsonResult;
    }

    /** 查询用户评价详情
     * @param comment
     * @return
     */
    @RequestMapping(value = "selectCommentDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectCommentDetail(HttpSession session, Comment comment) {
        User user = (User) session.getAttribute("userInfo");
        comment.setUserID(user.getId());
        comment = commentService.selectCommentDetail(comment);
        jsonResult = new JSONResult();
        jsonResult.setData(comment);
        return jsonResult;
    }

    /**
     * 添加用户评价
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertComment", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertComment(Comment comment) throws Exception {
        jsonResult = new JSONResult();
        commentService.insert(comment);
        Orderdetail orderdetail = new Orderdetail();
        orderdetail.setId(comment.getOID());
        orderdetail = orderdetailService.selectOne(orderdetail);
        orderdetail.setIsComment(ORDERDETAIL_COMMENT_YES);
        orderdetailService.update(orderdetail);
        return jsonResult;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.favorite;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.favorite.FavoriteService;
import shop.services.front.favorite.bean.Favorite;
import shop.services.front.product.ProductService;
import shop.services.front.product.bean.Product;
import shop.services.front.spec.SpecService;
import shop.services.front.spec.bean.Spec;

import javax.annotation.Resource;
import java.util.List;

/**   
 * @类名称：FavoriteController
 * @创建人：wzl
 * @创建时间：2017-12-14 下午15:28:28      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/favorite/")
public class FavoriteController extends FrontBaseController<Favorite> {
    @Resource
    private FavoriteService favoriteService;
    @Resource
    private SpecService specService;
    @Resource
    private ProductService productService;
    @Override
    public Services<Favorite> getService() {
        return favoriteService;
    }


    /**
     * 查询用户收藏记录
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectFavoriteList")
    @ResponseBody
    public JSONResult selectFavoriteList(Favorite favorite) throws Exception {
        List<Favorite> rsList = favoriteService.selectList(favorite);
        for(Favorite favoriteInfo :rsList){
            Spec spec =new Spec();
            spec.setProductID(favoriteInfo.getProductID());
            List<Spec> specList=specService.selectList(spec);
            favoriteInfo.setProductPrice(specList.get(0).getSpecNowPrice());
            Product product =new Product();
            product.setId(favoriteInfo.getProductID());
            product=productService.selectOne(product);
            favoriteInfo.setProductSellCount(product.getSellcount());
        }
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 批量删除用户收藏记录
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "deleteFavorites", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteFavorites(Favorite favorite,@RequestParam(value = "productIDs[]") String[] productIDs) throws Exception {
        for(int i=0;i<productIDs.length;i++){
            favorite.setProductID(productIDs[i]);
            favoriteService.delete(favorite);
        }
        jsonResult = new JSONResult();
        jsonResult.setMessage("success");
        return jsonResult;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.foot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.foot.FootService;
import shop.services.front.foot.bean.Foot;
import shop.services.front.product.ProductService;
import shop.services.front.product.bean.Product;
import shop.services.front.spec.SpecService;
import shop.services.front.spec.bean.Spec;

import javax.annotation.Resource;
import java.util.List;

/**   
 * @类名称：FootController
 * @创建人：wzl
 * @创建时间：2017-12-14 下午15:28:42      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/foot/")
public class FootController extends FrontBaseController<Foot> {
    @Resource
    private FootService footService;
    @Resource
    private SpecService specService;
    @Resource
    private ProductService productService;
    @Override
    public Services<Foot> getService() {
        return footService;
    }


    /**
     * 查询用户足迹
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectFootList")
    @ResponseBody
    public JSONResult selectFavoriteList(Foot foot) throws Exception {
        List<Foot> rsList = footService.selectList(foot);
        for (Foot footInfo : rsList) {
            Spec spec = new Spec();
            spec.setProductID(footInfo.getProductID());
            List<Spec> specList = specService.selectList(spec);
            footInfo.setProductPrice(specList.get(0).getSpecNowPrice());
            Product product =new Product();
            product.setId(footInfo.getProductID());
            product=productService.selectOne(product);
            footInfo.setProductSellCount(product.getSellcount());
        }
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 批量删除用户足迹
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "deleteFoots", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteFoots(Foot foot, @RequestParam(value = "foots[]") String[] productIDs) throws Exception {
        for (int i = 0; i < productIDs.length; i++) {
            foot.setProductID(productIDs[i]);
            footService.delete(foot);
        }
        jsonResult = new JSONResult();
        jsonResult.setMessage("success");
        return jsonResult;
    }

    /**
     * 添加用户足迹
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertFoot", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertFoot(Foot foot) throws Exception {
        jsonResult = new JSONResult();
        Foot footInfo = new Foot();
        footInfo.setUserID(foot.getUserID());
        footInfo.setProductID(foot.getProductID());
        footInfo = footService.selectOne(footInfo);
        if (footInfo == null) {
            footService.insert(foot);
        } else {
            footService.update(foot);
        }
        return jsonResult;
    }
}

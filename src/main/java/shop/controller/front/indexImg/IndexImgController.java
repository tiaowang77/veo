/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.indexImg;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.services.front.indexImg.IndexService;
import shop.services.front.indexImg.bean.IndexImg;

import javax.annotation.Resource;

/**   
 * @类名称：IndexController
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午16:04:29      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/indexImg/")
public class IndexImgController extends FrontBaseController<IndexImg> {
    @Resource
    private IndexService indexService;
    @Override
    public Services<IndexImg> getService() {
        return indexService;
    }
}

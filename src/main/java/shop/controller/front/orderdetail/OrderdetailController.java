/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.orderdetail;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.orderdetail.OrderdetailService;
import shop.services.front.orderdetail.bean.Orderdetail;
import shop.services.manage.system.bean.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**   
 * @类名称：OrderdetailController
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:49:46      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/orderdetail/")
public class OrderdetailController extends FrontBaseController<Orderdetail> {
    @Resource
    private OrderdetailService orderdetailService;
    @Override
    public Services<Orderdetail> getService() {
        return orderdetailService;
    }

    /**
     * 查询用户商品评价列表（待评价、已评价）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectOrderdetailList")
    @ResponseBody
    public JSONResult selectOrderdetailList(HttpSession session,Orderdetail orderdetail) throws Exception {
        User user = (User) session.getAttribute("userInfo");
        orderdetail.setUserID(user.getId());
        List<Orderdetail> rsList = orderdetailService.selectCommentList(orderdetail);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }
}

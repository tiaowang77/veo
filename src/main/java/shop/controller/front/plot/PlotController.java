/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.plot;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.plot.PlotService;
import shop.services.front.plot.bean.Plot;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**   
 * @类名称：PlotController
 * @创建人：Wzl
 * @创建时间：2018-01-02 下午19:26:29      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/plot")
public class PlotController extends FrontBaseController<Plot> {
    @Resource
    private PlotService plotService;
    @Override
    public Services<Plot> getService() {
        return plotService;
    }



}

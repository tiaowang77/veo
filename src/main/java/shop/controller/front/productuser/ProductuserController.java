/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.productuser;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.productuser.ProductuserService;
import shop.services.front.productuser.bean.Productuser;

import javax.annotation.Resource;
import java.util.List;

/**   
 * @类名称：ProductuserController
 * @创建人：Wzl
 * @创建时间：2018-06-09 下午14:44:56      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/productuser")
public class ProductuserController extends FrontBaseController<Productuser> {
    @Resource
    private ProductuserService productuserService;
    @Override
    public Services<Productuser> getService() {
        return productuserService;
    }
}

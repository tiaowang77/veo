/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.record;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.order.OrderService;
import shop.services.front.order.bean.Order;
import shop.services.front.orderdetail.OrderdetailService;
import shop.services.front.orderdetail.bean.Orderdetail;
import shop.services.front.record.RecordService;
import shop.services.front.record.bean.Record;
import shop.services.front.wallet.WalletService;
import shop.services.front.wallet.bean.Wallet;
import shop.services.manage.system.bean.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**   
 * @类名称：RecordController
 * @创建人：Wzl
 * @创建时间：2017-10-13 下午16:13:14      
 * @版本号：1.0
 * @描述：用户交易记录
 */
@Controller
@RequestMapping("/front/record/")
public class RecordController extends FrontBaseController<Record> {
    @Resource
    private RecordService recordService;
    @Resource
    private OrderService orderService;
    @Resource
    private OrderdetailService orderdetailService;
    @Resource
    private WalletService walletService;
    @Override
    public Services<Record> getService() {
        return recordService;
    }


    /**
     * 跳转到交易记录页面（列表）
     *
     * @return
     */
    @RequestMapping("toRecordList")
    public String toRecordList() {return "/front/wallet/walletList";}

    /**
     * 跳转到提现申请页面
     *
     * @return
     */
    @RequestMapping("toWalletAdd")
    public String toWalletAdd() {return "/front/wallet/walletAdd";}

    /**
     * 跳转到选择提现方式页面
     *
     * @return
     */
    @RequestMapping("toWalletChoose")
    public String toWalletChoose() {return "/front/wallet/walletSelect";}

    /**
     * 跳转到收入详情页面
     *
     * @return
     */
    @RequestMapping("toInComeDetail")
    public String toInComeDetail() {return "/front/wallet/inComeDetail";}

    /**
     * 跳转到支出详情页面
     *
     * @return
     */
    @RequestMapping("toOutComeDetail")
    public String toOutComeDetail() {return "/front/wallet/outComeDetail";}


    /**
     * 查询分销商交易记录列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectRecordList")
    @ResponseBody
    public JSONResult selectRecordList(HttpServletRequest request,Record record) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        record.setUserID(user.getId());
        List<Record> rsList = recordService.selectList(record);
        for(Record recordInfo :rsList){
            if("0".equals(recordInfo.getTransType())){
                Wallet wallet=new Wallet();
                wallet.setId(recordInfo.getTransID());
                wallet=walletService.selectOne(wallet);
                recordInfo.setCashStatus(wallet.getStatus());
            }
        }
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询收入详情
     * @param record
     * @return
     */
    @RequestMapping(value = "selectInComeDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectInComeDetail(Record record) {
        Order order=new Order();
        order.setId(record.getTransID());
        order =  orderService.selectOne(order);
        Orderdetail orderdetail=new Orderdetail();
        orderdetail.setOrderID(order.getId());
        List<Orderdetail> orderdetailList= orderdetailService.selectOrderDetailList(orderdetail);
        return new JSONResult(orderdetailList,true);
    }
}

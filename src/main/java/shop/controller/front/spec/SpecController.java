/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.spec;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.services.front.spec.SpecService;
import shop.services.front.spec.bean.Spec;

import javax.annotation.Resource;

/**   
 * @类名称：SpecController
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:51:23      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/spec/")
public class SpecController extends FrontBaseController<Spec> {
    @Resource
    private SpecService specService;
    @Override
    public Services<Spec> getService() {
        return specService;
    }

}

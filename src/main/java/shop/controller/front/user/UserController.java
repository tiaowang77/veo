package shop.controller.front.user;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.core.plugins.sms.SMS;
import shop.core.plugins.sms.SMSSendCodeResult;
import shop.core.plugins.sms.SMSVerifyCodeResult;
import shop.core.plugins.wxlogin.AccessToken;
import shop.core.plugins.wxlogin.WxUrlType;
import shop.core.plugins.wxlogin.WxUtil;
import shop.core.plugins.wxpay.WXPayConstants;
import shop.core.plugins.wxpay.WXPayUtil;
import shop.core.util.QRcodeUtils;
import shop.core.util.StrUtils;
import shop.services.front.productuser.ProductuserService;
import shop.services.front.productuser.bean.Productuser;
import shop.services.front.systemSetting.SystemSettingService;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by wzl on 2017/9/21.
 */
@Controller
@RequestMapping("/front/user/")
public class UserController extends FrontBaseController<User> {
    @Resource
    private UserService userService;
    @Resource
    private ProductuserService productuserService;
    @Resource
    private SystemSettingService systemSettingService;

    @Override
    public Services<User> getService() {
        return userService;
    }


    /**
     * 跳转到分享
     *
     * @return
     */
    @RequestMapping("toShare")
    public String toShare() {
        return "/front/userCenter/share";
    }

    /**
     * 跳转到个人中心页面
     *
     * @return
     */
    @RequestMapping("toMyPage")
    public String toMyPage() {
        return "/front/userCenter/userList";
    }

    /**
     * 跳转到个人信息页面
     *
     * @return
     */
    @RequestMapping("toUserInfo")
    public String toUserInfo() {
        return "/front/userCenter/userSet";
    }

    /**
     * 跳转到信息修改页面
     *
     * @return
     */
    @RequestMapping("toRevise")
    public String toBind() {
        return "/front/userCenter/revise";
    }

    /**
     * 跳转到我的收藏页面
     *
     * @return
     */
    @RequestMapping("toMyFavorite")
    public String toMyFavorite() {
        return "/front/userCenter/collectionList";
    }

    /**
     * 跳转到我的足迹页面
     *
     * @return
     */
    @RequestMapping("toMyFoot")
    public String toMyFoot() {
        return "/front/userCenter/browsingHistory";

    }


    /**
     * 微信授权登陆
     *
     * @return
     */
    @RequestMapping(value = "toWx", method = RequestMethod.GET)
    public String toWx(HttpServletRequest request) {
        String shareID = request.getParameter("shareID");
        String appidUrl ="?appid="+systemManager.getSystemSetting().getAppid().trim()+"&";
        String redirect_uri = "redirect_uri="+systemManager.getSystemSetting().getWww()+"/rest/front/user/callBack";
        String typeUrl = "&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
        if (!StringUtils.isBlank(shareID)) {
            redirect_uri = redirect_uri + "?shareID=" + shareID;
        }
        logger.debug("redirect_uri"+redirect_uri);
        return "redirect:" + WxUrlType.authorizeUrl + appidUrl + redirect_uri + typeUrl;
    }

    /**
     * 微信回调方法
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "callBack", method = RequestMethod.GET)
    public String callBack(HttpSession session, HttpServletRequest request) {
        String shareID = request.getParameter("shareID");
        logger.debug("分享shareID: "+shareID);
        String code = request.getParameter("code");
        Map<String, String> params = new HashMap<>();
        params.put("appid",systemManager.getSystemSetting().getAppid());
        params.put("secret", systemManager.getSystemSetting().getSecret());
        params.put("code", code);
        params.put("grant_type", "authorization_code");
        AccessToken accessToken = WxUtil.sendRequest(WxUrlType.accessTokenUrl, HttpMethod.GET, params, null, AccessToken.class);
        Map<String, String> getUserInfoParams = new HashMap<>();
        getUserInfoParams.put("access_token", accessToken.getAccess_token());
        getUserInfoParams.put("openid", accessToken.getOpenid());
        getUserInfoParams.put("lang", "zh_CN");
        Map<String, String> wxReturn = WxUtil.sendRequest(WxUrlType.userInfoUrl, HttpMethod.GET, getUserInfoParams, null, Map.class);
        logger.debug("微信登陆返回信息：" + wxReturn);
        String openID = accessToken.getOpenid();
        logger.debug("userOpenId：" + openID);
        session.setAttribute("userOpenId", openID);
        User user = userService.selectByOpenId(accessToken.getOpenid());
        if (user != null) {
            if(!StringUtils.isBlank(shareID)){
                String []shareIDs=shareID.split("_");
                Productuser productuser=new Productuser();
                Productuser productuser2=new Productuser();
                Productuser rs=new Productuser();
                Productuser rs2=new Productuser();
                productuser.setUserID(shareIDs[0]);
                productuser2.setUserID(user.getId());
                productuser2.setPid(shareIDs[0]);
                productuser.setProductID(shareIDs[1]);
                productuser2.setProductID(shareIDs[1]);
                rs.setUserID(shareIDs[0]);
                rs2.setUserID(shareIDs[0]);
                rs.setProductID(shareIDs[1]);
                rs2.setProductID(user.getId());
                rs=productuserService.selectOne(rs);
                rs2=productuserService.selectOne(rs2);
                if(rs==null){
                    productuserService.insert(productuser);
                }
                if(rs2==null){
                    productuserService.insert(productuser2);
                }

            }
            userService.update(user);
            session.setAttribute("userInfo", user);
            return "redirect:/rest/front/product/toGroupPurchase";
        } else {
            User loginUser = new User();
            loginUser.setOpenID(wxReturn.get("openid"));
            loginUser.setNickname(wxReturn.get("nickname"));
            loginUser.setSex(String.valueOf(wxReturn.get("sex")));
            loginUser.setIcon(wxReturn.get("headimgurl"));
            String username = StrUtils.getUUID();
            String password = StrUtils.getUUID();
            loginUser.setUsername(username);
            loginUser.setPassword(password);
            loginUser.setRid("4");
            userService.insert(loginUser);
            session.setAttribute("userInfo", loginUser);
            if(!StringUtils.isBlank(shareID)){
                String []shareIDs=shareID.split("_");
                Productuser productuser=new Productuser();
                Productuser productuser2=new Productuser();
                Productuser rs=new Productuser();
                Productuser rs2=new Productuser();
                productuser.setUserID(shareIDs[0]);
                productuser2.setUserID(loginUser.getId());
                productuser2.setPid(shareIDs[0]);
                productuser.setProductID(shareIDs[1]);
                productuser2.setProductID(shareIDs[1]);
                rs.setUserID(shareIDs[0]);
                rs2.setUserID(user.getId());
                rs.setProductID(shareIDs[1]);
                rs2.setProductID(shareIDs[1]);
                rs=productuserService.selectOne(rs);
                rs2=productuserService.selectOne(rs2);
                if(rs==null)
                    productuserService.insert(productuser);
                if(rs2==null)
                    productuserService.insert(productuser2);
            }
            return "redirect:/rest/front/product/toGroupPurchase";
        }
    }

    /**
     * 微信扫一扫
     *
     * @return
     */
    @RequestMapping("toScan")
    @ResponseBody
    public JSONResult wxScan(String pageUrl) {
        jsonResult = new JSONResult();
        //随机生成签名的时间戳
        String timeStamp = String.valueOf(new Date().getTime() / 1000);
        //随机生成签名，使用UUID来获取唯一签名
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        Map resMap = new HashMap<>();
        try {
            Map<String, String> reqData = new HashMap<String, String>();
            reqData.put("jsapi_ticket", systemManager.getJsapiTicket());
            reqData.put("timestamp", timeStamp);
            reqData.put("noncestr", nonceStr);
            //微信上传图片所对应的接口，使用全部路径
            reqData.put("url", pageUrl);
            String signature = WXPayUtil.generateSignature(reqData, null, WXPayConstants.SignType.SHA1);
            resMap.put("appId", systemManager.getSystemSetting().getAppid());
            resMap.put("timestamp", timeStamp);
            resMap.put("noncestr", nonceStr);
            resMap.put("signature", signature);
            logger.info("获取微信参数:" + resMap);
        } catch (Exception e) {
            logger.error("获取微信jsapi参数失败:" + e.toString());
            jsonResult.setSuccess(false);
            jsonResult.setMessage("获取微信jsapi参数失败");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        jsonResult.setData(resMap);
        return jsonResult;
    }

    /**
     * 获取缓存用户信息
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "getSession", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult getSession(HttpSession session) {
        JSONResult jsonResult = new JSONResult();
        Map result = new HashMap<>();
        if (session.getAttribute("userInfo") != null) {
            result.put("user", session.getAttribute("userInfo"));
        } else {
            result.put("user", null);
        }
        jsonResult.setData(result);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 更新缓存用户信息
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "updateSession", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult updateSession(HttpSession session) {
        JSONResult jsonResult = new JSONResult();
        Map result = new HashMap<>();
        if (session.getAttribute("userInfo") != null) {
            User user = (User) session.getAttribute("userInfo");
            user = userService.selectById(user.getId());
            session.setAttribute("userInfo", user);
            result.put("user", session.getAttribute("userInfo"));
        } else {
            result.put("user", null);
        }
        jsonResult.setData(result);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询合伙人返利比例
     *
     * @param
     * @return
     */
    @RequestMapping(value = "selectPercent", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectPercent(HttpSession session) {
        jsonResult = new JSONResult();
        User user = (User) session.getAttribute("userInfo");
        BigDecimal percent = new BigDecimal(0);
        if (!"0".equals(user.getPid())) {
            User supUser = new User();
            supUser.setId(user.getPid());
            supUser = userService.selectOne(supUser);
            percent = supUser.getPercent();
        }
        jsonResult.setData(percent);
        return jsonResult;
    }

    /**
     * 查询登录用户
     *
     * @param
     * @return
     */
    @RequestMapping(value = "selectUser", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectUser(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("userInfo");
        jsonResult = new JSONResult();
        jsonResult.setData(user);
        return jsonResult;
    }

    /**
     * 查询登录用户(cs)
     *
     * @param
     * @return
     */
    @RequestMapping(value = "selectUserCS", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectUserCS(HttpServletRequest request) {
        User user = new User();
        user.setId(request.getParameter("userID"));
        user = userService.selectOne(user);
        jsonResult = new JSONResult();
        jsonResult.setData(user);
        return jsonResult;
    }


    /**
     * 更新用户信息
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateUser(HttpServletRequest request, HttpSession session, User user) throws Exception {
        User rs = (User) request.getSession().getAttribute("userInfo");
        user.setId(rs.getId());
        jsonResult = new JSONResult();
        userService.update(user);
        User rs2 = userService.selectOne(user);
        session.setAttribute("userInfo", rs2);//更新缓存
        return jsonResult;
    }

    /**
     * 发送验证码（绑定手机）
     *
     * @throws Exception
     */
    @RequestMapping(value = "getSMSCode", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult getSMSCode(HttpServletRequest request) throws Exception {
        JSONResult rs = new JSONResult();
        String mobile = request.getParameter("mobile");
        //根据手机号码查询用户信息，判断是否存在手机号码
        User user = new User();
        user.setPhone(mobile);
        List<User> userList = userService.selectList(user);

        if (userList.size() > 0) {
            rs.setSuccess(false);
            rs.setMessage("该手机已绑定其它账号，请重新填写手机号码！");
            return rs;
        }
        //发送短信
        SMS sms = new SMS();
        SMSSendCodeResult smsSendCodeResult = sms.sendCode(mobile, systemManager.getSystemSetting().getSmsTemplateId(), "86", null, null);
        if (smsSendCodeResult.getCode() == 200) {
            rs.setSuccess(true);
            rs.setMessage("短信发送成功！");
            rs.setData(smsSendCodeResult);
            logger.debug("验证码返回信息" + rs);
            return rs;
        } else {
            rs.setSuccess(false);
            rs.setMessage("短信发送失败！");
            return rs;
        }
    }

    /**
     * 校验验证码
     *
     * @throws Exception
     */
    @RequestMapping(value = "checkCode", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult checkCode(HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        User user = (User) request.getSession().getAttribute("userInfo");
        // SmsId 短信验证码唯一标识   SmsContent 短信验证码内容
        String smsId = request.getParameter("SmsId");
        String smsContent = request.getParameter("SmsContent");
        String phone = request.getParameter("phone");
        SMS sms = new SMS();
        SMSVerifyCodeResult rs = sms.verifyCode(smsId, smsContent);//验证码验证方法
        if (rs.getSuccess() == null || !rs.getSuccess()) {
            jsonResult.setSuccess(false);
            jsonResult.setMessage("短信验证码错误！");
        } else {
            jsonResult.setSuccess(true);
            user.setPhone(phone);
            userService.update(user);
        }
        return jsonResult;
    }

    /**
     * 查询用户下级列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectSubList")
    @ResponseBody
    public JSONResult selectSubList(HttpServletRequest request) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        User rs = new User();
        rs.setPid(user.getId());
        List<User> rsList = userService.selectSubList(rs);
        jsonResult = new JSONResult();
        if (rsList.size() != 0) {
            jsonResult.setData(rsList);
        }
        return jsonResult;
    }
}

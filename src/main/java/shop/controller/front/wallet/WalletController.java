/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.wallet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.core.plugins.wxmsg.WxMsgUtil;
import shop.services.front.record.RecordService;
import shop.services.front.record.bean.Record;
import shop.services.front.wallet.WalletService;
import shop.services.front.wallet.bean.Wallet;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**   
 * @类名称：WalletController
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:55:25      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/wallet/")
public class WalletController extends FrontBaseController<Wallet> {
    @Resource
    private WalletService walletService;
    @Resource
    private RecordService recordService;
    @Resource
    private UserService userService;
    @Override
    public Services<Wallet> getService() {
        return walletService;
    }


    /**
     * 跳转到提现记录页面（列表）
     *
     * @return
     */
    @RequestMapping("toWalletList")
    public String toWalletList() {return "/front/wallet/walletList";}

    /**
     * 跳转到提现申请页面
     *
     * @return
     */
    @RequestMapping("toWalletAdd")
    public String toWalletAdd() {return "/front/wallet/walletAdd";}

    /**
     * 跳转到提现详情页面
     *
     * @return
     */
    @RequestMapping("toWalletDetail")
    public String toWalletChoose() {return "/front/wallet/walletDetail";}


    /**
     * 查询用户提现记录
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectWalletList")
    @ResponseBody
    public JSONResult selectWalletList(HttpServletRequest request,Wallet wallet) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        wallet.setApplicant(user.getId());
        List<Wallet> rsList = walletService.selectList(wallet);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 发起提现
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertWallet", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertWallet(HttpServletRequest request, Wallet wallet) throws Exception {
        jsonResult = new JSONResult();
        User user = (User) request.getSession().getAttribute("userInfo");
        wallet.setApplicant(user.getId());
        wallet.setApplicantName(user.getNickname());
        wallet.setStatus("1");
        if (user.getWalletMoney().compareTo(wallet.getMoney()) != -1) {
            int transID = walletService.insert(wallet);//插入提现表
            user.setWalletMoney(user.getWalletMoney().subtract(wallet.getMoney()));
            userService.update(user);//更新用户钱包
            Record record = new Record();
            record.setUserID(user.getId());
            record.setTransType("0");
            record.setTransID(String.valueOf(transID));
            record.setMoney(wallet.getMoney());
            recordService.insert(record);//插入交易记录表
            User manager=new User();
            manager.setRid("2");
            List<User> managerList=userService.selectList(manager);
            for(User managerInfo: managerList){
                //提现处理提醒
                Map<String, String> msgParam = new HashMap<>();
                msgParam.put("first", "有一笔新的用户提现申请需要处理！");
                msgParam.put("keyword1", user.getNickname());
                msgParam.put("keyword2", "提现金额"+wallet.getMoney()+"元，待审核。");
                msgParam.put("remark", "请及时处理该申请");
                WxMsgUtil.sendWalletCheckMsg(msgParam, systemManager, managerInfo.getOpenID());
            }
            return jsonResult;
        } else {
            jsonResult.setMessage("您的可提现金额不足，请重新输入提现金额！");
            jsonResult.setSuccess(false);
            return jsonResult;
        }
    }
}

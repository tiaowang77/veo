package shop.controller.manage.account;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.BaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * 用户管理
 * @类名称：AccountAction
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:33:04
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/account/")
public class AccountAction extends BaseController<User> {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AccountAction.class);

    private static final String page_toList = "/manage/account/accountList";
    private static final String page_toEdit = "/manage/account/accountDetail";

    public AccountAction() {
        super.page_toEdit = page_toEdit;
        super.page_toList = page_toList;
        super.page_toAdd = null;
    }


    @Autowired
    private UserService userService;

    @Override
    public Services<User> getService() {
        return userService;
    }
    /**
     * 跳转到查看详情页面，通讯录
     */
    @RequestMapping("toDetailList")
    public String toDetailList(User e , ModelMap modelMap){
        modelMap.addAttribute("e",e);
        return page_toEdit;
    }
    /**
     * 分页查询所有微信用户上级列表
     */
    @RequestMapping("loadDataAccount")
    @ResponseBody
    public PagerModel loadDataAccount(HttpServletRequest request, User e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        PagerModel pager = userService.selectPageAccountList(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }
    /**
     * 根据id批量删除微信上级用户,并解除与下级的关系
     */
    @RequestMapping(value = "deletesAccount",method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deletesAccount (@RequestParam(value = "ids[]") String[] ids){
        JSONResult jsonResult=new JSONResult();
        User user=new User();
        if (ids == null || ids.length == 0) {
            throw new NullPointerException("请选择需要删除用户！");
        }else {
            for (int i=0;i<ids.length;i++){
                user.setId(ids[i]);
                userService.delete(user);
                userService.updateJunior(user);
            }
            jsonResult.setSuccess(true);
        }
        return jsonResult;
    }
    /**
     * 根据id删除单个上级，并解除与下级的关系
     */
    @RequestMapping(value = "deleteOne",method = RequestMethod.POST)
    @ResponseBody
    public JSONResult detelteOne(User e){
        JSONResult jsonResult=new JSONResult();
        userService.delete(e);
        userService.updateJunior(e);
        jsonResult.setSuccess(true);
        return jsonResult;
    }
    /**
     * 通过父级id分页查询其所有下级用户
     */
    @RequestMapping("loadDataDetail")
    @ResponseBody
    public PagerModel loadDataDetail(HttpServletRequest request, User e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        PagerModel pager = userService.selectPageJuniorList(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }

    /**
     * 批量设置合伙人
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "setPartner", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult setPartner(@RequestParam(value = "ids[]") String[] ids,User user) throws Exception {
        jsonResult = new JSONResult();
        for(int i=0;i<ids.length;i++){
            user.setId(ids[i]);
            user.setRid("3");
            userService.update(user);
        }
        return jsonResult;
    }

    /**
     * 批量设置自营账号
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "setManager", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult setManager(@RequestParam(value = "ids[]") String[] ids,User user) throws Exception {
        jsonResult = new JSONResult();
        for(int i=0;i<ids.length;i++){
            user.setId(ids[i]);
            user.setRid("2");
            userService.update(user);
        }
        return jsonResult;
    }

    /**
     * 取消用户设置
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "setCommon", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult setCommon(@RequestParam(value = "ids[]") String[] ids,User user) throws Exception {
        jsonResult = new JSONResult();
        for(int i=0;i<ids.length;i++){
            user.setId(ids[i]);
            user.setRid("4");
            userService.update(user);
        }
        return jsonResult;
    }

    /**
     * 设置返利比例
     * @param user
     * @return
     */
    @RequestMapping(value = "editPercent", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult editPercent(User user){
        JSONResult jsonResult=new JSONResult();
        if (user.getPercent().doubleValue()>=100){
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        BigDecimal decimal=new BigDecimal(100);
        BigDecimal percentInfo=user.getPercent().divide(decimal);
        user.setPercent(percentInfo);
        userService.update(user);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

}

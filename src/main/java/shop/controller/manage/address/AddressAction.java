/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.address;
import shop.core.common.bean.JSONResult;
import shop.core.util.KeyValueHelper;
import shop.core.Services;
import shop.core.util.MD5;
import shop.services.manage.address.AddressService;
import shop.services.manage.address.bean.Address;
import shop.core.BaseController;
import shop.core.RequestHolder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**   
 * @类名称：AddressAction      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:04      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/address/")
public class AddressAction extends BaseController<Address> {
    private static final Logger logger = LoggerFactory.getLogger(AddressAction.class);
    @Autowired
    private AddressService addressService;
    private static final String page_toList = "/manage/address/addressList";
    public AddressService getService() {
        return addressService;
    }

    private AddressAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.brokerage;
import shop.core.common.bean.JSONResult;
import shop.core.util.KeyValueHelper;
import shop.core.Services;
import shop.core.util.MD5;
import shop.services.manage.brokerage.BrokerageService;
import shop.services.manage.brokerage.bean.Brokerage;
import shop.core.BaseController;
import shop.core.RequestHolder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import shop.services.manage.systemSetting.SystemSettingService;
import shop.services.manage.systemSetting.bean.SystemSetting;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**   
 * @类名称：BrokerageAction      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:32      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/brokerage/")
public class BrokerageAction extends BaseController<Brokerage> {
    private static final Logger logger = LoggerFactory.getLogger(BrokerageAction.class);
    @Autowired
    private BrokerageService brokerageService;
    @Autowired
    private SystemSettingService systemSettingService;
    private static final String page_toList = "/manage/brokerage/brokerageList";
    public BrokerageService getService() {
        return brokerageService;
    }

    private BrokerageAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    /**
     * 设置返利比例
     * @param systemSetting
     * @return
     */
    @RequestMapping(value = "editPercent", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult editPercent(SystemSetting systemSetting){
        JSONResult jsonResult=new JSONResult();
        if (systemSetting.getPercent().doubleValue()>=100){
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        BigDecimal decimal=new BigDecimal(100);
        BigDecimal percent1=systemSetting.getPercent().divide(decimal);
        systemSetting.setPercent(percent1);
        systemSettingService.update(systemSetting);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询返利比列
     * @param systemSetting
     * @return
     */
    @RequestMapping(value = "selectPercent",method = RequestMethod.POST)
    @ResponseBody
    public JSONResult selectPercent(SystemSetting systemSetting){
        JSONResult jsonResult=new JSONResult();
        systemSetting=systemSettingService.selectOne(systemSetting);
        jsonResult.setData(systemSetting);
        return  jsonResult;
    }
}

package shop.controller.manage.cache;

import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import shop.core.common.oscache.FrontCache;
import shop.core.common.oscache.ManageCache;
import shop.core.common.oscache.SystemManager;
import shop.core.plugins.wxlogin.WxUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 缓存管理
 * @author lintz
 * */
@Controller
@RequestMapping("/manage/cache/")
public class CacheAction {
    private static final Logger logger = LoggerFactory.getLogger(CacheAction.class);
    private static final String page_toIndex = "/manage/cache/cacheIndex";
    @Autowired
    private SystemManager systemManager;

    @Value(value="classpath:wxMenu.json")
    private org.springframework.core.io.Resource data;

    @RequestMapping(method = RequestMethod.GET)
    public String toIndex() throws Exception {
        return page_toIndex;
    }

    /**
     * 更新所有的后台缓存
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "loadAllCache", method = RequestMethod.GET)
    @ResponseBody
    public String loadAllCache(HttpServletRequest request) throws Exception{
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        ManageCache manageCache = (ManageCache) app.getBean("manageCache");
        manageCache.loadAllCache();
        logger.info("后台缓存更新成功");
        return "success";
    }

    /**
     * 更新所有的前台缓存
     * @param request
     * @return
     */
    @RequestMapping(value = "frontCache", method = RequestMethod.GET)
    @ResponseBody
    public String frontCache(HttpServletRequest request){
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        FrontCache frontCache = (FrontCache) app.getBean("frontCache");
        try{
            frontCache.loadAllCache();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * 更新微信token缓存
     * @param request
     * @return
     */
    @RequestMapping(value = "wxTokenCache", method = RequestMethod.GET)
    @ResponseBody
    public String wxTokenCache(HttpServletRequest request){
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        ManageCache manageCache = (ManageCache) app.getBean("manageCache");
        try{
            manageCache.loadAccessToken();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * 更新微信JsApi缓存
     * @param request
     * @return
     */
    @RequestMapping(value = "wxJsApiCache", method = RequestMethod.GET)
    @ResponseBody
    public String wxJsApiCache(HttpServletRequest request){
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        ManageCache manageCache = (ManageCache) app.getBean("manageCache");
        try{
            manageCache.loadJsapiTicket();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * 更新微信菜单缓存
     * @param request
     * @return
     */
    @RequestMapping(value = "wxMenuCache", method = RequestMethod.GET)
    @ResponseBody
    public String wxMenuCache(HttpServletRequest request){
        Scanner scanner = null;
        StringBuilder buffer = new StringBuilder();
        try {
            File file = data.getFile();
            scanner = new Scanner(file, "utf-8");
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine());
            }
        } catch (Exception e) {

        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        String url = "https://api.weixin.qq.com/cgi-bin/menu/create" ;
        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",systemManager.getAccessToken().getAccess_token());

        Map resMap = WxUtil.sendRequest(url, HttpMethod.POST, getParams, new StringEntity(buffer.toString(), "UTF-8"), Map.class);

        logger.info("创建微信菜单："+resMap);
        return "success";
    }


}

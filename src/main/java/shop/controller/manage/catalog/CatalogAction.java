/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.catalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.services.manage.catalog.CatalogService;
import shop.services.manage.catalog.bean.Catalog;

/**   
 * @类名称：CatalogAction      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:34:37      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/catalog/")
public class CatalogAction extends BaseController<Catalog> {
    private static final Logger logger = LoggerFactory.getLogger(CatalogAction.class);
    @Autowired
    private CatalogService catalogService;
    private static final String page_toList = "/manage/catalog/catalogList";
    public CatalogService getService() {
        return catalogService;
    }

    private CatalogAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    /**
     * 删除单个信息
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "deleteByIdJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteByIdJson(@RequestParam(value = "id") int id) throws Exception {
        JSONResult jsonResult = new JSONResult();
        getService().deleteById(id);
        jsonResult.setMessage("success");
        return jsonResult;
    }

    /**
     * 商品分类顺序--获取已经有序号的对象
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping("toOrder")
    @ResponseBody
    public JSONResult toEditBickName(@ModelAttribute("e") Catalog e, ModelMap model) throws Exception {
        e = catalogService.selectByOrder(e);
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(e);
        return jsonResult;
    }
}

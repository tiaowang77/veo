/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.favorite;
import shop.core.common.bean.JSONResult;
import shop.core.util.KeyValueHelper;
import shop.core.Services;
import shop.core.util.MD5;
import shop.services.manage.favorite.FavoriteService;
import shop.services.manage.favorite.bean.Favorite;
import shop.core.BaseController;
import shop.core.RequestHolder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**   
 * @类名称：FavoriteAction      
 * @创建人：Ltz   
 * @创建时间：2017-12-14 下午15:28:28      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/favorite/")
public class FavoriteAction extends BaseController<Favorite> {
    private static final Logger logger = LoggerFactory.getLogger(FavoriteAction.class);
    @Autowired
    private FavoriteService favoriteService;
    private static final String page_toList = "/manage/favorite/favoriteList";
    public FavoriteService getService() {
        return favoriteService;
    }

    private FavoriteAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}

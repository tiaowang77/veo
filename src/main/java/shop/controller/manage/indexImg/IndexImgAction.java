/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.manage.indexImg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.services.common.SystemSetting;
import shop.services.manage.indexImg.IndexImgService;
import shop.services.manage.indexImg.bean.IndexImg;

import javax.servlet.http.HttpServletRequest;

/**
 * @类名称：IndexAction
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午16:04:29      
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/indexImg/")
public class IndexImgAction extends BaseController<IndexImg> {
    private static final Logger logger = LoggerFactory.getLogger(IndexImgAction.class);
    @Autowired
    private IndexImgService indexImgService;
    private static final String page_toList = "/manage/indexImg/indexImgList";
    private static final String page_toFindEdit = "/manage/indexImg/findEdit";
    private static final String page_toFindUs = "/manage/indexImg/aboutUs";
    private static final String page_toJoinUs = "/manage/indexImg/joinUs";


    public IndexImgService getService() {
        return indexImgService;
    }

    private IndexImgAction() {
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    @RequestMapping("toFindEdit")
    public String page_toFindEdit(HttpServletRequest request, @ModelAttribute("e") SystemSetting e) throws Exception {
        return page_toFindEdit;
    }

    @RequestMapping("toFindUs")
    public String page_toFindUs(HttpServletRequest request, @ModelAttribute("e") SystemSetting e) throws Exception {
        return page_toFindUs;
    }

    @RequestMapping("toJoinUs")
    public String page_toJoinUs(HttpServletRequest request, @ModelAttribute("e") SystemSetting e) throws Exception {
        return page_toJoinUs;
    }

    /**
     * 删除单个信息
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "deleteByIdJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteByIdJson(@RequestParam(value = "id") int id) throws Exception {
        JSONResult jsonResult = new JSONResult();
        getService().deleteById(id);
        jsonResult.setMessage("success");
        return jsonResult;
    }

    /**
     * 轮播图顺序
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping("toOrder")
    @ResponseBody
    public JSONResult toEditBickName(@ModelAttribute("e") IndexImg e, ModelMap model) throws Exception {
        e = indexImgService.selectByOrder(e);
        jsonResult = new JSONResult();
        jsonResult.setData(e);
        return jsonResult;
    }
}

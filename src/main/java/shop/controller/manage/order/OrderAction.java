/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.manage.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.order.OrderService;
import shop.services.manage.order.bean.Order;
import shop.services.manage.orderdetail.OrderdetailService;
import shop.services.manage.orderdetail.bean.Orderdetail;
import shop.services.manage.product.ProductService;
import shop.services.manage.product.bean.Product;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @类名称：OrderAction
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:48:44      
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/order/")
public class OrderAction extends BaseController<Order> {
    private static final Logger logger = LoggerFactory.getLogger(OrderAction.class);
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderdetailService orderdetailService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;
    private static final String page_toList = "/manage/order/orderList";
    private static final String page_toEdit = "/manage/order/orderEdit";

    public OrderService getService() {
        return orderService;
    }

    private OrderAction() {
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = page_toEdit;
    }

    /**
     * 跳转到订单详情页面
     */
    @RequestMapping(value = "toEditOrder")
    public String toEditOrder(String id, ModelMap model) throws Exception {
        Orderdetail e = orderdetailService.selectById(id);
        if (e.getStatus().equals("init")) {
            e.setStatus("待付款");
        } else if (e.getStatus().equals("pass")) {
            e.setStatus("待发货");
        } else if (e.getStatus().equals("send")) {
            e.setStatus("已发货");
        } else if (e.getStatus().equals("finish")) {
            e.setStatus("已完成");
        } else {
            e.setStatus("已关闭");
        }
        model.addAttribute("e", e);
        return page_toEdit;
    }

    /**
     * 订单商品发货处理
     * @param e
     * @return
     */
    @RequestMapping("sendProduct")
    public String sendProduct(Orderdetail e) {
        Order order = new Order();
        //确认收货
        if (e.getStatus().equals("已发货")) {
            order.setId(e.getOrderID());
            order.setStatus("finish");
            orderService.update(order);
        }
        //商品发货
        if (e.getStatus().equals("待发货")) {
            //找出所有订单号为给定订单号的订单详情
            List<Orderdetail> orderdetails = orderdetailService.selectList(e);
            //对一个订单所对应的所有订单详情进行更新物流信息
            for (Orderdetail orderdetail : orderdetails) {
                e.setId(orderdetail.getId());
                orderdetailService.update(e);
            }
            //对应的订单表进行状态更新，状态变成已发货
            order.setId(e.getOrderID());
            order.setStatus("send");
            orderService.update(order);
        }
        return page_toList;
    }

    /**
     * 根据ID批量删除订单详情，以及对应的订单删除或者更新商品ID
     * @param ids
     * @param orderIDs
     * @return
     */
    @RequestMapping(value = "deletesOrderDetail", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deletesOrderDetail(@RequestParam(value = "ids[]") String[] ids, @RequestParam(value = "orderIDs[]") String[] orderIDs) {
        JSONResult jsonResult = new JSONResult();
        Orderdetail orderdetail = new Orderdetail();
        if (ids == null || ids.length == 0) {
            throw new NullPointerException("请选择需要删除订单！");
        } else {
            for (int i = 0; i < ids.length; i++) {
                orderdetail.setOrderID(orderIDs[i]);
                List<Orderdetail> orderdetails = orderdetailService.selectList(orderdetail);
                if (orderdetails.size() == 1) {
                    Order order = new Order();
                    order.setId(orderIDs[i]);
                    orderService.delete(order);
                }
                orderdetail.setId(ids[i]);
                orderdetailService.delete(orderdetail);
            }
            jsonResult.setSuccess(true);
        }
        return jsonResult;
    }

    /**
     * 根据ID删除单个订单详情，以及对应的订单删除或者更新商品ID
     */
    @RequestMapping(value = "deleteOne", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteOne(String id, String orderID) {
        JSONResult jsonResult = new JSONResult();
        Orderdetail orderdetail = new Orderdetail();
        orderdetail.setOrderID(orderID);
        List<Orderdetail> orderdetails = orderdetailService.selectList(orderdetail);
        if (orderdetails.size() == 1) {
            Order order = new Order();
            order.setId(orderID);
            orderService.delete(order);
        }
        orderdetail.setId(id);
        orderdetailService.delete(orderdetail);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 订单详情商品列表
     */
    @RequestMapping("loadProduct")
    @ResponseBody
    public PagerModel loadProduct(HttpServletRequest request, Orderdetail e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        PagerModel pager = orderdetailService.selectPageListProduct(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }


    /**
     * 查询历史订单
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectHistoryOrder")
    @ResponseBody
    public JSONResult selectHistoryOrder(Product product) throws Exception {
        Orderdetail orderdetail = new Orderdetail();
        orderdetail.setProductID(product.getId());
        List<Orderdetail> orderdetailList = orderdetailService.selectHistoryOrderdetail(orderdetail);
        List<Order> orderList = new ArrayList<>();
        for (Orderdetail orderdetail1Info : orderdetailList) {
            Order order = new Order();
            order.setId(orderdetail1Info.getOrderID());
            order = orderService.selectOne(order);
            User user = new User();
            user.setId(order.getUserID());
            user = userService.selectOne(user);
            order.setNickname(user.getNickname());
            order.setProductStartTime(product.getStartDate());
            orderList.add(order);
        }
        jsonResult = new JSONResult();
        jsonResult.setData(orderList);
        return jsonResult;
    }

    /**
     * 查询返利明细
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectBrokerageOrder")
    @ResponseBody
    public JSONResult selectBrokerageOrder(Order order) throws Exception {
        List<Order> orderList=orderService.selectBrokerageOrder(order);
        for(Order orderInfo: orderList){
            User buyer=new User();
            buyer.setId(orderInfo.getUserID());
            buyer=userService.selectOne(buyer);
            User gainer=new User();
            gainer.setId(orderInfo.getSupUserID());
            gainer=userService.selectOne(gainer);
            orderInfo.setNickname(buyer.getNickname());
            orderInfo.setGainer(gainer.getNickname());
        }
        jsonResult = new JSONResult();
        jsonResult.setData(orderList);
        return jsonResult;
    }

}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.orderpay;
import shop.core.common.bean.JSONResult;
import shop.core.util.KeyValueHelper;
import shop.core.Services;
import shop.core.util.MD5;
import shop.services.manage.orderpay.OrderpayService;
import shop.services.manage.orderpay.bean.Orderpay;
import shop.core.BaseController;
import shop.core.RequestHolder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**   
 * @类名称：OrderpayAction      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:50:16      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/orderpay/")
public class OrderpayAction extends BaseController<Orderpay> {
    private static final Logger logger = LoggerFactory.getLogger(OrderpayAction.class);
    @Autowired
    private OrderpayService orderpayService;
    private static final String page_toList = "/manage/orderpay/orderpayList";
    public OrderpayService getService() {
        return orderpayService;
    }

    private OrderpayAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}

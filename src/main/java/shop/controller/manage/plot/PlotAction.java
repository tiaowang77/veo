/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.plot;
import shop.core.common.bean.JSONResult;
import shop.core.util.KeyValueHelper;
import shop.core.Services;
import shop.core.util.MD5;
import shop.services.manage.plot.PlotService;
import shop.services.manage.plot.bean.Plot;
import shop.core.BaseController;
import shop.core.RequestHolder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**   
 * @类名称：PlotAction      
 * @创建人：wzl
 * @创建时间：2018-01-02 下午19:26:29      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/plot/")
public class PlotAction extends BaseController<Plot> {
    private static final Logger logger = LoggerFactory.getLogger(PlotAction.class);
    @Autowired
    private PlotService plotService;
    private static final String page_toList = "/manage/plot/plotList";
    public PlotService getService() {
        return plotService;
    }

    private PlotAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.manage.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.core.plugins.wxmsg.WxMsgUtil;
import shop.services.manage.product.ProductService;
import shop.services.manage.product.bean.Product;
import shop.services.manage.spec.SpecService;
import shop.services.manage.spec.bean.Spec;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @类名称：ProductAction
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:50:43
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/product/")
public class ProductAction extends BaseController<Product> {
    private static final Logger logger = LoggerFactory.getLogger(ProductAction.class);
    @Autowired
    private ProductService productService;
    @Autowired
    private SpecService specService;
    @Autowired
    private UserService userService;

    private static final String page_toList = "/manage/product/productList";
    private static final String page_toAdd = "/manage/product/productAdd";
    private static final String page_toEdit = "/manage/product/productEdit";
    private static final String page_toCheckList = "/manage/group/openGroupList";
    private static final String page_toCheckDetail = "/manage/group/openGroupDetail";

    public ProductService getService() {
        return productService;
    }

    private ProductAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }

    /**
     * 跳转到开团审核列表页面
     *
     * @return
     */
    @RequestMapping("toCheckList")
    public String toCheckList() {
        return page_toCheckList;
    }

    /**
     * 跳转到分销订单详情页面
     *
     * @return
     */
    @RequestMapping("toCheckDetail")
    public String toCheckDetail(@ModelAttribute("e") Product product, ModelMap model) throws Exception {
        product = productService.selectOne(product);
        model.addAttribute("e", product);
        return page_toCheckDetail;
    }

    /**
     * 商品上下架--批量更改
     *
     * @param ids 改变状态的商品ID集合
     * @return
     */
    @RequestMapping(value = "changeStatus", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult changeStatus(HttpServletRequest request, @RequestParam(value = "idArr[]") String[] ids) {
        JSONResult rs = new JSONResult();
        String changeType = request.getParameter("type");
        if (changeType.equals("0")) {
            productService.updateProductStatus(ids, 1);
        } else if (changeType.equals("1")) {
            productService.updateProductStatus(ids, 0);
        }
        rs.setSuccess(true);
        return rs;
    }

    /**
     * 商品上下架--批量更改
     *
     * @param id 改变状态的商品ID
     * @return
     */
    @RequestMapping(value = "changeOneStatus", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult changeOneStatus(HttpServletRequest request, @RequestParam(value = "id") String id) {
        JSONResult rs = new JSONResult();
        String changeType = request.getParameter("type");
        if (changeType.equals("0")) {
            productService.updateOneProductStatus(id, 1);
        } else if (changeType.equals("1")) {
            productService.updateOneProductStatus(id, 0);
        }
        rs.setSuccess(true);
        return rs;
    }

    /**
     * 根据ID删除单个商品信息
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "deleteByIdJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteByIdJson(@RequestParam(value = "id") int id) throws Exception {
        JSONResult jsonResult = new JSONResult();
        getService().deleteById(id);
        jsonResult.setMessage("success");
        return jsonResult;
    }

    /**
     * 添加商品--包括规格
     */
    @RequestMapping(value = "insertAllJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertAllJson(HttpServletRequest request, @ModelAttribute("e") Product e) throws Exception {
        JSONResult jsonResult = new JSONResult();
        int productID = getService().insert(e);
        //强制转化为string类型
        String[] specCombinations = e.getSpecCombination().split(";");
        String[] specStocks = e.getSpecStock().split(";");
        String[] specNowPrices = e.getSpecNowPrice().split(";");
        String[] specPurchasePrices = e.getSpecPurchasePrice().split(";");
        //逐个添加商品规格
        for (int i = 0; i < specCombinations.length; i++) {
            Spec spec = new Spec();
            spec.setProductID(String.valueOf(productID));
            spec.setSpecCombination(specCombinations[i]);
            spec.setSpecStock(Integer.parseInt(specStocks[i]));
            spec.setSpecNowPrice(new BigDecimal(specNowPrices[i]));
            spec.setSpecPurchasePrice(new BigDecimal(specPurchasePrices[i]));
            specService.insert(spec);
        }
        return jsonResult;
    }

    /**
     * 更新商品信息--主要为了更新商品的上下架状态
     */
    @RequestMapping(value = "updateProductJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateProductJson(HttpServletRequest request, @ModelAttribute("e") Product e) throws Exception {
        JSONResult jsonResult = new JSONResult();
        e.setStatus(0);
        getService().update(e);
        return jsonResult;
    }

    /**
     * 批量审核开团商品
     */
    @RequestMapping(value = "updateProducts", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateProducts(@RequestParam(value = "ids[]") String[] ids, Product product) throws Exception {
        JSONResult jsonResult = new JSONResult();
        for (int i = 0; i < ids.length; i++) {
            product.setId(ids[i]);
            product.setCheckStatus("2");
            productService.update(product);
        }
        return jsonResult;
    }


    /**
     * 单个审核开团商品
     */
    @RequestMapping(value = "updateProduct", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateProduct(Product product) throws Exception {
        JSONResult jsonResult = new JSONResult();
        productService.update(product);
        User seller = new User();
        seller.setId(product.getMerchantID());
        seller = userService.selectOne(seller);
        //开团审核结果消息通知
        Map<String, String> msgParam = new HashMap<>();
        msgParam.put("first", "恭喜您，您的商品已通过管理员审核！");
        msgParam.put("keyword1", product.getCheckStatus());
        msgParam.put("keyword2", product.getUpdateTime());
        msgParam.put("remark", "--");
        WxMsgUtil.sendProductCheckMsg(msgParam, systemManager, seller.getOpenID());
        return jsonResult;
    }

}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.productuser;
import shop.core.common.bean.JSONResult;
import shop.core.util.KeyValueHelper;
import shop.core.Services;
import shop.core.util.MD5;
import shop.services.manage.productuser.ProductuserService;
import shop.services.manage.productuser.bean.Productuser;
import shop.core.BaseController;
import shop.core.RequestHolder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**   
 * @类名称：ProductuserAction      
 * @创建人：wzl
 * @创建时间：2018-06-09 下午14:44:56      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/productuser/")
public class ProductuserAction extends BaseController<Productuser> {
    private static final Logger logger = LoggerFactory.getLogger(ProductuserAction.class);
    @Autowired
    private ProductuserService productuserService;
    private static final String page_toList = "/manage/productuser/productuserList";
    public ProductuserService getService() {
        return productuserService;
    }

    private ProductuserAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}

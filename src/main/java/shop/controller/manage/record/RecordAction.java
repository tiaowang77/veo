/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.record;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.BaseController;
import shop.services.manage.record.RecordService;
import shop.services.manage.record.bean.Record;

/**   
 * @类名称：RecordAction      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午16:13:14      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/record/")
public class RecordAction extends BaseController<Record> {
    private static final Logger logger = LoggerFactory.getLogger(RecordAction.class);
    @Autowired
    private RecordService recordService;
    private static final String page_toList = "/manage/record/recordList";
    public RecordService getService() {
        return recordService;
    }

    private RecordAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}

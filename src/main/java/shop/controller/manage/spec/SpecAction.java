/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.manage.spec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.spec.SpecService;
import shop.services.manage.spec.bean.Spec;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @类名称：SpecAction
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:51:23      
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/spec/")
public class SpecAction extends BaseController<Spec> {
    private static final Logger logger = LoggerFactory.getLogger(SpecAction.class);
    @Autowired
    private SpecService specService;
    private static final String page_toList = "/manage/spec/specList";

    public SpecService getService() {
        return specService;
    }

    private SpecAction() {
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    /**
     *添加商品规格，包括得到利润
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertPJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertPJson(HttpServletRequest request, @ModelAttribute("spec") Spec spec) throws Exception {
        JSONResult jsonResult = new JSONResult();
        spec.setProductID(request.getParameter("productID"));      //获取商品ID
        getService().insert(spec);
        return jsonResult;
    }

    /**
     * 根据规格参数删除规格
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deleteByComJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteByIdJson(@RequestParam(value = "specCombination") String specCombination) throws Exception {
        JSONResult jsonResult = new JSONResult();
        getService().deleteByCom(specCombination);
        jsonResult.setMessage("success");
        return jsonResult;
    }

    /**
     * 获取商品规格
     */
    @RequestMapping(value = "/getSpec", method = RequestMethod.GET)
    @ResponseBody
    public PagerModel getSpec(HttpServletRequest request, Spec e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        e.setProductID(request.getParameter("productID"));
        PagerModel pager = specService.selectPageList(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }

    /**
     * 查询商品规格
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectSpecList")
    @ResponseBody
    public JSONResult selectSpecList(Spec spec) throws Exception {
        List<Spec> rsList = specService.selectList(spec);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

}

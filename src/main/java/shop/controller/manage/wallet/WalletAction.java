/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.wallet;
import shop.core.common.bean.JSONResult;
import shop.core.plugins.wxmsg.WxMsgUtil;
import shop.services.manage.brokerage.BrokerageService;
import shop.services.manage.brokerage.bean.Brokerage;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;
import shop.services.manage.wallet.WalletService;
import shop.services.manage.wallet.bean.Wallet;
import shop.core.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**   
 * @类名称：WalletAction      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:55:25      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/wallet/")
public class WalletAction extends BaseController<Wallet> {
    private static final Logger logger = LoggerFactory.getLogger(WalletAction.class);
    private static final String EXAMINE_STATUS_INIT = "1";
    private static final String EXAMINE_STATUS_YES = "2";
    private static final String EXAMINE_STATUS_NO = "3";
    @Autowired
    private WalletService walletService;
    @Autowired
    private UserService userService;
    @Autowired
    private BrokerageService brokerageService;
    private static final String page_toList = "/manage/wallet/walletList";
    public WalletService getService() {
        return walletService;
    }

    private WalletAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    /**
     * 提现审批方法，当审批不通过时需要把钱回退给对应用户
     * @param request
     * @param e
     * @return
     * @throws Exception
     */
    @Override
    @RequestMapping(value = "updateJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateJson(HttpServletRequest request, @ModelAttribute("e") Wallet e) throws Exception {
        jsonResult = new JSONResult();
        User user = userService.selectById(e.getApplicant());
        if (e.getStatus().equals(EXAMINE_STATUS_NO)) {
            BigDecimal walletMoney = user.getWalletMoney().add(e.getMoney());
            user.setWalletMoney(walletMoney);
            userService.update(user);
        }
        getService().update(e);
        return jsonResult;
    }

    /**
     * 计算总的返利金额
     * @return
     */
    @RequestMapping(value = "selectTotalMoney",method = RequestMethod.POST)
    @ResponseBody
    public JSONResult selectTotalMoney(){
        JSONResult jsonResult=new JSONResult();
        BigDecimal totalMoney=new BigDecimal(0.00);
        User user=new User();
        List<User> userList =userService.selectList(user);
        for(int i=0;i<userList.size();i++){
            totalMoney=totalMoney.add(userList.get(i).getWalletMoney());
        }
        jsonResult.setData(totalMoney);
        return  jsonResult;
    }
}

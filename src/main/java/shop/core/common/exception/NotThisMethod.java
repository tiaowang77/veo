package shop.core.common.exception;

/**
 * 自定义异常-不支持此操作异常
 **
 */
public class NotThisMethod extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * @param arg0
	 */
	public NotThisMethod(String arg0) {
		super(arg0);
	}

}

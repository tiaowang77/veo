package shop.core.common.oscache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import shop.core.util.KeyValueHelper;
import shop.services.front.keyvalue.KeyvalueService;
import shop.services.front.keyvalue.bean.Keyvalue;

/**
 * 缓存管理器。 后台项目可以通过接口程序通知该类重新加载部分或全部的缓存
 *
 * @author huangf
 */
public class FrontCache {
    private static final Logger logger = LoggerFactory.getLogger(FrontCache.class);

    @Autowired
    private KeyvalueService keyvalueService;

    private static SystemManager systemManager;

    @Autowired
    public void setSystemManager(SystemManager systemManager) {
        FrontCache.systemManager = systemManager;
    }


    /**
     * 加载key-value键值对
     */
    public void loadKeyValue() {
        logger.info("load...");
        KeyValueHelper.load(keyvalueService.selectList(new Keyvalue()));
    }






    /**
     * 加载全部的缓存数据
     *
     * @throws Exception
     */
    public void loadAllCache() throws Exception {
        logger.info("loadAllCache...");
        loadKeyValue();
        logger.info("前台缓存加载完毕!");
    }
}

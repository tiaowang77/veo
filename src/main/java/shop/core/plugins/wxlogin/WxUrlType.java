package shop.core.plugins.wxlogin;

/**
 * @Description 微信请求路径
 * @Author semstouch
 * @Date 2017/4/23
 **/
public final  class WxUrlType {
    public final  static  String authorizeUrl = "https://open.weixin.qq.com/connect/oauth2/authorize";
    public final  static  String accessTokenUrl  = "https://api.weixin.qq.com/sns/oauth2/access_token";
    public final  static  String userInfoUrl ="https://api.weixin.qq.com/sns/userinfo";
    public final  static  String tokenUrl ="https://api.weixin.qq.com/cgi-bin/token";
    public final  static  String jsapiTicketUrl ="https://api.weixin.qq.com/cgi-bin/ticket/getticket";
    public final  static  String msgTemplateUrl ="https://api.weixin.qq.com/cgi-bin/message/template/send";

}
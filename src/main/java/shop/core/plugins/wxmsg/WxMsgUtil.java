package shop.core.plugins.wxmsg;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import shop.core.common.oscache.SystemManager;
import shop.core.plugins.wxlogin.WxUrlType;
import shop.core.plugins.wxlogin.WxUtil;
import shop.core.util.KeyValueHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description  微信发送模板消息
 * @Author semstouch
 * @Date 2017/7/16
 **/
public class WxMsgUtil {
    protected static final Logger logger = LoggerFactory.getLogger(WxMsgUtil.class);



    /**
     * 模板消息通知：付款成功
     *
     * @param param       要发送的信息内容
     * @param systemManager 系统参数
     * @param openid      需要接收的微信号openid
     * @return 成功或者失败
     */
    public static boolean sendPaySuccessMsg(Map<String, String> param, SystemManager systemManager, String openid) {
        String templateId = KeyValueHelper.get("sendPaySuccessMsg").trim();   //消息模板编号
        Map params = new HashMap();
        params.put("touser", openid);
        params.put("template_id", templateId);
        params.put("url", systemManager.getSystemSetting().getWww()+"/rest/front/order/toOrderList");

        Map msgParam= new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String, String> keyMap = new HashMap();
            keyMap.put("value", entry.getValue());
            keyMap.put("color", "#173177");
            msgParam.put(entry.getKey(), keyMap);
        }

        Map<String, String> getParams = new HashMap<String, String>();
        getParams.put("access_token", systemManager.getAccessToken().getAccess_token());
        params.put("data", msgParam);
        String postData = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData = objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果：" + wxRespCodeEntity.getErrcode());
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }

    /**
     * 模板消息通知：订单处理提醒
     *
     * @param param
     * @param systemManager 系统参数
     * @param openid
     * @return
     */
    public static boolean sendSubOrderMsg(Map<String, String> param,SystemManager systemManager, String openid) {

        String templateId = KeyValueHelper.get("sendSubOrderMsg").trim();   //消息模板编号
        Map params = new HashMap();
        params.put("touser", openid);
        params.put("template_id", templateId);
        params.put("url",systemManager.getSystemSetting().getWww()+"/rest/front/user/toWx");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String, String> keyMap = new HashMap();
            keyMap.put("value", entry.getValue());
            keyMap.put("color", "#173177");
            msgParam.put(entry.getKey(), keyMap);
        }

        Map<String, String> getParams = new HashMap<String, String>();
        getParams.put("access_token", systemManager.getAccessToken().getAccess_token());
        params.put("data", msgParam);
        String postData = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData = objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果：" + wxRespCodeEntity.getErrcode());
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }

    /**
     * 模板消息通知：订单返利提醒
     *
     * @param param
     * @param systemManager 系统参数
     * @param openid
     * @return
     */
    public static boolean sendOrderRebateMsg(Map<String, String> param, SystemManager systemManager, String openid) {
        String templateId = KeyValueHelper.get("sendOrderRebateMsg").trim();   //消息模板编号
        Map params = new HashMap();
        params.put("touser", openid);
        params.put("template_id", templateId);
        params.put("url", systemManager.getSystemSetting().getWww()+"/rest/front/user/toWx");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String, String> keyMap = new HashMap();
            keyMap.put("value", entry.getValue());
            keyMap.put("color", "#173177");
            msgParam.put(entry.getKey(), keyMap);
        }

        Map<String, String> getParams = new HashMap<String, String>();
        getParams.put("access_token", systemManager.getAccessToken().getAccess_token());
        params.put("data", msgParam);
        String postData = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData = objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果：" + wxRespCodeEntity.getErrcode());
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }

    /**
     * 模板消息通知：开团审核结果
     *
     * @param param
     * @param systemManager 系统参数
     * @param openid
     * @return
     */
    public static boolean sendProductCheckMsg(Map<String, String> param, SystemManager systemManager, String openid) {
        String templateId = KeyValueHelper.get("sendProductCheckMsg").trim();   //消息模板编号
        Map params = new HashMap();
        params.put("touser", openid);
        params.put("template_id", templateId);
        params.put("url", systemManager.getSystemSetting().getWww()+"/rest/front/user/toWx");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String, String> keyMap = new HashMap();
            keyMap.put("value", entry.getValue());
            keyMap.put("color", "#173177");
            msgParam.put(entry.getKey(), keyMap);
        }

        Map<String, String> getParams = new HashMap<String, String>();
        getParams.put("access_token", systemManager.getAccessToken().getAccess_token());
        params.put("data", msgParam);
        String postData = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData = objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果：" + wxRespCodeEntity.getErrcode());
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }

    /**
     * 模板消息通知：提现处理提醒
     *
     * @param param
     * @param systemManager 系统参数
     * @param openid
     * @return
     */
    public static boolean sendWalletCheckMsg(Map<String, String> param,SystemManager systemManager, String openid) {
        String templateId = KeyValueHelper.get("sendWalletCheckMsg").trim();   //消息模板编号
        Map params = new HashMap();
        params.put("touser", openid);
        params.put("template_id", templateId);
        params.put("url", systemManager.getSystemSetting().getWww()+"/rest/front/user/toWx");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String, String> keyMap = new HashMap();
            keyMap.put("value", entry.getValue());
            keyMap.put("color", "#173177");
            msgParam.put(entry.getKey(), keyMap);
        }

        Map<String, String> getParams = new HashMap<String, String>();
        getParams.put("access_token", systemManager.getAccessToken().getAccess_token());
        params.put("data", msgParam);
        String postData = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData = objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果：" + wxRespCodeEntity.getErrcode());
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }
}

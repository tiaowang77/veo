package shop.core.plugins.wxqrcode;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import shop.core.plugins.wxlogin.WxUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 微信创建带参数的二维码
 * @Author semstouch
 * @Date 2017/9/22
 **/
public class WxQrcodeUtil {
    protected static final Logger logger = LoggerFactory.getLogger(WxQrcodeUtil.class);

    // 临时二维码
    private final static String QR_STR_SCENE = "QR_STR_SCENE";
    // 永久二维码
    private final static String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";
    // 永久二维码(字符串)
    private final static String QR_LIMIT_STR_SCENE = "QR_LIMIT_STR_SCENE";


    /**
     * 创建临时带参数二维码
     * @param accessToken
     * @expireSeconds 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。
     * @param sceneId 场景Id
     * @return
     */
    public Map createTempQr(String accessToken, String expireSeconds, String sceneId) {
        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create" ;
        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",accessToken);

        Map<String,String> sceneIdMap = new HashMap<>();
        sceneIdMap.put("scene_id",sceneId);
        Map<String,Map<String,String>> sceneMap = new HashMap<>();
        sceneMap.put("scene", sceneIdMap);
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("expire_seconds", expireSeconds);
        params.put("action_name", QR_STR_SCENE);
        params.put("action_info", sceneMap);
        String postData="";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData= objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        Map resMap = WxUtil.sendRequest(url, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), Map.class);

        return resMap;
    }
    /**
     * 创建永久二维码(字符串)
     * @param sceneStr 场景id
     * @param accessToken
     * @return
     */
    public Map createForeverQr(String sceneStr,String accessToken ) {
        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create" ;
        // 参数：{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str": sceneStr}}}
        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",accessToken);

        Map<String,String> sceneIdMap = new HashMap<>();
        sceneIdMap.put("scene_id",sceneStr);
        Map<String,Map<String,String>> sceneMap = new HashMap<>();
        sceneMap.put("scene", sceneIdMap);
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("action_name", QR_LIMIT_STR_SCENE);
        params.put("action_info", sceneMap);
        String postData="";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData= objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        Map resMap = WxUtil.sendRequest(url, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), Map.class);
        return resMap;
    }



}

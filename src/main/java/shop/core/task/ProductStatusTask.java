package shop.core.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shop.services.front.product.ProductService;
import shop.services.front.product.bean.Product;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by wzl on 2017/12/21.
 */
@Component
public class ProductStatusTask implements Runnable {
    @Autowired
    private ProductService productService;

    @Override
    public void run() {
        Product product = new Product();
        List<Product> productList = productService.selectList(product);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        String nowTime = sdf.format(date);
        String oldTime = sdf.format(new Date(date.getTime() - 1 * 24 * 60 * 60 * 1000));
        for (Product productInfo : productList) {
            if (!"3".equals(productInfo.getCheckStatus()) && !"5".equals(productInfo.getCheckStatus())) {
                String startTime = productInfo.getStartDate();
                try{
                    Date date2=sdf2.parse(startTime);
                    startTime=sdf.format(date2);
                }catch(Exception e){}
                if (nowTime.compareTo(startTime) > 0 && oldTime.compareTo(startTime) < 0) {
                    productInfo.setCheckStatus("4");
                    productService.update(productInfo);
                } else if (oldTime.compareTo(startTime) > 0&&"n".equals(productInfo.getIsSelf())) {
                    productInfo.setCheckStatus("5");
                    productService.update(productInfo);
                }
            }
        }
    }
}

package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*用户地址
*/
public class Address extends PagerModel {
    //地址ID
    private String id;
    //用户ID
    private String userID;
    //姓名
    private String name;
    //电话
    private String phone;
    //是否默认（y 是  n 否）
    private String isdefault;
    //省份
    private String province;
    //城市
    private String city;
    //地区
    private String area;
    //地址
    private String address;
    //门牌号
    private String houseNumber;
    //更新时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }

    public String getUserID(){
        return this.userID;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public String getPhone(){
        return this.phone;
    }

    public void setIsdefault(String isdefault){
        this.isdefault = isdefault;
    }

    public String getIsdefault(){
        return this.isdefault;
    }

    public void setProvince(String province){
        this.province = province;
    }

    public String getProvince(){
        return this.province;
    }

    public void setCity(String city){
        this.city = city;
    }

    public String getCity(){
        return this.city;
    }

    public void setArea(String area){
        this.area = area;
    }

    public String getArea(){
        return this.area;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getAddress(){
        return this.address;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

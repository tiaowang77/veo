package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*用户购物车
*/
public class Cart extends PagerModel {
    //主键ID
    private String id;
    //用户ID
    private String userID;
    //商品ID
    private String productID;
    //商品数量
    private Integer cartNum;
    //规格ID
    private String specID;
    //创建时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public void setCartNum(Integer cartNum){
        this.cartNum = cartNum;
    }

    public Integer getCartNum(){
        return this.cartNum;
    }

    public void setSpecID(String specID){
        this.specID = specID;
    }

    public String getSpecID(){
        return this.specID;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

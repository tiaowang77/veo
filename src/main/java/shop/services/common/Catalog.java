package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*商品类别
*/
public class Catalog extends PagerModel {
    //id
    private String id;
    //名称
    private String name;
    //自定义排序
    private Integer order1;
    //是否在导航栏中显示（n 不显示  y 显示 ）
    private String showInNav;
    //商品目录图标
    private String icon;
    //创建时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setOrder1(Integer order1){
        this.order1 = order1;
    }

    public Integer getOrder1(){
        return this.order1;
    }

    public void setShowInNav(String showInNav){
        this.showInNav = showInNav;
    }

    public String getShowInNav(){
        return this.showInNav;
    }

    public void setIcon(String icon){
        this.icon = icon;
    }

    public String getIcon(){
        return this.icon;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*用户评论
*/
public class Comment extends PagerModel {
    //主键
    private String id;
    //用户ID
    private String userID;
    //订单详情ID
    private String oID;
    //评论内容
    private String content;
    //图片（多个用逗号隔开）
    private String picture;
    //是否匿名（n否、y是）
    private String isHide;
    //评价等级(好评good、中评common、差评bad)
    private String rank;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;


    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }

    public String getUserID(){
        return this.userID;
    }

    public void setOID(String oID){
        this.oID = oID;
    }

    public String getOID(){
        return this.oID;
    }

    public void setContent(String content){
        this.content = content;
    }

    public String getContent(){
        return this.content;
    }

    public void setPicture(String picture){
        this.picture = picture;
    }

    public String getPicture(){
        return this.picture;
    }

    public void setIsHide(String isHide){
        this.isHide = isHide;
    }

    public String getIsHide(){
        return this.isHide;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}

package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*首页图片
*/
public class IndexImg extends PagerModel {
    //id
    private String id;
    //图片
    private String picture;
    //排序
    private Integer order1;
    //广告链接
    private String link;
    //创建时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;


    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setPicture(String picture){
        this.picture = picture;
    }

    public String getPicture(){
        return this.picture;
    }

    public void setOrder1(Integer order1){
        this.order1 = order1;
    }

    public Integer getOrder1(){
        return this.order1;
    }

    public void setLink(String link){
        this.link = link;
    }

    public String getLink(){
        return this.link;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

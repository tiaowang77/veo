package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*订单
*/
public class Order extends PagerModel {
    //主键
    private String id;
    //用户ID
    private String userID;
    //上级用户ID
    private String supUserID;
    //商家ID
    private String merchantID;
    //订单状态(init:待付款，pass:待发货,send:待收货，finish:已完成，cancel:已取消)
    private String status;
    //总价
    private BigDecimal ptotal;
    //抵扣金额
    private BigDecimal deduction;
    //上级返利金额
    private BigDecimal rebate;
    //收货人姓名
    private String addressName;
    //收货人手机号码
    private String addressPhone;
    //收货人地区信息
    private String addressArea;
    //收货人详细地址
    private String addressDetail;
    //快递公司
    private String expressCompany;
    //快递单号
    private String expressNum;
    //发货人
    private String consigner;
    //备注
    private String remark;
    //是否团购(y是、n否)
    private String isGroup;
    //参与团购订单ID
    private String groupOrderID;
    //参团人数
    private Integer groupNumber;
    //是否预定(y是、n否)
    private String isReserve;
    //创建时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新次数
    private String updateTime;
    //更新者
    private Integer updateUser;


    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }

    public String getUserID(){
        return this.userID;
    }

    public String getSupUserID() {
        return supUserID;
    }

    public void setSupUserID(String supUserID) {
        this.supUserID = supUserID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setPtotal(BigDecimal ptotal){
        this.ptotal = ptotal;
    }

    public BigDecimal getPtotal(){
        return this.ptotal;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public BigDecimal getDeduction() {
        return deduction;
    }

    public void setDeduction(BigDecimal deduction) {
        this.deduction = deduction;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
    }

    public String getConsigner() {
        return consigner;
    }

    public void setConsigner(String consigner) {
        this.consigner = consigner;
    }

    public String getExpressNum() {
        return expressNum;
    }

    public void setExpressNum(String expressNum) {
        this.expressNum = expressNum;
    }

    public String getAddressPhone() {
        return addressPhone;
    }

    public void setAddressPhone(String addressPhone) {
        this.addressPhone = addressPhone;
    }

    public String getAddressArea() {
        return addressArea;
    }

    public void setAddressArea(String addressArea) {
        this.addressArea = addressArea;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    public String getGroupOrderID() {
        return groupOrderID;
    }

    public void setGroupOrderID(String groupOrderID) {
        this.groupOrderID = groupOrderID;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getIsReserve() {
        return isReserve;
    }

    public void setIsReserve(String isReserve) {
        this.isReserve = isReserve;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

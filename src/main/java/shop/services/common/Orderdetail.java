package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*订单详情
*/
public class Orderdetail extends PagerModel {
    //订单详情id
    private String id;
    //订单id
    private String orderID;
    //产品id
    private String productID;
    //价格
    private BigDecimal price;
    //数量
    private Integer number;
    //总价
    private BigDecimal dtotal;
    //规格
    private String specInfo;
    //评论状态(y已评论、n未评论)
    private String isComment;
    //创建时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public void setPrice(BigDecimal price){
        this.price = price;
    }

    public BigDecimal getPrice(){
        return this.price;
    }

    public void setNumber(Integer number){
        this.number = number;
    }

    public Integer getNumber(){
        return this.number;
    }

    public BigDecimal getDtotal() {
        return dtotal;
    }

    public void setDtotal(BigDecimal dtotal) {
        this.dtotal = dtotal;
    }

    public void setSpecInfo(String specInfo){
        this.specInfo = specInfo;
    }

    public String getSpecInfo(){
        return this.specInfo;
    }

    public String getIsComment() {
        return isComment;
    }

    public void setIsComment(String isComment) {
        this.isComment = isComment;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

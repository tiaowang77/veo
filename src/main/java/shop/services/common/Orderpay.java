package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*订单支付
*/
public class Orderpay extends PagerModel {
    //支付id
    private String id;
    //订单id
    private String orderID;
    //商户订单号
    private String outTradeNo;
    //微信预支付编号
    private String prepayId;
    //商品详情
    private String productBody;
    //商品标题
    private String productTitle;
    //
    private String transactionId;
    //支付状态(0 待支付,1 已支付,2 已完成)
    private String payStatus;
    //支付类型
    private String payType;
    //支付金额
    private BigDecimal payAmount;
    //支付时间
    private String payTime;
    //备注
    private String remark;
    //创建时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setOrderID(String orderID){
        this.orderID = orderID;
    }

    public String getOrderID(){
        return this.orderID;
    }

    public void setOutTradeNo(String outTradeNo){
        this.outTradeNo = outTradeNo;
    }

    public String getOutTradeNo(){
        return this.outTradeNo;
    }

    public void setPrepayId(String prepayId){
        this.prepayId = prepayId;
    }

    public String getPrepayId(){
        return this.prepayId;
    }

    public void setProductBody(String productBody){
        this.productBody = productBody;
    }

    public String getProductBody(){
        return this.productBody;
    }

    public void setProductTitle(String productTitle){
        this.productTitle = productTitle;
    }

    public String getProductTitle(){
        return this.productTitle;
    }

    public void setTransactionId(String transactionId){
        this.transactionId = transactionId;
    }

    public String getTransactionId(){
        return this.transactionId;
    }

    public void setPayStatus(String payStatus){
        this.payStatus = payStatus;
    }

    public String getPayStatus(){
        return this.payStatus;
    }

    public void setPayType(String payType){
        this.payType = payType;
    }

    public String getPayType(){
        return this.payType;
    }

    public void setPayAmount(BigDecimal payAmount){
        this.payAmount = payAmount;
    }

    public BigDecimal getPayAmount(){
        return this.payAmount;
    }

    public void setPayTime(String payTime){
        this.payTime = payTime;
    }

    public String getPayTime(){
        return this.payTime;
    }

    public void setRemark(String remark){
        this.remark = remark;
    }

    public String getRemark(){
        return this.remark;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

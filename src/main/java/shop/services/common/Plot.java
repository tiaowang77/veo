package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*小区信息
*/
public class Plot extends PagerModel {
    //主键
    private String id;
    //名称
    private String name;
    //省份
    private String province;
    //城市
    private String city;
    //地区
    private String area;
    //详细地址
    private String address;
    //经度
    private String lng;
    //纬度
    private String lat;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setProvince(String province){
        this.province = province;
    }

    public String getProvince(){
        return this.province;
    }

    public void setCity(String city){
        this.city = city;
    }

    public String getCity(){
        return this.city;
    }

    public void setArea(String area){
        this.area = area;
    }

    public String getArea(){
        return this.area;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getAddress(){
        return this.address;
    }

    public void setLng(String lng){
        this.lng = lng;
    }

    public String getLng(){
        return this.lng;
    }

    public void setLat(String lat){
        this.lat = lat;
    }

    public String getLat(){
        return this.lat;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}

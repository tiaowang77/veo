package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*商品
*/
public class Product extends PagerModel {
    //商品编号
    private String id;
    //商品名称
    private String name;
    //商品简介
    private String introduce;
    //商品详情
    private String productDetail;
    //商品主图
    private String picture;
    //商品状态（0上架、1下架）
    private Integer status;
    //商品图片
    private String images;
    //类别ID
    private String catalogID;
    //商家ID
    private String merchantID;
    //是否自营
    private String isSelf;
    //商品销售量
    private Integer sellcount;
    //开团时间
    private String startDate;
    //送货时间
    private String sendDate;
    //地区
    private String area;
    //小区
    private String garden;
    //自提点
    private String address;
    //联系人
    private String linkMan;
    //联系电话
    private String linkPhone;
    //审核状态
    private String checkStatus;
    //审核理由
    private String reason;
    //海报
    private String background;
    //第一单返利
    private BigDecimal firstPercent;
    //第二单返利
    private BigDecimal secondPercent;
    //第三单返利
    private BigDecimal thirdPercent;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setIntroduce(String introduce){
        this.introduce = introduce;
    }

    public String getIntroduce(){
        return this.introduce;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    public void setPicture(String picture){
        this.picture = picture;
    }

    public String getPicture(){
        return this.picture;
    }

    public void setStatus(Integer status){
        this.status = status;
    }

    public Integer getStatus(){
        return this.status;
    }

    public void setImages(String images){
        this.images = images;
    }

    public String getImages(){
        return this.images;
    }

    public void setCatalogID(String catalogID){
        this.catalogID = catalogID;
    }

    public String getCatalogID(){
        return this.catalogID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getIsSelf() {
        return isSelf;
    }

    public void setIsSelf(String isSelf) {
        this.isSelf = isSelf;
    }

    public void setSellcount(Integer sellcount){
        this.sellcount = sellcount;
    }

    public Integer getSellcount(){
        return this.sellcount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getGarden() {
        return garden;
    }

    public void setGarden(String garden) {
        this.garden = garden;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public BigDecimal getFirstPercent() {
        return firstPercent;
    }

    public void setFirstPercent(BigDecimal firstPercent) {
        this.firstPercent = firstPercent;
    }

    public BigDecimal getSecondPercent() {
        return secondPercent;
    }

    public void setSecondPercent(BigDecimal secondPercent) {
        this.secondPercent = secondPercent;
    }

    public BigDecimal getThirdPercent() {
        return thirdPercent;
    }

    public void setThirdPercent(BigDecimal thirdPercent) {
        this.thirdPercent = thirdPercent;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}

package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*用户商品返利
*/
public class Productuser extends PagerModel {
    //地址ID
    private String id;
    //用户ID
    private String userID;
    //上级ID
    private String pid;
    //商品ID
    private String productID;
    //返利等级
    private String rank;
    //更新时间
    private String createTime;
    //创建者
    private Integer createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }

    public String getUserID(){
        return this.userID;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public void setProductID(String productID){
        this.productID = productID;
    }

    public String getProductID(){
        return this.productID;
    }

    public void setRank(String rank){
        this.rank = rank;
    }

    public String getRank(){
        return this.rank;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(Integer createUser){
        this.createUser = createUser;
    }

    public Integer getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser){
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser(){
        return this.updateUser;
    }

}

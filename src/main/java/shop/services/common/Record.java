package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*用户交易记录
*/
public class Record extends PagerModel {
    //主键
    private String id;
    //用户ID
    private String userID;
    //交易类型（0提现、1返利、2用户购买商品、3、抵扣）
    private String transType;
    //交易类型ID
    private String transID;
    //金额
    private BigDecimal money;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }

    public String getUserID(){
        return this.userID;
    }

    public void setTransType(String transType){
        this.transType = transType;
    }

    public String getTransType(){
        return this.transType;
    }

    public void setTransID(String transID){
        this.transID = transID;
    }

    public String getTransID(){
        return this.transID;
    }

    public void setMoney(BigDecimal money){
        this.money = money;
    }

    public BigDecimal getMoney(){
        return this.money;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}

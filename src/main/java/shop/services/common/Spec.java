package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*商品规格
*/
public class Spec extends PagerModel {
    //商品规格编号
    private String id;
    //商品编号
    private String productID;
    //商品组合(用,隔开)
    private String specCombination;
    //规格进价
    private BigDecimal specPurchasePrice;
    //规格现价
    private BigDecimal specNowPrice;
    //团购价
    private BigDecimal specGroupPrice;
    //规格库存
    private Integer specStock;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setProductID(String productID){
        this.productID = productID;
    }

    public String getProductID(){
        return this.productID;
    }

    public void setSpecCombination(String specCombination){
        this.specCombination = specCombination;
    }

    public String getSpecCombination(){
        return this.specCombination;
    }

    public void setSpecPurchasePrice(BigDecimal specPurchasePrice){
        this.specPurchasePrice = specPurchasePrice;
    }

    public BigDecimal getSpecPurchasePrice(){
        return this.specPurchasePrice;
    }

    public void setSpecStock(Integer specStock){
        this.specStock = specStock;
    }

    public Integer getSpecStock(){
        return this.specStock;
    }

    public void setSpecNowPrice(BigDecimal specNowPrice){
        this.specNowPrice = specNowPrice;
    }

    public BigDecimal getSpecNowPrice(){
        return this.specNowPrice;
    }

    public BigDecimal getSpecGroupPrice() {
        return specGroupPrice;
    }

    public void setSpecGroupPrice(BigDecimal specGroupPrice) {
        this.specGroupPrice = specGroupPrice;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}

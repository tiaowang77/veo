package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*用户提现申请
*/
public class Wallet extends PagerModel {
    //ID
    private String id;
    //收款人
    private String payee;
    //收款账户
    private String payAccount;
    //账户类型（1支付宝、2微信、3银行卡 ）
    private String accountType;
    //申请人
    private String applicant;
    //提现金额
    private BigDecimal money;
    //状态(1待审核、2已通过、3不通过）
    private String status;
    //申请人姓名
    private String applicantName;
    //收款人手机
    private String payeePhone;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setPayee(String payee){
        this.payee = payee;
    }

    public String getPayee(){
        return this.payee;
    }

    public void setPayAccount(String payAccount){
        this.payAccount = payAccount;
    }

    public String getPayAccount(){
        return this.payAccount;
    }

    public void setAccountType(String accountType){
        this.accountType = accountType;
    }

    public String getAccountType(){
        return this.accountType;
    }

    public void setApplicant(String applicant){
        this.applicant = applicant;
    }

    public String getApplicant(){
        return this.applicant;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setApplicantName(String applicantName){
        this.applicantName = applicantName;
    }

    public String getApplicantName(){
        return this.applicantName;
    }

    public void setPayeePhone(String payeePhone){
        this.payeePhone = payeePhone;
    }

    public String getPayeePhone(){
        return this.payeePhone;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}

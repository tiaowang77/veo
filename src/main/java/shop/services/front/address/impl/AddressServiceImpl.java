/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.address.impl;

import shop.core.ServicesManager;
import shop.services.front.address.AddressService;
import shop.services.front.address.bean.Address;
import shop.services.front.address.dao.AddressDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：AddressServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:04        
 * @版本号：1.0
 * @描述：     
 */
@Service("addressServiceFront")
public class AddressServiceImpl extends ServicesManager<Address, AddressDao> implements
AddressService {
    @Resource(name = "addressDaoFront")
    @Override
    public void setDao(AddressDao addressDao) {
    this.dao = addressDao;
    }
}


/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.brokerage.bean;
import java.io.Serializable;

/**
* @类名称：Brokerage前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:33:32 
* @版本号：1.0
* @描述：
*/
public class Brokerage extends shop.services.common.Brokerage implements Serializable{
    private static final long serialVersionUID = 1L;
}

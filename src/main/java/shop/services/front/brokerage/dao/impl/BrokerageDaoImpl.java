/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.brokerage.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.brokerage.bean.Brokerage;
import shop.services.front.brokerage.dao.BrokerageDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：BrokerageDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:33:32  
 * @版本号：1.0
 * @描述：
 */
@Repository("brokerageDaoFront")
public class BrokerageDaoImpl  implements BrokerageDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Brokerage e) {
        return dao.selectPageList("front.brokerage.selectPageList","front.brokerage.selectPageCount", e);
    }

    public List selectList(Brokerage e) {
        return dao.selectList("front.brokerage.selectList", e);
    }

    public Brokerage selectOne(Brokerage e) {
        return (Brokerage) dao.selectOne("front.brokerage.selectOne", e);
    }

    public int delete(Brokerage e) {
        return dao.delete("front.brokerage.delete", e);
    }

    public int update(Brokerage e) {
        return dao.update("front.brokerage.update", e);
    }

    public int deletes(String[] ids) {
        Brokerage e = new Brokerage();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Brokerage e) {
        return dao.insert("front.brokerage.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.brokerage.deleteById", id);
    }

    @Override
    public Brokerage selectById(String id) {
        return (Brokerage) dao.selectOne("front.brokerage.selectById", id);
    }

}


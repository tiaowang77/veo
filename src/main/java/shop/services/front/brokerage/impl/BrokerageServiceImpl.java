/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.brokerage.impl;

import shop.core.ServicesManager;
import shop.services.front.brokerage.BrokerageService;
import shop.services.front.brokerage.bean.Brokerage;
import shop.services.front.brokerage.dao.BrokerageDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：BrokerageServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:32        
 * @版本号：1.0
 * @描述：     
 */
@Service("brokerageServiceFront")
public class BrokerageServiceImpl extends ServicesManager<Brokerage, BrokerageDao> implements
BrokerageService {
    @Resource(name = "brokerageDaoFront")
    @Override
    public void setDao(BrokerageDao brokerageDao) {
    this.dao = brokerageDao;
    }
}


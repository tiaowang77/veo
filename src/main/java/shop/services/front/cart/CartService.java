/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.cart;

import shop.core.Services;
import shop.services.front.cart.bean.Cart;

/**   
 * @类名称：CartService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:52       
 * @版本号：1.0
 * @描述：     
 */
public interface CartService extends Services<Cart>{

}


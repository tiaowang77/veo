/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.cart.bean;
import java.io.Serializable;

/**
* @类名称：Cart前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:33:52 
* @版本号：1.0
* @描述：
*/
public class Cart extends shop.services.common.Cart implements Serializable{
    private static final long serialVersionUID = 1L;
    //商品主图（查询用）
    private String picture;
    //商品名称（查询用）
    private String productName;
    //商品规格名称（查询用）
    private String specName;
    //商品价格（查询用）
    private String price;
    //商品库存（查询用）
    private String stock;


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}

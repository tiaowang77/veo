/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.cart.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.cart.bean.Cart;
import shop.services.front.cart.dao.CartDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：CartDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:33:52  
 * @版本号：1.0
 * @描述：
 */
@Repository("cartDaoFront")
public class CartDaoImpl  implements CartDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Cart e) {
        return dao.selectPageList("front.cart.selectPageList","front.cart.selectPageCount", e);
    }

    public List selectList(Cart e) {
        return dao.selectList("front.cart.selectList", e);
    }

    public Cart selectOne(Cart e) {
        return (Cart) dao.selectOne("front.cart.selectOne", e);
    }

    public int delete(Cart e) {
        return dao.delete("front.cart.delete", e);
    }

    public int update(Cart e) {
        return dao.update("front.cart.update", e);
    }

    public int deletes(String[] ids) {
        Cart e = new Cart();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Cart e) {
        return dao.insert("front.cart.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.cart.deleteById", id);
    }

    @Override
    public Cart selectById(String id) {
        return (Cart) dao.selectOne("front.cart.selectById", id);
    }

}


/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.catalog.bean;
import java.io.Serializable;

/**
* @类名称：Catalog前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:34:37 
* @版本号：1.0
* @描述：
*/
public class Catalog extends shop.services.common.Catalog implements Serializable{
    private static final long serialVersionUID = 1L;
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.catalog.dao;

import shop.core.DaoManager;
import shop.services.front.catalog.bean.Catalog;
import org.springframework.stereotype.Repository;

/**   
 * @类名称：CatalogDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:34:37        
 * @版本号：1.0
 * @描述：     
 */
public interface CatalogDao extends DaoManager<Catalog> {

}


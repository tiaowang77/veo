/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.catalog.impl;

import shop.core.ServicesManager;
import shop.services.front.catalog.CatalogService;
import shop.services.front.catalog.bean.Catalog;
import shop.services.front.catalog.dao.CatalogDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：CatalogServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:34:37        
 * @版本号：1.0
 * @描述：     
 */
@Service("catalogServiceFront")
public class CatalogServiceImpl extends ServicesManager<Catalog, CatalogDao> implements
CatalogService {
    @Resource(name = "catalogDaoFront")
    @Override
    public void setDao(CatalogDao catalogDao) {
    this.dao = catalogDao;
    }
}


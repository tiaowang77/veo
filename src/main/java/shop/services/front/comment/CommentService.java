/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.comment;

import shop.core.Services;
import shop.services.front.comment.bean.Comment;

/**   
 * @类名称：CommentService      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:05:09       
 * @版本号：1.0
 * @描述：     
 */
public interface CommentService extends Services<Comment> {

    Comment selectCommentDetail(Comment comment);
}


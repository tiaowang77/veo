/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.comment.bean;
import java.io.Serializable;

/**
* @类名称：Comment前台对象类
* @创建人：Ltz
* @创建时间：2017-10-13 下午15:05:09 
* @版本号：1.0
* @描述：
*/
public class Comment extends shop.services.common.Comment implements Serializable{
    private static final long serialVersionUID = 1L;
    //用户名称
    private String nickName;
    //用户头像
    private String icon;
    //商品名称（查询用）
    private String productName;
    //商品主图（查询用）
    private String productPicture;
    //商品图片（查询用）
    private String productImages;
    //商品规格（查询用）
    private String productSpecInfo;
    //商品数量（查询用）
    private String productNumber;
    //商品ID（查询用）
    private String productID;


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPicture() {
        return productPicture;
    }

    public void setProductPicture(String productPicture) {
        this.productPicture = productPicture;
    }

    public String getProductImages() {
        return productImages;
    }

    public void setProductImages(String productImages) {
        this.productImages = productImages;
    }

    public String getProductSpecInfo() {
        return productSpecInfo;
    }

    public void setProductSpecInfo(String productSpecInfo) {
        this.productSpecInfo = productSpecInfo;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }
}

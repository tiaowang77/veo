/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.comment.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.comment.bean.Comment;
import shop.services.front.comment.dao.CommentDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：CommentDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-10-13 下午15:05:09  
 * @版本号：1.0
 * @描述：
 */
@Repository("commentDaoFront")
public class CommentDaoImpl  implements CommentDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Comment e) {
        return dao.selectPageList("front.comment.selectPageList","front.comment.selectPageCount", e);
    }

    public List selectList(Comment e) {
        return dao.selectList("front.comment.selectList", e);
    }

    public Comment selectOne(Comment e) {
        return (Comment) dao.selectOne("front.comment.selectOne", e);
    }

    public int delete(Comment e) {
        return dao.delete("front.comment.delete", e);
    }

    public int update(Comment e) {
        return dao.update("front.comment.update", e);
    }

    public int deletes(String[] ids) {
        Comment e = new Comment();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Comment e) {
        return dao.insert("front.comment.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.comment.deleteById", id);
    }

    @Override
    public Comment selectById(String id) {
        return (Comment) dao.selectOne("front.comment.selectById", id);
    }

    @Override
    public Comment selectCommentDetail(Comment comment) {
        return (Comment) dao.selectOne("front.comment.selectCommentDetail", comment);
    }
}


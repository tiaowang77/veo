/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.favorite.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.favorite.bean.Favorite;
import shop.services.front.favorite.dao.FavoriteDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：FavoriteDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-10-13 下午15:06:28  
 * @版本号：1.0
 * @描述：
 */
@Repository("favoriteDaoFront")
public class FavoriteDaoImpl  implements FavoriteDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Favorite e) {
        return dao.selectPageList("front.favorite.selectPageList","front.favorite.selectPageCount", e);
    }

    public List selectList(Favorite e) {
        return dao.selectList("front.favorite.selectList", e);
    }

    public Favorite selectOne(Favorite e) {
        return (Favorite) dao.selectOne("front.favorite.selectOne", e);
    }

    public int delete(Favorite e) {
        return dao.delete("front.favorite.delete", e);
    }

    public int update(Favorite e) {
        return dao.update("front.favorite.update", e);
    }

    public int deletes(String[] ids) {
        Favorite e = new Favorite();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Favorite e) {
        return dao.insert("front.favorite.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.favorite.deleteById", id);
    }

    @Override
    public Favorite selectById(String id) {
        return (Favorite) dao.selectOne("front.favorite.selectById", id);
    }

}


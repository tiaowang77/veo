/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.favorite.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.front.favorite.FavoriteService;
import shop.services.front.favorite.bean.Favorite;
import shop.services.front.favorite.dao.FavoriteDao;

import javax.annotation.Resource;


/**   
 * @类名称：FavoriteServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:06:28        
 * @版本号：1.0
 * @描述：     
 */
@Service("favoriteServiceFront")
public class FavoriteServiceImpl extends ServicesManager<Favorite, FavoriteDao> implements
FavoriteService {
    @Resource(name = "favoriteDaoFront")
    @Override
    public void setDao(FavoriteDao favoriteDao) {
    this.dao = favoriteDao;
    }
}


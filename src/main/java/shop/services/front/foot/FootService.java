/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.foot;

import shop.core.Services;
import shop.services.front.foot.bean.Foot;

/**   
 * @类名称：FootService      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:07:59       
 * @版本号：1.0
 * @描述：     
 */
public interface FootService extends Services<Foot> {

}


/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.foot.bean;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* @类名称：Foot前台对象类
* @创建人：Ltz
* @创建时间：2017-10-13 下午15:07:59 
* @版本号：1.0
* @描述：
*/
public class Foot extends shop.services.common.Foot implements Serializable{
    private static final long serialVersionUID = 1L;
    //商品名称(查询用)
    private String productName;
    //商品介绍(查询用)
    private String productIntroduce;
    //商品主图(查询用)
    private String productPicture;
    //商品详情图片(查询用)
    private String productImages;
    //商品价格(查询用)
    private BigDecimal productPrice;
    //商品销量(查询用)
    private Integer productSellCount;


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductIntroduce() {
        return productIntroduce;
    }

    public void setProductIntroduce(String productIntroduce) {
        this.productIntroduce = productIntroduce;
    }

    public String getProductPicture() {
        return productPicture;
    }

    public void setProductPicture(String productPicture) {
        this.productPicture = productPicture;
    }

    public String getProductImages() {
        return productImages;
    }

    public void setProductImages(String productImages) {
        this.productImages = productImages;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductSellCount() {
        return productSellCount;
    }

    public void setProductSellCount(Integer productSellCount) {
        this.productSellCount = productSellCount;
    }
}

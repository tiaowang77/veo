/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.foot.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.front.foot.FootService;
import shop.services.front.foot.bean.Foot;
import shop.services.front.foot.dao.FootDao;

import javax.annotation.Resource;


/**   
 * @类名称：FootServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:07:59        
 * @版本号：1.0
 * @描述：     
 */
@Service("footServiceFront")
public class FootServiceImpl extends ServicesManager<Foot, FootDao> implements
FootService {
    @Resource(name = "footDaoFront")
    @Override
    public void setDao(FootDao footDao) {
    this.dao = footDao;
    }
}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.indexImg.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.indexImg.bean.IndexImg;
import shop.services.front.indexImg.dao.IndexImgDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：IndexDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午16:04:29  
 * @版本号：1.0
 * @描述：
 */
@Repository("indexDaoFront")
public class IndexImgDaoImpl implements IndexImgDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(IndexImg e) {
        return dao.selectPageList("front.indexImg.selectPageList","front.indexImg.selectPageCount", e);
    }

    public List selectList(IndexImg e) {
        return dao.selectList("front.indexImg.selectList", e);
    }

    public IndexImg selectOne(IndexImg e) {
        return (IndexImg) dao.selectOne("front.indexImg.selectOne", e);
    }

    public int delete(IndexImg e) {
        return dao.delete("front.indexImg.delete", e);
    }

    public int update(IndexImg e) {
        return dao.update("front.indexImg.update", e);
    }

    public int deletes(String[] ids) {
        IndexImg e = new IndexImg();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(IndexImg e) {
        return dao.insert("front.indexImg.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.indexImg.deleteById", id);
    }

    @Override
    public IndexImg selectById(String id) {
        return (IndexImg) dao.selectOne("front.indexImg.selectById", id);
    }

}


/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.indexImg.impl;

import shop.core.ServicesManager;
import shop.services.front.indexImg.IndexService;
import shop.services.front.indexImg.bean.IndexImg;
import shop.services.front.indexImg.dao.IndexImgDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：IndexServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午16:04:29        
 * @版本号：1.0
 * @描述：     
 */
@Service("indexServiceFront")
public class IndexImgServiceImpl extends ServicesManager<IndexImg, IndexImgDao> implements
IndexService {
    @Resource(name = "indexDaoFront")
    @Override
    public void setDao(IndexImgDao indexImgDao) {
    this.dao = indexImgDao;
    }
}


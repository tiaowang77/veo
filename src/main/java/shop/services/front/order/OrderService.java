/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.order;

import shop.core.Services;
import shop.services.front.order.bean.Order;

import java.util.List;

/**   
 * @类名称：OrderService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:48:44       
 * @版本号：1.0
 * @描述：     
 */
public interface OrderService extends Services<Order>{

    List<Order> selectGroupOrder(Order order);

    List<Order> selectSendOrderByDay(Order order);
}


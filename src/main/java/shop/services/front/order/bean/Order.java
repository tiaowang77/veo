/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.order.bean;
import shop.services.front.orderdetail.bean.Orderdetail;

import java.io.Serializable;
import java.util.List;

/**
* @类名称：Order前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:48:44 
* @版本号：1.0
* @描述：
*/
public class Order extends shop.services.common.Order implements Serializable{
    private static final long serialVersionUID = 1L;
    //订单详情列表（查询用）
    private List<Orderdetail> orderdetails;
    //规格组合数组（查询用）
    private String specIDs;
    //商品ID数组（插入订单使用）
    private String productIDs;
    //商品数量数组（插入订单使用）
    private String productAmount;
    //用户昵称(查询用)
    private String userName;
    //用户头像(查询用)
    private String userIcon;
    //日期(一键发货页面查询用)
    private String dayTime;
    //商品审核状态（查询用）
    private String productCheckStatus;


    public List<Orderdetail> getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(List<Orderdetail> orderdetails) {
        this.orderdetails = orderdetails;
    }

    public String getSpecIDs() {
        return specIDs;
    }

    public void setSpecIDs(String specIDs) {
        this.specIDs = specIDs;
    }

    public String getProductIDs() {
        return productIDs;
    }

    public void setProductIDs(String productIDs) {
        this.productIDs = productIDs;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getDayTime() {
        return dayTime;
    }

    public void setDayTime(String dayTime) {
        this.dayTime = dayTime;
    }

    public String getProductCheckStatus() {
        return productCheckStatus;
    }

    public void setProductCheckStatus(String productCheckStatus) {
        this.productCheckStatus = productCheckStatus;
    }
}

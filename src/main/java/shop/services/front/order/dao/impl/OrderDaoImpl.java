/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.order.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.order.bean.Order;
import shop.services.front.order.dao.OrderDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：OrderDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:48:44  
 * @版本号：1.0
 * @描述：
 */
@Repository("orderDaoFront")
public class OrderDaoImpl  implements OrderDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Order e) {
        return dao.selectPageList("front.order.selectPageList","front.order.selectPageCount", e);
    }

    public List selectList(Order e) {
        return dao.selectList("front.order.selectList", e);
    }

    public Order selectOne(Order e) {
        return (Order) dao.selectOne("front.order.selectOne", e);
    }

    public int delete(Order e) {
        return dao.delete("front.order.delete", e);
    }

    public int update(Order e) {
        return dao.update("front.order.update", e);
    }

    public int deletes(String[] ids) {
        Order e = new Order();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Order e) {
        return dao.insert("front.order.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.order.deleteById", id);
    }

    @Override
    public Order selectById(String id) {
        return (Order) dao.selectOne("front.order.selectById", id);
    }

    @Override
    public List<Order> selectGroupOrder(Order order) {
        return dao.selectList("front.order.selectGroupOrder", order);
    }

    @Override
    public List<Order> selectSendOrderByDay(Order order) {
        return dao.selectList("front.order.selectSendOrderByDay", order);
    }
}


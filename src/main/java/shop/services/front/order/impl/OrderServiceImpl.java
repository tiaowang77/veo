/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.order.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.front.cart.bean.Cart;
import shop.services.front.cart.dao.CartDao;
import shop.services.front.order.OrderService;
import shop.services.front.order.bean.Order;
import shop.services.front.order.dao.OrderDao;
import shop.services.front.orderdetail.bean.Orderdetail;
import shop.services.front.orderdetail.dao.OrderdetailDao;
import shop.services.front.product.bean.Product;
import shop.services.front.product.dao.ProductDao;
import shop.services.front.spec.bean.Spec;
import shop.services.front.spec.dao.impl.SpecDaoImpl;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**   
 * @类名称：OrderServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:48:44        
 * @版本号：1.0
 * @描述：     
 */
@Service("orderServiceFront")
public class OrderServiceImpl extends ServicesManager<Order, OrderDao> implements OrderService {


    @Resource(name = "orderDaoFront")
    @Override
    public void setDao(OrderDao orderDao) {this.dao = orderDao;}
    @Resource(name = "productDaoFront")
    private ProductDao productDao;
    @Resource(name = "specDaoFront")
    private SpecDaoImpl specDao;
    @Resource(name = "cartDaoFront")
    private CartDao cartDao;
    @Resource(name = "orderdetailDaoFront")
    private OrderdetailDao orderdetailDao;


    @Override
    public int insert(Order order) {
        dao.insert(order);
        String orderID = order.getId();
        String[] productIDs = order.getProductIDs().split(",");
        String[] specIDs = order.getSpecIDs().split(";");
        String[] productAmount = order.getProductAmount().split(",");
        for (int i = 0; i < productIDs.length; i++) {
            //获取规格信息
            Spec spec = new Spec();
            spec.setProductID(productIDs[i]);
            spec.setSpecCombination(specIDs[i]);
            spec = specDao.selectOne(spec);
            if(order.getDeduction().equals(order.getPtotal())){//余额购买的时候更新商品库存及销量
                spec.setSpecStock(spec.getSpecStock() - Integer.parseInt(productAmount[i]));
                specDao.update(spec);
                Product product=new Product();
                product.setId(spec.getProductID());
                product=productDao.selectOne(product);
                product.setSellcount(product.getSellcount()-Integer.parseInt(productAmount[i]));
                productDao.update(product);
            }
            //创建订单详情项
            Orderdetail orderdetail = new Orderdetail();
            orderdetail.setOrderID(orderID);
            orderdetail.setProductID(productIDs[i]);
            if("y".equals(order.getIsGroup())){
                orderdetail.setPrice(spec.getSpecGroupPrice());
            }else{
                orderdetail.setPrice(spec.getSpecNowPrice());
            }
            orderdetail.setNumber(Integer.valueOf(productAmount[i]));
            orderdetail.setSpecInfo(spec.getSpecCombination());
            //计算订单详情项价格
            BigDecimal productNumber = new BigDecimal(productAmount[i]);
            productNumber.setScale(2, BigDecimal.ROUND_HALF_UP);//四舍五入保留两位小数
            BigDecimal total = spec.getSpecNowPrice().multiply(productNumber);//相乘
            orderdetail.setDtotal(total);
            orderdetailDao.insert(orderdetail);//插入订单详情表
		    //购物车删除已购买项
            Cart cart = new Cart();
            cart.setUserID(order.getUserID());
            cart.setProductID(productIDs[i]);
            if (cartDao.selectList(cart).size() != 0) {
                cartDao.delete(cart);
            }

        }
        return Integer.parseInt(orderID);
    }

    @Override
    public List<Order> selectGroupOrder(Order order) {
        return dao.selectGroupOrder(order);
    }

    @Override
    public List<Order> selectSendOrderByDay(Order order) {
        return dao.selectSendOrderByDay(order);
    }
}


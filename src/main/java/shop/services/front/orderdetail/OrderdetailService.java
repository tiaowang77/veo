/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.orderdetail;

import shop.core.Services;
import shop.services.front.orderdetail.bean.Orderdetail;

import java.util.List;

/**   
 * @类名称：OrderdetailService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:49:46       
 * @版本号：1.0
 * @描述：     
 */
public interface OrderdetailService extends Services<Orderdetail> {

    List<Orderdetail> selectOrderDetailList(Orderdetail orderdetail);

    List<Orderdetail> selectCommentList(Orderdetail orderdetail);

    List<Orderdetail> selectHistoryOrderdetail(Orderdetail orderdetail);
}


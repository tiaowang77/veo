/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.orderdetail.bean;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* @类名称：Orderdetail前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:49:46 
* @版本号：1.0
* @描述：
*/
public class Orderdetail extends shop.services.common.Orderdetail implements Serializable{
    private static final long serialVersionUID = 1L;
    // 商品主图（查询用）
    private String picture;
    // 商品图片（查询用）
    private String images;
    //商品名称（查询用）
    private String productName;
    //商品介绍（查询用）
    private String introduce;
    //订单状态（查询用）
    private String orderStatus;
    //地址ID（查询用）
    private String addressID;
    //用户ID（查询用）
    private String userID;
    //订单总价（查询用）
    private BigDecimal ptotal;


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getAddressID() {
        return addressID;
    }

    public void setAddressID(String addressID) {
        this.addressID = addressID;
    }

    public BigDecimal getPtotal() {
        return ptotal;
    }

    public void setPtotal(BigDecimal ptotal) {
        this.ptotal = ptotal;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}

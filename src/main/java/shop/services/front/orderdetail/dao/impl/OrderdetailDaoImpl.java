/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.orderdetail.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.orderdetail.bean.Orderdetail;
import shop.services.front.orderdetail.dao.OrderdetailDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：OrderdetailDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:49:46  
 * @版本号：1.0
 * @描述：
 */
@Repository("orderdetailDaoFront")
public class OrderdetailDaoImpl  implements OrderdetailDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Orderdetail e) {
        return dao.selectPageList("front.orderdetail.selectPageList","front.orderdetail.selectPageCount", e);
    }

    public List selectList(Orderdetail e) {
        return dao.selectList("front.orderdetail.selectList", e);
    }

    public Orderdetail selectOne(Orderdetail e) {
        return (Orderdetail) dao.selectOne("front.orderdetail.selectOne", e);
    }

    public int delete(Orderdetail e) {
        return dao.delete("front.orderdetail.delete", e);
    }

    public int update(Orderdetail e) {
        return dao.update("front.orderdetail.update", e);
    }

    public int deletes(String[] ids) {
        Orderdetail e = new Orderdetail();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Orderdetail e) {
        return dao.insert("front.orderdetail.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.orderdetail.deleteById", id);
    }

    @Override
    public Orderdetail selectById(String id) {
        return (Orderdetail) dao.selectOne("front.orderdetail.selectById", id);
    }

    @Override
    public List<Orderdetail> selectOrderDetailList(Orderdetail orderdetail) {
        return dao.selectList("front.orderdetail.selectOrderDetailList", orderdetail);
    }

    @Override
    public List<Orderdetail> selectCommentList(Orderdetail orderdetail) {
        return dao.selectList("front.orderdetail.selectCommentList", orderdetail);
    }

    @Override
    public List<Orderdetail> selectHistoryOrderdetail(Orderdetail orderdetail) {
        return dao.selectList("front.orderdetail.selectHistoryOrderdetail", orderdetail);
    }
}


/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.orderdetail.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.front.orderdetail.OrderdetailService;
import shop.services.front.orderdetail.bean.Orderdetail;
import shop.services.front.orderdetail.dao.OrderdetailDao;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：OrderdetailServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:49:46        
 * @版本号：1.0
 * @描述：     
 */
@Service("orderdetailServiceFront")
public class OrderdetailServiceImpl extends ServicesManager<Orderdetail, OrderdetailDao> implements
        OrderdetailService {
    @Resource(name = "orderdetailDaoFront")
    @Override
    public void setDao(OrderdetailDao orderdetailDao) {
    this.dao = orderdetailDao;
    }

    @Override
    public List<Orderdetail> selectOrderDetailList(Orderdetail orderdetail) {
        return dao.selectOrderDetailList(orderdetail);
    }

    @Override
    public List<Orderdetail> selectCommentList(Orderdetail orderdetail) {
        return dao.selectCommentList(orderdetail);
    }

    @Override
    public List<Orderdetail> selectHistoryOrderdetail(Orderdetail orderdetail) {
        return dao.selectHistoryOrderdetail(orderdetail);
    }
}


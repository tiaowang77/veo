/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.orderpay.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.orderpay.bean.Orderpay;
import shop.services.front.orderpay.dao.OrderpayDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：OrderpayDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:50:16  
 * @版本号：1.0
 * @描述：
 */
@Repository("orderpayDaoFront")
public class OrderpayDaoImpl  implements OrderpayDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Orderpay e) {
        return dao.selectPageList("front.orderpay.selectPageList","front.orderpay.selectPageCount", e);
    }

    public List selectList(Orderpay e) {
        return dao.selectList("front.orderpay.selectList", e);
    }

    public Orderpay selectOne(Orderpay e) {
        return (Orderpay) dao.selectOne("front.orderpay.selectOne", e);
    }

    public int delete(Orderpay e) {
        return dao.delete("front.orderpay.delete", e);
    }

    public int update(Orderpay e) {
        return dao.update("front.orderpay.update", e);
    }

    public int deletes(String[] ids) {
        Orderpay e = new Orderpay();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Orderpay e) {
        return dao.insert("front.orderpay.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.orderpay.deleteById", id);
    }

    @Override
    public Orderpay selectById(String id) {
        return (Orderpay) dao.selectOne("front.orderpay.selectById", id);
    }

}


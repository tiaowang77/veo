/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.plot.dao;

import shop.core.DaoManager;
import shop.services.front.plot.bean.Plot;
import org.springframework.stereotype.Repository;

/**   
 * @类名称：PlotDao      
 * @创建人：wzl
 * @创建时间：2018-01-02 下午19:26:29        
 * @版本号：1.0
 * @描述：     
 */
public interface PlotDao extends DaoManager<Plot> {

}


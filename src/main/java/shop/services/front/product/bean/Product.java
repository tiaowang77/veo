/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.product.bean;

import shop.services.front.spec.bean.Spec;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
* @类名称：Product前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:50:43 
* @版本号：1.0
* @描述：
*/
public class Product extends shop.services.common.Product implements Serializable{
    private static final long serialVersionUID = 1L;
    //规格信息（查询用）
    private List<Spec> specs;
    //商品规格组合(用,隔开)（查询、添加用）
    private String specCombination;
    //商品规格进价(用,隔开)（查询、添加用）
    private String specPurchasePrice;
    //商品规格现价(用,隔开)（查询、添加用）
    private String specNowPrice;
    //商品规格团购价(用,隔开)（查询、添加用）
    private String specGroupPrice;
    //商品规格库存(用,隔开)（查询、添加用）
    private String specStock;
    //商品收藏状态
    private String favoriteStatus;
    //商品预定人数
    private Integer reserveNumber;
    //日期（排序用）
    private String dayTime;
    //分享价格等级列表
    private List<BigDecimal> priceList;
    //用户分享等级
    private String userRank;



    public List<Spec> getSpecs() {
        return specs;
    }

    public void setSpecs(List<Spec> specs) {
        this.specs = specs;
    }

    public String getSpecCombination() {
        return specCombination;
    }

    public void setSpecCombination(String specCombination) {
        this.specCombination = specCombination;
    }

    public String getSpecPurchasePrice() {
        return specPurchasePrice;
    }

    public void setSpecPurchasePrice(String specPurchasePrice) {
        this.specPurchasePrice = specPurchasePrice;
    }

    public String getSpecNowPrice() {
        return specNowPrice;
    }

    public void setSpecNowPrice(String specNowPrice) {
        this.specNowPrice = specNowPrice;
    }

    public String getSpecGroupPrice() {
        return specGroupPrice;
    }

    public void setSpecGroupPrice(String specGroupPrice) {
        this.specGroupPrice = specGroupPrice;
    }

    public String getSpecStock() {
        return specStock;
    }

    public void setSpecStock(String specStock) {
        this.specStock = specStock;
    }

    public String getFavoriteStatus() {
        return favoriteStatus;
    }

    public void setFavoriteStatus(String favoriteStatus) {
        this.favoriteStatus = favoriteStatus;
    }

    public Integer getReserveNumber() {
        return reserveNumber;
    }

    public void setReserveNumber(Integer reserveNumber) {
        this.reserveNumber = reserveNumber;
    }

    public String getDayTime() {
        return dayTime;
    }

    public void setDayTime(String dayTime) {
        this.dayTime = dayTime;
    }

    public List<BigDecimal> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<BigDecimal> priceList) {
        this.priceList = priceList;
    }

    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.product.dao;

import shop.core.DaoManager;
import shop.services.front.product.bean.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：ProductDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:50:43        
 * @版本号：1.0
 * @描述：     
 */
public interface ProductDao extends DaoManager<Product> {

    List<Product> selectProductList(Product product);

    List<Product> selectProductListBySellCount(Product product);

    List<Product> selectGroupProductList(Product product);
}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.product.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.product.bean.Product;
import shop.services.front.product.dao.ProductDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ProductDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:50:43  
 * @版本号：1.0
 * @描述：
 */
@Repository("productDaoFront")
public class ProductDaoImpl  implements ProductDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Product e) {
        return dao.selectPageList("front.product.selectPageList","front.product.selectPageCount", e);
    }

    public List selectList(Product e) {
        return dao.selectList("front.product.selectList", e);
    }

    public Product selectOne(Product e) {
        return (Product) dao.selectOne("front.product.selectOne", e);
    }

    public int delete(Product e) {
        return dao.delete("front.product.delete", e);
    }

    public int update(Product e) {
        return dao.update("front.product.update", e);
    }

    public int deletes(String[] ids) {
        Product e = new Product();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Product e) {
        return dao.insert("front.product.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.product.deleteById", id);
    }

    @Override
    public Product selectById(String id) {
        return (Product) dao.selectOne("front.product.selectById", id);
    }

    @Override
    public List<Product> selectProductList(Product product) {
        return dao.selectList("front.product.selectProductList", product);
    }

    @Override
    public List<Product> selectProductListBySellCount(Product product) {
        return dao.selectList("front.product.selectProductListBySellCount", product);
    }

    @Override
    public List<Product> selectGroupProductList(Product product) {
        return dao.selectList("front.product.selectGroupProductList", product);
    }
}


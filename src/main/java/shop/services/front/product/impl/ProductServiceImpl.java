/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.product.impl;

import shop.core.ServicesManager;
import shop.services.front.product.ProductService;
import shop.services.front.product.bean.Product;
import shop.services.front.product.dao.ProductDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：ProductServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:50:43        
 * @版本号：1.0
 * @描述：     
 */
@Service("productServiceFront")
public class ProductServiceImpl extends ServicesManager<Product, ProductDao> implements
ProductService {
    @Resource(name = "productDaoFront")
    @Override
    public void setDao(ProductDao productDao) {
    this.dao = productDao;
    }

    @Override
    public List<Product> selectProductList(Product product) {
        return dao.selectProductList(product);
    }

    @Override
    public List<Product> selectProductListBySellCount(Product product) {
        return dao.selectProductListBySellCount(product);
    }

    @Override
    public List<Product> selectGroupProductList(Product product) {
        return dao.selectGroupProductList(product);
    }
}


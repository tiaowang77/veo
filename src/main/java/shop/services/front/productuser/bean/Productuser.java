/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.productuser.bean;
import java.io.Serializable;

/**
* @类名称：Productuser前台对象类
* @创建人：wzl
* @创建时间：2018-06-09 下午14:44:56 
* @版本号：1.0
* @描述：
*/
public class Productuser extends shop.services.common.Productuser implements Serializable{
    private static final long serialVersionUID = 1L;
}

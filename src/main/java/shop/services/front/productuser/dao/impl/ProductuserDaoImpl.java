/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.productuser.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.productuser.bean.Productuser;
import shop.services.front.productuser.dao.ProductuserDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ProductuserDaoImpl
 * @创建人：wzl
 * @创建时间：2018-06-09 下午14:44:56  
 * @版本号：1.0
 * @描述：
 */
@Repository("productuserDaoFront")
public class ProductuserDaoImpl  implements ProductuserDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Productuser e) {
        return dao.selectPageList("front.productuser.selectPageList","front.productuser.selectPageCount", e);
    }

    public List selectList(Productuser e) {
        return dao.selectList("front.productuser.selectList", e);
    }

    public Productuser selectOne(Productuser e) {
        return (Productuser) dao.selectOne("front.productuser.selectOne", e);
    }

    public int delete(Productuser e) {
        return dao.delete("front.productuser.delete", e);
    }

    public int update(Productuser e) {
        return dao.update("front.productuser.update", e);
    }

    public int deletes(String[] ids) {
        Productuser e = new Productuser();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Productuser e) {
        return dao.insert("front.productuser.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.productuser.deleteById", id);
    }

    @Override
    public Productuser selectById(String id) {
        return (Productuser) dao.selectOne("front.productuser.selectById", id);
    }

}


/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.productuser.impl;

import shop.core.ServicesManager;
import shop.services.front.productuser.ProductuserService;
import shop.services.front.productuser.bean.Productuser;
import shop.services.front.productuser.dao.ProductuserDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：ProductuserServiceImpl      
 * @创建人：wzl
 * @创建时间：2018-06-09 下午14:44:56        
 * @版本号：1.0
 * @描述：     
 */
@Service("productuserServiceFront")
public class ProductuserServiceImpl extends ServicesManager<Productuser, ProductuserDao> implements
ProductuserService {
    @Resource(name = "productuserDaoFront")
    @Override
    public void setDao(ProductuserDao productuserDao) {
    this.dao = productuserDao;
    }
}


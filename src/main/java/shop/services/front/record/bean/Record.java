/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.record.bean;

import java.io.Serializable;

/**
 * @类名称：Record前台对象类
 * @创建人：Ltz
 * @创建时间：2017-10-13 下午16:13:14
 * @版本号：1.0
 * @描述：
 */
public class Record extends shop.services.common.Record implements Serializable{
    private static final long serialVersionUID = 1L;
    //提现状态(1待审核、2已通过、3不通过）（查询用）
    private String cashStatus;


    public String getCashStatus() {
        return cashStatus;
    }

    public void setCashStatus(String cashStatus) {
        this.cashStatus = cashStatus;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.record.dao;

import shop.core.DaoManager;
import shop.services.front.record.bean.Record;

/**   
 * @类名称：RecordDao      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午16:13:14        
 * @版本号：1.0
 * @描述：     
 */
public interface RecordDao extends DaoManager<Record> {

}


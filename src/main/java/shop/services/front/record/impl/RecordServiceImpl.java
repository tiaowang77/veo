/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.record.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.front.record.RecordService;
import shop.services.front.record.bean.Record;
import shop.services.front.record.dao.RecordDao;

import javax.annotation.Resource;


/**   
 * @类名称：RecordServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午16:13:14        
 * @版本号：1.0
 * @描述：     
 */
@Service("recordServiceFront")
public class RecordServiceImpl extends ServicesManager<Record, RecordDao> implements
        RecordService {
    @Resource(name = "recordDaoFront")
    @Override
    public void setDao(RecordDao recordDao) {
    this.dao = recordDao;
    }
}


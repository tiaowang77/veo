/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.spec;

import shop.core.Services;
import shop.services.front.spec.bean.Spec;

/**   
 * @类名称：SpecService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:51:23       
 * @版本号：1.0
 * @描述：     
 */
public interface SpecService extends Services<Spec>{
}


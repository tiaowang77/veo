/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.spec.bean;
import java.io.Serializable;

/**
* @类名称：Spec前台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:51:23
* @版本号：1.0
* @描述：
*/
public class Spec extends shop.services.common.Spec implements Serializable{
    private static final long serialVersionUID = 1L;

    //商品名称（查询用）
    private String productName;
    //商品主图（查询用）
    private String picture;
    //商品图片（查询用）
    private String productImages;
    //是否自营（查询用）
    private String productIsSelf;


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProductImages() {
        return productImages;
    }

    public void setProductImages(String productImages) {
        this.productImages = productImages;
    }

    public String getProductIsSelf() {
        return productIsSelf;
    }

    public void setProductIsSelf(String productIsSelf) {
        this.productIsSelf = productIsSelf;
    }
}

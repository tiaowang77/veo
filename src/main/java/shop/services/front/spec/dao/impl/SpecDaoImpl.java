/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.spec.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.spec.bean.Spec;
import shop.services.front.spec.dao.SpecDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：SpecDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:51:23  
 * @版本号：1.0
 * @描述：
 */
@Repository("specDaoFront")
public class SpecDaoImpl  implements SpecDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Spec e) {
        return dao.selectPageList("front.spec.selectPageList","front.spec.selectPageCount", e);
    }

    public List selectList(Spec e) {
        return dao.selectList("front.spec.selectList", e);
    }

    public Spec selectOne(Spec e) {
        return (Spec) dao.selectOne("front.spec.selectOne", e);
    }

    public int delete(Spec e) {
        return dao.delete("front.spec.delete", e);
    }

    public int update(Spec e) {
        return dao.update("front.spec.update", e);
    }

    public int deletes(String[] ids) {
        Spec e = new Spec();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Spec e) {
        return dao.insert("front.spec.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.spec.deleteById", id);
    }

    @Override
    public Spec selectById(String id) {
        return (Spec) dao.selectOne("front.spec.selectById", id);
    }

}


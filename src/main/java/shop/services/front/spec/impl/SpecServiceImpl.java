/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.spec.impl;

import shop.core.ServicesManager;
import shop.services.front.spec.SpecService;
import shop.services.front.spec.bean.Spec;
import shop.services.front.spec.dao.SpecDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：SpecServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:51:23        
 * @版本号：1.0
 * @描述：     
 */
@Service("specServiceFront")
public class SpecServiceImpl extends ServicesManager<Spec, SpecDao> implements
SpecService {
    @Resource(name = "specDaoFront")
    @Override
    public void setDao(SpecDao specDao) {
    this.dao = specDao;
    }
}


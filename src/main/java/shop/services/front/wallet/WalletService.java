/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.wallet;

import shop.core.Services;
import shop.services.front.wallet.bean.Wallet;

/**   
 * @类名称：WalletService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:55:25       
 * @版本号：1.0
 * @描述：     
 */
public interface WalletService extends Services<Wallet>{

}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.wallet.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.wallet.bean.Wallet;
import shop.services.front.wallet.dao.WalletDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：WalletDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:55:25  
 * @版本号：1.0
 * @描述：
 */
@Repository("walletDaoFront")
public class WalletDaoImpl  implements WalletDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Wallet e) {
        return dao.selectPageList("front.wallet.selectPageList","front.wallet.selectPageCount", e);
    }

    public List selectList(Wallet e) {
        return dao.selectList("front.wallet.selectList", e);
    }

    public Wallet selectOne(Wallet e) {
        return (Wallet) dao.selectOne("front.wallet.selectOne", e);
    }

    public int delete(Wallet e) {
        return dao.delete("front.wallet.delete", e);
    }

    public int update(Wallet e) {
        return dao.update("front.wallet.update", e);
    }

    public int deletes(String[] ids) {
        Wallet e = new Wallet();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Wallet e) {
        return dao.insert("front.wallet.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.wallet.deleteById", id);
    }

    @Override
    public Wallet selectById(String id) {
        return (Wallet) dao.selectOne("front.wallet.selectById", id);
    }

}


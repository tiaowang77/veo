/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.wallet.impl;

import shop.core.ServicesManager;
import shop.services.front.wallet.WalletService;
import shop.services.front.wallet.bean.Wallet;
import shop.services.front.wallet.dao.WalletDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：WalletServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:55:25        
 * @版本号：1.0
 * @描述：     
 */
@Service("walletServiceFront")
public class WalletServiceImpl extends ServicesManager<Wallet, WalletDao> implements
WalletService {
    @Resource(name = "walletDaoFront")
    @Override
    public void setDao(WalletDao walletDao) {
    this.dao = walletDao;
    }
}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.address.dao;

import shop.core.DaoManager;
import shop.services.manage.address.bean.Address;
import org.springframework.stereotype.Repository;

/**   
 * @类名称：AddressDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:04        
 * @版本号：1.0
 * @描述：     
 */
public interface AddressDao extends DaoManager<Address> {

}


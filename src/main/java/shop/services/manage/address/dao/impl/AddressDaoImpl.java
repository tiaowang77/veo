/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.address.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.address.bean.Address;
import shop.services.manage.address.dao.AddressDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：AddressDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:33:04  
 * @版本号：1.0
 * @描述：
 */
@Repository("addressDaoManage")
public class AddressDaoImpl  implements AddressDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Address e) {
        return dao.selectPageList("manage.address.selectPageList","manage.address.selectPageCount", e);
    }

    public List selectList(Address e) {
        return dao.selectList("manage.address.selectList", e);
    }

    public Address selectOne(Address e) {
        return (Address) dao.selectOne("manage.address.selectOne", e);
    }

    public int delete(Address e) {
        return dao.delete("manage.address.delete", e);
    }

    public int update(Address e) {
        return dao.update("manage.address.update", e);
    }

    public int deletes(String[] ids) {
        Address e = new Address();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Address e) {
        return dao.insert("manage.address.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.address.deleteById", id);
    }

    @Override
    public Address selectById(String id) {
        return (Address) dao.selectOne("manage.address.selectById", id);
    }

}


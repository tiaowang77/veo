/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.address.impl;

import shop.core.ServicesManager;
import shop.services.manage.address.AddressService;
import shop.services.manage.address.bean.Address;
import shop.services.manage.address.dao.AddressDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：AddressServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:04        
 * @版本号：1.0
 * @描述：     
 */
@Service("addressServiceManage")
public class AddressServiceImpl extends ServicesManager<Address, AddressDao> implements
AddressService {
    @Resource(name = "addressDaoManage")
    @Override
    public void setDao(AddressDao addressDao) {
    this.dao = addressDao;
    }
}


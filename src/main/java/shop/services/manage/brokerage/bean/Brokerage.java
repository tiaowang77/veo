/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.brokerage.bean;

/**
* @类名称：Brokerage后台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:33:32 
* @版本号：1.0
* @描述：
*/
public class Brokerage extends shop.services.common.Brokerage {
    //下单人
    private String orderPerson;
    //获利方
    private String profitPerson;

    public String getOrderPerson() {
        return orderPerson;
    }

    public void setOrderPerson(String orderPerson) {
        this.orderPerson = orderPerson;
    }

    public String getProfitPerson() {
        return profitPerson;
    }

    public void setProfitPerson(String profitPerson) {
        this.profitPerson = profitPerson;
    }

}

/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.brokerage.impl;

import shop.core.ServicesManager;
import shop.services.manage.brokerage.BrokerageService;
import shop.services.manage.brokerage.bean.Brokerage;
import shop.services.manage.brokerage.dao.BrokerageDao;
import org.springframework.stereotype.Service;
import shop.services.manage.wallet.bean.Wallet;

import javax.annotation.Resource;


/**   
 * @类名称：BrokerageServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:32        
 * @版本号：1.0
 * @描述：     
 */
@Service("brokerageServiceManage")
public class BrokerageServiceImpl extends ServicesManager<Brokerage, BrokerageDao> implements
BrokerageService {
    @Resource(name = "brokerageDaoManage")
    @Override
    public void setDao(BrokerageDao brokerageDao) {
    this.dao = brokerageDao;
    }

}


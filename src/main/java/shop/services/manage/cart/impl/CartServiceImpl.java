/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.cart.impl;

import shop.core.ServicesManager;
import shop.services.manage.cart.CartService;
import shop.services.manage.cart.bean.Cart;
import shop.services.manage.cart.dao.CartDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：CartServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:33:52        
 * @版本号：1.0
 * @描述：     
 */
@Service("cartServiceManage")
public class CartServiceImpl extends ServicesManager<Cart, CartDao> implements
CartService {
    @Resource(name = "cartDaoManage")
    @Override
    public void setDao(CartDao cartDao) {
    this.dao = cartDao;
    }
}


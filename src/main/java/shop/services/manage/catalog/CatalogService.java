/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.catalog;

import shop.core.Services;
import shop.services.manage.catalog.bean.Catalog;

/**   
 * @类名称：CatalogService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:34:37       
 * @版本号：1.0
 * @描述：     
 */
public interface CatalogService extends Services<Catalog>{
    int deleteById(int id);
    Catalog selectByOrder(Catalog catalog);

}


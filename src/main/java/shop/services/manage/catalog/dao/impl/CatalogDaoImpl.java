/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.catalog.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.catalog.bean.Catalog;
import shop.services.manage.catalog.dao.CatalogDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：CatalogDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:34:37  
 * @版本号：1.0
 * @描述：
 */
@Repository("catalogDaoManage")
public class CatalogDaoImpl  implements CatalogDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Catalog e) {
        return dao.selectPageList("manage.catalog.selectPageList","manage.catalog.selectPageCount", e);
    }

    public List selectList(Catalog e) {
        return dao.selectList("manage.catalog.selectList", e);
    }

    public Catalog selectOne(Catalog e) {
        return (Catalog) dao.selectOne("manage.catalog.selectOne", e);
    }

    public int delete(Catalog e) {
        return dao.delete("manage.catalog.delete", e);
    }

    public int update(Catalog e) {
        return dao.update("manage.catalog.update", e);
    }

    public int deletes(String[] ids) {
        Catalog e = new Catalog();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Catalog e) {
        return dao.insert("manage.catalog.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.catalog.deleteById", id);
    }

    @Override
    public Catalog selectById(String id) {
        return (Catalog) dao.selectOne("manage.catalog.selectById", id);
    }

    public Catalog selectByOrder(Catalog catalog) {
        return (Catalog) dao.selectOne("manage.catalog.selectByOrder", catalog);
    }

}


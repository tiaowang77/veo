/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.catalog.impl;

import shop.core.ServicesManager;
import shop.services.manage.catalog.CatalogService;
import shop.services.manage.catalog.bean.Catalog;
import shop.services.manage.catalog.dao.CatalogDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：CatalogServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:34:37        
 * @版本号：1.0
 * @描述：     
 */
@Service("catalogServiceManage")
public class CatalogServiceImpl extends ServicesManager<Catalog, CatalogDao> implements
CatalogService {
    @Resource(name = "catalogDaoManage")
    @Override
    public void setDao(CatalogDao catalogDao) {
    this.dao = catalogDao;
    }

    /**
    * 根据id来删除--单个删除操作
    * */
    public int deleteById(int id) {
        return dao.deleteById(id);
    }

    public Catalog selectByOrder(Catalog catalog) {
        return dao.selectByOrder(catalog);
    }


}


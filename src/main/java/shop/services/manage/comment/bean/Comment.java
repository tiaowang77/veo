/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.comment.bean;

/**
* @类名称：Comment后台对象类
* @创建人：Ltz
* @创建时间：2017-10-13 下午15:05:09 
* @版本号：1.0
* @描述：
*/
public class Comment extends shop.services.common.Comment {
    //商品名称
    private String productName;
    //商品规格
    private String specInfo;
    //评论人
    private String userName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSpecInfo() {
        return specInfo;
    }

    public void setSpecInfo(String specInfo) {
        this.specInfo = specInfo;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.comment.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.comment.bean.Comment;
import shop.services.manage.comment.dao.CommentDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：CommentDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-10-13 下午15:05:09  
 * @版本号：1.0
 * @描述：
 */
@Repository("commentDaoManage")
public class CommentDaoImpl  implements CommentDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Comment e) {
        return dao.selectPageList("manage.comment.selectPageList","manage.comment.selectPageCount", e);
    }

    public List selectList(Comment e) {
        return dao.selectList("manage.comment.selectList", e);
    }

    public Comment selectOne(Comment e) {
        return (Comment) dao.selectOne("manage.comment.selectOne", e);
    }

    public int delete(Comment e) {
        return dao.delete("manage.comment.delete", e);
    }

    public int update(Comment e) {
        return dao.update("manage.comment.update", e);
    }

    public int deletes(String[] ids) {
        Comment e = new Comment();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Comment e) {
        return dao.insert("manage.comment.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.comment.deleteById", id);
    }

    @Override
    public Comment selectById(String id) {
        return (Comment) dao.selectOne("manage.comment.selectById", id);
    }

}


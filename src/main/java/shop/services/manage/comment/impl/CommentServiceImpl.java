/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.comment.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.manage.comment.CommentService;
import shop.services.manage.comment.bean.Comment;
import shop.services.manage.comment.dao.CommentDao;

import javax.annotation.Resource;


/**   
 * @类名称：CommentServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:05:09        
 * @版本号：1.0
 * @描述：     
 */
@Service("commentServiceManage")
public class CommentServiceImpl extends ServicesManager<Comment, CommentDao> implements
        CommentService {
    @Resource(name = "commentDaoManage")
    @Override
    public void setDao(CommentDao commentDao) {
    this.dao = commentDao;
    }
}


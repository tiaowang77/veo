/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.favorite.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.manage.favorite.FavoriteService;
import shop.services.manage.favorite.bean.Favorite;
import shop.services.manage.favorite.dao.FavoriteDao;

import javax.annotation.Resource;


/**   
 * @类名称：FavoriteServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:06:28        
 * @版本号：1.0
 * @描述：     
 */
@Service("favoriteServiceManage")
public class FavoriteServiceImpl extends ServicesManager<Favorite, FavoriteDao> implements
        FavoriteService {
    @Resource(name = "favoriteDaoManage")
    @Override
    public void setDao(FavoriteDao favoriteDao) {
    this.dao = favoriteDao;
    }
}


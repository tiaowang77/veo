/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.foot.dao;

import shop.core.DaoManager;
import shop.services.manage.foot.bean.Foot;

/**   
 * @类名称：FootDao      
 * @创建人：Ltz   
 * @创建时间：2017-10-13 下午15:07:59        
 * @版本号：1.0
 * @描述：     
 */
public interface FootDao extends DaoManager<Foot> {

}


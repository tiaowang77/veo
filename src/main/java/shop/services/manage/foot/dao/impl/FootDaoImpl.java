/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.foot.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.foot.bean.Foot;
import shop.services.manage.foot.dao.FootDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：FootDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-10-13 下午15:07:59  
 * @版本号：1.0
 * @描述：
 */
@Repository("footDaoManage")
public class FootDaoImpl  implements FootDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Foot e) {
        return dao.selectPageList("manage.foot.selectPageList","manage.foot.selectPageCount", e);
    }

    public List selectList(Foot e) {
        return dao.selectList("manage.foot.selectList", e);
    }

    public Foot selectOne(Foot e) {
        return (Foot) dao.selectOne("manage.foot.selectOne", e);
    }

    public int delete(Foot e) {
        return dao.delete("manage.foot.delete", e);
    }

    public int update(Foot e) {
        return dao.update("manage.foot.update", e);
    }

    public int deletes(String[] ids) {
        Foot e = new Foot();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Foot e) {
        return dao.insert("manage.foot.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.foot.deleteById", id);
    }

    @Override
    public Foot selectById(String id) {
        return (Foot) dao.selectOne("manage.foot.selectById", id);
    }

}


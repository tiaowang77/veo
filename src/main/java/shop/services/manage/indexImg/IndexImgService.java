/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.indexImg;

import shop.core.Services;
import shop.services.manage.indexImg.bean.IndexImg;

/**
 * @类名称：IndexService
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午16:04:29       
 * @版本号：1.0
 * @描述：
 */
public interface IndexImgService extends Services<IndexImg>{
    int deleteById(int id);
    IndexImg selectByOrder(IndexImg e);

}


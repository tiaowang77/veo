/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.indexImg.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.indexImg.bean.IndexImg;
import shop.services.manage.indexImg.dao.IndexImgDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：IndexDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午16:04:29  
 * @版本号：1.0
 * @描述：
 */
@Repository("indexDaoManage")
public class IndexImgDaoImpl implements IndexImgDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(IndexImg e) {
        return dao.selectPageList("manage.index.selectPageList","manage.index.selectPageCount", e);
    }

    public List selectList(IndexImg e) {
        return dao.selectList("manage.index.selectList", e);
    }

    public IndexImg selectOne(IndexImg e) {
        return (IndexImg) dao.selectOne("manage.index.selectOne", e);
    }

    public int delete(IndexImg e) {
        return dao.delete("manage.index.delete", e);
    }

    public int update(IndexImg e) {
        return dao.update("manage.index.update", e);
    }

    public int deletes(String[] ids) {
        IndexImg e = new IndexImg();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(IndexImg e) {
        return dao.insert("manage.index.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.index.deleteById", id);
    }

    @Override
    public IndexImg selectById(String id) {
        return (IndexImg) dao.selectOne("manage.index.selectById", id);
    }

    /**
    * 通过排序号码查找--为了避免序号相同
    * */
    public IndexImg selectByOrder(IndexImg e) {
        return (IndexImg) dao.selectOne("manage.index.selectByOrder", e);
    }

}
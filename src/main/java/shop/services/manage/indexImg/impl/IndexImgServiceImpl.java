/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.indexImg.impl;

import shop.core.ServicesManager;
import shop.services.manage.indexImg.IndexImgService;
import shop.services.manage.indexImg.bean.IndexImg;
import shop.services.manage.indexImg.dao.IndexImgDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @类名称：IndexServiceImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午16:04:29        
 * @版本号：1.0
 * @描述：
 */
@Service("indexServiceManage")
public class IndexImgServiceImpl extends ServicesManager<IndexImg, IndexImgDao> implements
        IndexImgService {
    @Resource(name = "indexDaoManage")
    @Override
    public void setDao(IndexImgDao indexImgDao) {
        this.dao = indexImgDao;
    }

    @Override
    public int deleteById(int id) {
        return dao.deleteById(id);
    }

    /**
     * 获取排序的号码--排序的字段不能相同
     * */
    public IndexImg selectByOrder(IndexImg e) {
        return dao.selectByOrder(e);
    }



}


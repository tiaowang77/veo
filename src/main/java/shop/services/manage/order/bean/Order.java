/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.order.bean;

import java.math.BigDecimal;

/**
* @类名称：Order后台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:48:44 
* @版本号：1.0
* @描述：
*/
public class Order extends shop.services.common.Order {
    //商品名称（查询用）
    private String productName;
    //订单详情ID（查询用）
    private String orderDetailID;
    //订单ID（查询用）
    private Integer orderID;
    //买家账号（查询用）
    private String nickname;
    //获利者（查询用）
    private String gainer;
    //商品价格（查询用）
    private BigDecimal price;
    //商品数量（查询用）
    private Integer number;
    //商品总价（查询用）
    private BigDecimal dtotal;
    //开始时间（查询用）
    private String startDate;
    //结束时间（查询用）
    private String endDate;
    //商品开团时间（查询用）
    private String productStartTime;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOrderDetailID() {
        return orderDetailID;
    }

    public void setOrderDetailID(String orderDetailID) {
        this.orderDetailID = orderDetailID;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGainer() {
        return gainer;
    }

    public void setGainer(String gainer) {
        this.gainer = gainer;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getDtotal() {
        return dtotal;
    }

    public void setDtotal(BigDecimal dtotal) {
        this.dtotal = dtotal;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getProductStartTime() {
        return productStartTime;
    }

    public void setProductStartTime(String productStartTime) {
        this.productStartTime = productStartTime;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.order.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.order.bean.Order;
import shop.services.manage.order.dao.OrderDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：OrderDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:48:44  
 * @版本号：1.0
 * @描述：
 */
@Repository("orderDaoManage")
public class OrderDaoImpl  implements OrderDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Order e) {
        return dao.selectPageList("manage.order.selectPageList","manage.order.selectPageCount", e);
    }

    public List selectList(Order e) {
        return dao.selectList("manage.order.selectList", e);
    }

    public Order selectOne(Order e) {
        return (Order) dao.selectOne("manage.order.selectOne", e);
    }

    public int delete(Order e) {
        return dao.delete("manage.order.delete", e);
    }

    public int update(Order e) {
        return dao.update("manage.order.update", e);
    }

    public int deletes(String[] ids) {
        Order e = new Order();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Order e) {
        return dao.insert("manage.order.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.order.deleteById", id);
    }

    @Override
    public Order selectById(String id) {
        return (Order) dao.selectOne("manage.order.selectById", id);
    }

    @Override
    public List<Order> selectBrokerageOrder(Order order) {
        return dao.selectList("manage.order.selectBrokerageOrder", order);
    }
}


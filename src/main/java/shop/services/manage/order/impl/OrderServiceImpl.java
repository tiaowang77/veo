/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.order.impl;

import shop.core.ServicesManager;
import shop.services.manage.order.OrderService;
import shop.services.manage.order.bean.Order;
import shop.services.manage.order.dao.OrderDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：OrderServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:48:44        
 * @版本号：1.0
 * @描述：     
 */
@Service("orderServiceManage")
public class OrderServiceImpl extends ServicesManager<Order, OrderDao> implements
OrderService {
    @Resource(name = "orderDaoManage")
    @Override
    public void setDao(OrderDao orderDao) {
    this.dao = orderDao;
    }

    @Override
    public List<Order> selectBrokerageOrder(Order order) {
        return dao.selectBrokerageOrder(order);
    }
}


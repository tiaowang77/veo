/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.orderdetail.bean;

/**
* @类名称：Orderdetail后台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:49:46 
* @版本号：1.0
* @描述：
*/
public class Orderdetail extends shop.services.common.Orderdetail {
    //买家账号
    private String nickname;
    //商品名称
    private String productName;
    //商品图片
    private String picture;
    //订单状态
    private String status;
    //收货人姓名
    private String name;
    //收货人详细地址
    private String address;
    //收货人省市区
    private String province;
    //收货人手机号
    private String phone;
    //邮编
    private String zip;
    //物流公司
    private String expressCompany;
    //物流单号
    private String expressNum;
    //发货人
    private String consigner;
    //订单备注
    private String remark;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
    }

    public String getExpressNum() {
        return expressNum;
    }

    public void setExpressNum(String expressNum) {
        this.expressNum = expressNum;
    }

    public String getConsigner() {
        return consigner;
    }

    public void setConsigner(String consigner) {
        this.consigner = consigner;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.orderdetail.dao;

import shop.core.DaoManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.orderdetail.bean.Orderdetail;

import java.util.List;

/**   
 * @类名称：OrderdetailDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:49:46        
 * @版本号：1.0
 * @描述：     
 */
public interface OrderdetailDao extends DaoManager<Orderdetail> {

    public PagerModel selectPageListProduct(Orderdetail e);

    List<Orderdetail> selectHistoryOrderdetail(Orderdetail orderdetail);
}


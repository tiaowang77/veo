/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.orderdetail.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.orderdetail.bean.Orderdetail;
import shop.services.manage.orderdetail.dao.OrderdetailDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：OrderdetailDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:49:46  
 * @版本号：1.0
 * @描述：
 */
@Repository("orderdetailDaoManage")
public class OrderdetailDaoImpl  implements OrderdetailDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Orderdetail e) {
        return dao.selectPageList("manage.orderdetail.selectPageList","manage.orderdetail.selectPageCount", e);
    }

    public List selectList(Orderdetail e) {
        return dao.selectList("manage.orderdetail.selectList", e);
    }

    public Orderdetail selectOne(Orderdetail e) {
        return (Orderdetail) dao.selectOne("manage.orderdetail.selectOne", e);
    }

    public int delete(Orderdetail e) {
        return dao.delete("manage.orderdetail.delete", e);
    }

    public int update(Orderdetail e) {
        return dao.update("manage.orderdetail.update", e);
    }

    public int deletes(String[] ids) {
        Orderdetail e = new Orderdetail();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Orderdetail e) {
        return dao.insert("manage.orderdetail.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.orderdetail.deleteById", id);
    }

    @Override
    public Orderdetail selectById(String id) {
        return (Orderdetail) dao.selectOne("manage.orderdetail.selectById", id);
    }

    @Override
    public PagerModel selectPageListProduct(Orderdetail e) {
        return dao.selectPageList("manage.orderdetail.selectPageListProduct", "manage.orderdetail.selectProductCount", e);
    }

    @Override
    public List<Orderdetail> selectHistoryOrderdetail(Orderdetail orderdetail) {
        return dao.selectList("manage.orderdetail.selectHistoryOrderdetail", orderdetail);
    }
}


/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.orderdetail.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.orderdetail.OrderdetailService;
import shop.services.manage.orderdetail.bean.Orderdetail;
import shop.services.manage.orderdetail.dao.OrderdetailDao;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：OrderdetailServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:49:46        
 * @版本号：1.0
 * @描述：     
 */
@Service("orderdetailServiceManage")
public class OrderdetailServiceImpl extends ServicesManager<Orderdetail, OrderdetailDao> implements
        OrderdetailService {
    @Resource(name = "orderdetailDaoManage")
    @Override
    public void setDao(OrderdetailDao orderdetailDao) {
    this.dao = orderdetailDao;
    }

    @Override
    public PagerModel selectPageListProduct(Orderdetail e) {
        return dao.selectPageListProduct(e);
    }

    @Override
    public List<Orderdetail> selectHistoryOrderdetail(Orderdetail orderdetail) {
        return dao.selectHistoryOrderdetail(orderdetail);
    }
}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.orderpay.dao;

import shop.core.DaoManager;
import shop.services.manage.orderpay.bean.Orderpay;
import org.springframework.stereotype.Repository;

/**   
 * @类名称：OrderpayDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:50:16        
 * @版本号：1.0
 * @描述：     
 */
public interface OrderpayDao extends DaoManager<Orderpay> {

}


/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.orderpay.impl;

import shop.core.ServicesManager;
import shop.services.manage.orderpay.OrderpayService;
import shop.services.manage.orderpay.bean.Orderpay;
import shop.services.manage.orderpay.dao.OrderpayDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：OrderpayServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:50:16        
 * @版本号：1.0
 * @描述：     
 */
@Service("orderpayServiceManage")
public class OrderpayServiceImpl extends ServicesManager<Orderpay, OrderpayDao> implements
OrderpayService {
    @Resource(name = "orderpayDaoManage")
    @Override
    public void setDao(OrderpayDao orderpayDao) {
    this.dao = orderpayDao;
    }
}


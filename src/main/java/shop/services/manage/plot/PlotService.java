/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.plot;

import shop.core.Services;
import shop.services.manage.plot.bean.Plot;

/**   
 * @类名称：PlotService      
 * @创建人：wzl
 * @创建时间：2018-01-02 下午19:26:29       
 * @版本号：1.0
 * @描述：     
 */
public interface PlotService extends Services<Plot>{

}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.plot.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.plot.bean.Plot;
import shop.services.manage.plot.dao.PlotDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：PlotDaoImpl
 * @创建人：wzl
 * @创建时间：2018-01-02 下午19:26:29  
 * @版本号：1.0
 * @描述：
 */
@Repository("plotDaoManage")
public class PlotDaoImpl  implements PlotDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Plot e) {
        return dao.selectPageList("manage.plot.selectPageList","manage.plot.selectPageCount", e);
    }

    public List selectList(Plot e) {
        return dao.selectList("manage.plot.selectList", e);
    }

    public Plot selectOne(Plot e) {
        return (Plot) dao.selectOne("manage.plot.selectOne", e);
    }

    public int delete(Plot e) {
        return dao.delete("manage.plot.delete", e);
    }

    public int update(Plot e) {
        return dao.update("manage.plot.update", e);
    }

    public int deletes(String[] ids) {
        Plot e = new Plot();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Plot e) {
        return dao.insert("manage.plot.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.plot.deleteById", id);
    }

    @Override
    public Plot selectById(String id) {
        return (Plot) dao.selectOne("manage.plot.selectById", id);
    }

}


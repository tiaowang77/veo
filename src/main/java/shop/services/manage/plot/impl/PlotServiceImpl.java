/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.plot.impl;

import shop.core.ServicesManager;
import shop.services.manage.plot.PlotService;
import shop.services.manage.plot.bean.Plot;
import shop.services.manage.plot.dao.PlotDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：PlotServiceImpl      
 * @创建人：wzl
 * @创建时间：2018-01-02 下午19:26:29        
 * @版本号：1.0
 * @描述：     
 */
@Service("plotServiceManage")
public class PlotServiceImpl extends ServicesManager<Plot, PlotDao> implements
PlotService {
    @Resource(name = "plotDaoManage")
    @Override
    public void setDao(PlotDao plotDao) {
    this.dao = plotDao;
    }
}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.product;

import shop.core.Services;
import shop.services.manage.product.bean.Product;

/**   
 * @类名称：ProductService      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:50:43       
 * @版本号：1.0
 * @描述：     
 */
public interface ProductService extends Services<Product>{
    void updateProductStatus(String[] ids, int status);

    void updateOneProductStatus(String id, int status);

    int deleteById(int id);
}


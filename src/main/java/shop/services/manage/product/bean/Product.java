/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.product.bean;

/**
* @类名称：Product后台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:50:43 
* @版本号：1.0
* @描述：
*/
public class Product extends shop.services.common.Product {
    //商品类别名称
    private String catalogName;
    //商品总库存
    private int number;
    //商品编号
    private String productID;
    //商品规格的ID
    private String sid;
    //商品规格状态
    private String specStatus;
    //商品组合(用,隔开)
    private String specCombination;
    //规格进价
    private String specPurchasePrice;
    //规格库存
    private String specStock;
    //规格现价
    private String specNowPrice;
    //规格团购价
    private String specGroupPrice;
    //商品规格创建时间
    private String screateTime;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getSpecStatus() {
        return specStatus;
    }

    public void setSpecStatus(String specStatus) {
        this.specStatus = specStatus;
    }

    public String getSpecCombination() {
        return specCombination;
    }

    public void setSpecCombination(String specCombination) {
        this.specCombination = specCombination;
    }

    public String getSpecPurchasePrice() {
        return specPurchasePrice;
    }

    public void setSpecPurchasePrice(String specPurchasePrice) {
        this.specPurchasePrice = specPurchasePrice;
    }

    public String getSpecStock() {
        return specStock;
    }

    public void setSpecStock(String specStock) {
        this.specStock = specStock;
    }

    public String getSpecNowPrice() {
        return specNowPrice;
    }

    public void setSpecNowPrice(String specNowPrice) {
        this.specNowPrice = specNowPrice;
    }

    public String getScreateTime() {
        return screateTime;
    }

    public void setScreateTime(String screateTime) {
        this.screateTime = screateTime;
    }

    public String getSpecGroupPrice() {
        return specGroupPrice;
    }

    public void setSpecGroupPrice(String specGroupPrice) {
        this.specGroupPrice = specGroupPrice;
    }
}

/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.product.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.product.bean.Product;
import shop.services.manage.product.dao.ProductDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：ProductDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:50:43  
 * @版本号：1.0
 * @描述：
 */
@Repository("productDaoManage")
public class ProductDaoImpl  implements ProductDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Product e) {
        return dao.selectPageList("manage.product.selectPageList","manage.product.selectPageCount", e);
    }

    public List selectList(Product e) {
        return dao.selectList("manage.product.selectList", e);
    }

    public Product selectOne(Product e) {
        return (Product) dao.selectOne("manage.product.selectOne", e);
    }

    public int delete(Product e) {
        return dao.delete("manage.product.delete", e);
    }

    public int update(Product e) {
        return dao.update("manage.product.update", e);
    }

    public int deletes(String[] ids) {
        Product e = new Product();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Product e) {
        return dao.insert("manage.product.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.product.deleteById", id);
    }

    @Override
    public Product selectById(String id) {
        return (Product) dao.selectOne("manage.product.selectById", id);
    }

}


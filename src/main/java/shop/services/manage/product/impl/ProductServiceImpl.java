/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.services.manage.product.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.manage.product.ProductService;
import shop.services.manage.product.bean.Product;
import shop.services.manage.product.dao.ProductDao;

import javax.annotation.Resource;


/**
 * @类名称：ProductServiceImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:50:43        
 * @版本号：1.0
 * @描述：
 */
@Service("productServiceManage")
public class ProductServiceImpl extends ServicesManager<Product, ProductDao> implements
        ProductService {
    @Resource(name = "productDaoManage")
    @Override
    public void setDao(ProductDao productDao) {
        this.dao = productDao;
    }

    /**
     *   批量更改商品上架、下架状态
     * */
    public void updateProductStatus(String[] ids, int status) {
        if (ids == null || ids.length == 0) {
            throw new NullPointerException("商品ID不能为空！");
        }

        for (int i = 0; i < ids.length; i++) {
            if (StringUtils.isBlank(ids[i])) {
                throw new NullPointerException("商品ID不能存在空的！");
            }
            Product p = new Product();
            p.setId(ids[i]);
            p.setStatus(status);
            dao.update(p);
        }
    }

    /**
     *  更改当个商品上架、下架状态
     * */
    public void updateOneProductStatus(String id, int status) {
        if (id == null) {
            throw new NullPointerException("商品ID不能为空！");
        }
        if (StringUtils.isBlank(id)) {
            throw new NullPointerException("商品ID不能存在空的！");
        }
        Product p = new Product();
        p.setId(id);
        p.setStatus(status);
        dao.update(p);
    }

    /**
    * 根据id删除单个商品
    * */
    public int deleteById(int id) {
        return dao.deleteById(id);
    }

}


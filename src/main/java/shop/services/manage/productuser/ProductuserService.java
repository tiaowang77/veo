/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.productuser;

import shop.core.Services;
import shop.services.manage.productuser.bean.Productuser;

/**   
 * @类名称：ProductuserService      
 * @创建人：wzl
 * @创建时间：2018-06-09 下午14:44:56       
 * @版本号：1.0
 * @描述：     
 */
public interface ProductuserService extends Services<Productuser>{

}


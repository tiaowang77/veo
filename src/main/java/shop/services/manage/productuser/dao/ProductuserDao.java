/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.productuser.dao;

import shop.core.DaoManager;
import shop.services.manage.productuser.bean.Productuser;
import org.springframework.stereotype.Repository;

/**   
 * @类名称：ProductuserDao      
 * @创建人：wzl
 * @创建时间：2018-06-09 下午14:44:56        
 * @版本号：1.0
 * @描述：     
 */
public interface ProductuserDao extends DaoManager<Productuser> {

}


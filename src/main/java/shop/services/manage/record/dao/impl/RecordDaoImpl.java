/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.record.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.record.bean.Record;
import shop.services.manage.record.dao.RecordDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：RecordDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-10-13 下午16:13:14  
 * @版本号：1.0
 * @描述：
 */
@Repository("recordDaoManage")
public class RecordDaoImpl  implements RecordDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Record e) {
        return dao.selectPageList("manage.record.selectPageList","manage.record.selectPageCount", e);
    }

    public List selectList(Record e) {
        return dao.selectList("manage.record.selectList", e);
    }

    public Record selectOne(Record e) {
        return (Record) dao.selectOne("manage.record.selectOne", e);
    }

    public int delete(Record e) {
        return dao.delete("manage.record.delete", e);
    }

    public int update(Record e) {
        return dao.update("manage.record.update", e);
    }

    public int deletes(String[] ids) {
        Record e = new Record();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Record e) {
        return dao.insert("manage.record.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.record.deleteById", id);
    }

    @Override
    public Record selectById(String id) {
        return (Record) dao.selectOne("manage.record.selectById", id);
    }

}


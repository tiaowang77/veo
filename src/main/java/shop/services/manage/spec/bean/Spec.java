/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.services.manage.spec.bean;

import java.math.BigDecimal;

/**
 * @类名称：Spec后台对象类
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:51:23
 * @版本号：1.0
 * @描述：
 */
public class Spec extends shop.services.common.Spec {
    private BigDecimal profit;              //商品利润

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }
}

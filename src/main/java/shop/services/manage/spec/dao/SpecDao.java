/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.spec.dao;

import shop.core.DaoManager;
import shop.services.manage.spec.bean.Spec;

/**   
 * @类名称：SpecDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:51:23        
 * @版本号：1.0
 * @描述：     
 */
public interface SpecDao extends DaoManager<Spec> {
    int deleteByCom(String specCombination);

    Spec selectByCom(String id);
}


/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.spec.dao.impl;

import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.spec.bean.Spec;
import shop.services.manage.spec.dao.SpecDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * @类名称：SpecDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-20 下午15:51:23  
 * @版本号：1.0
 * @描述：
 */
@Repository("specDaoManage")
public class SpecDaoImpl  implements SpecDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Spec e) {
        return dao.selectPageList("manage.spec.selectPageList", "manage.spec.selectPageCount", e);
    }

    public List selectList(Spec e) {
        return dao.selectList("manage.spec.selectList", e);
    }

    public Spec selectOne(Spec e) {
        return (Spec) dao.selectOne("manage.spec.selectOne", e);
    }

    public int delete(Spec e) {
        return dao.delete("manage.spec.delete", e);
    }

    public int update(Spec e) {
        return dao.update("manage.spec.update", e);
    }

    public int deletes(String[] ids) {
        Spec e = new Spec();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Spec e) {
        return dao.insert("manage.spec.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.spec.deleteById", id);
    }

    @Override
    public Spec selectById(String id) {
        return (Spec) dao.selectOne("manage.spec.selectById", id);
    }

    /**
    * 根据规格参数删除
    * */
    public int deleteByCom(String specCombination) {
        return dao.delete("manage.spec.deleteByCom", specCombination);
    }

    /**
    * 根据规格参数获取对象
    * */
    public Spec selectByCom(String id) {
        return (Spec) dao.selectOne("manage.spec.selectByCom", id);
    }
}


package shop.services.manage.system;

import shop.core.Services;
import shop.services.manage.system.bean.User;

public interface UserInteface extends Services<User> {
	/**
	 * @param e
	 * @return
	 */
	public User login(User e);
}

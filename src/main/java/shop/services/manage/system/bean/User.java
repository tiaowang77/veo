package shop.services.manage.system.bean;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 系统用户
 */
public class User extends PagerModel {
    //主键
    private String id;
    //昵称
    private String nickname;
    //账号
    private String username;
    //密码
    private String password;
    //性别(1男、2女）
    private String sex;
    //手机
    private String phone;
    //用户设备
    private String openID;
    //用户头像
    private String icon;
    //二维码
    private String qrCode;
    //二维码参数
    private String sceneStr;
    //父级ID
    private String pid;
    //角色类型（1超级管理员、2线下合伙人、3普通用户）
    private String rid;
    //状态（y禁用、n启用）
    private String status;
    //用户钱包
    private BigDecimal walletMoney;
    //合伙人返利占订单金额百分比
    private BigDecimal percent;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private Integer updateUser;



    //序号（查询用）
    private String number;
    //父级昵称（查询用）
    private String parentname;

    private String role_dbPrivilege;

    private String role_name;

    private Map<String, String> dbPrivilegeMap;// 用户数据库权限

    public static final String user_status_y = "y";// 启用
    public static final String user_status_n = "n";// 禁用

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return this.sex;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setOpenID(String openID) {
        this.openID = openID;
    }

    public String getOpenID() {
        return this.openID;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrCode() {
        return this.qrCode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getRid() {
        return this.rid;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getUpdateUser() {
        return this.updateUser;
    }

    public String getRole_dbPrivilege() {
        return role_dbPrivilege;
    }

    public void setRole_dbPrivilege(String role_dbPrivilege) {
        this.role_dbPrivilege = role_dbPrivilege;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public Map<String, String> getDbPrivilegeMap() {
        return dbPrivilegeMap;
    }

    public void setDbPrivilegeMap(Map<String, String> dbPrivilegeMap) {
        this.dbPrivilegeMap = dbPrivilegeMap;
    }

    public static String getUser_status_y() {
        return user_status_y;
    }

    public static String getUser_status_n() {
        return user_status_n;
    }

    public String getSceneStr() {
        return sceneStr;
    }

    public void setSceneStr(String sceneStr) {
        this.sceneStr = sceneStr;
    }

    public BigDecimal getWalletMoney() {
        return walletMoney;
    }

    public void setWalletMoney(BigDecimal walletMoney) {
        this.walletMoney = walletMoney;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }
}

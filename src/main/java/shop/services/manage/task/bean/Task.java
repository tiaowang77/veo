package shop.services.manage.task.bean;

import shop.core.common.dao.page.PagerModel;

import java.io.Serializable;

/**
 * 任务Bean
 *
 * @author 草原狼
 * @date 2017-3-3
 */
public class Task extends PagerModel implements Serializable {
    //任务ID，默认系统时间戳
    private String id;
    //父级任务ID
    private String parentId ;
    //任务名称
    private String name;
    //任务描述
    private String nameDesc;
    //计划执行次数,默认0,表示满足条件循环执行次数
    private Integer planExe;
    //任务组名称,约定为一个类全名
    private String groupName;
    //任务组描述
    private String groupDesc;
    //任务表达式
    private String cron;
    //任务表达式
    private String cronDesc;
    //触发器，约定为一个方法名，格式com.dongnao.Xdd.methdo
    private String triggerName;
    //触发器组名
    private String triggerGrop;
    //触发器组描述
    private String triggerGropDesc;
    //任务被执行过多少次
    private Integer execute;
    //最后一次开始执行时间
    private Integer lastExeTime;
    //最后一次执行完成时间
    private Integer lastFinishTime;
    //任务状态0禁用，1启动，2删除
    private Integer state;
    //延迟启动，默认0,表示不延迟启动
    private Integer deply;
    public void clear() {
        this.id=null;
        this.parentId=null;
        this.name=null;
        this.nameDesc=null;
        this.planExe=0;
        this.groupName=null;
        this.groupDesc=null;
        this.cron=null;
        this.cronDesc=null;
        this.triggerName=null;
        this.triggerGrop =null;
        this.triggerGropDesc= null;
        this.execute=0;
        this.lastExeTime=0;
        this.lastFinishTime=0;
        this.state=0;
        this.deply=0;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDesc() {
        return nameDesc;
    }

    public void setNameDesc(String nameDesc) {
        this.nameDesc = nameDesc;
    }

    public Integer getPlanExe() {
        return planExe;
    }

    public void setPlanExe(Integer planExe) {
        this.planExe = planExe;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getCronDesc() {
        return cronDesc;
    }

    public void setCronDesc(String cronDesc) {
        this.cronDesc = cronDesc;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getTriggerGrop() {
        return triggerGrop;
    }

    public void setTriggerGrop(String triggerGrop) {
        this.triggerGrop = triggerGrop;
    }

    public String getTriggerGropDesc() {
        return triggerGropDesc;
    }

    public void setTriggerGropDesc(String triggerGropDesc) {
        this.triggerGropDesc = triggerGropDesc;
    }

    public Integer getExecute() {
        return execute;
    }

    public void setExecute(Integer execute) {
        this.execute = execute;
    }

    public Integer getLastExeTime() {
        return lastExeTime;
    }

    public void setLastExeTime(Integer lastExeTime) {
        this.lastExeTime = lastExeTime;
    }

    public Integer getLastFinishTime() {
        return lastFinishTime;
    }

    public void setLastFinishTime(Integer lastFinishTime) {
        this.lastFinishTime = lastFinishTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getDeply() {
        return deply;
    }

    public void setDeply(Integer deply) {
        this.deply = deply;
    }
}

/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.wallet.bean;

/**
* @类名称：Wallet后台对象类
* @创建人：Ltz
* @创建时间：2017-09-20 下午15:55:25 
* @版本号：1.0
* @描述：
*/
public class Wallet extends shop.services.common.Wallet {
    //开始日期
    private String startDate;
    //结束日期
    private String endDate;
    //返利总金额
    private String totalMoney;
    //已提现金额
    private String passMoney;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getPassMoney() {
        return passMoney;
    }

    public void setPassMoney(String passMoney) {
        this.passMoney = passMoney;
    }
}

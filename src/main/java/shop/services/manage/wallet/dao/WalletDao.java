/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.wallet.dao;

import shop.core.DaoManager;
import shop.services.manage.wallet.bean.Wallet;
import org.springframework.stereotype.Repository;

/**   
 * @类名称：WalletDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:55:25        
 * @版本号：1.0
 * @描述：     
 */
public interface WalletDao extends DaoManager<Wallet> {
    public Wallet totalMoney();
}


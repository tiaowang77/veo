/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.wallet.impl;

import shop.core.ServicesManager;
import shop.services.manage.wallet.WalletService;
import shop.services.manage.wallet.bean.Wallet;
import shop.services.manage.wallet.dao.WalletDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：WalletServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-20 下午15:55:25        
 * @版本号：1.0
 * @描述：     
 */
@Service("walletServiceManage")
public class WalletServiceImpl extends ServicesManager<Wallet, WalletDao> implements
WalletService {
    @Resource(name = "walletDaoManage")
    @Override
    public void setDao(WalletDao walletDao) {
    this.dao = walletDao;
    }

    @Override
    public Wallet totalMoney() {
        return dao.totalMoney();
    }
}


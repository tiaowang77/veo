/*
Navicat MySQL Data Transfer

Source Server         : xsveo
Source Server Version : 50635
Source Host           : 39.106.21.43:3306
Source Database       : veo

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-06-19 16:54:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_address
-- ----------------------------
DROP TABLE IF EXISTS `t_address`;
CREATE TABLE `t_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `userID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户ID',
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名',
  `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '电话',
  `isdefault` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'n' COMMENT '是否默认（y 是  n 否）',
  `province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '城市',
  `area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地区',
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `houseNumber` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '门牌号',
  `createTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户地址表';

-- ----------------------------
-- Records of t_address
-- ----------------------------
INSERT INTO `t_address` VALUES ('215', '237', '林添珍', '18650199186', 'n', null, null, '福建 厦门 思明区', '软件园二期观日路32号', '网宿科技', '2017-12-23 15:19:23', null, '2018-03-30 10:32:19', null);
INSERT INTO `t_address` VALUES ('231', '270', '怒不会', '18850458924', 'y', null, null, '陕西 渭南 潼关县', '不会v看', '：不吃v）给你们', '2018-01-07 14:11:41', null, null, null);
INSERT INTO `t_address` VALUES ('234', '274', '抖动', '13646464646', 'y', null, null, '北京 东城区 ', '纠结啊', '112', '2018-03-19 15:10:28', null, null, null);
INSERT INTO `t_address` VALUES ('235', '296', '汤文镇', '18950281173', 'y', null, null, '福建 厦门 思明区', '软件园观日路32号网宿科技', '观日路32号', '2018-03-23 13:58:13', null, null, null);
INSERT INTO `t_address` VALUES ('236', '268', '林志伟', '15980808080', 'y', null, null, '北京 东城区 ', '宴上', '515', '2018-03-23 14:21:27', null, null, null);
INSERT INTO `t_address` VALUES ('237', '280', '林志伟', '15980808080', 'y', null, null, '北京 东城区 ', '头有些', '505', '2018-03-23 14:55:41', null, null, null);
INSERT INTO `t_address` VALUES ('238', '297', '吴志林', '15711593316', 'y', null, null, '福建 厦门 思明区', '何厝69号', '333', '2018-03-23 15:18:48', null, null, null);
INSERT INTO `t_address` VALUES ('239', '277', 'ygc', '15059533680', 'y', null, null, '天津 红桥区 ', '易烊千玺', '商务号', '2018-03-23 16:39:49', null, null, null);
INSERT INTO `t_address` VALUES ('240', '300', '林美慧', '18659289253', 'y', null, null, '福建 厦门 思明区', '观音山宝业大厦网宿科技10楼', ' ', '2018-03-27 12:44:10', null, '2018-03-27 12:48:56', null);
INSERT INTO `t_address` VALUES ('241', '302', '王福彬', '18605029950', 'y', null, null, '福建 厦门 思明区', '台东路66号宝业大厦', '9楼网宿科技', '2018-03-27 12:51:01', null, null, null);
INSERT INTO `t_address` VALUES ('242', '305', '林万维', '18750942127', 'y', null, null, '福建 厦门 翔安区', '明发半岛祥湾b区', '34号楼2003室', '2018-03-27 13:32:25', null, null, null);
INSERT INTO `t_address` VALUES ('243', '308', '康晓欢', '18559262272', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路44号楼10层', '44号楼10层', '2018-03-27 13:42:27', null, null, null);
INSERT INTO `t_address` VALUES ('244', '318', '钟九爷', '15960842422', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号宝业大厦', '11楼网宿科技', '2018-03-27 13:55:35', null, null, null);
INSERT INTO `t_address` VALUES ('245', '316', '蔡沐云', '13616532453', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路', '32号楼2楼网宿科技', '2018-03-27 13:57:46', null, null, null);
INSERT INTO `t_address` VALUES ('246', '319', '王鸿彦', '18538171659', 'y', null, null, '福建 厦门 集美区', '中海锦城国际', '8栋', '2018-03-27 13:58:48', null, null, null);
INSERT INTO `t_address` VALUES ('247', '320', '沈云', '18973166662', 'y', null, null, '福建 厦门 集美区', '中航城C区', '21号楼2101室', '2018-03-27 14:03:32', null, null, null);
INSERT INTO `t_address` VALUES ('248', '321', '纪智鹏', '18396533736', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路32号', '20301网宿科技', '2018-03-27 14:09:26', null, null, null);
INSERT INTO `t_address` VALUES ('249', '326', '陈茵蓉', '13163983702', 'n', null, null, '福建 厦门 思明区', '软件园二期', '观日路32号之一2楼网宿科技', '2018-03-27 14:14:37', null, '2018-03-27 14:26:04', null);
INSERT INTO `t_address` VALUES ('250', '325', '陈雨宁', '18060915194', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号宝业运营中心', '9楼网宿科技', '2018-03-27 14:15:41', null, '2018-03-27 14:16:53', null);
INSERT INTO `t_address` VALUES ('251', '325', '陈雨宁', '18060915194', 'n', null, null, '福建 厦门 思明区', '观音山台东路66号宝业运营中心', '9楼网宿科技', '2018-03-27 14:16:18', null, '2018-03-27 14:16:49', null);
INSERT INTO `t_address` VALUES ('252', '327', '宋先生', '18650160988', 'y', null, null, '福建 厦门 思明区', '莲前西路2号莲富大厦公寓楼', '23层G室', '2018-03-27 14:17:00', null, null, null);
INSERT INTO `t_address` VALUES ('253', '326', '陈茵蓉', '13163983702', 'y', null, null, '福建 厦门 思明区', '软件园二期', '观日路44号十楼网宿科技', '2018-03-27 14:17:53', null, '2018-03-27 14:26:04', null);
INSERT INTO `t_address` VALUES ('254', '329', '洪少凯', '18965652727', 'y', null, null, '福建 厦门 思明区', '软件园二期32号网宿科技', '20301', '2018-03-27 14:18:50', null, null, null);
INSERT INTO `t_address` VALUES ('255', '328', '朱志海', '13400738025', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路32号楼网宿科技', '20301', '2018-03-27 14:30:47', null, null, null);
INSERT INTO `t_address` VALUES ('256', '332', '陈丽梅', '15980687260', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路', '32号楼20301', '2018-03-27 15:10:19', null, null, null);
INSERT INTO `t_address` VALUES ('257', '335', '吴姗姗', '18250790987', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路32号', '20301网宿科技', '2018-03-27 15:48:25', null, null, null);
INSERT INTO `t_address` VALUES ('258', '334', '梁焘', '15060779105', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号宝业大厦', '9楼', '2018-03-27 15:57:27', null, null, null);
INSERT INTO `t_address` VALUES ('259', '336', '杨静', '18205969193', 'y', null, null, '福建 厦门 思明区', '观音山营运中心台东路66号', '宝业大厦9-11楼', '2018-03-27 16:27:52', null, null, null);
INSERT INTO `t_address` VALUES ('260', '338', '许碧娟', '18659219801', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号', '宝业管理中心网宿科技有限公司', '2018-03-27 16:50:02', null, null, null);
INSERT INTO `t_address` VALUES ('261', '339', '郭爱玲', '15159257940', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路32号之二20301网宿科技', '20301', '2018-03-27 16:54:19', null, null, null);
INSERT INTO `t_address` VALUES ('262', '314', '张敏蓉', '18059277967', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号宝业大厦', '9楼-网宿科技', '2018-03-27 17:12:05', null, null, null);
INSERT INTO `t_address` VALUES ('263', '341', '邱锦鹏', '13159239633', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号宝业大厦9楼网宿科技', '宝业大厦9楼网宿科技', '2018-03-27 17:46:10', null, null, null);
INSERT INTO `t_address` VALUES ('264', '342', '陈秋霞', '18859241975', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号宝业运营中心', '11楼网宿科技', '2018-03-27 17:55:29', null, null, null);
INSERT INTO `t_address` VALUES ('265', '343', '江华忠', '13860469009', 'y', null, null, '福建 厦门 思明区', '软件园二期', '观日路32号', '2018-03-27 19:04:59', null, null, null);
INSERT INTO `t_address` VALUES ('266', '298', '小芳（彩虹朋友（）', '13960607758', 'y', null, null, '福建 厦门 思明区', '会展附近金海豪园89号（金海豪园别墅', '89号', '2018-03-28 20:54:19', null, '2018-03-30 09:56:28', null);
INSERT INTO `t_address` VALUES ('267', '362', '王惠明', '18650119363', 'y', null, null, '福建 厦门 思明区', '台东路66号', '宝业大厦10楼', '2018-03-29 14:17:19', null, null, null);
INSERT INTO `t_address` VALUES ('268', '361', '刘海健', '15960204783', 'y', null, null, '福建 厦门 同安区', '中海万锦熙岸', '9#2201', '2018-03-29 15:08:11', null, null, null);
INSERT INTO `t_address` VALUES ('269', '364', 'Tina Li', '13358389099', 'y', null, null, '福建 厦门 思明区', '软件园二期', '望海路45号5楼', '2018-03-29 15:49:42', null, null, null);
INSERT INTO `t_address` VALUES ('270', '347', '黄冰冰', '13225992997', 'y', null, null, '福建 厦门 思明区', '观音山宝业大厦', '11楼', '2018-03-29 16:15:07', null, null, null);
INSERT INTO `t_address` VALUES ('271', '371', '王开鹏', '15160034126', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路32号', '网宿科技', '2018-03-29 16:24:43', null, null, null);
INSERT INTO `t_address` VALUES ('272', '346', '林声隆', '13696952284', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路44号10楼网宿科技', '44号楼10楼', '2018-03-29 16:34:41', null, null, null);
INSERT INTO `t_address` VALUES ('273', '373', '黄美玲', '15705924414', 'y', null, null, '福建 厦门 思明区', '软件园二期 观日路32号 网宿科技', '二楼', '2018-03-29 16:36:56', null, null, null);
INSERT INTO `t_address` VALUES ('274', '365', '谢美莲', '18459220955', 'y', null, null, '福建 厦门 思明区', '软件园二期观日路32#网宿科技', '观日路32号', '2018-03-29 16:37:04', null, null, null);
INSERT INTO `t_address` VALUES ('275', '374', '丘琴秀', '18805921319', 'y', null, null, '福建 厦门 思明区', '莲前街道台东路66号宝业大厦', '10楼', '2018-03-29 16:37:35', null, null, null);
INSERT INTO `t_address` VALUES ('276', '310', '洪艺琳', '15960846679', 'y', null, null, '福建 厦门 思明区', '台东路66号宝业投资大厦', '9楼', '2018-03-29 17:02:19', null, null, null);
INSERT INTO `t_address` VALUES ('277', '370', '叶东明', '13806048530', 'y', null, null, '福建 厦门 思明区', '软件园二期', '观日路44号10楼', '2018-03-29 17:16:02', null, null, null);
INSERT INTO `t_address` VALUES ('278', '375', '陈艳华', '18650019921', 'y', null, null, '福建 厦门 思明区', '软件园二期', '望海路47号602', '2018-03-29 17:21:17', null, null, null);
INSERT INTO `t_address` VALUES ('279', '344', '陈丽娟', '13720881485', 'y', null, null, '福建 厦门 思明区', '观音山台东路', '66号宝业大厦9层网宿科技', '2018-03-29 23:13:53', null, null, null);
INSERT INTO `t_address` VALUES ('280', '368', '俞翔', '13606068257', 'y', null, null, '福建 厦门 思明区', '古楼北里', '50-601', '2018-03-30 01:21:00', null, null, null);
INSERT INTO `t_address` VALUES ('281', '386', '郑小龙', '18059218804', 'y', null, null, '福建 厦门 思明区', '龙虎山路', '384号101 御上茗', '2018-03-30 09:43:39', null, null, null);
INSERT INTO `t_address` VALUES ('282', '237', '林添珍', '18650199186', 'y', null, null, '福建 厦门 思明区', '软件园二期', '观日路22号303-6', '2018-03-30 10:32:17', null, '2018-03-30 10:32:25', null);
INSERT INTO `t_address` VALUES ('283', '388', '林榕芳', '15959259965', 'y', null, null, '福建 厦门 思明区', '文兴东二里21号', '21号101', '2018-03-30 10:49:57', null, null, null);
INSERT INTO `t_address` VALUES ('284', '353', '黄金霖', '18950116891', 'y', null, null, '福建 厦门 思明区', '软件园二期', '59#网宿科技', '2018-03-30 11:55:20', null, null, null);
INSERT INTO `t_address` VALUES ('285', '390', '黄燕芳', '15985824011', 'y', null, null, '福建 厦门 思明区', '观音山台东路66号9楼', '9楼', '2018-03-30 15:58:40', null, null, null);
INSERT INTO `t_address` VALUES ('286', '403', '吕添灵', '13950823618', 'y', null, null, '福建 龙岩 漳平市', '菁城镇菁城街道武馆路81号', '武馆路81号', '2018-04-05 21:16:55', null, null, null);
INSERT INTO `t_address` VALUES ('287', '407', '哈哈哈', '13646014530', 'y', null, null, '山西 运城 盐湖区', '嘿嘿嘿', '1234', '2018-06-02 21:34:38', null, null, null);

-- ----------------------------
-- Table structure for t_brokerage
-- ----------------------------
DROP TABLE IF EXISTS `t_brokerage`;
CREATE TABLE `t_brokerage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `userID` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `orderID` varchar(255) DEFAULT NULL COMMENT '订单ID',
  `profit` decimal(11,2) DEFAULT NULL COMMENT '利润',
  `affID` varchar(255) DEFAULT NULL COMMENT '获利人ID',
  `money` decimal(11,2) DEFAULT NULL COMMENT '获得返利金额',
  `percent` decimal(11,2) DEFAULT NULL COMMENT '返佣金额占利润比',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品返利表';

-- ----------------------------
-- Records of t_brokerage
-- ----------------------------

-- ----------------------------
-- Table structure for t_cart
-- ----------------------------
DROP TABLE IF EXISTS `t_cart`;
CREATE TABLE `t_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `userID` varchar(11) DEFAULT NULL COMMENT '用户ID',
  `productID` varchar(11) DEFAULT NULL COMMENT '商品ID',
  `cartNum` int(11) DEFAULT '0' COMMENT '商品数量',
  `specID` varchar(255) DEFAULT NULL COMMENT '规格ID',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户购物车表';

-- ----------------------------
-- Records of t_cart
-- ----------------------------

-- ----------------------------
-- Table structure for t_catalog
-- ----------------------------
DROP TABLE IF EXISTS `t_catalog`;
CREATE TABLE `t_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `order1` int(11) DEFAULT NULL COMMENT '自定义排序',
  `showInNav` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'n' COMMENT '是否在导航栏中显示（n 不显示  y 显示 ）',
  `icon` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品目录图标',
  `createTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='商品类别表';

-- ----------------------------
-- Records of t_catalog
-- ----------------------------
INSERT INTO `t_catalog` VALUES ('136', '蔬菜', '1', 'y', 'ueditor/upload/image/20170923/1506149858947053560.png', '2017-09-24 20:43:28', null, '2017-09-24 20:43:28', null);
INSERT INTO `t_catalog` VALUES ('138', '生鲜', '3', 'y', 'ueditor/upload/image/20170922/1506059827312086031.png', '2017-09-22 13:57:10', null, null, null);
INSERT INTO `t_catalog` VALUES ('139', '粮油', '4', 'y', 'ueditor/upload/image/20170922/1506059845961004644.png', '2017-09-22 13:57:30', null, null, null);
INSERT INTO `t_catalog` VALUES ('140', '调料', '5', 'y', 'ueditor/upload/image/20170922/1506059872237046453.png', '2017-09-22 13:58:29', null, null, null);
INSERT INTO `t_catalog` VALUES ('141', '肉类', '6', 'y', 'ueditor/upload/image/20170922/1506059894569099491.png', '2017-09-22 13:58:33', null, null, null);
INSERT INTO `t_catalog` VALUES ('142', '水果', '7', 'y', 'ueditor/upload/image/20170922/1506059932982043342.png', '2017-09-22 13:58:58', null, null, null);
INSERT INTO `t_catalog` VALUES ('143', '农庄', '2', 'y', 'ueditor/upload/image/20170924/1506256077991095085.png', '2017-09-24 20:43:22', null, '2017-09-24 20:43:22', null);

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userID` varchar(11) DEFAULT NULL COMMENT '用户ID',
  `oID` varchar(11) DEFAULT NULL COMMENT '订单详情ID',
  `content` varchar(1000) DEFAULT NULL COMMENT '评论内容',
  `picture` varchar(1000) DEFAULT NULL COMMENT '图片（多个用逗号隔开）',
  `isHide` varchar(3) DEFAULT NULL COMMENT '是否匿名（n否、y是）',
  `rank` varchar(255) DEFAULT '好评' COMMENT '评价等级(好评、中评、差评)',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户评论表';

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment` VALUES ('1', '297', '1064', '还不错', 'ueditor/upload/image/20180324/1521864041831013037.jpg', 'y', '好评', '2018-03-24 12:00:46', null, null, null);
INSERT INTO `t_comment` VALUES ('2', '270', '1065', '大女婿都不记得', 'ueditor/upload/image/20180324/1521864283227045119.jpg', 'y', '好评', '2018-03-24 12:04:47', null, null, null);
INSERT INTO `t_comment` VALUES ('3', '237', '1035', '甜', null, 'y', '好评', '2018-03-24 14:23:48', null, null, null);
INSERT INTO `t_comment` VALUES ('4', '237', '1035', '甜', null, 'y', '好评', '2018-03-24 14:23:49', null, null, null);

-- ----------------------------
-- Table structure for t_favorite
-- ----------------------------
DROP TABLE IF EXISTS `t_favorite`;
CREATE TABLE `t_favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userID` varchar(11) DEFAULT NULL COMMENT '用户ID',
  `productID` varchar(11) DEFAULT NULL COMMENT '商品ID',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COMMENT='用户收藏表';

-- ----------------------------
-- Records of t_favorite
-- ----------------------------
INSERT INTO `t_favorite` VALUES ('113', '326', '10502', '2018-03-27 14:23:02', null, null, null);
INSERT INTO `t_favorite` VALUES ('114', '341', '10502', '2018-03-27 17:46:53', null, null, null);
INSERT INTO `t_favorite` VALUES ('116', '312', '10502', '2018-03-29 13:48:55', null, null, null);
INSERT INTO `t_favorite` VALUES ('117', '377', '10502', '2018-03-29 18:13:22', null, null, null);
INSERT INTO `t_favorite` VALUES ('118', '402', '10502', '2018-04-03 12:42:53', null, null, null);

-- ----------------------------
-- Table structure for t_foot
-- ----------------------------
DROP TABLE IF EXISTS `t_foot`;
CREATE TABLE `t_foot` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userID` varchar(11) DEFAULT NULL COMMENT '用户ID',
  `productID` varchar(11) DEFAULT NULL COMMENT '商品ID',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=814 DEFAULT CHARSET=utf8 COMMENT='用户足迹表';

-- ----------------------------
-- Records of t_foot
-- ----------------------------
INSERT INTO `t_foot` VALUES ('564', '237', '10440', '2017-12-23 15:19:53', null, null, null);
INSERT INTO `t_foot` VALUES ('575', '237', '10457', '2017-12-23 21:17:52', null, null, null);
INSERT INTO `t_foot` VALUES ('577', '237', '10456', '2017-12-23 21:18:52', null, null, null);
INSERT INTO `t_foot` VALUES ('609', '237', '10461', '2017-12-25 10:53:27', null, null, null);
INSERT INTO `t_foot` VALUES ('620', '237', '10464', '2017-12-26 10:48:35', null, null, null);
INSERT INTO `t_foot` VALUES ('627', '270', '10461', '2018-01-15 19:32:18', null, null, null);
INSERT INTO `t_foot` VALUES ('628', '270', '10466', '2018-01-15 19:48:57', null, null, null);
INSERT INTO `t_foot` VALUES ('629', '274', '10466', '2018-01-15 20:16:34', null, null, null);
INSERT INTO `t_foot` VALUES ('631', '274', '10488', '2018-01-16 23:19:25', null, null, null);
INSERT INTO `t_foot` VALUES ('633', '270', '10488', '2018-01-17 09:53:22', null, null, null);
INSERT INTO `t_foot` VALUES ('634', '270', '10489', '2018-01-17 09:54:34', null, null, null);
INSERT INTO `t_foot` VALUES ('635', '270', '10490', '2018-01-17 09:54:41', null, null, null);
INSERT INTO `t_foot` VALUES ('636', '270', '10494', '2018-01-17 09:54:52', null, null, null);
INSERT INTO `t_foot` VALUES ('637', '274', '10489', '2018-01-17 11:14:26', null, null, null);
INSERT INTO `t_foot` VALUES ('640', '285', '10488', '2018-01-17 12:17:33', null, null, null);
INSERT INTO `t_foot` VALUES ('641', '274', '10493', '2018-01-17 12:19:42', null, null, null);
INSERT INTO `t_foot` VALUES ('643', '284', '10488', '2018-01-18 07:21:10', null, null, null);
INSERT INTO `t_foot` VALUES ('644', '237', '10494', '2018-01-18 09:12:16', null, null, null);
INSERT INTO `t_foot` VALUES ('645', '237', '10489', '2018-01-18 16:29:04', null, null, null);
INSERT INTO `t_foot` VALUES ('646', '288', '10493', '2018-01-19 11:43:25', null, null, null);
INSERT INTO `t_foot` VALUES ('647', '288', '10488', '2018-01-19 11:43:51', null, null, null);
INSERT INTO `t_foot` VALUES ('648', '288', '10490', '2018-01-19 11:59:42', null, null, null);
INSERT INTO `t_foot` VALUES ('649', '274', '10490', '2018-01-19 12:00:58', null, null, null);
INSERT INTO `t_foot` VALUES ('650', '274', '10491', '2018-01-19 12:01:09', null, null, null);
INSERT INTO `t_foot` VALUES ('652', '288', '10489', '2018-01-20 11:47:39', null, null, null);
INSERT INTO `t_foot` VALUES ('653', '237', '10488', '2018-01-22 14:33:55', null, null, null);
INSERT INTO `t_foot` VALUES ('654', '272', '10488', '2018-01-26 17:11:45', null, null, null);
INSERT INTO `t_foot` VALUES ('655', '237', '10493', '2018-01-27 13:27:24', null, null, null);
INSERT INTO `t_foot` VALUES ('658', '280', '10488', '2018-03-19 12:37:00', null, null, null);
INSERT INTO `t_foot` VALUES ('659', '274', '10502', '2018-03-19 15:09:27', null, null, null);
INSERT INTO `t_foot` VALUES ('660', '292', '10502', '2018-03-19 15:53:41', null, null, null);
INSERT INTO `t_foot` VALUES ('661', '293', '10502', '2018-03-19 16:22:58', null, null, null);
INSERT INTO `t_foot` VALUES ('662', '237', '10502', '2018-03-21 16:09:15', null, null, null);
INSERT INTO `t_foot` VALUES ('664', '274', '10504', '2018-03-23 09:48:31', null, null, null);
INSERT INTO `t_foot` VALUES ('665', '295', '10502', '2018-03-23 13:54:11', null, null, null);
INSERT INTO `t_foot` VALUES ('666', '296', '10502', '2018-03-23 13:56:24', null, null, null);
INSERT INTO `t_foot` VALUES ('668', '268', '10507', '2018-03-23 14:18:18', null, null, null);
INSERT INTO `t_foot` VALUES ('669', '268', '10502', '2018-03-23 14:18:34', null, null, null);
INSERT INTO `t_foot` VALUES ('670', '268', '10503', '2018-03-23 14:18:50', null, null, null);
INSERT INTO `t_foot` VALUES ('671', '274', '10507', '2018-03-23 14:38:07', null, null, null);
INSERT INTO `t_foot` VALUES ('672', '290', '10503', '2018-03-23 14:48:40', null, null, null);
INSERT INTO `t_foot` VALUES ('673', '280', '10502', '2018-03-23 15:02:27', null, null, null);
INSERT INTO `t_foot` VALUES ('675', '268', '10509', '2018-03-23 15:08:02', null, null, null);
INSERT INTO `t_foot` VALUES ('676', '297', '10509', '2018-03-23 15:17:31', null, null, null);
INSERT INTO `t_foot` VALUES ('677', '280', '10509', '2018-03-23 15:30:11', null, null, null);
INSERT INTO `t_foot` VALUES ('678', '274', '10509', '2018-03-23 15:33:46', null, null, null);
INSERT INTO `t_foot` VALUES ('679', '270', '10502', '2018-03-23 15:37:22', null, null, null);
INSERT INTO `t_foot` VALUES ('680', '297', '10502', '2018-03-23 15:37:59', null, null, null);
INSERT INTO `t_foot` VALUES ('681', '297', '10490', '2018-03-23 15:48:36', null, null, null);
INSERT INTO `t_foot` VALUES ('682', '297', '10510', '2018-03-23 15:51:15', null, null, null);
INSERT INTO `t_foot` VALUES ('683', '270', '10510', '2018-03-23 16:00:05', null, null, null);
INSERT INTO `t_foot` VALUES ('684', '274', '10512', '2018-03-23 16:09:23', null, null, null);
INSERT INTO `t_foot` VALUES ('685', '274', '10510', '2018-03-23 16:15:20', null, null, null);
INSERT INTO `t_foot` VALUES ('686', '295', '10512', '2018-03-23 16:18:22', null, null, null);
INSERT INTO `t_foot` VALUES ('687', '277', '10502', '2018-03-23 16:38:40', null, null, null);
INSERT INTO `t_foot` VALUES ('688', '277', '10510', '2018-03-23 16:38:48', null, null, null);
INSERT INTO `t_foot` VALUES ('689', '277', '10506', '2018-03-23 16:41:44', null, null, null);
INSERT INTO `t_foot` VALUES ('690', '277', '10512', '2018-03-23 16:41:53', null, null, null);
INSERT INTO `t_foot` VALUES ('691', '274', '10506', '2018-03-23 16:53:25', null, null, null);
INSERT INTO `t_foot` VALUES ('692', '297', '10506', '2018-03-24 11:27:36', null, null, null);
INSERT INTO `t_foot` VALUES ('693', '298', '10506', '2018-03-24 14:26:05', null, null, null);
INSERT INTO `t_foot` VALUES ('694', '298', '10502', '2018-03-24 14:26:13', null, null, null);
INSERT INTO `t_foot` VALUES ('695', '274', '10503', '2018-03-24 16:20:23', null, null, null);
INSERT INTO `t_foot` VALUES ('696', '301', '10502', '2018-03-27 12:46:26', null, null, null);
INSERT INTO `t_foot` VALUES ('697', '280', '10514', '2018-03-27 12:48:08', null, null, null);
INSERT INTO `t_foot` VALUES ('698', '300', '10502', '2018-03-27 12:50:54', null, null, null);
INSERT INTO `t_foot` VALUES ('699', '302', '10502', '2018-03-27 12:51:20', null, null, null);
INSERT INTO `t_foot` VALUES ('700', '303', '10502', '2018-03-27 12:53:54', null, null, null);
INSERT INTO `t_foot` VALUES ('701', '308', '10502', '2018-03-27 13:42:39', null, null, null);
INSERT INTO `t_foot` VALUES ('702', '311', '10515', '2018-03-27 13:43:50', null, null, null);
INSERT INTO `t_foot` VALUES ('703', '314', '10502', '2018-03-27 13:48:27', null, null, null);
INSERT INTO `t_foot` VALUES ('704', '315', '10502', '2018-03-27 13:48:57', null, null, null);
INSERT INTO `t_foot` VALUES ('705', '274', '10513', '2018-03-27 13:53:02', null, null, null);
INSERT INTO `t_foot` VALUES ('706', '317', '10513', '2018-03-27 13:54:28', null, null, null);
INSERT INTO `t_foot` VALUES ('707', '317', '10502', '2018-03-27 13:54:31', null, null, null);
INSERT INTO `t_foot` VALUES ('708', '318', '10502', '2018-03-27 13:56:01', null, null, null);
INSERT INTO `t_foot` VALUES ('709', '316', '10502', '2018-03-27 13:58:05', null, null, null);
INSERT INTO `t_foot` VALUES ('710', '320', '10502', '2018-03-27 14:03:41', null, null, null);
INSERT INTO `t_foot` VALUES ('711', '322', '10505', '2018-03-27 14:04:09', null, null, null);
INSERT INTO `t_foot` VALUES ('712', '322', '10502', '2018-03-27 14:04:12', null, null, null);
INSERT INTO `t_foot` VALUES ('713', '321', '10502', '2018-03-27 14:10:01', null, null, null);
INSERT INTO `t_foot` VALUES ('714', '326', '10502', '2018-03-27 14:14:48', null, null, null);
INSERT INTO `t_foot` VALUES ('715', '328', '10502', '2018-03-27 14:15:57', null, null, null);
INSERT INTO `t_foot` VALUES ('716', '327', '10502', '2018-03-27 14:17:23', null, null, null);
INSERT INTO `t_foot` VALUES ('717', '325', '10502', '2018-03-27 14:18:07', null, null, null);
INSERT INTO `t_foot` VALUES ('718', '329', '10502', '2018-03-27 14:29:59', null, null, null);
INSERT INTO `t_foot` VALUES ('719', '330', '10502', '2018-03-27 14:50:07', null, null, null);
INSERT INTO `t_foot` VALUES ('720', '331', '10502', '2018-03-27 15:09:27', null, null, null);
INSERT INTO `t_foot` VALUES ('721', '332', '10502', '2018-03-27 15:10:48', null, null, null);
INSERT INTO `t_foot` VALUES ('722', '335', '10502', '2018-03-27 15:48:48', null, null, null);
INSERT INTO `t_foot` VALUES ('723', '334', '10502', '2018-03-27 15:58:04', null, null, null);
INSERT INTO `t_foot` VALUES ('724', '337', '10503', '2018-03-27 16:27:43', null, null, null);
INSERT INTO `t_foot` VALUES ('725', '337', '10502', '2018-03-27 16:27:46', null, null, null);
INSERT INTO `t_foot` VALUES ('726', '336', '10502', '2018-03-27 16:28:08', null, null, null);
INSERT INTO `t_foot` VALUES ('727', '338', '10502', '2018-03-27 16:50:19', null, null, null);
INSERT INTO `t_foot` VALUES ('728', '339', '10502', '2018-03-27 16:54:40', null, null, null);
INSERT INTO `t_foot` VALUES ('729', '237', '10515', '2018-03-27 16:59:54', null, null, null);
INSERT INTO `t_foot` VALUES ('730', '237', '10514', '2018-03-27 17:04:10', null, null, null);
INSERT INTO `t_foot` VALUES ('731', '284', '10515', '2018-03-27 17:15:02', null, null, null);
INSERT INTO `t_foot` VALUES ('732', '284', '10502', '2018-03-27 17:15:06', null, null, null);
INSERT INTO `t_foot` VALUES ('733', '341', '10502', '2018-03-27 17:46:45', null, null, null);
INSERT INTO `t_foot` VALUES ('734', '342', '10502', '2018-03-27 17:56:31', null, null, null);
INSERT INTO `t_foot` VALUES ('735', '343', '10502', '2018-03-27 19:20:58', null, null, null);
INSERT INTO `t_foot` VALUES ('736', '305', '10505', '2018-03-27 21:50:45', null, null, null);
INSERT INTO `t_foot` VALUES ('737', '305', '10502', '2018-03-27 21:50:48', null, null, null);
INSERT INTO `t_foot` VALUES ('738', '237', '10506', '2018-03-27 22:35:44', null, null, null);
INSERT INTO `t_foot` VALUES ('739', '347', '10502', '2018-03-28 00:13:50', null, null, null);
INSERT INTO `t_foot` VALUES ('740', '339', '10505', '2018-03-28 00:39:28', null, null, null);
INSERT INTO `t_foot` VALUES ('741', '272', '10502', '2018-03-28 08:32:14', null, null, null);
INSERT INTO `t_foot` VALUES ('742', '274', '10517', '2018-03-28 20:48:35', null, null, null);
INSERT INTO `t_foot` VALUES ('743', '298', '10517', '2018-03-28 20:53:14', null, null, null);
INSERT INTO `t_foot` VALUES ('744', '274', '10514', '2018-03-28 21:27:23', null, null, null);
INSERT INTO `t_foot` VALUES ('745', '237', '10517', '2018-03-28 23:13:42', null, null, null);
INSERT INTO `t_foot` VALUES ('746', '237', '10516', '2018-03-28 23:13:52', null, null, null);
INSERT INTO `t_foot` VALUES ('747', '355', '10505', '2018-03-29 12:32:56', null, null, null);
INSERT INTO `t_foot` VALUES ('748', '356', '10502', '2018-03-29 12:36:57', null, null, null);
INSERT INTO `t_foot` VALUES ('749', '357', '10502', '2018-03-29 13:09:09', null, null, null);
INSERT INTO `t_foot` VALUES ('750', '359', '10502', '2018-03-29 13:49:55', null, null, null);
INSERT INTO `t_foot` VALUES ('751', '312', '10505', '2018-03-29 13:51:12', null, null, null);
INSERT INTO `t_foot` VALUES ('752', '362', '10502', '2018-03-29 14:13:00', null, null, null);
INSERT INTO `t_foot` VALUES ('753', '361', '10502', '2018-03-29 15:08:26', null, null, null);
INSERT INTO `t_foot` VALUES ('754', '367', '10505', '2018-03-29 15:42:53', null, null, null);
INSERT INTO `t_foot` VALUES ('755', '367', '10502', '2018-03-29 15:42:56', null, null, null);
INSERT INTO `t_foot` VALUES ('756', '364', '10502', '2018-03-29 15:50:15', null, null, null);
INSERT INTO `t_foot` VALUES ('757', '371', '10502', '2018-03-29 16:26:23', null, null, null);
INSERT INTO `t_foot` VALUES ('758', '346', '10502', '2018-03-29 16:35:05', null, null, null);
INSERT INTO `t_foot` VALUES ('759', '365', '10502', '2018-03-29 16:37:36', null, null, null);
INSERT INTO `t_foot` VALUES ('760', '373', '10502', '2018-03-29 16:37:57', null, null, null);
INSERT INTO `t_foot` VALUES ('761', '374', '10502', '2018-03-29 16:38:13', null, null, null);
INSERT INTO `t_foot` VALUES ('762', '310', '10502', '2018-03-29 17:02:31', null, null, null);
INSERT INTO `t_foot` VALUES ('763', '370', '10502', '2018-03-29 17:16:36', null, null, null);
INSERT INTO `t_foot` VALUES ('764', '375', '10502', '2018-03-29 17:20:22', null, null, null);
INSERT INTO `t_foot` VALUES ('765', '376', '10502', '2018-03-29 17:30:38', null, null, null);
INSERT INTO `t_foot` VALUES ('766', '377', '10502', '2018-03-29 18:13:52', null, null, null);
INSERT INTO `t_foot` VALUES ('767', '274', '10515', '2018-03-29 20:16:08', null, null, null);
INSERT INTO `t_foot` VALUES ('769', '344', '10502', '2018-03-29 23:14:56', null, null, null);
INSERT INTO `t_foot` VALUES ('770', '368', '10502', '2018-03-30 01:21:45', null, null, null);
INSERT INTO `t_foot` VALUES ('771', '382', '10502', '2018-03-30 09:14:22', null, null, null);
INSERT INTO `t_foot` VALUES ('772', '269', '10502', '2018-03-30 09:40:43', null, null, null);
INSERT INTO `t_foot` VALUES ('773', '386', '10502', '2018-03-30 09:43:52', null, null, null);
INSERT INTO `t_foot` VALUES ('774', '388', '10502', '2018-03-30 10:50:14', null, null, null);
INSERT INTO `t_foot` VALUES ('775', '284', '10503', '2018-03-30 10:54:37', null, null, null);
INSERT INTO `t_foot` VALUES ('776', '353', '10502', '2018-03-30 11:55:46', null, null, null);
INSERT INTO `t_foot` VALUES ('777', '390', '10502', '2018-03-30 15:59:09', null, null, null);
INSERT INTO `t_foot` VALUES ('778', '353', '10513', '2018-03-30 16:02:46', null, null, null);
INSERT INTO `t_foot` VALUES ('779', '272', '10505', '2018-03-30 22:33:38', null, null, null);
INSERT INTO `t_foot` VALUES ('780', '272', '10512', '2018-03-31 06:36:41', null, null, null);
INSERT INTO `t_foot` VALUES ('781', '284', '10506', '2018-03-31 08:34:40', null, null, null);
INSERT INTO `t_foot` VALUES ('782', '298', '10512', '2018-03-31 15:10:09', null, null, null);
INSERT INTO `t_foot` VALUES ('783', '288', '10502', '2018-04-01 11:43:17', null, null, null);
INSERT INTO `t_foot` VALUES ('784', '274', '10505', '2018-04-01 11:43:42', null, null, null);
INSERT INTO `t_foot` VALUES ('785', '288', '10512', '2018-04-01 11:44:05', null, null, null);
INSERT INTO `t_foot` VALUES ('786', '288', '10515', '2018-04-01 12:09:48', null, null, null);
INSERT INTO `t_foot` VALUES ('787', '396', '10502', '2018-04-02 09:22:07', null, null, null);
INSERT INTO `t_foot` VALUES ('788', '272', '10504', '2018-04-02 10:50:51', null, null, null);
INSERT INTO `t_foot` VALUES ('789', '272', '10513', '2018-04-02 10:54:56', null, null, null);
INSERT INTO `t_foot` VALUES ('790', '362', '10513', '2018-04-02 15:25:09', null, null, null);
INSERT INTO `t_foot` VALUES ('791', '277', '10515', '2018-04-02 20:51:38', null, null, null);
INSERT INTO `t_foot` VALUES ('792', '400', '10502', '2018-04-02 20:54:06', null, null, null);
INSERT INTO `t_foot` VALUES ('793', '402', '10512', '2018-04-03 12:43:10', null, null, null);
INSERT INTO `t_foot` VALUES ('794', '402', '10502', '2018-04-03 12:43:42', null, null, null);
INSERT INTO `t_foot` VALUES ('795', '403', '10502', '2018-04-05 21:17:20', null, null, null);
INSERT INTO `t_foot` VALUES ('796', '403', '10512', '2018-04-05 21:20:37', null, null, null);
INSERT INTO `t_foot` VALUES ('797', '297', '10518', '2018-04-05 23:35:23', null, null, null);
INSERT INTO `t_foot` VALUES ('798', '268', '10515', '2018-04-13 21:40:58', null, null, null);
INSERT INTO `t_foot` VALUES ('799', '268', '10513', '2018-04-13 21:41:14', null, null, null);
INSERT INTO `t_foot` VALUES ('800', '270', '10515', '2018-05-02 13:43:15', null, null, null);
INSERT INTO `t_foot` VALUES ('801', '268', '10519', '2018-05-02 13:43:44', null, null, null);
INSERT INTO `t_foot` VALUES ('802', '297', '10520', '2018-05-02 14:18:39', null, null, null);
INSERT INTO `t_foot` VALUES ('803', '268', '10520', '2018-05-02 14:18:54', null, null, null);
INSERT INTO `t_foot` VALUES ('804', '280', '10521', '2018-05-02 14:31:18', null, null, null);
INSERT INTO `t_foot` VALUES ('805', '268', '10521', '2018-05-02 14:32:01', null, null, null);
INSERT INTO `t_foot` VALUES ('806', '270', '10519', '2018-05-03 10:42:03', null, null, null);
INSERT INTO `t_foot` VALUES ('807', '406', '10502', '2018-05-28 15:29:24', null, null, null);
INSERT INTO `t_foot` VALUES ('808', '405', '10515', '2018-05-31 16:14:50', null, null, null);
INSERT INTO `t_foot` VALUES ('809', '284', '10513', '2018-05-31 23:01:32', null, null, null);
INSERT INTO `t_foot` VALUES ('810', '284', '10505', '2018-05-31 23:03:12', null, null, null);
INSERT INTO `t_foot` VALUES ('811', '297', '10515', '2018-06-01 09:51:43', null, null, null);
INSERT INTO `t_foot` VALUES ('812', '274', '10518', '2018-06-01 10:34:14', null, null, null);
INSERT INTO `t_foot` VALUES ('813', '407', '10518', '2018-06-02 21:34:14', null, null, null);

-- ----------------------------
-- Table structure for t_index_img
-- ----------------------------
DROP TABLE IF EXISTS `t_index_img`;
CREATE TABLE `t_index_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `picture` varchar(100) NOT NULL COMMENT '图片',
  `order1` int(11) NOT NULL COMMENT '排序',
  `link` varchar(145) DEFAULT NULL COMMENT '广告链接',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order1` (`order1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='首页图片表';

-- ----------------------------
-- Records of t_index_img
-- ----------------------------
INSERT INTO `t_index_img` VALUES ('15', 'ueditor/upload/image/20170922/1506054581107038757.jpg', '2', 'http://www.baidu.com', '2017-09-25 16:17:13', null, '2017-09-25 16:17:13', null);

-- ----------------------------
-- Table structure for t_keyvalue
-- ----------------------------
DROP TABLE IF EXISTS `t_keyvalue`;
CREATE TABLE `t_keyvalue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key1` varchar(45) NOT NULL,
  `value` varchar(145) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='键值对表';

-- ----------------------------
-- Records of t_keyvalue
-- ----------------------------
INSERT INTO `t_keyvalue` VALUES ('46', 'sendPaySuccessMsg', '5jSwJjTnnfgepU0o97jDmMHSe3URrq72C5Gy-F_YArw');
INSERT INTO `t_keyvalue` VALUES ('47', 'sendSubOrderMsg', 'mMxIn7MVydBOR4GtTUAkoD8w_pDkgoK5-Z5ayacoN9k');
INSERT INTO `t_keyvalue` VALUES ('48', 'sendOrderRebateMsg', 'q8B3TPTP21Ti6JdZGDVTH_Ah1SWDs0G6US9xOqijh4A');
INSERT INTO `t_keyvalue` VALUES ('49', 'sendProductCheckMsg', '4kBFJqzVwK4P2HdFSByE7XL5Us9F-vhs0QkSvdLC_X8');
INSERT INTO `t_keyvalue` VALUES ('50', 'sendWalletCheckMsg', '89ARYuUV6_ktTmdb7s2HxzVZihMb4LodmsE7X9CMmt4');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT '111',
  `orderNum` int(11) NOT NULL DEFAULT '0',
  `type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='系统菜单表';

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', '0', '', '系统管理', '20', 'module');
INSERT INTO `t_menu` VALUES ('2', '1', '/rest/manage/menu/toList', '资源管理', '3', 'page');
INSERT INTO `t_menu` VALUES ('3', '1', '/rest/manage/user/toList', '用户设置', '1', 'page');
INSERT INTO `t_menu` VALUES ('4', '1', '/rest/manage/role/toList', '角色管理', '2', 'page');
INSERT INTO `t_menu` VALUES ('6', '0', '/rest/manage/news/toList', '资讯管理', '2', 'module');
INSERT INTO `t_menu` VALUES ('11', '0', '', '商品管理', '4', 'module');
INSERT INTO `t_menu` VALUES ('14', '11', '/rest/manage/product/toList', '商品管理', '12', 'page');
INSERT INTO `t_menu` VALUES ('18', '0', '/rest/manage/order/toList', '订单管理', '5', 'module');
INSERT INTO `t_menu` VALUES ('36', '1', '/rest/manage/systemlog/toList', '日志管理', '8', 'page');
INSERT INTO `t_menu` VALUES ('49', '1', '/rest/manage/systemSetting/toEdit?init=y', '系统设置', '9', 'page');
INSERT INTO `t_menu` VALUES ('51', '1', '/rest/manage/keyvalue/toList', '键值对管理', '11', 'page');
INSERT INTO `t_menu` VALUES ('52', '3', '/rest/manage/user/toList', '查询', '1', 'button');
INSERT INTO `t_menu` VALUES ('55', '3', '/rest/manage/user/insert', '添加', '2', 'button');
INSERT INTO `t_menu` VALUES ('56', '3', '/rest/manage/user/deletes', '删除', '3', 'button');
INSERT INTO `t_menu` VALUES ('58', '4', '/rest/manage/role/toList', '查询', '1', 'button');
INSERT INTO `t_menu` VALUES ('59', '4', '/rest/manage/role/insert', '添加', '2', 'button');
INSERT INTO `t_menu` VALUES ('60', '4', '/rest/manage/role/deletes', '删除', '3', 'button');
INSERT INTO `t_menu` VALUES ('61', '2', '/rest/manage/menu/toList', '查询', '1', 'button');
INSERT INTO `t_menu` VALUES ('62', '2', '/rest/manage/menu/insert', '添加', '2', 'button');
INSERT INTO `t_menu` VALUES ('63', '2', '/rest/manage/menu/deletes', '删除', '3', 'button');
INSERT INTO `t_menu` VALUES ('65', '11', '/rest/manage/catalog/toList', '分类管理', '11', 'page');
INSERT INTO `t_menu` VALUES ('95', '1', '/rest/manage/cache/', '缓存更新', '155', 'page');
INSERT INTO `t_menu` VALUES ('97', '1', '/rest/manage/task/toList', '任务管理', '0', 'page');
INSERT INTO `t_menu` VALUES ('123', '0', '', '信息管理', '3', 'module');
INSERT INTO `t_menu` VALUES ('131', '0', '', '提现管理', '5', 'module');
INSERT INTO `t_menu` VALUES ('147', '0', '/rest/manage/account/toList', '用户管理', '0', 'module');
INSERT INTO `t_menu` VALUES ('148', '123', '/rest/manage/indexImg/toList', '轮播图管理', '11', 'module');
INSERT INTO `t_menu` VALUES ('149', '123', '/rest/manage/indexImg/toFindEdit', '发现管理', '12', 'module');
INSERT INTO `t_menu` VALUES ('150', '123', '/rest/manage/indexImg/toJoinUs', '加入我们', '13', 'module');
INSERT INTO `t_menu` VALUES ('151', '131', '/rest/manage/brokerage/toList', '返利明细', '11', 'module');
INSERT INTO `t_menu` VALUES ('152', '131', '/rest/manage/wallet/toList', '提现审批', '12', 'module');
INSERT INTO `t_menu` VALUES ('155', '123', '/rest/manage/indexImg/toFindUs', '关于我们', '15', 'module');
INSERT INTO `t_menu` VALUES ('157', '0', '/rest/manage/product/toCheckList', '开团审核', '6', 'module');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `userID` varchar(11) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户id',
  `merchantID` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '商家ID',
  `supUserID` varchar(11) CHARACTER SET utf8 DEFAULT '0' COMMENT '上级ID',
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'init' COMMENT '订单状态(init:待付款，pass:待发货,send:待收货，finish:已完成，cancel:已取消)',
  `ptotal` decimal(20,2) DEFAULT '0.00' COMMENT '总价',
  `deduction` decimal(20,2) DEFAULT '0.00' COMMENT '钱包抵扣金额',
  `rebate` decimal(20,2) DEFAULT '0.00' COMMENT '上级返利金额',
  `addressName` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货人姓名',
  `addressPhone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货人手机号码',
  `addressArea` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货人地区信息',
  `addressDetail` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货人详细地址',
  `expressCompany` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '快递公司',
  `expressNum` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '快递单号',
  `consigner` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '发货人',
  `remark` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `isGroup` varchar(255) CHARACTER SET utf8 DEFAULT 'n' COMMENT '是否团购(y是、n否)',
  `groupOrderID` varchar(255) CHARACTER SET utf8 DEFAULT '0' COMMENT '参与团购订单ID',
  `groupNumber` int(11) DEFAULT '0' COMMENT '参团人数',
  `isReserve` varchar(255) CHARACTER SET utf8 DEFAULT 'n' COMMENT '是否预定（y是、n否）',
  `createTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  KEY `order_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10875 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='订单表';

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('10774', '237', '0', '0', 'finish', '85.00', '0.00', '0.00', '林添珍', '18650199186', '福建 厦门 思明区', '软件园二期观日路32号 网宿科技', null, null, null, null, 'y', '0', '0', 'n', '2018-03-23 13:57:14', null, '2018-03-23 14:36:22', null);
INSERT INTO `t_order` VALUES ('10775', '296', '0', '0', 'send', '85.00', '0.00', '0.00', '汤文镇', '18950281173', '福建 厦门 思明区', '软件园观日路32号网宿科技 观日路32号', null, null, null, null, 'y', '0', '0', 'n', '2018-03-23 13:58:19', null, '2018-03-29 20:10:36', null);
INSERT INTO `t_order` VALUES ('10805', '300', '0', '0', 'send', '88.00', '0.00', '0.00', '林美慧', '18659289253', '福建 厦门 思明区', '观音山宝业大厦网宿科技10楼  ', null, null, null, null, 'y', '0', '0', 'n', '2018-03-27 12:50:32', null, null, null);
INSERT INTO `t_order` VALUES ('10806', '302', '0', '0', 'send', '88.00', '0.00', '0.00', '王福彬', '18605029950', '福建 厦门 思明区', '台东路66号宝业大厦 9楼网宿科技', null, null, null, null, 'y', '0', '0', 'n', '2018-03-27 12:51:11', null, null, null);
INSERT INTO `t_order` VALUES ('10808', '297', '0', '0', 'send', '19.80', '0.00', '0.00', '吴志林', '15711593316', '福建 厦门 思明区', '何厝69号 333', null, null, null, '测试购买', 'y', '0', '1', 'n', '2018-03-27 13:16:08', null, '2018-03-27 13:50:31', null);
INSERT INTO `t_order` VALUES ('10809', '274', '0', '292', 'finish', '19.80', '0.00', '1.98', '抖动', '13646464646', '北京 东城区 ', '纠结啊 112', null, null, null, null, 'y', '0', '0', 'n', '2018-03-27 13:24:17', null, '2018-03-29 19:52:26', null);
INSERT INTO `t_order` VALUES ('10810', '308', '0', '0', 'send', '88.00', '0.00', '0.00', '康晓欢', '18559262272', '福建 厦门 思明区', '软件园二期观日路44号楼10层 44号楼10层', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 13:42:31', null, '2018-03-27 14:16:44', null);
INSERT INTO `t_order` VALUES ('10812', '297', '0', '0', 'send', '19.80', '0.00', '0.00', '吴志林', '15711593316', '福建 厦门 思明区', '何厝69号 333', null, null, null, '测试购买', 'y', '0', '1', 'n', '2018-03-27 13:51:26', null, '2018-03-27 13:52:07', null);
INSERT INTO `t_order` VALUES ('10813', '318', '0', '0', 'send', '88.00', '0.00', '0.00', '钟九爷', '15960842422', '福建 厦门 思明区', '观音山台东路66号宝业大厦 11楼网宿科技', null, null, null, null, 'y', '0', '2', 'n', '2018-03-27 13:55:53', null, '2018-03-29 14:18:15', null);
INSERT INTO `t_order` VALUES ('10814', '316', '0', '0', 'send', '88.00', '0.00', '0.00', '蔡沐云', '13616532453', '福建 厦门 思明区', '软件园二期观日路 32号楼2楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 13:57:56', null, '2018-03-27 13:58:03', null);
INSERT INTO `t_order` VALUES ('10815', '319', '0', '0', 'init', '88.00', '0.00', '0.00', '王鸿彦', '18538171659', '福建 厦门 集美区', '中海锦城国际 8栋', null, null, null, null, 'y', '0', '0', 'n', '2018-03-27 13:59:41', null, null, null);
INSERT INTO `t_order` VALUES ('10816', '320', '0', '0', 'send', '176.00', '0.00', '0.00', '沈云', '18973166662', '福建 厦门 集美区', '中航城C区 21号楼2101室', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:04:12', null, '2018-03-27 14:04:19', null);
INSERT INTO `t_order` VALUES ('10817', '321', '0', '0', 'send', '88.00', '0.00', '0.00', '纪智鹏', '18396533736', '福建 厦门 思明区', '软件园二期观日路32号 20301网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:09:56', null, '2018-03-27 14:10:00', null);
INSERT INTO `t_order` VALUES ('10819', '325', '0', '0', 'init', '88.00', '0.00', '0.00', '陈雨宁', '18060915194', '福建 厦门 思明区', '观音山台东路66号宝业运营中心 9楼网宿科技', null, null, null, null, 'y', '0', '0', 'n', '2018-03-27 14:16:10', null, null, null);
INSERT INTO `t_order` VALUES ('10820', '327', '0', '0', 'send', '88.00', '0.00', '0.00', '宋先生', '18650160988', '福建 厦门 思明区', '莲前西路2号莲富大厦公寓楼 23层G室', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:17:08', null, '2018-03-27 14:17:21', null);
INSERT INTO `t_order` VALUES ('10821', '325', '0', '0', 'send', '88.00', '0.00', '0.00', '陈雨宁', '18060915194', '福建 厦门 思明区', '观音山台东路66号宝业运营中心 9楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:17:59', null, '2018-03-27 14:18:05', null);
INSERT INTO `t_order` VALUES ('10822', '326', '0', '0', 'finish', '264.00', '0.00', '0.00', '陈茵蓉', '13163983702', '福建 厦门 思明区', '软件园二期 观日路32号之一2楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:25:41', null, '2018-03-29 16:30:15', null);
INSERT INTO `t_order` VALUES ('10823', '326', '0', '0', 'finish', '176.00', '0.00', '0.00', '陈茵蓉', '13163983702', '福建 厦门 思明区', '软件园二期 观日路44号十楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:26:52', null, '2018-03-29 16:30:09', null);
INSERT INTO `t_order` VALUES ('10824', '329', '0', '0', 'send', '88.00', '0.00', '0.00', '洪少凯', '18965652727', '福建 厦门 思明区', '软件园二期32号网宿科技 20301', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:29:50', null, '2018-03-27 14:29:56', null);
INSERT INTO `t_order` VALUES ('10825', '328', '0', '0', 'send', '88.00', '0.00', '0.00', '朱志海', '13400738025', '福建 厦门 思明区', '软件园二期观日路32号楼网宿科技 20301', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 14:30:51', null, '2018-03-27 14:30:57', null);
INSERT INTO `t_order` VALUES ('10826', '332', '0', '0', 'send', '88.00', '0.00', '0.00', '陈丽梅', '15980687260', '福建 厦门 思明区', '软件园二期观日路 32号楼20301', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 15:10:38', null, '2018-03-27 15:10:45', null);
INSERT INTO `t_order` VALUES ('10827', '335', '0', '0', 'send', '88.00', '0.00', '0.00', '吴姗姗', '18250790987', '福建 厦门 思明区', '软件园二期观日路32号 20301网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 15:48:35', null, '2018-03-27 15:48:45', null);
INSERT INTO `t_order` VALUES ('10828', '334', '0', '0', 'send', '88.00', '0.00', '0.00', '梁焘', '15060779105', '福建 厦门 思明区', '观音山台东路66号宝业大厦 9楼', null, null, null, '多点叶子好保存', 'y', '0', '1', 'n', '2018-03-27 15:57:49', null, '2018-03-27 15:57:59', null);
INSERT INTO `t_order` VALUES ('10829', '336', '0', '0', 'send', '88.00', '0.00', '0.00', '杨静', '18205969193', '福建 厦门 思明区', '观音山营运中心台东路66号 宝业大厦9-11楼', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 16:28:02', null, '2018-03-27 16:28:08', null);
INSERT INTO `t_order` VALUES ('10830', '338', '0', '0', 'send', '88.00', '0.00', '0.00', '许碧娟', '18659219801', '福建 厦门 思明区', '观音山台东路66号 宝业管理中心网宿科技有限公司', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 16:50:07', null, '2018-03-27 16:50:14', null);
INSERT INTO `t_order` VALUES ('10831', '339', '0', '0', 'send', '88.00', '0.00', '0.00', '郭爱玲', '15159257940', '福建 厦门 思明区', '软件园二期观日路32号之二20301网宿科技 20301', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 16:54:33', null, '2018-03-27 16:54:39', null);
INSERT INTO `t_order` VALUES ('10832', '314', '0', '0', 'send', '88.00', '0.00', '0.00', '张敏蓉', '18059277967', '福建 厦门 思明区', '观音山台东路66号宝业大厦 9楼-网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 17:12:11', null, '2018-03-27 17:12:18', null);
INSERT INTO `t_order` VALUES ('10833', '341', '0', '0', 'send', '88.00', '0.00', '0.00', '邱锦鹏', '13159239633', '福建 厦门 思明区', '观音山台东路66号宝业大厦9楼网宿科技 宝业大厦9楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 17:46:20', null, '2018-03-27 17:46:36', null);
INSERT INTO `t_order` VALUES ('10834', '342', '0', '0', 'send', '88.00', '0.00', '0.00', '陈秋霞', '18859241975', '福建 厦门 思明区', '观音山台东路66号宝业运营中心 11楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-27 17:56:15', null, '2018-03-27 17:56:24', null);
INSERT INTO `t_order` VALUES ('10835', '343', '0', '0', 'send', '88.00', '0.00', '0.00', '江华忠', '13860469009', '福建 厦门 思明区', '软件园二期 观日路32号', null, null, null, '网宿32号楼', 'y', '0', '1', 'n', '2018-03-27 19:05:20', null, '2018-03-27 19:05:58', null);
INSERT INTO `t_order` VALUES ('10836', '274', '0', '292', 'init', '19.80', '0.00', '1.98', '抖动', '13646464646', '北京 东城区 ', '纠结啊 112', null, null, null, null, 'y', '0', '0', 'n', '2018-03-28 20:46:23', null, null, null);
INSERT INTO `t_order` VALUES ('10838', '362', '0', 'null', 'send', '99.00', '0.00', '0.00', '王惠明', '18650119363', '福建 厦门 思明区', '台东路66号 宝业大厦10楼', null, null, null, null, 'y', '10813', '2', 'n', '2018-03-29 14:18:06', null, '2018-03-30 10:32:59', null);
INSERT INTO `t_order` VALUES ('10839', '361', '0', '0', 'send', '99.00', '0.00', '0.00', '刘海健', '15960204783', '福建 厦门 同安区', '中海万锦熙岸 9#2201', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 15:08:19', null, '2018-03-29 15:08:26', null);
INSERT INTO `t_order` VALUES ('10840', '364', '0', '0', 'send', '99.00', '0.00', '0.00', 'Tina Li', '13358389099', '福建 厦门 思明区', '软件园二期 望海路45号5楼', null, null, null, '网宿科技', 'y', '0', '1', 'n', '2018-03-29 15:49:54', null, '2018-03-29 15:50:13', null);
INSERT INTO `t_order` VALUES ('10841', '347', '0', '0', 'send', '99.00', '0.00', '0.00', '黄冰冰', '13225992997', '福建 厦门 思明区', '观音山宝业大厦 11楼', null, null, null, '周一周二都可以', 'y', '0', '1', 'n', '2018-03-29 16:15:33', null, '2018-03-29 16:15:40', null);
INSERT INTO `t_order` VALUES ('10842', '371', '0', '0', 'send', '99.00', '0.00', '0.00', '王开鹏', '15160034126', '福建 厦门 思明区', '软件园二期观日路32号 网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 16:24:47', null, '2018-03-29 16:26:22', null);
INSERT INTO `t_order` VALUES ('10843', '346', '0', '0', 'send', '99.00', '0.00', '0.00', '林声隆', '13696952284', '福建 厦门 思明区', '软件园二期观日路44号10楼网宿科技 44号楼10楼', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 16:34:54', null, '2018-03-29 16:35:03', null);
INSERT INTO `t_order` VALUES ('10844', '373', '0', '0', 'send', '99.00', '0.00', '0.00', '黄美玲', '15705924414', '福建 厦门 思明区', '软件园二期 观日路32号 网宿科技 二楼', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 16:37:12', null, '2018-03-29 16:37:21', null);
INSERT INTO `t_order` VALUES ('10845', '365', '0', '0', 'send', '99.00', '0.00', '0.00', '谢美莲', '18459220955', '福建 厦门 思明区', '软件园二期观日路32#网宿科技 观日路32号', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 16:37:25', null, '2018-03-29 16:37:35', null);
INSERT INTO `t_order` VALUES ('10846', '374', '0', '0', 'send', '99.00', '0.00', '0.00', '丘琴秀', '18805921319', '福建 厦门 思明区', '莲前街道台东路66号宝业大厦 10楼', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 16:37:54', null, '2018-03-29 16:38:09', null);
INSERT INTO `t_order` VALUES ('10847', '310', '0', '0', 'send', '99.00', '0.00', '0.00', '洪艺琳', '15960846679', '福建 厦门 思明区', '台东路66号宝业投资大厦 9楼', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 17:02:24', null, '2018-03-29 17:02:29', null);
INSERT INTO `t_order` VALUES ('10848', '370', '0', '0', 'send', '99.00', '0.00', '0.00', '叶东明', '13806048530', '福建 厦门 思明区', '软件园二期 观日路44号10楼', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 17:16:15', null, '2018-03-29 17:16:33', null);
INSERT INTO `t_order` VALUES ('10849', '375', '0', '0', 'send', '99.00', '0.00', '0.00', '陈艳华', '18650019921', '福建 厦门 思明区', '软件园二期 望海路47号602', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 17:21:20', null, '2018-03-29 17:21:26', null);
INSERT INTO `t_order` VALUES ('10850', '344', '0', '0', 'send', '198.00', '0.00', '0.00', '陈丽娟', '13720881485', '福建 厦门 思明区', '观音山台东路 66号宝业大厦9层网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-29 23:14:45', null, '2018-03-29 23:14:53', null);
INSERT INTO `t_order` VALUES ('10851', '368', '0', '0', 'send', '99.00', '0.00', '0.00', '俞翔', '13606068257', '福建 厦门 思明区', '古楼北里 50-601', null, null, null, null, 'y', '0', '1', 'n', '2018-03-30 01:21:19', null, '2018-03-30 01:21:39', null);
INSERT INTO `t_order` VALUES ('10852', '386', '0', '0', 'send', '99.00', '0.00', '0.00', '郑小龙', '18059218804', '福建 厦门 思明区', '龙虎山路 384号101 御上茗', null, null, null, null, 'y', '0', '1', 'n', '2018-03-30 09:43:46', null, '2018-03-30 09:43:52', null);
INSERT INTO `t_order` VALUES ('10853', '298', '0', '0', 'send', '198.00', '0.00', '0.00', '小芳（彩虹朋友（）', '13960607758', '福建 厦门 思明区', '会展附近金海豪园89号（金海豪园别墅 89号', null, null, null, '彩虹盆友', 'y', '0', '1', 'n', '2018-03-30 09:57:21', null, '2018-03-30 09:57:31', null);
INSERT INTO `t_order` VALUES ('10854', '237', '0', '0', 'finish', '99.00', '0.00', '0.00', '林添珍', '18650199186', '福建 厦门 思明区', '软件园二期 观日路22号303-6', null, null, null, null, 'y', '10838', '1', 'n', '2018-03-30 10:32:52', null, '2018-06-12 15:54:52', null);
INSERT INTO `t_order` VALUES ('10855', '388', '0', '0', 'send', '99.00', '0.00', '0.00', '林榕芳', '15959259965', '福建 厦门 思明区', '文兴东二里21号 21号101', null, null, null, '彩虹朋友', 'y', '0', '1', 'n', '2018-03-30 10:50:06', null, '2018-03-30 10:50:13', null);
INSERT INTO `t_order` VALUES ('10856', '353', '0', '0', 'send', '99.00', '0.00', '0.00', '黄金霖', '18950116891', '福建 厦门 思明区', '软件园二期 59#网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-30 11:55:34', null, '2018-03-30 11:55:45', null);
INSERT INTO `t_order` VALUES ('10857', '237', '0', '0', 'finish', '99.00', '0.00', '0.00', '林添珍', '18650199186', '福建 厦门 思明区', '软件园二期 观日路22号303-6', null, null, null, null, 'y', '0', '1', 'n', '2018-03-30 11:59:33', null, '2018-06-12 15:54:48', null);
INSERT INTO `t_order` VALUES ('10858', '335', '0', '0', 'send', '99.00', '0.00', '0.00', '吴姗姗', '18250790987', '福建 厦门 思明区', '软件园二期观日路32号 20301网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-30 14:05:17', null, '2018-03-30 14:05:24', null);
INSERT INTO `t_order` VALUES ('10859', '390', '0', '0', 'send', '99.00', '0.00', '0.00', '黄燕芳', '15985824011', '福建 厦门 思明区', '观音山台东路66号9楼 9楼', null, null, null, '网宿', 'y', '0', '1', 'n', '2018-03-30 15:59:02', null, '2018-03-30 15:59:07', null);
INSERT INTO `t_order` VALUES ('10860', '325', '0', '0', 'send', '99.00', '0.00', '0.00', '陈雨宁', '18060915194', '福建 厦门 思明区', '观音山台东路66号宝业运营中心 9楼网宿科技', null, null, null, null, 'y', '0', '1', 'n', '2018-03-30 21:33:18', null, '2018-03-30 21:33:23', null);
INSERT INTO `t_order` VALUES ('10861', '339', '0', '0', 'send', '99.00', '0.00', '0.00', '郭爱玲', '15159257940', '福建 厦门 思明区', '软件园二期观日路32号之二20301网宿科技 20301', null, null, null, null, 'y', '0', '1', 'n', '2018-04-01 01:13:27', null, '2018-04-01 01:13:34', null);
INSERT INTO `t_order` VALUES ('10862', '403', '0', '0', 'pass', '130.00', '0.00', '0.00', '吕添灵', '13950823618', '福建 龙岩 漳平市', '菁城镇菁城街道武馆路81号 武馆路81号', null, null, null, '彩虹朋友', 'y', '0', '0', 'n', '2018-04-05 21:17:09', null, null, null);
INSERT INTO `t_order` VALUES ('10863', '297', '0', '0', 'pass', '0.01', '0.00', '0.00', '吴志林', '15711593316', '福建 厦门 思明区', '何厝69号 333', null, null, null, null, 'y', '0', '1', 'n', '2018-04-05 23:35:28', null, '2018-04-05 23:35:34', null);
INSERT INTO `t_order` VALUES ('10864', '268', '270', '0', 'pass', '0.01', '0.00', '0.00', '林志伟', '15980808080', '北京 东城区 ', '宴上 515', null, null, null, null, 'y', '0', '1', 'n', '2018-05-02 13:43:52', null, '2018-05-02 13:44:02', null);
INSERT INTO `t_order` VALUES ('10866', '280', '280', '0', 'pass', '0.01', '0.00', '0.00', '林志伟', '15980808080', '北京 东城区 ', '头有些 505', null, null, null, null, 'n', '0', '0', 'n', '2018-05-02 14:31:25', null, '2018-05-02 14:31:34', null);
INSERT INTO `t_order` VALUES ('10867', '268', '280', '0', 'pass', '0.01', '0.00', '0.00', '林志伟', '15980808080', '北京 东城区 ', '宴上 515', null, null, null, null, 'n', '0', '0', 'n', '2018-05-02 14:32:07', null, '2018-05-02 14:32:12', null);
INSERT INTO `t_order` VALUES ('10868', '270', '270', '0', 'init', '0.01', '0.00', '0.00', '怒不会', '18850458924', '陕西 渭南 潼关县', '不会v看 ：不吃v）给你们', null, null, null, null, 'y', '0', '0', 'n', '2018-05-03 10:42:05', null, null, null);
INSERT INTO `t_order` VALUES ('10869', '270', '0', '0', 'init', '45.00', '0.00', '0.00', '怒不会', '18850458924', '陕西 渭南 潼关县', '不会v看 ：不吃v）给你们', null, null, null, null, 'y', '0', '0', 'n', '2018-05-07 19:10:53', null, null, null);
INSERT INTO `t_order` VALUES ('10871', '297', '0', '0', 'init', '45.00', '0.00', '0.00', '吴志林', '15711593316', '福建 厦门 思明区', '何厝69号 333', null, null, null, null, 'y', '0', '0', 'n', '2018-06-01 09:56:34', null, null, null);
INSERT INTO `t_order` VALUES ('10872', '274', '0', '292', 'pass', '0.01', '0.00', '0.00', '抖动', '13646464646', '北京 东城区 ', '纠结啊 112', null, null, null, null, 'y', '0', '1', 'n', '2018-06-01 10:34:16', null, '2018-06-01 10:34:22', null);
INSERT INTO `t_order` VALUES ('10873', '274', '0', '292', 'pass', '5.50', '0.00', '0.55', '抖动', '13646464646', '北京 东城区 ', '纠结啊 112', null, null, null, null, 'y', '0', '1', 'n', '2018-06-01 10:35:32', null, '2018-06-01 10:35:38', null);
INSERT INTO `t_order` VALUES ('10874', '407', '0', '0', 'pass', '5.50', '0.00', '0.00', '哈哈哈', '13646014530', '山西 运城 盐湖区', '嘿嘿嘿 1234', null, null, null, null, 'y', '0', '1', 'n', '2018-06-02 21:34:40', null, '2018-06-02 21:34:48', null);

-- ----------------------------
-- Table structure for t_orderdetail
-- ----------------------------
DROP TABLE IF EXISTS `t_orderdetail`;
CREATE TABLE `t_orderdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单详情id',
  `orderID` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '订单ID',
  `productID` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品ID',
  `price` decimal(20,2) DEFAULT NULL COMMENT '价格',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `dtotal` decimal(20,2) DEFAULT NULL COMMENT '总价',
  `specInfo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '规格',
  `isComment` varchar(3) CHARACTER SET utf8 DEFAULT 'n' COMMENT '是否评论（y已评论、n未评论）',
  `createTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1136 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='订单详情表';

-- ----------------------------
-- Records of t_orderdetail
-- ----------------------------
INSERT INTO `t_orderdetail` VALUES ('1035', '10774', '10502', '85.00', '1', '150.00', '10斤装', 'y', '2018-03-23 13:57:14', null, '2018-03-24 14:23:49', null);
INSERT INTO `t_orderdetail` VALUES ('1036', '10775', '10502', '85.00', '1', '150.00', '10斤装', 'n', '2018-03-23 13:58:19', null, '2018-03-29 20:10:36', null);
INSERT INTO `t_orderdetail` VALUES ('1066', '10805', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 12:50:32', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1067', '10806', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 12:51:11', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1069', '10808', '10506', '19.80', '1', '22.50', '500g', 'n', '2018-03-27 13:16:08', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1070', '10809', '10506', '19.80', '1', '22.50', '500g', 'n', '2018-03-27 13:24:17', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1071', '10810', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 13:42:31', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1073', '10812', '10506', '19.80', '1', '22.50', '500g', 'n', '2018-03-27 13:51:26', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1074', '10813', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 13:55:53', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1075', '10814', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 13:57:56', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1076', '10815', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 13:59:41', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1077', '10816', '10502', '88.00', '2', '300.00', '10斤', 'n', '2018-03-27 14:04:12', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1078', '10817', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 14:09:56', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1080', '10819', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 14:16:10', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1081', '10820', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 14:17:08', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1082', '10821', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 14:17:59', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1083', '10822', '10502', '88.00', '3', '450.00', '10斤', 'n', '2018-03-27 14:25:41', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1084', '10823', '10502', '88.00', '2', '300.00', '10斤', 'n', '2018-03-27 14:26:52', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1085', '10824', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 14:29:50', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1086', '10825', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 14:30:51', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1087', '10826', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 15:10:38', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1088', '10827', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 15:48:35', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1089', '10828', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 15:57:49', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1090', '10829', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 16:28:02', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1091', '10830', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 16:50:07', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1092', '10831', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 16:54:33', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1093', '10832', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 17:12:11', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1094', '10833', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 17:46:20', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1095', '10834', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 17:56:15', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1096', '10835', '10502', '88.00', '1', '150.00', '10斤', 'n', '2018-03-27 19:05:20', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1097', '10836', '10506', '19.80', '1', '22.50', '500g', 'n', '2018-03-28 20:46:23', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1099', '10838', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 14:18:06', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1100', '10839', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 15:08:19', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1101', '10840', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 15:49:54', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1102', '10841', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 16:15:33', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1103', '10842', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 16:24:47', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1104', '10843', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 16:34:54', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1105', '10844', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 16:37:12', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1106', '10845', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 16:37:25', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1107', '10846', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 16:37:54', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1108', '10847', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 17:02:24', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1109', '10848', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 17:16:15', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1110', '10849', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-29 17:21:20', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1111', '10850', '10502', '99.00', '2', '338.00', '10斤', 'n', '2018-03-29 23:14:45', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1112', '10851', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 01:21:19', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1113', '10852', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 09:43:46', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1114', '10853', '10502', '99.00', '2', '338.00', '10斤', 'n', '2018-03-30 09:57:21', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1115', '10854', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 10:32:52', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1116', '10855', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 10:50:06', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1117', '10856', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 11:55:34', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1118', '10857', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 11:59:33', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1119', '10858', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 14:05:17', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1120', '10859', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 15:59:02', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1121', '10860', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-03-30 21:33:18', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1122', '10861', '10502', '99.00', '1', '169.00', '10斤', 'n', '2018-04-01 01:13:27', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1123', '10862', '10502', '130.00', '1', '169.00', '10斤', 'n', '2018-04-05 21:17:09', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1124', '10863', '10518', '0.01', '1', '0.01', '瓶装', 'n', '2018-04-05 23:35:28', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1125', '10864', '10519', '0.01', '1', '0.01', '你到家对不对', 'n', '2018-05-02 13:43:52', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1127', '10866', '10521', '0.01', '1', '0.01', '测试', 'n', '2018-05-02 14:31:25', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1128', '10867', '10521', '0.01', '1', '0.01', '测试', 'n', '2018-05-02 14:32:07', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1129', '10868', '10519', '0.01', '1', '0.01', '你到家对不对', 'n', '2018-05-03 10:42:05', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1130', '10869', '10515', '45.00', '1', '55.00', '1瓶', 'n', '2018-05-07 19:10:53', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1132', '10871', '10515', '45.00', '1', '55.00', '1瓶', 'n', '2018-06-01 09:56:34', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1133', '10872', '10518', '0.01', '1', '0.01', '瓶装', 'n', '2018-06-01 10:34:16', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1134', '10873', '10518', '5.50', '1', '0.01', '瓶装', 'n', '2018-06-01 10:35:32', null, null, null);
INSERT INTO `t_orderdetail` VALUES ('1135', '10874', '10518', '5.50', '1', '0.01', '瓶装', 'n', '2018-06-02 21:34:40', null, null, null);

-- ----------------------------
-- Table structure for t_orderpay
-- ----------------------------
DROP TABLE IF EXISTS `t_orderpay`;
CREATE TABLE `t_orderpay` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '支付id',
  `orderID` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '订单id',
  `outTradeNo` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商户订单号',
  `prepayId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '微信预支付编号',
  `productBody` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品详情',
  `productTitle` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品标题',
  `transactionId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payStatus` varchar(3) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '支付状态(0 待支付,1 已支付,2 已完成)',
  `payType` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付类型',
  `payAmount` decimal(20,2) DEFAULT NULL COMMENT '支付金额',
  `payTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付时间',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `createTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=509 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='订单支付表';

-- ----------------------------
-- Records of t_orderpay
-- ----------------------------
INSERT INTO `t_orderpay` VALUES ('405', '10767', 'dc5fc932c4944699ac456e4c6a6d19f9', 'wx20180122145033e928e019a40452451328', '味猫商城', null, null, '0', 'JSAPI', '138.00', null, null, '2018-01-22 14:50:33', null, null, null);
INSERT INTO `t_orderpay` VALUES ('406', '10772', '046d166a9b9b45f7aee8e1015bb33243', 'wx201803191510293de55d4cdd0162931396', '味猫商城', null, null, '0', 'JSAPI', '65.00', null, null, '2018-03-19 15:10:29', null, null, null);
INSERT INTO `t_orderpay` VALUES ('407', '10773', 'af1c686f73bc4643b4f4c06966dbc4bc', 'wx20180322135128209d56a95e0508865325', '味猫商城', null, null, '0', 'JSAPI', '65.00', null, null, '2018-03-21 17:59:01', null, '2018-03-22 13:51:28', null);
INSERT INTO `t_orderpay` VALUES ('408', '10774', 'd0c811b10aaf4f58ae4db3d1f1e960d7', 'wx2018032313571468e81b792c0883066975', '味猫商城', null, '4200000069201803234012123797', '1', 'JSAPI', '85.00', null, null, '2018-03-23 13:57:14', null, '2018-03-23 15:01:39', null);
INSERT INTO `t_orderpay` VALUES ('409', '10775', '18e515cf6c4c4ba2a1b16e509beeeac3', 'wx201803231358207460b5d8230598129269', '味猫商城', null, '4200000073201803233929904711', '1', 'JSAPI', '85.00', null, null, '2018-03-23 13:58:20', null, '2018-03-23 15:02:45', null);
INSERT INTO `t_orderpay` VALUES ('410', '10776', '3df54e765e9b49e2a8c8eaca8a460bf0', 'wx2018032314030398051f182c0633129391', '味猫商城', null, null, '0', 'JSAPI', '65.00', null, null, '2018-03-23 14:02:26', null, '2018-03-23 14:03:04', null);
INSERT INTO `t_orderpay` VALUES ('411', '10777', 'b9d3e5ca15f14c00a908cd7e1bbdba61', 'wx20180323141449dbad7505370186698303', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:14:49', null, null, null);
INSERT INTO `t_orderpay` VALUES ('412', '10778', 'd3c990a182b44a31beaf19299c96e83c', 'wx20180323141909127b2b10370303028462', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:19:09', null, null, null);
INSERT INTO `t_orderpay` VALUES ('413', '10779', '779f4ccd46fe4f81ace136345b3a1b42', 'wx201803231421571f16d5eb2c0295713333', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:21:34', null, '2018-03-23 14:21:57', null);
INSERT INTO `t_orderpay` VALUES ('414', '10780', '0c85d2e0e7ff48e4ba3c085221fc1d75', 'wx2018032314231329e260777f0746684629', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:23:13', null, null, null);
INSERT INTO `t_orderpay` VALUES ('415', '10781', 'e2f19bb7c724407581a86d2af13ddf64', 'wx20180323142901728ff2a1ba0839462774', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:29:01', null, null, null);
INSERT INTO `t_orderpay` VALUES ('416', '10782', 'b65d84865f3b4485810ce4a3d640deb6', 'wx20180323143546ae30e18fff0586484896', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:35:46', null, null, null);
INSERT INTO `t_orderpay` VALUES ('417', '10783', 'c82990b98f70404ab70bf098fcd820db', 'wx201803231440410b471c6d910059624155', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:40:42', null, null, null);
INSERT INTO `t_orderpay` VALUES ('418', '10784', '02bd5989bfd043b59a270f62ae2d872f', 'wx20180323145246db76b3e7970658352469', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 14:52:46', null, null, null);
INSERT INTO `t_orderpay` VALUES ('422', '10788', '105f781020ce41288d89e457eef7b621', 'wx20180323150806deaa0c12fb0995529469', '味猫商城', null, '4200000072201803233987852928', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:08:07', null, '2018-03-23 15:08:13', null);
INSERT INTO `t_orderpay` VALUES ('423', '10789', 'c0a481b72f0d4a1ebcf074b8e7688552', 'wx20180323152602b8fad6273a0457734668', '味猫商城', null, '4200000056201803233996830881', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:26:02', null, '2018-03-23 15:26:13', null);
INSERT INTO `t_orderpay` VALUES ('424', '10790', 'a0371a5dc5f4412daafb62724da361c1', 'wx20180323153030efd4e2d8270651764790', '味猫商城', null, '4200000054201803234055362398', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:30:20', null, '2018-03-23 15:30:38', null);
INSERT INTO `t_orderpay` VALUES ('425', '10791', '904287f563e7466c9d1d55ff5337f51b', 'wx201803231531343a30742ced0184198587', '味猫商城', null, '4200000075201803234080925508', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:31:34', null, '2018-03-23 15:31:39', null);
INSERT INTO `t_orderpay` VALUES ('426', '10792', '8ebc6b57fad14e50bde87c4ff421e203', 'wx2018032315324563510c22ab0193112443', '味猫商城', null, '4200000072201803234027775548', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:32:45', null, '2018-03-23 15:32:49', null);
INSERT INTO `t_orderpay` VALUES ('427', '10793', '8a9e326a31b445d8bc6093a658a5bc4f', 'wx2018032315335323b52e47cc0891457827', '味猫商城', null, '4200000069201803234005815364', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:33:53', null, '2018-03-23 15:33:58', null);
INSERT INTO `t_orderpay` VALUES ('428', '10794', '4341911295d949609de0445f7ac4e0a1', 'wx201803231534278057d73d2e0949223376', '味猫商城', null, '4200000073201803234077932650', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:34:27', null, '2018-03-23 15:34:31', null);
INSERT INTO `t_orderpay` VALUES ('429', '10795', 'a80f397ef3dc4475b830e34b11571126', 'wx20180323155142a40cf6c0a90857905527', '味猫商城', null, '4200000078201803234072788816', '1', 'JSAPI', '0.01', null, null, '2018-03-23 15:51:42', null, '2018-03-23 15:53:53', null);
INSERT INTO `t_orderpay` VALUES ('430', '10796', '75daa50682ae4521a5e0f2001de2a8bf', 'wx20180323160013db403e994e0168537772', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-03-23 16:00:13', null, null, null);
INSERT INTO `t_orderpay` VALUES ('431', '10797', '2930b01ee08840d2a22bf28776411dd9', 'wx2018032316135158e9052c7a0615696659', '味猫商城', null, null, '0', 'JSAPI', '85.00', null, null, '2018-03-23 16:13:51', null, null, null);
INSERT INTO `t_orderpay` VALUES ('432', '10798', '69fc2a1db6af4d48b8ed9eadd876750d', 'wx201803231615262e2ef50b560875925243', '味猫商城', null, '4200000054201803234091282565', '1', 'JSAPI', '0.01', null, null, '2018-03-23 16:15:26', null, '2018-03-23 16:15:30', null);
INSERT INTO `t_orderpay` VALUES ('433', '10799', '24f711e6c2da4fdba6a6baba17429208', 'wx201803231639499a4cdb80600314434858', '味猫商城', null, '4200000068201803234069811082', '1', 'JSAPI', '0.01', null, null, '2018-03-23 16:39:49', null, '2018-03-23 16:39:54', null);
INSERT INTO `t_orderpay` VALUES ('434', '10800', '4e39a85f31444ee2abaa8935e1a3c400', 'wx201803231640122c9d4ba8a50235549010', '味猫商城', null, '4200000055201803234061610471', '1', 'JSAPI', '0.01', null, null, '2018-03-23 16:40:12', null, '2018-03-23 16:40:18', null);
INSERT INTO `t_orderpay` VALUES ('435', '10801', '2c3edde827c54e58b4cd7d7b3bf5c168', 'wx20180323164217ceea9c91f20347924684', '味猫商城', null, '4200000075201803234116373015', '1', 'JSAPI', '0.01', null, null, '2018-03-23 16:42:17', null, '2018-03-23 16:42:23', null);
INSERT INTO `t_orderpay` VALUES ('436', '10802', '9e436e56fd25467fbac29ea2b97b455f', 'wx20180323164643d28e01b9220949862164', '味猫商城', null, '4200000072201803234085737898', '1', 'JSAPI', '0.01', null, null, '2018-03-23 16:46:43', null, '2018-03-23 16:46:47', null);
INSERT INTO `t_orderpay` VALUES ('437', '10803', '6f933d1acc774123ba4f0553ad61efe0', 'wx2018032317011563057f30300734606516', '味猫商城', null, '4200000078201803234068739876', '1', 'JSAPI', '0.01', null, null, '2018-03-23 17:01:15', null, '2018-03-23 17:01:20', null);
INSERT INTO `t_orderpay` VALUES ('438', '10804', 'bc24a963149e4046b28325367ad9ec32', 'wx20180324113708fedaa848b20666277939', '味猫商城', null, '4200000072201803244510480924', '1', 'JSAPI', '0.01', null, null, '2018-03-24 11:37:09', null, '2018-03-24 11:37:14', null);
INSERT INTO `t_orderpay` VALUES ('439', '10805', '22ec850032b14f15946762832059ed59', 'wx201803271254466c2f31493a0031573906', '味猫商城', null, null, '0', 'JSAPI', '88.00', null, null, '2018-03-27 12:50:33', null, '2018-03-27 12:54:46', null);
INSERT INTO `t_orderpay` VALUES ('440', '10806', '48a4a238ce7c4529a438266fc1625405', 'wx201803271251116feb217b350031450684', '味猫商城', null, '4200000077201803276572656808', '1', 'JSAPI', '88.00', null, null, '2018-03-27 12:51:11', null, '2018-03-27 13:25:35', null);
INSERT INTO `t_orderpay` VALUES ('441', '10807', '48553a4defa9476fb44478d7776ab459', 'wx201803271313137458cf531b0085427849', '味猫商城', null, '4200000073201803276524072561', '1', 'JSAPI', '0.01', null, null, '2018-03-27 13:13:14', null, '2018-03-27 14:17:24', null);
INSERT INTO `t_orderpay` VALUES ('442', '10808', '45f4c8ede8264e2b8ec451ab21320c99', 'wx2018032713160938f6acce290048549725', '味猫商城', null, '4200000065201803276564168165', '1', 'JSAPI', '19.80', null, null, '2018-03-27 13:16:09', null, '2018-03-27 13:50:31', null);
INSERT INTO `t_orderpay` VALUES ('443', '10809', '245a53d00bec440c9676b4d52ef7f90b', 'wx201803271324182863fbf3170726471263', '味猫商城', null, '4200000053201803276594034831', '1', 'JSAPI', '19.80', null, null, '2018-03-27 13:24:18', null, '2018-03-27 13:58:31', null);
INSERT INTO `t_orderpay` VALUES ('444', '10810', '8aa91d81be17489182b01613340a84d5', 'wx201803271342314ec346c1c00391280224', '味猫商城', null, '4200000076201803276545802936', '1', 'JSAPI', '88.00', null, null, '2018-03-27 13:42:31', null, '2018-03-27 14:16:44', null);
INSERT INTO `t_orderpay` VALUES ('445', '10811', '1ddd4064d05d42bdbb93c2056823c505', 'wx20180327134942175f19e7490322033736', '味猫商城', null, '4200000079201803276549098296', '1', 'JSAPI', '0.01', null, null, '2018-03-27 13:49:43', null, '2018-03-27 13:49:53', null);
INSERT INTO `t_orderpay` VALUES ('446', '10812', 'eb7a3e5d6ab4439c9882ac30e3c152bb', 'wx201803271351436cc61958f60753672781', '味猫商城', null, '4200000061201803276576749263', '1', 'JSAPI', '19.80', null, null, '2018-03-27 13:51:27', null, '2018-03-27 13:52:07', null);
INSERT INTO `t_orderpay` VALUES ('447', '10813', '964740a5567648fcb7371eda37c204fe', 'wx2018032713555304e8fbcb580627095264', '味猫商城', null, '4200000074201803276577888517', '1', 'JSAPI', '88.00', null, null, '2018-03-27 13:55:53', null, '2018-03-27 13:55:59', null);
INSERT INTO `t_orderpay` VALUES ('448', '10814', 'c4e4b041923c45779a702dfad6f44207', 'wx20180327135756d529f388220826130896', '味猫商城', null, '4200000064201803276630427101', '1', 'JSAPI', '88.00', null, null, '2018-03-27 13:57:56', null, '2018-03-27 13:58:03', null);
INSERT INTO `t_orderpay` VALUES ('449', '10815', '8420c56a43ed4e17a3977065f22d52a6', 'wx201803271359421bcb0674b90132002415', '味猫商城', null, null, '0', 'JSAPI', '88.00', null, null, '2018-03-27 13:59:42', null, null, null);
INSERT INTO `t_orderpay` VALUES ('450', '10816', '9a879fb25ef94ab98a43f01090b3188e', 'wx20180327140412432142b8280628010050', '味猫商城', null, '4200000060201803276614452540', '1', 'JSAPI', '176.00', null, null, '2018-03-27 14:04:12', null, '2018-03-27 14:04:19', null);
INSERT INTO `t_orderpay` VALUES ('451', '10817', 'c9d22f1c43e741f2b067aef0cb00409b', 'wx20180327140956c644b0d9070650821159', '味猫商城', null, '4200000077201803276537543953', '1', 'JSAPI', '88.00', null, null, '2018-03-27 14:09:56', null, '2018-03-27 14:10:00', null);
INSERT INTO `t_orderpay` VALUES ('452', '10818', '2f7d6986cb3342c7925e5de3404bd356', 'wx20180327141502b0ddc2a0630398973661', '味猫商城', null, null, '0', 'JSAPI', '264.00', null, null, '2018-03-27 14:15:02', null, null, null);
INSERT INTO `t_orderpay` VALUES ('453', '10819', '9363bd03e66644f8a347ec25b7285f6c', 'wx20180327141610c287efa09f0331222948', '味猫商城', null, null, '0', 'JSAPI', '88.00', null, null, '2018-03-27 14:16:10', null, null, null);
INSERT INTO `t_orderpay` VALUES ('454', '10820', '8c86c4588a9e458781bfcfde6dcfcd80', 'wx201803271417096ce176bfb60336008834', '味猫商城', null, '4200000071201803276552929327', '1', 'JSAPI', '88.00', null, null, '2018-03-27 14:17:09', null, '2018-03-27 14:17:21', null);
INSERT INTO `t_orderpay` VALUES ('455', '10821', '5560b453bbd14d23a810b07ab6bba2b9', 'wx201803271417597788a156220052922045', '味猫商城', null, '4200000051201803276560890487', '1', 'JSAPI', '88.00', null, null, '2018-03-27 14:17:59', null, '2018-03-27 14:18:05', null);
INSERT INTO `t_orderpay` VALUES ('456', '10822', 'a4aa581d32ee4d5f818ef71c91977d68', 'wx20180327142541afd82446000157742054', '味猫商城', null, '4200000055201803276610564335', '1', 'JSAPI', '264.00', null, null, '2018-03-27 14:25:41', null, '2018-03-27 14:25:47', null);
INSERT INTO `t_orderpay` VALUES ('457', '10823', '19a00daa66aa46c989d0874c258239a6', 'wx20180327142652aaf5c41eec0286066989', '味猫商城', null, '4200000064201803276585617989', '1', 'JSAPI', '176.00', null, null, '2018-03-27 14:26:52', null, '2018-03-27 14:26:57', null);
INSERT INTO `t_orderpay` VALUES ('458', '10824', '48dd51728d6343f7a851c1605d580873', 'wx20180327142950c2ae35585e0065706062', '味猫商城', null, '4200000072201803276588732538', '1', 'JSAPI', '88.00', null, null, '2018-03-27 14:29:50', null, '2018-03-27 14:29:56', null);
INSERT INTO `t_orderpay` VALUES ('459', '10825', 'f4635dc2534b48ad901c9a382e83b9c6', 'wx20180327143051ea868930450154728925', '味猫商城', null, '4200000078201803276631214218', '1', 'JSAPI', '88.00', null, null, '2018-03-27 14:30:51', null, '2018-03-27 14:30:57', null);
INSERT INTO `t_orderpay` VALUES ('460', '10826', 'f1e3eb7bcaea458fa96dfb13827c89d7', 'wx201803271510386a9f66f1390833509956', '味猫商城', null, '4200000073201803276604179286', '1', 'JSAPI', '88.00', null, null, '2018-03-27 15:10:38', null, '2018-03-27 15:10:45', null);
INSERT INTO `t_orderpay` VALUES ('461', '10827', '36a127f76f9847f6bc948fa7666a824f', 'wx201803271548360031428dd70431644974', '味猫商城', null, '4200000074201803276636265904', '1', 'JSAPI', '88.00', null, null, '2018-03-27 15:48:36', null, '2018-03-27 15:48:45', null);
INSERT INTO `t_orderpay` VALUES ('462', '10828', '33498cbe2d104d5b8a073e45918684e6', 'wx20180327155749e15999986f0506186486', '味猫商城', null, '4200000080201803276642375485', '1', 'JSAPI', '88.00', null, null, '2018-03-27 15:57:50', null, '2018-03-27 15:57:59', null);
INSERT INTO `t_orderpay` VALUES ('463', '10829', 'b1c83284043e48a9be810ee9d0fea313', 'wx20180327162802b69efb8cc40926045020', '味猫商城', null, '4200000073201803276641427796', '1', 'JSAPI', '88.00', null, null, '2018-03-27 16:28:02', null, '2018-03-27 16:28:08', null);
INSERT INTO `t_orderpay` VALUES ('464', '10830', '223e9e1c342049eba324920a9be46722', 'wx201803271650076b40d9f02b0815203414', '味猫商城', null, '4200000075201803276680936310', '1', 'JSAPI', '88.00', null, null, '2018-03-27 16:50:07', null, '2018-03-27 16:50:14', null);
INSERT INTO `t_orderpay` VALUES ('465', '10831', 'd68c9ea34496479490ddd8a52c06920a', 'wx2018032716543307d8dcd7820069651181', '味猫商城', null, '4200000073201803276622464201', '1', 'JSAPI', '88.00', null, null, '2018-03-27 16:54:33', null, '2018-03-27 16:54:39', null);
INSERT INTO `t_orderpay` VALUES ('466', '10832', '4c4cf0aad4cb4e43a4296265d812ac4e', 'wx20180327171211071fde61f30928951455', '味猫商城', null, '4200000056201803276674407526', '1', 'JSAPI', '88.00', null, null, '2018-03-27 17:12:11', null, '2018-03-27 17:12:18', null);
INSERT INTO `t_orderpay` VALUES ('467', '10833', 'a211d5c9c02b45fb801c7150f178be00', 'wx201803271746215873f184760133565104', '味猫商城', null, '4200000065201803276645877381', '1', 'JSAPI', '88.00', null, null, '2018-03-27 17:46:21', null, '2018-03-27 17:46:36', null);
INSERT INTO `t_orderpay` VALUES ('468', '10834', 'b85fc1a399ef43a09c84f32c1e7dd737', 'wx20180327175615367511a67d0983812927', '味猫商城', null, '4200000070201803276729445462', '1', 'JSAPI', '88.00', null, null, '2018-03-27 17:56:15', null, '2018-03-27 17:56:24', null);
INSERT INTO `t_orderpay` VALUES ('469', '10835', '7999789a589541b68d424f566f29615e', 'wx20180327190548d60beda1cb0739074402', '味猫商城', null, '4200000060201803276707970033', '1', 'JSAPI', '88.00', null, null, '2018-03-27 19:05:21', null, '2018-03-27 19:05:58', null);
INSERT INTO `t_orderpay` VALUES ('470', '10836', '7c53a221fe784240a411b5d543367ec1', 'wx20180328204623ea815a0da30034752394', '味猫商城', null, null, '0', 'JSAPI', '19.80', null, null, '2018-03-28 20:46:23', null, null, null);
INSERT INTO `t_orderpay` VALUES ('471', '10837', '527ee6fd1e0447d5a2a9fe669347672e', 'wx201803282054248e51d58b130731962188', '味猫商城', null, '4200000067201803287494508726', '1', 'JSAPI', '1.00', null, null, '2018-03-28 20:54:24', null, '2018-03-28 20:54:32', null);
INSERT INTO `t_orderpay` VALUES ('472', '10838', 'e9b98f7d996d4f45b0c695854284efe1', 'wx20180329141806b3e69fb5500699717305', '味猫商城', null, '4200000055201803297852959884', '1', 'JSAPI', '99.00', null, null, '2018-03-29 14:18:06', null, '2018-03-29 14:18:15', null);
INSERT INTO `t_orderpay` VALUES ('473', '10839', 'ce4df852eb95408088ee22e8f0ff24d1', 'wx20180329150820c155c47d410043854857', '味猫商城', null, '4200000071201803297836641324', '1', 'JSAPI', '99.00', null, null, '2018-03-29 15:08:20', null, '2018-03-29 15:08:26', null);
INSERT INTO `t_orderpay` VALUES ('474', '10840', '82213bc81a7a4ceb8832a93b7057ea42', 'wx20180329154954658afa4ab80064457173', '味猫商城', null, '4200000061201803297865557329', '1', 'JSAPI', '99.00', null, null, '2018-03-29 15:49:54', null, '2018-03-29 15:50:13', null);
INSERT INTO `t_orderpay` VALUES ('475', '10841', 'e93e5ec360294706a128bd646fd312fd', 'wx20180329161533da72b48bf00981672614', '味猫商城', null, '4200000071201803297850011862', '1', 'JSAPI', '99.00', null, null, '2018-03-29 16:15:33', null, '2018-03-29 16:15:41', null);
INSERT INTO `t_orderpay` VALUES ('476', '10842', 'f11c2eee6c9a4f229f6bf5b279dbc6d0', 'wx201803291624474ff159f3e30277831993', '味猫商城', null, '4200000067201803297872674228', '1', 'JSAPI', '99.00', null, null, '2018-03-29 16:24:47', null, '2018-03-29 16:26:22', null);
INSERT INTO `t_orderpay` VALUES ('477', '10843', '56b2c895c864457eb3b344ef36632cf3', 'wx2018032916345566b84be1850090763874', '味猫商城', null, '4200000061201803297225325626', '1', 'JSAPI', '99.00', null, null, '2018-03-29 16:34:55', null, '2018-03-29 16:35:03', null);
INSERT INTO `t_orderpay` VALUES ('478', '10844', '6d3c8b8e5cf14e1ca918b0b0ff1d75a4', 'wx201803291637120e4343ab670261026855', '味猫商城', null, '4200000051201803297920034358', '1', 'JSAPI', '99.00', null, null, '2018-03-29 16:37:12', null, '2018-03-29 16:37:21', null);
INSERT INTO `t_orderpay` VALUES ('479', '10845', 'c5469365829f4104a0ad66160ec957bb', 'wx201803291637259dd034a3960239243455', '味猫商城', null, '4200000053201803297895180636', '1', 'JSAPI', '99.00', null, null, '2018-03-29 16:37:25', null, '2018-03-29 16:37:35', null);
INSERT INTO `t_orderpay` VALUES ('480', '10846', '62a01fcc05254340b9cdab3aa8cdc2a1', 'wx20180329163754b7100ed0030664710787', '味猫商城', null, '4200000054201803297273326747', '1', 'JSAPI', '99.00', null, null, '2018-03-29 16:37:54', null, '2018-03-29 16:38:09', null);
INSERT INTO `t_orderpay` VALUES ('481', '10847', '9c628552eb3c4094af9a2afc54874308', 'wx2018032917022483b70f5db10642334919', '味猫商城', null, '4200000074201803297847390850', '1', 'JSAPI', '99.00', null, null, '2018-03-29 17:02:24', null, '2018-03-29 17:02:29', null);
INSERT INTO `t_orderpay` VALUES ('482', '10848', '09181c7f997b42e8a5600cb6d09cd163', 'wx20180329171616a80f4e7ff70722809171', '味猫商城', null, '4200000066201803297970486645', '1', 'JSAPI', '99.00', null, null, '2018-03-29 17:16:16', null, '2018-03-29 17:16:33', null);
INSERT INTO `t_orderpay` VALUES ('483', '10849', '665c909c960f4b92b75213d1170b6851', 'wx20180329172121a8253a68e50664881921', '味猫商城', null, '4200000070201803297891303963', '1', 'JSAPI', '99.00', null, null, '2018-03-29 17:21:21', null, '2018-03-29 17:21:26', null);
INSERT INTO `t_orderpay` VALUES ('484', '10850', 'efb9f191dd904533ab47107240112ecc', 'wx201803292314458ceb87ea2e0428396999', '味猫商城', null, '4200000074201803298063069551', '1', 'JSAPI', '198.00', null, null, '2018-03-29 23:14:45', null, '2018-03-29 23:14:53', null);
INSERT INTO `t_orderpay` VALUES ('485', '10851', 'f735f8ba081a40e3adc0544e3184b3cf', 'wx20180330012119eaadcc93ea0948125937', '味猫商城', null, '4200000067201803308131365506', '1', 'JSAPI', '99.00', null, null, '2018-03-30 01:21:19', null, '2018-03-30 01:21:39', null);
INSERT INTO `t_orderpay` VALUES ('486', '10852', '751aca7e388d4b22b9401e849a7b5a70', 'wx20180330094346886d095c700694635822', '味猫商城', null, '4200000061201803308292360823', '1', 'JSAPI', '99.00', null, null, '2018-03-30 09:43:46', null, '2018-03-30 09:43:52', null);
INSERT INTO `t_orderpay` VALUES ('487', '10853', 'aac1494ddc16454bac360c47048f9115', 'wx2018033009572168ad3d67e30790761869', '味猫商城', null, '4200000052201803308267065972', '1', 'JSAPI', '198.00', null, null, '2018-03-30 09:57:21', null, '2018-03-30 09:57:31', null);
INSERT INTO `t_orderpay` VALUES ('488', '10854', '251ce211606244ffb1a1bf3a39a890aa', 'wx201803301032527f42fceb1b0916678426', '味猫商城', null, '4200000078201803308299437665', '1', 'JSAPI', '99.00', null, null, '2018-03-30 10:32:52', null, '2018-03-30 10:32:59', null);
INSERT INTO `t_orderpay` VALUES ('489', '10855', 'e95edd9197b64937a69e22fd9d510a75', 'wx201803301050079880238d980627845743', '味猫商城', null, '4200000064201803308302616379', '1', 'JSAPI', '99.00', null, null, '2018-03-30 10:50:07', null, '2018-03-30 10:50:13', null);
INSERT INTO `t_orderpay` VALUES ('490', '10856', '117447c84a264091bd0d7b28de8eb731', 'wx2018033011553423519878200241494166', '味猫商城', null, '4200000067201803308341489899', '1', 'JSAPI', '99.00', null, null, '2018-03-30 11:55:34', null, '2018-03-30 11:55:45', null);
INSERT INTO `t_orderpay` VALUES ('491', '10857', '37c5e7dadb94466ab043d75d6e84268b', 'wx20180330115933cff8978ccf0462485074', '味猫商城', null, '4200000054201803308358420292', '1', 'JSAPI', '99.00', null, null, '2018-03-30 11:59:34', null, '2018-03-30 11:59:38', null);
INSERT INTO `t_orderpay` VALUES ('492', '10858', 'bbe22304f63849509f3cd7f1a58cf811', 'wx2018033014051739d3177d750791199281', '味猫商城', null, '4200000075201803308461286789', '1', 'JSAPI', '99.00', null, null, '2018-03-30 14:05:17', null, '2018-03-30 14:05:24', null);
INSERT INTO `t_orderpay` VALUES ('493', '10859', '7550b3320fad467596bc8ce4df803dc0', 'wx20180330155903447ef6920d0217575859', '味猫商城', null, '4200000067201803308515951739', '1', 'JSAPI', '99.00', null, null, '2018-03-30 15:59:03', null, '2018-03-30 15:59:07', null);
INSERT INTO `t_orderpay` VALUES ('494', '10860', '68fe216f8eb64b13aa1c32009c38a755', 'wx2018033021331892624dc3f20658791254', '味猫商城', null, '4200000064201803308722319427', '1', 'JSAPI', '99.00', null, null, '2018-03-30 21:33:18', null, '2018-03-30 21:33:23', null);
INSERT INTO `t_orderpay` VALUES ('495', '10861', '57fe35666015445e8398dad51e10fa99', 'wx201804010113277083d43a7a0233134374', '味猫商城', null, '4200000074201804019493941395', '1', 'JSAPI', '99.00', null, null, '2018-04-01 01:13:27', null, '2018-04-01 04:17:51', null);
INSERT INTO `t_orderpay` VALUES ('496', '10862', '2376a01564b74fdf817eb2e289a0d7f5', 'wx05211710140294bfbf773dc40089999855', '味猫商城', null, '4200000065201804052943346670', '1', 'JSAPI', '130.00', null, null, '2018-04-05 21:17:10', null, '2018-04-06 00:21:40', null);
INSERT INTO `t_orderpay` VALUES ('497', '10863', '673b4e1177de4ebcad74825cc7e4acfa', 'wx05233528539686c2641426704266962170', '味猫商城', null, '4200000059201804053041119062', '1', 'JSAPI', '0.01', null, null, '2018-04-05 23:35:28', null, '2018-04-05 23:35:34', null);
INSERT INTO `t_orderpay` VALUES ('498', '10864', 'e2108ac03df24c908d1c5b8acb41e2e3', 'wx02134352387220528ebad87b4033095547', '味猫商城', null, '4200000134201805021027420593', '1', 'JSAPI', '0.01', null, null, '2018-05-02 13:43:52', null, '2018-05-02 13:44:02', null);
INSERT INTO `t_orderpay` VALUES ('499', '10865', '1d71a99b500b4bc181f2d1da1b794405', 'wx021419029257891999c07f4e2000970788', '味猫商城', null, '4200000120201805020999987512', '1', 'JSAPI', '0.01', null, null, '2018-05-02 14:19:02', null, '2018-05-02 14:19:08', null);
INSERT INTO `t_orderpay` VALUES ('500', '10866', 'e932645c4edf471d9b5c286558864fa4', 'wx0214312524691529362c487d1425647359', '味猫商城', null, '4200000118201805021104532074', '1', 'JSAPI', '0.01', null, null, '2018-05-02 14:31:25', null, '2018-05-02 14:31:34', null);
INSERT INTO `t_orderpay` VALUES ('501', '10867', 'ff8d253a37124c18b37b17c19fc00a16', 'wx021432077809678e030299810941802487', '味猫商城', null, '4200000129201805020409550622', '1', 'JSAPI', '0.01', null, null, '2018-05-02 14:32:07', null, '2018-05-02 14:32:12', null);
INSERT INTO `t_orderpay` VALUES ('502', '10868', '2071d875c4d946dd9dab540bb93a9c76', 'wx03104206160111eb20028a1a3793573222', '味猫商城', null, null, '0', 'JSAPI', '0.01', null, null, '2018-05-03 10:42:06', null, null, null);
INSERT INTO `t_orderpay` VALUES ('503', '10869', '90b09a7b9de1420cbb673ee839124f13', 'wx07191053345728fe8425e6f32634114308', '味猫商城', null, null, '0', 'JSAPI', '45.00', null, null, '2018-05-07 19:10:53', null, null, null);
INSERT INTO `t_orderpay` VALUES ('504', '10870', '2c90dfc775dd4c9092f4a1bfbb8830df', 'wx0109492133925350292fe33f3806726936', '味猫商城', null, null, '0', 'JSAPI', '19.80', null, null, '2018-06-01 09:49:21', null, null, null);
INSERT INTO `t_orderpay` VALUES ('505', '10871', 'fba0022ff16e46dbb0d7ef4ab7235509', 'wx01095635048434abb933a06e4231252879', '味猫商城', null, null, '0', 'JSAPI', '45.00', null, null, '2018-06-01 09:56:35', null, null, null);
INSERT INTO `t_orderpay` VALUES ('506', '10872', 'd6ffe20dba6845a18367a387d6289f55', 'wx011034170353133bddfc6b7a1508839876', '味猫商城', null, '4200000134201806018554820066', '1', 'JSAPI', '0.01', null, null, '2018-06-01 10:34:17', null, '2018-06-01 10:34:22', null);
INSERT INTO `t_orderpay` VALUES ('507', '10873', '0befae98dea549b28c7f750858f0cb92', 'wx011035325169041ac5875f710472308059', '味猫商城', null, '4200000114201806012001938062', '1', 'JSAPI', '5.50', null, null, '2018-06-01 10:35:32', null, '2018-06-01 10:35:38', null);
INSERT INTO `t_orderpay` VALUES ('508', '10874', 'fc0bdff7dcfb4084bf46c462c2dc3d34', 'wx022134403471933b1beb3ebf3140638333', '味猫商城', null, '4200000117201806025726455298', '1', 'JSAPI', '5.50', null, null, '2018-06-02 21:34:40', null, '2018-06-02 21:34:48', null);

-- ----------------------------
-- Table structure for t_plot
-- ----------------------------
DROP TABLE IF EXISTS `t_plot`;
CREATE TABLE `t_plot` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(25) DEFAULT NULL COMMENT '名称',
  `province` varchar(25) DEFAULT NULL COMMENT '省份',
  `city` varchar(25) DEFAULT NULL COMMENT '城市',
  `area` varchar(25) DEFAULT NULL COMMENT '地区',
  `address` varchar(50) DEFAULT NULL COMMENT '详细地址',
  `lng` varchar(25) DEFAULT NULL COMMENT '经度',
  `lat` varchar(25) DEFAULT NULL COMMENT '纬度',
  `createTime` varchar(25) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(25) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plotName` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6980 DEFAULT CHARSET=utf8 COMMENT='小区信息表';

-- ----------------------------
-- Records of t_plot
-- ----------------------------
INSERT INTO `t_plot` VALUES ('4345', '枫丹雅苑', '福建省', '厦门市', '思明区', null, '118.117493', '24.474999', '2018-01-03 20:05:00', null, '2018-01-04 19:26:00', null);
INSERT INTO `t_plot` VALUES ('4346', '新景七星公馆', '福建省', '厦门市', '思明区', null, '118.103941', '24.492167', '2018-01-03 20:05:00', null, '2018-01-04 19:26:00', null);
INSERT INTO `t_plot` VALUES ('4347', '瑞景公园', '福建省', '厦门市', '思明区', null, '118.166559', '24.478112', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4348', '华侨海景城', '福建省', '厦门市', '思明区', null, '118.098087', '24.476291', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4349', '富山名士园', '福建省', '厦门市', '思明区', null, '118.124829', '24.483695', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4350', '故宫裕景', '福建省', '厦门市', '思明区', null, '118.08944', '24.466136', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4351', '新景中心二期', '福建省', '厦门市', '思明区', null, '118.125906', '24.486564', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4352', '银聚祥邸', '福建省', '厦门市', '思明区', null, '118.098416', '24.469804', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4353', '文屏山庄', '福建省', '厦门市', '思明区', null, '118.121913', '24.463947', '2018-01-03 20:05:00', null, '2018-01-04 19:26:01', null);
INSERT INTO `t_plot` VALUES ('4354', '西堤别墅', '福建省', '厦门市', '思明区', null, '118.087332', '24.481733', '2018-01-03 20:05:00', null, '2018-01-04 19:26:02', null);
INSERT INTO `t_plot` VALUES ('4355', '福满家园', '福建省', '厦门市', '思明区', null, '118.098969', '24.472248', '2018-01-03 20:05:00', null, '2018-01-04 19:26:02', null);
INSERT INTO `t_plot` VALUES ('4356', '联发紫微花园', '福建省', '厦门市', '思明区', null, '118.175048', '24.478752', '2018-01-03 20:05:00', null, '2018-01-04 19:26:02', null);
INSERT INTO `t_plot` VALUES ('4357', '前埔南小区(店上东里)', '福建省', '厦门市', '思明区', null, '118.179854', '24.47174', '2018-01-03 20:05:00', null, '2018-01-04 19:26:02', null);
INSERT INTO `t_plot` VALUES ('4358', '国贸信隆城', '福建省', '厦门市', '思明区', null, '118.083365', '24.472497', '2018-01-03 20:05:00', null, '2018-01-04 19:26:02', null);
INSERT INTO `t_plot` VALUES ('4359', '鑫塔水尚', '福建省', '厦门市', '思明区', null, '118.195309', '24.501257', '2018-01-03 20:05:00', null, '2018-01-04 19:26:02', null);
INSERT INTO `t_plot` VALUES ('4360', '汇丰家园', '福建省', '厦门市', '思明区', null, '118.113274', '24.471816', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4361', '槟榔东里', '福建省', '厦门市', '思明区', null, '118.121085', '24.483826', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4362', '永年天墅', '福建省', '厦门市', '思明区', null, '118.130904', '24.501102', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4363', '海峡国际社区三期', '福建省', '厦门市', '思明区', null, '118.196718', '24.483843', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4364', '屿后南里小区', '福建省', '厦门市', '思明区', null, '118.127438', '24.497206', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4365', '金鸡亭花园小区', '福建省', '厦门市', '思明区', null, '118.149647', '24.479739', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4366', '华天花园', '福建省', '厦门市', '思明区', null, '118.128952', '24.490684', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4367', '山水芳邻', '福建省', '厦门市', '思明区', null, '118.099959', '24.49089', '2018-01-03 20:05:00', null, '2018-01-04 19:26:03', null);
INSERT INTO `t_plot` VALUES ('4368', '永升华庭', '福建省', '厦门市', '思明区', null, '118.13115', '24.495078', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4369', '君临宝邸', '福建省', '厦门市', '思明区', null, '118.100301', '24.470342', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4370', '爱琴海', '福建省', '厦门市', '思明区', null, '118.131636', '24.431477', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4371', '云顶至尊', '福建省', '厦门市', '思明区', null, '118.167723', '24.47606', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4372', '玫瑰园', '福建省', '厦门市', '思明区', null, '118.106781', '24.494981', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4373', '福达里', '福建省', '厦门市', '思明区', null, '118.101778', '24.494316', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4374', '自然家园', '福建省', '厦门市', '思明区', null, '118.127048', '24.474582', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4375', '嘉盛豪园', '福建省', '厦门市', '思明区', null, '118.160872', '24.48434', '2018-01-03 20:05:00', null, '2018-01-04 19:26:04', null);
INSERT INTO `t_plot` VALUES ('4376', '绿家园', '福建省', '厦门市', '思明区', null, '118.120781', '24.4804', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4377', '聚祥花园', '福建省', '厦门市', '思明区', null, '118.122526', '24.500714', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4378', '浦南花园', '福建省', '厦门市', '思明区', null, '118.140953', '24.478829', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4379', '侨福城', '福建省', '厦门市', '思明区', null, '118.171616', '24.48362', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4380', '翡翠城', '福建省', '厦门市', '思明区', null, '118.13401', '24.477088', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4381', '海豚湾', '福建省', '厦门市', '思明区', null, '118.123005', '24.43574', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4382', '富城花园', '福建省', '厦门市', '思明区', null, '118.121954', '24.478622', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4383', '同亨大厦', '福建省', '厦门市', '思明区', null, '118.135888', '24.495477', '2018-01-03 20:05:00', null, '2018-01-04 19:26:05', null);
INSERT INTO `t_plot` VALUES ('4384', '广顺花园', '福建省', '厦门市', '思明区', null, '118.152668', '24.483763', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4385', '复兴世纪', '福建省', '厦门市', '思明区', null, '118.126515', '24.484273', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4386', '槟榔西里小区', '福建省', '厦门市', '思明区', null, '118.118848', '24.485217', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4387', '香江大花园', '福建省', '厦门市', '思明区', null, '118.130318', '24.488973', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4388', '源昌新天地', '福建省', '厦门市', '思明区', null, '118.117311', '24.482469', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4389', '莲岳里小区', '福建省', '厦门市', '思明区', null, '118.125305', '24.498913', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4390', '岳阳小区', '福建省', '厦门市', '思明区', null, '118.101999', '24.496085', '2018-01-03 20:05:00', null, '2018-01-04 19:26:06', null);
INSERT INTO `t_plot` VALUES ('4391', '山木清华', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4392', '福满山庄', '福建省', '厦门市', '思明区', null, '118.171017', '24.481798', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4393', '观海澜庭', '福建省', '厦门市', '思明区', null, '118.196782', '24.504523', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4394', '大洲城市花园', '福建省', '厦门市', '思明区', null, '118.11969', '24.478877', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4395', '新嘉园', '福建省', '厦门市', '思明区', null, '118.101397', '24.474538', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4396', '益泰大厦', '福建省', '厦门市', '思明区', null, '118.124742', '24.47685', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4397', '当代天境', '福建省', '厦门市', '思明区', null, '118.090129', '24.450164', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4398', '源昌君悦山', '福建省', '厦门市', '思明区', null, '118.14087', '24.484973', '2018-01-03 20:05:00', null, '2018-01-04 19:26:07', null);
INSERT INTO `t_plot` VALUES ('4399', '九龙城', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4400', '湖明丽景', '福建省', '厦门市', '思明区', null, '118.122242', '24.484967', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4401', '厦禾裕景', '福建省', '厦门市', '思明区', null, '118.105527', '24.471879', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4402', '永升花园', '福建省', '厦门市', '思明区', null, '118.132326', '24.497793', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4403', '万峰华庭', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4404', '南湖中祥大厦', '福建省', '厦门市', '思明区', null, '118.115756', '24.485874', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4405', '惠祥花园', '福建省', '厦门市', '思明区', null, '118.103377', '24.493609', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4406', '海峡国际社区一期', '福建省', '厦门市', '思明区', null, '118.196718', '24.483843', '2018-01-03 20:05:00', null, '2018-01-04 19:26:08', null);
INSERT INTO `t_plot` VALUES ('4407', '莲花广场', '福建省', '厦门市', '思明区', null, '118.136551', '24.493566', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4408', '水岸筼筜', '福建省', '厦门市', '思明区', null, '118.123795', '24.493381', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4409', '源泉山庄', '福建省', '厦门市', '思明区', null, '118.172751', '24.474399', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4410', '英皇湖畔花苑', '福建省', '厦门市', '思明区', null, '118.129869', '24.494959', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4411', '建达花园', '福建省', '厦门市', '思明区', null, '118.112511', '24.4704', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4412', '国贸汇景', '福建省', '厦门市', '思明区', null, '118.17969', '24.478208', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4413', '联发滨海名居', '福建省', '厦门市', '思明区', null, '118.190489', '24.479906', '2018-01-03 20:05:00', null, '2018-01-04 19:26:09', null);
INSERT INTO `t_plot` VALUES ('4414', '森景华庭', '福建省', '厦门市', '思明区', null, '118.134864', '24.47391', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4415', '和光里小区', '福建省', '厦门市', '思明区', null, '118.140415', '24.493426', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4416', '嘉益大厦', '福建省', '厦门市', '思明区', null, '118.132582', '24.491958', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4417', '太湖新城', '福建省', '厦门市', '思明区', null, '118.097085', '24.472708', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4418', '黄金大厦', '福建省', '厦门市', '思明区', null, '118.083106', '24.484559', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4419', '万科金域蓝湾', '福建省', '厦门市', '思明区', null, '118.171622', '24.490665', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4420', '阳光花园', '福建省', '厦门市', '思明区', null, '118.129054', '24.495537', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4421', '仙阁里花园', '福建省', '厦门市', '思明区', null, '118.124357', '24.502127', '2018-01-03 20:05:00', null, '2018-01-04 19:26:10', null);
INSERT INTO `t_plot` VALUES ('4422', '时尚国际', '福建省', '厦门市', '思明区', null, '118.132316', '24.477278', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4423', '金枫园', '福建省', '厦门市', '思明区', null, '118.128885', '24.476245', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4424', '皇家御城', '福建省', '厦门市', '思明区', null, '118.098757', '24.47034', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4425', '官邸大厦', '福建省', '厦门市', '思明区', null, '118.124452', '24.477931', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4426', '永成大厦', '福建省', '厦门市', '思明区', null, '118.102627', '24.470781', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4427', '宝嘉誉峰', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4428', '信和银湖天峰', '福建省', '厦门市', '思明区', null, '118.088724', '24.483741', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4429', '银龙广场', '福建省', '厦门市', '思明区', null, '118.113151', '24.474288', '2018-01-03 20:05:00', null, '2018-01-04 19:26:11', null);
INSERT INTO `t_plot` VALUES ('4430', '禹洲世贸国际', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4431', '源昌广场', '福建省', '厦门市', '思明区', null, '118.122574', '24.47734', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4432', '信和中央广场', '福建省', '厦门市', '思明区', null, '118.134331', '24.493375', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4433', '米兰春天', '福建省', '厦门市', '思明区', null, '118.109361', '24.491331', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4434', '卧龙晓城', '福建省', '厦门市', '思明区', null, '118.146783', '24.484865', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4435', '侨建花园', '福建省', '厦门市', '思明区', null, '118.121663', '24.500369', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4436', '禾祥首府', '福建省', '厦门市', '思明区', null, '118.086181', '24.469201', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4437', '华尔顿1275', '福建省', '厦门市', '思明区', null, '118.187912', '24.479189', '2018-01-03 20:05:00', null, '2018-01-04 19:26:12', null);
INSERT INTO `t_plot` VALUES ('4438', '半山御景二期', '福建省', '厦门市', '思明区', null, '118.117951', '24.50256', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4439', '松柏华庭', '福建省', '厦门市', '思明区', null, '118.124767', '24.504736', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4440', '海峡国际社区四期', '福建省', '厦门市', '思明区', null, '118.196718', '24.483843', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4441', '华林紫微小区', '福建省', '厦门市', '思明区', null, '118.172678', '24.479288', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4442', '浦南新村', '福建省', '厦门市', '思明区', null, '118.134944', '24.479354', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4443', '都市新巢小区', '福建省', '厦门市', '思明区', null, '118.171988', '24.486167', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4444', '蓝湾国际', '福建省', '厦门市', '思明区', null, '118.118406', '24.494587', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4445', '学林雅苑', '福建省', '厦门市', '思明区', null, '118.127786', '24.44193', '2018-01-03 20:05:00', null, '2018-01-04 19:26:13', null);
INSERT INTO `t_plot` VALUES ('4446', '益辉花园', '福建省', '厦门市', '思明区', null, '118.167892', '24.485305', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4447', '航空别墅', '福建省', '厦门市', '思明区', null, '118.172207', '24.4762', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4448', '世嘉花园', '福建省', '厦门市', '思明区', null, '118.127605', '24.496454', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4449', '天宝大厦', '福建省', '厦门市', '思明区', null, '118.13316', '24.500953', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4450', '港龙花园', '福建省', '厦门市', '思明区', null, '118.094508', '24.480872', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4451', '新景禾祥高峰会', '福建省', '厦门市', '思明区', null, '118.105462', '24.472923', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4452', '夏新花园', '福建省', '厦门市', '思明区', null, '118.109162', '24.494768', '2018-01-03 20:05:00', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4453', '东坪花园', '福建省', '厦门市', '思明区', null, '118.129283', '24.472018', '2018-01-03 20:05:01', null, '2018-01-04 19:26:14', null);
INSERT INTO `t_plot` VALUES ('4454', '罗宾森璀璨新城1期', '福建省', '厦门市', '思明区', null, '118.118158', '24.475513', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4455', '大学城', '福建省', '厦门市', '思明区', null, '118.098937', '24.442878', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4456', '城立方', '福建省', '厦门市', '思明区', null, '118.124794', '24.478701', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4457', '海晟棕蓝海', '福建省', '厦门市', '思明区', null, '118.115919', '24.486423', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4458', '亿力音乐花园一期琴园', '福建省', '厦门市', '思明区', null, '118.094387', '24.468991', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4459', '联发花园', '福建省', '厦门市', '思明区', null, '118.129632', '24.50277', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4460', '屿后北里', '福建省', '厦门市', '思明区', null, '118.12643', '24.500849', '2018-01-03 20:05:01', null, '2018-01-04 19:26:15', null);
INSERT INTO `t_plot` VALUES ('4461', '东方时代广场', '福建省', '厦门市', '思明区', null, '118.086234', '24.46785', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4462', '信和上筑', '福建省', '厦门市', '思明区', null, '118.130967', '24.491666', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4463', '莲福阁', '福建省', '厦门市', '思明区', null, '118.151549', '24.483707', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4464', '阳鸿新城', '福建省', '厦门市', '思明区', null, '118.107932', '24.46757', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4465', '至尊门第', '福建省', '厦门市', '思明区', null, '118.187404', '24.467689', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4466', '山景叠院', '福建省', '厦门市', '思明区', null, '118.09925', '24.493488', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4467', '鹭江新城', '福建省', '厦门市', '思明区', null, '118.139118', '24.495084', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4468', '金祥大厦', '福建省', '厦门市', '思明区', null, '118.11595', '24.476476', '2018-01-03 20:05:01', null, '2018-01-04 19:26:16', null);
INSERT INTO `t_plot` VALUES ('4469', '税务宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4470', '皇府御园', '福建省', '厦门市', '思明区', null, '118.164344', '24.459035', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4471', '禹洲碧山临海', '福建省', '厦门市', '思明区', null, '118.095313', '24.449402', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4472', '名仕御园', '福建省', '厦门市', '思明区', null, '118.105363', '24.466721', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4473', '明丽山庄', '福建省', '厦门市', '思明区', null, '118.178862', '24.46214', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4474', '海湾新城', '福建省', '厦门市', '思明区', null, '118.0897', '24.485237', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4475', '港澳中心', '福建省', '厦门市', '思明区', null, '118.098832', '24.476159', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4476', '图强路小区', '福建省', '厦门市', '思明区', null, '118.098246', '24.463442', '2018-01-03 20:05:01', null, '2018-01-04 19:26:17', null);
INSERT INTO `t_plot` VALUES ('4477', '聚祥广场', '福建省', '厦门市', '思明区', null, '118.102971', '24.474386', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4478', '大西洋海景城', '福建省', '厦门市', '思明区', null, '118.084781', '24.4684', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4479', '建群花园', '福建省', '厦门市', '思明区', null, '118.149313', '24.495801', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4480', '东方明珠广场', '福建省', '厦门市', '思明区', null, '118.129129', '24.489995', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4481', '奔马新村', '福建省', '厦门市', '思明区', null, '118.113609', '24.476201', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4482', '祥云公寓', '福建省', '厦门市', '思明区', null, '118.099091', '24.474247', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4483', '国贸春天', '福建省', '厦门市', '思明区', null, '118.099344', '24.492523', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4484', '白鹭花园公寓', '福建省', '厦门市', '思明区', null, '118.103943', '24.473322', '2018-01-03 20:05:01', null, '2018-01-04 19:26:18', null);
INSERT INTO `t_plot` VALUES ('4485', '莲坂西小区', '福建省', '厦门市', '思明区', null, '118.127273', '24.492238', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4486', '金林花园', '福建省', '厦门市', '思明区', null, '118.147678', '24.480875', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4487', '嘉英大厦', '福建省', '厦门市', '思明区', null, '118.082132', '24.467032', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4488', '南湖花园', '福建省', '厦门市', '思明区', null, '118.115118', '24.488454', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4489', '新华大厦', '福建省', '厦门市', '思明区', null, '118.123698', '24.495207', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4490', '电台山小区', '福建省', '厦门市', '思明区', null, '118.103532', '24.465858', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4491', '滨北国际', '福建省', '厦门市', '思明区', null, '118.089206', '24.48501', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4492', '龙福花园', '福建省', '厦门市', '思明区', null, '118.115749', '24.499107', '2018-01-03 20:05:01', null, '2018-01-04 19:26:19', null);
INSERT INTO `t_plot` VALUES ('4493', '摩登时代大厦', '福建省', '厦门市', '思明区', null, '118.13234', '24.503273', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4494', '吉祥家园', '福建省', '厦门市', '思明区', null, '118.089943', '24.481297', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4495', '联丰新城', '福建省', '厦门市', '思明区', null, '118.166514', '24.483352', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4496', '鹭腾花园', '福建省', '厦门市', '思明区', null, '118.114211', '24.474092', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4497', '源泉海景公寓', '福建省', '厦门市', '思明区', null, '118.171567', '24.473797', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4498', '家家海景', '福建省', '厦门市', '思明区', null, '118.186198', '24.480352', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4499', '育秀里', '福建省', '厦门市', '思明区', null, '118.120876', '24.492278', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4500', '新华城', '福建省', '厦门市', '思明区', null, '118.093864', '24.461044', '2018-01-03 20:05:01', null, '2018-01-04 19:26:20', null);
INSERT INTO `t_plot` VALUES ('4501', '禾祥苑', '福建省', '厦门市', '思明区', null, '118.089102', '24.469118', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4502', '益友花园', '福建省', '厦门市', '思明区', null, '118.1375', '24.480834', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4503', '文园雅阁', '福建省', '厦门市', '思明区', null, '118.108806', '24.469425', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4504', '珍珠湾花园', '福建省', '厦门市', '思明区', null, '118.118007', '24.43498', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4505', '育秀中心', '福建省', '厦门市', '思明区', null, '118.118199', '24.491579', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4506', '古楼北里', '福建省', '厦门市', '思明区', null, '118.177015', '24.472637', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4507', '源昌国际城二期', '福建省', '厦门市', '思明区', null, '118.0878', '24.46727', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4508', '前埔北区一里', '福建省', '厦门市', '思明区', null, '118.175291', '24.481889', '2018-01-03 20:05:01', null, '2018-01-04 19:26:21', null);
INSERT INTO `t_plot` VALUES ('4509', '振兴新村', '福建省', '厦门市', '思明区', null, '118.100455', '24.485473', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4510', '莲花天伦花园', '福建省', '厦门市', '思明区', null, '118.142198', '24.491726', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4511', '森海丽景', '福建省', '厦门市', '思明区', null, '118.132784', '24.474591', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4512', '东芳花园', '福建省', '厦门市', '思明区', null, '118.153417', '24.483608', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4513', '银行家园', '福建省', '厦门市', '思明区', null, '118.088311', '24.468499', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4514', '瑞景新村腾鹭苑', '福建省', '厦门市', '思明区', null, '118.161945', '24.480394', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4515', '源昌商业中心', '福建省', '厦门市', '思明区', null, '118.125547', '24.47974', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4516', '万寿新城', '福建省', '厦门市', '思明区', null, '118.110039', '24.467013', '2018-01-03 20:05:01', null, '2018-01-04 19:26:22', null);
INSERT INTO `t_plot` VALUES ('4517', '禹洲花园', '福建省', '厦门市', '思明区', null, '118.150468', '24.488531', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4518', '禾丰新景', '福建省', '厦门市', '思明区', null, '118.145381', '24.493068', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4519', '东卉花园', '福建省', '厦门市', '思明区', null, '118.093135', '24.480551', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4520', '建发花园一期', '福建省', '厦门市', '思明区', null, '118.129888', '24.478385', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4521', '明发国际新城', '福建省', '厦门市', '思明区', null, '118.185514', '24.473259', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4522', '文园公寓', '福建省', '厦门市', '思明区', null, '118.114937', '24.465555', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4523', '假日香港广场', '福建省', '厦门市', '思明区', null, '118.086849', '24.465851', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4524', '益田海景大厦', '福建省', '厦门市', '思明区', null, '118.103684', '24.495982', '2018-01-03 20:05:01', null, '2018-01-04 19:26:23', null);
INSERT INTO `t_plot` VALUES ('4525', '台湾山庄巴厘香墅', '福建省', '厦门市', '思明区', null, '118.130344', '24.50459', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4526', '岭兜小区', '福建省', '厦门市', '思明区', null, '118.185523', '24.482891', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4527', '阳光百合', '福建省', '厦门市', '思明区', null, '118.13229', '24.48202', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4528', '百合花园', '福建省', '厦门市', '思明区', null, '118.126239', '24.489499', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4529', '白鹭苑', '福建省', '厦门市', '思明区', null, '118.086692', '24.470061', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4530', '东坪山庄', '福建省', '厦门市', '思明区', null, '118.129294', '24.474068', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4531', '菡青里小区', '福建省', '厦门市', '思明区', null, '118.130063', '24.486274', '2018-01-03 20:05:01', null, '2018-01-04 19:26:24', null);
INSERT INTO `t_plot` VALUES ('4532', '南湖豪苑', '福建省', '厦门市', '思明区', null, '118.114774', '24.486902', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4533', '中国铁建海曦', '福建省', '厦门市', '思明区', null, '118.19805', '24.505484', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4534', '龙山小区', '福建省', '厦门市', '思明区', null, '118.149187', '24.48954', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4535', '明发海景苑', '福建省', '厦门市', '思明区', null, '118.185511', '24.468539', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4536', '华瑞花园二期', '福建省', '厦门市', '思明区', null, '118.166759', '24.479315', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4537', '天湖苑', '福建省', '厦门市', '思明区', null, '118.097188', '24.475987', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4538', '美新广场', '福建省', '厦门市', '思明区', null, '118.115746', '24.474161', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4539', '禹洲世纪海湾', '福建省', '厦门市', '思明区', null, '118.08966', '24.485356', '2018-01-03 20:05:01', null, '2018-01-04 19:26:25', null);
INSERT INTO `t_plot` VALUES ('4540', '协成大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4541', '碧湖豪庭', '福建省', '厦门市', '思明区', null, '118.122607', '24.490191', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4542', '筼筜书院', '福建省', '厦门市', '思明区', null, '118.108694', '24.465736', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4543', '国贸金海岸', '福建省', '厦门市', '思明区', null, '118.080766', '24.471986', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4544', '国贸广场', '福建省', '厦门市', '思明区', null, '118.106127', '24.475398', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4545', '鸿山小区二期', '福建省', '厦门市', '思明区', null, '118.090919', '24.45675', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4546', '东浦花园', '福建省', '厦门市', '思明区', null, '118.133759', '24.477916', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4547', '广景花园', '福建省', '厦门市', '思明区', null, '118.133129', '24.478428', '2018-01-03 20:05:01', null, '2018-01-04 19:26:26', null);
INSERT INTO `t_plot` VALUES ('4548', '长青北里小区', '福建省', '厦门市', '思明区', null, '118.126842', '24.493613', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4549', '御园', '福建省', '厦门市', '思明区', null, '118.13154', '24.490561', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4550', '蓉芳里', '福建省', '厦门市', '思明区', null, '118.137846', '24.494456', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4551', '定安小区', '福建省', '厦门市', '思明区', null, '118.088181', '24.458561', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4552', '槟东公寓', '福建省', '厦门市', '思明区', null, '118.123652', '24.488315', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4553', '前埔南区文兴三里', '福建省', '厦门市', '思明区', null, '118.16675', '24.477347', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4554', '中山海景广场', '福建省', '厦门市', '思明区', null, '118.085118', '24.459659', '2018-01-03 20:05:01', null, '2018-01-04 19:26:27', null);
INSERT INTO `t_plot` VALUES ('4555', '牡丹园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4556', '英才商厦', '福建省', '厦门市', '思明区', null, '118.11175', '24.473096', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4557', '海韵园', '福建省', '厦门市', '思明区', null, '118.120868', '24.435969', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4558', '金榜铁路家园', '福建省', '厦门市', '思明区', null, '118.118233', '24.472364', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4559', '潇湘大厦', '福建省', '厦门市', '思明区', null, '118.13265', '24.50228', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4560', '乔康大厦', '福建省', '厦门市', '思明区', null, '118.099963', '24.474659', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4561', '流芳里', '福建省', '厦门市', '思明区', null, '118.140883', '24.49526', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4562', '千禧园', '福建省', '厦门市', '思明区', null, '118.1163', '24.47388', '2018-01-03 20:05:01', null, '2018-01-04 19:26:28', null);
INSERT INTO `t_plot` VALUES ('4563', '莲北小区', '福建省', '厦门市', '思明区', null, '118.122419', '24.492974', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4564', '汇禾新城', '福建省', '厦门市', '思明区', null, '118.104661', '24.475489', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4565', '繁荣广场', '福建省', '厦门市', '思明区', null, '118.099291', '24.473507', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4566', '湖滨四里', '福建省', '厦门市', '思明区', null, '118.116015', '24.482588', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4567', '云顶山庄花园', '福建省', '厦门市', '思明区', null, '118.151584', '24.478516', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4568', '国贸新城', '福建省', '厦门市', '思明区', null, '118.179182', '24.478398', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4569', '演武花园', '福建省', '厦门市', '思明区', null, '118.097914', '24.444059', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4570', '皓晖花园', '福建省', '厦门市', '思明区', null, '118.121136', '24.478358', '2018-01-03 20:05:01', null, '2018-01-04 19:26:29', null);
INSERT INTO `t_plot` VALUES ('4571', '百源双玺', '福建省', '厦门市', '思明区', null, '118.122539', '24.476608', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4572', '工商局宿舍', '福建省', '厦门市', '思明区', null, '118.117285', '24.476709', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4573', '佳旺花园', '福建省', '厦门市', '思明区', null, '118.101615', '24.471724', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4574', '金榜小区', '福建省', '厦门市', '思明区', null, '118.115589', '24.478595', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4575', '前埔南区文兴二里', '福建省', '厦门市', '思明区', null, '118.16675', '24.477347', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4576', '旧莲岳里', '福建省', '厦门市', '思明区', null, '118.122796', '24.498739', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4577', '新景数码港', '福建省', '厦门市', '思明区', null, '118.127155', '24.484517', '2018-01-03 20:05:01', null, '2018-01-04 19:26:30', null);
INSERT INTO `t_plot` VALUES ('4578', '紫荆园', '福建省', '厦门市', '思明区', null, '118.115957', '24.474718', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4579', '香秀里小区', '福建省', '厦门市', '思明区', null, '118.139698', '24.4956', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4580', '仙源里', '福建省', '厦门市', '思明区', null, '118.126231', '24.503842', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4581', '美仁新村', '福建省', '厦门市', '思明区', null, '118.094971', '24.472397', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4582', '东坪山花园', '福建省', '厦门市', '思明区', null, '118.126303', '24.473204', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4583', '厦成海景花园', '福建省', '厦门市', '思明区', null, '118.08995', '24.449024', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4584', '莲花五村', '福建省', '厦门市', '思明区', null, '118.143721', '24.490572', '2018-01-03 20:05:01', null, '2018-01-04 19:26:31', null);
INSERT INTO `t_plot` VALUES ('4585', '松柏湖花园', '福建省', '厦门市', '思明区', null, '118.123088', '24.499965', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4586', '古楼南里', '福建省', '厦门市', '思明区', null, '118.177106', '24.471174', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4587', '莲花二村', '福建省', '厦门市', '思明区', null, '118.1379', '24.493771', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4588', '玉成豪园', '福建省', '厦门市', '思明区', null, '118.155094', '24.48376', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4589', '阳光海岸(别墅)', '福建省', '厦门市', '思明区', null, '118.17128', '24.45325', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4590', '富源世家', '福建省', '厦门市', '思明区', null, '118.124568', '24.485232', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4591', '瓷厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4592', '金海豪园', '福建省', '厦门市', '思明区', null, '118.187522', '24.465491', '2018-01-03 20:05:01', null, '2018-01-04 19:26:32', null);
INSERT INTO `t_plot` VALUES ('4593', '宝龙中心一期', '福建省', '厦门市', '思明区', null, '118.133579', '24.494243', '2018-01-03 20:05:01', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4594', '惠盛花园', '福建省', '厦门市', '思明区', null, '118.093298', '24.471589', '2018-01-03 20:05:01', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4595', '住总佳苑', '福建省', '厦门市', '思明区', null, '118.121244', '24.493264', '2018-01-03 20:05:01', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4596', '毅宏花园', '福建省', '厦门市', '思明区', null, '118.126791', '24.499663', '2018-01-03 20:05:02', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4597', '宏辉大厦', '福建省', '厦门市', '思明区', null, '118.089729', '24.460365', '2018-01-03 20:05:02', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4598', '慧景城', '福建省', '厦门市', '思明区', null, '118.084424', '24.47234', '2018-01-03 20:05:02', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4599', '湖北大厦', '福建省', '厦门市', '思明区', null, '118.13275', '24.502628', '2018-01-03 20:05:02', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4600', '宝达大厦', '福建省', '厦门市', '思明区', null, '118.132241', '24.492853', '2018-01-03 20:05:02', null, '2018-01-04 19:26:33', null);
INSERT INTO `t_plot` VALUES ('4601', '前埔北区二里', '福建省', '厦门市', '思明区', null, '118.179513', '24.480305', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4602', '武夷嘉园', '福建省', '厦门市', '思明区', null, '118.089584', '24.482569', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4603', '城市花园', '福建省', '厦门市', '思明区', null, '118.111938', '24.474354', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4604', '槟榔花园', '福建省', '厦门市', '思明区', null, '118.12136', '24.487382', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4605', '嘉华大厦', '福建省', '厦门市', '思明区', null, '118.129706', '24.493797', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4606', '华鸿花园', '福建省', '厦门市', '思明区', null, '118.097847', '24.47293', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4607', '厦门国际文化大厦', '福建省', '厦门市', '思明区', null, '118.124847', '24.4827', '2018-01-03 20:05:02', null, '2018-01-04 19:26:34', null);
INSERT INTO `t_plot` VALUES ('4608', '音乐家生活广场', '福建省', '厦门市', '思明区', null, '118.13294', '24.502856', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4609', '海晟花园', '福建省', '厦门市', '思明区', null, '118.121407', '24.484501', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4610', '宝马花园', '福建省', '厦门市', '思明区', null, '118.088943', '24.470408', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4611', '联丰瑞园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4612', '禾祥商城', '福建省', '厦门市', '思明区', null, '118.10529', '24.474288', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4613', '百家村花园', '福建省', '厦门市', '思明区', null, '118.099325', '24.464463', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4614', '莲坂新村一区', '福建省', '厦门市', '思明区', null, '118.12411', '24.487485', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4615', '瑞丽花园', '福建省', '厦门市', '思明区', null, '118.169032', '24.479553', '2018-01-03 20:05:02', null, '2018-01-04 19:26:35', null);
INSERT INTO `t_plot` VALUES ('4616', '故宫新城', '福建省', '厦门市', '思明区', null, '118.089772', '24.468716', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4617', '万禾花园', '福建省', '厦门市', '思明区', null, '118.101285', '24.472759', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4618', '墩仔家园', '福建省', '厦门市', '思明区', null, '118.153047', '24.481927', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4619', '湖滨名宫', '福建省', '厦门市', '思明区', null, '118.092128', '24.474506', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4620', '银苑花园', '福建省', '厦门市', '思明区', null, '118.123923', '24.483332', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4621', '怡祥花园', '福建省', '厦门市', '思明区', null, '118.126428', '24.494349', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4622', '镇海明珠', '福建省', '厦门市', '思明区', null, '118.094925', '24.459766', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4623', '海鸿花园', '福建省', '厦门市', '思明区', null, '118.122001', '24.495922', '2018-01-03 20:05:02', null, '2018-01-04 19:26:36', null);
INSERT INTO `t_plot` VALUES ('4624', '龙凤园', '福建省', '厦门市', '思明区', null, '118.12983', '24.499647', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4625', '双福花园', '福建省', '厦门市', '思明区', null, '118.087792', '24.47006', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4626', '华年邨', '福建省', '厦门市', '思明区', null, '118.083482', '24.469007', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4627', '浅水湾畔', '福建省', '厦门市', '思明区', null, '118.129625', '24.438043', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4628', '裕康花园', '福建省', '厦门市', '思明区', null, '118.124222', '24.478831', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4629', '御景豪庭', '福建省', '厦门市', '思明区', null, '118.089504', '24.483109', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4630', '市政宿舍', '福建省', '厦门市', '思明区', null, '118.1247', '24.493583', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4631', '博士新村', '福建省', '厦门市', '思明区', null, '118.106011', '24.495156', '2018-01-03 20:05:02', null, '2018-01-04 19:26:37', null);
INSERT INTO `t_plot` VALUES ('4632', '东方瑞士', '福建省', '厦门市', '思明区', null, '118.118559', '24.47698', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4633', '金丰花园', '福建省', '厦门市', '思明区', null, '118.101476', '24.473116', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4634', '禾祥公寓', '福建省', '厦门市', '思明区', null, '118.115186', '24.476552', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4635', '裕发广场', '福建省', '厦门市', '思明区', null, '118.125207', '24.480593', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4636', '国贸怡和园', '福建省', '厦门市', '思明区', null, '118.126726', '24.493814', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4637', '明发商城', '福建省', '厦门市', '思明区', null, '118.147329', '24.481735', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4638', '凤凰山庄', '福建省', '厦门市', '思明区', null, '118.092202', '24.48593', '2018-01-03 20:05:02', null, '2018-01-04 19:26:38', null);
INSERT INTO `t_plot` VALUES ('4639', '香榭园', '福建省', '厦门市', '思明区', null, '118.151754', '24.479227', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4640', '东方巴黎', '福建省', '厦门市', '思明区', null, '118.116741', '24.476153', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4641', '何厝小区', '福建省', '厦门市', '思明区', null, '118.200527', '24.490287', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4642', '领秀中山', '福建省', '厦门市', '思明区', null, '118.093513', '24.463445', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4643', '鹭江海景二期', '福建省', '厦门市', '思明区', null, '118.09106', '24.45118', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4644', '天鹭大厦', '福建省', '厦门市', '思明区', null, '118.095225', '24.460093', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4645', '国贸花园', '福建省', '厦门市', '思明区', null, '118.095034', '24.476267', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4646', '燃气宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:39', null);
INSERT INTO `t_plot` VALUES ('4647', '港务局宿舍', '福建省', '厦门市', '思明区', null, '118.089489', '24.472405', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4648', '天耀湖畔', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4649', '天润海景', '福建省', '厦门市', '思明区', null, '118.169237', '24.451988', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4650', '金辉园', '福建省', '厦门市', '思明区', null, '118.122602', '24.480826', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4651', '老虎城', '福建省', '厦门市', '思明区', null, '118.087576', '24.459382', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4652', '观远里', '福建省', '厦门市', '思明区', null, '118.13694', '24.490008', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4653', '亿力乐园', '福建省', '厦门市', '思明区', null, '118.102066', '24.470798', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4654', '一代风华', '福建省', '厦门市', '思明区', null, '118.128508', '24.502297', '2018-01-03 20:05:02', null, '2018-01-04 19:26:40', null);
INSERT INTO `t_plot` VALUES ('4655', '港龙新城', '福建省', '厦门市', '思明区', null, '118.12288', '24.494522', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4656', '禾祥观斓', '福建省', '厦门市', '思明区', null, '118.114022', '24.477844', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4657', '五州大厦', '福建省', '厦门市', '思明区', null, '118.104385', '24.474136', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4658', '华林绿景花园', '福建省', '厦门市', '思明区', null, '118.17451', '24.477931', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4659', '金同花园', '福建省', '厦门市', '思明区', null, '118.122646', '24.495155', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4660', '长城小区', '福建省', '厦门市', '思明区', null, '118.120583', '24.483285', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4661', '金榜阁', '福建省', '厦门市', '思明区', null, '118.118504', '24.47137', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4662', '永红家园', '福建省', '厦门市', '思明区', null, '118.105829', '24.494036', '2018-01-03 20:05:02', null, '2018-01-04 19:26:41', null);
INSERT INTO `t_plot` VALUES ('4663', '瑞景生活广场', '福建省', '厦门市', '思明区', null, '118.163522', '24.481356', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4664', '官任小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4665', '润泽园', '福建省', '厦门市', '思明区', null, '118.099752', '24.472238', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4666', '香莲里', '福建省', '厦门市', '思明区', null, '118.136505', '24.494779', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4667', '华林东盛花园一期', '福建省', '厦门市', '思明区', null, '118.179624', '24.482317', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4668', '盈翠嘉园', '福建省', '厦门市', '思明区', null, '118.132483', '24.49033', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4669', '金榜家园', '福建省', '厦门市', '思明区', null, '118.115818', '24.478843', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4670', '云景花园', '福建省', '厦门市', '思明区', null, '118.169401', '24.48038', '2018-01-03 20:05:02', null, '2018-01-04 19:26:42', null);
INSERT INTO `t_plot` VALUES ('4671', '仙岳山庄', '福建省', '厦门市', '思明区', null, '118.106975', '24.496891', '2018-01-03 20:05:02', null, '2018-01-04 19:26:43', null);
INSERT INTO `t_plot` VALUES ('4672', '亿力百家苑', '福建省', '厦门市', '思明区', null, '118.102315', '24.467885', '2018-01-03 20:05:02', null, '2018-01-04 19:26:43', null);
INSERT INTO `t_plot` VALUES ('4673', '金岳花园', '福建省', '厦门市', '思明区', null, '118.124966', '24.501762', '2018-01-03 20:05:02', null, '2018-01-04 19:26:43', null);
INSERT INTO `t_plot` VALUES ('4674', '云顶山庄别墅', '福建省', '厦门市', '思明区', null, '118.150397', '24.478603', '2018-01-03 20:05:02', null, '2018-01-04 19:26:43', null);
INSERT INTO `t_plot` VALUES ('4675', '石亭小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:43', null);
INSERT INTO `t_plot` VALUES ('4676', '高帝镇海花园', '福建省', '厦门市', '思明区', null, '118.094244', '24.458602', '2018-01-03 20:05:02', null, '2018-01-04 19:26:43', null);
INSERT INTO `t_plot` VALUES ('4677', '华鑫明都', '福建省', '厦门市', '思明区', null, '118.114481', '24.473622', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4678', '湖东花园', '福建省', '厦门市', '思明区', null, '118.114427', '24.487649', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4679', '古龙湖景', '福建省', '厦门市', '思明区', null, '118.109979', '24.479777', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4680', '假日万寿公寓', '福建省', '厦门市', '思明区', null, '118.110812', '24.466', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4681', '金领广场', '福建省', '厦门市', '思明区', null, '118.11792', '24.481615', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4682', '龙昌里居住区', '福建省', '厦门市', '思明区', null, '118.148083', '24.495449', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4683', '富山现代城', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4684', '金象嘉园', '福建省', '厦门市', '思明区', null, '118.11327', '24.478157', '2018-01-03 20:05:02', null, '2018-01-04 19:26:44', null);
INSERT INTO `t_plot` VALUES ('4685', '特运松柏文明小区', '福建省', '厦门市', '思明区', null, '118.120338', '24.498706', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4686', '万泰东方', '福建省', '厦门市', '思明区', null, '118.199733', '24.489869', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4687', '富山花园紫荆苑', '福建省', '厦门市', '思明区', null, '118.122548', '24.483094', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4688', '东芳山庄', '福建省', '厦门市', '思明区', null, '118.153208', '24.484827', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4689', '燃料公寓', '福建省', '厦门市', '思明区', null, '118.129851', '24.47752', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4690', '江山帝景(别墅)', '福建省', '厦门市', '思明区', null, '118.184811', '24.464127', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4691', '岳阳西里', '福建省', '厦门市', '思明区', null, '118.102852', '24.496824', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4692', '莲花花园', '福建省', '厦门市', '思明区', null, '118.145654', '24.496608', '2018-01-03 20:05:02', null, '2018-01-04 19:26:45', null);
INSERT INTO `t_plot` VALUES ('4693', '益城广场', '福建省', '厦门市', '思明区', null, '118.11393', '24.475245', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4694', '大洲新世纪广场', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4695', '文图花园', '福建省', '厦门市', '思明区', null, '118.113473', '24.469282', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4696', '莲坂押运宿舍', '福建省', '厦门市', '思明区', null, '118.119392', '24.478045', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4697', '雍景湾', '福建省', '厦门市', '思明区', null, '118.18861', '24.467198', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4698', '福津大街二节点', '福建省', '厦门市', '思明区', null, '118.118973', '24.489095', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4699', '源盛花园', '福建省', '厦门市', '思明区', null, '118.129038', '24.50154', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4700', '源隆花园', '福建省', '厦门市', '思明区', null, '118.089282', '24.468657', '2018-01-03 20:05:02', null, '2018-01-04 19:26:46', null);
INSERT INTO `t_plot` VALUES ('4701', '御景苑', '福建省', '厦门市', '思明区', null, '118.085923', '24.471221', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4702', '梅园小区', '福建省', '厦门市', '思明区', null, '118.099593', '24.475179', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4703', '禾祥花园', '福建省', '厦门市', '思明区', null, '118.088373', '24.47152', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4704', '松岳里', '福建省', '厦门市', '思明区', null, '118.116884', '24.499236', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4705', '官都里', '福建省', '厦门市', '思明区', null, '118.129788', '24.479477', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4706', '嘉美花园', '福建省', '厦门市', '思明区', null, '118.085872', '24.470371', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4707', '博海豪园', '福建省', '厦门市', '思明区', null, '118.112544', '24.43762', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4708', '香山海景苑', '福建省', '厦门市', '思明区', null, '118.187969', '24.487499', '2018-01-03 20:05:02', null, '2018-01-04 19:26:47', null);
INSERT INTO `t_plot` VALUES ('4709', '滨海湾花园', '福建省', '厦门市', '思明区', null, '118.128615', '24.433689', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4710', '珍珠湾花园(别墅)', '福建省', '厦门市', '思明区', null, '118.118007', '24.43498', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4711', '松柏别墅', '福建省', '厦门市', '思明区', null, '118.125845', '24.496937', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4712', '燃料公司宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4713', '嘉禾花园', '福建省', '厦门市', '思明区', null, '118.119206', '24.479666', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4714', '佳韵园', '福建省', '厦门市', '思明区', null, '118.096732', '24.469311', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4715', '荣滨大厦', '福建省', '厦门市', '思明区', null, '118.124684', '24.500286', '2018-01-03 20:05:02', null, '2018-01-04 19:26:48', null);
INSERT INTO `t_plot` VALUES ('4716', '古龙商城', '福建省', '厦门市', '思明区', null, '118.10848', '24.474328', '2018-01-03 20:05:02', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4717', '金瑞园', '福建省', '厦门市', '思明区', null, '118.114953', '24.474455', '2018-01-03 20:05:02', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4718', '银河大厦', '福建省', '厦门市', '思明区', null, '118.123919', '24.476624', '2018-01-03 20:05:02', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4719', '翠湖小区', '福建省', '厦门市', '思明区', null, '118.131667', '24.502192', '2018-01-03 20:05:02', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4720', '文化局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:02', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4721', '望海花园', '福建省', '厦门市', '思明区', null, '118.089548', '24.453375', '2018-01-03 20:05:03', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4722', '鑫利花园', '福建省', '厦门市', '思明区', null, '118.150471', '24.495365', '2018-01-03 20:05:03', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4723', '中兴阁', '福建省', '厦门市', '思明区', null, '118.141687', '24.481546', '2018-01-03 20:05:03', null, '2018-01-04 19:26:49', null);
INSERT INTO `t_plot` VALUES ('4724', '光明大厦', '福建省', '厦门市', '思明区', null, '118.092545', '24.468443', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4725', '美仁宫大厦公寓', '福建省', '厦门市', '思明区', null, '118.099257', '24.468354', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4726', '金磊花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4727', '海峡国际社区三期(别墅)', '福建省', '厦门市', '思明区', null, '118.196718', '24.483843', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4728', '湖滨二里小区', '福建省', '厦门市', '思明区', null, '118.11003', '24.480115', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4729', '鸿山大厦', '福建省', '厦门市', '思明区', null, '118.090519', '24.456032', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4730', '富山美迪斯', '福建省', '厦门市', '思明区', null, '118.123783', '24.48214', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4731', '共和大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:50', null);
INSERT INTO `t_plot` VALUES ('4732', '嘉盛海景大厦', '福建省', '厦门市', '思明区', null, '118.084192', '24.469159', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4733', '台湾山庄福德堡', '福建省', '厦门市', '思明区', null, '118.12901', '24.503728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4734', '华星大厦', '福建省', '厦门市', '思明区', null, '118.121246', '24.475536', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4735', '豆仔苑', '福建省', '厦门市', '思明区', null, '118.093217', '24.472516', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4736', '建群雅苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4737', '华普大厦', '福建省', '厦门市', '思明区', null, '118.115805', '24.47324', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4738', '华孚花园', '福建省', '厦门市', '思明区', null, '118.150639', '24.489481', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4739', '福隆东方丽景', '福建省', '厦门市', '思明区', null, '118.099827', '24.491617', '2018-01-03 20:05:03', null, '2018-01-04 19:26:51', null);
INSERT INTO `t_plot` VALUES ('4740', '福星花园', '福建省', '厦门市', '思明区', null, '118.145715', '24.497668', '2018-01-03 20:05:03', null, '2018-01-04 19:26:52', null);
INSERT INTO `t_plot` VALUES ('4741', '盈翠里', '福建省', '厦门市', '思明区', null, '118.134955', '24.488954', '2018-01-03 20:05:03', null, '2018-01-04 19:26:52', null);
INSERT INTO `t_plot` VALUES ('4742', '大洲国际龙郡', '福建省', '厦门市', '思明区', null, '118.18707', '24.481807', '2018-01-03 20:05:03', null, '2018-01-04 19:26:52', null);
INSERT INTO `t_plot` VALUES ('4743', '金庭花园', '福建省', '厦门市', '思明区', null, '118.114954', '24.475675', '2018-01-03 20:05:03', null, '2018-01-04 19:26:52', null);
INSERT INTO `t_plot` VALUES ('4744', '嘉隆商业小区', '福建省', '厦门市', '思明区', null, '118.118761', '24.478448', '2018-01-03 20:05:03', null, '2018-01-04 19:26:52', null);
INSERT INTO `t_plot` VALUES ('4745', '龙华大厦', '福建省', '厦门市', '思明区', null, '118.11508', '24.48846', '2018-01-03 20:05:03', null, '2018-01-04 19:26:52', null);
INSERT INTO `t_plot` VALUES ('4746', '宝龙中心二期', '福建省', '厦门市', '思明区', null, '118.133579', '24.494243', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4747', '嘉禾公寓', '福建省', '厦门市', '思明区', null, '118.094679', '24.470929', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4748', '思东商厦', '福建省', '厦门市', '思明区', null, '118.086918', '24.46345', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4749', '金沙花园', '福建省', '厦门市', '思明区', null, '118.143817', '24.480198', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4750', '惠祥公馆', '福建省', '厦门市', '思明区', null, '118.111033', '24.497335', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4751', '新景世纪城', '福建省', '厦门市', '思明区', null, '118.125512', '24.484558', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4752', '明发花园', '福建省', '厦门市', '思明区', null, '118.173008', '24.480854', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4753', '禹洲花园二期', '福建省', '厦门市', '思明区', null, '118.150144', '24.487372', '2018-01-03 20:05:03', null, '2018-01-04 19:26:53', null);
INSERT INTO `t_plot` VALUES ('4754', '局口街144号小区', '福建省', '厦门市', '思明区', null, '118.08605', '24.462111', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4755', '莲花三村社区', '福建省', '厦门市', '思明区', null, '118.14109', '24.496002', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4756', '鸿山新村', '福建省', '厦门市', '思明区', null, '118.091', '24.45229', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4757', '联邦广场', '福建省', '厦门市', '思明区', null, '118.087671', '24.46872', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4758', '古兴里', '福建省', '厦门市', '思明区', null, '118.178569', '24.472546', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4759', '华岳山庄', '福建省', '厦门市', '思明区', null, '118.127315', '24.502914', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4760', '海滨商厦', '福建省', '厦门市', '思明区', null, '118.088358', '24.451558', '2018-01-03 20:05:03', null, '2018-01-04 19:26:54', null);
INSERT INTO `t_plot` VALUES ('4761', '武夷花园', '福建省', '厦门市', '思明区', null, '118.088283', '24.482352', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4762', '源通中心', '福建省', '厦门市', '思明区', null, '118.085239', '24.46573', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4763', '故宫小区', '福建省', '厦门市', '思明区', null, '118.089124', '24.471678', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4764', '嘉成花园', '福建省', '厦门市', '思明区', null, '118.099993', '24.473945', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4765', '文馨园', '福建省', '厦门市', '思明区', null, '118.104671', '24.466721', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4766', '佳祥花园', '福建省', '厦门市', '思明区', null, '118.120006', '24.478145', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4767', '华山园', '福建省', '厦门市', '思明区', null, '118.107784', '24.491947', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4768', '瑞成大厦', '福建省', '厦门市', '思明区', null, '118.092505', '24.468913', '2018-01-03 20:05:03', null, '2018-01-04 19:26:55', null);
INSERT INTO `t_plot` VALUES ('4769', '厦商物流宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4770', '中信惠扬公寓楼', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4771', '南岳山庄', '福建省', '厦门市', '思明区', null, '118.113621', '24.498206', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4772', '西郭新村', '福建省', '厦门市', '思明区', null, '118.124385', '24.503792', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4773', '屿后里小区北2', '福建省', '厦门市', '思明区', null, '118.130915', '24.500402', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4774', '天湖花园', '福建省', '厦门市', '思明区', null, '118.117072', '24.486', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4775', '德盛大厦', '福建省', '厦门市', '思明区', null, '118.095556', '24.460201', '2018-01-03 20:05:03', null, '2018-01-04 19:26:56', null);
INSERT INTO `t_plot` VALUES ('4776', '南岳山庄(别墅)', '福建省', '厦门市', '思明区', null, '118.113621', '24.498206', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4777', '育嘉花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4778', '观音山公寓', '福建省', '厦门市', '思明区', null, '118.200097', '24.500148', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4779', '南湖明珠', '福建省', '厦门市', '思明区', null, '118.115011', '24.484557', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4780', '创冠体育大厦', '福建省', '厦门市', '思明区', null, '118.100216', '24.462432', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4781', '美仁新村南区', '福建省', '厦门市', '思明区', null, '118.096243', '24.471936', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4782', '美仁前社', '福建省', '厦门市', '思明区', null, '118.097934', '24.469398', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4783', '故宫路财政宿舍', '福建省', '厦门市', '思明区', null, '118.089463', '24.470186', '2018-01-03 20:05:03', null, '2018-01-04 19:26:57', null);
INSERT INTO `t_plot` VALUES ('4784', '仙洞苑', '福建省', '厦门市', '思明区', null, '118.119595', '24.499626', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4785', '仙岳海景大厦', '福建省', '厦门市', '思明区', null, '118.101137', '24.496207', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4786', '巡司顶社区', '福建省', '厦门市', '思明区', null, '118.09575', '24.451166', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4787', '湖滨雅居', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4788', '凤凰阁', '福建省', '厦门市', '思明区', null, '118.130308', '24.499208', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4789', '半山假期', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4790', '龙山山庄', '福建省', '厦门市', '思明区', null, '118.144371', '24.484228', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4791', '云松居', '福建省', '厦门市', '思明区', null, '118.147005', '24.477664', '2018-01-03 20:05:03', null, '2018-01-04 19:26:58', null);
INSERT INTO `t_plot` VALUES ('4792', '碧山社区', '福建省', '厦门市', '思明区', null, '118.090991', '24.450466', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4793', '国贸海景公寓', '福建省', '厦门市', '思明区', null, '118.080844', '24.468196', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4794', '骏景园', '福建省', '厦门市', '思明区', null, '118.123292', '24.484649', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4795', '龙江大厦', '福建省', '厦门市', '思明区', null, '118.138698', '24.497069', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4796', '万寿万景公寓', '福建省', '厦门市', '思明区', null, '118.1121', '24.466814', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4797', '恒通花园', '福建省', '厦门市', '思明区', null, '118.088874', '24.483131', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4798', '联丰商城', '福建省', '厦门市', '思明区', null, '118.167281', '24.483084', '2018-01-03 20:05:03', null, '2018-01-04 19:26:59', null);
INSERT INTO `t_plot` VALUES ('4799', '文屏大厦', '福建省', '厦门市', '思明区', null, '118.11218', '24.470936', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4800', '永升松柏中心', '福建省', '厦门市', '思明区', null, '118.123215', '24.496401', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4801', '电业花园', '福建省', '厦门市', '思明区', null, '118.126089', '24.489892', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4802', '文塔苑', '福建省', '厦门市', '思明区', null, '118.108225', '24.471095', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4803', '仙岳里小区西区', '福建省', '厦门市', '思明区', null, '118.110387', '24.495252', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4804', '特贸苑', '福建省', '厦门市', '思明区', null, '118.092618', '24.483505', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4805', '七星园', '福建省', '厦门市', '思明区', null, '118.102779', '24.493873', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4806', '坑内路13-18号小区', '福建省', '厦门市', '思明区', null, '118.107836', '24.463165', '2018-01-03 20:05:03', null, '2018-01-04 19:27:00', null);
INSERT INTO `t_plot` VALUES ('4807', '浦南安置房', '福建省', '厦门市', '思明区', null, '118.131483', '24.476788', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4808', '新金花园', '福建省', '厦门市', '思明区', null, '118.146485', '24.497328', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4809', '厦港花园', '福建省', '厦门市', '思明区', null, '118.096909', '24.449598', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4810', '宝成花园', '福建省', '厦门市', '思明区', null, '118.105314', '24.467792', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4811', '宏华大厦', '福建省', '厦门市', '思明区', null, '118.089284', '24.471757', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4812', '鸿禧花园', '福建省', '厦门市', '思明区', null, '118.149486', '24.48851', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4813', '嵩海公寓', '福建省', '厦门市', '思明区', null, '118.089883', '24.469275', '2018-01-03 20:05:03', null, '2018-01-04 19:27:01', null);
INSERT INTO `t_plot` VALUES ('4814', '非矿大厦', '福建省', '厦门市', '思明区', null, '118.099333', '24.476504', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4815', '将军祠社区', '福建省', '厦门市', '思明区', null, '118.106337', '24.467866', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4816', '恒滨名宫', '福建省', '厦门市', '思明区', null, '118.096577', '24.475923', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4817', '公园东路96号之一小区', '福建省', '厦门市', '思明区', null, '118.099657', '24.467759', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4818', '艺盛花园', '福建省', '厦门市', '思明区', null, '118.121649', '24.495838', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4819', '农机宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4820', '祥和广场', '福建省', '厦门市', '思明区', null, '118.092896', '24.469371', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4821', '莲坂小区', '福建省', '厦门市', '思明区', null, '118.131135', '24.493167', '2018-01-03 20:05:03', null, '2018-01-04 19:27:02', null);
INSERT INTO `t_plot` VALUES ('4822', '兴鸿广场', '福建省', '厦门市', '思明区', null, '118.082063', '24.469362', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4823', '莲景花园小区', '福建省', '厦门市', '思明区', null, '118.131296', '24.485931', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4824', '联发云海山庄', '福建省', '厦门市', '思明区', null, '118.17413', '24.459912', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4825', '金桥园', '福建省', '厦门市', '思明区', null, '118.108938', '24.491998', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4826', '明发园', '福建省', '厦门市', '思明区', null, '118.183875', '24.488625', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4827', '凤凰名都', '福建省', '厦门市', '思明区', null, '118.091997', '24.484928', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4828', '种子大厦宿舍楼', '福建省', '厦门市', '思明区', null, '118.092528', '24.484386', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4829', '泛华大厦', '福建省', '厦门市', '思明区', null, '118.103885', '24.475863', '2018-01-03 20:05:03', null, '2018-01-04 19:27:03', null);
INSERT INTO `t_plot` VALUES ('4830', '禹洲花园三期', '福建省', '厦门市', '思明区', null, '118.149763', '24.486581', '2018-01-03 20:05:03', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4831', '凯悦丽池大厦', '福建省', '厦门市', '思明区', null, '118.142554', '24.497427', '2018-01-03 20:05:04', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4832', '新港龙名府', '福建省', '厦门市', '思明区', null, '118.127103', '24.501537', '2018-01-03 20:05:04', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4833', '大洋山庄', '福建省', '厦门市', '思明区', null, '118.129483', '24.439145', '2018-01-03 20:05:04', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4834', '阳台山', '福建省', '厦门市', '思明区', null, '118.111041', '24.461295', '2018-01-03 20:05:04', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4835', '万寿山铁路小区', '福建省', '厦门市', '思明区', null, '118.110457', '24.468473', '2018-01-03 20:05:04', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4836', '盈翠里41-43号小区', '福建省', '厦门市', '思明区', null, '118.136517', '24.489114', '2018-01-03 20:05:04', null, '2018-01-04 19:27:04', null);
INSERT INTO `t_plot` VALUES ('4837', '华新苑', '福建省', '厦门市', '思明区', null, '118.092545', '24.481535', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4838', '鹭江海景一期', '福建省', '厦门市', '思明区', null, '118.09106', '24.45118', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4839', '锦绣广场', '福建省', '厦门市', '思明区', null, '118.133476', '24.495349', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4840', '嘉滨里', '福建省', '厦门市', '思明区', null, '118.131735', '24.494255', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4841', '邦妮大厦', '福建省', '厦门市', '思明区', null, '118.130725', '24.502505', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4842', '山海花园', '福建省', '厦门市', '思明区', null, '118.09538', '24.443043', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4843', '宏益大厦', '福建省', '厦门市', '思明区', null, '118.132875', '24.495586', '2018-01-03 20:05:04', null, '2018-01-04 19:27:05', null);
INSERT INTO `t_plot` VALUES ('4844', '仙岳东村', '福建省', '厦门市', '思明区', null, '118.115629', '24.498609', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4845', '金源花园', '福建省', '厦门市', '思明区', null, '118.114743', '24.4751', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4846', '南湖阁', '福建省', '厦门市', '思明区', null, '118.117579', '24.484246', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4847', '华盛花园', '福建省', '厦门市', '思明区', null, '118.090663', '24.468542', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4848', '运轩阁', '福建省', '厦门市', '思明区', null, '118.089674', '24.471346', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4849', '电化宿舍', '福建省', '厦门市', '思明区', null, '118.116952', '24.465616', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4850', '云亭花园', '福建省', '厦门市', '思明区', null, '118.14621', '24.482115', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4851', '建东大厦', '福建省', '厦门市', '思明区', null, '118.097118', '24.476458', '2018-01-03 20:05:04', null, '2018-01-04 19:27:06', null);
INSERT INTO `t_plot` VALUES ('4852', '毕升宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4853', '元利花园D区', '福建省', '厦门市', '思明区', null, '118.126558', '24.477269', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4854', '角滨路广电宿舍', '福建省', '厦门市', '思明区', null, '118.102504', '24.47462', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4855', '三航六公司宿舍', '福建省', '厦门市', '思明区', null, '118.110372', '24.480512', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4856', '思明苑', '福建省', '厦门市', '思明区', null, '118.0911', '24.45275', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4857', '红星公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4858', '利华苑', '福建省', '厦门市', '思明区', null, '118.088842', '24.48138', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4859', '美丽新世界', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:07', null);
INSERT INTO `t_plot` VALUES ('4860', '土地规划局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4861', '东方旺族', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4862', '海明苑', '福建省', '厦门市', '思明区', null, '118.104035', '24.475351', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4863', '龙山二期安置房', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4864', '秀德大厦', '福建省', '厦门市', '思明区', null, '118.094785', '24.460837', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4865', '海滨花园', '福建省', '厦门市', '思明区', null, '118.09025', '24.449823', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4866', '富仕公寓', '福建省', '厦门市', '思明区', null, '118.100646', '24.496673', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4867', '外贸宿舍', '福建省', '厦门市', '思明区', null, '118.10299', '24.473922', '2018-01-03 20:05:04', null, '2018-01-04 19:27:08', null);
INSERT INTO `t_plot` VALUES ('4868', '莲花一村', '福建省', '厦门市', '思明区', null, '118.136348', '24.489257', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4869', '湖光小区', '福建省', '厦门市', '思明区', null, '118.116575', '24.483427', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4870', '厦门日报社宿舍', '福建省', '厦门市', '思明区', null, '118.109829', '24.480795', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4871', '假日商城', '福建省', '厦门市', '思明区', null, '118.125777', '24.478183', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4872', '厦门医药站宿舍', '福建省', '厦门市', '思明区', null, '118.123087', '24.479454', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4873', '德馨大厦', '福建省', '厦门市', '思明区', null, '118.131999', '24.503277', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4874', '警备区宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4875', '华林花园三期', '福建省', '厦门市', '思明区', null, '118.175694', '24.477243', '2018-01-03 20:05:04', null, '2018-01-04 19:27:09', null);
INSERT INTO `t_plot` VALUES ('4876', '禾祥西广电宿舍', '福建省', '厦门市', '思明区', null, '118.102504', '24.47462', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4877', '莲坂村', '福建省', '厦门市', '思明区', null, '118.129374', '24.486738', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4878', '新家园', '福建省', '厦门市', '思明区', null, '118.090263', '24.470244', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4879', '华菲大厦', '福建省', '厦门市', '思明区', null, '118.094895', '24.460456', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4880', '香秀里海关宿舍', '福建省', '厦门市', '思明区', null, '118.109261', '24.4814', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4881', '弘龙商业城', '福建省', '厦门市', '思明区', null, '118.109239', '24.470468', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4882', '中山医院宿舍', '福建省', '厦门市', '思明区', null, '118.109375', '24.480276', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4883', '美湖花园', '福建省', '厦门市', '思明区', null, '118.096273', '24.472736', '2018-01-03 20:05:04', null, '2018-01-04 19:27:10', null);
INSERT INTO `t_plot` VALUES ('4884', '宝莲花园', '福建省', '厦门市', '思明区', null, '118.13684', '24.491532', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4885', '湖滨三里南', '福建省', '厦门市', '思明区', null, '118.114574', '24.480145', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4886', '龙景台', '福建省', '厦门市', '思明区', null, '118.11332', '24.477856', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4887', '洪山柄南区', '福建省', '厦门市', '思明区', null, '118.164658', '24.480642', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4888', '玉荷里', '福建省', '厦门市', '思明区', null, '118.130347', '24.486971', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4889', '五龙大厦', '福建省', '厦门市', '思明区', null, '118.100133', '24.475363', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4890', '康桥圣菲', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:11', null);
INSERT INTO `t_plot` VALUES ('4891', '天湖大厦', '福建省', '厦门市', '思明区', null, '118.116862', '24.486333', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4892', '湖滨南路国药宿舍', '福建省', '厦门市', '思明区', null, '118.113712', '24.47913', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4893', '文秀阁', '福建省', '厦门市', '思明区', null, '118.10848', '24.470162', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4894', '兴华路小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4895', '美仁后社', '福建省', '厦门市', '思明区', null, '118.098247', '24.472256', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4896', '彩霞公寓', '福建省', '厦门市', '思明区', null, '118.116015', '24.482588', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4897', '部队宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:12', null);
INSERT INTO `t_plot` VALUES ('4898', '大正大厦', '福建省', '厦门市', '思明区', null, '118.096646', '24.475423', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4899', '禾祥西公寓', '福建省', '厦门市', '思明区', null, '118.104661', '24.475489', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4900', '汇成花园', '福建省', '厦门市', '思明区', null, '118.130361', '24.487299', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4901', '凌香里', '福建省', '厦门市', '思明区', null, '118.135849', '24.492592', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4902', '台湾山庄福德堡(别墅)', '福建省', '厦门市', '思明区', null, '118.12901', '24.503728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4903', '海城花园', '福建省', '厦门市', '思明区', null, '118.082562', '24.467914', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4904', '天泉富邑', '福建省', '厦门市', '思明区', null, '118.134888', '24.431825', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4905', '经典大第', '福建省', '厦门市', '思明区', null, '118.088872', '24.469678', '2018-01-03 20:05:04', null, '2018-01-04 19:27:13', null);
INSERT INTO `t_plot` VALUES ('4906', '金秋豪园', '福建省', '厦门市', '思明区', null, '118.097199', '24.476818', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4907', '心悦园', '福建省', '厦门市', '思明区', null, '118.130895', '24.502983', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4908', '育青路15号前楼小区', '福建省', '厦门市', '思明区', null, '118.102051', '24.461888', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4909', '怡和大厦', '福建省', '厦门市', '思明区', null, '118.097902', '24.464108', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4910', '莲花别墅', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4911', '文兴社', '福建省', '厦门市', '思明区', null, '118.167537', '24.480028', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4912', '龙门世家', '福建省', '厦门市', '思明区', null, '118.123896', '24.484656', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4913', '绿杨村', '福建省', '厦门市', '思明区', null, '118.15078', '24.483794', '2018-01-03 20:05:04', null, '2018-01-04 19:27:14', null);
INSERT INTO `t_plot` VALUES ('4914', '防疫站宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4915', '永康大厦', '福建省', '厦门市', '思明区', null, '118.099978', '24.474584', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4916', '明发城', '福建省', '厦门市', '思明区', null, '118.181494', '24.489383', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4917', '松柏禾山家园', '福建省', '厦门市', '思明区', null, '118.128598', '24.501955', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4918', '金峰山庄', '福建省', '厦门市', '思明区', null, '118.114097', '24.470481', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4919', '检察院宿舍', '福建省', '厦门市', '思明区', null, '118.120041', '24.511004', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4920', '台光花园', '福建省', '厦门市', '思明区', null, '118.089608', '24.459595', '2018-01-03 20:05:04', null, '2018-01-04 19:27:15', null);
INSERT INTO `t_plot` VALUES ('4921', '绿泉新村', '福建省', '厦门市', '思明区', null, '118.12164', '24.47914', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4922', '莲坂新小区', '福建省', '厦门市', '思明区', null, '118.136071', '24.484317', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4923', '白鹤岩小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4924', '中裕花园', '福建省', '厦门市', '思明区', null, '118.097701', '24.450355', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4925', '潘宅南小区', '福建省', '厦门市', '思明区', null, '118.171364', '24.479904', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4926', '海德花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4927', '新港龙花园', '福建省', '厦门市', '思明区', null, '118.126923', '24.490502', '2018-01-03 20:05:04', null, '2018-01-04 19:27:16', null);
INSERT INTO `t_plot` VALUES ('4928', '佳苑星河', '福建省', '厦门市', '思明区', null, '118.096487', '24.476744', '2018-01-03 20:05:04', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4929', '金龙苑', '福建省', '厦门市', '思明区', null, '118.132933', '24.483261', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4930', '华林花园一期', '福建省', '厦门市', '思明区', null, '118.176193', '24.478173', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4931', '金龙花园', '福建省', '厦门市', '思明区', null, '118.129233', '24.493064', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4932', '新天地豪庭', '福建省', '厦门市', '思明区', null, '118.090969', '24.482216', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4933', '古楼广场', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4934', '裕盛园', '福建省', '厦门市', '思明区', null, '118.115797', '24.47913', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4935', '枫景阁', '福建省', '厦门市', '思明区', null, '118.117392', '24.475465', '2018-01-03 20:05:05', null, '2018-01-04 19:27:17', null);
INSERT INTO `t_plot` VALUES ('4936', '嘉怡园', '福建省', '厦门市', '思明区', null, '118.130895', '24.502982', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4937', '厦汽宿舍', '福建省', '厦门市', '思明区', null, '118.134593', '24.483265', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4938', '文园春晓', '福建省', '厦门市', '思明区', null, '118.109438', '24.468704', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4939', '假日花园', '福建省', '厦门市', '思明区', null, '118.126498', '24.478129', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4940', '瑞景新村蟠龙苑', '福建省', '厦门市', '思明区', null, '118.161945', '24.480394', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4941', '厦大北村小区', '福建省', '厦门市', '思明区', null, '118.10115', '24.447101', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4942', '宝福大厦', '福建省', '厦门市', '思明区', null, '118.126142', '24.483096', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4943', '仙岳花园', '福建省', '厦门市', '思明区', null, '118.125596', '24.501415', '2018-01-03 20:05:05', null, '2018-01-04 19:27:18', null);
INSERT INTO `t_plot` VALUES ('4944', '九州宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4945', '国贸宿舍', '福建省', '厦门市', '思明区', null, '118.083903', '24.467809', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4946', '汇成公寓', '福建省', '厦门市', '思明区', null, '118.129501', '24.498082', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4947', '光华大厦', '福建省', '厦门市', '思明区', null, '118.093838', '24.471215', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4948', '建发宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4949', '嘉华里', '福建省', '厦门市', '思明区', null, '118.13212', '24.492474', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4950', '半山御景一期', '福建省', '厦门市', '思明区', null, '118.117951', '24.50256', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4951', '鸿图嘉园一期', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:19', null);
INSERT INTO `t_plot` VALUES ('4952', '白鹿大厦', '福建省', '厦门市', '思明区', null, '118.095651', '24.455629', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4953', '家家景园', '福建省', '厦门市', '思明区', null, '118.150686', '24.490145', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4954', '文曾花苑', '福建省', '厦门市', '思明区', null, '118.127675', '24.443161', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4955', '阿里山大厦', '福建省', '厦门市', '思明区', null, '118.135483', '24.495107', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4956', '福建省四建宿舍', '福建省', '厦门市', '思明区', null, '118.109527', '24.471435', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4957', '九竹巷', '福建省', '厦门市', '思明区', null, '118.091573', '24.455929', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4958', '边检站宿舍', '福建省', '厦门市', '思明区', null, '118.133977', '24.483158', '2018-01-03 20:05:05', null, '2018-01-04 19:27:20', null);
INSERT INTO `t_plot` VALUES ('4959', '金成大厦', '福建省', '厦门市', '思明区', null, '118.089663', '24.470706', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4960', '华富大厦', '福建省', '厦门市', '思明区', null, '118.101229', '24.474962', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4961', '叉车厂宿舍', '福建省', '厦门市', '思明区', null, '118.085403', '24.467427', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4962', '上李新村', '福建省', '厦门市', '思明区', null, '118.130683', '24.444587', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4963', '滨南e居', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4964', '云顶亿华山庄', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4965', '翠湖公寓', '福建省', '厦门市', '思明区', null, '118.131585', '24.499053', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4966', '法院宿舍', '福建省', '厦门市', '思明区', null, '118.10672', '24.489819', '2018-01-03 20:05:05', null, '2018-01-04 19:27:21', null);
INSERT INTO `t_plot` VALUES ('4967', '华林花园二期', '福建省', '厦门市', '思明区', null, '118.177778', '24.479052', '2018-01-03 20:05:05', null, '2018-01-04 19:27:22', null);
INSERT INTO `t_plot` VALUES ('4968', '新景海韵园', '福建省', '厦门市', '思明区', null, '118.18603', '24.470638', '2018-01-03 20:05:05', null, '2018-01-04 19:27:22', null);
INSERT INTO `t_plot` VALUES ('4969', '岷宫花园大厦', '福建省', '厦门市', '思明区', null, '118.133158', '24.48768', '2018-01-03 20:05:05', null, '2018-01-04 19:27:22', null);
INSERT INTO `t_plot` VALUES ('4970', '厦门电机厂宿舍', '福建省', '厦门市', '思明区', null, '118.103904', '24.471183', '2018-01-03 20:05:05', null, '2018-01-04 19:27:22', null);
INSERT INTO `t_plot` VALUES ('4971', '中建大厦', '福建省', '厦门市', '思明区', null, '118.103289', '24.495915', '2018-01-03 20:05:05', null, '2018-01-04 19:27:22', null);
INSERT INTO `t_plot` VALUES ('4972', '立志花园', '福建省', '厦门市', '思明区', null, '118.128879', '24.442873', '2018-01-03 20:05:05', null, '2018-01-04 19:27:22', null);
INSERT INTO `t_plot` VALUES ('4973', '颐景苑', '福建省', '厦门市', '思明区', null, '118.127239', '24.478477', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4974', '宝龙一城', '福建省', '厦门市', '思明区', null, '118.178677', '24.491993', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4975', '油漆厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4976', '小学路民房', '福建省', '厦门市', '思明区', null, '118.083301', '24.467906', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4977', '名仕御园(别墅)', '福建省', '厦门市', '思明区', null, '118.105363', '24.466721', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4978', '海燕新村', '福建省', '厦门市', '思明区', null, '118.11254', '24.476213', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4979', '湖滨南财政局宿舍', '福建省', '厦门市', '思明区', null, '118.111463', '24.47756', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4980', '盈翠大厦', '福建省', '厦门市', '思明区', null, '118.131425', '24.489373', '2018-01-03 20:05:05', null, '2018-01-04 19:27:23', null);
INSERT INTO `t_plot` VALUES ('4981', '水晶国际', '福建省', '厦门市', '思明区', null, '118.195244', '24.502868', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4982', '映碧里', '福建省', '厦门市', '思明区', null, '118.131837', '24.485389', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4983', '阳明楼', '福建省', '厦门市', '思明区', null, '118.090625', '24.482985', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4984', '侨鑫公寓', '福建省', '厦门市', '思明区', null, '118.147604', '24.496678', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4985', '金属材料公司宿舍', '福建省', '厦门市', '思明区', null, '118.095325', '24.467945', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4986', '安溪大厦', '福建省', '厦门市', '思明区', null, '118.145655', '24.497738', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4987', '金都综合楼', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4988', '雅观园', '福建省', '厦门市', '思明区', null, '118.106337', '24.467866', '2018-01-03 20:05:05', null, '2018-01-04 19:27:24', null);
INSERT INTO `t_plot` VALUES ('4989', '仙岳山庄(别墅)', '福建省', '厦门市', '思明区', null, '118.106975', '24.496891', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4990', '香港时代广场', '福建省', '厦门市', '思明区', null, '118.087058', '24.462015', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4991', '海洋新村', '福建省', '厦门市', '思明区', null, '118.098878', '24.440929', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4992', '斗西路政协宿舍', '福建省', '厦门市', '思明区', null, '118.091966', '24.471916', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4993', '冠成大厦', '福建省', '厦门市', '思明区', null, '118.097363', '24.469475', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4994', '南华苑', '福建省', '厦门市', '思明区', null, '118.098593', '24.447871', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4995', '鑫阳公寓', '福建省', '厦门市', '思明区', null, '118.115976', '24.477677', '2018-01-03 20:05:05', null, '2018-01-04 19:27:25', null);
INSERT INTO `t_plot` VALUES ('4996', '江山帝景', '福建省', '厦门市', '思明区', null, '118.184811', '24.464127', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('4997', '毅宏公寓', '福建省', '厦门市', '思明区', null, '118.140136', '24.482135', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('4998', '松柏公寓', '福建省', '厦门市', '思明区', null, '118.127693', '24.498588', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('4999', '水产集团', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('5000', '外贸土产新村', '福建省', '厦门市', '思明区', null, '118.089969', '24.452764', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('5001', '鸿图嘉园二期', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('5002', '源成商住综合楼', '福建省', '厦门市', '思明区', null, '118.12084', '24.501156', '2018-01-03 20:05:05', null, '2018-01-04 19:27:26', null);
INSERT INTO `t_plot` VALUES ('5003', '洪山柄北区', '福建省', '厦门市', '思明区', null, '118.159987', '24.48687', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5004', '京华大厦', '福建省', '厦门市', '思明区', null, '118.127534', '24.479427', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5005', '五金宿舍', '福建省', '厦门市', '思明区', null, '118.123513', '24.489599', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5006', '民盛商厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5007', '明发楼', '福建省', '厦门市', '思明区', null, '118.164524', '24.486622', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5008', '龙辉花园', '福建省', '厦门市', '思明区', null, '118.128737', '24.479799', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5009', '海峡国际天璟', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5010', '学府花园', '福建省', '厦门市', '思明区', null, '118.099476', '24.446091', '2018-01-03 20:05:05', null, '2018-01-04 19:27:27', null);
INSERT INTO `t_plot` VALUES ('5011', '宝悦公寓', '福建省', '厦门市', '思明区', null, '118.096437', '24.474054', '2018-01-03 20:05:05', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5012', '建发花园二期', '福建省', '厦门市', '思明区', null, '118.132297', '24.478698', '2018-01-03 20:05:05', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5013', '万景公寓', '福建省', '厦门市', '思明区', null, '118.171293', '24.489135', '2018-01-03 20:05:05', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5014', '瑞馨阁', '福建省', '厦门市', '思明区', null, '118.170358', '24.480387', '2018-01-03 20:05:05', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5015', '永升丽景', '福建省', '厦门市', '思明区', null, '118.10175', '24.477304', '2018-01-03 20:05:05', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5016', '罐头厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5017', '中铁大厦', '福建省', '厦门市', '思明区', null, '118.102614', '24.464691', '2018-01-03 20:05:06', null, '2018-01-04 19:27:28', null);
INSERT INTO `t_plot` VALUES ('5018', '华润苑', '福建省', '厦门市', '思明区', null, '118.093983', '24.459602', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5019', '思明农行宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5020', '建业大厦', '福建省', '厦门市', '思明区', null, '118.089675', '24.484099', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5021', '福津大街小区五、六节点', '福建省', '厦门市', '思明区', null, '118.118973', '24.489095', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5022', '联谊广场', '福建省', '厦门市', '思明区', null, '118.124461', '24.490905', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5023', '市政工程宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5024', '草埔巷小区', '福建省', '厦门市', '思明区', null, '118.090019', '24.459894', '2018-01-03 20:05:06', null, '2018-01-04 19:27:29', null);
INSERT INTO `t_plot` VALUES ('5025', '海洋花园', '福建省', '厦门市', '思明区', null, '118.096148', '24.447616', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5026', '五交化工宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5027', '世茂海峡大厦', '福建省', '厦门市', '思明区', null, '118.096222', '24.442196', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5028', '利海苑', '福建省', '厦门市', '思明区', null, '118.125159', '24.492378', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5029', '怡富花园', '福建省', '厦门市', '思明区', null, '118.14456', '24.481822', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5030', '丰联达大厦', '福建省', '厦门市', '思明区', null, '118.136809', '24.490952', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5031', '古龙综合楼', '福建省', '厦门市', '思明区', null, '118.107978', '24.475385', '2018-01-03 20:05:06', null, '2018-01-04 19:27:30', null);
INSERT INTO `t_plot` VALUES ('5032', '中烟工业大厦', '福建省', '厦门市', '思明区', null, '118.124237', '24.494978', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5033', '罗宾森璀璨新城二期', '福建省', '厦门市', '思明区', null, '118.07961', '24.467504', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5034', '海峡国际社区二期', '福建省', '厦门市', '思明区', null, '118.196718', '24.483843', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5035', '残联宿舍', '福建省', '厦门市', '思明区', null, '118.14809', '24.490073', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5036', '文渊海景苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5037', '嘉年华大厦', '福建省', '厦门市', '思明区', null, '118.083449', '24.461286', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5038', '浪琴苑', '福建省', '厦门市', '思明区', null, '118.094167', '24.443262', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5039', '大同学府', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:31', null);
INSERT INTO `t_plot` VALUES ('5040', '莲前西路71-77号小区', '福建省', '厦门市', '思明区', null, '118.132061', '24.483312', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5041', '中行宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5042', '领秀城', '福建省', '厦门市', '思明区', null, '118.117006', '24.467057', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5043', '虹霞大厦', '福建省', '厦门市', '思明区', null, '118.089108', '24.461347', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5044', '通士达小区', '福建省', '厦门市', '思明区', null, '118.147186', '24.488126', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5045', '不见天', '福建省', '厦门市', '思明区', null, '118.098823', '24.450888', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5046', '侨福城小区二期', '福建省', '厦门市', '思明区', null, '118.169076', '24.484005', '2018-01-03 20:05:06', null, '2018-01-04 19:27:32', null);
INSERT INTO `t_plot` VALUES ('5047', '建花宿舍', '福建省', '厦门市', '思明区', null, '118.108909', '24.478563', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5048', '建发花园三期', '福建省', '厦门市', '思明区', null, '118.130327', '24.479107', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5049', '育秀厦工宿舍', '福建省', '厦门市', '思明区', null, '118.118803', '24.493292', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5050', '璟湾二十八玺', '福建省', '厦门市', '思明区', null, '118.180673', '24.4634', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5051', '特房G01新座', '福建省', '厦门市', '思明区', null, '118.091954', '24.468536', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5052', '海关宿舍', '福建省', '厦门市', '思明区', null, '118.109261', '24.4814', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5053', '中信广场', '福建省', '厦门市', '思明区', null, '118.090542', '24.485234', '2018-01-03 20:05:06', null, '2018-01-04 19:27:33', null);
INSERT INTO `t_plot` VALUES ('5055', '金帝花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5056', '文屏花园', '福建省', '厦门市', '思明区', null, '118.112732', '24.471676', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5057', '禹洲云顶国际', '福建省', '厦门市', '思明区', null, '118.157388', '24.48165', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5058', '华林东盛花园二期', '福建省', '厦门市', '思明区', null, '118.179624', '24.482317', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5059', '大洲国际龙郡(别墅)', '福建省', '厦门市', '思明区', null, '118.18707', '24.481807', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5060', '中关委大厦', '福建省', '厦门市', '思明区', null, '118.132582', '24.501406', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5061', '亿力音乐花园二期瑟园', '福建省', '厦门市', '思明区', null, '118.095071', '24.468726', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5062', '阳光海岸', '福建省', '厦门市', '思明区', null, '118.17128', '24.45325', '2018-01-03 20:05:06', null, '2018-01-04 19:27:34', null);
INSERT INTO `t_plot` VALUES ('5063', '中山路香水城', '福建省', '厦门市', '思明区', null, '118.087638', '24.460409', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5064', '莲花二村玉亭里', '福建省', '厦门市', '思明区', null, '118.139301', '24.491925', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5065', '万寿花园', '福建省', '厦门市', '思明区', null, '118.109206', '24.466928', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5066', '湖滨南路44-48号', '福建省', '厦门市', '思明区', null, '118.093689', '24.473036', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5067', '嘉莲大厦', '福建省', '厦门市', '思明区', null, '118.133012', '24.492414', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5068', '禾祥西轮船厂宿舍', '福建省', '厦门市', '思明区', null, '118.104661', '24.475489', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5069', '福津大街小区七节点', '福建省', '厦门市', '思明区', null, '118.118973', '24.489095', '2018-01-03 20:05:06', null, '2018-01-04 19:27:35', null);
INSERT INTO `t_plot` VALUES ('5070', '希望大厦', '福建省', '厦门市', '思明区', null, '118.102471', '24.475654', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5071', '福津大街四节点', '福建省', '厦门市', '思明区', null, '118.118973', '24.489095', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5072', '虎园路海关宿舍', '福建省', '厦门市', '思明区', null, '118.109261', '24.4814', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5073', '陶然北岸', '福建省', '厦门市', '思明区', null, '118.097196', '24.4951', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5074', '凤凰山庄(别墅)', '福建省', '厦门市', '思明区', null, '118.092202', '24.48593', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5076', '岭兜佳园小区', '福建省', '厦门市', '思明区', null, '118.182398', '24.491377', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5077', '文园路小区', '福建省', '厦门市', '思明区', null, '118.104691', '24.465961', '2018-01-03 20:05:06', null, '2018-01-04 19:27:36', null);
INSERT INTO `t_plot` VALUES ('5078', '仙阁里小区', '福建省', '厦门市', '思明区', null, '118.12305', '24.503065', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5079', '鹭江新城四期', '福建省', '厦门市', '思明区', null, '118.137656', '24.496666', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5080', '建兴小区', '福建省', '厦门市', '思明区', null, '118.093938', '24.482006', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5081', '石壁街36号小区', '福建省', '厦门市', '思明区', null, '118.09158', '24.458517', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5082', '角滨路宿舍房', '福建省', '厦门市', '思明区', null, '118.088081', '24.468659', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5083', '特房尊府', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5084', '伴山公寓', '福建省', '厦门市', '思明区', null, '118.131505', '24.475183', '2018-01-03 20:05:06', null, '2018-01-04 19:27:37', null);
INSERT INTO `t_plot` VALUES ('5085', '新景世纪城A区', '福建省', '厦门市', '思明区', null, '118.124611', '24.484023', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5086', '名汇广场公寓', '福建省', '厦门市', '思明区', null, '118.087978', '24.462789', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5087', '海艺大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5088', '上东美地', '福建省', '厦门市', '思明区', null, '118.178287', '24.489814', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5089', '裕成大厦', '福建省', '厦门市', '思明区', null, '118.083906', '24.484971', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5090', '大学路小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5091', '湖滨一里64-67号院', '福建省', '厦门市', '思明区', null, '118.109247', '24.48063', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5092', '禾祥西路小区', '福建省', '厦门市', '思明区', null, '118.097786', '24.47251', '2018-01-03 20:05:06', null, '2018-01-04 19:27:38', null);
INSERT INTO `t_plot` VALUES ('5093', '鼓浪屿别墅', '福建省', '厦门市', '思明区', null, '118.073672', '24.45234', '2018-01-03 20:05:06', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5094', '嘉盛豪园G组团', '福建省', '厦门市', '思明区', null, '118.161274', '24.486866', '2018-01-03 20:05:06', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5095', '瑞景新村祥驎苑', '福建省', '厦门市', '思明区', null, '118.161945', '24.480394', '2018-01-03 20:05:06', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5096', '新莲花新龙山花园二期', '福建省', '厦门市', '思明区', null, '118.143288', '24.491004', '2018-01-03 20:05:06', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5097', '仙岳嘉园', '福建省', '厦门市', '思明区', null, '118.123661', '24.501404', '2018-01-03 20:05:06', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5098', '富山花园', '福建省', '厦门市', '思明区', null, '118.124057', '24.482544', '2018-01-03 20:05:06', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5099', '玉滨城一期', '福建省', '厦门市', '思明区', null, '118.094895', '24.462856', '2018-01-03 20:05:07', null, '2018-01-04 19:27:39', null);
INSERT INTO `t_plot` VALUES ('5100', '怡富花园一期', '福建省', '厦门市', '思明区', null, '118.14249', '24.481351', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5101', '物资局宿舍楼', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5102', '源昌国际城一期', '福建省', '厦门市', '思明区', null, '118.0878', '24.46727', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5103', '文屏巷59号小区', '福建省', '厦门市', '思明区', null, '118.111798', '24.469762', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5104', '厦港新村', '福建省', '厦门市', '思明区', null, '118.097701', '24.45061', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5105', '侨建花园西区', '福建省', '厦门市', '思明区', null, '118.121392', '24.500274', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5106', '南湖新村', '福建省', '厦门市', '思明区', null, '118.114381', '24.488176', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5107', '瑞景商业广场', '福建省', '厦门市', '思明区', null, '118.166143', '24.48073', '2018-01-03 20:05:07', null, '2018-01-04 19:27:40', null);
INSERT INTO `t_plot` VALUES ('5108', '福津大街小区三节点', '福建省', '厦门市', '思明区', null, '118.118973', '24.489095', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5109', '塔埔社区东区', '福建省', '厦门市', '思明区', null, '118.200425', '24.50218', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5110', '江头(长青)K小区', '福建省', '厦门市', '思明区', null, '118.138506', '24.501695', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5111', '新景5.8', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5112', '福津大街一节点', '福建省', '厦门市', '思明区', null, '118.122228', '24.479665', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5113', '新莲花新龙山花园一期', '福建省', '厦门市', '思明区', null, '118.143288', '24.491004', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5114', '宝华花园', '福建省', '厦门市', '思明区', null, '118.131392', '24.495215', '2018-01-03 20:05:07', null, '2018-01-04 19:27:41', null);
INSERT INTO `t_plot` VALUES ('5115', '鹭江新城三期', '福建省', '厦门市', '思明区', null, '118.138727', '24.495978', '2018-01-03 20:05:07', null, '2018-01-04 19:27:42', null);
INSERT INTO `t_plot` VALUES ('5116', '文屏山庄二期', '福建省', '厦门市', '思明区', null, '118.122926', '24.463299', '2018-01-03 20:05:07', null, '2018-01-04 19:27:42', null);
INSERT INTO `t_plot` VALUES ('5117', '前埔南区文兴一里', '福建省', '厦门市', '思明区', null, '118.16675', '24.477347', '2018-01-03 20:05:07', null, '2018-01-04 19:27:42', null);
INSERT INTO `t_plot` VALUES ('5119', '鸿运大厦', '福建省', '厦门市', '思明区', null, '118.120447', '24.477626', '2018-01-03 20:05:07', null, '2018-01-04 19:27:42', null);
INSERT INTO `t_plot` VALUES ('5120', '侨旺大厦', '福建省', '厦门市', '思明区', null, '118.132472', '24.49325', '2018-01-03 20:05:07', null, '2018-01-04 19:27:42', null);
INSERT INTO `t_plot` VALUES ('5121', '罗宾森广场三期', '福建省', '厦门市', '思明区', null, '118.117743', '24.474424', '2018-01-03 20:05:07', null, '2018-01-04 19:27:42', null);
INSERT INTO `t_plot` VALUES ('5122', '恒达大厦', '福建省', '厦门市', '思明区', null, '118.095179', '24.443744', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5123', '前埔南小区(店上西里)', '福建省', '厦门市', '思明区', null, '118.179854', '24.47174', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5124', '长青路180号小区', '福建省', '厦门市', '思明区', null, '118.128258', '24.491507', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5125', '湖滨南路142-144号', '福建省', '厦门市', '思明区', null, '118.102371', '24.476066', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5126', '南中大地广场', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5127', '侨福城小区三期', '福建省', '厦门市', '思明区', null, '118.169076', '24.484005', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5128', '幸福路别墅', '福建省', '厦门市', '思明区', null, '118.092333', '24.467083', '2018-01-03 20:05:07', null, '2018-01-04 19:27:43', null);
INSERT INTO `t_plot` VALUES ('5129', '浦南公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5131', '莲富大厦', '福建省', '厦门市', '思明区', null, '118.127737', '24.481774', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5132', '武汉大厦', '福建省', '厦门市', '思明区', null, '118.130274', '24.491159', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5133', '湖滨三里', '福建省', '厦门市', '思明区', null, '118.11248', '24.481502', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5134', '红杏花园', '福建省', '厦门市', '思明区', null, '118.128053', '24.480332', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5135', '厦航洪文小区', '福建省', '厦门市', '思明区', null, '118.169518', '24.478001', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5136', '富兴大厦', '福建省', '厦门市', '思明区', null, '118.126721', '24.479099', '2018-01-03 20:05:07', null, '2018-01-04 19:27:44', null);
INSERT INTO `t_plot` VALUES ('5137', '云顶庄园', '福建省', '厦门市', '思明区', null, '118.15967', '24.460977', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5138', '玉滨城二期', '福建省', '厦门市', '思明区', null, '118.093804', '24.462664', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5139', '福津大街八节点', '福建省', '厦门市', '思明区', null, '118.118973', '24.489095', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5140', '汇景园', '福建省', '厦门市', '思明区', null, '118.066693', '24.452759', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5141', '厦大西村', '福建省', '厦门市', '思明区', null, '118.099318', '24.444841', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5142', '阜康大厦', '福建省', '厦门市', '思明区', null, '118.131092', '24.489457', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5143', '西堤公寓', '福建省', '厦门市', '思明区', null, '118.08448', '24.471532', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5144', '食品厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:45', null);
INSERT INTO `t_plot` VALUES ('5145', '龙盛花园', '福建省', '厦门市', '思明区', null, '118.147948', '24.490558', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5147', '禾祥西厦罐宿舍', '福建省', '厦门市', '思明区', null, '118.104661', '24.475489', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5148', '龙华里居住区', '福建省', '厦门市', '思明区', null, '118.14389', '24.493381', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5149', '银兴苑', '福建省', '厦门市', '思明区', null, '118.113229', '24.476938', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5150', '先锋营小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5151', '万寿路28号院', '福建省', '厦门市', '思明区', null, '118.111082', '24.465285', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5152', '一七四医院主任楼', '福建省', '厦门市', '思明区', null, '118.101544', '24.465095', '2018-01-03 20:05:07', null, '2018-01-04 19:27:46', null);
INSERT INTO `t_plot` VALUES ('5153', '水仙路48号小区', '福建省', '厦门市', '思明区', null, '118.082838', '24.458954', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5154', '定安路36号小区', '福建省', '厦门市', '思明区', null, '118.086117', '24.45977', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5155', '教育局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5156', '联丰商城二期', '福建省', '厦门市', '思明区', null, '118.167281', '24.483084', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5157', '钨钢厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5158', '轮渡私宅', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5159', '二轻宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:47', null);
INSERT INTO `t_plot` VALUES ('5160', '新港广场', '福建省', '厦门市', '思明区', null, '118.086274', '24.483694', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5161', '佘厝新村', '福建省', '厦门市', '思明区', null, '118.130262', '24.500511', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5162', '邮电宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5163', '凌志大厦', '福建省', '厦门市', '思明区', null, '118.13382', '24.488062', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5164', '湖滨南路湖滨一里11-13号', '福建省', '厦门市', '思明区', null, '118.108193', '24.478449', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5165', '水务宿舍(体育路)', '福建省', '厦门市', '思明区', null, '118.110996', '24.492922', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5166', '汇腾大厦', '福建省', '厦门市', '思明区', null, '118.133957', '24.496737', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5167', '湖滨中路11-29号小区', '福建省', '厦门市', '思明区', null, '118.109332', '24.474757', '2018-01-03 20:05:07', null, '2018-01-04 19:27:48', null);
INSERT INTO `t_plot` VALUES ('5168', '瑞景新村寿鹤苑', '福建省', '厦门市', '思明区', null, '118.161945', '24.480394', '2018-01-03 20:05:07', null, '2018-01-04 19:27:49', null);
INSERT INTO `t_plot` VALUES ('5169', '湖滨南路182号', '福建省', '厦门市', '思明区', null, '118.105361', '24.476832', '2018-01-03 20:05:07', null, '2018-01-04 19:27:49', null);
INSERT INTO `t_plot` VALUES ('5171', '能群大厦', '福建省', '厦门市', '思明区', null, '118.126438', '24.483408', '2018-01-03 20:05:07', null, '2018-01-04 19:27:49', null);
INSERT INTO `t_plot` VALUES ('5172', '国际广场', '福建省', '厦门市', '思明区', null, '118.087954', '24.483553', '2018-01-03 20:05:07', null, '2018-01-04 19:27:49', null);
INSERT INTO `t_plot` VALUES ('5173', '边防武警公寓', '福建省', '厦门市', '思明区', null, '118.13107', '24.510741', '2018-01-03 20:05:07', null, '2018-01-04 19:27:49', null);
INSERT INTO `t_plot` VALUES ('5174', '深田路57号小区', '福建省', '厦门市', '思明区', null, '118.098202', '24.464195', '2018-01-03 20:05:07', null, '2018-01-04 19:27:49', null);
INSERT INTO `t_plot` VALUES ('5176', '鸿图大厦', '福建省', '厦门市', '思明区', null, '118.091049', '24.45584', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5177', '建设大厦', '福建省', '厦门市', '思明区', null, '118.090702', '24.467241', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5178', '新路街115号小区', '福建省', '厦门市', '思明区', null, '118.08126', '24.461248', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5179', '聚泰大厦', '福建省', '厦门市', '思明区', null, '118.127047', '24.481127', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5180', '龙盛里居住区', '福建省', '厦门市', '思明区', null, '118.145509', '24.493902', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5181', '漳州路17号小区', '福建省', '厦门市', '思明区', null, '118.081052', '24.447027', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5182', '百家村电信宿舍', '福建省', '厦门市', '思明区', null, '118.100598', '24.464487', '2018-01-03 20:05:08', null, '2018-01-04 19:27:50', null);
INSERT INTO `t_plot` VALUES ('5183', '东芳山庄(别墅)', '福建省', '厦门市', '思明区', null, '118.153208', '24.484827', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5184', '涵光路17-19号小区', '福建省', '厦门市', '思明区', null, '118.129331', '24.486967', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5185', '白鹤山路14号小区', '福建省', '厦门市', '思明区', null, '118.101663', '24.468375', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5186', '假日酒店公寓', '福建省', '厦门市', '思明区', null, '118.133769', '24.487414', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5187', '兴华路15号小区', '福建省', '厦门市', '思明区', null, '118.095676', '24.459429', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5188', '万寿苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5189', '万禾广场', '福建省', '厦门市', '思明区', null, '118.125484', '24.482922', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5190', '四季阁', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:51', null);
INSERT INTO `t_plot` VALUES ('5191', '南洋大厦', '福建省', '厦门市', '思明区', null, '118.115351', '24.473977', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5192', '槟榔东里(单号区)', '福建省', '厦门市', '思明区', null, '118.121085', '24.483826', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5193', '玉滨城三期', '福建省', '厦门市', '思明区', null, '118.092752', '24.462391', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5194', '岭兜小区中区', '福建省', '厦门市', '思明区', null, '118.186522', '24.485028', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5195', '太子山庄', '福建省', '厦门市', '思明区', null, '118.108895', '24.466253', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5196', '海景公寓', '福建省', '厦门市', '思明区', null, '118.088367', '24.457738', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5197', '富宇大厦', '福建省', '厦门市', '思明区', null, '118.129014', '24.495658', '2018-01-03 20:05:08', null, '2018-01-04 19:27:52', null);
INSERT INTO `t_plot` VALUES ('5198', '前埔北区', '福建省', '厦门市', '思明区', null, '118.174803', '24.482198', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5199', '莲坂新村三区', '福建省', '厦门市', '思明区', null, '118.12411', '24.487485', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5200', '蜂巢山路小区', '福建省', '厦门市', '思明区', null, '118.095117', '24.447484', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5201', '红星瑞景小区', '福建省', '厦门市', '思明区', null, '118.162919', '24.476766', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5202', '定安路35-41号小区', '福建省', '厦门市', '思明区', null, '118.086584', '24.459501', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5203', '交警宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5204', '鹭悦', '福建省', '厦门市', '思明区', null, '118.12899', '24.442132', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5205', '航管小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:53', null);
INSERT INTO `t_plot` VALUES ('5206', '当代半岛', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5207', '莲花二村蓉芳里', '福建省', '厦门市', '思明区', null, '118.137846', '24.494456', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5208', '外贸新村', '福建省', '厦门市', '思明区', null, '118.089409', '24.451016', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5209', '南华路15号小区', '福建省', '厦门市', '思明区', null, '118.100719', '24.448245', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5210', '富华广场', '福建省', '厦门市', '思明区', null, '118.09547', '24.470202', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5211', '阳台佳苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5212', '光明馨苑', '福建省', '厦门市', '思明区', null, '118.091676', '24.469057', '2018-01-03 20:05:08', null, '2018-01-04 19:27:54', null);
INSERT INTO `t_plot` VALUES ('5213', '特房尊墅', '福建省', '厦门市', '思明区', null, '118.10325', '24.472133', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5214', '厦门税务局职工宿舍', '福建省', '厦门市', '思明区', null, '118.129745', '24.498071', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5215', '湖滨南路164号', '福建省', '厦门市', '思明区', null, '118.104494', '24.476191', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5216', '思明区政府镇海路宿舍楼', '福建省', '厦门市', '思明区', null, '118.095915', '24.485821', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5217', '龙山综合楼', '福建省', '厦门市', '思明区', null, '118.148743', '24.490846', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5218', '湖滨一里小区', '福建省', '厦门市', '思明区', null, '118.10882', '24.480729', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5219', '松柏大厦', '福建省', '厦门市', '思明区', null, '118.123761', '24.495817', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5220', '嘉盛豪园二期', '福建省', '厦门市', '思明区', null, '118.160872', '24.48434', '2018-01-03 20:05:08', null, '2018-01-04 19:27:55', null);
INSERT INTO `t_plot` VALUES ('5221', '公园西路1-300小区', '福建省', '厦门市', '思明区', null, '118.093634', '24.463285', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5222', '华瑞花园', '福建省', '厦门市', '思明区', null, '118.164706', '24.479643', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5223', '湖滨中路59-83号', '福建省', '厦门市', '思明区', null, '118.108049', '24.476069', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5224', '山海筼筜', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5225', '烟草宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5226', '繁荣广场二期', '福建省', '厦门市', '思明区', null, '118.098749', '24.473821', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5227', '曾厝垵西里小区', '福建省', '厦门市', '思明区', null, '118.121415', '24.440677', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5228', '和乐苑小区', '福建省', '厦门市', '思明区', null, '118.117232', '24.483314', '2018-01-03 20:05:08', null, '2018-01-04 19:27:56', null);
INSERT INTO `t_plot` VALUES ('5229', '湖滨南路72号', '福建省', '厦门市', '思明区', null, '118.096616', '24.474583', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5230', '华新路老别墅', '福建省', '厦门市', '思明区', null, '118.093805', '24.466054', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5231', '瑞景双座', '福建省', '厦门市', '思明区', null, '118.165091', '24.48549', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5232', '育青路17-23号楼', '福建省', '厦门市', '思明区', null, '118.106485', '24.463163', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5233', '岳北花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5234', '厦航洪文居住小区', '福建省', '厦门市', '思明区', null, '118.156777', '24.484754', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5235', '融景居', '福建省', '厦门市', '思明区', null, '118.099915', '24.476458', '2018-01-03 20:05:08', null, '2018-01-04 19:27:57', null);
INSERT INTO `t_plot` VALUES ('5236', '石泉路小区', '福建省', '厦门市', '思明区', null, '118.093562', '24.456515', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5237', '镇海大厦', '福建省', '厦门市', '思明区', null, '118.092971', '24.4584', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5238', '海光大厦', '福建省', '厦门市', '思明区', null, '118.082569', '24.459393', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5239', '台亚大厦', '福建省', '厦门市', '思明区', null, '118.131328', '24.491955', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5240', '岭兜二里', '福建省', '厦门市', '思明区', null, '118.185466', '24.485238', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5241', '瑞景新村鸣凤苑', '福建省', '厦门市', '思明区', null, '118.161945', '24.480394', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5242', '莲前西路水务宿舍', '福建省', '厦门市', '思明区', null, '118.14475', '24.482737', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5243', '金海豪园(别墅)', '福建省', '厦门市', '思明区', null, '118.187522', '24.465491', '2018-01-03 20:05:08', null, '2018-01-04 19:27:58', null);
INSERT INTO `t_plot` VALUES ('5244', '思明南路300号小区', '福建省', '厦门市', '思明区', null, '118.094024', '24.450252', '2018-01-03 20:05:08', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5245', '湖滨一里84号-85号院', '福建省', '厦门市', '思明区', null, '118.108616', '24.481351', '2018-01-03 20:05:08', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5246', '思明南路290-296号小区', '福建省', '厦门市', '思明区', null, '118.090739', '24.453601', '2018-01-03 20:05:08', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5247', '溪岸路73号小区', '福建省', '厦门市', '思明区', null, '118.096079', '24.467486', '2018-01-03 20:05:09', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5248', '厦门豪公馆', '福建省', '厦门市', '思明区', null, '118.089984', '24.460425', '2018-01-03 20:05:09', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5249', '育青路教卫大院', '福建省', '厦门市', '思明区', null, '118.104722', '24.463178', '2018-01-03 20:05:09', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5250', '钨业宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:27:59', null);
INSERT INTO `t_plot` VALUES ('5251', '鹭江新城二期', '福建省', '厦门市', '思明区', null, '118.138868', '24.495717', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5252', '阳光visa', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5253', '文屏小区', '福建省', '厦门市', '思明区', null, '118.123232', '24.463434', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5254', '大中路小区', '福建省', '厦门市', '思明区', null, '118.083748', '24.461597', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5255', '嘉莲花园', '福建省', '厦门市', '思明区', null, '118.13338', '24.49148', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5256', '莲前西路法院宿舍', '福建省', '厦门市', '思明区', null, '118.10672', '24.489819', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5257', '万寿阁', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5258', '西林东路小区', '福建省', '厦门市', '思明区', null, '118.153033', '24.479444', '2018-01-03 20:05:09', null, '2018-01-04 19:28:00', null);
INSERT INTO `t_plot` VALUES ('5259', '开元路225号小区', '福建省', '厦门市', '思明区', null, '118.084495', '24.464834', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5260', '湖滨二里65-68号', '福建省', '厦门市', '思明区', null, '118.110463', '24.481869', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5261', '岭兜小区北区', '福建省', '厦门市', '思明区', null, '118.188537', '24.48632', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5262', '电业小区西区', '福建省', '厦门市', '思明区', null, '118.105432', '24.462889', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5263', '白鹤山路28号小区', '福建省', '厦门市', '思明区', null, '118.101663', '24.468375', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5264', '龙祥花园', '福建省', '厦门市', '思明区', null, '118.115376', '24.476029', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5265', '禾祥西民房', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:01', null);
INSERT INTO `t_plot` VALUES ('5266', '万寿隆公寓', '福建省', '厦门市', '思明区', null, '118.116071', '24.465643', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5267', '鹭腾花园之文华阁', '福建省', '厦门市', '思明区', null, '118.113718', '24.474639', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5268', '筼筜西小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5269', '湖光路88-91号院', '福建省', '厦门市', '思明区', null, '118.117924', '24.484373', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5270', '假日e时代', '福建省', '厦门市', '思明区', null, '118.095479', '24.475136', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5271', '湖滨西路5号之一', '福建省', '厦门市', '思明区', null, '118.085507', '24.46751', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5272', '双民花园', '福建省', '厦门市', '思明区', null, '118.125277', '24.460536', '2018-01-03 20:05:09', null, '2018-01-04 19:28:02', null);
INSERT INTO `t_plot` VALUES ('5273', '湖滨南路336-338号院', '福建省', '厦门市', '思明区', null, '118.124014', '24.480699', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5274', '草埔巷4-8号小区', '福建省', '厦门市', '思明区', null, '118.090215', '24.459645', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5275', '福星商业中心', '福建省', '厦门市', '思明区', null, '118.14071', '24.497454', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5276', '双莲池科普示范楼苑', '福建省', '厦门市', '思明区', null, '118.090431', '24.465494', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5277', '湖明路64号院', '福建省', '厦门市', '思明区', null, '118.123758', '24.486819', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5278', '莲坂新村二区', '福建省', '厦门市', '思明区', null, '118.12411', '24.487485', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5279', '侨建大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:03', null);
INSERT INTO `t_plot` VALUES ('5280', '化工宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5281', '天泉富邑(别墅)', '福建省', '厦门市', '思明区', null, '118.134888', '24.431825', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5282', '六中教师宿舍', '福建省', '厦门市', '思明区', null, '118.094399', '24.500846', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5283', '嘉盛花园', '福建省', '厦门市', '思明区', null, '118.158107', '24.483708', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5284', '嘉禾商业中心', '福建省', '厦门市', '思明区', null, '118.115289', '24.479781', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5285', '厦大西村B区', '福建省', '厦门市', '思明区', null, '118.099318', '24.444841', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5287', '联丰商城五期', '福建省', '厦门市', '思明区', null, '118.167281', '24.483084', '2018-01-03 20:05:09', null, '2018-01-04 19:28:04', null);
INSERT INTO `t_plot` VALUES ('5288', '阳台山公安宿舍', '福建省', '厦门市', '思明区', null, '118.109142', '24.46423', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5289', '建委宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5290', '厦门市邮电局仙岳宿舍', '福建省', '厦门市', '思明区', null, '118.110809', '24.495323', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5291', '龙翔花园', '福建省', '厦门市', '思明区', null, '118.144262', '24.49522', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5292', '电业小区东区', '福建省', '厦门市', '思明区', null, '118.105432', '24.462889', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5293', '公安宿舍楼(凤屿路)', '福建省', '厦门市', '思明区', null, '118.121128', '24.481417', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5294', '百家阁', '福建省', '厦门市', '思明区', null, '118.100678', '24.464486', '2018-01-03 20:05:09', null, '2018-01-04 19:28:05', null);
INSERT INTO `t_plot` VALUES ('5295', '富山公寓', '福建省', '厦门市', '思明区', null, '118.138742', '24.480044', '2018-01-03 20:05:09', null, '2018-01-04 19:28:06', null);
INSERT INTO `t_plot` VALUES ('5297', '粮食局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:06', null);
INSERT INTO `t_plot` VALUES ('5298', '美兰大厦', '福建省', '厦门市', '思明区', null, '118.095416', '24.472549', '2018-01-03 20:05:09', null, '2018-01-04 19:28:06', null);
INSERT INTO `t_plot` VALUES ('5299', '厦门造纸厂翻建宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:06', null);
INSERT INTO `t_plot` VALUES ('5300', '涵光路11号小区', '福建省', '厦门市', '思明区', null, '118.129055', '24.487187', '2018-01-03 20:05:09', null, '2018-01-04 19:28:06', null);
INSERT INTO `t_plot` VALUES ('5301', '莲坂村567号楼', '福建省', '厦门市', '思明区', null, '118.129374', '24.486738', '2018-01-03 20:05:09', null, '2018-01-04 19:28:06', null);
INSERT INTO `t_plot` VALUES ('5302', '航管宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5303', '元利花园', '福建省', '厦门市', '思明区', null, '118.127047', '24.477512', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5304', '石亭路21-25号小区', '福建省', '厦门市', '思明区', null, '118.097861', '24.487361', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5305', '湖滨南宿舍', '福建省', '厦门市', '思明区', null, '118.111463', '24.47756', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5306', '道平路8号', '福建省', '厦门市', '思明区', null, '118.082511', '24.465023', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5307', '鸿山花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5308', '外贸局单位宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:09', null, '2018-01-04 19:28:07', null);
INSERT INTO `t_plot` VALUES ('5309', '市建宿舍', '福建省', '厦门市', '思明区', null, '118.126166', '24.48791', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5310', '湖滨南路18号', '福建省', '厦门市', '思明区', null, '118.089675', '24.472736', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5311', '源泉山庄C区', '福建省', '厦门市', '思明区', null, '118.172751', '24.474399', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5312', '龙翔里', '福建省', '厦门市', '思明区', null, '118.144514', '24.495659', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5313', '绿洲花园', '福建省', '厦门市', '思明区', null, '118.173125', '24.478236', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5314', '电业小区', '福建省', '厦门市', '思明区', null, '118.105432', '24.462889', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5315', '湖滨南路160号', '福建省', '厦门市', '思明区', null, '118.104251', '24.476469', '2018-01-03 20:05:09', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5316', '侨建小区', '福建省', '厦门市', '思明区', null, '118.15439', '24.478633', '2018-01-03 20:05:10', null, '2018-01-04 19:28:08', null);
INSERT INTO `t_plot` VALUES ('5317', '明鑫苑', '福建省', '厦门市', '思明区', null, '118.104955', '24.472117', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5318', '厦门染整厂宿舍楼', '福建省', '厦门市', '思明区', null, '118.120071', '24.510863', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5319', '湖滨南路32-38号', '福建省', '厦门市', '思明区', null, '118.092418', '24.473214', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5320', '龙虎南里', '福建省', '厦门市', '思明区', null, '118.131264', '24.44484', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5321', '石油宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5322', '湖滨东路48-50号院', '福建省', '厦门市', '思明区', null, '118.120868', '24.477118', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5323', '仁安社区居民区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:09', null);
INSERT INTO `t_plot` VALUES ('5324', '电台山化工厂宿舍', '福建省', '厦门市', '思明区', null, '118.104152', '24.466068', '2018-01-03 20:05:10', null, '2018-01-04 19:28:10', null);
INSERT INTO `t_plot` VALUES ('5325', '鹭江新城一期', '福建省', '厦门市', '思明区', null, '118.139068', '24.49505', '2018-01-03 20:05:10', null, '2018-01-04 19:28:10', null);
INSERT INTO `t_plot` VALUES ('5326', '顶澳仔路小区', '福建省', '厦门市', '思明区', null, '118.09963', '24.44575', '2018-01-03 20:05:10', null, '2018-01-04 19:28:10', null);
INSERT INTO `t_plot` VALUES ('5327', '三建宿舍', '福建省', '厦门市', '思明区', null, '118.12969', '24.446021', '2018-01-03 20:05:10', null, '2018-01-04 19:28:10', null);
INSERT INTO `t_plot` VALUES ('5328', '大生里', '福建省', '厦门市', '思明区', null, '118.09145', '24.452148', '2018-01-03 20:05:10', null, '2018-01-04 19:28:10', null);
INSERT INTO `t_plot` VALUES ('5329', '开元路129号小区', '福建省', '厦门市', '思明区', null, '118.0823', '24.463532', '2018-01-03 20:05:10', null, '2018-01-04 19:28:10', null);
INSERT INTO `t_plot` VALUES ('5330', '大同路382号小区', '福建省', '厦门市', '思明区', null, '118.088479', '24.464128', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5331', '洪文六里小区', '福建省', '厦门市', '思明区', null, '118.168392', '24.479923', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5332', '小学苑小区', '福建省', '厦门市', '思明区', null, '118.082853', '24.469175', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5333', '同馨小筑', '福建省', '厦门市', '思明区', null, '118.082842', '24.468395', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5334', '瑞泉山庄', '福建省', '厦门市', '思明区', null, '118.093142', '24.455008', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5335', '太平洋广场', '福建省', '厦门市', '思明区', null, '118.133719', '24.498326', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5336', '民族路小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:11', null);
INSERT INTO `t_plot` VALUES ('5337', '海联中心综合楼', '福建省', '厦门市', '思明区', null, '118.123901', '24.5003', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5338', '假日双星楼台', '福建省', '厦门市', '思明区', null, '118.111979', '24.47145', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5339', '上古街82号小区', '福建省', '厦门市', '思明区', null, '118.094352', '24.458441', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5340', '合展天玺', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5341', '塔厝社', '福建省', '厦门市', '思明区', null, '118.107935', '24.468954', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5342', '中埔头宿舍', '福建省', '厦门市', '思明区', null, '118.094697', '24.445588', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5343', '开平路19号小区', '福建省', '厦门市', '思明区', null, '118.082591', '24.465534', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5344', '新乐大厦', '福建省', '厦门市', '思明区', null, '118.101169', '24.476614', '2018-01-03 20:05:10', null, '2018-01-04 19:28:12', null);
INSERT INTO `t_plot` VALUES ('5345', '建兴阁', '福建省', '厦门市', '思明区', null, '118.148394', '24.496037', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5346', '科普楼院', '福建省', '厦门市', '思明区', null, '118.097618', '24.460734', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5347', 'SOHO大厦', '福建省', '厦门市', '思明区', null, '118.12294', '24.481823', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5348', '金属公司小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5349', '厚德花园', '福建省', '厦门市', '思明区', null, '118.189378', '24.468169', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5350', '宝龙阁', '福建省', '厦门市', '思明区', null, '118.133776', '24.494308', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5351', '东浦路15-19号楼', '福建省', '厦门市', '思明区', null, '118.130006', '24.47736', '2018-01-03 20:05:10', null, '2018-01-04 19:28:13', null);
INSERT INTO `t_plot` VALUES ('5352', '纪委宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5353', '工程厂单身宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5354', '悦都会', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5355', '水仙路23号', '福建省', '厦门市', '思明区', null, '118.082829', '24.458784', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5356', '市直机关宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5357', '卫生局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5358', '仙岳里小区东区', '福建省', '厦门市', '思明区', null, '118.110014', '24.495268', '2018-01-03 20:05:10', null, '2018-01-04 19:28:14', null);
INSERT INTO `t_plot` VALUES ('5359', '海监宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5360', '厦门机电设备总公司宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5361', '宾鸿苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5362', '海洋集团小区', '福建省', '厦门市', '思明区', null, '118.1293', '24.443867', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5363', '恒菁村', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5364', '南轿巷小区', '福建省', '厦门市', '思明区', null, '118.085458', '24.46274', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5365', '湖滨二里60号院', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5366', '东禾园', '福建省', '厦门市', '思明区', null, '118.195241', '24.493114', '2018-01-03 20:05:10', null, '2018-01-04 19:28:15', null);
INSERT INTO `t_plot` VALUES ('5367', '将军祠路132-137号小区', '福建省', '厦门市', '思明区', null, '118.105276', '24.467465', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5368', '华岳山庄三期', '福建省', '厦门市', '思明区', null, '118.127315', '24.502914', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5369', '建南大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5370', '莲前西路192号小区', '福建省', '厦门市', '思明区', null, '118.139925', '24.481666', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5371', '谊爱路20-28号小区', '福建省', '厦门市', '思明区', null, '118.142933', '24.490295', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5372', '故宫商厦', '福建省', '厦门市', '思明区', null, '118.089322', '24.469507', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5373', '曾厝垵东里小区', '福建省', '厦门市', '思明区', null, '118.12629', '24.443533', '2018-01-03 20:05:10', null, '2018-01-04 19:28:16', null);
INSERT INTO `t_plot` VALUES ('5374', '尧洲公司住宅楼', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5375', '特区苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5376', '特运驾校', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5377', '潘宅龙舌溪小区', '福建省', '厦门市', '思明区', null, '118.169165', '24.483206', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5378', '湖滨二里47号院', '福建省', '厦门市', '思明区', null, '118.10983', '24.48098', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5379', '百批公寓', '福建省', '厦门市', '思明区', null, '118.091012', '24.466361', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5380', '湖滨南煤气公司宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:10', null, '2018-01-04 19:28:17', null);
INSERT INTO `t_plot` VALUES ('5381', '蔬菜宿舍', '福建省', '厦门市', '思明区', null, '118.12185', '24.482086', '2018-01-03 20:05:10', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5382', '屿浦路6-10号小区', '福建省', '厦门市', '思明区', null, '118.127647', '24.499732', '2018-01-03 20:05:11', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5383', '湖光路46号院', '福建省', '厦门市', '思明区', null, '118.12077', '24.482824', '2018-01-03 20:05:11', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5384', '房管局宿舍', '福建省', '厦门市', '思明区', null, '118.095405', '24.47566', '2018-01-03 20:05:11', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5385', '虎仔山庄', '福建省', '厦门市', '思明区', null, '118.195825', '24.491908', '2018-01-03 20:05:11', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5386', '湖光路48号小区', '福建省', '厦门市', '思明区', null, '118.113264', '24.482738', '2018-01-03 20:05:11', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5387', '后江埭路59-63号小区', '福建省', '厦门市', '思明区', null, '118.099106', '24.474247', '2018-01-03 20:05:11', null, '2018-01-04 19:28:18', null);
INSERT INTO `t_plot` VALUES ('5388', '湖明路62-68号院', '福建省', '厦门市', '思明区', null, '118.123668', '24.486801', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5389', '新潘宅小区', '福建省', '厦门市', '思明区', null, '118.169264', '24.48501', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5390', '凤屿路30-32号院', '福建省', '厦门市', '思明区', null, '118.122365', '24.478822', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5391', '后江埭路57号', '福建省', '厦门市', '思明区', null, '118.099572', '24.474381', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5392', '南大沟墘27号小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5393', '湖滨东路39号楼', '福建省', '厦门市', '思明区', null, '118.119121', '24.47732', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5394', '湖滨一里82号-83号院', '福建省', '厦门市', '思明区', null, '118.108375', '24.481145', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5395', '后江埭路71号之1-2号', '福建省', '厦门市', '思明区', null, '118.099369', '24.474421', '2018-01-03 20:05:11', null, '2018-01-04 19:28:19', null);
INSERT INTO `t_plot` VALUES ('5396', '湖滨三里66-67号院', '福建省', '厦门市', '思明区', null, '118.111888', '24.481633', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5397', '厦纸宿舍二期', '福建省', '厦门市', '思明区', null, '118.12601', '24.480169', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5398', '东方铭建山庄', '福建省', '厦门市', '思明区', null, '118.153916', '24.483307', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5399', '菜妈街50号', '福建省', '厦门市', '思明区', null, '118.092167', '24.466181', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5400', '尚峰国际', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5401', '丽晶花园一期', '福建省', '厦门市', '思明区', null, '118.187674', '24.481436', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5402', '帝景苑', '福建省', '厦门市', '思明区', null, '118.087527', '24.473411', '2018-01-03 20:05:11', null, '2018-01-04 19:28:20', null);
INSERT INTO `t_plot` VALUES ('5404', '翠碧花苑', '福建省', '厦门市', '思明区', null, '118.107762', '24.475147', '2018-01-03 20:05:11', null, '2018-01-04 19:28:21', null);
INSERT INTO `t_plot` VALUES ('5405', '担水巷19号', '福建省', '厦门市', '思明区', null, '118.080203', '24.466072', '2018-01-03 20:05:11', null, '2018-01-04 19:28:21', null);
INSERT INTO `t_plot` VALUES ('5406', '利华苑(别墅)', '福建省', '厦门市', '思明区', null, '118.088842', '24.48138', '2018-01-03 20:05:11', null, '2018-01-04 19:28:21', null);
INSERT INTO `t_plot` VALUES ('5407', '观音山小区', '福建省', '厦门市', '思明区', null, '118.099203', '24.488719', '2018-01-03 20:05:11', null, '2018-01-04 19:28:21', null);
INSERT INTO `t_plot` VALUES ('5408', '赤岭小区', '福建省', '厦门市', '思明区', null, '118.099728', '24.448116', '2018-01-03 20:05:11', null, '2018-01-04 19:28:21', null);
INSERT INTO `t_plot` VALUES ('5409', '湖滨东路29-33号小区', '福建省', '厦门市', '思明区', null, '118.119422', '24.477114', '2018-01-03 20:05:11', null, '2018-01-04 19:28:21', null);
INSERT INTO `t_plot` VALUES ('5410', '金色梦想小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5411', '厦门粮食集团油厂公寓', '福建省', '厦门市', '思明区', null, '118.11262', '24.47465', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5412', '虎园路财政宿舍', '福建省', '厦门市', '思明区', null, '118.099243', '24.459343', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5413', '海悦豪园(别墅)', '福建省', '厦门市', '思明区', null, '118.054861', '24.409742', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5414', '侨龙小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5415', '中航当代半岛', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5416', '屿后里小区北1', '福建省', '厦门市', '思明区', null, '118.130915', '24.500402', '2018-01-03 20:05:11', null, '2018-01-04 19:28:22', null);
INSERT INTO `t_plot` VALUES ('5417', '厦门市水产发展公司宿舍大楼', '福建省', '厦门市', '思明区', null, '118.0959', '24.450347', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5418', '安宝大厦', '福建省', '厦门市', '思明区', null, '118.137116', '24.496081', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5419', '飞龙公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5420', '长龙领海', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5421', '东浦路170-200号', '福建省', '厦门市', '思明区', null, '118.13769', '24.480262', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5422', '海洋集团宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5423', '兴华路别墅', '福建省', '厦门市', '思明区', null, '118.095676', '24.459429', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5424', '怡祥大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:23', null);
INSERT INTO `t_plot` VALUES ('5425', '永同昌花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5426', '吉大吉小区', '福建省', '厦门市', '思明区', null, '118.129281', '24.441468', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5427', '湖滨一里74号-77号院', '福建省', '厦门市', '思明区', null, '118.108425', '24.480894', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5428', '东浦路房产宿舍', '福建省', '厦门市', '思明区', null, '118.13066', '24.47656', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5429', '市委宣传部宿舍', '福建省', '厦门市', '思明区', null, '118.109829', '24.480795', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5430', '前埔轮训队古楼公寓', '福建省', '厦门市', '思明区', null, '118.175837', '24.468825', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5431', '湖明路103-108号', '福建省', '厦门市', '思明区', null, '118.123065', '24.486341', '2018-01-03 20:05:11', null, '2018-01-04 19:28:24', null);
INSERT INTO `t_plot` VALUES ('5432', '交通银行宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5433', '莲坂造船厂宿舍', '福建省', '厦门市', '思明区', null, '118.119392', '24.478045', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5434', '文明公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5435', '湖滨南路86号-88号', '福建省', '厦门市', '思明区', null, '118.098559', '24.473943', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5436', '天湖路电子城宿舍', '福建省', '厦门市', '思明区', null, '118.096506', '24.476043', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5437', '厦门嘉华进出口有限公司职工宿舍...', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5438', '湖滨二里58-59号院', '福建省', '厦门市', '思明区', null, '118.110593', '24.481526', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5439', '欣景佳苑', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:25', null);
INSERT INTO `t_plot` VALUES ('5440', '屿后里', '福建省', '厦门市', '思明区', null, '118.127178', '24.501225', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5441', '机电厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5442', '公园北路小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5443', '湖边水库后坑小区', '福建省', '厦门市', '思明区', null, '118.170458', '24.502842', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5444', '必利达大厦', '福建省', '厦门市', '思明区', null, '118.136794', '24.495654', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5445', '汇成商业中心', '福建省', '厦门市', '思明区', null, '118.111147', '24.472675', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5446', '文安社区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:11', null, '2018-01-04 19:28:26', null);
INSERT INTO `t_plot` VALUES ('5448', '新华路小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:27', null);
INSERT INTO `t_plot` VALUES ('5449', '源泉山庄A区', '福建省', '厦门市', '思明区', null, '118.172751', '24.474399', '2018-01-03 20:05:12', null, '2018-01-04 19:28:27', null);
INSERT INTO `t_plot` VALUES ('5450', '苏厝街', '福建省', '厦门市', '思明区', null, '118.092641', '24.459231', '2018-01-03 20:05:12', null, '2018-01-04 19:28:27', null);
INSERT INTO `t_plot` VALUES ('5451', '角尾路小区', '福建省', '厦门市', '思明区', null, '118.089191', '24.467687', '2018-01-03 20:05:12', null, '2018-01-04 19:28:27', null);
INSERT INTO `t_plot` VALUES ('5452', '莲花大厦', '福建省', '厦门市', '思明区', null, '118.135762', '24.494383', '2018-01-03 20:05:12', null, '2018-01-04 19:28:27', null);
INSERT INTO `t_plot` VALUES ('5453', '湖滨北路法院宿舍', '福建省', '厦门市', '思明区', null, '118.10672', '24.489819', '2018-01-03 20:05:12', null, '2018-01-04 19:28:27', null);
INSERT INTO `t_plot` VALUES ('5454', '粮油宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5455', '文滨花园', '福建省', '厦门市', '思明区', null, '118.108186', '24.477194', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5456', '石泉路19号', '福建省', '厦门市', '思明区', null, '118.093572', '24.456259', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5457', '海悦豪园', '福建省', '厦门市', '思明区', null, '118.054861', '24.409742', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5458', '凯旋广场', '福建省', '厦门市', '思明区', null, '118.11675', '24.475003', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5459', '污水宿舍', '福建省', '厦门市', '思明区', null, '118.124854', '24.493366', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5460', '富山女人街', '福建省', '厦门市', '思明区', null, '118.125373', '24.48304', '2018-01-03 20:05:12', null, '2018-01-04 19:28:28', null);
INSERT INTO `t_plot` VALUES ('5461', '龙泽花园', '福建省', '厦门市', '思明区', null, '118.076822', '24.453332', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5462', '源泉山庄B区', '福建省', '厦门市', '思明区', null, '118.172751', '24.474399', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5463', '明发新天地', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5464', '莲湖花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5465', '新景花园', '福建省', '厦门市', '思明区', null, '118.189883', '24.486299', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5466', '厦门航空宿舍', '福建省', '厦门市', '思明区', null, '118.110093', '24.472633', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5467', '龙山仓储区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:29', null);
INSERT INTO `t_plot` VALUES ('5468', '怡湖楼', '福建省', '厦门市', '思明区', null, '118.096578', '24.476148', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5469', '第一七四医院将军祠小区', '福建省', '厦门市', '思明区', null, '118.101544', '24.465095', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5470', '公安局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5471', '园林宿舍', '福建省', '厦门市', '思明区', null, '118.115089', '24.479076', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5472', '厦门市市直机关宿舍区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5473', '冷冻宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5474', '洪莲里77-82号', '福建省', '厦门市', '思明区', null, '118.168703', '24.48223', '2018-01-03 20:05:12', null, '2018-01-04 19:28:30', null);
INSERT INTO `t_plot` VALUES ('5475', '善见豪山', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5476', '侨岳厦工花园', '福建省', '厦门市', '思明区', null, '118.117338', '24.502178', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5477', '中航紫金广场', '福建省', '厦门市', '思明区', null, '118.199877', '24.482543', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5478', '莲坂外图', '福建省', '厦门市', '思明区', null, '118.119392', '24.478045', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5479', '厦门市中心血站宿舍楼', '福建省', '厦门市', '思明区', null, '118.101806', '24.476551', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5480', '金湖花园', '福建省', '厦门市', '思明区', null, '118.093601', '24.475547', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5481', '先锋营街', '福建省', '厦门市', '思明区', null, '118.092603', '24.465852', '2018-01-03 20:05:12', null, '2018-01-04 19:28:31', null);
INSERT INTO `t_plot` VALUES ('5482', '美湖路1-999小区', '福建省', '厦门市', '思明区', null, '118.095651', '24.47305', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5483', '海关莲花宿舍(香秀里63-74号)', '福建省', '厦门市', '思明区', null, '118.1402', '24.496788', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5484', '南湖公寓', '福建省', '厦门市', '思明区', null, '118.112938', '24.482271', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5485', '双涵路税务宿舍', '福建省', '厦门市', '思明区', null, '118.122606', '24.479178', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5486', '梧村街道办宿舍', '福建省', '厦门市', '思明区', null, '118.125281', '24.47772', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5487', '环保宿舍', '福建省', '厦门市', '思明区', null, '118.094914', '24.473865', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5488', '莲前造船厂宿舍', '福建省', '厦门市', '思明区', null, '118.132522', '24.482537', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5489', '大幸阁', '福建省', '厦门市', '思明区', null, '118.117422', '24.475011', '2018-01-03 20:05:12', null, '2018-01-04 19:28:32', null);
INSERT INTO `t_plot` VALUES ('5490', '金榜大厦', '福建省', '厦门市', '思明区', null, '118.11452', '24.472764', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5491', '屿后南里110-113号小区', '福建省', '厦门市', '思明区', null, '118.12995', '24.498524', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5492', '洋坂保险宿舍楼', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5493', '筑馨公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5494', '墙顶巷', '福建省', '厦门市', '思明区', null, '118.090416', '24.463175', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5495', '麦仔埕巷', '福建省', '厦门市', '思明区', null, '118.085132', '24.463816', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5496', '嘉丽广场', '福建省', '厦门市', '思明区', null, '118.081023', '24.467187', '2018-01-03 20:05:12', null, '2018-01-04 19:28:33', null);
INSERT INTO `t_plot` VALUES ('5497', '金海公寓', '福建省', '厦门市', '思明区', null, '118.120199', '24.46465', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5498', '中山路名汇广场', '福建省', '厦门市', '思明区', null, '118.09062', '24.46119', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5499', '红星安华小区', '福建省', '厦门市', '思明区', null, '118.139934', '24.480366', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5500', '纸厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5501', '锦龙阁', '福建省', '厦门市', '思明区', null, '118.109455', '24.470458', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5502', '电池厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5503', '双十教师宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:34', null);
INSERT INTO `t_plot` VALUES ('5504', '厦门市消防宿舍', '福建省', '厦门市', '思明区', null, '118.108775', '24.490459', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5505', '思明区消防宿舍', '福建省', '厦门市', '思明区', null, '118.108775', '24.490459', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5506', '罗宾森广场二期', '福建省', '厦门市', '思明区', null, '118.118163', '24.475597', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5507', '莲岳路水务宿舍', '福建省', '厦门市', '思明区', null, '118.124146', '24.493745', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5508', '蓝湾美墅', '福建省', '厦门市', '思明区', null, '118.168166', '24.46638', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5509', '玛瑙阁', '福建省', '厦门市', '思明区', null, '118.118073', '24.474741', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5510', '银河新村', '福建省', '厦门市', '思明区', null, '118.125665', '24.476615', '2018-01-03 20:05:12', null, '2018-01-04 19:28:35', null);
INSERT INTO `t_plot` VALUES ('5511', '鑫龙大厦', '福建省', '厦门市', '思明区', null, '118.104465', '24.494645', '2018-01-03 20:05:12', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5512', '夏纸宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:12', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5513', '钢窗厂宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5514', '福建省发改委宿舍', '福建省', '厦门市', '思明区', null, '118.125674', '24.488012', '2018-01-03 20:05:13', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5515', '莲前西路131号小区', '福建省', '厦门市', '思明区', null, '118.13393', '24.483168', '2018-01-03 20:05:13', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5516', '文屏巷小区', '福建省', '厦门市', '思明区', null, '118.111798', '24.469762', '2018-01-03 20:05:13', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5517', '幸福里', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:36', null);
INSERT INTO `t_plot` VALUES ('5518', '莲花外口公寓', '福建省', '厦门市', '思明区', null, '118.138789', '24.481518', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5519', '罗马假日', '福建省', '厦门市', '思明区', null, '118.124066', '24.482796', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5520', '弘龙时代花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5521', '龙山中路2号小区', '福建省', '厦门市', '思明区', null, '118.146044', '24.486006', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5522', '七星路公安局宿舍', '福建省', '厦门市', '思明区', null, '118.105826', '24.491378', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5523', '莲花新村蓉芳里1号小区', '福建省', '厦门市', '思明区', null, '118.137846', '24.494456', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5524', '达意商业城公寓', '福建省', '厦门市', '思明区', null, '118.089759', '24.461155', '2018-01-03 20:05:13', null, '2018-01-04 19:28:37', null);
INSERT INTO `t_plot` VALUES ('5525', '毅宏大厦', '福建省', '厦门市', '思明区', null, '118.131693', '24.482736', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5526', '志盛花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5527', '景观公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5528', '嘉都综合楼', '福建省', '厦门市', '思明区', null, '118.130169', '24.444719', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5529', '民航宿舍综合楼', '福建省', '厦门市', '思明区', null, '118.140714', '24.479681', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5530', '中山路步行街', '福建省', '厦门市', '思明区', null, '118.083209', '24.459807', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5532', '后埭溪路90号', '福建省', '厦门市', '思明区', null, '118.11491', '24.470196', '2018-01-03 20:05:13', null, '2018-01-04 19:28:38', null);
INSERT INTO `t_plot` VALUES ('5533', '源成花园', '福建省', '厦门市', '思明区', null, '118.125651', '24.505138', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5534', '文园新村', '福建省', '厦门市', '思明区', null, '118.107341', '24.468809', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5535', '太子公寓', '福建省', '厦门市', '思明区', null, '118.110093', '24.466231', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5536', '骏宇广场', '福建省', '厦门市', '思明区', null, '118.121417', '24.48352', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5537', '龙山中路85号小区', '福建省', '厦门市', '思明区', null, '118.146202', '24.490221', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5538', '半屏山路1256号', '福建省', '厦门市', '思明区', null, '118.199526', '24.502205', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5539', '清河花园', '福建省', '厦门市', '思明区', null, '118.136201', '24.480516', '2018-01-03 20:05:13', null, '2018-01-04 19:28:39', null);
INSERT INTO `t_plot` VALUES ('5540', '富贵楼', '福建省', '厦门市', '思明区', null, '118.127613', '24.479791', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5541', '民族路蜂巢山社区', '福建省', '厦门市', '思明区', null, '118.091531', '24.448668', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5542', '金榜山庄', '福建省', '厦门市', '思明区', null, '118.121595', '24.470114', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5543', '紫微花园', '福建省', '厦门市', '思明区', null, '118.172678', '24.479288', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5544', '仙岳宿舍', '福建省', '厦门市', '思明区', null, '118.113168', '24.498627', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5545', '容谷别墅', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5546', '金地翠园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:40', null);
INSERT INTO `t_plot` VALUES ('5547', '闽南大厦', '福建省', '厦门市', '思明区', null, '118.109406', '24.478855', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5548', '汇盛花园', '福建省', '厦门市', '思明区', null, '118.143535', '24.487691', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5549', '湖明路120-126号院', '福建省', '厦门市', '思明区', null, '118.123513', '24.489599', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5550', '联丰心家园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5551', '莲花四村', '福建省', '厦门市', '思明区', null, '118.139387', '24.494913', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5552', '民盛花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5553', '龙山居', '福建省', '厦门市', '思明区', null, '118.142089', '24.483685', '2018-01-03 20:05:13', null, '2018-01-04 19:28:41', null);
INSERT INTO `t_plot` VALUES ('5554', '坑内路', '福建省', '厦门市', '思明区', null, '118.104299', '24.465506', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5555', '嘉莲里', '福建省', '厦门市', '思明区', null, '118.133402', '24.490858', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5556', '湖滨中路海关宿舍', '福建省', '厦门市', '思明区', null, '118.109261', '24.4814', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5557', '天泉山庄', '福建省', '厦门市', '思明区', null, '118.137759', '24.43038', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5558', '制革宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5559', '盈翠里51-53号小区', '福建省', '厦门市', '思明区', null, '118.136507', '24.489474', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5560', '古楼公寓', '福建省', '厦门市', '思明区', null, '118.175837', '24.468825', '2018-01-03 20:05:13', null, '2018-01-04 19:28:42', null);
INSERT INTO `t_plot` VALUES ('5561', '邮电局宿舍', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5562', '中康阁', '福建省', '厦门市', '思明区', null, '118.086071', '24.468321', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5563', '硅谷精英公寓', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5564', '湖畔尊爵', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5565', '湖滨东路8号院', '福建省', '厦门市', '思明区', null, '118.121239', '24.476341', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5566', '万寿外口公寓', '福建省', '厦门市', '思明区', null, '118.116543', '24.466015', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5567', '小金星大厦', '福建省', '厦门市', '思明区', null, '118.171101', '24.484187', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5568', '函菁大厦', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:43', null);
INSERT INTO `t_plot` VALUES ('5569', '福缘公寓', '福建省', '厦门市', '思明区', null, '118.188488', '24.484996', '2018-01-03 20:05:13', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5570', '中科院小区', '福建省', '厦门市', '思明区', null, '118.102753', '24.494273', '2018-01-03 20:05:13', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5571', '玉亭里', '福建省', '厦门市', '思明区', null, '118.139301', '24.491925', '2018-01-03 20:05:13', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5572', '莲前西路227号小区', '福建省', '厦门市', '思明区', null, '118.141489', '24.483608', '2018-01-03 20:05:13', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5573', '莲花西路285号小区', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:13', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5574', '华林前景城', '福建省', '厦门市', '思明区', null, '118.184917', '24.474143', '2018-01-03 20:05:14', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5575', '古龙小区', '福建省', '厦门市', '思明区', null, '118.108943', '24.474338', '2018-01-03 20:05:14', null, '2018-01-04 19:28:44', null);
INSERT INTO `t_plot` VALUES ('5576', '鼓浪天籁', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5577', '厦门造船厂宿舍', '福建省', '厦门市', '思明区', null, '118.134825', '24.482779', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5578', '鹭悦豪庭', '福建省', '厦门市', '思明区', null, '118.129262', '24.440678', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5579', '联丰西塔楼', '福建省', '厦门市', '思明区', null, '118.147209', '24.497751', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5580', '新莲花新龙山花园', '福建省', '厦门市', '思明区', null, '118.143288', '24.491004', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5581', '一建公司家属花园', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5582', '莲前西路285号小区', '福建省', '厦门市', '思明区', null, '118.14353', '24.48322', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5583', '联丰大厦', '福建省', '厦门市', '思明区', null, '118.166832', '24.482017', '2018-01-03 20:05:14', null, '2018-01-04 19:28:45', null);
INSERT INTO `t_plot` VALUES ('5584', '镇海花园', '福建省', '厦门市', '思明区', null, '118.094374', '24.45892', '2018-01-03 20:05:14', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5585', '建宝花园', '福建省', '厦门市', '思明区', null, '118.081743', '24.46885', '2018-01-03 20:05:14', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5586', '民盛商厦安置房', '福建省', '厦门市', '思明区', null, '118.134535', '24.468728', '2018-01-03 20:05:14', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5587', '大洋公寓', '福建省', '厦门市', '思明区', null, '118.135242', '24.487549', '2018-01-03 20:05:14', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5588', '四川大厦', '福建省', '厦门市', '思明区', null, '118.132569', '24.501821', '2018-01-03 20:05:14', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5589', '住宅水晶国际', '福建省', '厦门市', '思明区', null, '118.195244', '24.502868', '2018-01-03 20:05:14', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5590', '恒禾七尚', '福建省', '厦门市', '湖里区', null, '118.189425', '24.531134', '2018-01-03 20:19:00', null, '2018-01-04 19:28:46', null);
INSERT INTO `t_plot` VALUES ('5591', '万科湖心岛', '福建省', '厦门市', '湖里区', null, '118.170133', '24.494305', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5592', '源昌鑫海湾', '福建省', '厦门市', '湖里区', null, '118.167861', '24.547484', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5593', '裕兴大厦', '福建省', '厦门市', '湖里区', null, '118.134594', '24.503022', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5594', '特房山水杰座', '福建省', '厦门市', '湖里区', null, '118.180856', '24.50851', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5595', '特房五缘尚座', '福建省', '厦门市', '湖里区', null, '118.168415', '24.518581', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5596', '禹洲新村', '福建省', '厦门市', '湖里区', null, '118.123846', '24.508872', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5597', '东晖广场', '福建省', '厦门市', '湖里区', null, '118.149516', '24.513256', '2018-01-03 20:19:00', null, '2018-01-04 19:28:47', null);
INSERT INTO `t_plot` VALUES ('5598', '唐庄别墅', '福建省', '厦门市', '湖里区', null, '118.152644', '24.501516', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5599', '新景天湖广场', '福建省', '厦门市', '湖里区', null, '118.138492', '24.501181', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5600', '万景花园', '福建省', '厦门市', '湖里区', null, '118.116307', '24.51231', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5601', '国贸润园', '福建省', '厦门市', '湖里区', null, '118.172862', '24.540915', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5602', '书香佳缘', '福建省', '厦门市', '湖里区', null, '118.161415', '24.517987', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5603', '联发新天地', '福建省', '厦门市', '湖里区', null, '118.152858', '24.508238', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5604', '天成花园', '福建省', '厦门市', '湖里区', null, '118.137313', '24.508081', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5605', '特房山水尚座', '福建省', '厦门市', '湖里区', null, '118.179988', '24.510238', '2018-01-03 20:19:00', null, '2018-01-04 19:28:48', null);
INSERT INTO `t_plot` VALUES ('5606', '特房五缘新座', '福建省', '厦门市', '湖里区', null, '118.170951', '24.520031', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5607', '江宁里', '福建省', '厦门市', '湖里区', null, '118.137594', '24.499019', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5608', '长乐邨三期', '福建省', '厦门市', '湖里区', null, '118.110694', '24.527459', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5609', '国贸阳光', '福建省', '厦门市', '湖里区', null, '118.15039', '24.508306', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5610', '芙蓉苑', '福建省', '厦门市', '湖里区', null, '118.116766', '24.522704', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5611', '裕兴花园', '福建省', '厦门市', '湖里区', null, '118.137454', '24.509909', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5612', '筼筜温莎公馆', '福建省', '厦门市', '湖里区', null, '118.147348', '24.503113', '2018-01-03 20:19:00', null, '2018-01-04 19:28:49', null);
INSERT INTO `t_plot` VALUES ('5613', '中铁财富港湾', '福建省', '厦门市', '湖里区', null, '118.133832', '24.501315', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5614', '永升新城', '福建省', '厦门市', '湖里区', null, '118.12369', '24.523854', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5615', '裕发花园', '福建省', '厦门市', '湖里区', null, '118.135168', '24.506502', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5616', '建发央座', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5617', '东方巴黎广场', '福建省', '厦门市', '湖里区', null, '118.135564', '24.499612', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5618', '金岭花园', '福建省', '厦门市', '湖里区', null, '118.147215', '24.497798', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5619', '中铁元湾', '福建省', '厦门市', '湖里区', null, '118.17608', '24.545066', '2018-01-03 20:19:00', null, '2018-01-04 19:28:50', null);
INSERT INTO `t_plot` VALUES ('5620', '新景缘', '福建省', '厦门市', '湖里区', null, '118.190141', '24.537245', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5621', '信达大厦', '福建省', '厦门市', '湖里区', null, '118.101977', '24.515392', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5622', '第二城幸福生活', '福建省', '厦门市', '湖里区', null, '118.148501', '24.508821', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5623', '佳馨美墅', '福建省', '厦门市', '湖里区', null, '118.129836', '24.513297', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5624', '博士花园', '福建省', '厦门市', '湖里区', null, '118.145871', '24.506819', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5625', '特房山水新座', '福建省', '厦门市', '湖里区', null, '118.181365', '24.510833', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5626', '翠湖庄园', '福建省', '厦门市', '湖里区', null, '118.144553', '24.53557', '2018-01-03 20:19:00', null, '2018-01-04 19:28:51', null);
INSERT INTO `t_plot` VALUES ('5627', '幸福密码', '福建省', '厦门市', '湖里区', null, '118.10243', '24.513132', '2018-01-03 20:19:00', null, '2018-01-04 19:28:52', null);
INSERT INTO `t_plot` VALUES ('5628', '金秋花园', '福建省', '厦门市', '湖里区', null, '118.156858', '24.502559', '2018-01-03 20:19:00', null, '2018-01-04 19:28:52', null);
INSERT INTO `t_plot` VALUES ('5629', '国际邮轮城二期', '福建省', '厦门市', '湖里区', null, '118.0838', '24.489402', '2018-01-03 20:19:00', null, '2018-01-04 19:28:52', null);
INSERT INTO `t_plot` VALUES ('5630', '建发中央天成', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:00', null, '2018-01-04 19:28:52', null);
INSERT INTO `t_plot` VALUES ('5631', '永建顶尚', '福建省', '厦门市', '湖里区', null, '118.156204', '24.518742', '2018-01-03 20:19:00', null, '2018-01-04 19:28:52', null);
INSERT INTO `t_plot` VALUES ('5632', '翔鹭花城一期', '福建省', '厦门市', '湖里区', null, '118.114034', '24.529764', '2018-01-03 20:19:00', null, '2018-01-04 19:28:52', null);
INSERT INTO `t_plot` VALUES ('5633', '国际山庄', '福建省', '厦门市', '湖里区', null, '118.159895', '24.510312', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5634', '碧湖嘉园', '福建省', '厦门市', '湖里区', null, '118.15794', '24.51804', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5635', '康乐新村一期', '福建省', '厦门市', '湖里区', null, '118.125845', '24.511778', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5636', '湖边花园', '福建省', '厦门市', '湖里区', null, '118.177379', '24.499269', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5637', '大洋雅苑', '福建省', '厦门市', '湖里区', null, '118.158739', '24.516519', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5638', '第二城快乐天地', '福建省', '厦门市', '湖里区', null, '118.149504', '24.50653', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5639', '福隆国际', '福建省', '厦门市', '湖里区', null, '118.135322', '24.496639', '2018-01-03 20:19:00', null, '2018-01-04 19:28:53', null);
INSERT INTO `t_plot` VALUES ('5640', '凯悦新城', '福建省', '厦门市', '湖里区', null, '118.151463', '24.513452', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5641', '中港花园二期', '福建省', '厦门市', '湖里区', null, '118.13554', '24.509097', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5642', '源昌豪庭一期', '福建省', '厦门市', '湖里区', null, '118.120589', '24.511624', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5643', '翔鹭花城二期', '福建省', '厦门市', '湖里区', null, '118.110205', '24.532721', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5644', '晶尚名苑', '福建省', '厦门市', '湖里区', null, '118.162598', '24.520607', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5645', '新景国际外滩', '福建省', '厦门市', '湖里区', null, '118.173203', '24.545529', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5646', '新景龙郡', '福建省', '厦门市', '湖里区', null, '118.148753', '24.517412', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5647', '新景翡翠苑', '福建省', '厦门市', '湖里区', null, '118.171163', '24.527855', '2018-01-03 20:19:00', null, '2018-01-04 19:28:54', null);
INSERT INTO `t_plot` VALUES ('5648', '源昌宝墅湾', '福建省', '厦门市', '湖里区', null, '118.164536', '24.54665', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5649', '中孚花园', '福建省', '厦门市', '湖里区', null, '118.15374', '24.498053', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5650', '世茂湖滨首府D区', '福建省', '厦门市', '湖里区', null, '118.175715', '24.498418', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5651', '新景华府国际', '福建省', '厦门市', '湖里区', null, '118.148162', '24.501288', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5652', '禹洲高HOUSE尧洲新城', '福建省', '厦门市', '湖里区', null, '118.148148', '24.528004', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5653', '华林金盛花园', '福建省', '厦门市', '湖里区', null, '118.154043', '24.502915', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5654', '鹭槟大厦', '福建省', '厦门市', '湖里区', null, '118.08546', '24.489695', '2018-01-03 20:19:00', null, '2018-01-04 19:28:55', null);
INSERT INTO `t_plot` VALUES ('5655', '双鲤新城', '福建省', '厦门市', '湖里区', null, '118.13792', '24.530944', '2018-01-03 20:19:00', null, '2018-01-04 19:28:56', null);
INSERT INTO `t_plot` VALUES ('5656', '联发欣悦园', '福建省', '厦门市', '湖里区', null, '118.146173', '24.5133', '2018-01-03 20:19:00', null, '2018-01-04 19:28:56', null);
INSERT INTO `t_plot` VALUES ('5657', '康乐新村二期', '福建省', '厦门市', '湖里区', null, '118.125456', '24.511328', '2018-01-03 20:19:00', null, '2018-01-04 19:28:56', null);
INSERT INTO `t_plot` VALUES ('5658', '源昌豪庭2期', '福建省', '厦门市', '湖里区', null, '118.120896', '24.511404', '2018-01-03 20:19:00', null, '2018-01-04 19:28:56', null);
INSERT INTO `t_plot` VALUES ('5659', '古龙公寓', '福建省', '厦门市', '湖里区', null, '118.104636', '24.521555', '2018-01-03 20:19:00', null, '2018-01-04 19:28:56', null);
INSERT INTO `t_plot` VALUES ('5660', '大唐五缘yoho公寓', '福建省', '厦门市', '湖里区', null, '118.166335', '24.519149', '2018-01-03 20:19:00', null, '2018-01-04 19:28:56', null);
INSERT INTO `t_plot` VALUES ('5661', '雪梨星光一期', '福建省', '厦门市', '湖里区', null, '118.135461', '24.517018', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5662', '太微花园', '福建省', '厦门市', '湖里区', null, '118.152735', '24.503107', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5663', '新景雅苑', '福建省', '厦门市', '湖里区', null, '118.137782', '24.504887', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5664', '金尚小区', '福建省', '厦门市', '湖里区', null, '118.156556', '24.498855', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5665', '中骏天誉', '福建省', '厦门市', '湖里区', null, '118.169579', '24.51602', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5666', '世茂湖滨首府C区', '福建省', '厦门市', '湖里区', null, '118.175715', '24.498418', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5667', '华泰苑', '福建省', '厦门市', '湖里区', null, '118.111078', '24.511093', '2018-01-03 20:19:00', null, '2018-01-04 19:28:57', null);
INSERT INTO `t_plot` VALUES ('5669', '三航六宿舍', '福建省', '厦门市', '湖里区', null, '118.117158', '24.530972', '2018-01-03 20:19:00', null, '2018-01-04 19:28:58', null);
INSERT INTO `t_plot` VALUES ('5670', '吕岭花园', '福建省', '厦门市', '湖里区', null, '118.145808', '24.501539', '2018-01-03 20:19:00', null, '2018-01-04 19:28:58', null);
INSERT INTO `t_plot` VALUES ('5671', '联发五缘湾1号', '福建省', '厦门市', '湖里区', null, '118.182754', '24.538406', '2018-01-03 20:19:00', null, '2018-01-04 19:28:58', null);
INSERT INTO `t_plot` VALUES ('5672', '国贸新天地', '福建省', '厦门市', '湖里区', null, '118.173158', '24.526041', '2018-01-03 20:19:00', null, '2018-01-04 19:28:58', null);
INSERT INTO `t_plot` VALUES ('5673', '冠宏花园', '福建省', '厦门市', '湖里区', null, '118.134802', '24.497758', '2018-01-03 20:19:00', null, '2018-01-04 19:28:58', null);
INSERT INTO `t_plot` VALUES ('5674', '国贸天琴湾1期', '福建省', '厦门市', '湖里区', null, '118.175845', '24.524214', '2018-01-03 20:19:00', null, '2018-01-04 19:28:58', null);
INSERT INTO `t_plot` VALUES ('5675', '山谷城市', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5676', '铭爵山庄', '福建省', '厦门市', '湖里区', null, '118.114893', '24.523813', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5677', '欣家园', '福建省', '厦门市', '湖里区', null, '118.108809', '24.524002', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5678', '欣华花园', '福建省', '厦门市', '湖里区', null, '118.115455', '24.516785', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5679', '中央美地', '福建省', '厦门市', '湖里区', null, '118.151154', '24.515847', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5680', '禹洲棕榈城', '福建省', '厦门市', '湖里区', null, '118.093462', '24.504846', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5681', '怡景花园', '福建省', '厦门市', '湖里区', null, '118.118165', '24.521665', '2018-01-03 20:19:00', null, '2018-01-04 19:28:59', null);
INSERT INTO `t_plot` VALUES ('5682', '海发大厦', '福建省', '厦门市', '湖里区', null, '118.10333', '24.526758', '2018-01-03 20:19:00', null, '2018-01-04 19:29:00', null);
INSERT INTO `t_plot` VALUES ('5683', '长乐村B组', '福建省', '厦门市', '湖里区', null, '118.112792', '24.527632', '2018-01-03 20:19:00', null, '2018-01-04 19:29:00', null);
INSERT INTO `t_plot` VALUES ('5684', '优山美地', '福建省', '厦门市', '湖里区', null, '118.097695', '24.505198', '2018-01-03 20:19:00', null, '2018-01-04 19:29:00', null);
INSERT INTO `t_plot` VALUES ('5685', '佳丽花园', '福建省', '厦门市', '湖里区', null, '118.10803', '24.510461', '2018-01-03 20:19:00', null, '2018-01-04 19:29:00', null);
INSERT INTO `t_plot` VALUES ('5686', '金色阳光', '福建省', '厦门市', '湖里区', null, '118.152326', '24.52266', '2018-01-03 20:19:00', null, '2018-01-04 19:29:00', null);
INSERT INTO `t_plot` VALUES ('5688', '禹洲城上城', '福建省', '厦门市', '湖里区', null, '118.153805', '24.513665', '2018-01-03 20:19:00', null, '2018-01-04 19:29:00', null);
INSERT INTO `t_plot` VALUES ('5689', '国贸蓝海', '福建省', '厦门市', '湖里区', null, '118.191281', '24.533091', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5690', '中福城', '福建省', '厦门市', '湖里区', null, '118.153569', '24.496141', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5691', '中港花园一期', '福建省', '厦门市', '湖里区', null, '118.136804', '24.50959', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5692', '紫金家园北区', '福建省', '厦门市', '湖里区', null, '118.169512', '24.527846', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5693', '枋湖新村', '福建省', '厦门市', '湖里区', null, '118.147885', '24.521339', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5694', '水岸豪景', '福建省', '厦门市', '湖里区', null, '118.13697', '24.503093', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5695', '建德公寓', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5696', '樱花山庄', '福建省', '厦门市', '湖里区', null, '118.131551', '24.514094', '2018-01-03 20:19:00', null, '2018-01-04 19:29:01', null);
INSERT INTO `t_plot` VALUES ('5697', '泰裕新城', '福建省', '厦门市', '湖里区', null, '118.123906', '24.509631', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5698', '中环花园', '福建省', '厦门市', '湖里区', null, '118.141114', '24.499844', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5699', '湖里外口生活小区', '福建省', '厦门市', '湖里区', null, '118.101395', '24.509997', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5700', '宝龙御湖官邸', '福建省', '厦门市', '湖里区', null, '118.175403', '24.500325', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5701', '神山公寓', '福建省', '厦门市', '湖里区', null, '118.111672', '24.535681', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5702', '振华大厦', '福建省', '厦门市', '湖里区', null, '118.08493', '24.489144', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5703', '玉山花园', '福建省', '厦门市', '湖里区', null, '118.155331', '24.502425', '2018-01-03 20:19:00', null, '2018-01-04 19:29:02', null);
INSERT INTO `t_plot` VALUES ('5704', '富贵门花园(别墅)', '福建省', '厦门市', '湖里区', null, '118.153355', '24.527651', '2018-01-03 20:19:00', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5705', '永同昌莱茵花苑', '福建省', '厦门市', '湖里区', null, '118.15493', '24.499682', '2018-01-03 20:19:00', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5706', '万山小区', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:00', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5707', '南山小区', '福建省', '厦门市', '湖里区', null, '118.106306', '24.50957', '2018-01-03 20:19:00', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5708', '金永昌家园', '福建省', '厦门市', '湖里区', null, '118.156342', '24.508315', '2018-01-03 20:19:01', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5710', '榕福苑', '福建省', '厦门市', '湖里区', null, '118.107224', '24.522829', '2018-01-03 20:19:01', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5711', '高林居住区北区', '福建省', '厦门市', '湖里区', null, '118.195251', '24.509136', '2018-01-03 20:19:01', null, '2018-01-04 19:29:03', null);
INSERT INTO `t_plot` VALUES ('5712', '银龙公寓', '福建省', '厦门市', '湖里区', null, '118.153895', '24.506125', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5713', '和通里边检宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5714', '翰林苑', '福建省', '厦门市', '湖里区', null, '118.160364', '24.520518', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5715', '玉鹭苑', '福建省', '厦门市', '湖里区', null, '118.154781', '24.501121', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5716', '滨海森林二期', '福建省', '厦门市', '湖里区', null, '118.097303', '24.502602', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5717', '韵园公寓', '福建省', '厦门市', '湖里区', null, '118.10884', '24.520802', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5718', '玉泉花园', '福建省', '厦门市', '湖里区', null, '118.108891', '24.520561', '2018-01-03 20:19:01', null, '2018-01-04 19:29:04', null);
INSERT INTO `t_plot` VALUES ('5719', '厦北铁路小区', '福建省', '厦门市', '湖里区', null, '118.118029', '24.537165', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5720', '西埭公寓', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5721', '特房五缘尊墅', '福建省', '厦门市', '湖里区', null, '118.170962', '24.525517', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5722', '都市港湾', '福建省', '厦门市', '湖里区', null, '118.088312', '24.492459', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5723', '亿力悦海', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5724', '御之苑', '福建省', '厦门市', '湖里区', null, '118.153349', '24.517022', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5725', '一元花园', '福建省', '厦门市', '湖里区', null, '118.15451', '24.498818', '2018-01-03 20:19:01', null, '2018-01-04 19:29:05', null);
INSERT INTO `t_plot` VALUES ('5726', '水晶苹果', '福建省', '厦门市', '湖里区', null, '118.158019', '24.515756', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5727', '港吉大厦', '福建省', '厦门市', '湖里区', null, '118.086123', '24.493226', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5728', '锦绣金山', '福建省', '厦门市', '湖里区', null, '118.160861', '24.506524', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5729', '时尚Party', '福建省', '厦门市', '湖里区', null, '118.139887', '24.499159', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5730', '恒丰花园', '福建省', '厦门市', '湖里区', null, '118.100171', '24.513904', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5731', '特房华苑', '福建省', '厦门市', '湖里区', null, '118.100405', '24.510016', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5732', '雅景花园', '福建省', '厦门市', '湖里区', null, '118.115244', '24.513288', '2018-01-03 20:19:01', null, '2018-01-04 19:29:06', null);
INSERT INTO `t_plot` VALUES ('5733', '南泉小筑', '福建省', '厦门市', '湖里区', null, '118.109784', '24.521851', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5734', '世纪嘉园西区', '福建省', '厦门市', '湖里区', null, '118.116609', '24.521717', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5735', '古龙花园(别墅)', '福建省', '厦门市', '湖里区', null, '118.136312', '24.510601', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5736', '天宇花园', '福建省', '厦门市', '湖里区', null, '118.152166', '24.496731', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5737', '彩虹花园', '福建省', '厦门市', '湖里区', null, '118.145086', '24.499629', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5738', '海天大厦', '福建省', '厦门市', '湖里区', null, '118.109743', '24.511187', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5739', '五缘公寓', '福建省', '厦门市', '湖里区', null, '118.190066', '24.530395', '2018-01-03 20:19:01', null, '2018-01-04 19:29:07', null);
INSERT INTO `t_plot` VALUES ('5740', '悦星园', '福建省', '厦门市', '湖里区', null, '118.108379', '24.521479', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5741', '长乐村', '福建省', '厦门市', '湖里区', null, '118.1094', '24.527072', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5742', '福隆花园', '福建省', '厦门市', '湖里区', null, '118.098071', '24.498353', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5743', '源昌大厦', '福建省', '厦门市', '湖里区', null, '118.087504', '24.492902', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5744', '徐厝和悦里小区', '福建省', '厦门市', '湖里区', null, '118.10699', '24.520715', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5745', '国贸蓝海(别墅)', '福建省', '厦门市', '湖里区', null, '118.191281', '24.533091', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5746', '一元花园二期梦想城真', '福建省', '厦门市', '湖里区', null, '118.152792', '24.499107', '2018-01-03 20:19:01', null, '2018-01-04 19:29:08', null);
INSERT INTO `t_plot` VALUES ('5747', '禹洲香槟城', '福建省', '厦门市', '湖里区', null, '118.157505', '24.513446', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5748', '明园花园', '福建省', '厦门市', '湖里区', null, '118.119722', '24.517315', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5749', '区政府宿舍楼', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5750', '悦华公寓', '福建省', '厦门市', '湖里区', null, '118.113995', '24.52029', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5751', '兴湖花园', '福建省', '厦门市', '湖里区', null, '118.103463', '24.520476', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5752', '仙洞山庄', '福建省', '厦门市', '湖里区', null, '118.11707', '24.508284', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5753', '中骏天誉(别墅)', '福建省', '厦门市', '湖里区', null, '118.169579', '24.51602', '2018-01-03 20:19:01', null, '2018-01-04 19:29:09', null);
INSERT INTO `t_plot` VALUES ('5754', '鸿图苑', '福建省', '厦门市', '湖里区', null, '118.10588', '24.522831', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5755', '特祥苑', '福建省', '厦门市', '湖里区', null, '118.106081', '24.522107', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5756', '天宝花园', '福建省', '厦门市', '湖里区', null, '118.146359', '24.502899', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5757', '和宁里小区', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5758', '太微山庄', '福建省', '厦门市', '湖里区', null, '118.154645', '24.50878', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5759', '公交宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5760', '嘉诚花园', '福建省', '厦门市', '湖里区', null, '118.13496', '24.513163', '2018-01-03 20:19:01', null, '2018-01-04 19:29:10', null);
INSERT INTO `t_plot` VALUES ('5761', '阳光美地', '福建省', '厦门市', '湖里区', null, '118.154425', '24.507889', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5762', '中环贵宾楼', '福建省', '厦门市', '湖里区', null, '118.141033', '24.499643', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5763', '金山花园', '福建省', '厦门市', '湖里区', null, '118.162177', '24.506151', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5764', '五缘印象', '福建省', '厦门市', '湖里区', null, '118.166488', '24.534614', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5765', '枋湖花园', '福建省', '厦门市', '湖里区', null, '118.147423', '24.51849', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5766', '五通小区一里', '福建省', '厦门市', '湖里区', null, '118.198752', '24.51401', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5767', '特房五缘尊府', '福建省', '厦门市', '湖里区', null, '118.171113', '24.524568', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5768', '华昌小区', '福建省', '厦门市', '湖里区', null, '118.10486', '24.511553', '2018-01-03 20:19:01', null, '2018-01-04 19:29:11', null);
INSERT INTO `t_plot` VALUES ('5770', '泰和花园', '福建省', '厦门市', '湖里区', null, '118.146536', '24.498928', '2018-01-03 20:19:01', null, '2018-01-04 19:29:12', null);
INSERT INTO `t_plot` VALUES ('5771', '东渡小区', '福建省', '厦门市', '湖里区', null, '118.090542', '24.496378', '2018-01-03 20:19:01', null, '2018-01-04 19:29:12', null);
INSERT INTO `t_plot` VALUES ('5772', '桃源大厦', '福建省', '厦门市', '湖里区', null, '118.140001', '24.503918', '2018-01-03 20:19:01', null, '2018-01-04 19:29:12', null);
INSERT INTO `t_plot` VALUES ('5773', '雪梨星光三期', '福建省', '厦门市', '湖里区', null, '118.135461', '24.517018', '2018-01-03 20:19:01', null, '2018-01-04 19:29:12', null);
INSERT INTO `t_plot` VALUES ('5774', '三航公寓', '福建省', '厦门市', '湖里区', null, '118.102199', '24.521645', '2018-01-03 20:19:01', null, '2018-01-04 19:29:12', null);
INSERT INTO `t_plot` VALUES ('5775', '新景园', '福建省', '厦门市', '湖里区', null, '118.136546', '24.498316', '2018-01-03 20:19:01', null, '2018-01-04 19:29:12', null);
INSERT INTO `t_plot` VALUES ('5777', '大唐世家八期', '福建省', '厦门市', '湖里区', null, '118.13106', '24.51869', '2018-01-03 20:19:01', null, '2018-01-04 19:29:13', null);
INSERT INTO `t_plot` VALUES ('5778', '富贵门花园', '福建省', '厦门市', '湖里区', null, '118.153355', '24.527651', '2018-01-03 20:19:01', null, '2018-01-04 19:29:13', null);
INSERT INTO `t_plot` VALUES ('5779', '华成花园', '福建省', '厦门市', '湖里区', null, '118.135025', '24.502433', '2018-01-03 20:19:01', null, '2018-01-04 19:29:13', null);
INSERT INTO `t_plot` VALUES ('5780', '寨上生活小区', '福建省', '厦门市', '湖里区', null, '118.10937', '24.528012', '2018-01-03 20:19:01', null, '2018-01-04 19:29:13', null);
INSERT INTO `t_plot` VALUES ('5781', '长安大厦', '福建省', '厦门市', '湖里区', null, '118.140334', '24.498157', '2018-01-03 20:19:01', null, '2018-01-04 19:29:13', null);
INSERT INTO `t_plot` VALUES ('5782', '兴隆阁', '福建省', '厦门市', '湖里区', null, '118.100783', '24.514164', '2018-01-03 20:19:01', null, '2018-01-04 19:29:13', null);
INSERT INTO `t_plot` VALUES ('5783', '台北新村', '福建省', '厦门市', '湖里区', null, '118.120795', '24.512176', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5784', '德辉花园', '福建省', '厦门市', '湖里区', null, '118.10997', '24.511004', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5785', '东海山庄', '福建省', '厦门市', '湖里区', null, '118.147208', '24.512774', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5786', '雪梨星光单身公寓', '福建省', '厦门市', '湖里区', null, '118.133215', '24.514666', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5787', '祥店新村1区', '福建省', '厦门市', '湖里区', null, '118.154046', '24.506956', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5788', '悦华园', '福建省', '厦门市', '湖里区', null, '118.11091', '24.520519', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5789', '国际邮轮城一期', '福建省', '厦门市', '湖里区', null, '118.08285', '24.486954', '2018-01-03 20:19:01', null, '2018-01-04 19:29:14', null);
INSERT INTO `t_plot` VALUES ('5790', '边检宿舍', '福建省', '厦门市', '湖里区', null, '118.097932', '24.499725', '2018-01-03 20:19:01', null, '2018-01-04 19:29:15', null);
INSERT INTO `t_plot` VALUES ('5791', '加州花园', '福建省', '厦门市', '湖里区', null, '118.133406', '24.517661', '2018-01-03 20:19:01', null, '2018-01-04 19:29:15', null);
INSERT INTO `t_plot` VALUES ('5792', '大唐世家七期', '福建省', '厦门市', '湖里区', null, '118.132256', '24.517031', '2018-01-03 20:19:01', null, '2018-01-04 19:29:15', null);
INSERT INTO `t_plot` VALUES ('5794', '融景湾', '福建省', '厦门市', '湖里区', null, '118.166822', '24.50408', '2018-01-03 20:19:01', null, '2018-01-04 19:29:15', null);
INSERT INTO `t_plot` VALUES ('5795', '国宝新城', '福建省', '厦门市', '湖里区', null, '118.13706', '24.503763', '2018-01-03 20:19:01', null, '2018-01-04 19:29:15', null);
INSERT INTO `t_plot` VALUES ('5796', '世纪嘉园东区', '福建省', '厦门市', '湖里区', null, '118.118068', '24.520895', '2018-01-03 20:19:01', null, '2018-01-04 19:29:15', null);
INSERT INTO `t_plot` VALUES ('5797', '赣商海领馆', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5798', '佳丽豪庭', '福建省', '厦门市', '湖里区', null, '118.108247', '24.509128', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5799', '双狮山路14号', '福建省', '厦门市', '湖里区', null, '118.086635', '24.494816', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5800', '龙腾花园二期', '福建省', '厦门市', '湖里区', null, '118.123957', '24.52128', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5801', '银都广场', '福建省', '厦门市', '湖里区', null, '118.100855', '24.511242', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5802', '建邦大厦', '福建省', '厦门市', '湖里区', null, '118.139104', '24.504267', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5803', '天地花园', '福建省', '厦门市', '湖里区', null, '118.138713', '24.502798', '2018-01-03 20:19:01', null, '2018-01-04 19:29:16', null);
INSERT INTO `t_plot` VALUES ('5804', '龙门天下', '福建省', '厦门市', '湖里区', null, '118.133974', '24.503984', '2018-01-03 20:19:01', null, '2018-01-04 19:29:17', null);
INSERT INTO `t_plot` VALUES ('5805', '福园公寓', '福建省', '厦门市', '湖里区', null, '118.134033', '24.502383', '2018-01-03 20:19:01', null, '2018-01-04 19:29:17', null);
INSERT INTO `t_plot` VALUES ('5806', '滨海森林一期', '福建省', '厦门市', '湖里区', null, '118.097303', '24.502602', '2018-01-03 20:19:01', null, '2018-01-04 19:29:17', null);
INSERT INTO `t_plot` VALUES ('5807', '裕隆国际大厦', '福建省', '厦门市', '湖里区', null, '118.165674', '24.534133', '2018-01-03 20:19:01', null, '2018-01-04 19:29:17', null);
INSERT INTO `t_plot` VALUES ('5808', '东渡华城', '福建省', '厦门市', '湖里区', null, '118.087524', '24.493945', '2018-01-03 20:19:01', null, '2018-01-04 19:29:17', null);
INSERT INTO `t_plot` VALUES ('5810', '港务宿舍楼', '福建省', '厦门市', '湖里区', null, '118.086824', '24.494277', '2018-01-03 20:19:01', null, '2018-01-04 19:29:17', null);
INSERT INTO `t_plot` VALUES ('5811', '外代小区', '福建省', '厦门市', '湖里区', null, '118.119561', '24.512399', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5812', '都市华庭', '福建省', '厦门市', '湖里区', null, '118.137998', '24.498074', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5813', '金鼎社区', '福建省', '厦门市', '湖里区', null, '118.089192', '24.494383', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5814', '宝鹭苑', '福建省', '厦门市', '湖里区', null, '118.084733', '24.490027', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5815', '国贸天琴湾2期', '福建省', '厦门市', '湖里区', null, '118.175845', '24.524214', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5816', '信宏大厦', '福建省', '厦门市', '湖里区', null, '118.107286', '24.515638', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5817', '假日江南雅筑', '福建省', '厦门市', '湖里区', null, '118.16178', '24.508886', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5818', '永乐阁', '福建省', '厦门市', '湖里区', null, '118.107278', '24.526709', '2018-01-03 20:19:01', null, '2018-01-04 19:29:18', null);
INSERT INTO `t_plot` VALUES ('5819', '翔鹭花城三期', '福建省', '厦门市', '湖里区', null, '118.111212', '24.532094', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5820', '万达SOHO公寓', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5821', '古龙花园', '福建省', '厦门市', '湖里区', null, '118.136312', '24.510601', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5822', '华东花园', '福建省', '厦门市', '湖里区', null, '118.126806', '24.509803', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5823', '明发豪庭', '福建省', '厦门市', '湖里区', null, '118.139588', '24.500285', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5824', '盛唐苑', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5825', '金山城', '福建省', '厦门市', '湖里区', null, '118.16505', '24.506658', '2018-01-03 20:19:01', null, '2018-01-04 19:29:19', null);
INSERT INTO `t_plot` VALUES ('5826', '新城国际', '福建省', '厦门市', '湖里区', null, '118.160393', '24.508488', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5827', '小城故事禹洲新城一期', '福建省', '厦门市', '湖里区', null, '118.093682', '24.501243', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5828', '金彩花苑', '福建省', '厦门市', '湖里区', null, '118.147126', '24.499278', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5829', '悦明阁', '福建省', '厦门市', '湖里区', null, '118.107385', '24.521396', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5830', '中央湾区珊瑚海', '福建省', '厦门市', '湖里区', null, '118.167388', '24.523937', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5831', '优山美地(别墅)', '福建省', '厦门市', '湖里区', null, '118.097695', '24.505198', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5832', '乌石埔新村', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:20', null);
INSERT INTO `t_plot` VALUES ('5833', '金福缘新城', '福建省', '厦门市', '湖里区', null, '118.16927', '24.539716', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5834', '永同昌大厦', '福建省', '厦门市', '湖里区', null, '118.133742', '24.501877', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5835', '宏益华府', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5836', '海天山庄', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5837', '金祥花园', '福建省', '厦门市', '湖里区', null, '118.154466', '24.506996', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5838', '现代家园', '福建省', '厦门市', '湖里区', null, '118.127329', '24.515695', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5839', '金北花园', '福建省', '厦门市', '湖里区', null, '118.158259', '24.501097', '2018-01-03 20:19:01', null, '2018-01-04 19:29:21', null);
INSERT INTO `t_plot` VALUES ('5840', '泰和大厦', '福建省', '厦门市', '湖里区', null, '118.145755', '24.498318', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5841', '龙湖花园一期', '福建省', '厦门市', '湖里区', null, '118.147583', '24.5174', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5842', '世茂湖滨首府B区', '福建省', '厦门市', '湖里区', null, '118.175715', '24.498418', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5843', '水利水电宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5844', '都市雅苑', '福建省', '厦门市', '湖里区', null, '118.106519', '24.520947', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5845', '万华中心', '福建省', '厦门市', '湖里区', null, '118.166636', '24.536016', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5846', '国联大厦', '福建省', '厦门市', '湖里区', null, '118.138209', '24.499303', '2018-01-03 20:19:01', null, '2018-01-04 19:29:22', null);
INSERT INTO `t_plot` VALUES ('5847', '南山新村', '福建省', '厦门市', '湖里区', null, '118.110625', '24.506441', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5848', '力拓大厦', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5849', '东兴小区', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5850', '鑫湖花园', '福建省', '厦门市', '湖里区', null, '118.15633', '24.516124', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5851', '国贸天琴湾1期(别墅)', '福建省', '厦门市', '湖里区', null, '118.175845', '24.524214', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5852', '晶尚名居', '福建省', '厦门市', '湖里区', null, '118.161242', '24.520259', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5853', '榕海花园', '福建省', '厦门市', '湖里区', null, '118.091013', '24.502107', '2018-01-03 20:19:01', null, '2018-01-04 19:29:23', null);
INSERT INTO `t_plot` VALUES ('5854', '亿星大厦', '福建省', '厦门市', '湖里区', null, '118.139895', '24.503351', '2018-01-03 20:19:01', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5855', '梅阳花园', '福建省', '厦门市', '湖里区', null, '118.139381', '24.509883', '2018-01-03 20:19:01', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5856', '海天小区', '福建省', '厦门市', '湖里区', null, '118.108282', '24.511727', '2018-01-03 20:19:02', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5857', '雅丽阁', '福建省', '厦门市', '湖里区', null, '118.106055', '24.511134', '2018-01-03 20:19:02', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5858', '和通新村', '福建省', '厦门市', '湖里区', null, '118.099898', '24.501613', '2018-01-03 20:19:02', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5859', '紫金家园南区', '福建省', '厦门市', '湖里区', null, '118.169512', '24.527846', '2018-01-03 20:19:02', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5860', '建发央玺', '福建省', '厦门市', '湖里区', null, '118.170455', '24.529429', '2018-01-03 20:19:02', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5861', '中央湾区琥珀湾', '福建省', '厦门市', '湖里区', null, '118.167388', '24.523937', '2018-01-03 20:19:02', null, '2018-01-04 19:29:24', null);
INSERT INTO `t_plot` VALUES ('5862', '恒祥新苑', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:25', null);
INSERT INTO `t_plot` VALUES ('5863', '江头小区', '福建省', '厦门市', '湖里区', null, '118.141886', '24.500811', '2018-01-03 20:19:02', null, '2018-01-04 19:29:25', null);
INSERT INTO `t_plot` VALUES ('5864', '兴山花园', '福建省', '厦门市', '湖里区', null, '118.13785', '24.507363', '2018-01-03 20:19:02', null, '2018-01-04 19:29:25', null);
INSERT INTO `t_plot` VALUES ('5865', '韵园别墅', '福建省', '厦门市', '湖里区', null, '118.108871', '24.520132', '2018-01-03 20:19:02', null, '2018-01-04 19:29:25', null);
INSERT INTO `t_plot` VALUES ('5866', '金山小区', '福建省', '厦门市', '湖里区', null, '118.163943', '24.508266', '2018-01-03 20:19:02', null, '2018-01-04 19:29:25', null);
INSERT INTO `t_plot` VALUES ('5867', '龙潭花园', '福建省', '厦门市', '湖里区', null, '118.139278', '24.513375', '2018-01-03 20:19:02', null, '2018-01-04 19:29:25', null);
INSERT INTO `t_plot` VALUES ('5868', '城里', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5869', '华永天地', '福建省', '厦门市', '湖里区', null, '118.139353', '24.501105', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5870', '园山小区', '福建省', '厦门市', '湖里区', null, '118.140419', '24.516838', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5871', '兴隆新村', '福建省', '厦门市', '湖里区', null, '118.133918', '24.515455', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5872', '信达宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5873', '世茂湖滨首府A区', '福建省', '厦门市', '湖里区', null, '118.175715', '24.498418', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5874', '金龙大厦', '福建省', '厦门市', '湖里区', null, '118.094069', '24.50634', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5875', '高林居住区南区', '福建省', '厦门市', '湖里区', null, '118.195251', '24.509136', '2018-01-03 20:19:02', null, '2018-01-04 19:29:26', null);
INSERT INTO `t_plot` VALUES ('5876', '商检宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5877', '永同昌假日国际公寓', '福建省', '厦门市', '湖里区', null, '118.138773', '24.504893', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5878', '万科中心云玺', '福建省', '厦门市', '湖里区', null, '118.193053', '24.514825', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5879', '博士山庄', '福建省', '厦门市', '湖里区', null, '118.119403', '24.51482', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5880', '象屿花园', '福建省', '厦门市', '湖里区', null, '118.103903', '24.523021', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5881', '江浦南里', '福建省', '厦门市', '湖里区', null, '118.142634', '24.500359', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5882', '信诚VBO', '福建省', '厦门市', '湖里区', null, '118.166854', '24.530646', '2018-01-03 20:19:02', null, '2018-01-04 19:29:27', null);
INSERT INTO `t_plot` VALUES ('5883', '科瑞大厦', '福建省', '厦门市', '湖里区', null, '118.138469', '24.498371', '2018-01-03 20:19:02', null, '2018-01-04 19:29:28', null);
INSERT INTO `t_plot` VALUES ('5884', '建发中央湾区央墅', '福建省', '厦门市', '湖里区', null, '118.171284', '24.52233', '2018-01-03 20:19:02', null, '2018-01-04 19:29:28', null);
INSERT INTO `t_plot` VALUES ('5885', '华景小区', '福建省', '厦门市', '湖里区', null, '118.118955', '24.513546', '2018-01-03 20:19:02', null, '2018-01-04 19:29:28', null);
INSERT INTO `t_plot` VALUES ('5886', '南泰苑', '福建省', '厦门市', '湖里区', null, '118.111048', '24.508776', '2018-01-03 20:19:02', null, '2018-01-04 19:29:28', null);
INSERT INTO `t_plot` VALUES ('5887', '福满园', '福建省', '厦门市', '湖里区', null, '118.153436', '24.505482', '2018-01-03 20:19:02', null, '2018-01-04 19:29:28', null);
INSERT INTO `t_plot` VALUES ('5888', '建发湾区SOHO', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:28', null);
INSERT INTO `t_plot` VALUES ('5889', '江鹭花园', '福建省', '厦门市', '湖里区', null, '118.140869', '24.510925', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5890', '嘉景花园一期', '福建省', '厦门市', '湖里区', null, '118.132303', '24.512025', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5891', '恒禾七尚(别墅)', '福建省', '厦门市', '湖里区', null, '118.189425', '24.531134', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5892', '供电局宿舍', '福建省', '厦门市', '湖里区', null, '118.115674', '24.514377', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5893', '台湾街297-299号小区', '福建省', '厦门市', '湖里区', null, '118.138062', '24.504134', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5894', '德辉花园2期', '福建省', '厦门市', '湖里区', null, '118.10997', '24.511004', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5895', '新湖花园', '福建省', '厦门市', '湖里区', null, '118.126516', '24.514379', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5896', '国际山庄(别墅)', '福建省', '厦门市', '湖里区', null, '118.159895', '24.510312', '2018-01-03 20:19:02', null, '2018-01-04 19:29:29', null);
INSERT INTO `t_plot` VALUES ('5898', '华海苑', '福建省', '厦门市', '湖里区', null, '118.103521', '24.510197', '2018-01-03 20:19:02', null, '2018-01-04 19:29:30', null);
INSERT INTO `t_plot` VALUES ('5899', '湖里公安分局宿舍', '福建省', '厦门市', '湖里区', null, '118.116274', '24.515039', '2018-01-03 20:19:02', null, '2018-01-04 19:29:30', null);
INSERT INTO `t_plot` VALUES ('5900', '嘉隆公寓', '福建省', '厦门市', '湖里区', null, '118.14134', '24.511103', '2018-01-03 20:19:02', null, '2018-01-04 19:29:30', null);
INSERT INTO `t_plot` VALUES ('5901', '永建顶尚(别墅)', '福建省', '厦门市', '湖里区', null, '118.156204', '24.518742', '2018-01-03 20:19:02', null, '2018-01-04 19:29:30', null);
INSERT INTO `t_plot` VALUES ('5902', '江头花园', '福建省', '厦门市', '湖里区', null, '118.134826', '24.504135', '2018-01-03 20:19:02', null, '2018-01-04 19:29:30', null);
INSERT INTO `t_plot` VALUES ('5903', '南山华府', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:30', null);
INSERT INTO `t_plot` VALUES ('5904', '沧海苑', '福建省', '厦门市', '湖里区', null, '118.093606', '24.502083', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5905', '航空大厦', '福建省', '厦门市', '湖里区', null, '118.141413', '24.500123', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5906', '和谐家园', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5907', '和旭里小区', '福建省', '厦门市', '湖里区', null, '118.095211', '24.505381', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5908', '豪峰大厦', '福建省', '厦门市', '湖里区', null, '118.136625', '24.496986', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5909', '集鑫佳园', '福建省', '厦门市', '湖里区', null, '118.160519', '24.503436', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5910', '嘉兴园', '福建省', '厦门市', '湖里区', null, '118.142043', '24.498349', '2018-01-03 20:19:02', null, '2018-01-04 19:29:31', null);
INSERT INTO `t_plot` VALUES ('5911', '岭下小区', '福建省', '厦门市', '湖里区', null, '118.169327', '24.53974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5912', '嘉景花园', '福建省', '厦门市', '湖里区', null, '118.132674', '24.511748', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5913', '明月园', '福建省', '厦门市', '湖里区', null, '118.086139', '24.488715', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5914', '嘉湖花园', '福建省', '厦门市', '湖里区', null, '118.108157', '24.5201', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5915', '半山豪庭', '福建省', '厦门市', '湖里区', null, '118.12499', '24.508682', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5916', '金鸿花园', '福建省', '厦门市', '湖里区', null, '118.154659', '24.497749', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5917', '南山公寓', '福建省', '厦门市', '湖里区', null, '118.111418', '24.507737', '2018-01-03 20:19:02', null, '2018-01-04 19:29:32', null);
INSERT INTO `t_plot` VALUES ('5918', '金鹭花园', '福建省', '厦门市', '湖里区', null, '118.164542', '24.510365', '2018-01-03 20:19:02', null, '2018-01-04 19:29:33', null);
INSERT INTO `t_plot` VALUES ('5919', '雪梨星光二期', '福建省', '厦门市', '湖里区', null, '118.135461', '24.517018', '2018-01-03 20:19:02', null, '2018-01-04 19:29:33', null);
INSERT INTO `t_plot` VALUES ('5920', '艾德花园一期', '福建省', '厦门市', '湖里区', null, '118.144262', '24.53641', '2018-01-03 20:19:02', null, '2018-01-04 19:29:33', null);
INSERT INTO `t_plot` VALUES ('5921', '金庄花园', '福建省', '厦门市', '湖里区', null, '118.107495', '24.523374', '2018-01-03 20:19:02', null, '2018-01-04 19:29:33', null);
INSERT INTO `t_plot` VALUES ('5922', '金山国际', '福建省', '厦门市', '湖里区', null, '118.163993', '24.51148', '2018-01-03 20:19:02', null, '2018-01-04 19:29:33', null);
INSERT INTO `t_plot` VALUES ('5923', '安泰花园', '福建省', '厦门市', '湖里区', null, '118.14141', '24.513553', '2018-01-03 20:19:02', null, '2018-01-04 19:29:33', null);
INSERT INTO `t_plot` VALUES ('5924', '富豪花园', '福建省', '厦门市', '湖里区', null, '118.120239', '24.515261', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5925', '公园新村', '福建省', '厦门市', '湖里区', null, '118.114634', '24.510576', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5926', '金秋花园3期', '福建省', '厦门市', '湖里区', null, '118.157128', '24.503242', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5927', '宝龙御湖官邸(别墅)', '福建省', '厦门市', '湖里区', null, '118.175403', '24.500325', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5928', '嘉达花园', '福建省', '厦门市', '湖里区', null, '118.136088', '24.502142', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5929', '工商宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5930', '林业花苑', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5931', '金尚首府', '福建省', '厦门市', '湖里区', null, '118.152531', '24.499604', '2018-01-03 20:19:02', null, '2018-01-04 19:29:34', null);
INSERT INTO `t_plot` VALUES ('5933', '金枋世家', '福建省', '厦门市', '湖里区', null, '118.144826', '24.519901', '2018-01-03 20:19:02', null, '2018-01-04 19:29:35', null);
INSERT INTO `t_plot` VALUES ('5934', '龙腾花园一期', '福建省', '厦门市', '湖里区', null, '118.122175', '24.521598', '2018-01-03 20:19:02', null, '2018-01-04 19:29:35', null);
INSERT INTO `t_plot` VALUES ('5935', '国泰大厦', '福建省', '厦门市', '湖里区', null, '118.134155', '24.503005', '2018-01-03 20:19:02', null, '2018-01-04 19:29:35', null);
INSERT INTO `t_plot` VALUES ('5936', '恒晟大厦', '福建省', '厦门市', '湖里区', null, '118.167234', '24.536304', '2018-01-03 20:19:02', null, '2018-01-04 19:29:35', null);
INSERT INTO `t_plot` VALUES ('5937', '联发五缘湾1号二期熙岸', '福建省', '厦门市', '湖里区', null, '118.182754', '24.538406', '2018-01-03 20:19:02', null, '2018-01-04 19:29:35', null);
INSERT INTO `t_plot` VALUES ('5938', '琴岛花园', '福建省', '厦门市', '湖里区', null, '118.160362', '24.505978', '2018-01-03 20:19:02', null, '2018-01-04 19:29:35', null);
INSERT INTO `t_plot` VALUES ('5939', '东方威尼斯', '福建省', '厦门市', '湖里区', null, '118.138142', '24.502512', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5940', '惠鸿花园', '福建省', '厦门市', '湖里区', null, '118.153743', '24.501663', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5941', '湖边美苑', '福建省', '厦门市', '湖里区', null, '118.17515', '24.507325', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5942', '嘉隆建材城小区', '福建省', '厦门市', '湖里区', null, '118.141299', '24.508911', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5943', '龙湖花园二期', '福建省', '厦门市', '湖里区', null, '118.146593', '24.51744', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5944', '艾德花园二期', '福建省', '厦门市', '湖里区', null, '118.144262', '24.53641', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5945', '宏山新村', '福建省', '厦门市', '湖里区', null, '118.157191', '24.509943', '2018-01-03 20:19:02', null, '2018-01-04 19:29:36', null);
INSERT INTO `t_plot` VALUES ('5946', '园山北里小区', '福建省', '厦门市', '湖里区', null, '118.132303', '24.513304', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5947', '豪景阁', '福建省', '厦门市', '湖里区', null, '118.120314', '24.514525', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5948', '金海花园', '福建省', '厦门市', '湖里区', null, '118.096231', '24.502712', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5949', '江浦北里', '福建省', '厦门市', '湖里区', null, '118.14268', '24.501728', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5950', '荣华大厦', '福建省', '厦门市', '湖里区', null, '118.104446', '24.514479', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5951', '嘉景花园二期', '福建省', '厦门市', '湖里区', null, '118.133947', '24.511435', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5952', '三航六公司宿舍区', '福建省', '厦门市', '湖里区', null, '118.088134', '24.490963', '2018-01-03 20:19:02', null, '2018-01-04 19:29:37', null);
INSERT INTO `t_plot` VALUES ('5953', '高林居住区(二期)', '福建省', '厦门市', '湖里区', null, '118.195251', '24.509136', '2018-01-03 20:19:02', null, '2018-01-04 19:29:38', null);
INSERT INTO `t_plot` VALUES ('5954', '濠头村新庄园', '福建省', '厦门市', '湖里区', null, '118.093695', '24.503619', '2018-01-03 20:19:02', null, '2018-01-04 19:29:38', null);
INSERT INTO `t_plot` VALUES ('5955', '金盛大厦', '福建省', '厦门市', '湖里区', null, '118.139604', '24.502603', '2018-01-03 20:19:02', null, '2018-01-04 19:29:38', null);
INSERT INTO `t_plot` VALUES ('5956', '金山公寓', '福建省', '厦门市', '湖里区', null, '118.139517', '24.511679', '2018-01-03 20:19:02', null, '2018-01-04 19:29:38', null);
INSERT INTO `t_plot` VALUES ('5957', '海丰大厦', '福建省', '厦门市', '湖里区', null, '118.109653', '24.511908', '2018-01-03 20:19:02', null, '2018-01-04 19:29:38', null);
INSERT INTO `t_plot` VALUES ('5958', '达嘉馨园', '福建省', '厦门市', '湖里区', null, '118.165908', '24.510246', '2018-01-03 20:19:02', null, '2018-01-04 19:29:38', null);
INSERT INTO `t_plot` VALUES ('5959', '国贸天琴湾2期(别墅)', '福建省', '厦门市', '湖里区', null, '118.175845', '24.524214', '2018-01-03 20:19:02', null, '2018-01-04 19:29:39', null);
INSERT INTO `t_plot` VALUES ('5960', '金山湖景大厦', '福建省', '厦门市', '湖里区', null, '118.168171', '24.504465', '2018-01-03 20:19:02', null, '2018-01-04 19:29:39', null);
INSERT INTO `t_plot` VALUES ('5961', '信达大厦二期', '福建省', '厦门市', '湖里区', null, '118.101977', '24.515392', '2018-01-03 20:19:02', null, '2018-01-04 19:29:39', null);
INSERT INTO `t_plot` VALUES ('5962', '源昌鑫海湾(别墅)', '福建省', '厦门市', '湖里区', null, '118.167861', '24.547484', '2018-01-03 20:19:02', null, '2018-01-04 19:29:39', null);
INSERT INTO `t_plot` VALUES ('5963', '半山林海', '福建省', '厦门市', '湖里区', null, '118.098789', '24.503116', '2018-01-03 20:19:02', null, '2018-01-04 19:29:39', null);
INSERT INTO `t_plot` VALUES ('5964', '新丰家园', '福建省', '厦门市', '湖里区', null, '118.097943', '24.500616', '2018-01-03 20:19:02', null, '2018-01-04 19:29:39', null);
INSERT INTO `t_plot` VALUES ('5965', '小城故事禹州新城二期', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5966', '福洋花园', '福建省', '厦门市', '湖里区', null, '118.140226', '24.504639', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5967', '新景七星府邸', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5968', '大唐世家三期', '福建省', '厦门市', '湖里区', null, '118.128961', '24.515863', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5969', '丰泽苑金丰里', '福建省', '厦门市', '湖里区', null, '118.154337', '24.496529', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5970', '港务小区', '福建省', '厦门市', '湖里区', null, '118.098004', '24.500885', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5971', '大唐世家五期', '福建省', '厦门市', '湖里区', null, '118.129034', '24.515929', '2018-01-03 20:19:02', null, '2018-01-04 19:29:40', null);
INSERT INTO `t_plot` VALUES ('5972', '兆翔海天韵', '福建省', '厦门市', '湖里区', null, '118.137662', '24.558732', '2018-01-03 20:19:02', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5973', '金鼎巷38-46号小区', '福建省', '厦门市', '湖里区', null, '118.090143', '24.4941', '2018-01-03 20:19:02', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5974', '万科湖心岛(别墅)', '福建省', '厦门市', '湖里区', null, '118.170133', '24.494305', '2018-01-03 20:19:02', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5975', '御之苑(别墅)', '福建省', '厦门市', '湖里区', null, '118.153349', '24.517022', '2018-01-03 20:19:02', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5976', '半山豪园一期', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5977', '德信庄园', '福建省', '厦门市', '湖里区', null, '118.108338', '24.50866', '2018-01-03 20:19:03', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5978', '恒丰花园2期', '福建省', '厦门市', '湖里区', null, '118.100171', '24.513904', '2018-01-03 20:19:03', null, '2018-01-04 19:29:41', null);
INSERT INTO `t_plot` VALUES ('5979', '青翔花园', '福建省', '厦门市', '湖里区', null, '118.106701', '24.508997', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5980', '世茂湖滨首府别墅', '福建省', '厦门市', '湖里区', null, '118.175715', '24.498418', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5981', '福龙公寓', '福建省', '厦门市', '湖里区', null, '118.145759', '24.502939', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5982', '宏伟大厦', '福建省', '厦门市', '湖里区', null, '118.156742', '24.520219', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5983', '神山三航小区', '福建省', '厦门市', '湖里区', null, '118.110217', '24.535156', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5984', '都市阳光', '福建省', '厦门市', '湖里区', null, '118.10887', '24.508571', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5985', '凯城花园', '福建省', '厦门市', '湖里区', null, '118.126836', '24.513163', '2018-01-03 20:19:03', null, '2018-01-04 19:29:42', null);
INSERT INTO `t_plot` VALUES ('5986', '半山豪园二期', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:43', null);
INSERT INTO `t_plot` VALUES ('5987', '和悦里20-24号', '福建省', '厦门市', '湖里区', null, '118.107225', '24.521363', '2018-01-03 20:19:03', null, '2018-01-04 19:29:43', null);
INSERT INTO `t_plot` VALUES ('5988', '古龙居住公园', '福建省', '厦门市', '湖里区', null, '118.160063', '24.525334', '2018-01-03 20:19:03', null, '2018-01-04 19:29:43', null);
INSERT INTO `t_plot` VALUES ('5989', '明德花园', '福建省', '厦门市', '湖里区', null, '118.127971', '24.511966', '2018-01-03 20:19:03', null, '2018-01-04 19:29:43', null);
INSERT INTO `t_plot` VALUES ('5990', '科技村', '福建省', '厦门市', '湖里区', null, '118.12778', '24.514898', '2018-01-03 20:19:03', null, '2018-01-04 19:29:43', null);
INSERT INTO `t_plot` VALUES ('5991', '华昌路1号', '福建省', '厦门市', '湖里区', null, '118.103803', '24.509072', '2018-01-03 20:19:03', null, '2018-01-04 19:29:43', null);
INSERT INTO `t_plot` VALUES ('5993', '东渡农行宿舍', '福建省', '厦门市', '湖里区', null, '118.086866', '24.484285', '2018-01-03 20:19:03', null, '2018-01-04 19:29:44', null);
INSERT INTO `t_plot` VALUES ('5994', '安兜社1200号', '福建省', '厦门市', '湖里区', null, '118.138862', '24.530387', '2018-01-03 20:19:03', null, '2018-01-04 19:29:44', null);
INSERT INTO `t_plot` VALUES ('5995', '滨海苑', '福建省', '厦门市', '湖里区', null, '118.099622', '24.511775', '2018-01-03 20:19:03', null, '2018-01-04 19:29:44', null);
INSERT INTO `t_plot` VALUES ('5996', '南山大厦', '福建省', '厦门市', '湖里区', null, '118.10893', '24.50882', '2018-01-03 20:19:03', null, '2018-01-04 19:29:44', null);
INSERT INTO `t_plot` VALUES ('5998', '第二城闽南印象', '福建省', '厦门市', '湖里区', null, '118.150249', '24.506007', '2018-01-03 20:19:03', null, '2018-01-04 19:29:44', null);
INSERT INTO `t_plot` VALUES ('5999', '墩上小区', '福建省', '厦门市', '湖里区', null, '118.167154', '24.540856', '2018-01-03 20:19:03', null, '2018-01-04 19:29:44', null);
INSERT INTO `t_plot` VALUES ('6000', '双狮南里', '福建省', '厦门市', '湖里区', null, '118.084411', '24.490473', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6001', '东辉小区', '福建省', '厦门市', '湖里区', null, '118.090906', '24.494434', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6002', '美源别墅', '福建省', '厦门市', '湖里区', null, '118.108831', '24.509967', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6003', '信源大厦', '福建省', '厦门市', '湖里区', null, '118.104636', '24.515548', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6004', '空港新元宿', '福建省', '厦门市', '湖里区', null, '118.135052', '24.533651', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6005', '华荣路法院宿舍', '福建省', '厦门市', '湖里区', null, '118.10672', '24.489819', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6006', '乔丹大厦', '福建省', '厦门市', '湖里区', null, '118.133538', '24.507516', '2018-01-03 20:19:03', null, '2018-01-04 19:29:45', null);
INSERT INTO `t_plot` VALUES ('6007', '大唐世家三期(别墅)', '福建省', '厦门市', '湖里区', null, '118.128961', '24.515863', '2018-01-03 20:19:03', null, '2018-01-04 19:29:46', null);
INSERT INTO `t_plot` VALUES ('6008', '奔马馨园', '福建省', '厦门市', '湖里区', null, '118.118377', '24.515621', '2018-01-03 20:19:03', null, '2018-01-04 19:29:46', null);
INSERT INTO `t_plot` VALUES ('6009', '和通里小区', '福建省', '厦门市', '湖里区', null, '118.099476', '24.500327', '2018-01-03 20:19:03', null, '2018-01-04 19:29:46', null);
INSERT INTO `t_plot` VALUES ('6010', '东苑邨', '福建省', '厦门市', '湖里区', null, '118.087072', '24.492025', '2018-01-03 20:19:03', null, '2018-01-04 19:29:46', null);
INSERT INTO `t_plot` VALUES ('6011', '鸿发苑', '福建省', '厦门市', '湖里区', null, '118.114354', '24.516716', '2018-01-03 20:19:03', null, '2018-01-04 19:29:46', null);
INSERT INTO `t_plot` VALUES ('6012', '华泰苑(别墅)', '福建省', '厦门市', '湖里区', null, '118.111078', '24.511093', '2018-01-03 20:19:03', null, '2018-01-04 19:29:46', null);
INSERT INTO `t_plot` VALUES ('6013', '海俱宿舍区', '福建省', '厦门市', '湖里区', null, '118.089805', '24.496213', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6014', '东荣社区', '福建省', '厦门市', '湖里区', null, '118.090397', '24.493308', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6015', '华昌路7-61号', '福建省', '厦门市', '湖里区', null, '118.103783', '24.510392', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6016', '东渡农资宿舍', '福建省', '厦门市', '湖里区', null, '118.086866', '24.484285', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6017', '天地雅园', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6018', '金翠院', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6019', '明园大厦', '福建省', '厦门市', '湖里区', null, '118.119377', '24.517218', '2018-01-03 20:19:03', null, '2018-01-04 19:29:47', null);
INSERT INTO `t_plot` VALUES ('6020', '双师山港务宿舍', '福建省', '厦门市', '湖里区', null, '118.098091', '24.500918', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6021', '东渡水务宿舍', '福建省', '厦门市', '湖里区', null, '118.086866', '24.484285', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6022', '建港指挥部宿舍', '福建省', '厦门市', '湖里区', null, '118.089711', '24.494288', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6023', '吕岭路69-73号', '福建省', '厦门市', '湖里区', null, '118.139584', '24.498412', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6024', '吕岭路201-211号', '福建省', '厦门市', '湖里区', null, '118.146427', '24.499978', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6025', '湖里大道159-161号', '福建省', '厦门市', '湖里区', null, '118.12201', '24.517784', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6026', '清河公寓', '福建省', '厦门市', '湖里区', null, '118.153791', '24.532913', '2018-01-03 20:19:03', null, '2018-01-04 19:29:48', null);
INSERT INTO `t_plot` VALUES ('6027', '临海苑', '福建省', '厦门市', '湖里区', null, '118.099586', '24.5112', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6028', '枋湖社584号集资房', '福建省', '厦门市', '湖里区', null, '118.148022', '24.520471', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6029', '浦园社', '福建省', '厦门市', '湖里区', null, '118.14292', '24.504221', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6030', '富璟大厦', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6031', '玉岭大厦', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6032', '五通小区二里', '福建省', '厦门市', '湖里区', null, '118.198752', '24.51401', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6033', '三德兴员工新村', '福建省', '厦门市', '湖里区', null, '118.120745', '24.510522', '2018-01-03 20:19:03', null, '2018-01-04 19:29:49', null);
INSERT INTO `t_plot` VALUES ('6034', '临海时代', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6035', '大湖新城', '福建省', '厦门市', '湖里区', null, '118.145805', '24.528346', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6036', '南山丽苑', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6037', '森宝大厦', '福建省', '厦门市', '湖里区', null, '118.116088', '24.515482', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6038', '新海关宿舍', '福建省', '厦门市', '湖里区', null, '118.090059', '24.494613', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6039', '金枋世家二期', '福建省', '厦门市', '湖里区', null, '118.144126', '24.52162', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6040', '禾山阳光公寓', '福建省', '厦门市', '湖里区', null, '118.159489', '24.534066', '2018-01-03 20:19:03', null, '2018-01-04 19:29:50', null);
INSERT INTO `t_plot` VALUES ('6041', '和顺里小区', '福建省', '厦门市', '湖里区', null, '118.096908', '24.503008', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6042', '金叶小区', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6043', '海景曙光', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6044', '东南花园', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6045', '金龙公寓', '福建省', '厦门市', '湖里区', null, '118.140227', '24.509943', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6046', '殿前铁路家园', '福建省', '厦门市', '湖里区', null, '118.1175', '24.537882', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6047', '金山西三里小区', '福建省', '厦门市', '湖里区', null, '118.157482', '24.506874', '2018-01-03 20:19:03', null, '2018-01-04 19:29:51', null);
INSERT INTO `t_plot` VALUES ('6048', '嘉信公寓', '福建省', '厦门市', '湖里区', null, '118.140569', '24.511508', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6049', '和通里港务宿舍楼', '福建省', '厦门市', '湖里区', null, '118.086824', '24.494277', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6050', '嘉福花园', '福建省', '厦门市', '湖里区', null, '118.122261', '24.554925', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6051', '马山安置小区', '福建省', '厦门市', '湖里区', null, '118.157499', '24.506616', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6052', '劳教所宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6053', '昆仑公寓', '福建省', '厦门市', '湖里区', null, '118.096948', '24.503432', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6054', '东渡变电站宿舍', '福建省', '厦门市', '湖里区', null, '118.09096', '24.495391', '2018-01-03 20:19:03', null, '2018-01-04 19:29:52', null);
INSERT INTO `t_plot` VALUES ('6055', '艾德花园', '福建省', '厦门市', '湖里区', null, '118.144262', '24.53641', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6056', '东渡邮电局宿舍', '福建省', '厦门市', '湖里区', null, '118.084708', '24.490027', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6057', '蓝海园', '福建省', '厦门市', '湖里区', null, '118.132726', '24.554703', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6058', '禾山信用社大楼', '福建省', '厦门市', '湖里区', null, '118.140064', '24.512155', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6059', '吕岭综合楼', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6060', '深汇大厦', '福建省', '厦门市', '湖里区', null, '118.100252', '24.511852', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6061', '合兴小区', '福建省', '厦门市', '湖里区', null, '118.136923', '24.509915', '2018-01-03 20:19:03', null, '2018-01-04 19:29:53', null);
INSERT INTO `t_plot` VALUES ('6062', '乌石浦新村二期', '福建省', '厦门市', '湖里区', null, '118.13522', '24.5114', '2018-01-03 20:19:03', null, '2018-01-04 19:29:54', null);
INSERT INTO `t_plot` VALUES ('6063', '华景花园公寓', '福建省', '厦门市', '湖里区', null, '118.119774', '24.513844', '2018-01-03 20:19:03', null, '2018-01-04 19:29:54', null);
INSERT INTO `t_plot` VALUES ('6064', '江村丽苑', '福建省', '厦门市', '湖里区', null, '118.137885', '24.509567', '2018-01-03 20:19:03', null, '2018-01-04 19:29:54', null);
INSERT INTO `t_plot` VALUES ('6065', '凌霄大厦', '福建省', '厦门市', '湖里区', null, '118.152311', '24.496394', '2018-01-03 20:19:03', null, '2018-01-04 19:29:54', null);
INSERT INTO `t_plot` VALUES ('6066', '古龙居住公园(别墅)', '福建省', '厦门市', '湖里区', null, '118.160063', '24.525334', '2018-01-03 20:19:03', null, '2018-01-04 19:29:54', null);
INSERT INTO `t_plot` VALUES ('6067', '祥店新村2区', '福建省', '厦门市', '湖里区', null, '118.152208', '24.506544', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6068', '马垅阳光公寓', '福建省', '厦门市', '湖里区', null, '118.127457', '24.525602', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6069', '皇苑公寓', '福建省', '厦门市', '湖里区', null, '118.11624', '24.514011', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6070', '金林湾花园', '福建省', '厦门市', '湖里区', null, '118.182387', '24.515952', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6071', '金南园', '福建省', '厦门市', '湖里区', null, '118.155098', '24.496361', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6072', '海明大厦', '福建省', '厦门市', '湖里区', null, '118.082887', '24.485708', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6073', '立杰华苑', '福建省', '厦门市', '湖里区', null, '118.104175', '24.510427', '2018-01-03 20:19:03', null, '2018-01-04 19:29:55', null);
INSERT INTO `t_plot` VALUES ('6074', '上苑山庄', '福建省', '厦门市', '湖里区', null, '118.089407', '24.493222', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6075', '万隆国际广场', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6076', '海湾豪景', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6077', '湖里大道37号', '福建省', '厦门市', '湖里区', null, '118.110137', '24.51849', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6078', '绿岭园', '福建省', '厦门市', '湖里区', null, '118.145398', '24.501869', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6079', '阁下的海', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6080', '厦门市禾山供销社', '福建省', '厦门市', '湖里区', null, '118.14174', '24.512131', '2018-01-03 20:19:03', null, '2018-01-04 19:29:56', null);
INSERT INTO `t_plot` VALUES ('6081', '绿野轩踪', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:03', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6082', '殿前外口公寓', '福建省', '厦门市', '湖里区', null, '118.120796', '24.529813', '2018-01-03 20:19:03', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6083', '南山路101号', '福建省', '厦门市', '湖里区', null, '118.110804', '24.507395', '2018-01-03 20:19:03', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6084', '南山公寓西区', '福建省', '厦门市', '湖里区', null, '118.111418', '24.507737', '2018-01-03 20:19:03', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6085', '金昌二里', '福建省', '厦门市', '湖里区', null, '118.160045', '24.503869', '2018-01-03 20:19:04', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6086', '江头工商局宿舍', '福建省', '厦门市', '湖里区', null, '118.087019', '24.491159', '2018-01-03 20:19:04', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6087', '海山路小区', '福建省', '厦门市', '湖里区', null, '118.089544', '24.493261', '2018-01-03 20:19:04', null, '2018-01-04 19:29:57', null);
INSERT INTO `t_plot` VALUES ('6088', '金湖一里', '福建省', '厦门市', '湖里区', null, '118.146077', '24.513417', '2018-01-03 20:19:04', null, '2018-01-04 19:29:58', null);
INSERT INTO `t_plot` VALUES ('6089', '东荣公寓', '福建省', '厦门市', '湖里区', null, '118.089791', '24.492275', '2018-01-03 20:19:04', null, '2018-01-04 19:29:58', null);
INSERT INTO `t_plot` VALUES ('6090', '双狮南里6-8号', '福建省', '厦门市', '湖里区', null, '118.085264', '24.490793', '2018-01-03 20:19:04', null, '2018-01-04 19:29:58', null);
INSERT INTO `t_plot` VALUES ('6091', '银岭园', '福建省', '厦门市', '湖里区', null, '118.145097', '24.50074', '2018-01-03 20:19:04', null, '2018-01-04 19:29:58', null);
INSERT INTO `t_plot` VALUES ('6092', '阳光翠海', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:29:58', null);
INSERT INTO `t_plot` VALUES ('6093', '公盈公司宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:29:58', null);
INSERT INTO `t_plot` VALUES ('6094', '怡鹭大厦', '福建省', '厦门市', '湖里区', null, '118.131551', '24.510044', '2018-01-03 20:19:04', null, '2018-01-04 19:29:59', null);
INSERT INTO `t_plot` VALUES ('6095', '总裁行馆', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:29:59', null);
INSERT INTO `t_plot` VALUES ('6096', '建发中央湾区', '福建省', '厦门市', '湖里区', null, '118.171039', '24.52235', '2018-01-03 20:19:04', null, '2018-01-04 19:29:59', null);
INSERT INTO `t_plot` VALUES ('6097', '百果山公寓', '福建省', '厦门市', '湖里区', null, '118.139736', '24.506592', '2018-01-03 20:19:04', null, '2018-01-04 19:29:59', null);
INSERT INTO `t_plot` VALUES ('6098', '鹭岛心筑', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:29:59', null);
INSERT INTO `t_plot` VALUES ('6099', '四海明楼', '福建省', '厦门市', '湖里区', null, '118.148373', '24.513491', '2018-01-03 20:19:04', null, '2018-01-04 19:29:59', null);
INSERT INTO `t_plot` VALUES ('6100', '中山公寓', '福建省', '厦门市', '湖里区', null, '118.121347', '24.520977', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6101', '厦门三中宿舍', '福建省', '厦门市', '湖里区', null, '118.12782', '24.516417', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6102', '公盈楼', '福建省', '厦门市', '湖里区', null, '118.103374', '24.509232', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6104', '长勘大厦', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6105', '湖里万达广场', '福建省', '厦门市', '湖里区', null, '118.183221', '24.511362', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6106', '永同昌温馨家园', '福建省', '厦门市', '湖里区', null, '118.149659', '24.510726', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6107', '新都国际', '福建省', '厦门市', '湖里区', null, '118.167613', '24.537628', '2018-01-03 20:19:04', null, '2018-01-04 19:30:00', null);
INSERT INTO `t_plot` VALUES ('6108', '徐厝小区', '福建省', '厦门市', '湖里区', null, '118.106676', '24.520783', '2018-01-03 20:19:04', null, '2018-01-04 19:30:01', null);
INSERT INTO `t_plot` VALUES ('6109', '双狮山港务宿舍', '福建省', '厦门市', '湖里区', null, '118.098091', '24.500918', '2018-01-03 20:19:04', null, '2018-01-04 19:30:01', null);
INSERT INTO `t_plot` VALUES ('6110', '卓晟园', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:01', null);
INSERT INTO `t_plot` VALUES ('6111', '源益工业小区', '福建省', '厦门市', '湖里区', null, '118.130609', '24.515536', '2018-01-03 20:19:04', null, '2018-01-04 19:30:01', null);
INSERT INTO `t_plot` VALUES ('6113', '省国防计量站宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:01', null);
INSERT INTO `t_plot` VALUES ('6114', '塘边邮电局宿舍', '福建省', '厦门市', '湖里区', null, '118.125124', '24.513709', '2018-01-03 20:19:04', null, '2018-01-04 19:30:01', null);
INSERT INTO `t_plot` VALUES ('6115', '世纪之星', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6116', '源生楼', '福建省', '厦门市', '湖里区', null, '118.142731', '24.513537', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6117', '国防宿舍', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6118', '双十中学小区', '福建省', '厦门市', '湖里区', null, '118.159296', '24.526541', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6119', '怡嘉园', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6120', '嘉景公寓', '福建省', '厦门市', '湖里区', null, '118.135691', '24.512954', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6121', '新永成花园', '福建省', '厦门市', '湖里区', null, '118.148265', '24.516344', '2018-01-03 20:19:04', null, '2018-01-04 19:30:02', null);
INSERT INTO `t_plot` VALUES ('6122', '明元花园', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6123', '小东山公寓', '福建省', '厦门市', '湖里区', null, '118.134268', '24.519281', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6124', '悦馨庄园', '福建省', '厦门市', '湖里区', null, '118.111719', '24.524541', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6125', '广厦宿舍', '福建省', '厦门市', '湖里区', null, '118.148239', '24.50338', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6127', '园山雅阁', '福建省', '厦门市', '湖里区', null, '118.132785', '24.515028', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6128', '中闽药检宿舍楼', '福建省', '厦门市', '湖里区', null, '118.089021', '24.490874', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6129', '依山丽景', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:03', null);
INSERT INTO `t_plot` VALUES ('6130', '广兴新村', '福建省', '厦门市', '湖里区', null, '118.119821', '24.509263', '2018-01-03 20:19:04', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6131', '渔港宿舍', '福建省', '厦门市', '湖里区', null, '118.087144', '24.490772', '2018-01-03 20:19:04', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6132', '佳园公寓', '福建省', '厦门市', '湖里区', null, '118.110746', '24.506879', '2018-01-03 20:19:04', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6133', '西城海岸', '福建省', '厦门市', '湖里区', null, '118.144676', '24.521974', '2018-01-03 20:19:04', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6134', '武警公寓', '福建省', '厦门市', '湖里区', null, '118.13107', '24.510741', '2018-01-03 20:19:04', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6135', '彼岸Patio', '福建省', '厦门市', '海沧区', null, '118.05894', '24.495467', '2018-01-03 20:30:00', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6136', '绿苑新城四组团', '福建省', '厦门市', '海沧区', null, '118.057548', '24.498332', '2018-01-03 20:30:00', null, '2018-01-04 19:30:04', null);
INSERT INTO `t_plot` VALUES ('6137', '禹洲尊海', '福建省', '厦门市', '海沧区', null, '118.038985', '24.471946', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6138', '吉祥花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6139', '都市恬园', '福建省', '厦门市', '海沧区', null, '118.043597', '24.49701', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6140', '天御花园', '福建省', '厦门市', '海沧区', null, '118.035436', '24.490804', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6141', '禹洲澜山墅', '福建省', '厦门市', '海沧区', null, '118.044927', '24.533852', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6142', '宝成海景苑', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6143', '兴港花园一期', '福建省', '厦门市', '海沧区', null, '118.032309', '24.492661', '2018-01-03 20:30:00', null, '2018-01-04 19:30:05', null);
INSERT INTO `t_plot` VALUES ('6144', '华兴花园', '福建省', '厦门市', '海沧区', null, '118.038549', '24.495365', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6145', '泰禾厦门院子', '福建省', '厦门市', '海沧区', null, '117.999658', '24.540113', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6146', '未来海岸系天成', '福建省', '厦门市', '海沧区', null, '118.042532', '24.46576', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6147', '锦辉花园', '福建省', '厦门市', '海沧区', null, '118.033467', '24.49386', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6148', '富祥苑', '福建省', '厦门市', '海沧区', null, '118.040866', '24.498233', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6149', '新阳名仕阁', '福建省', '厦门市', '海沧区', null, '118.005537', '24.526074', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6150', '壹号公馆', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:00', null, '2018-01-04 19:30:06', null);
INSERT INTO `t_plot` VALUES ('6151', '海青花园', '福建省', '厦门市', '海沧区', null, '118.040908', '24.502044', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6152', '水务宿舍', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6153', '大金门花园广场', '福建省', '厦门市', '海沧区', null, '118.037426', '24.501255', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6154', '海景奥斯卡', '福建省', '厦门市', '海沧区', null, '118.056821', '24.491567', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6155', '海沧生活区一小区', '福建省', '厦门市', '海沧区', null, '118.040348', '24.499507', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6156', '未来海岸水云湾', '福建省', '厦门市', '海沧区', null, '118.039442', '24.468651', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6157', '绿苑小区', '福建省', '厦门市', '海沧区', null, '118.051589', '24.495596', '2018-01-03 20:30:00', null, '2018-01-04 19:30:07', null);
INSERT INTO `t_plot` VALUES ('6158', '中骏蓝湾半岛', '福建省', '厦门市', '海沧区', null, '118.060774', '24.496201', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6159', '海上明珠家园', '福建省', '厦门市', '海沧区', null, '118.056849', '24.496178', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6160', '兴旺广场', '福建省', '厦门市', '海沧区', null, '118.018252', '24.528962', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6161', '金茂花园', '福建省', '厦门市', '海沧区', null, '118.038983', '24.501691', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6162', '永信花园', '福建省', '厦门市', '海沧区', null, '118.040031', '24.502703', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6163', '金海华景', '福建省', '厦门市', '海沧区', null, '118.058061', '24.49302', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6164', '泉舜滨海上城', '福建省', '厦门市', '海沧区', null, '118.059719', '24.497512', '2018-01-03 20:30:00', null, '2018-01-04 19:30:08', null);
INSERT INTO `t_plot` VALUES ('6165', '未来海岸系蓝屿', '福建省', '厦门市', '海沧区', null, '118.042532', '24.46576', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6166', '京口岩小区', '福建省', '厦门市', '海沧区', null, '118.039129', '24.458896', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6167', '海沧万科城一期', '福建省', '厦门市', '海沧区', null, '118.037097', '24.537093', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6168', '加州海岸水印象', '福建省', '厦门市', '海沧区', null, '118.050146', '24.493664', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6169', '泉舜信海花园', '福建省', '厦门市', '海沧区', null, '118.039358', '24.496515', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6170', '兴海苑', '福建省', '厦门市', '海沧区', null, '118.045197', '24.503445', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6171', '汇景雅苑', '福建省', '厦门市', '海沧区', null, '118.042639', '24.458251', '2018-01-03 20:30:00', null, '2018-01-04 19:30:09', null);
INSERT INTO `t_plot` VALUES ('6172', '富佳苑', '福建省', '厦门市', '海沧区', null, '118.044916', '24.50042', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6173', '隆晖花园', '福建省', '厦门市', '海沧区', null, '118.03805', '24.49537', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6174', '荣发楼', '福建省', '厦门市', '海沧区', null, '118.042821', '24.502066', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6175', '广场湖畔花园', '福建省', '厦门市', '海沧区', null, '118.034425', '24.493317', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6176', '海银大厦', '福建省', '厦门市', '海沧区', null, '118.039881', '24.501441', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6177', '金龙商城', '福建省', '厦门市', '海沧区', null, '118.046613', '24.505687', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6178', '海湾锦园', '福建省', '厦门市', '海沧区', null, '118.042649', '24.459111', '2018-01-03 20:30:00', null, '2018-01-04 19:30:10', null);
INSERT INTO `t_plot` VALUES ('6179', '禹洲尊海(别墅)', '福建省', '厦门市', '海沧区', null, '118.038985', '24.471946', '2018-01-03 20:30:00', null, '2018-01-04 19:30:11', null);
INSERT INTO `t_plot` VALUES ('6180', '沧二小区', '福建省', '厦门市', '海沧区', null, '118.043281', '24.501156', '2018-01-03 20:30:00', null, '2018-01-04 19:30:11', null);
INSERT INTO `t_plot` VALUES ('6181', '中骏海岸1号', '福建省', '厦门市', '海沧区', null, '118.0554', '24.490848', '2018-01-03 20:30:00', null, '2018-01-04 19:30:11', null);
INSERT INTO `t_plot` VALUES ('6182', '未来海岸蓝月湾一期南区', '福建省', '厦门市', '海沧区', null, '118.040259', '24.464105', '2018-01-03 20:30:00', null, '2018-01-04 19:30:11', null);
INSERT INTO `t_plot` VALUES ('6183', '新华园', '福建省', '厦门市', '海沧区', null, '118.040645', '24.49678', '2018-01-03 20:30:00', null, '2018-01-04 19:30:11', null);
INSERT INTO `t_plot` VALUES ('6184', '佳宏花园', '福建省', '厦门市', '海沧区', null, '117.95092', '24.574239', '2018-01-03 20:30:00', null, '2018-01-04 19:30:11', null);
INSERT INTO `t_plot` VALUES ('6185', '亚桥花园', '福建省', '厦门市', '海沧区', null, '118.041986', '24.502998', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6186', '禹洲领海', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6187', '湖景华庭', '福建省', '厦门市', '海沧区', null, '118.045728', '24.494542', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6188', '大永固大厦', '福建省', '厦门市', '海沧区', null, '118.041258', '24.502419', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6189', '泉舜星园美第', '福建省', '厦门市', '海沧区', null, '118.041251', '24.457369', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6190', '鼓浪花园', '福建省', '厦门市', '海沧区', null, '118.052264', '24.493333', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6191', '海晟维多利亚', '福建省', '厦门市', '海沧区', null, '118.062263', '24.496777', '2018-01-03 20:30:00', null, '2018-01-04 19:30:12', null);
INSERT INTO `t_plot` VALUES ('6192', '碧水芳庭', '福建省', '厦门市', '海沧区', null, '118.048509', '24.494827', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6193', '旭日海湾三期', '福建省', '厦门市', '海沧区', null, '118.053938', '24.493944', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6194', '未来海岸鹭景湾', '福建省', '厦门市', '海沧区', null, '118.044326', '24.463875', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6195', '东方高尔夫国际公寓', '福建省', '厦门市', '海沧区', null, '118.059733', '24.529493', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6196', '腾龙商厦', '福建省', '厦门市', '海沧区', null, '118.042552', '24.499256', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6197', '正元新都汇', '福建省', '厦门市', '海沧区', null, '118.038866', '24.482776', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6198', '天湖城天湖', '福建省', '厦门市', '海沧区', null, '118.036264', '24.485791', '2018-01-03 20:30:00', null, '2018-01-04 19:30:13', null);
INSERT INTO `t_plot` VALUES ('6199', '金茗花园', '福建省', '厦门市', '海沧区', null, '118.017378', '24.531589', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6200', '旭日海湾一期', '福建省', '厦门市', '海沧区', null, '118.052793', '24.494403', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6201', '海福广场2期', '福建省', '厦门市', '海沧区', null, '118.039542', '24.497987', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6202', '三源弘', '福建省', '厦门市', '海沧区', null, '118.04153', '24.458384', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6203', '华澳花园', '福建省', '厦门市', '海沧区', null, '118.033008', '24.494868', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6204', '海岸明珠', '福建省', '厦门市', '海沧区', null, '118.044146', '24.457493', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6205', '鑫茂花园', '福建省', '厦门市', '海沧区', null, '118.038952', '24.500691', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6206', '兴东鑫花园', '福建省', '厦门市', '海沧区', null, '118.052176', '24.501099', '2018-01-03 20:30:00', null, '2018-01-04 19:30:14', null);
INSERT INTO `t_plot` VALUES ('6207', '未来海岸系蓝水郡', '福建省', '厦门市', '海沧区', null, '118.033773', '24.48087', '2018-01-03 20:30:00', null, '2018-01-04 19:30:15', null);
INSERT INTO `t_plot` VALUES ('6208', '东方名园', '福建省', '厦门市', '海沧区', null, '118.041291', '24.493958', '2018-01-03 20:30:00', null, '2018-01-04 19:30:15', null);
INSERT INTO `t_plot` VALUES ('6209', '天乙广场', '福建省', '厦门市', '海沧区', null, '118.037605', '24.500266', '2018-01-03 20:30:00', null, '2018-01-04 19:30:15', null);
INSERT INTO `t_plot` VALUES ('6210', '过云溪一组团', '福建省', '厦门市', '海沧区', null, '117.951668', '24.573089', '2018-01-03 20:30:00', null, '2018-01-04 19:30:15', null);
INSERT INTO `t_plot` VALUES ('6211', '信发楼', '福建省', '厦门市', '海沧区', null, '118.040847', '24.497808', '2018-01-03 20:30:00', null, '2018-01-04 19:30:15', null);
INSERT INTO `t_plot` VALUES ('6212', '泉舜新旺府', '福建省', '厦门市', '海沧区', null, '118.038446', '24.496279', '2018-01-03 20:30:00', null, '2018-01-04 19:30:15', null);
INSERT INTO `t_plot` VALUES ('6213', '天心岛', '福建省', '厦门市', '海沧区', null, '118.054511', '24.49418', '2018-01-03 20:30:00', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6214', '绿苑海景', '福建省', '厦门市', '海沧区', null, '118.053308', '24.491722', '2018-01-03 20:30:00', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6215', '天湖城天源A区', '福建省', '厦门市', '海沧区', null, '118.03591', '24.480606', '2018-01-03 20:30:00', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6216', '汇景佳园', '福建省', '厦门市', '海沧区', null, '118.045042', '24.459766', '2018-01-03 20:30:00', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6217', '东方高尔夫别墅', '福建省', '厦门市', '海沧区', null, '118.056586', '24.527995', '2018-01-03 20:30:00', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6218', '金城湾花园', '福建省', '厦门市', '海沧区', null, '118.043008', '24.457786', '2018-01-03 20:30:00', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6219', '金桥苑', '福建省', '厦门市', '海沧区', null, '118.045501', '24.457713', '2018-01-03 20:30:01', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6220', '绿苑水岸名筑', '福建省', '厦门市', '海沧区', null, '118.036338', '24.472328', '2018-01-03 20:30:01', null, '2018-01-04 19:30:16', null);
INSERT INTO `t_plot` VALUES ('6221', '祥庆花园', '福建省', '厦门市', '海沧区', null, '118.047007', '24.497843', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6222', '翠湖名珠一期亚太广场', '福建省', '厦门市', '海沧区', null, '118.040483', '24.494248', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6223', '未来橙堡', '福建省', '厦门市', '海沧区', null, '118.032356', '24.481891', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6224', '铭仕花园', '福建省', '厦门市', '海沧区', null, '118.037914', '24.499819', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6225', '新宝成花园', '福建省', '厦门市', '海沧区', null, '118.055644', '24.495546', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6226', '未来海岸碧海湾', '福建省', '厦门市', '海沧区', null, '118.042533', '24.46805', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6227', '绿苑滨海国际公寓', '福建省', '厦门市', '海沧区', null, '118.052386', '24.495856', '2018-01-03 20:30:01', null, '2018-01-04 19:30:17', null);
INSERT INTO `t_plot` VALUES ('6228', '未来海岸系天籁', '福建省', '厦门市', '海沧区', null, '118.039337', '24.494684', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6229', '未来海岸浪琴湾南区', '福建省', '厦门市', '海沧区', null, '118.042093', '24.465802', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6230', '未来海岸系凌波', '福建省', '厦门市', '海沧区', null, '118.043956', '24.459629', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6231', '汇利花园', '福建省', '厦门市', '海沧区', null, '118.043612', '24.503691', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6232', '绿苑商城', '福建省', '厦门市', '海沧区', null, '118.052386', '24.495856', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6233', '中骏天峰', '福建省', '厦门市', '海沧区', null, '118.042736', '24.47182', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6234', '禹洲华侨金海岸', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:18', null);
INSERT INTO `t_plot` VALUES ('6235', '汇景蓝湾(别墅)', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6236', '融信海上城', '福建省', '厦门市', '海沧区', null, '118.066098', '24.501185', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6237', '置信花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6238', '禹洲高尔夫国际社区', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6239', '未来海岸蓝月湾二期', '福建省', '厦门市', '海沧区', null, '118.040259', '24.464105', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6240', '金龙商城三期', '福建省', '厦门市', '海沧区', null, '118.04752', '24.506094', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6241', '兴港花园二期', '福建省', '厦门市', '海沧区', null, '118.029741', '24.494043', '2018-01-03 20:30:01', null, '2018-01-04 19:30:19', null);
INSERT INTO `t_plot` VALUES ('6242', '翔鹭花园', '福建省', '厦门市', '海沧区', null, '118.032987', '24.498661', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6243', '金岸领寓', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6244', '海尔华玺', '福建省', '厦门市', '海沧区', null, '118.032584', '24.538494', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6245', '西雅图', '福建省', '厦门市', '海沧区', null, '118.035153', '24.499792', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6246', '贤文公寓', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6247', '万科城', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6248', '半山公寓', '福建省', '厦门市', '海沧区', null, '118.049916', '24.497912', '2018-01-03 20:30:01', null, '2018-01-04 19:30:20', null);
INSERT INTO `t_plot` VALUES ('6249', '海沧万科城三期', '福建省', '厦门市', '海沧区', null, '118.037097', '24.537093', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6250', '鑫德楼', '福建省', '厦门市', '海沧区', null, '118.041097', '24.498241', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6251', '金华楼', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6252', '绿苑新城三组团', '福建省', '厦门市', '海沧区', null, '118.056543', '24.500623', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6253', '福祥花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6254', '王子广场', '福建省', '厦门市', '海沧区', null, '118.048711', '24.494788', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6255', '金门海景山庄', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:21', null);
INSERT INTO `t_plot` VALUES ('6256', '文圃花园', '福建省', '厦门市', '海沧区', null, '118.041452', '24.49553', '2018-01-03 20:30:01', null, '2018-01-04 19:30:22', null);
INSERT INTO `t_plot` VALUES ('6257', '金龙商城二期', '福建省', '厦门市', '海沧区', null, '118.046033', '24.505948', '2018-01-03 20:30:01', null, '2018-01-04 19:30:22', null);
INSERT INTO `t_plot` VALUES ('6258', '长欣花园', '福建省', '厦门市', '海沧区', null, '118.041445', '24.499811', '2018-01-03 20:30:01', null, '2018-01-04 19:30:22', null);
INSERT INTO `t_plot` VALUES ('6259', '未来橙堡B', '福建省', '厦门市', '海沧区', null, '118.032356', '24.481891', '2018-01-03 20:30:01', null, '2018-01-04 19:30:22', null);
INSERT INTO `t_plot` VALUES ('6261', '未来海岸系滨湖花园', '福建省', '厦门市', '海沧区', null, '118.042532', '24.46576', '2018-01-03 20:30:01', null, '2018-01-04 19:30:22', null);
INSERT INTO `t_plot` VALUES ('6262', '福裕花园', '福建省', '厦门市', '海沧区', null, '118.042769', '24.498382', '2018-01-03 20:30:01', null, '2018-01-04 19:30:22', null);
INSERT INTO `t_plot` VALUES ('6263', '金屿楼', '福建省', '厦门市', '海沧区', null, '118.044961', '24.458825', '2018-01-03 20:30:01', null, '2018-01-04 19:30:23', null);
INSERT INTO `t_plot` VALUES ('6264', '沧隆楼', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:23', null);
INSERT INTO `t_plot` VALUES ('6265', '旭日海湾四期', '福建省', '厦门市', '海沧区', null, '118.052793', '24.494403', '2018-01-03 20:30:01', null, '2018-01-04 19:30:23', null);
INSERT INTO `t_plot` VALUES ('6266', '鑫银公寓', '福建省', '厦门市', '海沧区', null, '118.039958', '24.497832', '2018-01-03 20:30:01', null, '2018-01-04 19:30:23', null);
INSERT INTO `t_plot` VALUES ('6267', '融侨观邸(别墅)', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:23', null);
INSERT INTO `t_plot` VALUES ('6268', '盛元广场', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:23', null);
INSERT INTO `t_plot` VALUES ('6269', '滨海阳光', '福建省', '厦门市', '海沧区', null, '118.056978', '24.496461', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6270', '文轩花园', '福建省', '厦门市', '海沧区', null, '118.043676', '24.495771', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6271', '电厂宿舍', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6272', '加州海岸', '福建省', '厦门市', '海沧区', null, '118.050146', '24.493664', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6273', '泰地海西中心SOHO', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6274', '王将花园', '福建省', '厦门市', '海沧区', null, '118.042366', '24.50406', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6275', '天华花园', '福建省', '厦门市', '海沧区', null, '118.043658', '24.498651', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6276', '幸福港湾', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:24', null);
INSERT INTO `t_plot` VALUES ('6277', '海西西引力', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6278', '海达路166号', '福建省', '厦门市', '海沧区', null, '118.045694', '24.500083', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6279', '东屿海滨花园', '福建省', '厦门市', '海沧区', null, '118.059266', '24.495703', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6280', '龙湖春江彼岸', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6281', '过云溪二组团', '福建省', '厦门市', '海沧区', null, '117.951668', '24.573089', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6282', '裕佳苑', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6283', '未来海岸浪琴湾北区', '福建省', '厦门市', '海沧区', null, '118.042093', '24.465802', '2018-01-03 20:30:01', null, '2018-01-04 19:30:25', null);
INSERT INTO `t_plot` VALUES ('6284', '金鑫公寓', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6285', '新月湾', '福建省', '厦门市', '海沧区', null, '118.041429', '24.471605', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6286', '未来海岸蓝月湾第一期北区', '福建省', '厦门市', '海沧区', null, '118.040259', '24.464105', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6287', '海投第一湾(别墅)', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6288', '嵩屿电厂宿舍小区', '福建省', '厦门市', '海沧区', null, '118.050562', '24.457574', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6289', '绿苑新城二组团', '福建省', '厦门市', '海沧区', null, '118.055914', '24.497971', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6290', '海投第一湾', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:26', null);
INSERT INTO `t_plot` VALUES ('6291', '海投青春海岸', '福建省', '厦门市', '海沧区', null, '117.996991', '24.537615', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6292', '禹洲澜山墅(别墅)', '福建省', '厦门市', '海沧区', null, '118.044927', '24.533852', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6293', '海西轻公寓', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6294', '绿苑新城一组团', '福建省', '厦门市', '海沧区', null, '118.055914', '24.497971', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6295', '海投尚书房', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6296', '瑞兴楼', '福建省', '厦门市', '海沧区', null, '118.040042', '24.502328', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6297', '星河花园', '福建省', '厦门市', '海沧区', null, '118.043974', '24.494675', '2018-01-03 20:30:01', null, '2018-01-04 19:30:27', null);
INSERT INTO `t_plot` VALUES ('6298', '京口岩居住小区一期', '福建省', '厦门市', '海沧区', null, '118.039857', '24.457395', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6299', '银河花园', '福建省', '厦门市', '海沧区', null, '118.043934', '24.494354', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6300', '万科城二期', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6301', '海投自贸城', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6302', '汇景蓝湾', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6303', '天竺花园', '福建省', '厦门市', '海沧区', null, '117.947032', '24.573582', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6304', '未来海岸浪琴湾(别墅)', '福建省', '厦门市', '海沧区', null, '118.042093', '24.465802', '2018-01-03 20:30:01', null, '2018-01-04 19:30:28', null);
INSERT INTO `t_plot` VALUES ('6305', '庙兜102号小区', '福建省', '厦门市', '海沧区', null, '118.059813', '24.528084', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6306', '泉舜信宇花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6307', '融侨观邸', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6308', '海新阳光公寓二期', '福建省', '厦门市', '海沧区', null, '117.996842', '24.535456', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6309', '旭日海湾二期', '福建省', '厦门市', '海沧区', null, '118.05275', '24.494445', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6310', '天湖城天源B区', '福建省', '厦门市', '海沧区', null, '118.03591', '24.480606', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6311', '金海苑', '福建省', '厦门市', '海沧区', null, '118.056391', '24.49654', '2018-01-03 20:30:01', null, '2018-01-04 19:30:29', null);
INSERT INTO `t_plot` VALUES ('6312', '宏大花园', '福建省', '厦门市', '海沧区', null, '118.043965', '24.45863', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6313', '福宇公寓', '福建省', '厦门市', '海沧区', null, '118.048803', '24.500258', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6314', '泰禾首玺', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6315', '海福广场1期', '福建省', '厦门市', '海沧区', null, '118.038454', '24.498676', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6316', '沧龙花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6317', '海新阳光公寓', '福建省', '厦门市', '海沧区', null, '117.996842', '24.535456', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6318', '佳福花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:30', null);
INSERT INTO `t_plot` VALUES ('6320', '佳鑫花园', '福建省', '厦门市', '海沧区', null, '117.944341', '24.572408', '2018-01-03 20:30:01', null, '2018-01-04 19:30:31', null);
INSERT INTO `t_plot` VALUES ('6321', '正顺花园', '福建省', '厦门市', '海沧区', null, '118.002397', '24.529896', '2018-01-03 20:30:01', null, '2018-01-04 19:30:31', null);
INSERT INTO `t_plot` VALUES ('6322', '五华小米公寓', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:31', null);
INSERT INTO `t_plot` VALUES ('6323', '鑫荣苑', '福建省', '厦门市', '海沧区', null, '118.040274', '24.494725', '2018-01-03 20:30:01', null, '2018-01-04 19:30:31', null);
INSERT INTO `t_plot` VALUES ('6324', '海景城公寓', '福建省', '厦门市', '海沧区', null, '118.043547', '24.45958', '2018-01-03 20:30:01', null, '2018-01-04 19:30:31', null);
INSERT INTO `t_plot` VALUES ('6325', '银都花园', '福建省', '厦门市', '海沧区', null, '118.042341', '24.496882', '2018-01-03 20:30:01', null, '2018-01-04 19:30:31', null);
INSERT INTO `t_plot` VALUES ('6326', '金玉花园', '福建省', '厦门市', '海沧区', null, '117.983956', '24.53619', '2018-01-03 20:30:01', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6327', '中骏天峰(别墅)', '福建省', '厦门市', '海沧区', null, '118.042736', '24.47182', '2018-01-03 20:30:01', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6328', '未来海岸系云樽', '福建省', '厦门市', '海沧区', null, '118.043547', '24.457623', '2018-01-03 20:30:01', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6329', '金龙商城一期', '福建省', '厦门市', '海沧区', null, '118.046454', '24.505675', '2018-01-03 20:30:01', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6330', '祥和雅筑二期', '福建省', '厦门市', '集美区', null, '118.108406', '24.612107', '2018-01-03 20:38:00', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6331', '万科云城', '福建省', '厦门市', '集美区', null, '118.085857', '24.611123', '2018-01-03 20:38:00', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6332', '联发杏林湾一号', '福建省', '厦门市', '集美区', null, '118.07177', '24.565521', '2018-01-03 20:38:00', null, '2018-01-04 19:30:32', null);
INSERT INTO `t_plot` VALUES ('6333', '水晶湖郡一期(别墅)', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:00', null, '2018-01-04 19:30:33', null);
INSERT INTO `t_plot` VALUES ('6334', '宁宝小区', '福建省', '厦门市', '集美区', null, '118.059254', '24.568927', '2018-01-03 20:38:00', null, '2018-01-04 19:30:33', null);
INSERT INTO `t_plot` VALUES ('6335', '杏福园', '福建省', '厦门市', '集美区', null, '118.058463', '24.571393', '2018-01-03 20:38:00', null, '2018-01-04 19:30:33', null);
INSERT INTO `t_plot` VALUES ('6336', '聚鑫广场', '福建省', '厦门市', '集美区', null, '118.035047', '24.571876', '2018-01-03 20:38:00', null, '2018-01-04 19:30:33', null);
INSERT INTO `t_plot` VALUES ('6337', '高迪墅', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:33', null);
INSERT INTO `t_plot` VALUES ('6338', '高铁阳光花园', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:33', null);
INSERT INTO `t_plot` VALUES ('6339', '广兴新城', '福建省', '厦门市', '集美区', null, '118.034977', '24.573836', '2018-01-03 20:38:00', null, '2018-01-04 19:30:34', null);
INSERT INTO `t_plot` VALUES ('6340', '禹洲中央海岸二期', '福建省', '厦门市', '集美区', null, '118.072153', '24.568359', '2018-01-03 20:38:00', null, '2018-01-04 19:30:34', null);
INSERT INTO `t_plot` VALUES ('6341', '园博1号', '福建省', '厦门市', '集美区', null, '118.070891', '24.582649', '2018-01-03 20:38:00', null, '2018-01-04 19:30:34', null);
INSERT INTO `t_plot` VALUES ('6342', '雍景金銮湾', '福建省', '厦门市', '集美区', null, '118.050469', '24.558946', '2018-01-03 20:38:00', null, '2018-01-04 19:30:34', null);
INSERT INTO `t_plot` VALUES ('6343', '世茂璀璨天城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:34', null);
INSERT INTO `t_plot` VALUES ('6344', '联发杏林湾一号(别墅)', '福建省', '厦门市', '集美区', null, '118.07177', '24.565521', '2018-01-03 20:38:00', null, '2018-01-04 19:30:34', null);
INSERT INTO `t_plot` VALUES ('6345', '中交和美新城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6346', '海上五月花(别墅)', '福建省', '厦门市', '集美区', null, '118.131126', '24.610216', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6347', '禹洲中央海岸三期', '福建省', '厦门市', '集美区', null, '118.072153', '24.568359', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6348', '中骏四季阳光', '福建省', '厦门市', '集美区', null, '118.070289', '24.645062', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6349', '中海锦城国际', '福建省', '厦门市', '集美区', null, '118.034558', '24.589202', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6350', '海上五月花一期', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6351', 'IOI园博湾', '福建省', '厦门市', '集美区', null, '118.060748', '24.593022', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6352', '凤麟花园', '福建省', '厦门市', '集美区', null, '118.108546', '24.59026', '2018-01-03 20:38:00', null, '2018-01-04 19:30:35', null);
INSERT INTO `t_plot` VALUES ('6353', '万科金域华府一期', '福建省', '厦门市', '集美区', null, '118.072402', '24.569965', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6354', '乐凯家园', '福建省', '厦门市', '集美区', null, '117.994474', '24.60481', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6355', '禹洲中央海岸一期', '福建省', '厦门市', '集美区', null, '118.072153', '24.568359', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6356', '大学湾', '福建省', '厦门市', '集美区', null, '118.114318', '24.591257', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6357', '杏北新城锦园居住区', '福建省', '厦门市', '集美区', null, '118.039859', '24.583795', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6358', '新景舜弘天籁(别墅)', '福建省', '厦门市', '集美区', null, '118.124196', '24.600152', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6359', '水晶湖郡二期(别墅)', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:00', null, '2018-01-04 19:30:36', null);
INSERT INTO `t_plot` VALUES ('6360', '中海寰宇天下(别墅)', '福建省', '厦门市', '集美区', null, '118.066506', '24.567134', '2018-01-03 20:38:00', null, '2018-01-04 19:30:37', null);
INSERT INTO `t_plot` VALUES ('6361', '盛光社区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:37', null);
INSERT INTO `t_plot` VALUES ('6362', '天鹅美苑', '福建省', '厦门市', '集美区', null, '118.116184', '24.589793', '2018-01-03 20:38:00', null, '2018-01-04 19:30:37', null);
INSERT INTO `t_plot` VALUES ('6363', '中铁海湾豪园', '福建省', '厦门市', '集美区', null, '118.04579', '24.564275', '2018-01-03 20:38:00', null, '2018-01-04 19:30:37', null);
INSERT INTO `t_plot` VALUES ('6364', '龙湖春江郦城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:37', null);
INSERT INTO `t_plot` VALUES ('6365', '水晶湖郡三期', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:00', null, '2018-01-04 19:30:37', null);
INSERT INTO `t_plot` VALUES ('6366', '乐活小镇', '福建省', '厦门市', '集美区', null, '117.990357', '24.605557', '2018-01-03 20:38:00', null, '2018-01-04 19:30:38', null);
INSERT INTO `t_plot` VALUES ('6367', '福南小区', '福建省', '厦门市', '集美区', null, '118.102692', '24.573939', '2018-01-03 20:38:00', null, '2018-01-04 19:30:38', null);
INSERT INTO `t_plot` VALUES ('6369', '日东电厂生活小区', '福建省', '厦门市', '集美区', null, '118.041523', '24.570336', '2018-01-03 20:38:00', null, '2018-01-04 19:30:38', null);
INSERT INTO `t_plot` VALUES ('6370', '杏林国土局宿舍', '福建省', '厦门市', '集美区', null, '118.047365', '24.569791', '2018-01-03 20:38:00', null, '2018-01-04 19:30:38', null);
INSERT INTO `t_plot` VALUES ('6371', '星海湾', '福建省', '厦门市', '集美区', null, '118.129419', '24.619432', '2018-01-03 20:38:00', null, '2018-01-04 19:30:38', null);
INSERT INTO `t_plot` VALUES ('6372', '悦海园南区', '福建省', '厦门市', '集美区', null, '118.112957', '24.580531', '2018-01-03 20:38:00', null, '2018-01-04 19:30:38', null);
INSERT INTO `t_plot` VALUES ('6373', '莲花国际', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6374', '凤凰花城', '福建省', '厦门市', '集美区', null, '118.025811', '24.592254', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6375', '金海湾', '福建省', '厦门市', '集美区', null, '118.064771', '24.569226', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6376', '中航城国际社区C区北区', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6377', '国贸天悦', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6378', '荣景花园', '福建省', '厦门市', '集美区', null, '118.104046', '24.57498', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6379', '日东电厂小区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:39', null);
INSERT INTO `t_plot` VALUES ('6380', '广宇公寓', '福建省', '厦门市', '集美区', null, '118.04042', '24.564938', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6381', '建昌商业广场', '福建省', '厦门市', '集美区', null, '118.049401', '24.587258', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6382', '招商海德公园一期(别墅)', '福建省', '厦门市', '集美区', null, '118.126441', '24.607391', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6383', '集美中心花园', '福建省', '厦门市', '集美区', null, '118.109998', '24.584844', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6384', '昌城月美湾', '福建省', '厦门市', '集美区', null, '118.050507', '24.563757', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6385', '东方牡丹园', '福建省', '厦门市', '集美区', null, '118.049119', '24.570742', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6386', '招商海德公园一期', '福建省', '厦门市', '集美区', null, '118.126441', '24.607391', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6387', '嘉源新城', '福建省', '厦门市', '集美区', null, '118.047747', '24.56421', '2018-01-03 20:38:00', null, '2018-01-04 19:30:40', null);
INSERT INTO `t_plot` VALUES ('6388', '新景舜弘天籁', '福建省', '厦门市', '集美区', null, '118.124196', '24.600152', '2018-01-03 20:38:00', null, '2018-01-04 19:30:41', null);
INSERT INTO `t_plot` VALUES ('6389', '龙湖嘉屿城', '福建省', '厦门市', '集美区', null, '118.010339', '24.624708', '2018-01-03 20:38:00', null, '2018-01-04 19:30:41', null);
INSERT INTO `t_plot` VALUES ('6390', '泉水湾二期', '福建省', '厦门市', '集美区', null, '118.117098', '24.592449', '2018-01-03 20:38:00', null, '2018-01-04 19:30:41', null);
INSERT INTO `t_plot` VALUES ('6391', '国建东海岸东区', '福建省', '厦门市', '集美区', null, '118.114585', '24.580388', '2018-01-03 20:38:00', null, '2018-01-04 19:30:41', null);
INSERT INTO `t_plot` VALUES ('6392', '古龙明珠', '福建省', '厦门市', '集美区', null, '118.1128', '24.587624', '2018-01-03 20:38:00', null, '2018-01-04 19:30:41', null);
INSERT INTO `t_plot` VALUES ('6393', '聚镇', '福建省', '厦门市', '集美区', null, '118.03494', '24.585596', '2018-01-03 20:38:00', null, '2018-01-04 19:30:41', null);
INSERT INTO `t_plot` VALUES ('6395', '集美学府SOHO', '福建省', '厦门市', '集美区', null, '118.106701', '24.580247', '2018-01-03 20:38:00', null, '2018-01-04 19:30:42', null);
INSERT INTO `t_plot` VALUES ('6396', '莲花新城三期', '福建省', '厦门市', '集美区', null, '118.065508', '24.607692', '2018-01-03 20:38:00', null, '2018-01-04 19:30:42', null);
INSERT INTO `t_plot` VALUES ('6397', '新城际广场', '福建省', '厦门市', '集美区', null, '118.072562', '24.645168', '2018-01-03 20:38:00', null, '2018-01-04 19:30:42', null);
INSERT INTO `t_plot` VALUES ('6398', '永祥花园', '福建省', '厦门市', '集美区', null, '118.053112', '24.631931', '2018-01-03 20:38:00', null, '2018-01-04 19:30:42', null);
INSERT INTO `t_plot` VALUES ('6399', '四季芳园', '福建省', '厦门市', '集美区', null, '118.107297', '24.579029', '2018-01-03 20:38:00', null, '2018-01-04 19:30:42', null);
INSERT INTO `t_plot` VALUES ('6400', '马銮公寓', '福建省', '厦门市', '集美区', null, '118.036946', '24.564984', '2018-01-03 20:38:00', null, '2018-01-04 19:30:42', null);
INSERT INTO `t_plot` VALUES ('6401', '住宅莲花尚院', '福建省', '厦门市', '集美区', null, '118.088361', '24.613903', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6402', '杏花苑', '福建省', '厦门市', '集美区', null, '118.050873', '24.568703', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6403', '丰融尚城', '福建省', '厦门市', '集美区', null, '118.115186', '24.59265', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6404', '荣坪新村', '福建省', '厦门市', '集美区', null, '118.036076', '24.569025', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6405', '鑫锦家园一期', '福建省', '厦门市', '集美区', null, '118.031916', '24.601269', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6406', '中海寰宇天下', '福建省', '厦门市', '集美区', null, '118.066506', '24.567134', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6407', '夏商新纪元', '福建省', '厦门市', '集美区', null, '118.029016', '24.591352', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6408', '中航城国际社区A区(A03地块)', '福建省', '厦门市', '集美区', null, '118.064302', '24.590208', '2018-01-03 20:38:00', null, '2018-01-04 19:30:43', null);
INSERT INTO `t_plot` VALUES ('6409', '中铁海湾华庭', '福建省', '厦门市', '集美区', null, '118.060058', '24.567922', '2018-01-03 20:38:00', null, '2018-01-04 19:30:44', null);
INSERT INTO `t_plot` VALUES ('6410', '夏商大学康城三期', '福建省', '厦门市', '集美区', null, '118.029855', '24.599315', '2018-01-03 20:38:00', null, '2018-01-04 19:30:44', null);
INSERT INTO `t_plot` VALUES ('6411', '中航城国际社区', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:00', null, '2018-01-04 19:30:44', null);
INSERT INTO `t_plot` VALUES ('6412', '万科金域华府二期', '福建省', '厦门市', '集美区', null, '118.069613', '24.57001', '2018-01-03 20:38:00', null, '2018-01-04 19:30:44', null);
INSERT INTO `t_plot` VALUES ('6413', '祥发荣寓杰座', '福建省', '厦门市', '集美区', null, '118.050474', '24.570865', '2018-01-03 20:38:00', null, '2018-01-04 19:30:44', null);
INSERT INTO `t_plot` VALUES ('6414', '莲花国际(商住楼)', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:44', null);
INSERT INTO `t_plot` VALUES ('6415', '红树康桥', '福建省', '厦门市', '集美区', null, '118.112783', '24.584878', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6416', '万科广场', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6417', '中航城国际社区C区(别墅)', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6418', '福信商城小区', '福建省', '厦门市', '集美区', null, '118.107948', '24.579667', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6419', '橡树湾', '福建省', '厦门市', '集美区', null, '118.052967', '24.586101', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6420', '万龙商业城', '福建省', '厦门市', '集美区', null, '118.050215', '24.570902', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6421', '中航城国际社区A区(A01地块)', '福建省', '厦门市', '集美区', null, '118.064302', '24.590208', '2018-01-03 20:38:00', null, '2018-01-04 19:30:45', null);
INSERT INTO `t_plot` VALUES ('6422', '天润新城', '福建省', '厦门市', '集美区', null, '118.09617', '24.621922', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6423', '颐海嘉园', '福建省', '厦门市', '集美区', null, '118.114527', '24.596252', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6424', '滨海花园', '福建省', '厦门市', '集美区', null, '118.027408', '24.563603', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6425', '泉水湾一期', '福建省', '厦门市', '集美区', null, '118.116205', '24.593668', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6426', '古龙御景一期', '福建省', '厦门市', '集美区', null, '118.124356', '24.590806', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6427', '海韵华庭(二期)', '福建省', '厦门市', '集美区', null, '118.061717', '24.569331', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6428', '泉水湾三期', '福建省', '厦门市', '集美区', null, '118.117098', '24.592449', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6429', '宁宝花园', '福建省', '厦门市', '集美区', null, '118.064042', '24.572033', '2018-01-03 20:38:00', null, '2018-01-04 19:30:46', null);
INSERT INTO `t_plot` VALUES ('6430', '鹭岛北海湾', '福建省', '厦门市', '集美区', null, '118.114175', '24.580754', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6431', '中交和美新城(别墅)', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6432', '招商莱顿小镇', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6433', '东华苑', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6434', '海上五月花海明薇', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6435', '招商海德公园二期', '福建省', '厦门市', '集美区', null, '118.126441', '24.607391', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6436', '中航城国际社区A区(别墅)', '福建省', '厦门市', '集美区', null, '118.064302', '24.590208', '2018-01-03 20:38:00', null, '2018-01-04 19:30:47', null);
INSERT INTO `t_plot` VALUES ('6437', '铁山花园', '福建省', '厦门市', '集美区', null, '118.007335', '24.601109', '2018-01-03 20:38:00', null, '2018-01-04 19:30:48', null);
INSERT INTO `t_plot` VALUES ('6438', '联发欣悦湾', '福建省', '厦门市', '集美区', null, '118.01298', '24.620905', '2018-01-03 20:38:00', null, '2018-01-04 19:30:48', null);
INSERT INTO `t_plot` VALUES ('6439', '金博水岸', '福建省', '厦门市', '集美区', null, '118.057231', '24.564001', '2018-01-03 20:38:00', null, '2018-01-04 19:30:48', null);
INSERT INTO `t_plot` VALUES ('6441', '幸福家园', '福建省', '厦门市', '集美区', null, '118.011029', '24.607075', '2018-01-03 20:38:00', null, '2018-01-04 19:30:48', null);
INSERT INTO `t_plot` VALUES ('6442', '杏糖生活片区', '福建省', '厦门市', '集美区', null, '118.048291', '24.577498', '2018-01-03 20:38:00', null, '2018-01-04 19:30:48', null);
INSERT INTO `t_plot` VALUES ('6443', '集美北部新城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6444', '水晶湖郡二期', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6445', '乐安新村', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6446', '恒美园', '福建省', '厦门市', '集美区', null, '118.049591', '24.575396', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6447', '宁宝世家', '福建省', '厦门市', '集美区', null, '118.062796', '24.574291', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6448', '中航城国际社区(别墅)', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6449', '海韵华庭', '福建省', '厦门市', '集美区', null, '118.061717', '24.569331', '2018-01-03 20:38:00', null, '2018-01-04 19:30:49', null);
INSERT INTO `t_plot` VALUES ('6450', '古龙明珠(别墅)', '福建省', '厦门市', '集美区', null, '118.1128', '24.587624', '2018-01-03 20:38:00', null, '2018-01-04 19:30:50', null);
INSERT INTO `t_plot` VALUES ('6451', '华铃花园', '福建省', '厦门市', '集美区', null, '118.053352', '24.575778', '2018-01-03 20:38:00', null, '2018-01-04 19:30:50', null);
INSERT INTO `t_plot` VALUES ('6452', '水晶湖郡四期', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:00', null, '2018-01-04 19:30:50', null);
INSERT INTO `t_plot` VALUES ('6453', '海上五月花海蔚蓝', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:50', null);
INSERT INTO `t_plot` VALUES ('6455', '杏南路31号小区', '福建省', '厦门市', '集美区', null, '118.047759', '24.569937', '2018-01-03 20:38:01', null, '2018-01-04 19:30:50', null);
INSERT INTO `t_plot` VALUES ('6456', '东华路1-8号小区', '福建省', '厦门市', '集美区', null, '118.059032', '24.569144', '2018-01-03 20:38:01', null, '2018-01-04 19:30:50', null);
INSERT INTO `t_plot` VALUES ('6457', '中闽鹭江花园', '福建省', '厦门市', '集美区', null, '118.035777', '24.568163', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6458', '万佳公寓', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6459', '中海锦城国际二期', '福建省', '厦门市', '集美区', null, '118.034558', '24.589202', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6460', '宁安里', '福建省', '厦门市', '集美区', null, '118.062698', '24.57032', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6461', '联发杏林湾一号二期', '福建省', '厦门市', '集美区', null, '118.07177', '24.565521', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6462', '岑西安置房', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6463', '海上五月花海贝', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:51', null);
INSERT INTO `t_plot` VALUES ('6464', '兑山里小区', '福建省', '厦门市', '集美区', null, '118.102035', '24.616099', '2018-01-03 20:38:01', null, '2018-01-04 19:30:52', null);
INSERT INTO `t_plot` VALUES ('6465', '万科广场(商住楼)', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:52', null);
INSERT INTO `t_plot` VALUES ('6466', '尚南小院', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:52', null);
INSERT INTO `t_plot` VALUES ('6467', '古龙御景二期', '福建省', '厦门市', '集美区', null, '118.124356', '24.590806', '2018-01-03 20:38:01', null, '2018-01-04 19:30:52', null);
INSERT INTO `t_plot` VALUES ('6468', '集美平阳里小区', '福建省', '厦门市', '集美区', null, '118.098415', '24.622473', '2018-01-03 20:38:01', null, '2018-01-04 19:30:52', null);
INSERT INTO `t_plot` VALUES ('6469', '莲花尚院2', '福建省', '厦门市', '集美区', null, '118.088361', '24.613903', '2018-01-03 20:38:01', null, '2018-01-04 19:30:52', null);
INSERT INTO `t_plot` VALUES ('6470', '海景明珠(别墅)', '福建省', '厦门市', '集美区', null, '118.109867', '24.582406', '2018-01-03 20:38:01', null, '2018-01-04 19:30:53', null);
INSERT INTO `t_plot` VALUES ('6471', '中航城国际社区A区(B01地块)', '福建省', '厦门市', '集美区', null, '118.064302', '24.590208', '2018-01-03 20:38:01', null, '2018-01-04 19:30:53', null);
INSERT INTO `t_plot` VALUES ('6472', '比华丽海湾别墅', '福建省', '厦门市', '集美区', null, '118.115699', '24.58145', '2018-01-03 20:38:01', null, '2018-01-04 19:30:53', null);
INSERT INTO `t_plot` VALUES ('6473', '园博湾上', '福建省', '厦门市', '集美区', null, '118.071223', '24.601369', '2018-01-03 20:38:01', null, '2018-01-04 19:30:53', null);
INSERT INTO `t_plot` VALUES ('6475', '碧海蓝天东区', '福建省', '厦门市', '集美区', null, '118.114485', '24.584573', '2018-01-03 20:38:01', null, '2018-01-04 19:30:53', null);
INSERT INTO `t_plot` VALUES ('6476', '中航城国际社区A区(A02地块)', '福建省', '厦门市', '集美区', null, '118.064302', '24.590208', '2018-01-03 20:38:01', null, '2018-01-04 19:30:53', null);
INSERT INTO `t_plot` VALUES ('6477', '悦美筼筜', '福建省', '厦门市', '集美区', null, '118.053027', '24.565213', '2018-01-03 20:38:01', null, '2018-01-04 19:30:54', null);
INSERT INTO `t_plot` VALUES ('6478', '中央公园城', '福建省', '厦门市', '集美区', null, '118.12222', '24.604215', '2018-01-03 20:38:01', null, '2018-01-04 19:30:54', null);
INSERT INTO `t_plot` VALUES ('6479', '龙湖嘉誉', '福建省', '厦门市', '集美区', null, '118.010339', '24.624708', '2018-01-03 20:38:01', null, '2018-01-04 19:30:54', null);
INSERT INTO `t_plot` VALUES ('6480', '中航城国际社区C区南区', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:01', null, '2018-01-04 19:30:54', null);
INSERT INTO `t_plot` VALUES ('6481', '禹洲中央海岸二期(商住楼)', '福建省', '厦门市', '集美区', null, '118.072153', '24.568359', '2018-01-03 20:38:01', null, '2018-01-04 19:30:54', null);
INSERT INTO `t_plot` VALUES ('6482', '龙湖嘉屿城(别墅)', '福建省', '厦门市', '集美区', null, '118.010339', '24.624708', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6483', '恒大帝景', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6484', '水晶湖郡一期', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6485', '纺织路社区', '福建省', '厦门市', '集美区', null, '118.053983', '24.573421', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6486', '兴阁里', '福建省', '厦门市', '集美区', null, '118.051821', '24.570964', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6487', '莲花新城湖园', '福建省', '厦门市', '集美区', null, '118.065508', '24.607692', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6488', '集安广场', '福建省', '厦门市', '集美区', null, '118.123451', '24.610785', '2018-01-03 20:38:01', null, '2018-01-04 19:30:55', null);
INSERT INTO `t_plot` VALUES ('6489', '建昌花园', '福建省', '厦门市', '集美区', null, '118.04849', '24.575912', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6490', '龙湖新壹城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6491', '文华苑', '福建省', '厦门市', '集美区', null, '118.058115', '24.569997', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6492', 'IOI棕榈城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6493', '双桥明珠', '福建省', '厦门市', '集美区', null, '117.997077', '24.6108', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6494', '海上五月花别墅', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6495', '龙湖春江郦城二期', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:56', null);
INSERT INTO `t_plot` VALUES ('6496', '祥和雅筑一期', '福建省', '厦门市', '集美区', null, '118.108625', '24.610132', '2018-01-03 20:38:01', null, '2018-01-04 19:30:57', null);
INSERT INTO `t_plot` VALUES ('6497', '锦录公寓', '福建省', '厦门市', '集美区', null, '118.060826', '24.572911', '2018-01-03 20:38:01', null, '2018-01-04 19:30:57', null);
INSERT INTO `t_plot` VALUES ('6498', '白泉集贸市场小区', '福建省', '厦门市', '集美区', null, '118.049897', '24.568986', '2018-01-03 20:38:01', null, '2018-01-04 19:30:57', null);
INSERT INTO `t_plot` VALUES ('6499', '中航城国际社区A区(B02地块)', '福建省', '厦门市', '集美区', null, '118.064302', '24.590208', '2018-01-03 20:38:01', null, '2018-01-04 19:30:57', null);
INSERT INTO `t_plot` VALUES ('6500', '古龙御景二期(别墅)', '福建省', '厦门市', '集美区', null, '118.124356', '24.590806', '2018-01-03 20:38:01', null, '2018-01-04 19:30:57', null);
INSERT INTO `t_plot` VALUES ('6501', '集美外口公寓', '福建省', '厦门市', '集美区', null, '118.03738', '24.57145', '2018-01-03 20:38:01', null, '2018-01-04 19:30:57', null);
INSERT INTO `t_plot` VALUES ('6502', '海湾明珠', '福建省', '厦门市', '集美区', null, '118.047633', '24.563093', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6503', '永祥新城', '福建省', '厦门市', '集美区', null, '118.053108', '24.631527', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6504', '国贸商城集悦', '福建省', '厦门市', '集美区', null, '118.113649', '24.595385', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6505', '中航城国际社区C区东区', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6506', '印斗路小区', '福建省', '厦门市', '集美区', null, '118.111898', '24.586618', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6507', '夏商新纪元二期', '福建省', '厦门市', '集美区', null, '118.029016', '24.591352', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6508', '水晶湖郡三期(别墅)', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:01', null, '2018-01-04 19:30:58', null);
INSERT INTO `t_plot` VALUES ('6509', '银杏花园', '福建省', '厦门市', '集美区', null, '118.048901', '24.575122', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6510', '夏商大学康城一期', '福建省', '厦门市', '集美区', null, '118.029855', '24.599315', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6511', '莲花新城臻园', '福建省', '厦门市', '集美区', null, '118.065818', '24.608737', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6512', '鑫锦家园二期', '福建省', '厦门市', '集美区', null, '118.031916', '24.601269', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6513', '石鼓路100号小区', '福建省', '厦门市', '集美区', null, '118.106278', '24.57942', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6514', '丽海清庭', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6515', '华雅花园', '福建省', '厦门市', '集美区', null, '118.028118', '24.600146', '2018-01-03 20:38:01', null, '2018-01-04 19:30:59', null);
INSERT INTO `t_plot` VALUES ('6516', '乐安花园', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6517', '日东路职工住宅楼', '福建省', '厦门市', '集美区', null, '118.045881', '24.568479', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6518', '中航城国际社区C区西区', '福建省', '厦门市', '集美区', null, '118.067191', '24.587647', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6519', '华创公寓', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6520', '龙湖嘉誉（别墅）', '福建省', '厦门市', '集美区', null, '118.010339', '24.624708', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6521', '鑫锦家园三期', '福建省', '厦门市', '集美区', null, '118.031916', '24.601269', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6522', '国贸商城同悦', '福建省', '厦门市', '集美区', null, '118.111073', '24.597601', '2018-01-03 20:38:01', null, '2018-01-04 19:31:00', null);
INSERT INTO `t_plot` VALUES ('6523', '灌一新城', '福建省', '厦门市', '集美区', null, '118.006264', '24.608039', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6524', '永兴楼', '福建省', '厦门市', '集美区', null, '117.99716', '24.61349', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6525', '纺织路1-11号小区', '福建省', '厦门市', '集美区', null, '118.053602', '24.570723', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6526', '联发欣悦湾(别墅)', '福建省', '厦门市', '集美区', null, '118.01298', '24.620905', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6527', '怡家园', '福建省', '厦门市', '集美区', null, '118.037006', '24.565784', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6528', '庄园新城', '福建省', '厦门市', '集美区', null, '117.992072', '24.604881', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6529', '安仁里小区', '福建省', '厦门市', '集美区', null, '118.011935', '24.607025', '2018-01-03 20:38:01', null, '2018-01-04 19:31:01', null);
INSERT INTO `t_plot` VALUES ('6530', '祥业小区', '福建省', '厦门市', '集美区', null, '118.053031', '24.569976', '2018-01-03 20:38:01', null, '2018-01-04 19:31:02', null);
INSERT INTO `t_plot` VALUES ('6531', '文华路1-9号小区', '福建省', '厦门市', '集美区', null, '118.057225', '24.569716', '2018-01-03 20:38:01', null, '2018-01-04 19:31:02', null);
INSERT INTO `t_plot` VALUES ('6532', '银铃大厦', '福建省', '厦门市', '集美区', null, '118.047614', '24.5631', '2018-01-03 20:38:01', null, '2018-01-04 19:31:02', null);
INSERT INTO `t_plot` VALUES ('6533', '塘荣公寓', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:02', null);
INSERT INTO `t_plot` VALUES ('6534', '夏商大学康城', '福建省', '厦门市', '集美区', null, '118.029855', '24.599315', '2018-01-03 20:38:01', null, '2018-01-04 19:31:02', null);
INSERT INTO `t_plot` VALUES ('6535', '悦海园北区', '福建省', '厦门市', '集美区', null, '118.113028', '24.58088', '2018-01-03 20:38:01', null, '2018-01-04 19:31:02', null);
INSERT INTO `t_plot` VALUES ('6536', '广科新城', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6537', '文达路6号小区', '福建省', '厦门市', '集美区', null, '118.06478', '24.570926', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6538', '集美中心花园四期', '福建省', '厦门市', '集美区', null, '118.109998', '24.584844', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6539', '福华苑', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6540', '集源社区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6541', '昌顺公寓', '福建省', '厦门市', '集美区', null, '118.058384', '24.571651', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6542', '职工宿舍楼', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6543', '香港公馆', '福建省', '厦门市', '集美区', null, '118.053563', '24.568942', '2018-01-03 20:38:01', null, '2018-01-04 19:31:03', null);
INSERT INTO `t_plot` VALUES ('6544', '祥和苑', '福建省', '厦门市', '集美区', null, '118.053478', '24.572391', '2018-01-03 20:38:01', null, '2018-01-04 19:31:04', null);
INSERT INTO `t_plot` VALUES ('6545', '颐海嘉园(别墅)', '福建省', '厦门市', '集美区', null, '118.114527', '24.596252', '2018-01-03 20:38:01', null, '2018-01-04 19:31:04', null);
INSERT INTO `t_plot` VALUES ('6546', '建昌大厦', '福建省', '厦门市', '集美区', null, '118.048452', '24.570021', '2018-01-03 20:38:01', null, '2018-01-04 19:31:04', null);
INSERT INTO `t_plot` VALUES ('6547', '杏林西路4号小区', '福建省', '厦门市', '集美区', null, '118.036395', '24.570808', '2018-01-03 20:38:01', null, '2018-01-04 19:31:04', null);
INSERT INTO `t_plot` VALUES ('6548', '集美北区配套商品房', '福建省', '厦门市', '集美区', null, '118.108676', '24.609096', '2018-01-03 20:38:01', null, '2018-01-04 19:31:04', null);
INSERT INTO `t_plot` VALUES ('6549', '集美大唐别墅', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6550', '国建东海岸西区', '福建省', '厦门市', '集美区', null, '118.114585', '24.580388', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6551', '集美雅苑', '福建省', '厦门市', '集美区', null, '118.112862', '24.602486', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6552', '集友公寓', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6553', '集宏大厦', '福建省', '厦门市', '集美区', null, '118.106203', '24.580467', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6554', '日东明珠', '福建省', '厦门市', '集美区', null, '118.038716', '24.570214', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6555', '海光花园小区', '福建省', '厦门市', '集美区', null, '118.110267', '24.581112', '2018-01-03 20:38:01', null, '2018-01-04 19:31:05', null);
INSERT INTO `t_plot` VALUES ('6556', '海景明珠', '福建省', '厦门市', '集美区', null, '118.109867', '24.582406', '2018-01-03 20:38:01', null, '2018-01-04 19:31:06', null);
INSERT INTO `t_plot` VALUES ('6557', '杏林湾商务营运中心', '福建省', '厦门市', '集美区', null, '118.07172', '24.599643', '2018-01-03 20:38:01', null, '2018-01-04 19:31:06', null);
INSERT INTO `t_plot` VALUES ('6558', '明达玻璃宿舍', '福建省', '厦门市', '集美区', null, '118.046595', '24.570689', '2018-01-03 20:38:01', null, '2018-01-04 19:31:06', null);
INSERT INTO `t_plot` VALUES ('6559', '岑东社区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:06', null);
INSERT INTO `t_plot` VALUES ('6560', '黄庄外口公寓', '福建省', '厦门市', '集美区', null, '118.03738', '24.57145', '2018-01-03 20:38:01', null, '2018-01-04 19:31:06', null);
INSERT INTO `t_plot` VALUES ('6561', '华杏公寓', '福建省', '厦门市', '集美区', null, '118.0491', '24.573464', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6562', '文达路10号小区', '福建省', '厦门市', '集美区', null, '118.06478', '24.570926', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6563', '集美中学教工宿舍', '福建省', '厦门市', '集美区', null, '118.122216', '24.601058', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6564', '综合大楼', '福建省', '厦门市', '集美区', null, '118.049766', '24.577744', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6565', '汇海天下', '福建省', '厦门市', '集美区', null, '118.108382', '24.580218', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6566', '金融大厦', '福建省', '厦门市', '集美区', null, '118.055087', '24.56955', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6567', '仁德里小区', '福建省', '厦门市', '集美区', null, '118.052614', '24.632132', '2018-01-03 20:38:01', null, '2018-01-04 19:31:07', null);
INSERT INTO `t_plot` VALUES ('6568', '住宅莲花尚院(别墅)', '福建省', '厦门市', '集美区', null, '118.088361', '24.613903', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6569', '海角7号', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6570', '麒麟居', '福建省', '厦门市', '集美区', null, '118.104187', '24.580798', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6571', '滨水小区3#', '福建省', '厦门市', '集美区', null, '118.09559', '24.601753', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6572', '住宅水晶湖郡', '福建省', '厦门市', '集美区', null, '118.095648', '24.600042', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6573', '日东新村9-13号小区', '福建省', '厦门市', '集美区', null, '118.045442', '24.570284', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6574', '祥联楼', '福建省', '厦门市', '集美区', null, '117.99736', '24.612656', '2018-01-03 20:38:01', null, '2018-01-04 19:31:08', null);
INSERT INTO `t_plot` VALUES ('6575', '杏林东路45-49号小区', '福建省', '厦门市', '集美区', null, '118.058451', '24.568997', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6576', '月台路9号小区', '福建省', '厦门市', '集美区', null, '118.049858', '24.566196', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6577', '文达路37-39号', '福建省', '厦门市', '集美区', null, '118.066773', '24.571098', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6578', '石鼓路', '福建省', '厦门市', '集美区', null, '118.10535', '24.57856', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6579', '祥发广场', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6580', '集美区政府宿舍', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6581', '太源花园', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:09', null);
INSERT INTO `t_plot` VALUES ('6582', '农行宿舍', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6583', '盛光小区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6584', '集美大学湾', '福建省', '厦门市', '集美区', null, '118.113516', '24.591899', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6585', '中海御龙府(别墅)', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6586', '集美区商会综合大厦', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6587', '黄庄社区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6588', '夏商大学康城二期', '福建省', '厦门市', '集美区', null, '118.029855', '24.599315', '2018-01-03 20:38:01', null, '2018-01-04 19:31:10', null);
INSERT INTO `t_plot` VALUES ('6589', '滨水小区1#', '福建省', '厦门市', '集美区', null, '118.09559', '24.601753', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6590', '杏鹭苑', '福建省', '厦门市', '集美区', null, '118.060367', '24.570882', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6591', '滨水小区4#', '福建省', '厦门市', '集美区', null, '118.09559', '24.601753', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6592', '集北华庭', '福建省', '厦门市', '集美区', null, '118.115647', '24.606216', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6593', '鹤井巷2-8号小区', '福建省', '厦门市', '集美区', null, '118.047405', '24.571113', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6594', '集美大学莲花池住宅小区', '福建省', '厦门市', '集美区', null, '118.103465', '24.580839', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6595', '岑东路小区', '福建省', '厦门市', '集美区', null, '118.101148', '24.574699', '2018-01-03 20:38:01', null, '2018-01-04 19:31:11', null);
INSERT INTO `t_plot` VALUES ('6596', '杏信花园', '福建省', '厦门市', '集美区', null, '118.046796', '24.569308', '2018-01-03 20:38:01', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6597', '碧溪花园', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6598', '碧海蓝天西区', '福建省', '厦门市', '集美区', null, '118.114485', '24.584573', '2018-01-03 20:38:02', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6599', '凤泉广场', '福建省', '厦门市', '集美区', null, '117.998144', '24.613941', '2018-01-03 20:38:02', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6600', '中海御龙府', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6601', '鳌园路19号', '福建省', '厦门市', '集美区', null, '118.106394', '24.572274', '2018-01-03 20:38:02', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6602', '永同昌万美花园', '福建省', '厦门市', '集美区', null, '118.112286', '24.582943', '2018-01-03 20:38:02', null, '2018-01-04 19:31:12', null);
INSERT INTO `t_plot` VALUES ('6603', '文达二里小区', '福建省', '厦门市', '集美区', null, '118.066378', '24.569008', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6604', '滨海新苑', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6605', '观海豪庭', '福建省', '厦门市', '集美区', null, '118.110218', '24.58183', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6606', '纯净生活', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6607', '后溪花园', '福建省', '厦门市', '集美区', null, '118.066939', '24.645058', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6608', '广厦花园', '福建省', '厦门市', '集美区', null, '118.104078', '24.58433', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6609', '星海湾百桥别墅', '福建省', '厦门市', '集美区', null, '118.129419', '24.619432', '2018-01-03 20:38:02', null, '2018-01-04 19:31:13', null);
INSERT INTO `t_plot` VALUES ('6610', '集安广场安置房', '福建省', '厦门市', '集美区', null, '118.123451', '24.610785', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6611', '日东靖花园', '福建省', '厦门市', '集美区', null, '118.043025', '24.570062', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6612', '杏林东路25-27号小区', '福建省', '厦门市', '集美区', null, '118.054673', '24.569642', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6613', '滨水小区2#', '福建省', '厦门市', '集美区', null, '118.09559', '24.601753', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6614', '冶炼综合楼', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6615', '六华楼', '福建省', '厦门市', '集美区', null, '117.996587', '24.61264', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6616', '杏林东路51号小区', '福建省', '厦门市', '集美区', null, '118.057807', '24.568751', '2018-01-03 20:38:02', null, '2018-01-04 19:31:14', null);
INSERT INTO `t_plot` VALUES ('6617', '文康花园', '福建省', '厦门市', '集美区', null, '118.059429', '24.572041', '2018-01-03 20:38:02', null, '2018-01-04 19:31:15', null);
INSERT INTO `t_plot` VALUES ('6619', '水晶湖郡四期(别墅)', '福建省', '厦门市', '集美区', null, '118.094411', '24.599759', '2018-01-03 20:38:02', null, '2018-01-04 19:31:15', null);
INSERT INTO `t_plot` VALUES ('6620', '建南商城', '福建省', '厦门市', '集美区', null, '118.06226', '24.568432', '2018-01-03 20:38:02', null, '2018-01-04 19:31:15', null);
INSERT INTO `t_plot` VALUES ('6621', '时代家园', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:15', null);
INSERT INTO `t_plot` VALUES ('6622', '三秀社区', '福建省', '厦门市', '集美区', null, '118.029412', '24.640973', '2018-01-03 20:38:02', null, '2018-01-04 19:31:15', null);
INSERT INTO `t_plot` VALUES ('6623', '杏林湾高脚楼', '福建省', '厦门市', '集美区', null, '118.0977', '24.601136', '2018-01-03 20:38:02', null, '2018-01-04 19:31:15', null);
INSERT INTO `t_plot` VALUES ('6624', '天杏苑', '福建省', '厦门市', '集美区', null, '118.038791', '24.568153', '2018-01-03 20:38:02', null, '2018-01-04 19:31:16', null);
INSERT INTO `t_plot` VALUES ('6625', '福星大地', '福建省', '厦门市', '同安区', null, '118.150019', '24.732093', '2018-01-03 20:43:00', null, '2018-01-04 19:31:16', null);
INSERT INTO `t_plot` VALUES ('6626', '汉景嘉园', '福建省', '厦门市', '同安区', null, '118.132736', '24.69488', '2018-01-03 20:43:00', null, '2018-01-04 19:31:16', null);
INSERT INTO `t_plot` VALUES ('6627', '凤凰城', '福建省', '厦门市', '同安区', null, '118.169767', '24.726817', '2018-01-03 20:43:00', null, '2018-01-04 19:31:16', null);
INSERT INTO `t_plot` VALUES ('6628', '创业楼', '福建省', '厦门市', '同安区', null, '118.155431', '24.730054', '2018-01-03 20:43:00', null, '2018-01-04 19:31:16', null);
INSERT INTO `t_plot` VALUES ('6629', '西城晶华', '福建省', '厦门市', '同安区', null, '118.153408', '24.747862', '2018-01-03 20:43:00', null, '2018-01-04 19:31:16', null);
INSERT INTO `t_plot` VALUES ('6630', '银湖花园', '福建省', '厦门市', '同安区', null, '118.180034', '24.733947', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6631', '阳光美郡', '福建省', '厦门市', '同安区', null, '118.138905', '24.666351', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6632', '岳口小区', '福建省', '厦门市', '同安区', null, '118.169042', '24.729625', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6633', '华清中心城一期', '福建省', '厦门市', '同安区', null, '118.156436', '24.726752', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6634', '我爱莲花', '福建省', '厦门市', '同安区', null, '118.071146', '24.752529', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6635', '西池小区北区', '福建省', '厦门市', '同安区', null, '118.151908', '24.745503', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6636', '华森公园首府', '福建省', '厦门市', '同安区', null, '118.143236', '24.727713', '2018-01-03 20:43:00', null, '2018-01-04 19:31:17', null);
INSERT INTO `t_plot` VALUES ('6637', '中福花园', '福建省', '厦门市', '同安区', null, '118.15629', '24.730702', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6638', '巴厘香泉', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6639', '华福楼', '福建省', '厦门市', '同安区', null, '118.162877', '24.734149', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6640', '凤祥别墅', '福建省', '厦门市', '同安区', null, '118.145729', '24.730687', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6641', '西溪花园', '福建省', '厦门市', '同安区', null, '118.153244', '24.729508', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6642', '银溪春墅', '福建省', '厦门市', '同安区', null, '118.166689', '24.721272', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6643', '阳光城翡丽海岸(别墅)', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:18', null);
INSERT INTO `t_plot` VALUES ('6644', '公园道1号', '福建省', '厦门市', '同安区', null, '118.142836', '24.728773', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6645', '银星花园', '福建省', '厦门市', '同安区', null, '118.161674', '24.738573', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6646', '芸溪居住公园四期', '福建省', '厦门市', '同安区', null, '118.140452', '24.735691', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6647', '振兴小区', '福建省', '厦门市', '同安区', null, '118.149674', '24.737213', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6648', '华清中心城二期', '福建省', '厦门市', '同安区', null, '118.157791', '24.724865', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6649', '芸溪居住公园二期', '福建省', '厦门市', '同安区', null, '118.137947', '24.736403', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6650', '和力花园', '福建省', '厦门市', '同安区', null, '118.14884', '24.731579', '2018-01-03 20:43:00', null, '2018-01-04 19:31:19', null);
INSERT INTO `t_plot` VALUES ('6651', '欣发嘉园', '福建省', '厦门市', '同安区', null, '118.151673', '24.717486', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6652', '裕景新城', '福建省', '厦门市', '同安区', null, '118.162766', '24.732377', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6653', '芸溪居住公园一期', '福建省', '厦门市', '同安区', null, '118.140149', '24.735087', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6654', '禹洲阳光花城', '福建省', '厦门市', '同安区', null, '118.131456', '24.695052', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6655', '唐鸣祥福花园', '福建省', '厦门市', '同安区', null, '118.151602', '24.730251', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6656', '五福楼', '福建省', '厦门市', '同安区', null, '118.164606', '24.739705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6657', '城西一里小区', '福建省', '厦门市', '同安区', null, '118.153877', '24.73576', '2018-01-03 20:43:00', null, '2018-01-04 19:31:20', null);
INSERT INTO `t_plot` VALUES ('6658', '中铁花园', '福建省', '厦门市', '同安区', null, '118.162612', '24.737845', '2018-01-03 20:43:00', null, '2018-01-04 19:31:21', null);
INSERT INTO `t_plot` VALUES ('6659', '同成大厦', '福建省', '厦门市', '同安区', null, '118.149229', '24.73094', '2018-01-03 20:43:00', null, '2018-01-04 19:31:21', null);
INSERT INTO `t_plot` VALUES ('6660', '禹洲香溪里', '福建省', '厦门市', '同安区', null, '118.174985', '24.753208', '2018-01-03 20:43:00', null, '2018-01-04 19:31:21', null);
INSERT INTO `t_plot` VALUES ('6661', '厦航同城湾', '福建省', '厦门市', '同安区', null, '118.167725', '24.727923', '2018-01-03 20:43:00', null, '2018-01-04 19:31:21', null);
INSERT INTO `t_plot` VALUES ('6662', '恒亿尚品湾', '福建省', '厦门市', '同安区', null, '118.170567', '24.649684', '2018-01-03 20:43:00', null, '2018-01-04 19:31:21', null);
INSERT INTO `t_plot` VALUES ('6663', '古庄新城北区', '福建省', '厦门市', '同安区', null, '118.147565', '24.74094', '2018-01-03 20:43:00', null, '2018-01-04 19:31:21', null);
INSERT INTO `t_plot` VALUES ('6664', '银城佳园', '福建省', '厦门市', '同安区', null, '118.158731', '24.738047', '2018-01-03 20:43:00', null, '2018-01-04 19:31:22', null);
INSERT INTO `t_plot` VALUES ('6665', '佳成公寓', '福建省', '厦门市', '同安区', null, '118.149209', '24.73124', '2018-01-03 20:43:00', null, '2018-01-04 19:31:22', null);
INSERT INTO `t_plot` VALUES ('6666', '锦华金都', '福建省', '厦门市', '同安区', null, '118.153753', '24.740283', '2018-01-03 20:43:00', null, '2018-01-04 19:31:22', null);
INSERT INTO `t_plot` VALUES ('6668', '城北花园', '福建省', '厦门市', '同安区', null, '118.159744', '24.74467', '2018-01-03 20:43:00', null, '2018-01-04 19:31:22', null);
INSERT INTO `t_plot` VALUES ('6669', '阳光清境', '福建省', '厦门市', '同安区', null, '118.151959', '24.746823', '2018-01-03 20:43:00', null, '2018-01-04 19:31:22', null);
INSERT INTO `t_plot` VALUES ('6670', '锦辉国际花园', '福建省', '厦门市', '同安区', null, '118.164704', '24.721134', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6671', '银城明珠', '福建省', '厦门市', '同安区', null, '118.151371', '24.714404', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6672', '大族泉天下', '福建省', '厦门市', '同安区', null, '118.141234', '24.793438', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6673', '居安里小区', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6674', '新城花园', '福建省', '厦门市', '同安区', null, '118.154864', '24.73205', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6675', '三秀新城', '福建省', '厦门市', '同安区', null, '118.157524', '24.738075', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6676', '同安城北小区', '福建省', '厦门市', '同安区', null, '118.14951', '24.744613', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6677', '邮电新村', '福建省', '厦门市', '同安区', null, '118.149771', '24.733734', '2018-01-03 20:43:00', null, '2018-01-04 19:31:23', null);
INSERT INTO `t_plot` VALUES ('6678', '后炉新村', '福建省', '厦门市', '同安区', null, '118.16034', '24.741007', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6679', '古龙御园', '福建省', '厦门市', '同安区', null, '118.164819', '24.731677', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6680', '鸿梅花园', '福建省', '厦门市', '同安区', null, '118.172802', '24.747315', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6681', '西安苑', '福建省', '厦门市', '同安区', null, '118.156075', '24.736201', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6682', '汀溪山庄', '福建省', '厦门市', '同安区', null, '118.144042', '24.786968', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6683', '大唐世家一期', '福建省', '厦门市', '同安区', null, '118.155031', '24.72467', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6684', '公路局宿舍楼', '福建省', '厦门市', '同安区', null, '118.154713', '24.730581', '2018-01-03 20:43:00', null, '2018-01-04 19:31:24', null);
INSERT INTO `t_plot` VALUES ('6685', '荣发新村', '福建省', '厦门市', '同安区', null, '118.159374', '24.721812', '2018-01-03 20:43:00', null, '2018-01-04 19:31:25', null);
INSERT INTO `t_plot` VALUES ('6686', '温馨佳苑', '福建省', '厦门市', '同安区', null, '118.149456', '24.718188', '2018-01-03 20:43:00', null, '2018-01-04 19:31:25', null);
INSERT INTO `t_plot` VALUES ('6687', '瑞祥家园', '福建省', '厦门市', '同安区', null, '118.140789', '24.733704', '2018-01-03 20:43:00', null, '2018-01-04 19:31:25', null);
INSERT INTO `t_plot` VALUES ('6688', '桃园山庄', '福建省', '厦门市', '同安区', null, '118.14177', '24.667116', '2018-01-03 20:43:00', null, '2018-01-04 19:31:25', null);
INSERT INTO `t_plot` VALUES ('6689', '印发花园', '福建省', '厦门市', '同安区', null, '118.158954', '24.741831', '2018-01-03 20:43:00', null, '2018-01-04 19:31:25', null);
INSERT INTO `t_plot` VALUES ('6690', '金都海尚国际', '福建省', '厦门市', '同安区', null, '118.155064', '24.633472', '2018-01-03 20:43:00', null, '2018-01-04 19:31:25', null);
INSERT INTO `t_plot` VALUES ('6691', '同发大厦', '福建省', '厦门市', '同安区', null, '118.152877', '24.745918', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6692', '原乡园墅(别墅)', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6693', '银环佳园', '福建省', '厦门市', '同安区', null, '118.152115', '24.729191', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6694', '彩虹湾', '福建省', '厦门市', '同安区', null, '118.145313', '24.63421', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6695', '朝元保险公司宿舍楼', '福建省', '厦门市', '同安区', null, '118.15316', '24.742248', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6696', '臻品盛世', '福建省', '厦门市', '同安区', null, '118.16272', '24.733544', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6697', '华盛楼', '福建省', '厦门市', '同安区', null, '118.16617', '24.737589', '2018-01-03 20:43:00', null, '2018-01-04 19:31:26', null);
INSERT INTO `t_plot` VALUES ('6698', '银华新村', '福建省', '厦门市', '同安区', null, '118.174681', '24.730067', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6699', '芸溪居住公园三期', '福建省', '厦门市', '同安区', null, '118.140141', '24.736728', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6700', '祥云楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6701', '厦航高郡', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6702', '金帝中洲滨海城', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6703', '金利蓝湾新城', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6704', '水湾清城', '福建省', '厦门市', '同安区', null, '118.153027', '24.733077', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6705', '航空祥郡(别墅)', '福建省', '厦门市', '同安区', null, '118.139535', '24.731822', '2018-01-03 20:43:00', null, '2018-01-04 19:31:27', null);
INSERT INTO `t_plot` VALUES ('6706', '特房银溪墅府(别墅)', '福建省', '厦门市', '同安区', null, '118.196134', '24.66782', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6707', '洪塘集资楼', '福建省', '厦门市', '同安区', null, '118.20559', '24.720201', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6708', '新景舜弘上院', '福建省', '厦门市', '同安区', null, '118.151284', '24.727647', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6709', '尚美花城', '福建省', '厦门市', '同安区', null, '118.15109', '24.748459', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6710', '古庄教育楼', '福建省', '厦门市', '同安区', null, '118.145568', '24.741389', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6711', '后炉花园', '福建省', '厦门市', '同安区', null, '118.159252', '24.740964', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6712', '凤祥小区', '福建省', '厦门市', '同安区', null, '118.146718', '24.729776', '2018-01-03 20:43:00', null, '2018-01-04 19:31:28', null);
INSERT INTO `t_plot` VALUES ('6713', '新景城市天骄', '福建省', '厦门市', '同安区', null, '118.12968', '24.69623', '2018-01-03 20:43:00', null, '2018-01-04 19:31:29', null);
INSERT INTO `t_plot` VALUES ('6714', '银城商厦', '福建省', '厦门市', '同安区', null, '118.156416', '24.737934', '2018-01-03 20:43:00', null, '2018-01-04 19:31:29', null);
INSERT INTO `t_plot` VALUES ('6715', '大唐世家六期', '福建省', '厦门市', '同安区', null, '118.152116', '24.721342', '2018-01-03 20:43:00', null, '2018-01-04 19:31:29', null);
INSERT INTO `t_plot` VALUES ('6716', '同城四季北座', '福建省', '厦门市', '同安区', null, '118.14153', '24.724356', '2018-01-03 20:43:00', null, '2018-01-04 19:31:29', null);
INSERT INTO `t_plot` VALUES ('6717', '城北新村', '福建省', '厦门市', '同安区', null, '118.158748', '24.745467', '2018-01-03 20:43:00', null, '2018-01-04 19:31:29', null);
INSERT INTO `t_plot` VALUES ('6718', '洋坂里小区', '福建省', '厦门市', '同安区', null, '118.151168', '24.740948', '2018-01-03 20:43:00', null, '2018-01-04 19:31:29', null);
INSERT INTO `t_plot` VALUES ('6719', '双溪公寓', '福建省', '厦门市', '同安区', null, '118.163332', '24.729985', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6720', '鸿福新村', '福建省', '厦门市', '同安区', null, '118.148661', '24.732919', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6721', '大溪地', '福建省', '厦门市', '同安区', null, '118.146577', '24.738688', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6722', '西安广场', '福建省', '厦门市', '同安区', null, '118.156224', '24.735412', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6723', '禹洲香溪里(别墅)', '福建省', '厦门市', '同安区', null, '118.174985', '24.753208', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6724', '禹洲大学城', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6725', '大唐水云间', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:30', null);
INSERT INTO `t_plot` VALUES ('6726', '禹洲溪堤尚城', '福建省', '厦门市', '同安区', null, '118.176918', '24.751991', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6727', '银鹭苑', '福建省', '厦门市', '同安区', null, '118.16424', '24.730698', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6728', '大唐CBD高巢', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6729', '祥顺新村一期', '福建省', '厦门市', '同安区', null, '118.159134', '24.721459', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6730', '新景城市天骄二期', '福建省', '厦门市', '同安区', null, '118.130089', '24.696245', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6731', '祥富佳园', '福建省', '厦门市', '同安区', null, '118.146931', '24.732017', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6732', '华泰', '福建省', '厦门市', '同安区', null, '118.161793', '24.737245', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6733', '向阳楼', '福建省', '厦门市', '同安区', null, '118.1501', '24.732793', '2018-01-03 20:43:00', null, '2018-01-04 19:31:31', null);
INSERT INTO `t_plot` VALUES ('6734', '和祥苑', '福建省', '厦门市', '同安区', null, '118.154782', '24.729982', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6735', '居安里26-27号', '福建省', '厦门市', '同安区', null, '118.1548', '24.738901', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6736', '鹭洲花园', '福建省', '厦门市', '同安区', null, '118.149122', '24.73392', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6737', '大族泉天下(别墅)', '福建省', '厦门市', '同安区', null, '118.141234', '24.793438', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6738', '大唐世家二期', '福建省', '厦门市', '同安区', null, '118.154266', '24.723614', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6739', '长荣花园', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6740', '同顺公寓', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:32', null);
INSERT INTO `t_plot` VALUES ('6741', '同安畜牧综合楼小区', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6742', '阳光城翡丽海岸', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6743', '古龙翰林阁', '福建省', '厦门市', '同安区', null, '118.144562', '24.733989', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6744', '兴福楼', '福建省', '厦门市', '同安区', null, '118.172926', '24.728144', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6745', '舒安里', '福建省', '厦门市', '同安区', null, '118.152142', '24.737775', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6746', '瑞华楼', '福建省', '厦门市', '同安区', null, '118.151024', '24.738198', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6747', '泰禾汀溪院子(别墅)', '福建省', '厦门市', '同安区', null, '118.144648', '24.783664', '2018-01-03 20:43:00', null, '2018-01-04 19:31:33', null);
INSERT INTO `t_plot` VALUES ('6748', '银安大厦', '福建省', '厦门市', '同安区', null, '118.155876', '24.735979', '2018-01-03 20:43:00', null, '2018-01-04 19:31:34', null);
INSERT INTO `t_plot` VALUES ('6749', '南门新村', '福建省', '厦门市', '同安区', null, '118.160615', '24.736349', '2018-01-03 20:43:00', null, '2018-01-04 19:31:34', null);
INSERT INTO `t_plot` VALUES ('6750', '芸溪居住公园三期(别墅)', '福建省', '厦门市', '同安区', null, '118.140141', '24.736728', '2018-01-03 20:43:00', null, '2018-01-04 19:31:34', null);
INSERT INTO `t_plot` VALUES ('6751', '建发中央天悦', '福建省', '厦门市', '同安区', null, '118.146339', '24.723855', '2018-01-03 20:43:00', null, '2018-01-04 19:31:34', null);
INSERT INTO `t_plot` VALUES ('6752', '富贵家园', '福建省', '厦门市', '同安区', null, '118.153706', '24.733022', '2018-01-03 20:43:00', null, '2018-01-04 19:31:34', null);
INSERT INTO `t_plot` VALUES ('6753', '西安卫校安置房', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:34', null);
INSERT INTO `t_plot` VALUES ('6754', '新景城市天骄一期', '福建省', '厦门市', '同安区', null, '118.128444', '24.696239', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6755', '埕西新村', '福建省', '厦门市', '同安区', null, '118.153931', '24.738844', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6756', '鸿利小区', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6757', '陆丰里', '福建省', '厦门市', '同安区', null, '118.15671', '24.731282', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6758', '育安', '福建省', '厦门市', '同安区', null, '118.153801', '24.74577', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6759', '新景舜弘现代城', '福建省', '厦门市', '同安区', null, '118.147444', '24.695362', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6760', '汀溪尚品', '福建省', '厦门市', '同安区', null, '118.144648', '24.783664', '2018-01-03 20:43:00', null, '2018-01-04 19:31:35', null);
INSERT INTO `t_plot` VALUES ('6761', '西池小区南区', '福建省', '厦门市', '同安区', null, '118.151908', '24.745503', '2018-01-03 20:43:00', null, '2018-01-04 19:31:36', null);
INSERT INTO `t_plot` VALUES ('6762', '莲福广场', '福建省', '厦门市', '同安区', null, '118.173223', '24.726519', '2018-01-03 20:43:00', null, '2018-01-04 19:31:36', null);
INSERT INTO `t_plot` VALUES ('6763', '祥和楼', '福建省', '厦门市', '同安区', null, '118.14926', '24.732671', '2018-01-03 20:43:00', null, '2018-01-04 19:31:36', null);
INSERT INTO `t_plot` VALUES ('6764', '建发环中楼', '福建省', '厦门市', '同安区', null, '118.160341', '24.742747', '2018-01-03 20:43:00', null, '2018-01-04 19:31:36', null);
INSERT INTO `t_plot` VALUES ('6765', '溢翔首府', '福建省', '厦门市', '同安区', null, '118.168038', '24.650275', '2018-01-03 20:43:00', null, '2018-01-04 19:31:36', null);
INSERT INTO `t_plot` VALUES ('6766', '同安人家', '福建省', '厦门市', '同安区', null, '118.151449', '24.729625', '2018-01-03 20:43:00', null, '2018-01-04 19:31:37', null);
INSERT INTO `t_plot` VALUES ('6767', '北镇公寓', '福建省', '厦门市', '同安区', null, '118.160909', '24.742004', '2018-01-03 20:43:00', null, '2018-01-04 19:31:38', null);
INSERT INTO `t_plot` VALUES ('6768', '银福小区', '福建省', '厦门市', '同安区', null, '118.156381', '24.731803', '2018-01-03 20:43:00', null, '2018-01-04 19:31:38', null);
INSERT INTO `t_plot` VALUES ('6769', '古龙山语听溪(别墅)', '福建省', '厦门市', '同安区', null, '118.1436', '24.778821', '2018-01-03 20:43:01', null, '2018-01-04 19:31:38', null);
INSERT INTO `t_plot` VALUES ('6770', '翔安洪塘西炉小区', '福建省', '厦门市', '同安区', null, '118.206333', '24.669764', '2018-01-03 20:43:01', null, '2018-01-04 19:31:38', null);
INSERT INTO `t_plot` VALUES ('6771', '邮电局综合楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:38', null);
INSERT INTO `t_plot` VALUES ('6772', '佰仕家园', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6773', '欣盛丰森林海', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6774', '北星花园', '福建省', '厦门市', '同安区', null, '118.161557', '24.741972', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6775', '银晟书苑', '福建省', '厦门市', '同安区', null, '118.132298', '24.728991', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6776', '同城四季南座', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6777', '富丽华名园', '福建省', '厦门市', '同安区', null, '118.172277', '24.725211', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6778', '陆丰嘉园二期', '福建省', '厦门市', '同安区', null, '118.155063', '24.731551', '2018-01-03 20:43:01', null, '2018-01-04 19:31:39', null);
INSERT INTO `t_plot` VALUES ('6779', '鸿吉公寓', '福建省', '厦门市', '同安区', null, '118.153362', '24.73834', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6780', '聚发新村', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6781', '凤岳楼', '福建省', '厦门市', '同安区', null, '118.166352', '24.729991', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6782', '利祥花园', '福建省', '厦门市', '同安区', null, '118.154056', '24.745183', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6783', '水利局宿舍楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6784', '钟楼安置房', '福建省', '厦门市', '同安区', null, '118.15894', '24.73661', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6785', '磐金文化城', '福建省', '厦门市', '同安区', null, '118.157874', '24.739078', '2018-01-03 20:43:01', null, '2018-01-04 19:31:40', null);
INSERT INTO `t_plot` VALUES ('6786', '陆丰嘉园', '福建省', '厦门市', '同安区', null, '118.155063', '24.731551', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6787', '祥顺新村二期', '福建省', '厦门市', '同安区', null, '118.159134', '24.721459', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6788', '航空祥郡', '福建省', '厦门市', '同安区', null, '118.139535', '24.731822', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6789', '保利叁仟栋', '福建省', '厦门市', '同安区', null, '118.158335', '24.639904', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6790', '国贸金沙湾', '福建省', '厦门市', '同安区', null, '118.168584', '24.648446', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6791', '幸福U品', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6792', '特房银溪墅府', '福建省', '厦门市', '同安区', null, '118.196134', '24.66782', '2018-01-03 20:43:01', null, '2018-01-04 19:31:41', null);
INSERT INTO `t_plot` VALUES ('6793', '佳业阳光大第', '福建省', '厦门市', '同安区', null, '118.151535', '24.728958', '2018-01-03 20:43:01', null, '2018-01-04 19:31:42', null);
INSERT INTO `t_plot` VALUES ('6794', '杜桥教师楼', '福建省', '厦门市', '同安区', null, '118.144006', '24.71921', '2018-01-03 20:43:01', null, '2018-01-04 19:31:42', null);
INSERT INTO `t_plot` VALUES ('6795', '保利叁仟栋(别墅)', '福建省', '厦门市', '同安区', null, '118.158335', '24.639904', '2018-01-03 20:43:01', null, '2018-01-04 19:31:42', null);
INSERT INTO `t_plot` VALUES ('6796', '祥发楼', '福建省', '厦门市', '同安区', null, '118.148999', '24.73098', '2018-01-03 20:43:01', null, '2018-01-04 19:31:42', null);
INSERT INTO `t_plot` VALUES ('6797', '磐金锦绣江南', '福建省', '厦门市', '同安区', null, '118.169018', '24.72992', '2018-01-03 20:43:01', null, '2018-01-04 19:31:42', null);
INSERT INTO `t_plot` VALUES ('6798', '富贵家园二期', '福建省', '厦门市', '同安区', null, '118.155728', '24.727826', '2018-01-03 20:43:01', null, '2018-01-04 19:31:42', null);
INSERT INTO `t_plot` VALUES ('6799', '金安大厦', '福建省', '厦门市', '同安区', null, '118.155805', '24.735478', '2018-01-03 20:43:01', null, '2018-01-04 19:31:43', null);
INSERT INTO `t_plot` VALUES ('6800', '东溪新村', '福建省', '厦门市', '同安区', null, '118.161383', '24.734742', '2018-01-03 20:43:01', null, '2018-01-04 19:31:43', null);
INSERT INTO `t_plot` VALUES ('6801', '古龙山语听溪', '福建省', '厦门市', '同安区', null, '118.1436', '24.778821', '2018-01-03 20:43:01', null, '2018-01-04 19:31:43', null);
INSERT INTO `t_plot` VALUES ('6802', '中联观云溪', '福建省', '厦门市', '同安区', null, '118.142487', '24.666218', '2018-01-03 20:43:01', null, '2018-01-04 19:31:43', null);
INSERT INTO `t_plot` VALUES ('6803', '银星商厦', '福建省', '厦门市', '同安区', null, '118.172677', '24.72851', '2018-01-03 20:43:01', null, '2018-01-04 19:31:43', null);
INSERT INTO `t_plot` VALUES ('6804', '大溪地(别墅)', '福建省', '厦门市', '同安区', null, '118.146577', '24.738688', '2018-01-03 20:43:01', null, '2018-01-04 19:31:43', null);
INSERT INTO `t_plot` VALUES ('6805', '原乡园墅', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6806', '禹洲溪堤尚城(别墅)', '福建省', '厦门市', '同安区', null, '118.176918', '24.751991', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6807', '和谐天下', '福建省', '厦门市', '同安区', null, '118.135382', '24.617067', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6808', '同安滨海公寓', '福建省', '厦门市', '同安区', null, '118.169527', '24.65893', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6809', '瑞祥花园', '福建省', '厦门市', '同安区', null, '118.145782', '24.733598', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6810', '城南阳翟教师楼', '福建省', '厦门市', '同安区', null, '118.153473', '24.72109', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6811', '富鸿温泉公寓', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:44', null);
INSERT INTO `t_plot` VALUES ('6812', '海西舜弘公馆', '福建省', '厦门市', '同安区', null, '118.152522', '24.728342', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6813', '得月楼', '福建省', '厦门市', '同安区', null, '118.15102', '24.733027', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6814', '中海万锦熙岸', '福建省', '厦门市', '同安区', null, '118.148221', '24.658682', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6815', '新都花园', '福建省', '厦门市', '同安区', null, '118.152827', '24.722664', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6816', '中医院宿舍楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6817', '银溪春墅(别墅)', '福建省', '厦门市', '同安区', null, '118.166689', '24.721272', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6818', '舒安居', '福建省', '厦门市', '同安区', null, '118.152183', '24.738083', '2018-01-03 20:43:01', null, '2018-01-04 19:31:45', null);
INSERT INTO `t_plot` VALUES ('6819', '水岸华庭', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6820', '联谊花园', '福建省', '厦门市', '同安区', null, '118.159492', '24.729425', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6821', '融信铂悦湾', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6822', '大唐世家', '福建省', '厦门市', '同安区', null, '118.152116', '24.721342', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6823', '后炉里80号小区', '福建省', '厦门市', '同安区', null, '118.159863', '24.741792', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6824', '祥安楼', '福建省', '厦门市', '同安区', null, '118.14967', '24.732532', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6825', '丁宜山庄', '福建省', '厦门市', '同安区', null, '118.135851', '24.614672', '2018-01-03 20:43:01', null, '2018-01-04 19:31:46', null);
INSERT INTO `t_plot` VALUES ('6826', '百缘居', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6827', '凤祥苑', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6828', '佳展楼', '福建省', '厦门市', '同安区', null, '118.151046', '24.729616', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6829', '同安为民楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6830', '红怡苑', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6831', '食品住宅楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6832', '环城中路法院宿舍', '福建省', '厦门市', '同安区', null, '118.161247', '24.742085', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6833', '金都新蓝湾', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:47', null);
INSERT INTO `t_plot` VALUES ('6834', '三秀南里35号', '福建省', '厦门市', '同安区', null, '118.157747', '24.736953', '2018-01-03 20:43:01', null, '2018-01-04 19:31:48', null);
INSERT INTO `t_plot` VALUES ('6835', '九曜山家园', '福建省', '厦门市', '同安区', null, '118.165446', '24.731017', '2018-01-03 20:43:01', null, '2018-01-04 19:31:48', null);
INSERT INTO `t_plot` VALUES ('6836', '华安大厦', '福建省', '厦门市', '同安区', null, '118.151533', '24.73794', '2018-01-03 20:43:01', null, '2018-01-04 19:31:48', null);
INSERT INTO `t_plot` VALUES ('6837', '芸溪溪湖尚景', '福建省', '厦门市', '同安区', null, '118.144855', '24.736599', '2018-01-03 20:43:01', null, '2018-01-04 19:31:48', null);
INSERT INTO `t_plot` VALUES ('6838', '外贸综合楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:48', null);
INSERT INTO `t_plot` VALUES ('6839', '皮肤院宿舍楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:48', null);
INSERT INTO `t_plot` VALUES ('6840', '华侨新村', '福建省', '厦门市', '同安区', null, '118.211044', '24.78061', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6841', '古庄新城南区', '福建省', '厦门市', '同安区', null, '118.147565', '24.74094', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6842', '荣华楼', '福建省', '厦门市', '同安区', null, '118.156969', '24.74357', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6843', '瑞华商业城', '福建省', '厦门市', '同安区', null, '118.152304', '24.739374', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6844', '新银楼', '福建省', '厦门市', '同安区', null, '118.163518', '24.737969', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6845', '古庄交通宿舍楼', '福建省', '厦门市', '同安区', null, '118.145568', '24.741389', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6846', '古龙水岸华庭', '福建省', '厦门市', '同安区', null, '118.146383', '24.735208', '2018-01-03 20:43:01', null, '2018-01-04 19:31:49', null);
INSERT INTO `t_plot` VALUES ('6847', '祥福阁', '福建省', '厦门市', '同安区', null, '118.162546', '24.741234', '2018-01-03 20:43:01', null, '2018-01-04 19:31:50', null);
INSERT INTO `t_plot` VALUES ('6848', '凯歌会所别莊', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:50', null);
INSERT INTO `t_plot` VALUES ('6849', '洋坂宫', '福建省', '厦门市', '同安区', null, '118.151425', '24.74021', '2018-01-03 20:43:01', null, '2018-01-04 19:31:50', null);
INSERT INTO `t_plot` VALUES ('6850', '同安西福小区', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:50', null);
INSERT INTO `t_plot` VALUES ('6851', '紫云花园', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:50', null);
INSERT INTO `t_plot` VALUES ('6852', '丽水云天', '福建省', '厦门市', '同安区', null, '118.141506', '24.796801', '2018-01-03 20:43:01', null, '2018-01-04 19:31:50', null);
INSERT INTO `t_plot` VALUES ('6853', '西溪社区', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6854', '和祥楼', '福建省', '厦门市', '同安区', null, '118.15679', '24.743918', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6855', '同安三秀制面厂安置房', '福建省', '厦门市', '同安区', null, '118.163552', '24.738', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6856', '畜牧宿舍', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6857', '和谐佳园', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6858', '金桥楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6859', '同安县电力公司职工住宅', '福建省', '厦门市', '同安区', null, '118.152716', '24.740814', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6860', '卫生局宿舍楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:51', null);
INSERT INTO `t_plot` VALUES ('6861', '银益楼', '福建省', '厦门市', '同安区', null, '118.15118', '24.733227', '2018-01-03 20:43:01', null, '2018-01-04 19:31:52', null);
INSERT INTO `t_plot` VALUES ('6863', '交警宿舍楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:52', null);
INSERT INTO `t_plot` VALUES ('6864', '运发公寓', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:52', null);
INSERT INTO `t_plot` VALUES ('6865', '同安祥和花园', '福建省', '厦门市', '同安区', null, '118.154055', '24.744676', '2018-01-03 20:43:01', null, '2018-01-04 19:31:52', null);
INSERT INTO `t_plot` VALUES ('6866', '新安楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:52', null);
INSERT INTO `t_plot` VALUES ('6867', '城西二里', '福建省', '厦门市', '同安区', null, '118.149932', '24.737688', '2018-01-03 20:43:01', null, '2018-01-04 19:31:53', null);
INSERT INTO `t_plot` VALUES ('6868', '巴厘香泉(别墅)', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:53', null);
INSERT INTO `t_plot` VALUES ('6869', '小坪水电大楼', '福建省', '厦门市', '同安区', null, '118.164277', '24.73856', '2018-01-03 20:43:01', null, '2018-01-04 19:31:53', null);
INSERT INTO `t_plot` VALUES ('6870', '大兴商城', '福建省', '厦门市', '同安区', null, '118.157473', '24.737311', '2018-01-03 20:43:01', null, '2018-01-04 19:31:53', null);
INSERT INTO `t_plot` VALUES ('6871', '方圆楼', '福建省', '厦门市', '同安区', null, '118.152602', '24.737996', '2018-01-03 20:43:01', null, '2018-01-04 19:31:53', null);
INSERT INTO `t_plot` VALUES ('6872', '莲兴楼', '福建省', '厦门市', '同安区', null, '118.14987', '24.732882', '2018-01-03 20:43:01', null, '2018-01-04 19:31:53', null);
INSERT INTO `t_plot` VALUES ('6873', '南门里4-9号', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:54', null);
INSERT INTO `t_plot` VALUES ('6874', '银佳楼', '福建省', '厦门市', '同安区', null, '118.174712', '24.726887', '2018-01-03 20:43:01', null, '2018-01-04 19:31:54', null);
INSERT INTO `t_plot` VALUES ('6875', '同顺', '福建省', '厦门市', '同安区', null, '118.163411', '24.740468', '2018-01-03 20:43:01', null, '2018-01-04 19:31:54', null);
INSERT INTO `t_plot` VALUES ('6876', '居安', '福建省', '厦门市', '同安区', null, '118.155129', '24.738873', '2018-01-03 20:43:01', null, '2018-01-04 19:31:54', null);
INSERT INTO `t_plot` VALUES ('6877', '华清中心城三期祥顺新村', '福建省', '厦门市', '同安区', null, '118.159134', '24.721459', '2018-01-03 20:43:01', null, '2018-01-04 19:31:54', null);
INSERT INTO `t_plot` VALUES ('6878', '华泰楼', '福建省', '厦门市', '同安区', null, '118.114685', '24.781705', '2018-01-03 20:43:01', null, '2018-01-04 19:31:54', null);
INSERT INTO `t_plot` VALUES ('6879', '三秀北里', '福建省', '厦门市', '同安区', null, '118.155821', '24.739979', '2018-01-03 20:43:01', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6880', '火炬星光园', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6881', '春江里(55-79)', '福建省', '厦门市', '翔安区', null, '118.255198', '24.638378', '2018-01-03 20:48:00', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6882', '美地雅登祥吴三里', '福建省', '厦门市', '翔安区', null, '118.250589', '24.631376', '2018-01-03 20:48:00', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6883', '新景国际城', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6884', '联发欣悦学府', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6885', '特房黎安小镇四期', '福建省', '厦门市', '翔安区', null, '118.250758', '24.667339', '2018-01-03 20:48:00', null, '2018-01-04 19:31:55', null);
INSERT INTO `t_plot` VALUES ('6886', '建发翔城国际2', '福建省', '厦门市', '翔安区', null, '118.2505', '24.578689', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6887', '洋唐居住区保障性安居房', '福建省', '厦门市', '翔安区', null, '118.25205', '24.583194', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6888', '招商雍华府1', '福建省', '厦门市', '翔安区', null, '118.251239', '24.620279', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6889', '阳光城翡丽湾', '福建省', '厦门市', '翔安区', null, '118.234864', '24.565143', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6890', '海峡现代城', '福建省', '厦门市', '翔安区', null, '118.247291', '24.554559', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6891', '新城安置二组团', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6892', '东方新城', '福建省', '厦门市', '翔安区', null, '118.255236', '24.621559', '2018-01-03 20:48:00', null, '2018-01-04 19:31:56', null);
INSERT INTO `t_plot` VALUES ('6893', '联发欣悦学府(别墅)', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6894', '顺兴楼', '福建省', '厦门市', '翔安区', null, '118.340619', '24.566118', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6895', '联合博学园一期', '福建省', '厦门市', '翔安区', null, '118.247184', '24.600671', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6896', '舫华楼', '福建省', '厦门市', '翔安区', null, '118.256569', '24.671714', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6897', '东城合院(别墅)', '福建省', '厦门市', '翔安区', null, '118.254448', '24.627002', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6898', '阳光城翡丽湾(别墅)', '福建省', '厦门市', '翔安区', null, '118.234864', '24.565143', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6899', '首开领翔上郡', '福建省', '厦门市', '翔安区', null, '118.261106', '24.622291', '2018-01-03 20:48:00', null, '2018-01-04 19:31:57', null);
INSERT INTO `t_plot` VALUES ('6900', '吉翔安康第一城', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:58', null);
INSERT INTO `t_plot` VALUES ('6901', '欣盛丰乐活', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:58', null);
INSERT INTO `t_plot` VALUES ('6902', '国贸金门湾大德沙', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:00', null, '2018-01-04 19:31:58', null);
INSERT INTO `t_plot` VALUES ('6903', '联发橄榄墅', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:58', null);
INSERT INTO `t_plot` VALUES ('6904', '世茂御海墅', '福建省', '厦门市', '翔安区', null, '118.249312', '24.549841', '2018-01-03 20:48:00', null, '2018-01-04 19:31:58', null);
INSERT INTO `t_plot` VALUES ('6905', '特房黎安小镇二期', '福建省', '厦门市', '翔安区', null, '118.250758', '24.667339', '2018-01-03 20:48:00', null, '2018-01-04 19:31:58', null);
INSERT INTO `t_plot` VALUES ('6906', '新城安置三组团', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6907', '禹洲卢卡小镇(别墅)', '福建省', '厦门市', '翔安区', null, '118.252167', '24.60052', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6908', '滨安花园', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6909', '住宅莲花尚城', '福建省', '厦门市', '翔安区', null, '118.248715', '24.60529', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6910', '春江里(80-85)', '福建省', '厦门市', '翔安区', null, '118.255198', '24.638378', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6911', '国贸金门湾皓月天', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6912', '禹洲卢卡小镇', '福建省', '厦门市', '翔安区', null, '118.252167', '24.60052', '2018-01-03 20:48:00', null, '2018-01-04 19:31:59', null);
INSERT INTO `t_plot` VALUES ('6913', '汇景新城中心', '福建省', '厦门市', '翔安区', null, '118.250245', '24.625611', '2018-01-03 20:48:00', null, '2018-01-04 19:32:00', null);
INSERT INTO `t_plot` VALUES ('6914', '首开领翔国际', '福建省', '厦门市', '翔安区', null, '118.247717', '24.612196', '2018-01-03 20:48:00', null, '2018-01-04 19:32:00', null);
INSERT INTO `t_plot` VALUES ('6916', '美地雅登祥吴一里', '福建省', '厦门市', '翔安区', null, '118.246626', '24.631486', '2018-01-03 20:48:00', null, '2018-01-04 19:32:00', null);
INSERT INTO `t_plot` VALUES ('6917', '八融LOFT', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:00', null);
INSERT INTO `t_plot` VALUES ('6918', '春江里(23-54)', '福建省', '厦门市', '翔安区', null, '118.255198', '24.638378', '2018-01-03 20:48:00', null, '2018-01-04 19:32:00', null);
INSERT INTO `t_plot` VALUES ('6919', '特房锦绣祥安', '福建省', '厦门市', '翔安区', null, '118.265708', '24.661596', '2018-01-03 20:48:00', null, '2018-01-04 19:32:00', null);
INSERT INTO `t_plot` VALUES ('6920', '美地雅登祥吴二里', '福建省', '厦门市', '翔安区', null, '118.248262', '24.63146', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6921', '碧桂园云空间', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6922', '特房黎安小镇三期', '福建省', '厦门市', '翔安区', null, '118.250758', '24.667339', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6923', '海峡现代城一期', '福建省', '厦门市', '翔安区', null, '118.247291', '24.554559', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6924', '祥福三里小区', '福建省', '厦门市', '翔安区', null, '118.25729', '24.620502', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6925', '新店教师楼', '福建省', '厦门市', '翔安区', null, '118.253699', '24.614739', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6926', '新城安置一组团', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:01', null);
INSERT INTO `t_plot` VALUES ('6927', '后滨安置房', '福建省', '厦门市', '翔安区', null, '118.257049', '24.658213', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6928', '春江里(1-22)', '福建省', '厦门市', '翔安区', null, '118.255198', '24.638378', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6929', '东方新城二期', '福建省', '厦门市', '翔安区', null, '118.255236', '24.621559', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6930', '闽篮城市广场', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6931', '中骏蓝湾尚都(别墅)', '福建省', '厦门市', '翔安区', null, '118.144769', '24.539658', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6932', '华论国际大厦', '福建省', '厦门市', '翔安区', null, '118.247', '24.583021', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6933', '首开领翔上郡(别墅)', '福建省', '厦门市', '翔安区', null, '118.261106', '24.622291', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6934', '联合博学园二期', '福建省', '厦门市', '翔安区', null, '118.247184', '24.600671', '2018-01-03 20:48:00', null, '2018-01-04 19:32:02', null);
INSERT INTO `t_plot` VALUES ('6935', '翔林夏都', '福建省', '厦门市', '翔安区', null, '118.25998', '24.675474', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6936', '祥福一里', '福建省', '厦门市', '翔安区', null, '118.255842', '24.623557', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6937', '美地雅登祥吴五里', '福建省', '厦门市', '翔安区', null, '118.254613', '24.63259', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6938', '洋唐居住区保障性安居房A11', '福建省', '厦门市', '翔安区', null, '118.25205', '24.583194', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6939', '洋唐居住区保障性安居房B13', '福建省', '厦门市', '翔安区', null, '118.25205', '24.583194', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6940', '中澳城(别墅)', '福建省', '厦门市', '翔安区', null, '118.253841', '24.738944', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6941', '海西舜弘自在城', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:03', null);
INSERT INTO `t_plot` VALUES ('6942', '泰禾红门', '福建省', '厦门市', '翔安区', null, '118.252132', '24.613783', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6943', '首开万科白鹭郡(别墅)', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6944', '五华偶寓', '福建省', '厦门市', '翔安区', null, '118.248849', '24.623545', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6945', '联合博学园', '福建省', '厦门市', '翔安区', null, '118.247184', '24.600671', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6946', '国贸金门湾朗琴园(别墅)', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6947', '海西9号', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6948', '国贸金门湾朗琴园', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:00', null, '2018-01-04 19:32:04', null);
INSERT INTO `t_plot` VALUES ('6949', '中骏蓝湾尚都', '福建省', '厦门市', '翔安区', null, '118.144769', '24.539658', '2018-01-03 20:48:00', null, '2018-01-04 19:32:05', null);
INSERT INTO `t_plot` VALUES ('6950', '特房黎安小镇一期', '福建省', '厦门市', '翔安区', null, '118.250758', '24.667339', '2018-01-03 20:48:00', null, '2018-01-04 19:32:05', null);
INSERT INTO `t_plot` VALUES ('6951', '东城合院', '福建省', '厦门市', '翔安区', null, '118.254448', '24.627002', '2018-01-03 20:48:00', null, '2018-01-04 19:32:05', null);
INSERT INTO `t_plot` VALUES ('6952', '美地雅登祥吴六里', '福建省', '厦门市', '翔安区', null, '118.254801', '24.634421', '2018-01-03 20:48:00', null, '2018-01-04 19:32:05', null);
INSERT INTO `t_plot` VALUES ('6953', '世茂御海墅(别墅)', '福建省', '厦门市', '翔安区', null, '118.249312', '24.549841', '2018-01-03 20:48:00', null, '2018-01-04 19:32:05', null);
INSERT INTO `t_plot` VALUES ('6954', '龙翔峰景', '福建省', '厦门市', '翔安区', null, '118.261118', '24.667006', '2018-01-03 20:48:00', null, '2018-01-04 19:32:05', null);
INSERT INTO `t_plot` VALUES ('6955', '信洲国际', '福建省', '厦门市', '翔安区', null, '118.246307', '24.573632', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6956', '桐梓新村', '福建省', '厦门市', '翔安区', null, '118.251369', '24.679049', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6957', '国贸金门湾日光海', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6958', '首开领翔花郡', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6959', '都会99小区', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6960', '宝嘉拉德芳斯', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6961', '国贸金门湾星怡轩', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:00', null, '2018-01-04 19:32:06', null);
INSERT INTO `t_plot` VALUES ('6962', '明发半岛祥湾B区', '福建省', '厦门市', '翔安区', null, '118.239042', '24.570482', '2018-01-03 20:48:00', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6963', '美地雅登祥吴四里', '福建省', '厦门市', '翔安区', null, '118.252995', '24.631435', '2018-01-03 20:48:00', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6964', '汇景新城三期', '福建省', '厦门市', '翔安区', null, '118.25642', '24.624498', '2018-01-03 20:48:00', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6965', '舫春楼', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:00', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6966', '翔安商业广场一期', '福建省', '厦门市', '翔安区', null, '118.259436', '24.668849', '2018-01-03 20:48:00', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6967', '翔城国际', '福建省', '厦门市', '翔安区', null, '118.2505', '24.578689', '2018-01-03 20:48:01', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6968', '招商雍华府2', '福建省', '厦门市', '翔安区', null, '118.251239', '24.620279', '2018-01-03 20:48:01', null, '2018-01-04 19:32:07', null);
INSERT INTO `t_plot` VALUES ('6969', '新城安置四组团', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6970', '首开花郡', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6971', '明发半岛祥湾A区', '福建省', '厦门市', '翔安区', null, '118.239042', '24.570482', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6972', '海峡商贸城', '福建省', '厦门市', '翔安区', null, '118.25447', '24.676032', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6973', '海翼0592', '福建省', '厦门市', '翔安区', null, '118.252774', '24.677861', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6974', '龙郡青年城', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6975', '阳光城翡丽湾二期', '福建省', '厦门市', '翔安区', null, '118.234864', '24.565143', '2018-01-03 20:48:01', null, '2018-01-04 19:32:08', null);
INSERT INTO `t_plot` VALUES ('6976', '万科金色悦城', '福建省', '厦门市', '翔安区', null, '118.280803', '24.675485', '2018-01-03 20:48:01', null, '2018-01-04 19:32:09', null);
INSERT INTO `t_plot` VALUES ('6977', '莲花尚城', '福建省', '厦门市', '翔安区', null, '118.249456', '24.605308', '2018-01-03 20:48:01', null, '2018-01-04 19:32:09', null);
INSERT INTO `t_plot` VALUES ('6978', '国贸金门湾大德沙(别墅)', '福建省', '厦门市', '翔安区', null, '118.323397', '24.558591', '2018-01-03 20:48:01', null, '2018-01-04 19:32:09', null);
INSERT INTO `t_plot` VALUES ('6979', '中澳城', '福建省', '厦门市', '翔安区', null, '118.253841', '24.738944', '2018-01-03 20:48:01', null, '2018-01-04 19:32:09', null);

-- ----------------------------
-- Table structure for t_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_privilege`;
CREATE TABLE `t_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7870 DEFAULT CHARSET=utf8 COMMENT='角色权限关系表';

-- ----------------------------
-- Records of t_privilege
-- ----------------------------
INSERT INTO `t_privilege` VALUES ('5652', '10', '98');
INSERT INTO `t_privilege` VALUES ('5653', '10', '99');
INSERT INTO `t_privilege` VALUES ('5654', '10', '102');
INSERT INTO `t_privilege` VALUES ('5655', '10', '112');
INSERT INTO `t_privilege` VALUES ('5656', '10', '103');
INSERT INTO `t_privilege` VALUES ('5657', '10', '104');
INSERT INTO `t_privilege` VALUES ('5658', '10', '114');
INSERT INTO `t_privilege` VALUES ('5659', '10', '115');
INSERT INTO `t_privilege` VALUES ('5660', '10', '23');
INSERT INTO `t_privilege` VALUES ('5661', '10', '108');
INSERT INTO `t_privilege` VALUES ('5662', '10', '110');
INSERT INTO `t_privilege` VALUES ('5663', '10', '24');
INSERT INTO `t_privilege` VALUES ('5664', '10', '105');
INSERT INTO `t_privilege` VALUES ('7841', '1', '147');
INSERT INTO `t_privilege` VALUES ('7842', '1', '11');
INSERT INTO `t_privilege` VALUES ('7843', '1', '14');
INSERT INTO `t_privilege` VALUES ('7844', '1', '18');
INSERT INTO `t_privilege` VALUES ('7845', '1', '131');
INSERT INTO `t_privilege` VALUES ('7846', '1', '151');
INSERT INTO `t_privilege` VALUES ('7847', '1', '152');
INSERT INTO `t_privilege` VALUES ('7848', '1', '157');
INSERT INTO `t_privilege` VALUES ('7849', '1', '1');
INSERT INTO `t_privilege` VALUES ('7850', '1', '97');
INSERT INTO `t_privilege` VALUES ('7851', '1', '3');
INSERT INTO `t_privilege` VALUES ('7852', '1', '52');
INSERT INTO `t_privilege` VALUES ('7853', '1', '55');
INSERT INTO `t_privilege` VALUES ('7854', '1', '56');
INSERT INTO `t_privilege` VALUES ('7855', '1', '4');
INSERT INTO `t_privilege` VALUES ('7856', '1', '58');
INSERT INTO `t_privilege` VALUES ('7857', '1', '59');
INSERT INTO `t_privilege` VALUES ('7858', '1', '60');
INSERT INTO `t_privilege` VALUES ('7859', '1', '2');
INSERT INTO `t_privilege` VALUES ('7860', '1', '61');
INSERT INTO `t_privilege` VALUES ('7861', '1', '62');
INSERT INTO `t_privilege` VALUES ('7862', '1', '63');
INSERT INTO `t_privilege` VALUES ('7863', '1', '36');
INSERT INTO `t_privilege` VALUES ('7864', '1', '49');
INSERT INTO `t_privilege` VALUES ('7865', '1', '51');
INSERT INTO `t_privilege` VALUES ('7866', '1', '95');
INSERT INTO `t_privilege` VALUES ('7867', '14', '0');
INSERT INTO `t_privilege` VALUES ('7868', '15', '0');
INSERT INTO `t_privilege` VALUES ('7869', '16', '0');

-- ----------------------------
-- Table structure for t_product
-- ----------------------------
DROP TABLE IF EXISTS `t_product`;
CREATE TABLE `t_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `name` varchar(45) CHARACTER SET utf8 NOT NULL COMMENT '商品名称',
  `introduce` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品简介',
  `productDetail` varchar(10000) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品详情',
  `picture` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品主图',
  `status` int(11) DEFAULT '0' COMMENT '商品状态（0上架、1下架）',
  `images` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品图片',
  `catalogID` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '类别ID',
  `merchantID` varchar(255) CHARACTER SET utf8 DEFAULT '0' COMMENT '商家ID',
  `isSelf` varchar(11) CHARACTER SET utf8 DEFAULT 'n' COMMENT '是否自营（y是、n否）',
  `sellcount` int(11) DEFAULT '0' COMMENT '商品销售量',
  `startDate` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '开团日期',
  `sendDate` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '送货时间',
  `area` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '地区',
  `garden` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '所在小区',
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '自提点',
  `linkMan` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '联系人',
  `linkPhone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '联系电话',
  `checkStatus` varchar(5) CHARACTER SET utf8 DEFAULT '1' COMMENT '审核状态（1未审核、2通过、3不通过、4开团中、5已结束）',
  `reason` varchar(1000) CHARACTER SET utf8 DEFAULT NULL COMMENT '审核理由',
  `background` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '海报',
  `firstPercent` decimal(11,2) DEFAULT '0.05' COMMENT '第一单返利',
  `secondPercent` decimal(11,2) DEFAULT '0.15' COMMENT '第二单返利',
  `thirdPercent` decimal(11,2) DEFAULT '0.20' COMMENT '第三单返利',
  `createTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  KEY `product_name` (`name`) USING BTREE,
  KEY `product_catalog` (`catalogID`) USING BTREE,
  KEY `product_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10525 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='商品表';

-- ----------------------------
-- Records of t_product
-- ----------------------------
INSERT INTO `t_product` VALUES ('10488', '团圆伴手礼', '团圆伴手礼', '<p><img src=\"/ueditor/upload/image/20180117/1516169911594029509.jpg\" title=\"1516169911594029509.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169914574004399.jpg\" title=\"1516169914574004399.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169917591020627.jpg\" title=\"1516169917591020627.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169921102077831.jpg\" title=\"1516169921102077831.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169924177053546.jpg\" title=\"1516169924177053546.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169986656010509.jpg\" title=\"1516169986656010509.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169989642078509.jpg\" title=\"1516169989642078509.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169992296057359.jpg\" title=\"1516169992296057359.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169994950011450.jpg\" title=\"1516169994950011450.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169998785086305.jpg\" title=\"1516169998785086305.jpg\" alt=\"11.jpg\"/></p>', 'ueditor/upload/image/20180116/1516115855709038363.jpg', '1', 'ueditor/upload/image/20180116/1516115868680034712.jpg,ueditor/upload/image/20180116/1516115868884072148.jpg,ueditor/upload/image/20180116/1516115870013008333.jpg,ueditor/upload/image/20180116/1516115872575089566.jpg,ueditor/upload/image/20180116/1516115873017051188.jpg', null, '0', 'y', '0', '2018-01-18 23:23:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', '张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516115855709038363.jpg', null, null, null, '2018-01-16 23:18:24', null, '2018-03-19 14:51:55', null);
INSERT INTO `t_product` VALUES ('10489', '鹭岛尚品坚果6罐装', '鹭岛尚品坚果6罐装', '<p><img src=\"/ueditor/upload/image/20180117/1516169813904096309.jpg\" title=\"1516169813904096309.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169818450070188.jpg\" title=\"1516169818450070188.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169821626064437.jpg\" title=\"1516169821626064437.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169825315066123.jpg\" title=\"1516169825315066123.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169828397038443.jpg\" title=\"1516169828397038443.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169831496055001.jpg\" title=\"1516169831496055001.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169834356082465.jpg\" title=\"1516169834356082465.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169837131000208.jpg\" title=\"1516169837131000208.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169840679064725.jpg\" title=\"1516169840679064725.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169843644052522.jpg\" title=\"1516169843644052522.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169847441079217.jpg\" title=\"1516169847441079217.jpg\" alt=\"11.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169851741002565.jpg\" title=\"1516169851741002565.jpg\" alt=\"12.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169856970082656.jpg\" title=\"1516169856970082656.jpg\" alt=\"13.jpg\"/></p>', 'ueditor/upload/image/20180116/1516116337647091371.jpg', '1', 'ueditor/upload/image/20180116/1516116359391047379.jpg,ueditor/upload/image/20180116/1516116361891073902.jpg,ueditor/upload/image/20180116/1516116362204049265.jpg,ueditor/upload/image/20180116/1516116365242098737.jpg,ueditor/upload/image/20180116/1516116365848020699.jpg', null, '0', 'y', '0', '2018-01-18 23:31:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', ' 张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516116337647091371.jpg', null, null, null, '2018-01-16 23:26:54', null, '2018-03-19 14:51:54', null);
INSERT INTO `t_product` VALUES ('10490', '难吃瓜子焦糖味桶装', '难吃瓜子焦糖味桶装', '<p><img src=\"/ueditor/upload/image/20180117/1516169696766068723.jpg\" title=\"1516169696766068723.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169700006001793.jpg\" title=\"1516169700006001793.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169704511054766.jpg\" title=\"1516169704511054766.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169708738081443.jpg\" title=\"1516169708738081443.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169713237031012.jpg\" title=\"1516169713237031012.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169716917024595.jpg\" title=\"1516169716917024595.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169720096045712.jpg\" title=\"1516169720096045712.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169724279020222.jpg\" title=\"1516169724279020222.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169726218060857.jpg\" title=\"1516169726218060857.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169728895068693.jpg\" title=\"1516169728895068693.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169732456025162.jpg\" title=\"1516169732456025162.jpg\" alt=\"11.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169736720056404.jpg\" title=\"1516169736720056404.jpg\" alt=\"12.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169741200091381.jpg\" title=\"1516169741200091381.jpg\" alt=\"13.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169745396077226.jpg\" title=\"1516169745396077226.jpg\" alt=\"14.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169749757022403.jpg\" title=\"1516169749757022403.jpg\" alt=\"15.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169753906049575.jpg\" title=\"1516169753906049575.jpg\" alt=\"16.jpg\"/></p>', 'ueditor/upload/image/20180116/1516116572989044195.jpg', '1', 'ueditor/upload/image/20180116/1516116587303048299.jpg,ueditor/upload/image/20180116/1516116590452099549.jpg,ueditor/upload/image/20180116/1516116590698078818.jpg,ueditor/upload/image/20180116/1516116591173078020.jpg', null, '0', 'y', '0', '2018-01-18 23:35:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', '张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516116572989044195.jpg', null, null, null, '2018-01-16 23:30:45', null, '2018-03-23 09:49:58', null);
INSERT INTO `t_product` VALUES ('10491', '难吃瓜子核桃味桶装', '难吃瓜子核桃味桶装', '<p><img src=\"/ueditor/upload/image/20180117/1516169603880005729.jpg\" title=\"1516169603880005729.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169606174058255.jpg\" title=\"1516169606174058255.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169609162011168.jpg\" title=\"1516169609162011168.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169612626031703.jpg\" title=\"1516169612626031703.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169615619024954.jpg\" title=\"1516169615619024954.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169620047041171.jpg\" title=\"1516169620047041171.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169623245076673.jpg\" title=\"1516169623245076673.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169626135032454.jpg\" title=\"1516169626135032454.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169629426099487.jpg\" title=\"1516169629426099487.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169632573073272.jpg\" title=\"1516169632573073272.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169636435043673.jpg\" title=\"1516169636435043673.jpg\" alt=\"11.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169641361017473.jpg\" title=\"1516169641361017473.jpg\" alt=\"12.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169645682069821.jpg\" title=\"1516169645682069821.jpg\" alt=\"13.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169650289072847.jpg\" title=\"1516169650289072847.jpg\" alt=\"14.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169654936052841.jpg\" title=\"1516169654936052841.jpg\" alt=\"15.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169659719095602.jpg\" title=\"1516169659719095602.jpg\" alt=\"16.jpg\"/></p>', 'ueditor/upload/image/20180116/1516116712571099423.jpg', '1', 'ueditor/upload/image/20180116/1516116732296040799.jpg,ueditor/upload/image/20180116/1516116733591010422.jpg,ueditor/upload/image/20180116/1516116734940080074.jpg,ueditor/upload/image/20180116/1516116736990032676.jpg', null, '0', 'y', '0', '2018-01-18 23:37:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', '张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516116712571099423.jpg', null, null, null, '2018-01-16 23:32:48', null, '2018-03-23 09:49:24', null);
INSERT INTO `t_product` VALUES ('10492', '西瓜子桶装', '西瓜子桶装', '<p><img src=\"/ueditor/upload/image/20180117/1516169528218048082.jpg\" title=\"1516169528218048082.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169533056034687.jpg\" title=\"1516169533056034687.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169536151023459.jpg\" title=\"1516169536151023459.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169540214037403.jpg\" title=\"1516169540214037403.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169543706056338.jpg\" title=\"1516169543706056338.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169548662030480.jpg\" title=\"1516169548662030480.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169550551031568.jpg\" title=\"1516169550551031568.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169554349026207.jpg\" title=\"1516169554349026207.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169557311080159.jpg\" title=\"1516169557311080159.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169561073052526.jpg\" title=\"1516169561073052526.jpg\" alt=\"10.jpg\"/></p>', 'ueditor/upload/image/20180116/1516116821060004279.jpg', '1', 'ueditor/upload/image/20180116/1516116830103096569.jpg,ueditor/upload/image/20180116/1516116831373088531.jpg,ueditor/upload/image/20180116/1516116832401037507.jpg,ueditor/upload/image/20180116/1516116833229001801.jpg', null, '0', 'y', '0', '2018-01-18 23:39:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', '张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516116821060004279.jpg', null, null, null, '2018-01-16 23:35:03', null, '2018-03-23 15:38:39', null);
INSERT INTO `t_product` VALUES ('10493', '开心果桶装', '开心果桶装', '<p><img src=\"/ueditor/upload/image/20180117/1516169420069089504.jpg\" title=\"1516169420069089504.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169426641022971.jpg\" title=\"1516169426641022971.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169429335057425.jpg\" title=\"1516169429335057425.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169432831039173.jpg\" title=\"1516169432831039173.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169435441094009.jpg\" title=\"1516169435441094009.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169439687078172.jpg\" title=\"1516169439687078172.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169442701082848.jpg\" title=\"1516169442701082848.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169445663037820.jpg\" title=\"1516169445663037820.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169449294023943.jpg\" title=\"1516169449294023943.jpg\" alt=\"9.jpg\"/></p>', 'ueditor/upload/image/20180116/1516116953830057993.jpg', '1', 'ueditor/upload/image/20180116/1516116963097015257.jpg,ueditor/upload/image/20180116/1516116964013075539.jpg,ueditor/upload/image/20180116/1516116964106088825.jpg,ueditor/upload/image/20180116/1516116966119056666.jpg', null, '0', 'y', '0', '2018-01-18 23:41:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', '张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-01-16 23:36:48', null, '2018-03-19 14:51:50', null);
INSERT INTO `t_product` VALUES ('10494', '巴旦木桶装', '巴旦木桶装', '<p><img src=\"/ueditor/upload/image/20180117/1516169310795007091.jpg\" title=\"1516169310795007091.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169314614048317.jpg\" title=\"1516169314614048317.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169318089084829.jpg\" title=\"1516169318089084829.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169322089035150.jpg\" title=\"1516169322089035150.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169326915041743.jpg\" title=\"1516169326915041743.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169329974063559.jpg\" title=\"1516169329974063559.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169333056023981.jpg\" title=\"1516169333056023981.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169335973014784.jpg\" title=\"1516169335973014784.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169339498069495.jpg\" title=\"1516169339498069495.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180117/1516169343376031503.jpg\" title=\"1516169343376031503.jpg\" alt=\"10.jpg\"/></p>', 'ueditor/upload/image/20180116/1516117064018009501.jpg', '1', 'ueditor/upload/image/20180116/1516117073924041113.jpg,ueditor/upload/image/20180116/1516117073972077786.jpg,ueditor/upload/image/20180116/1516117074849032165.jpg,ueditor/upload/image/20180116/1516117074982080873.jpg', null, '0', 'y', '0', '2018-01-18 23:43:00', null, '福建省厦门市集美区', '联发杏林湾一号2期7号楼2403', '联发杏林湾一号2期7号楼2403', '张颖', '13696945705', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-01-16 23:38:23', null, '2018-03-19 14:51:47', null);
INSERT INTO `t_product` VALUES ('10502', '现摘云霄甜枇杷10斤', '由于生鲜水果特殊，不支持7天无理由退货，请不要拒收，如遇坏果请及时与卖家联系，我们对您的损失进行赔付。枇杷是新鲜采摘，直接从云霄送过来的，放一两天后更甜哟', '<p><img src=\"/ueditor/upload/image/20180327/1522112518791081258.jpg\" title=\"1522112518791081258.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112522330029682.jpg\" title=\"1522112522330029682.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112527172092552.jpg\" title=\"1522112527172092552.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112532533012120.jpg\" title=\"1522112532533012120.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112535469001420.jpg\" title=\"1522112535469001420.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112538492065797.jpg\" title=\"1522112538492065797.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112541102004530.jpg\" title=\"1522112541102004530.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112544980079033.jpg\" title=\"1522112544980079033.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112547661083889.jpg\" title=\"1522112547661083889.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112550631063184.jpg\" title=\"1522112550631063184.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112553682073139.jpg\" title=\"1522112553682073139.jpg\" alt=\"11.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112557850006399.jpg\" title=\"1522112557850006399.jpg\" alt=\"12.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522112560062755708.jpg\" title=\"1522112560062755708.jpg\" alt=\"13.jpg\"/></p>', 'ueditor/upload/image/20180319/1521442462309098870.jpg', '0', 'ueditor/upload/image/20180319/1521442584034073235.jpg,ueditor/upload/image/20180319/1521442589909020983.jpg,ueditor/upload/image/20180319/1521442598684085619.jpg,ueditor/upload/image/20180319/1521442611535053020.jpg,ueditor/upload/image/20180319/1521442614104073806.jpg', null, '0', 'y', '53', '2018-04-03 00:59:00', null, '厦门市软件园二期观日路', '厦门市软件园二期观日路', '厦门市软件园二期观日路', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-19 15:09:13', null, '2018-04-01 01:16:03', null);
INSERT INTO `t_product` VALUES ('10503', '五分文6a桂圆干500g*2桂圆桂圆肉', '五分文6a桂圆干500g*2桂圆桂圆肉 干桂圆龙眼龙眼干 干龙眼非无核', '<p><img src=\"/ueditor/upload/image/20180323/1521766732026095938.jpg\" title=\"1521766732026095938.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766735033037369.jpg\" title=\"1521766735033037369.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766737795043418.jpg\" title=\"1521766737795043418.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766742066034909.jpg\" title=\"1521766742066034909.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766753235083502.jpg\" title=\"1521766753235083502.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766755974029012.jpg\" title=\"1521766755974029012.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766765116023013.jpg\" title=\"1521766765116023013.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521766768091090762.jpg\" title=\"1521766768091090762.jpg\" alt=\"7.jpg\"/><img class=\"loadingclass\" id=\"loading_jf38n6ti\" src=\"http://www.veomall.com/resource/base/ueditor/themes/default/images/spacer.gif\" title=\"正在上传...\"/></p>', 'ueditor/upload/image/20180323/1521766696927040001.jpg', '0', 'ueditor/upload/image/20180323/1521766783804084229.png,ueditor/upload/image/20180323/1521766785071029081.jpg,ueditor/upload/image/20180323/1521766786202065601.jpg,ueditor/upload/image/20180323/1521766789283010123.jpg', null, '0', 'y', '0', '2018-03-24 09:56:00', null, '软件园二期', '软件园二期', '软件园二期', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-23 09:00:43', null, '2018-03-25 08:46:00', null);
INSERT INTO `t_product` VALUES ('10504', '五分文桂圆肉无核桂圆干500g干桂圆罐装桂圆龙眼龙眼干龙眼肉批发', '五分文桂圆肉无核桂圆干500g干桂圆罐装桂圆龙眼龙眼干龙眼肉批发', '<p><img src=\"/ueditor/upload/image/20180323/1521767920824033016.jpg\" title=\"1521767920824033016.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767924653061830.jpg\" title=\"1521767924653061830.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767928196093294.jpg\" title=\"1521767928196093294.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767933397017821.jpg\" title=\"1521767933397017821.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767936675016387.jpg\" title=\"1521767936675016387.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767940075075582.jpg\" title=\"1521767940075075582.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767942786050809.jpg\" title=\"1521767942786050809.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767945534083937.jpg\" title=\"1521767945534083937.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767947885073529.jpg\" title=\"1521767947885073529.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767950955011247.jpg\" title=\"1521767950955011247.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767954016013750.jpg\" title=\"1521767954016013750.jpg\" alt=\"11.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767956617073709.jpg\" title=\"1521767956617073709.jpg\" alt=\"12.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767959447031125.jpg\" title=\"1521767959447031125.jpg\" alt=\"13.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767962627083389.jpg\" title=\"1521767962627083389.jpg\" alt=\"14.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767964735042307.jpg\" title=\"1521767964735042307.jpg\" alt=\"15.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521767966825043165.jpg\" title=\"1521767966825043165.jpg\" alt=\"16.jpg\"/></p>', 'ueditor/upload/image/20180323/1521767908865002050.jpg', '0', 'ueditor/upload/image/20180323/1521767976428075817.jpg,ueditor/upload/image/20180323/1521767979576053354.jpg,ueditor/upload/image/20180323/1521767979616049337.jpg', null, '0', 'y', '0', '2018-03-24 10:17:00', null, '软件园二期', '软件园二期', '软件园二期', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-23 09:21:16', null, '2018-03-25 08:46:00', null);
INSERT INTO `t_product` VALUES ('10505', '5a新货 莆田桂圆干500g*2袋 桂圆桂圆肉 龙眼龙眼干 干桂圆 批发', '5a新货 莆田桂圆干500g*2袋 桂圆桂圆肉 龙眼龙眼干 干桂圆 批发', '<p><img src=\"/ueditor/upload/image/20180323/1521769052475005832.jpg\" title=\"1521769052475005832.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769054644072807.jpg\" title=\"1521769054644072807.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769056674015326.jpg\" title=\"1521769056674015326.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769058626038103.jpg\" title=\"1521769058626038103.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769060025047662.jpg\" title=\"1521769060025047662.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769064396031476.jpg\" title=\"1521769064396031476.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769064396031476.jpg\" title=\"1521769064396031476.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769066453048283.jpg\" title=\"1521769066453048283.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769073487026484.jpg\" title=\"1521769073487026484.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769073487026484.jpg\" title=\"1521769073487026484.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769083885003801.jpg\" title=\"1521769083885003801.jpg\" alt=\"11.jpg\"/></p>', 'ueditor/upload/image/20180323/1521769042065010521.jpg', '0', 'ueditor/upload/image/20180323/1521769091287031372.jpg,ueditor/upload/image/20180323/1521769093164067078.jpg,ueditor/upload/image/20180323/1521769095005055597.jpg,ueditor/upload/image/20180323/1521769098207022558.jpg', null, '0', 'y', '0', '2018-03-24 10:36:00', null, '软件园二期', '软件园二期', '软件园二期', '吴合利', '13696943515', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpgueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-23 09:38:43', null, '2018-03-25 08:46:00', null);
INSERT INTO `t_product` VALUES ('10506', '6a莆田桂圆干500g龙眼干干货 桂圆肉桂圆龙眼龙眼肉 干桂圆零食', '6a莆田桂圆干500g龙眼干干货 桂圆肉桂圆龙眼龙眼肉 干桂圆零食', '<p><img src=\"/ueditor/upload/image/20180323/1521769415013011736.jpg\" title=\"1521769415013011736.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769417060032593.jpg\" title=\"1521769417060032593.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769419231005968.jpg\" title=\"1521769419231005968.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769422932058017.jpg\" title=\"1521769422932058017.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769422932058017.jpg\" title=\"1521769422932058017.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769426809009207.jpg\" title=\"1521769426809009207.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769426809009207.jpg\" title=\"1521769426809009207.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180323/1521769428848031211.jpg\" title=\"1521769428848031211.jpg\" alt=\"8.jpg\"/></p>', 'ueditor/upload/image/20180323/1521769407410034741.jpg', '0', 'ueditor/upload/image/20180323/1521769436191009049.jpg,ueditor/upload/image/20180323/1521769450718032423.jpg,ueditor/upload/image/20180323/1521769452999010699.png', null, '0', 'y', '2', '2018-03-24 10:42:00', null, '软件园二期', '软件园二期', '软件园二期', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-23 09:44:46', null, '2018-03-27 13:52:07', null);
INSERT INTO `t_product` VALUES ('10512', '现摘云霄甜枇杷5斤装 ', '下周一统一发货\r\n由于生鲜水果特殊，不支持7天无理由退货，请不要拒收，如遇坏果请及时与卖家联系，我们对您的损失进行赔付。', '<p><img src=\"/ueditor/upload/image/20180319/1521442525982095825.jpg\" title=\"1521442525982095825.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442529843021013.jpg\" title=\"1521442529843021013.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442532754043442.jpg\" title=\"1521442532754043442.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442536404013111.jpg\" title=\"1521442536404013111.jpg\" alt=\"4.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442540164053001.jpg\" title=\"1521442540164053001.jpg\" alt=\"5.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442543134062936.jpg\" title=\"1521442543134062936.jpg\" alt=\"6.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442545794095039.jpg\" title=\"1521442545794095039.jpg\" alt=\"7.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442548794089423.jpg\" title=\"1521442548794089423.jpg\" alt=\"8.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442551504036403.jpg\" title=\"1521442551504036403.jpg\" alt=\"9.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442553981028582.jpg\" title=\"1521442553981028582.jpg\" alt=\"10.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442556365061320.jpg\" title=\"1521442556365061320.jpg\" alt=\"11.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442558672044740.jpg\" title=\"1521442558672044740.jpg\" alt=\"12.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442561093073858.jpg\" title=\"1521442561093073858.jpg\" alt=\"13.jpg\"/><img src=\"/ueditor/upload/image/20180319/1521442563643017592.jpg\" title=\"1521442563643017592.jpg\" alt=\"14.jpg\"/></p>', 'ueditor/upload/image/20180323/1521792143106032919.jpg', '0', 'ueditor/upload/image/20180323/1521792175544055131.jpg,ueditor/upload/image/20180323/1521792180332042721.jpg,ueditor/upload/image/20180323/1521792183002096709.jpg,ueditor/upload/image/20180323/1521792185662053200.jpg,ueditor/upload/image/20180323/1521792190142075673.jpg', null, '0', 'y', '0', '2018-03-31 18:01:00', null, '厦门市软件园二期观日路', '厦门市软件园二期观日路', '厦门市软件园二期观日路', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-19 16:04:22', null, '2018-04-01 12:57:00', null);
INSERT INTO `t_product` VALUES ('10513', '云霄农家熬制枇杷膏纯手工琵琶膏', '纯手工制作，大人小孩孕妇都可以吃.无添加', '<p><img src=\"/ueditor/upload/image/20180327/1522113010435065566.jpg\" title=\"1522113010435065566.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522113013163052099.jpg\" title=\"1522113013163052099.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522113015247082647.jpg\" title=\"1522113015247082647.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522113016895070358.jpg\" title=\"1522113016895070358.jpg\" alt=\"4.jpg\"/></p>', 'ueditor/upload/image/20180327/1522125731746093010.jpg', '0', 'ueditor/upload/image/20180327/1522125738972097947.jpg,ueditor/upload/image/20180327/1522125743680097865.jpg,ueditor/upload/image/20180327/1522125745780069095.jpg,ueditor/upload/image/20180327/1522125748585014852.jpg,ueditor/upload/image/20180327/1522125750610099665.jpg', null, '0', 'y', '0', '2018-03-31 16:31:00', null, '厦门市软件园二期', '厦门市软件园二期', '厦门市软件园二期', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-19 15:12:45', null, '2018-04-01 12:57:00', null);
INSERT INTO `t_product` VALUES ('10515', '云霄农家熬制枇杷膏纯手工琵琶膏1瓶', '纯手工制作，大人小孩孕妇都可以吃.无添加', '<p><img src=\"/ueditor/upload/image/20180327/1522113010435065566.jpg\" title=\"1522113010435065566.jpg\" alt=\"1.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522113013163052099.jpg\" title=\"1522113013163052099.jpg\" alt=\"2.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522113015247082647.jpg\" title=\"1522113015247082647.jpg\" alt=\"3.jpg\"/><img src=\"/ueditor/upload/image/20180327/1522113016895070358.jpg\" title=\"1522113016895070358.jpg\" alt=\"4.jpg\"/></p>', 'ueditor/upload/image/20180327/1522125857193074331.jpg', '0', 'ueditor/upload/image/20180327/1522125925462049999.jpg,ueditor/upload/image/20180327/1522125927286008426.jpg,ueditor/upload/image/20180327/1522125929220032662.jpg,ueditor/upload/image/20180327/1522125930962051146.jpg,ueditor/upload/image/20180327/1522125932733008791.jpg', null, '0', 'y', '0', '2018-03-28 16:45:00', null, '软件园二期', '软件园二期', '软件园二期', '吴合利', '13696945715', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-03-19 12:07:15', null, '2018-03-29 16:44:00', null);
INSERT INTO `t_product` VALUES ('10518', '测试专用商品（勿删）', '测试专用', '<p>旺仔牛奶</p>', 'ueditor/upload/image/20180405/1522942444619012354.jpg', '0', 'ueditor/upload/image/20180405/1522942477335082610.jpg', null, '0', 'y', '4', '2018-06-09 11:32:00', null, '厦门市思明区', '阳光小区', '101号', '吴', '15711593315', '4', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-04-05 23:35:02', null, '2018-06-02 21:34:48', null);
INSERT INTO `t_product` VALUES ('10519', '测试1', '简单机械不对劲', null, null, '0', 'ueditor/upload/image/20180502/1525232118337051251.jpg', null, '270', 'n', '1', '2018-05-02 11:34', null, '陕西 渭南 潼关县', '不会v看：不吃v）给你们', '大家互相', '怒不会', '18850458924', '5', null, 'ueditor/upload/image/20180116/1516116953830057993.jpgueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-05-02 11:35:29', null, '2018-05-08 10:01:47', null);
INSERT INTO `t_product` VALUES ('10521', '测试', '测试', null, null, '0', 'ueditor/upload/image/20180502/1525242058745032016.jpg', null, '280', 'n', '2', '2018-05-02 14:20', null, '北京 东城区 ', '头有些505', '测试', '林志伟', '15980808080', '5', null, 'ueditor/upload/image/20180116/1516116953830057993.jpg', null, null, null, '2018-05-02 14:21:17', null, '2018-05-03 14:21:00', null);
INSERT INTO `t_product` VALUES ('10524', 'qq', 'qq', '<p>wwww</p>', 'ueditor/upload/image/20180614/1528978962043072738.jpg', '0', 'ueditor/upload/image/20180614/1528978967039055121.jpg', null, '0', 'n', '0', null, null, 'qq', 'qq', 'qq', 'qq', '15711593316', '1', null, 'ueditor/upload/image/20180614/1528978959623058194.jpg', '5.00', '10.00', '20.00', '2018-06-14 20:23:12', null, '2018-06-14 20:23:23', null);

-- ----------------------------
-- Table structure for t_productuser
-- ----------------------------
DROP TABLE IF EXISTS `t_productuser`;
CREATE TABLE `t_productuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `userID` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户ID',
  `pid` varchar(11) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '上级ID',
  `productID` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品ID',
  `rank` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '返利等级',
  `createTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `updateUser` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户商品返利表';

-- ----------------------------
-- Records of t_productuser
-- ----------------------------
INSERT INTO `t_productuser` VALUES ('299', '297', '0', '10515', '1', '2018-06-19 15:51:34', null, null, null);
INSERT INTO `t_productuser` VALUES ('300', '409', '297', '10515', '1', '2018-06-19 15:51:34', null, null, null);

-- ----------------------------
-- Table structure for t_record
-- ----------------------------
DROP TABLE IF EXISTS `t_record`;
CREATE TABLE `t_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userID` varchar(11) DEFAULT NULL COMMENT '用户ID',
  `transType` varchar(4) DEFAULT NULL COMMENT '交易类型（0提现、1卖出商品、2返利、3抵扣）',
  `transID` varchar(11) DEFAULT NULL COMMENT '交易类型ID',
  `money` decimal(11,2) DEFAULT NULL COMMENT '金额',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COMMENT='用户交易记录表';

-- ----------------------------
-- Records of t_record
-- ----------------------------
INSERT INTO `t_record` VALUES ('80', '297', '3', null, null, '2018-03-27 13:47:22', null, null, null);
INSERT INTO `t_record` VALUES ('81', '280', '1', null, '0.01', '2018-03-27 13:47:22', null, null, null);
INSERT INTO `t_record` VALUES ('82', '297', '3', null, null, '2018-03-27 13:49:53', null, null, null);
INSERT INTO `t_record` VALUES ('83', '280', '1', null, '0.01', '2018-03-27 13:49:53', null, null, null);
INSERT INTO `t_record` VALUES ('84', '297', '3', null, null, '2018-03-27 13:50:31', null, null, null);
INSERT INTO `t_record` VALUES ('85', '297', '3', null, null, '2018-03-27 13:52:07', null, null, null);
INSERT INTO `t_record` VALUES ('86', '318', '3', null, null, '2018-03-27 13:55:59', null, null, null);
INSERT INTO `t_record` VALUES ('87', '316', '3', null, null, '2018-03-27 13:58:02', null, null, null);
INSERT INTO `t_record` VALUES ('88', '320', '3', null, null, '2018-03-27 14:04:19', null, null, null);
INSERT INTO `t_record` VALUES ('89', '321', '3', null, null, '2018-03-27 14:10:00', null, null, null);
INSERT INTO `t_record` VALUES ('90', '308', '3', null, null, '2018-03-27 14:16:44', null, null, null);
INSERT INTO `t_record` VALUES ('91', '327', '3', null, null, '2018-03-27 14:17:21', null, null, null);
INSERT INTO `t_record` VALUES ('92', '325', '3', null, null, '2018-03-27 14:18:05', null, null, null);
INSERT INTO `t_record` VALUES ('93', '326', '3', null, null, '2018-03-27 14:25:47', null, null, null);
INSERT INTO `t_record` VALUES ('94', '326', '3', null, null, '2018-03-27 14:26:57', null, null, null);
INSERT INTO `t_record` VALUES ('95', '329', '3', null, null, '2018-03-27 14:29:56', null, null, null);
INSERT INTO `t_record` VALUES ('96', '328', '3', null, null, '2018-03-27 14:30:57', null, null, null);
INSERT INTO `t_record` VALUES ('97', '332', '3', null, null, '2018-03-27 15:10:45', null, null, null);
INSERT INTO `t_record` VALUES ('98', '335', '3', null, null, '2018-03-27 15:48:45', null, null, null);
INSERT INTO `t_record` VALUES ('99', '334', '3', null, null, '2018-03-27 15:57:59', null, null, null);
INSERT INTO `t_record` VALUES ('100', '336', '3', null, null, '2018-03-27 16:28:08', null, null, null);
INSERT INTO `t_record` VALUES ('101', '338', '3', null, null, '2018-03-27 16:50:14', null, null, null);
INSERT INTO `t_record` VALUES ('102', '339', '3', null, null, '2018-03-27 16:54:39', null, null, null);
INSERT INTO `t_record` VALUES ('103', '314', '3', null, null, '2018-03-27 17:12:18', null, null, null);
INSERT INTO `t_record` VALUES ('104', '341', '3', null, null, '2018-03-27 17:46:36', null, null, null);
INSERT INTO `t_record` VALUES ('105', '342', '3', null, null, '2018-03-27 17:56:24', null, null, null);
INSERT INTO `t_record` VALUES ('106', '343', '3', null, null, '2018-03-27 19:05:58', null, null, null);
INSERT INTO `t_record` VALUES ('107', '298', '3', null, null, '2018-03-28 20:54:32', null, null, null);
INSERT INTO `t_record` VALUES ('108', '362', '3', null, null, '2018-03-29 14:18:15', null, null, null);
INSERT INTO `t_record` VALUES ('109', '361', '3', null, null, '2018-03-29 15:08:26', null, null, null);
INSERT INTO `t_record` VALUES ('110', '364', '3', null, null, '2018-03-29 15:50:13', null, null, null);
INSERT INTO `t_record` VALUES ('111', '347', '3', null, null, '2018-03-29 16:15:40', null, null, null);
INSERT INTO `t_record` VALUES ('112', '371', '3', null, null, '2018-03-29 16:26:21', null, null, null);
INSERT INTO `t_record` VALUES ('113', '346', '3', null, null, '2018-03-29 16:35:03', null, null, null);
INSERT INTO `t_record` VALUES ('114', '373', '3', null, null, '2018-03-29 16:37:21', null, null, null);
INSERT INTO `t_record` VALUES ('115', '365', '3', null, null, '2018-03-29 16:37:35', null, null, null);
INSERT INTO `t_record` VALUES ('116', '374', '3', null, null, '2018-03-29 16:38:09', null, null, null);
INSERT INTO `t_record` VALUES ('117', '310', '3', null, null, '2018-03-29 17:02:29', null, null, null);
INSERT INTO `t_record` VALUES ('118', '370', '3', null, null, '2018-03-29 17:16:33', null, null, null);
INSERT INTO `t_record` VALUES ('119', '375', '3', null, null, '2018-03-29 17:21:26', null, null, null);
INSERT INTO `t_record` VALUES ('120', '344', '3', null, null, '2018-03-29 23:14:53', null, null, null);
INSERT INTO `t_record` VALUES ('121', '368', '3', null, null, '2018-03-30 01:21:39', null, null, null);
INSERT INTO `t_record` VALUES ('122', '386', '3', null, null, '2018-03-30 09:43:52', null, null, null);
INSERT INTO `t_record` VALUES ('123', '298', '3', null, null, '2018-03-30 09:57:31', null, null, null);
INSERT INTO `t_record` VALUES ('124', '237', '3', null, null, '2018-03-30 10:32:59', null, null, null);
INSERT INTO `t_record` VALUES ('125', '388', '3', null, null, '2018-03-30 10:50:13', null, null, null);
INSERT INTO `t_record` VALUES ('126', '353', '3', null, null, '2018-03-30 11:55:45', null, null, null);
INSERT INTO `t_record` VALUES ('127', '237', '3', null, null, '2018-03-30 11:59:38', null, null, null);
INSERT INTO `t_record` VALUES ('128', '335', '3', null, null, '2018-03-30 14:05:24', null, null, null);
INSERT INTO `t_record` VALUES ('129', '390', '3', null, null, '2018-03-30 15:59:07', null, null, null);
INSERT INTO `t_record` VALUES ('130', '325', '3', null, null, '2018-03-30 21:33:23', null, null, null);
INSERT INTO `t_record` VALUES ('131', '339', '3', null, null, '2018-04-01 01:13:34', null, null, null);
INSERT INTO `t_record` VALUES ('132', '297', '3', null, null, '2018-04-05 23:35:34', null, null, null);
INSERT INTO `t_record` VALUES ('133', '268', '3', null, null, '2018-05-02 13:44:02', null, null, null);
INSERT INTO `t_record` VALUES ('134', '270', '1', null, '0.01', '2018-05-02 13:44:02', null, null, null);
INSERT INTO `t_record` VALUES ('135', '268', '3', null, null, '2018-05-02 14:19:08', null, null, null);
INSERT INTO `t_record` VALUES ('136', '280', '1', null, '0.01', '2018-05-02 14:19:08', null, null, null);
INSERT INTO `t_record` VALUES ('137', '280', '3', null, null, '2018-05-02 14:31:34', null, null, null);
INSERT INTO `t_record` VALUES ('138', '268', '3', null, null, '2018-05-02 14:32:12', null, null, null);
INSERT INTO `t_record` VALUES ('139', '274', '3', null, null, '2018-06-01 10:34:22', null, null, null);
INSERT INTO `t_record` VALUES ('140', '292', '2', null, null, '2018-06-01 10:34:22', null, null, null);
INSERT INTO `t_record` VALUES ('141', '274', '3', null, null, '2018-06-01 10:35:38', null, null, null);
INSERT INTO `t_record` VALUES ('142', '292', '2', null, '0.55', '2018-06-01 10:35:38', null, null, null);
INSERT INTO `t_record` VALUES ('143', '407', '3', null, null, '2018-06-02 21:34:48', null, null, null);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  `role_desc` varchar(45) DEFAULT NULL,
  `role_dbPrivilege` varchar(45) DEFAULT NULL,
  `status` varchar(2) DEFAULT 'y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name_UNIQUE` (`role_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '超级管理员', '系统角色，不可删除', 'select,insert,update,delete', 'y');
INSERT INTO `t_role` VALUES ('2', '自营账号', '自营账号', 'select', 'y');
INSERT INTO `t_role` VALUES ('3', '线下合伙人', '线下合伙人', 'select', 'y');
INSERT INTO `t_role` VALUES ('4', '普通用户', '普通用户', 'select', 'y');

-- ----------------------------
-- Table structure for t_spec
-- ----------------------------
DROP TABLE IF EXISTS `t_spec`;
CREATE TABLE `t_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品规格编号',
  `productID` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品编号',
  `specCombination` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品组合(用,隔开)',
  `specPurchasePrice` decimal(11,2) DEFAULT NULL COMMENT '规格进价',
  `specStock` int(11) DEFAULT NULL COMMENT '规格库存',
  `specNowPrice` decimal(10,2) DEFAULT NULL COMMENT '规格现价',
  `specGroupPrice` decimal(10,2) DEFAULT '0.00' COMMENT '团购价',
  `createTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='商品规格表';

-- ----------------------------
-- Records of t_spec
-- ----------------------------
INSERT INTO `t_spec` VALUES ('368', '10488', '团圆伴手礼', '198.00', '500', '138.00', '120.00', '2018-01-16 23:18:24', null, '2018-01-16 23:19:16', null);
INSERT INTO `t_spec` VALUES ('369', '10489', '鹭岛尚品坚果6罐装', '168.00', '500', '138.00', '120.00', '2018-01-16 23:26:54', null, null, null);
INSERT INTO `t_spec` VALUES ('370', '10490', '难吃瓜子焦糖味桶装', '46.80', '500', '35.80', '26.00', '2018-01-16 23:30:45', null, null, null);
INSERT INTO `t_spec` VALUES ('371', '10491', '难吃瓜子核桃味桶装', '46.80', '500', '35.80', '26.00', '2018-01-16 23:32:48', null, null, null);
INSERT INTO `t_spec` VALUES ('372', '10492', '西瓜子桶装', '69.80', '500', '45.80', '35.00', '2018-01-16 23:35:03', null, null, null);
INSERT INTO `t_spec` VALUES ('373', '10493', '开心果桶装', '95.80', '500', '69.90', '55.00', '2018-01-16 23:36:48', null, null, null);
INSERT INTO `t_spec` VALUES ('374', '10494', '巴旦木桶装', '88.00', '500', '69.90', '35.00', '2018-01-16 23:38:23', null, '2018-01-17 11:39:10', null);
INSERT INTO `t_spec` VALUES ('382', '10502', '10斤', '190.00', '165', '169.00', '130.00', '2018-03-19 15:09:13', null, '2018-04-05 23:21:38', null);
INSERT INTO `t_spec` VALUES ('384', '10503', '500g*2', '119.00', '200', '34.90', '32.90', '2018-03-23 09:00:43', null, null, null);
INSERT INTO `t_spec` VALUES ('385', '10504', '500g', '139.00', '200', '39.80', '36.80', '2018-03-23 09:21:16', null, null, null);
INSERT INTO `t_spec` VALUES ('386', '10505', '500g*2', '59.00', '200', '24.90', '22.80', '2018-03-23 09:38:43', null, null, null);
INSERT INTO `t_spec` VALUES ('387', '10506', '500g', '59.00', '188', '22.50', '19.80', '2018-03-23 09:44:46', null, '2018-03-27 13:52:07', null);
INSERT INTO `t_spec` VALUES ('395', '10512', '5斤', '95.00', '200', '75.00', '59.00', '2018-03-23 16:04:22', null, '2018-03-30 10:41:56', null);
INSERT INTO `t_spec` VALUES ('396', '10513', '两瓶', '120.00', '200', '110.00', '68.00', '2018-03-27 09:12:45', null, '2018-03-30 16:05:30', null);
INSERT INTO `t_spec` VALUES ('399', '10515', '1瓶', '60.00', '200', '55.00', '45.00', '2018-03-27 12:47:15', null, null, null);
INSERT INTO `t_spec` VALUES ('402', '10518', '瓶装', '0.01', '996', '0.01', '5.50', '2018-04-05 23:35:02', null, '2018-06-02 21:34:48', null);
INSERT INTO `t_spec` VALUES ('405', '10521', '测试', '0.01', '98', '0.01', '0.01', '2018-05-02 14:21:17', null, '2018-05-02 14:32:12', null);
INSERT INTO `t_spec` VALUES ('406', '10519', '你到家对不对', '0.01', '433', '0.01', '0.01', '2018-05-08 10:01:47', null, null, null);
INSERT INTO `t_spec` VALUES ('407', '10524', '1', '1.00', '1', '11.00', '0.00', '2018-06-14 20:23:12', null, null, null);

-- ----------------------------
-- Table structure for t_systemlog
-- ----------------------------
DROP TABLE IF EXISTS `t_systemlog`;
CREATE TABLE `t_systemlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` varchar(500) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `account` varchar(45) DEFAULT NULL,
  `loginIP` varchar(15) DEFAULT NULL,
  `logintime` varchar(50) DEFAULT NULL,
  `loginArea` varchar(45) DEFAULT NULL,
  `diffAreaLogin` char(1) DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4083 DEFAULT CHARSET=utf8 COMMENT='系统日志表';

-- ----------------------------
-- Records of t_systemlog
-- ----------------------------
INSERT INTO `t_systemlog` VALUES ('3395', 'login', 'login', '1', 'admin', '112.5.168.40', '2017-09-23 21:34:53', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3396', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-24 12:12:49', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3397', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 14:57:17', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3398', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-24 17:53:25', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3399', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-24 19:10:16', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3400', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:17:10', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3401', 'loginOut', 'loginOut', '1', 'admin', '117.30.208.99', '2017-09-24 20:17:14', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3402', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:17:18', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3403', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:24:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3404', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:30:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3405', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:34:36', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3406', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:48:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3407', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 20:59:13', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3408', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-24 21:02:19', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3409', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 10:12:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3410', 'login', 'login', '1', 'admin', '120.41.4.229', '2017-09-25 10:52:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3411', 'login', 'login', '1', 'admin', '120.41.4.229', '2017-09-25 13:51:06', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3412', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 14:30:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3413', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 14:42:03', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3414', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 14:49:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3415', 'loginOut', 'loginOut', '1', 'admin', '117.30.208.99', '2017-09-25 14:54:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3416', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 14:54:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3417', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 15:23:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3418', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 16:01:38', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3419', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 16:52:18', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3420', 'loginOut', 'loginOut', '1', 'admin', '117.30.208.99', '2017-09-25 16:53:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3421', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 16:53:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3422', 'login', 'login', '1', 'admin', '139.224.223.49', '2017-09-25 16:59:37', '上海市上海市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3423', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 17:15:28', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3424', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 17:17:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3425', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 19:20:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3426', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 19:23:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3427', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-25 19:32:55', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3428', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-25 20:32:26', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3429', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-25 21:18:24', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3430', 'login', 'login', '1', 'admin', '117.30.208.99', '2017-09-25 23:45:26', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3431', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-27 16:24:51', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3432', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-27 16:36:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3433', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-27 16:47:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3434', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-27 16:54:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3435', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-27 20:54:45', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3436', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 09:57:33', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3437', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 09:57:55', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3438', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 10:35:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3439', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 10:55:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3440', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 11:06:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3441', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 11:40:21', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3442', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 12:47:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3443', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 13:39:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3444', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 13:44:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3445', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 13:50:33', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3446', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 13:56:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3447', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:11:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3448', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:25:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3449', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:25:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3450', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:25:10', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3451', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:37:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3452', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:37:19', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3453', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:47:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3454', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 14:59:41', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3455', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 15:00:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3456', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 16:01:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3457', 'login', 'login', '1', 'admin', '139.224.223.49', '2017-09-28 16:05:47', '上海市上海市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3458', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 16:09:09', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3459', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 16:16:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3460', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 16:20:19', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3461', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 16:36:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3462', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 16:44:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3463', 'login', 'login', '1', 'admin', '139.224.223.49', '2017-09-28 17:02:01', '上海市上海市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3464', 'login', 'login', '1', 'admin', '139.224.223.49', '2017-09-28 17:21:17', '上海市上海市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3465', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 17:38:35', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3466', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 17:44:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3467', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-28 18:49:43', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3468', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 18:52:59', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3469', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 18:53:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3470', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 18:54:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3471', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 20:29:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3472', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-28 20:56:49', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3473', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 20:57:44', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3474', 'login', 'login', '1', 'admin', '112.5.168.111', '2017-09-28 21:12:17', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3475', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 22:53:25', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3476', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-28 23:13:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3477', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-29 09:31:57', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3478', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-29 09:32:41', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3479', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-29 10:01:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3480', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-29 10:05:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3481', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-29 10:40:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3482', 'login', 'login', '1', 'admin', '121.204.161.25', '2017-09-29 11:12:48', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3483', 'login', 'login', '1', 'admin', '127.0.0.1', '2017-12-15 14:47:20', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3484', 'login', 'login', '1', 'admin', '127.0.0.1', '2017-12-16 16:24:05', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3485', 'login', 'login', '1', 'admin', '112.5.168.7', '2017-12-16 17:10:14', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3486', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 17:10:23', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3487', 'loginOut', 'loginOut', '1', 'admin', '112.5.168.7', '2017-12-16 17:10:28', null, 'y');
INSERT INTO `t_systemlog` VALUES ('3488', 'loginOut', 'loginOut', '1', 'admin', '112.5.168.7', '2017-12-16 17:10:29', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3489', 'login', 'login', '1', 'admin', '112.5.168.7', '2017-12-16 17:32:02', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3490', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 17:47:06', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3491', 'loginOut', 'loginOut', '1', 'admin', '117.30.208.161', '2017-12-16 17:48:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3492', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 17:48:29', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3493', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 18:53:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3494', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 19:18:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3495', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 19:46:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3496', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 19:50:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3497', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-16 20:45:51', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3498', 'login', 'login', '1', 'admin', '112.5.168.7', '2017-12-16 21:59:30', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3499', 'login', 'login', '1', 'admin', '112.5.168.7', '2017-12-17 12:59:33', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3500', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 14:38:22', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3501', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 14:43:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3502', 'login', 'login', '1', 'admin', '112.5.168.7', '2017-12-17 17:44:55', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3503', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 18:58:42', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3504', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 19:07:21', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3505', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 19:11:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3506', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 19:14:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3507', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-17 20:50:00', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3508', 'login', 'login', '1', 'admin', '117.30.208.161', '2017-12-18 15:43:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3509', 'login', 'login', '1', 'admin', '47.94.169.210', '2017-12-18 16:00:09', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3510', 'login', 'login', '1', 'admin', '120.41.4.229', '2017-12-18 17:54:22', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3511', 'login', 'login', '1', 'admin', '120.41.4.229', '2017-12-18 18:07:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3512', 'login', 'login', '1', 'admin', '120.41.4.229', '2017-12-18 18:49:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3513', 'login', 'login', '1', 'admin', '47.94.169.210', '2017-12-19 10:01:41', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3514', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-19 14:21:12', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3515', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-19 14:39:08', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3516', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-19 14:43:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3517', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-19 18:03:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3518', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-19 19:15:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3519', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 10:33:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3520', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 15:31:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3521', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 19:15:48', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3522', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 19:17:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3523', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 19:41:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3524', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 20:35:45', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3525', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-20 20:46:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3526', 'login', 'login', '1', 'admin', '117.30.60.16', '2017-12-21 00:06:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3527', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 09:52:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3528', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 10:16:48', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3529', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 10:53:19', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3530', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 11:01:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3531', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 14:37:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3532', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 15:46:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3533', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 15:51:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3534', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 16:51:19', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3535', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 17:13:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3536', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 17:23:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3537', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 18:08:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3538', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 18:10:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3539', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 18:26:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3540', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 20:23:52', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3541', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 20:45:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3542', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 20:47:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3543', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 20:59:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3544', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 21:10:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3545', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 21:13:52', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3546', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 21:15:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3547', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 21:37:33', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3548', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 21:49:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3549', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:03:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3550', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:13:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3551', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:13:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3552', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:17:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3553', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:19:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3554', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:34:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3555', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:38:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3556', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:44:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3557', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:54:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3558', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 22:56:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3559', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 23:02:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3560', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 23:19:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3561', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-21 23:25:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3562', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 10:05:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3563', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:06:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3564', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 10:08:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3565', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:18:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3566', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:19:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3567', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:22:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3568', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:24:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3569', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:30:13', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3570', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:31:41', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3571', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:37:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3572', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 10:50:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3573', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 11:07:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3574', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 11:17:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3575', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 11:30:36', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3576', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 11:59:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3577', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 14:43:46', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3578', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 14:49:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3579', 'login', 'login', '1', 'admin', '47.94.169.210', '2017-12-22 15:15:52', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3580', 'loginOut', 'loginOut', '1', 'admin', '47.94.169.210', '2017-12-22 15:23:36', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3581', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 16:10:55', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3582', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 16:16:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3583', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 16:19:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3584', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 16:23:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3585', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 16:47:18', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3586', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 16:53:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3587', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 17:05:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3588', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 17:44:33', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3589', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 17:51:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3590', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 18:05:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3591', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 18:13:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3592', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 18:19:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3593', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 18:28:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3594', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 18:46:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3595', 'loginOut', 'loginOut', '1', 'admin', '117.28.113.76', '2017-12-22 19:28:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3596', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 19:28:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3597', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 19:30:08', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3598', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 19:33:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3599', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 19:38:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3600', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 19:41:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3601', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 19:47:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3602', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 19:54:06', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3603', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:00:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3604', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:07:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3605', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:10:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3606', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:12:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3607', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:16:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3608', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:24:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3609', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:33:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3610', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 20:50:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3611', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:02:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3612', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:14:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3613', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:18:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3614', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:20:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3615', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:25:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3616', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:29:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3617', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:31:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3618', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 21:51:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3619', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 22:04:52', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3620', 'login', 'login', '1', 'admin', '117.28.113.76', '2017-12-22 22:12:03', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3621', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-22 22:16:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3622', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 10:08:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3623', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 10:26:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3624', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 10:39:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3625', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 10:39:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3626', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 10:58:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3627', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 11:06:51', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3628', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 11:19:03', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3629', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 11:45:59', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3630', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 12:02:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3631', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 12:07:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3632', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 12:11:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3633', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 12:22:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3634', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 13:12:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3635', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 13:23:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3636', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 13:51:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3637', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 13:58:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3638', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:00:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3639', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:07:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3640', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:09:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3641', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:10:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3642', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:11:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3643', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:16:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3644', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:20:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3645', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:24:19', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3646', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:27:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3647', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:30:46', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3648', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:39:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3649', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:39:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3650', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:42:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3651', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:42:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3652', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:42:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3653', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:43:33', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3654', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:49:36', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3655', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 14:53:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3656', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 14:55:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3657', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 15:04:51', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3658', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 15:08:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3659', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 15:18:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3660', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 15:24:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3661', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 15:35:29', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3662', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 15:44:03', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3663', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 15:50:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3664', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 15:51:57', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3665', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:01:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3666', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:04:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3667', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:04:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3668', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:17:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3669', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:21:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3670', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:25:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3671', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 16:27:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3672', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:36:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3673', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 16:37:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3674', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:43:33', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3675', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 16:49:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3676', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:06:36', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3677', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:11:00', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3678', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:13:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3679', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:19:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3680', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:25:36', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3681', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 17:28:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3682', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:57:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3683', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 17:59:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3684', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 18:22:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3685', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 18:24:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3686', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 18:29:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3687', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 18:36:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3688', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 19:05:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3689', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 19:28:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3690', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 19:39:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3691', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 19:47:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3692', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 20:02:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3693', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 20:11:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3694', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 20:27:45', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3695', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 20:39:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3696', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 20:53:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3697', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 20:56:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3698', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 21:04:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3699', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 21:26:57', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3700', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 21:42:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3701', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-23 21:55:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3702', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 21:56:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3703', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 21:59:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3704', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 22:02:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3705', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 22:04:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3706', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-23 22:21:09', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3707', 'login', 'login', '1', 'admin', '223.104.6.59', '2017-12-24 01:53:45', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3708', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 10:15:08', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3709', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 10:25:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3710', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 10:34:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3711', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 10:42:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3712', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 10:57:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3713', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 11:05:36', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3714', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 11:06:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3715', 'loginOut', 'loginOut', '1', 'admin', '117.30.238.61', '2017-12-24 11:37:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3716', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 11:37:43', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3717', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 11:55:12', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3718', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 12:07:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3719', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 12:12:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3720', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 12:35:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3721', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 12:50:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3722', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 13:29:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3723', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 13:30:21', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3724', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 13:32:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3725', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 13:47:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3726', 'loginOut', 'loginOut', '1', 'admin', '117.30.238.61', '2017-12-24 13:51:35', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3727', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 13:51:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3728', 'loginOut', 'loginOut', '1', 'admin', '117.30.238.61', '2017-12-24 13:55:02', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3729', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 13:55:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3730', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 14:27:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3731', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 15:53:49', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3732', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 16:32:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3733', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 17:00:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3734', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 17:06:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3735', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 17:10:49', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3736', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 17:16:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3737', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 17:20:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3738', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 17:29:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3739', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 17:42:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3740', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 18:41:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3741', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 18:46:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3742', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 18:49:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3743', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 19:19:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3744', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 19:45:21', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3745', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 19:50:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3746', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 19:51:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3747', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 20:13:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3748', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 20:17:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3749', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 20:25:06', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3750', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 20:27:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3751', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 20:30:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3752', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 20:41:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3753', 'login', 'login', '1', 'admin', '117.30.238.61', '2017-12-24 20:44:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3754', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-24 21:09:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3755', 'login', 'login', '1', 'admin', '223.104.6.27', '2017-12-25 09:17:42', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3756', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 09:47:35', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3757', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 09:58:18', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3758', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 10:15:08', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3759', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 10:32:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3760', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 10:57:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3761', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:11:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3762', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:30:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3763', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:35:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3764', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:41:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3765', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:49:46', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3766', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:52:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3767', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:54:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3768', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 11:58:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3769', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 13:47:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3770', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 14:09:05', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3771', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 14:10:29', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3772', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 14:47:06', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3773', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 14:59:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3774', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 15:07:00', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3775', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 15:25:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3776', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 15:41:22', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3777', 'login', 'login', '1', 'admin', '222.76.241.234', '2017-12-25 16:23:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3778', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 16:49:21', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3779', 'login', 'login', '1', 'admin', '222.76.241.234', '2017-12-25 16:51:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3780', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 17:13:13', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3781', 'login', 'login', '1', 'admin', '222.76.241.234', '2017-12-25 17:24:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3782', 'login', 'login', '1', 'admin', '222.76.241.234', '2017-12-25 17:26:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3783', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 18:20:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3784', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 18:50:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3785', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 19:32:49', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3786', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 19:55:51', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3787', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 20:14:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3788', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 20:20:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3789', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 20:32:46', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3790', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 20:33:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3791', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 20:33:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3792', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 21:00:06', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3793', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 21:19:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3794', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 21:26:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3795', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:30:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3796', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 21:33:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3797', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:36:18', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3798', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:43:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3799', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:44:29', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3800', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:48:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3801', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:51:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3802', 'login', 'login', '1', 'admin', '117.28.112.216', '2017-12-25 21:54:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3803', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 21:56:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3804', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 22:05:43', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3805', 'login', 'login', '1', 'admin', '117.28.112.78', '2017-12-25 22:11:56', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3806', 'login', 'login', '1', 'admin', '120.41.17.56', '2017-12-26 07:51:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3807', 'login', 'login', '1', 'admin', '120.41.17.56', '2017-12-26 07:54:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3808', 'login', 'login', '1', 'admin', '120.41.17.56', '2017-12-26 08:41:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3809', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 10:33:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3810', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 11:59:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3811', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 12:26:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3812', 'login', 'login', '1', 'admin', '47.94.169.210', '2017-12-26 14:32:07', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3813', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 14:39:32', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3814', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 14:48:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3815', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 15:47:25', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3816', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 16:14:49', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3817', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 16:26:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3818', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 16:56:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3819', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 17:04:55', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3820', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 17:11:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3821', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 17:32:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3822', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 17:34:38', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3823', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 17:37:20', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3824', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 17:41:19', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3825', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 17:46:12', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3826', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 18:00:10', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3827', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 18:37:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3828', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 18:56:03', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3829', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 19:11:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3830', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-26 19:18:44', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3831', 'login', 'login', '1', 'admin', '117.30.209.129', '2017-12-26 19:18:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3832', 'login', 'login', '1', 'admin', '120.41.17.56', '2017-12-26 23:40:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3833', 'login', 'login', '1', 'admin', '223.104.6.24', '2017-12-27 00:45:19', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3834', 'login', 'login', '1', 'admin', '117.28.112.141', '2017-12-28 11:50:01', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3835', 'login', 'login', '1', 'admin', '117.30.239.188', '2018-01-02 10:02:21', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3836', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-03 08:54:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3837', 'login', 'login', '1', 'admin', '117.30.208.7', '2018-01-04 17:47:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3838', 'login', 'login', '1', 'admin', '117.30.208.7', '2018-01-04 20:24:14', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3839', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-05 11:20:47', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3840', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-05 15:24:25', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3841', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-05 16:05:23', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3842', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-06 18:35:11', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3843', 'login', 'login', '1', 'admin', '183.250.213.8', '2018-01-06 22:13:13', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3844', 'login', 'login', '1', 'admin', '183.250.213.8', '2018-01-06 22:41:59', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3845', 'login', 'login', '1', 'admin', '183.250.213.8', '2018-01-07 11:32:03', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3846', 'login', 'login', '1', 'admin', '183.250.213.8', '2018-01-07 11:41:08', '福建省厦门市[移动]', 'n');
INSERT INTO `t_systemlog` VALUES ('3847', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-07 13:46:50', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3848', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-07 14:02:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3849', 'login', 'login', '1', 'admin', '183.250.213.8', '2018-01-07 19:39:31', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3850', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-08 09:53:48', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3851', 'login', 'login', '1', 'admin', '117.28.112.134', '2018-01-09 09:30:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3852', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-09 17:41:37', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3853', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-09 17:46:57', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3854', 'loginOut', 'loginOut', '1', 'admin', '47.94.169.210', '2018-01-09 17:47:33', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3855', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-09 17:49:43', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3856', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-09 17:53:35', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3857', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-09 17:58:05', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3858', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-09 18:24:28', '北京市北京市[阿里云]', 'n');
INSERT INTO `t_systemlog` VALUES ('3859', 'login', 'login', '1', 'admin', '117.28.112.154', '2018-01-12 18:18:25', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3860', 'login', 'login', '1', 'admin', '117.28.112.154', '2018-01-12 18:18:39', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3861', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-12 18:21:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3862', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-12 18:21:49', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3863', 'loginOut', 'loginOut', '1', 'admin', '117.28.113.216', '2018-01-12 18:22:00', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3864', 'login', 'login', '1', 'admin', '117.28.112.154', '2018-01-12 19:26:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3865', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-12 19:36:40', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3866', 'login', 'login', '1', 'admin', '58.23.44.59', '2018-01-14 21:03:50', '福建省厦门市[联通]', 'y');
INSERT INTO `t_systemlog` VALUES ('3867', 'login', 'login', '1', 'admin', '183.253.42.126', '2018-01-14 21:08:46', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3868', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-15 11:14:19', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3869', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-01-15 15:13:09', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3870', 'login', 'login', '1', 'admin', '117.30.208.95', '2018-01-15 15:40:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3871', 'login', 'login', '1', 'admin', '117.30.208.95', '2018-01-15 16:20:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3872', 'login', 'login', '1', 'admin', '117.30.208.95', '2018-01-15 18:37:31', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3873', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-15 19:57:09', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3874', 'login', 'login', '1', 'admin', '183.253.42.126', '2018-01-15 20:01:18', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3875', 'login', 'login', '1', 'admin', '27.154.74.178', '2018-01-15 20:17:51', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3876', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-01-16 08:48:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3877', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 09:05:26', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3878', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 09:44:24', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3879', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 10:05:08', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3880', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-16 10:17:03', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3881', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 10:19:17', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3882', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-16 10:48:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3883', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 10:51:58', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3884', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 10:53:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3885', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 10:56:50', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3886', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-16 12:31:45', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3887', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 14:15:00', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3888', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 14:27:23', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3889', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 14:40:30', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3890', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 15:49:04', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3891', 'login', 'login', '1', 'admin', '117.28.112.129', '2018-01-16 16:09:52', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3892', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-16 19:21:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3893', 'login', 'login', '1', 'admin', '27.154.74.178', '2018-01-16 22:29:54', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3894', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 09:21:45', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3895', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-17 09:32:01', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3896', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-17 09:35:53', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3897', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-17 09:39:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3898', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-17 09:44:29', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3899', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 09:44:34', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3900', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 09:59:45', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3901', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-17 10:08:47', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3902', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 11:18:37', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3903', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 11:34:27', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3904', 'loginOut', 'loginOut', '1', 'admin', '117.30.238.159', '2018-01-17 12:23:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3905', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 12:23:41', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3906', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 12:25:37', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3907', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 14:04:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3908', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-17 15:15:16', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3909', 'login', 'login', '1', 'admin', '117.28.113.216', '2018-01-17 15:18:51', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3910', 'login', 'login', '1', 'admin', '117.30.238.159', '2018-01-17 20:41:15', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3911', 'login', 'login', '1', 'admin', '117.28.113.79', '2018-01-18 15:12:28', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3912', 'login', 'login', '1', 'admin', '47.94.169.210', '2018-01-22 14:50:00', '北京市北京市[阿里云]', 'y');
INSERT INTO `t_systemlog` VALUES ('3913', 'login', 'login', '1', 'admin', '117.30.209.252', '2018-01-25 15:26:33', '福建省厦门市[电信]', 'y');
INSERT INTO `t_systemlog` VALUES ('3914', 'login', 'login', '1', 'admin', '117.30.209.252', '2018-01-25 15:27:42', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3915', 'login', 'login', '1', 'admin', '117.30.209.252', '2018-01-25 15:51:07', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3916', 'login', 'login', '1', 'admin', '117.30.209.252', '2018-01-25 20:25:13', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3917', 'login', 'login', '1', 'admin', '117.30.209.252', '2018-01-26 15:19:32', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3918', 'login', 'login', '1', 'admin', '117.28.113.199', '2018-01-26 20:40:49', '福建省厦门市[电信]', 'n');
INSERT INTO `t_systemlog` VALUES ('3919', 'login', 'login', '1', 'admin', '183.253.42.194', '2018-01-26 21:43:50', '福建省厦门市[移动]', 'y');
INSERT INTO `t_systemlog` VALUES ('3920', 'login', 'login', '1', 'admin ', '117.30.209.65', '2018-02-10 15:43:17', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3921', 'login', 'login', '1', 'admin ', '117.30.209.65', '2018-02-10 15:43:35', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3922', 'loginOut', 'loginOut', '1', 'admin ', '117.30.209.65', '2018-02-10 15:45:17', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3923', 'login', 'login', '1', 'admin ', '117.30.209.65', '2018-02-10 15:45:23', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3924', 'login', 'login', '1', 'admin ', '117.30.209.65', '2018-02-10 15:48:57', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3925', 'login', 'login', '1', 'admin ', '117.30.209.27', '2018-02-28 19:46:58', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3926', 'login', 'login', '1', 'admin', '183.250.213.0', '2018-03-14 21:00:57', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3927', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-19 08:46:10', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3928', 'login', 'login', '1', 'admin', '117.28.112.115', '2018-03-19 09:16:19', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3929', 'login', 'login', '1', 'admin', '27.154.75.107', '2018-03-19 22:59:12', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3930', 'login', 'login', '1', 'admin', '223.104.6.23', '2018-03-20 14:35:11', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3931', 'login', 'login', '1', 'admin', '27.154.75.107', '2018-03-20 22:53:16', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3932', 'login', 'login', '1', 'admin', '27.154.75.107', '2018-03-20 22:53:53', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3933', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-21 16:53:49', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3934', 'login', 'login', '1', 'admin', '223.104.6.28', '2018-03-21 17:56:56', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3935', 'login', 'login', '1', 'admin', '120.36.147.57', '2018-03-21 18:30:31', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3936', 'login', 'login', '1', 'admin', '117.30.208.68', '2018-03-21 18:43:00', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3937', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-22 13:50:00', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3938', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-23 08:48:08', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3939', 'login', 'login', '1', 'admin', '120.41.4.229', '2018-03-23 13:58:32', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3940', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 14:06:04', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3941', 'login', 'login', '1', 'admin', '120.41.4.229', '2018-03-23 14:09:43', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3942', 'login', 'login', '1', 'admin', '223.104.6.24', '2018-03-23 14:10:28', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3943', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 14:16:31', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3944', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 14:28:26', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3945', 'login', 'login', '1', 'admin', '120.41.4.229', '2018-03-23 14:33:27', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3946', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 14:40:12', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3947', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 14:57:26', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3948', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 15:00:47', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3949', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 15:00:51', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3950', 'loginOut', 'loginOut', '1', 'admin', '117.30.209.194', '2018-03-23 15:09:25', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3951', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 15:09:28', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3952', 'loginOut', 'loginOut', '1', 'admin', '117.30.209.194', '2018-03-23 15:16:51', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3953', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 15:17:08', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3954', 'loginOut', 'loginOut', '1', 'admin', '117.30.209.194', '2018-03-23 15:24:53', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3955', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 15:25:13', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3956', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 15:40:27', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3957', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 16:01:05', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3958', 'login', 'login', '1', 'admin', '223.104.6.24', '2018-03-23 16:16:42', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3959', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 17:00:23', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3960', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-23 17:54:18', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3961', 'login', 'login', '1', 'admin', '223.104.6.18', '2018-03-23 17:57:00', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3962', 'login', 'login', '1', 'admin', '58.23.44.121', '2018-03-24 11:28:42', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3963', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-24 11:38:21', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3964', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-24 11:45:07', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3965', 'login', 'login', '1', 'admin', '58.23.44.121', '2018-03-24 11:59:41', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3966', 'login', 'login', '1', 'admin', '117.31.116.64', '2018-03-24 12:20:33', '漳州电信[350600]', 'y');
INSERT INTO `t_systemlog` VALUES ('3967', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-24 12:46:07', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3968', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-24 12:57:13', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3969', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-24 13:00:08', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3970', 'login', 'login', '1', 'admin', '117.31.116.64', '2018-03-24 21:08:50', '漳州电信[350600]', 'y');
INSERT INTO `t_systemlog` VALUES ('3971', 'login', 'login', '1', 'admin', '117.31.116.64', '2018-03-25 09:05:38', null, 'n');
INSERT INTO `t_systemlog` VALUES ('3972', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-26 11:32:15', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3973', 'login', 'login', '1', 'admin', '110.87.111.108', '2018-03-27 00:09:18', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3974', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-27 08:34:25', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3975', 'login', 'login', '1', 'admin', '223.104.6.37', '2018-03-27 12:31:19', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3976', 'login', 'login', '1', 'admin', '110.87.111.108', '2018-03-27 12:41:17', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3977', 'login', 'login', '1', 'admin', '110.87.111.108', '2018-03-27 12:44:57', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3978', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-27 12:46:20', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3979', 'login', 'login', '1', 'admin', '120.36.147.117', '2018-03-27 12:56:55', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3980', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-27 13:11:49', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3981', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-27 13:36:20', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3982', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-27 13:36:20', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3983', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-27 13:47:14', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3984', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-27 13:48:55', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3985', 'login', 'login', '1', 'admin', '223.104.6.37', '2018-03-27 13:49:25', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3986', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-27 15:37:59', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3987', 'login', 'login', '1', 'admin', '110.87.111.108', '2018-03-27 17:09:09', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3988', 'login', 'login', '1', 'admin', '110.87.111.108', '2018-03-27 19:13:36', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3989', 'login', 'login', '1', 'admin', '223.104.6.33', '2018-03-27 19:38:46', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3990', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-27 20:18:32', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3991', 'login', 'login', '1', 'admin', '223.104.6.35', '2018-03-27 21:08:16', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3992', 'login', 'login', '1', 'admin', '211.97.131.4', '2018-03-27 21:53:07', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3993', 'login', 'login', '1', 'admin', '58.23.44.170', '2018-03-27 22:57:55', '厦门联通[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3994', 'login', 'login', '1', 'admin', '223.104.6.35', '2018-03-27 23:55:49', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3995', 'login', 'login', '1', 'admin', '211.97.131.4', '2018-03-28 09:00:19', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3996', 'login', 'login', '1', 'admin', '110.87.111.108', '2018-03-28 10:14:14', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3997', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-28 14:20:32', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('3998', 'login', 'login', '1', 'admin', '223.104.6.53', '2018-03-28 16:31:49', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('3999', 'login', 'login', '1', 'admin', '58.23.250.74', '2018-03-28 21:57:13', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4000', 'login', 'login', '1', 'admin', '223.104.45.99', '2018-03-29 08:52:09', '泉州移动[350500]', 'y');
INSERT INTO `t_systemlog` VALUES ('4001', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-29 10:28:07', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4002', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-29 10:55:13', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4003', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-29 10:57:06', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4004', 'login', 'login', '1', 'admin', '223.104.45.99', '2018-03-29 10:59:54', '泉州移动[350500]', 'y');
INSERT INTO `t_systemlog` VALUES ('4005', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-29 15:43:12', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4006', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-29 16:10:06', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4007', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-29 16:43:29', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4008', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-29 19:57:58', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4009', 'login', 'login', '1', 'admin', '183.250.213.190', '2018-03-29 20:08:52', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4010', 'login', 'login', '1', 'admin', '58.23.251.97', '2018-03-29 21:43:51', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4011', 'login', 'login', '1', 'admin', '58.23.250.44', '2018-03-29 23:31:50', '厦门联通[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4012', 'login', 'login', '1', 'admin', '183.250.213.190', '2018-03-30 08:21:46', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4013', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-03-30 08:39:19', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4014', 'login', 'login', '1', 'admin', '120.41.4.229', '2018-03-30 08:57:04', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4015', 'login', 'login', '1', 'admin', '120.41.4.229', '2018-03-30 09:26:04', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4016', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-30 09:34:24', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4017', 'login', 'login', '1', 'admin', '223.104.6.6', '2018-03-30 10:02:01', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4018', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-30 10:40:57', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4019', 'login', 'login', '1', 'admin', '120.41.4.229', '2018-03-30 10:46:13', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4020', 'login', 'login', '1', 'admin', '183.250.213.190', '2018-03-30 11:57:57', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4021', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-30 13:36:57', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4022', 'login', 'login', '1', 'admin', '120.41.4.226', '2018-03-30 15:21:04', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4023', 'login', 'login', '1', 'admin', '117.30.209.194', '2018-03-30 17:25:16', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4024', 'login', 'login', '1', 'admin', '220.161.69.119', '2018-03-31 11:57:55', '漳州电信[350600]', 'y');
INSERT INTO `t_systemlog` VALUES ('4025', 'login', 'login', '1', 'admin', '211.97.129.76', '2018-03-31 15:06:48', '厦门联通[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4026', 'login', 'login', '1', 'admin', '220.161.69.119', '2018-04-01 01:14:20', '漳州电信[350600]', 'y');
INSERT INTO `t_systemlog` VALUES ('4027', 'login', 'login', '1', 'admin', '220.161.69.119', '2018-04-01 09:32:25', '漳州电信[350600]', 'n');
INSERT INTO `t_systemlog` VALUES ('4028', 'login', 'login', '1', 'admin', '120.43.159.209', '2018-04-01 18:34:17', '漳州电信[350600]', 'n');
INSERT INTO `t_systemlog` VALUES ('4029', 'login', 'login', '1', 'admin', '112.5.168.165', '2018-04-01 20:29:48', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4030', 'login', 'login', '1', 'admin', '223.104.6.47', '2018-04-02 08:31:05', '厦门移动[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4031', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-04-02 09:25:14', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4032', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-04-03 08:37:14', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4033', 'login', 'login', '1', 'admin', '223.104.6.51', '2018-04-03 08:58:18', '厦门移动[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4034', 'login', 'login', '1', 'admin', '27.154.75.152', '2018-04-03 18:59:19', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4035', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-04-04 16:22:56', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4036', 'login', 'login', '1', 'admin', '220.161.69.119', '2018-04-05 13:37:07', '漳州电信[350600]', 'y');
INSERT INTO `t_systemlog` VALUES ('4037', 'login', 'login', '1', 'admin', '27.157.20.6', '2018-04-05 21:20:01', '漳州电信[350600]', 'n');
INSERT INTO `t_systemlog` VALUES ('4038', 'login', 'login', '1', 'admin', '222.47.27.111', '2018-04-05 23:26:20', '福州铁通[350100]', 'y');
INSERT INTO `t_systemlog` VALUES ('4039', 'login', 'login', '1', 'admin', '222.47.27.111', '2018-04-05 23:46:47', '福州铁通[350100]', 'n');
INSERT INTO `t_systemlog` VALUES ('4040', 'login', 'login', '1', 'admin', '27.157.20.6', '2018-04-05 23:47:25', '漳州电信[350600]', 'y');
INSERT INTO `t_systemlog` VALUES ('4041', 'login', 'login', '1', 'admin', '117.30.208.32', '2018-04-08 14:25:43', '厦门电信[350200]', 'y');
INSERT INTO `t_systemlog` VALUES ('4042', 'login', 'login', '1', 'admin', '117.30.208.32', '2018-04-09 10:30:26', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4043', 'login', 'login', '1', 'admin', '117.30.208.32', '2018-04-10 19:02:21', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4044', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-04-11 15:11:03', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4045', 'login', 'login', '1', 'admin', '117.30.208.32', '2018-04-11 15:12:26', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4046', 'loginOut', 'loginOut', '1', 'admin', '222.76.241.234', '2018-04-11 15:13:48', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4047', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-04-11 15:13:52', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4048', 'login', 'login', '1', 'admin', '117.28.112.62', '2018-04-12 21:05:36', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4049', 'login', 'login', '1', 'admin', '117.28.112.62', '2018-04-13 10:29:38', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4050', 'login', 'login', '1', 'admin', '117.30.208.171', '2018-04-25 15:24:44', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4051', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-02 11:38:42', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4052', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-02 14:13:50', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4053', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-02 14:47:02', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4054', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-02 15:39:22', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4055', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-02 16:05:41', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4056', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-05-03 18:38:15', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4057', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-07 15:16:49', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4058', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-08 10:28:53', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4059', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-08 11:07:59', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4060', 'login', 'login', '1', 'admin', '117.30.208.113', '2018-05-08 14:23:05', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4061', 'login', 'login', '1', 'admin', '117.28.112.110', '2018-05-24 11:19:21', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4062', 'login', 'login', '1', 'admin', '117.30.208.223', '2018-05-30 16:24:37', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4063', 'login', 'login', '1', 'admin', '117.30.208.223', '2018-05-30 18:53:23', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4064', 'login', 'login', '1', 'admin', '117.30.208.223', '2018-06-01 10:28:55', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4065', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-06-01 10:32:49', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4066', 'login', 'login', '1', 'admin', '27.154.26.77', '2018-06-04 20:56:18', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4067', 'login', 'login', '1', 'admin', '222.76.241.234', '2018-06-05 15:20:09', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4068', 'login', 'login', '1', 'admin', '117.30.239.89', '2018-06-06 09:34:43', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4069', 'login', 'login', '1', 'admin', '117.30.208.46', '2018-06-11 09:31:33', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4070', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-14 20:11:06', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4071', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-14 20:17:42', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4072', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-14 20:22:12', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4073', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 14:04:56', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4074', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 14:10:25', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4075', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 14:18:22', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4076', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 14:26:40', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4077', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 14:43:41', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4078', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 14:51:03', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4079', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 15:03:03', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4080', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 15:36:51', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4081', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 15:50:50', '厦门电信[350200]', 'n');
INSERT INTO `t_systemlog` VALUES ('4082', 'login', 'login', '1', 'admin', '117.28.113.9', '2018-06-19 16:40:29', '厦门电信[350200]', 'n');

-- ----------------------------
-- Table structure for t_systemsetting
-- ----------------------------
DROP TABLE IF EXISTS `t_systemsetting`;
CREATE TABLE `t_systemsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `systemCode` varchar(45) DEFAULT NULL COMMENT '系统编码',
  `name` varchar(100) NOT NULL COMMENT '系统名称',
  `www` varchar(100) NOT NULL COMMENT '系统路径',
  `logo` varchar(100) DEFAULT NULL COMMENT 'logo路径',
  `title` varchar(45) NOT NULL COMMENT '系统标题',
  `description` varchar(45) NOT NULL COMMENT '系统描述',
  `keywords` varchar(100) NOT NULL COMMENT '关键字',
  `shortcuticon` varchar(100) NOT NULL COMMENT '页面icon',
  `appid` varchar(1000) DEFAULT NULL COMMENT '微信公众号ID',
  `secret` varchar(1000) DEFAULT NULL COMMENT '微信公众号密钥',
  `mchID` varchar(1000) DEFAULT NULL COMMENT '商户号',
  `smsKey` varchar(1000) DEFAULT NULL COMMENT '融云短信Key',
  `smsSecret` varchar(1000) DEFAULT NULL COMMENT '融云短信Secret',
  `smsTemplateId` varchar(1000) DEFAULT NULL COMMENT '融云短信注册模板ID',
  `percent` decimal(11,2) DEFAULT NULL COMMENT '返佣金额占利润比',
  `aboutHTML` varchar(5000) DEFAULT NULL COMMENT '发现页面',
  `introduce` varchar(5000) DEFAULT NULL COMMENT '关于我们页面',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统参数设置表';

-- ----------------------------
-- Records of t_systemsetting
-- ----------------------------
INSERT INTO `t_systemsetting` VALUES ('1', 'veo', '味猫商城', 'http://www.veomall.com', '/resource/images/log.png', '味猫商城', '味猫商城', '味猫商城', '/resource/images/favicon.png', 'wxcc8ce260da900c6b', '0f96aec6b7fef911aa8acea9becf2433', '1491033762', '8brlm7ufrrr63', 'Ubp51aeJKe1KB', 'aFIUD380k5I8c9F3aAyjlq', '0.05', '<p><span style=\"font-family: 宋体; font-size: 16px;\">B2B（<span style=\"font-family: Calibri;\">Business To Business, </span>商家对商家）</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">B2B典型代表有阿里巴巴。中国制造网，慧聪等，主要是从事批发业务；</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">B2C(Business To Customer,商家对顾客销售<span style=\"font-size: 16px; font-family: Calibri;\">)</span></span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">B2C典型代表有当当网、京东商城、中国购、凡客诚品、稀货街、百宝汇商城、新蛋商城、中国巨蛋、思和电器商城、聚购商城、聚购商城主要是经营工艺品与创意产品。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">B2C中又分为三种，一种是实体企业转网上商城，代表网站为库巴网；一种是实体市场转网上商城，代表为蚕丝网城；一种是原有电子商务公司建设的网上商城，代表为京东商城，中华网库商城系统等</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">C2C(Customer to Customer,客户和客户）</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">C2C典型代表有淘宝、鸟差、易趣、拍拍、百度有啊。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">O2O（<span style=\"font-size: 16px; font-family: Calibri;\">Online To Offline,</span>线上线下相结合）</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">O2O典型代表有象屿商城。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">G2C:G2C电子政务是指政府<span style=\"font-size: 16px; font-family: Calibri;\">( Government)</span>与公众<span style=\"font-size: 16px; font-family: Calibri;\">(Citizen)</span>之间的电子政务。是政府通过电子网络系统为公民提供各种服务。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">B2B2C:目前最新的一种整合型网上商城模式，代表有汇诚网（又名“汇城网”）。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">O2P（<span style=\"font-size: 16px; font-family: Calibri;\">Online To Place,</span>本地化线上线下）</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">O2P：<span style=\"font-size: 16px; font-family: Calibri;\">2013</span>年最新的一种电商模式，针对大型家电或者汽车等大件商品不便运输，由电动车业界精英提出的线上商城，本地化配送的新模式。代表有道易行商城（又名“道易行专业电动车商城”）。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">G2C电子政务所包含的内容十分广泛，主要的应用包括：公众信息服务、电子身份认证、电子税务、电子社会保障服务、电子民主管理、电子医疗服务、电子就业服务、电子教育、培训服务、电子交通管理等。<span style=\"font-size: 16px; font-family: Calibri;\">G2C</span>电子政务的目的是除了政府给公众提供方便、快捷、高质量的服务外，更重要的是可以开辟公众参政、议政的渠道，畅通公众的利益表达机制，建立政府与公众的良性互动平台。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">网上商城是在为个人用户和企业用户提供人性化的全方位服务，努力为用户创造亲切、轻松和愉悦的购物环境，不断丰富产品结构，最大化地满足消费者日趋多样的购物需求，并凭借更具竞争力的价格和逐渐完善的物流配送体系等各项优势，赢得市场占有率多年稳居行业首位的骄人成绩，也是时代发展的趋势。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">两大模块</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">编辑</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">后台常见功能模块</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">1、商品管理：包括后台商品库存管理、上货、出货、编辑管理和商品分类管理、商品品牌管理等。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">2、订单管理：在线订单程序，使消费者能够顺利的通过<span style=\"font-size: 16px; font-family: Calibri;\">WEB</span>在线的方式，直接生成购买订单。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">3、商品促销：一般的购物系统多有商品促销功能，通过商品促销功能，能够迅速的促进商城的消费积极性。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">4、支付方式：即通过网上钱包、电子支付卡。进行网上资金流转换的业务流程；国内主流支付方式包括：支付宝、财富通、网银在线等。还有部分网上商城支持货到付款，如京东商城、第九大道、鹏程万里贸易商城。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">5、配送方式：购物系统集成的物流配送方式，从而方便消费者对物流方式进行在线选择。如：<span style=\"font-size: 16px; font-family: Calibri;\">EMS</span>、顺丰等等。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">6、会员模块：在购物系统中，集成会员注册是吸引会员进行二次购买和提升转换率最好的方式。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">前端界面功能模块</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">1、模板风格自定义：即通过系统内置的模板引擎，可以方便的通过后台可视化编辑，设计出符合自身需求的风格界面。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">2、商品多图展示：随着电子商务的发展商品图片成为吸引消费者的第一要素，多图展示即提供前台多张图片的展示，从而提升消费者的购物欲望。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">3、自定义广告模块：内置在系统中的广告模块，网站管理员能够顺利的通过操作就可以在前端界面中添加各种广告图片。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">4、商品展示：通过前端界面，以标准的或者其他个性化的方式向用户展示商品各类信息，完成购物系统内信息流的传递。</span></p><p><span style=\"font-family: 宋体; font-size: 16px;\">5、购物车： 用户可对想要购买的商品进行网上订购，在购物过程中，随时增删商品。</span></p><p><br/></p>', '<p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">B2B（<span style=\"font-family: Calibri;\">Business To Business,&nbsp;</span>商家对商家）</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">B2B典型代表有阿里巴巴。中国制造网，慧聪等，主要是从事批发业务；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">B2C(Business To Customer,商家对顾客销售<span style=\"font-family: Calibri;\">)</span></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">B2C典型代表有当当网、京东商城、中国购、凡客诚品、稀货街、百宝汇商城、新蛋商城、中国巨蛋、思和电器商城、聚购商城、聚购商城主要是经营工艺品与创意产品。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">B2C中又分为三种，一种是实体企业转网上商城，代表网站为库巴网；一种是实体市场转网上商城，代表为蚕丝网城；一种是原有电子商务公司建设的网上商城，代表为京东商城，中华网库商城系统等</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">C2C(Customer to Customer,客户和客户）</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">C2C典型代表有淘宝、鸟差、易趣、拍拍、百度有啊。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">O2O（<span style=\"font-family: Calibri;\">Online To Offline,</span>线上线下相结合）</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">O2O典型代表有象屿商城。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">G2C:G2C电子政务是指政府<span style=\"font-family: Calibri;\">( Government)</span>与公众<span style=\"font-family: Calibri;\">(Citizen)</span>之间的电子政务。是政府通过电子网络系统为公民提供各种服务。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">B2B2C:目前最新的一种整合型网上商城模式，代表有汇诚网（又名“汇城网”）。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">O2P（<span style=\"font-family: Calibri;\">Online To Place,</span>本地化线上线下）</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">O2P：<span style=\"font-family: Calibri;\">2013</span>年最新的一种电商模式，针对大型家电或者汽车等大件商品不便运输，由电动车业界精英提出的线上商城，本地化配送的新模式。代表有道易行商城（又名“道易行专业电动车商城”）。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">G2C电子政务所包含的内容十分广泛，主要的应用包括：公众信息服务、电子身份认证、电子税务、电子社会保障服务、电子民主管理、电子医疗服务、电子就业服务、电子教育、培训服务、电子交通管理等。<span style=\"font-family: Calibri;\">G2C</span>电子政务的目的是除了政府给公众提供方便、快捷、高质量的服务外，更重要的是可以开辟公众参政、议政的渠道，畅通公众的利益表达机制，建立政府与公众的良性互动平台。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">网上商城是在为个人用户和企业用户提供人性化的全方位服务，努力为用户创造亲切、轻松和愉悦的购物环境，不断丰富产品结构，最大化地满足消费者日趋多样的购物需求，并凭借更具竞争力的价格和逐渐完善的物流配送体系等各项优势，赢得市场占有率多年稳居行业首位的骄人成绩，也是时代发展的趋势。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">两大模块</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">编辑</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">后台常见功能模块</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">1、商品管理：包括后台商品库存管理、上货、出货、编辑管理和商品分类管理、商品品牌管理等。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">2、订单管理：在线订单程序，使消费者能够顺利的通过<span style=\"font-family: Calibri;\">WEB</span>在线的方式，直接生成购买订单。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">3、商品促销：一般的购物系统多有商品促销功能，通过商品促销功能，能够迅速的促进商城的消费积极性。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">4、支付方式：即通过网上钱包、电子支付卡。进行网上资金流转换的业务流程；国内主流支付方式包括：支付宝、财富通、网银在线等。还有部分网上商城支持货到付款，如京东商城、第九大道、鹏程万里贸易商城。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">5、配送方式：购物系统集成的物流配送方式，从而方便消费者对物流方式进行在线选择。如：<span style=\"font-family: Calibri;\">EMS</span>、顺丰等等。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">6、会员模块：在购物系统中，集成会员注册是吸引会员进行二次购买和提升转换率最好的方式。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">前端界面功能模块</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">1、模板风格自定义：即通过系统内置的模板引擎，可以方便的通过后台可视化编辑，设计出符合自身需求的风格界面。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">2、商品多图展示：随着电子商务的发展商品图片成为吸引消费者的第一要素，多图展示即提供前台多张图片的展示，从而提升消费者的购物欲望。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">3、自定义广告模块：内置在系统中的广告模块，网站管理员能够顺利的通过操作就可以在前端界面中添加各种广告图片。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">4、商品展示：通过前端界面，以标准的或者其他个性化的方式向用户展示商品各类信息，完成购物系统内信息流的传递。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体;\">5、购物车： 用户可对想要购买的商品进行网上订购，在购物过程中，随时增删商品。</span></p><p><span style=\"font-family: 宋体;\"><br/></span></p><p><br/></p>');

-- ----------------------------
-- Table structure for t_task
-- ----------------------------
DROP TABLE IF EXISTS `t_task`;
CREATE TABLE `t_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `parentId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '父级任务ID',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务名称',
  `nameDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务描述',
  `planExe` int(11) DEFAULT NULL COMMENT '计划执行次数,默认0,表示满足条件循环执行次数',
  `groupName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务组名称,约定为一个类全名',
  `groupDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务组描述',
  `cron` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务表达式',
  `cronDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务表达式描述',
  `triggerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发器，约定为一个方法名，格式com.dongnao.Xdd.methdo',
  `triggerGrop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发器组名',
  `triggerGropDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发器组描述',
  `execute` int(11) DEFAULT NULL COMMENT '任务被执行过多少次',
  `lastExeTime` int(100) DEFAULT NULL COMMENT '最后一次开始执行时间',
  `lastFinishTime` int(100) DEFAULT NULL,
  `state` int(11) DEFAULT '0' COMMENT '任务状态0禁用，1启动，2删除',
  `deply` int(11) DEFAULT '0' COMMENT '延迟启动，默认0,表示不延迟启动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='定时任务表';

-- ----------------------------
-- Records of t_task
-- ----------------------------
INSERT INTO `t_task` VALUES ('11', null, 'shop.core.task.TokenTask', null, null, 'shop.core.task.TokenTask', null, '0 0/5 * * * ?', null, 'run', null, null, null, null, null, '1', '0');
INSERT INTO `t_task` VALUES ('12', null, '微信jsapi刷新定时任务', null, null, 'shop.core.task.JsapiTicketTask', null, '0 0/30 * * * ?', null, 'run', null, null, null, null, null, '1', '0');
INSERT INTO `t_task` VALUES ('13', null, '更新开团状态', null, null, 'shop.core.task.ProductStatusTask', null, '0 */1 * * * ?', null, 'run', null, null, null, null, null, '1', '0');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nickname` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
  `username` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账号',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `sex` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '性别(1男、2女）',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机',
  `openID` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户设备',
  `icon` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户头像',
  `qrCode` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '二维码',
  `sceneStr` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '二维码参数',
  `pid` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '父级ID',
  `rid` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '角色类型（1超级管理员、2自营账号、3线下合伙人、4普通用户）',
  `status` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT 'y' COMMENT '状态（y禁用、n启用）',
  `walletMoney` decimal(11,2) DEFAULT '0.00' COMMENT '用户钱包',
  `percent` decimal(11,2) DEFAULT '0.05' COMMENT '合伙人返利占订单金额百分比',
  `createTime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=410 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统用户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '超级管理员', 'admin', '24c34f664ffe10586ed51f026681cda1', '1', null, null, null, null, null, '0', '1', 'y', '0.00', '0.00', null, 'admin', '2018-03-23 15:24:36', null);
INSERT INTO `t_user` VALUES ('237', '华鱼丸', '53162a81e17e4f61977ba9f3acbca1c3', '9d23a7d478854e2c8ab6da2f6ff7a9cb', '1', null, 'oLyuwxNUWvrzIhF7Maj98gjMaWyw', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJLUZnFHdQfFtpwUH1pKHYbyLK7KbNsfBRRnuibibRhc5yxHkeHJofrzVsYKsFD9RBXWCzz6PIuSFOw/0', 'ueditor/upload/qrCodeImgs/201801081003252234.png', null, '0', '2', 'y', '0.00', '0.05', '2018-01-08 10:03:25', null, '2018-03-30 11:59:38', null);
INSERT INTO `t_user` VALUES ('266', 'Janny', 'd88b3f6863a04f8da0398b2b5a78b4b7', '675128f08a3647798811652427dc623b', '2', null, 'oLyuwxD6ahgPYLDHS8FwQaCvbVM4', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJwwkibCiaW6Qg5AWBAGqVMfDtveWMibPG97MYKSnOSjAgRrkWFTum5onRzHpIr4iaS0EBAzD4U9lLr0Q/0', 'ueditor/upload/qrCodeImgs/201801061837315344.png', null, '0', '3', 'y', '0.00', '0.05', '2018-01-06 18:37:31', null, '2018-01-06 18:37:50', null);
INSERT INTO `t_user` VALUES ('267', '少年已不再年少', 'cf1134a32351459a877f0aca22a13ce6', '9318488f51024d209ad1a9ac67718970', '1', null, 'oLyuwxC5dlI5QYYMFtpOlxWuts_c', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK9JBzfWYCB8n9f4icUtDISzL2p4caR5bccEheiaEcibGVI3iamoAI5e0p6icictZbt1sA6gPm7571eZz9A/0', 'ueditor/upload/qrCodeImgs/201801071245115010.png', null, '0', '4', 'y', '0.00', '0.05', '2018-01-07 12:45:11', null, '2018-01-07 12:45:11', null);
INSERT INTO `t_user` VALUES ('269', '颖子', 'db9f366480f141c7bea06047d24dcd8e', '5aac198020544af5b0e5e590923a3024', '2', null, 'oLyuwxNObMw0eo7WNXHzItB5iWJw', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI9sOEkia2psnrJgKdypZOzdichfhqxce9GSOZjjrhzRd19j1F4oObbxtR9E2wGKBR4UoUAFAxJpN2g/0', 'ueditor/upload/qrCodeImgs/20180107140755670.png', null, '0', '2', 'y', '0.00', '0.05', '2018-01-07 14:07:55', null, '2018-01-15 15:13:57', null);
INSERT INTO `t_user` VALUES ('270', '橙橙橙', 'f8e01f36f2ff41dbba2b173d99c32acd', 'bcd5bf278136488cb969c89c38305600', '1', null, 'oLyuwxOEFpCzse3waNSr9tV80B8E', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLGvq4ibibC5usKu1bHINgmcKzicfgkySzOfu7HGvhkP4aZ4auibwEhlFf69p1oz7AEEicpJ3j7icibjawWg/0', 'ueditor/upload/qrCodeImgs/201801071409107551.png', null, '0', '4', 'y', '0.01', '0.05', '2018-01-07 14:09:10', null, '2018-05-02 13:44:02', null);
INSERT INTO `t_user` VALUES ('272', '小游', '455d0d486a914546aabbde10de39defd', 'fc8ece4b64a44df6931081bd0dc69d91', '2', null, 'oLyuwxHhG7HmlNTQqUw3goiPDbOA', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKVXhCibFWKH66IaNKNgfcFhlseqCcribyLroz2TCJuu6XZB5s6Sy1K0gSH5xbzZdlPIas8pmgJPaqA/0', 'ueditor/upload/qrCodeImgs/201801071417462067.png', null, '0', '2', 'y', '0.00', '0.05', '2018-01-07 14:17:46', null, '2018-01-15 15:13:49', null);
INSERT INTO `t_user` VALUES ('274', 'I-Q-I', '6d7b428431c64a84a0a463586e68e6b2', '0d2cc42eb45842869cc5422ac3f629d3', '1', null, 'oLyuwxIQX2uYCUY8um_iG63fUZKw', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83epB4GUz8iaG1Wzcfiaxb7D6EHWM8yOcic07Ewzl5HC4P6xpGcJmlZVhv03ScDklEibOQcuTLEWvV376ibg/0', 'ueditor/upload/qrCodeImgs/201801071531017657.png', null, '292', '2', 'y', '1000.00', '0.05', '2018-01-07 15:31:01', null, '2018-06-01 10:35:38', null);
INSERT INTO `t_user` VALUES ('277', 'sasha', 'd8762c7181334ddea45090f1ed8818e3', 'f1897ff128184038b2aaf0b5b822550a', '2', null, 'oLyuwxHTtKfrStIeRftQowgntQNY', 'http://wx.qlogo.cn/mmopen/vi_32/GbaQiaaefCcst6Qofib4kJmEico5Jic6Bw1We2ZicGZfauvibCcgugtN1v3akCk6dV9uthdjf0aQHrg0tfe4QdzsaVdg/0', 'ueditor/upload/qrCodeImgs/20180108090710296.png', null, '0', '4', 'y', '0.00', '0.05', '2018-01-08 09:07:10', null, '2018-03-23 16:40:18', null);
INSERT INTO `t_user` VALUES ('280', '故世。', '9b9bf372070c4c2a9dd2529997a3d1b0', '18338c6200d34184885faf58f12673bc', '1', null, 'oLyuwxHznca03VATTFjVn5B2J-lk', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLrXGbJR5nwsEC3nASzicRexhqic5oScFyCTl8F6rqjIyQaBJZWXWe7rasscVpt1sYum5skqSvPe7MQ/0', 'ueditor/upload/qrCodeImgs/201801080956168153.png', null, '0', '2', 'y', '0.11', '0.05', '2018-01-08 09:56:16', null, '2018-05-02 14:31:34', null);
INSERT INTO `t_user` VALUES ('282', '柳', '7acb4b91592646efb2f386a007444d61', '4b592312d21a49d0b129b5a15cb77d65', '1', null, 'oLyuwxDexPjsJ6nT6nbn0I3NfZK8', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ77TffP9LicNEY8qEK2tsibJ3ob8WPCpiaBo6FUVmAia9PHV57IKAB2xeCUMwM9XKvFjibETC0wX3CxSg/132', 'ueditor/upload/qrCodeImgs/201801090949009721.png', null, '0', '4', 'y', '0.00', '0.05', '2018-01-09 09:49:00', null, '2018-01-09 09:49:00', null);
INSERT INTO `t_user` VALUES ('283', '李健', 'fc3d9e342fa24053be45f74eb0256131', '56809e96826142498322b0a8bf0db78c', '1', null, 'oLyuwxJo_vm1tA_mxYY3aS5WNsJE', 'http://wx.qlogo.cn/mmopen/vi_32/FR45iaWKnDES0uUqOS3pRTD9EE8ibRBRcTJJTJp2e7HaM0UVEaGGrHFV1tmMx6SnmtmO7wHicAbITPLyxUa9a5LIw/0', 'ueditor/upload/qrCodeImgs/201801100635068905.png', null, '0', '4', 'y', '0.00', '0.05', '2018-01-10 06:35:06', null, '2018-01-10 06:35:06', null);
INSERT INTO `t_user` VALUES ('284', '龙四', '0f499e27b20047fc880b7b2a002eec4e', 'd80577949e81483ba77cbebc52572cc7', '1', null, 'oLyuwxDVXIwsFJ_hCf4ks1D-Yz5c', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLbbXViaLLFrKr342NegT6GMhBA4XLmYoyoHn2rwxMJAOb3eeWjiaBxRxDbX2GJN1VlSKZYBEvOmHhw/132', 'ueditor/upload/qrCodeImgs/201801142110139560.png', null, '0', '2', 'y', '0.00', '0.05', '2018-01-14 21:10:13', null, '2018-06-14 23:27:07', null);
INSERT INTO `t_user` VALUES ('285', '林家三少', 'd4b374d47ae840c9afec199e1b372a62', 'cd0ea8199874423cbda1fbcc23c56ef7', '1', null, 'oLyuwxHwgf7EjCIEe5aiZd2OG5OI', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo8IGGkHia6FRLNM12efYhGWjSIDAltZGibo9Wl1Rjk87E2GmTnEnIFhRibKXnyO2fKu36zGxbGklXrw/132', 'ueditor/upload/qrCodeImgs/2018011712170517.png', null, '285', '3', 'y', '0.00', '0.10', '2018-01-17 12:17:05', null, '2018-04-12 11:15:07', null);
INSERT INTO `t_user` VALUES ('288', '顺丰优选-13959227895', '8eeb0edeb46240f5b72b1ddc6b51d76b', 'd1cd4aa2c07f4f6ea8ce142c987ca4b3', '2', null, 'oLyuwxPJKFoN3s4COsBczJ8dxLl0', 'http://wx.qlogo.cn/mmopen/vi_32/MibHSd06VgZ1g8pHicVqDg19PuQNO2b3udCR2ib17TthjagUSHogDuUBZjhykZV9LxG5DYAZueUvpbDTP5rrCVSDA/132', 'ueditor/upload/qrCodeImgs/201801191141132948.png', null, '0', '4', 'y', '0.00', '0.05', '2018-01-19 11:41:13', null, '2018-01-19 11:41:13', null);
INSERT INTO `t_user` VALUES ('289', '吴晓明', '1b4743fb481348dd8426935f42440197', 'f4ebc47eeb5c4867b3faa5b72c33e59d', '1', null, 'oLyuwxPdLyXyyntDzcPZUhsRczYw', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep8GsibYfjF12k61qBNr6IRssNXibdTonvBESKiaoMKpWF0FazC73mcFxYopfv7n2MVkfXo9ib8UbYkoQ/132', 'ueditor/upload/qrCodeImgs/201802141644026365.png', null, '0', '4', 'y', '0.00', '0.05', '2018-02-14 16:44:02', null, '2018-02-14 16:44:02', null);
INSERT INTO `t_user` VALUES ('290', '心尘', '774ae9a1d4a64e04887c8b320950b14e', '4b5abb2ab63f48828afcdb170a2c7d7e', '1', null, 'oLyuwxMK-KRElXz1Ys2HFkMIwE0Y', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKgNQbtic3oYSdSA2paXib0MfVI5H0JWw1q7fIH5eyyL8YfpM6qaFecH2x4YwUCiaa5TQT5ibUSlnKzicw/132', 'ueditor/upload/qrCodeImgs/201803171126396312.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-17 11:26:38', null, '2018-03-17 11:29:12', null);
INSERT INTO `t_user` VALUES ('291', '朱锋华', '2299a55aadcd4b7fbe4200f3a42818cb', 'bc17265375774ea0bd62d2f073355852', '1', null, 'oLyuwxDr7e7I_4hHOh2d3frkmHrU', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ2TJJNNSnKkEk8Oky6vFZBvV6PtGuR3D2CpMrlAd6znTz72NJNX5lukXEwUYtONwq4TJnQtwq6Yg/132', 'ueditor/upload/qrCodeImgs/201803181405224124.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-18 14:05:22', null, '2018-03-18 14:05:22', null);
INSERT INTO `t_user` VALUES ('292', '凡来尘往，莫不如此。', 'd7a8af6094174540b402589eb8596115', '4711b531646c4e14bf7acfcadf119957', '2', null, 'oLyuwxIsgJBVVcqUMrhWxR130VIM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/yV7HPeBAafia5HaWaccjls6pJ5xkzSc00my5ZpP4WPAMZTMAEMl3GpZ7BCCfjGq5d5QibQ27WDkAPZxKOjvPAtpQ/132', 'ueditor/upload/qrCodeImgs/201803191553038254.png', null, '292', '3', 'y', '0.55', '0.10', '2018-03-19 15:53:03', null, '2018-06-01 10:35:38', null);
INSERT INTO `t_user` VALUES ('293', 'wendy', 'bb5f35a100c2486290fa1d9ebf7fb149', 'eb6931ddaa2e420988f961ecae8bbe77', '2', null, 'oLyuwxGNrbDkNLV1n-S47FxipJUM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIaDqHQT3FyNM7PnzH483dG7WKVKriaplnO2P1xW1qOTq5Mf08q6R5gjOV4RibSXnXkHX6n72P8YL3w/132', 'ueditor/upload/qrCodeImgs/201803191622467269.png', null, '292', '4', 'y', '0.00', '0.05', '2018-03-19 16:22:46', null, '2018-03-19 16:22:51', null);
INSERT INTO `t_user` VALUES ('295', '华鱼西', 'a2b2eadc173b40cc9af3c385b24db98f', '575e54a69ba74fd8b5a4b10b428c2008', '1', null, 'oLyuwxDb8txydMeZO-b8AmCGaVrs', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83era0ibfHEVq9vDNQnw2ufoverVMbziaQJpc6bNhsUsFqHSjibAsANYNTqeFG6F88yL5ia4yrqghbLtHUw/132', 'ueditor/upload/qrCodeImgs/201803231354055734.png', null, '0', '2', 'y', '0.00', '0.05', '2018-03-23 13:54:05', null, '2018-03-24 21:56:40', null);
INSERT INTO `t_user` VALUES ('296', 'inrtyx', '45ca9f26deb0446cb9312ca10c874aa0', '5fafc518e40a44c99eee02c65f7908e5', '1', null, 'oLyuwxPoPLnvtAxC5Zd4oYgFEDbI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eptHG2NOOYgzJNicicJHPo5iab9MzXVAr08A8YRysib9IDodx420lWNWMZGORiciaK7kLm5tS5remKkJaZA/132', 'ueditor/upload/qrCodeImgs/201803231356039380.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-23 13:56:03', null, '2018-03-23 14:02:41', null);
INSERT INTO `t_user` VALUES ('297', '木&木', 'c170b4680eda47bfa8272ddd1ea54906', 'b73099879c814ad5813caf7aae2486c8', '1', null, 'oLyuwxHxl19cGbzPuMegbyYvooNo', 'http://thirdwx.qlogo.cn/mmopen/vi_32/pG1544PCU9f7D1SPJBQLEaP3qGqOmdHCiaUxRAo26M2Ky36VibQk9JbQRqs6PLGQiacsPOxYUib9JvTGCUxk2WswbA/132', 'ueditor/upload/qrCodeImgs/201803231517229272.png', null, '0', '4', 'y', '1000.00', '0.05', '2018-03-23 15:17:22', null, '2018-06-19 15:51:12', null);
INSERT INTO `t_user` VALUES ('298', '彩虹', 'de4f0a18650d4028a19aeb8c52be86fd', '2106bd790144436392dbf737496c9756', '2', null, 'oLyuwxLkYAaA2JmHakP1WCdQe2kQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJDHGxYQwyWUbOEJsawblwxsFtoyagUmHU7WmdIIN7Yngic3eib7hY79xTN01GuHY5uic3jJpoCowtYQ/132', 'ueditor/upload/qrCodeImgs/201803241410369459.png', null, '0', '3', 'y', '0.00', '0.05', '2018-03-24 14:10:36', null, '2018-03-30 09:57:31', null);
INSERT INTO `t_user` VALUES ('299', '你的名字', 'dbf6bc191e294c5e9dabe822ce581e1a', '39b76c81570a423bae2b583417827943', '1', null, 'oLyuwxBezc5us7d0HxrnCcvP5C7Q', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKrkibgwek5bcgiauhzqQNB9NT5yPmnZhkBGOQ1AL6hox3I8ntySKChTpCzdPxW6N3sB1b6y0kWep2w/132', 'ueditor/upload/qrCodeImgs/201803271239233881.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 12:39:23', null, '2018-03-27 12:39:23', null);
INSERT INTO `t_user` VALUES ('300', '格林', '4894ed89a0d74c169f28327b2b51cea5', '64c46e4e97c44c00aa2ff4af064bb0ec', '1', null, 'oLyuwxIJ2RYukncu1tbIJG0xZ1LY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIX3s9IlQoc1gZLUs6xavPYS4V6icwvXlgpReVpMc6rBSWmT91NISJkAFDz5wicoQ9Raw8Jfd9FA4Jg/132', 'ueditor/upload/qrCodeImgs/201803271240504284.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 12:40:50', null, '2018-03-27 12:40:50', null);
INSERT INTO `t_user` VALUES ('301', '飛星', 'e6b29f2418964e2c802b343fd673258e', 'a5507485a4454e199e18507b4567e9da', '1', null, 'oLyuwxD4vKXMcfs0uKWvCweyVRKA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTItSYutMYL9Ff1NVKJB4KakvOeq5kqdfTEtVJHBQhMK4vwOia43Plsl9xjJv5QcHFRo1yibJkrQMibAw/132', 'ueditor/upload/qrCodeImgs/201803271246208448.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 12:46:20', null, '2018-03-27 12:46:20', null);
INSERT INTO `t_user` VALUES ('302', 'benwang', 'ee0f9e36382b480796b7d3b189d8e0d6', 'fe63dc3e578d4ddeb65300a9ecbaeaef', '1', null, 'oLyuwxMwTcYbdciLJnuPgCn1jx_0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/RwG7JrdHrkp3lX3dickyiaB8V0NCto77uKu2ZyLiaj9xcwuKraqaeS5I1sO0nTQ4yjeyFbF7KKDzcVMUHudK2UCwQ/132', 'ueditor/upload/qrCodeImgs/201803271249164456.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 12:49:16', null, '2018-03-27 12:49:16', null);
INSERT INTO `t_user` VALUES ('303', '凡', '7dee330ffa1e49a9ba55ecfc35463ffb', '654a8c38bd0545bebe654f72954bad05', '1', null, 'oLyuwxJDbBpifNlkp7NQgEDMWPcQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/MzfKJP8K6qSviaIHYYWEPjrr3bhkmichorcx9r0QmFpXmwaZouXuzgwbibib4WibcOWCUymrjmbZ3qTyDuIyQnSZrvA/132', 'ueditor/upload/qrCodeImgs/201803271253017209.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 12:53:01', null, '2018-03-27 12:53:02', null);
INSERT INTO `t_user` VALUES ('304', 'Penna', '437205db1c3541e681fa5d1866527720', '097f7e6014bf41348226601ce3924217', '1', null, 'oLyuwxCZ8a7xg22yvk_L2dD7EEHg', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIU2cGfln4boicowmOn8Ufg8YCFO5Jl08Ycby1yusEcnltIdn15Q8ZTqdwhqb8C39Uezf2nWQ25RNA/132', 'ueditor/upload/qrCodeImgs/201803271307487798.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:07:48', null, '2018-03-27 13:07:48', null);
INSERT INTO `t_user` VALUES ('305', '林万伟', '86992d054fec4e7aa5a7288d2a13eaaf', '77cc45f9db8d4ca09a7dc3b6e887f3c5', '1', null, 'oLyuwxN_XJ6mi03Xyc1eU6gRKsQU', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83er6nHzeTyt7Au7UIXsuELjujSlmohFnesbzPGSC2s1JKVLu6xUCaKtmKeGAsX6kiaGw8tWkK3mXwng/132', 'ueditor/upload/qrCodeImgs/201803271330111706.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:30:11', null, '2018-03-27 13:30:11', null);
INSERT INTO `t_user` VALUES ('306', '指针🎶', '2bd5ff52c1fe408ab73bd6409dd8c668', '497e1c5da4db432ca3e9c8d12ca165bc', '1', null, 'oLyuwxGuwqC4f6aOz9OECjGWGrrc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJWmNaeY7bzAEpQKATlEIibLNRLGYibzxDsDyAmO3FoM6ydMBnTM7ZyKlLykThhMuKQjoRPcdichLD8A/132', 'ueditor/upload/qrCodeImgs/201803271332465596.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:32:46', null, '2018-03-27 13:32:46', null);
INSERT INTO `t_user` VALUES ('307', 'LB', '8d636e7d7dc44925a276a876d7728b6d', '74f9016ec46540499df255f9a4ea4ae7', '1', null, 'oLyuwxHxafNQdH1gWcUQ6DFBnyvI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLBLsFDricL2jBuY0wZ0Y5cXDMeBH2jZXnKmKOlYdpEC7wiagKLibL3RUrwqB6XVTOACZRH9ohJMQpoBw/132', 'ueditor/upload/qrCodeImgs/201803271337297978.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:37:29', null, '2018-03-27 13:37:29', null);
INSERT INTO `t_user` VALUES ('308', '可乐', '6e1b82e0915c4a9b812c755b0cbbd91c', 'c5c4b8448271487ea15378d984742858', '0', null, 'oLyuwxHPMVyjvDMf8PjJ5S4TyIbo', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIBT4wprjtw854erbAOYAIicjibFSicXX9KLEibI00cw1w9E3f6ssowSKfYWXPFHToALaTBzF286eyo5g/132', 'ueditor/upload/qrCodeImgs/201803271340471821.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:40:47', null, '2018-03-27 14:16:44', null);
INSERT INTO `t_user` VALUES ('309', '滿血復活', 'd7f469e1758c449ca81209a44d8747db', '9025b16314c14d38908e02ff61f75c8a', '2', null, 'oLyuwxEwvR1qbc1wQkbNSJtuhOmU', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKD6bl87Ug3esWA2zX6rukYMFqHt0877qvkugY2RuYNQtpWwFTxVhFs3mfcmPibuzmqpdNLhXemdfw/132', 'ueditor/upload/qrCodeImgs/201803271340557717.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:40:55', null, '2018-03-27 13:40:55', null);
INSERT INTO `t_user` VALUES ('310', '洪艺琳', '4d43744bcd6a4071b66af2947c0d2007', '4d457914b7e94dd086d79f11a83bcab1', '2', null, 'oLyuwxGbI7mbI5xnw_7_sDqzGG-Q', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKFgKm94IvjUIXlpJFMyXe6461MOzrvwXtDKmekuIhhutVicmbyxsjXibw1Vyj2LiaNMT0JtWIRnt8fw/132', 'ueditor/upload/qrCodeImgs/201803271340576394.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:40:57', null, '2018-03-29 17:02:29', null);
INSERT INTO `t_user` VALUES ('311', '封里', '0febba178f7640cd94af293cd7e175a1', 'd2b7834148714943bdf527e930df890e', '1', null, 'oLyuwxI5I8ebh1PNXD3mdJN18OXU', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqgiaKGo2ibEtuMuWicicJbcVeZS0Qaib3E6QQdlAQoCticSnfWIsyhaU4v3RVrxywicPnIk2AkuGaicjwcow/132', 'ueditor/upload/qrCodeImgs/201803271341399197.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:41:39', null, '2018-03-27 13:41:39', null);
INSERT INTO `t_user` VALUES ('312', 'Paramita', '4f6493d4e3544799ae173d6f73748149', '4f8765477b004d9dbd3fdced23c4eaf8', '1', null, 'oLyuwxED8G-4Kc7gwPDp9RW88_Kc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKWNnaDsHcMqCXGSL6eS8yCMrIc3wH67f0pPicOl2YrDibN7uX1O503krlKEbyASCMfuHhvT2hdju4Q/132', 'ueditor/upload/qrCodeImgs/201803271343353923.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:43:35', null, '2018-03-27 13:43:35', null);
INSERT INTO `t_user` VALUES ('313', '阿观⃕‮', 'd9662f7e06354951820e3a6245e5ee58', 'e3fc3281a5784b13b6e106abe828fb49', '1', null, 'oLyuwxPQqhSlQENrWCOkS6cqVsDk', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJ1pAXDtbsWDdicnRX94NfVs9Ck9HecDBAkuuN3Q3YmJgBGlMa0X2rcZxoEAD9XVcWAtR0ib7Q19ZzA/132', 'ueditor/upload/qrCodeImgs/201803271345198315.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:45:19', null, '2018-03-27 13:45:19', null);
INSERT INTO `t_user` VALUES ('314', 'zmr', 'e74f5388dcd14c969df31127fba437d1', '159394dfec7e4f15be5f648278e7ec80', '2', null, 'oLyuwxMSfHo4eDnXxoQQATYoUg2Y', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJPLEHXRlRyNtwdjY0a4wicD8LIAicgBQEvTOScPfaz4ozgN3oeyUDWBuLbwMzt2nqrPnq7qTB3cLVQ/132', 'ueditor/upload/qrCodeImgs/201803271345348796.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:45:34', null, '2018-03-27 17:12:18', null);
INSERT INTO `t_user` VALUES ('315', '刘可利', 'd4f0998b75524ae5b6acb9752c792ad0', 'b58e433cfe614fb4bbc0b697dc6d2d9c', '2', null, 'oLyuwxE7mYnWa3Ly-W04644LtuQw', 'http://thirdwx.qlogo.cn/mmopen/vi_32/B6l9HbWQl7PZXfBDRj28xhibL82ITy50lsoy3SCantpZ9W5scDHvNc9BbTqx3Ria8wG0SOic8BuwR4GzV9Xicu9fsA/132', 'ueditor/upload/qrCodeImgs/201803271347395402.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:47:39', null, '2018-03-27 13:47:39', null);
INSERT INTO `t_user` VALUES ('316', 'hemmy cai', '35fd43325e394c8584990dcdab940a03', 'dee4db6ee63241a68b3d20ed2db5a111', '2', null, 'oLyuwxDxVQr3ldVNWu9L9SYcQr0M', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKXMzwCdjicCTSGYyKricjNBRsaXYPP6uw0v9thGC96NsLr0pZk30ZDLfVWU24iamSIMLxJJBrlR3ia4Q/132', 'ueditor/upload/qrCodeImgs/201803271351326300.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:51:32', null, '2018-03-27 13:58:02', null);
INSERT INTO `t_user` VALUES ('317', '上善若水', '797a5ede17b04eae871bcd710ce1827d', '852161fd220e4e109f75302fdc96640c', '1', null, 'oLyuwxATYY-UNO00G2RZQhcqgyHc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/poT8r9mcT2sBOp7IWa48PAXXHj1VkYENPQet0BrW1ToZHfoGYLduasH2Fcq2GGbDia51yicUufYj5vn2NWZIREjA/132', 'ueditor/upload/qrCodeImgs/201803271352427621.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:52:42', null, '2018-03-27 13:52:42', null);
INSERT INTO `t_user` VALUES ('318', '鬼爷是朵狂躁的太阳花', 'f563c0d899f7418eb450de1af9c56f8e', '8a071f077fa141fbb8b0e6580127e4b6', '2', null, 'oLyuwxFph36tu2JEMCNEZ6Ur7Syo', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLhTicgypR7aZgp61HP74yHSp5glo7PGribiadicfU6vSONTkia6uoyr8Q6GvAyh1Oibt9CUzR49Rjrviblw/132', 'ueditor/upload/qrCodeImgs/20180327135305639.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:53:05', null, '2018-03-27 13:55:59', null);
INSERT INTO `t_user` VALUES ('319', '王鸿彦', 'e2f963eb46404dc088b3c64c89aa78bc', 'ee1bdc2df4de46469002405610d0c291', '2', null, 'oLyuwxLF5WBTl7uJ8PqdfL1ixmoQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK093QtDzCgKlvBy7TMRzDFgiamN8unuucokrSNM3iaW2BmqL2hhARzFmvdBVCgzPSk5ANESScibiaicFA/132', 'ueditor/upload/qrCodeImgs/201803271357003777.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 13:57:00', null, '2018-03-27 13:57:00', null);
INSERT INTO `t_user` VALUES ('320', 'shawn', '64821902b31e494e8422d3563fc63cf3', '1e912974f5ef43a6825a2fbe4e1a7196', '1', null, 'oLyuwxGjVngUwUbU124dFZzAF0eQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIhseUSvPCfkgWwULAMjeYibganicKonpFYJArd8a7CLetEJ0cjHmw6rQ2XibSI2ibgLt6n3noKuic6zmg/132', 'ueditor/upload/qrCodeImgs/201803271401015054.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:01:01', null, '2018-03-27 14:04:19', null);
INSERT INTO `t_user` VALUES ('321', '一知半晓😝', '9f2b8bd261514cb2b3e024bce3991738', '4d6b724855ec44be9e8f689b149cc5c2', '1', null, 'oLyuwxKL8pMyctwR9m-i5Uj-o06Q', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ4eNPwia9NQ3F5FqWmwvcGqfibTsm3UAw3javmhcpAjUxB5fzXlGicfkQibTuqhNH7TECp7TqZofcAZA/132', 'ueditor/upload/qrCodeImgs/201803271401422451.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:01:42', null, '2018-03-27 14:10:00', null);
INSERT INTO `t_user` VALUES ('322', 'aiyaya_o', '3b918b95f6014f2c876245c3622a69ea', '67c0deb83858413bb592dd1b459bdb9a', '2', null, 'oLyuwxFcKaks_RsWf9bIUB64fjH8', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epWyhRs1JP1RNMmsJxzkWEhmBzWK1OpcFxPq91iaBCUOIF2zF1anvOB2p5IzeoRrI04uK9t7fm4xjQ/132', 'ueditor/upload/qrCodeImgs/201803271402581238.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:02:58', null, '2018-03-27 14:02:58', null);
INSERT INTO `t_user` VALUES ('323', '紫风', 'f9968aa7d257413baabbde814f07e4f1', '3bc2ab6fb55744c1a46c66a24c05c0d0', '1', null, 'oLyuwxO2qCsWtkazMvZT06AQzVYA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKN1Ot5ibrSUogfSA8XTlv7kUsV2HHjvu3Ve8fX2RXGssRL396MoQiaj7GgctQPBfAsUzCRcr0zWcSA/132', 'ueditor/upload/qrCodeImgs/201803271404267113.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:04:26', null, '2018-03-27 14:04:26', null);
INSERT INTO `t_user` VALUES ('324', '啊哦啊哦', 'fb50ec7bab3d4a9d8f71dacfd16747d6', '9359e4e8a3b346e0a8e6fe3ebb3011d1', '2', null, 'oLyuwxCmvfeMh20EW7yS4DRZcpxE', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLtnrnqbbgaQLC1QBTlbia2Cfa4SL9uJP13y6icHJ74x07ibcibV6fdMiaKuAT2MN04gZe4R8XhXHoQHSg/132', 'ueditor/upload/qrCodeImgs/201803271409336692.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:09:33', null, '2018-03-27 14:09:33', null);
INSERT INTO `t_user` VALUES ('325', 'Irene_猫', '483696cbecd043cba972e2a317334e0a', '463e60f5e2044a559de59594f704e70e', '2', null, 'oLyuwxIPF-2wdauyfmndV1g_F3W8', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eq6K9UM66lAl7E0OFuCJyLBXDahLBOumTYvW3xLibkYWIqRYbcdIu1fpdicbWV0wL86UoP1caD8iaUcA/132', 'ueditor/upload/qrCodeImgs/201803271411394073.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:11:39', null, '2018-03-30 21:33:23', null);
INSERT INTO `t_user` VALUES ('326', '蓉', '4729f1ec55b440ec8f9d0d39ca7297bc', 'd1f6d3c4cb004437ad20633ef3674165', '2', null, 'oLyuwxAE4R-N8vChuQZCCw0d3IuA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/YYPPXNfPXfW1JAxUuqiaBpxOkkqfkJnsoVqUCx7VPLerTRmNTj7n30JygibwFYzDQ9ibnDQpMJXFYOsLCXPbpBUtg/132', 'ueditor/upload/qrCodeImgs/20180327141157107.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:11:57', null, '2018-03-27 14:26:57', null);
INSERT INTO `t_user` VALUES ('327', '粽子', 'b1d0d7a24e384f2288f248ade41db07a', '0a6c0749c0cb4c62aca2502108fae2ce', '1', null, 'oLyuwxJsKrdUp8Sw4usq8X7Y7dPA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKWXDSUibzXttIVUiaGAtfSdkCbtDZHWd3DTFo4MW64o4tsnnHpQTT4RlMXE9GaXvYBx9KzFREW6jzQ/132', 'ueditor/upload/qrCodeImgs/201803271415071173.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:15:07', null, '2018-03-27 14:17:21', null);
INSERT INTO `t_user` VALUES ('328', '梧桐', '64d16dcb4f6043e69c2670137f5e9aca', '4f9cc9275c26460e996aa35e75368f9c', '1', null, 'oLyuwxFv2WrBSoJ0bbdONJaYKf3U', 'http://thirdwx.qlogo.cn/mmopen/vi_32/doMXR1kVhPFfWGkgEsS9jwicicUeCMMI9734QEbicpy8LPWhCEXz0diaBrkCEUZ3JIQjTunDAmn2udxeSOst8loEiaQ/132', 'ueditor/upload/qrCodeImgs/201803271415554699.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:15:55', null, '2018-03-27 14:30:57', null);
INSERT INTO `t_user` VALUES ('329', 'shaokai', '55c60a5ae0ce41288861ae418059161a', '7970ad855cef421ea3e2c4e5394fbe5b', '1', null, 'oLyuwxBInqcKSJBXynko2_NAZuzs', 'http://thirdwx.qlogo.cn/mmopen/vi_32/yV7HPeBAafia5HaWaccjlszda54nILjDUwztE6j3NUXdCBZMqUrEBf4T7NOt099rKWexibO26AxcSVaOS7oZcFIw/132', 'ueditor/upload/qrCodeImgs/201803271417422693.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:17:42', null, '2018-03-27 14:29:56', null);
INSERT INTO `t_user` VALUES ('330', '猴子🐒', '35fccf59164446feb6c8ed1eb6f75b28', '4e24bde50e8c4a0cb9410662f84edab1', '0', null, 'oLyuwxN8L4ahZZM_6RbcvcpSgD9c', 'http://thirdwx.qlogo.cn/mmopen/vi_32/2Mdk3SiacBZhudQD96KyewMt3hcCSvvQia5ET9AiaAWGtw4jtHKiaxicjOZCplM35dcF35eMz9IND8icysvr99jfXcibA/132', 'ueditor/upload/qrCodeImgs/201803271449055962.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 14:49:05', null, '2018-03-27 14:49:05', null);
INSERT INTO `t_user` VALUES ('331', 'Max', '962a7a0453874302b8d1bba2780d9d14', '3ab155b54e424deaac97d58386f3f7e6', '1', null, 'oLyuwxKM65-iYpdvbGo0h33JCqZE', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epINK60ak0ZjYzVQ2iauKdUG0HDgYrNVx5cjjPFibCe0WdiayjibWZk3icXFDqSapJ78gs7gN7G2991YRQ/132', 'ueditor/upload/qrCodeImgs/201803271506379503.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 15:06:37', null, '2018-03-27 15:06:37', null);
INSERT INTO `t_user` VALUES ('332', '花布雨', 'e163d28d4a19444c8872ec5f6320394a', 'd3711ecdff9f4e23a79ca0be1d5180bc', '2', null, 'oLyuwxOVoS_aDo56RQ9obMWe3AJs', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKmW5VOrLibiadj7iacibK4jy27p9WMmDjolLksy7ibIADdBwslicpGGKlviauqYZ0IFDCZYVAsJbqg43tAg/132', 'ueditor/upload/qrCodeImgs/201803271508117876.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 15:08:11', null, '2018-03-27 15:10:45', null);
INSERT INTO `t_user` VALUES ('333', '布丁Doris', '09bf826724da4bf59d44d16ad774d337', '9ce74b08a36145f985058e4cd58625dd', '2', null, 'oLyuwxHb8HGjZ-pMbIXcvPeEGmPc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJfhcpyAQYCcj2xaMeveNI3HwVTZcIAVJU8ZjhpRjm472KyOlAM1ibt0ZswFo7gdQmFJiavgdiad0UsQ/132', 'ueditor/upload/qrCodeImgs/201803271541459459.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 15:41:45', null, '2018-03-27 15:41:45', null);
INSERT INTO `t_user` VALUES ('334', '星屑回忆', 'a738698ad3aa43158399821436741e08', 'b85fa1256f4046bbba1116d7b41b9950', '1', null, 'oLyuwxEHSx-UxebMxzOw8kl7rqWE', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLmQKiatJc5RavEYc2tEUNY0IseKpcIRPyLISSIShcReWhAOicJiawgqZgwcpnk9Yic1qRibicd0XhAo31w/132', 'ueditor/upload/qrCodeImgs/201803271546103903.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 15:46:10', null, '2018-03-27 15:57:59', null);
INSERT INTO `t_user` VALUES ('335', 'shinee', 'bf80063c8dbc4afba5cc4dcbd485812a', 'd8124b24149e47a4a913745e4f47bdc4', '2', null, 'oLyuwxDqjOsRu75Jh-4Phf3D3SVI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erKJb12UoiagIcRKAjWNleB3gicr8Hsm1vNL00j9SbJGSvtbpoC31qXZsDM6bWQbicURWTYeAnEVzIqw/132', 'ueditor/upload/qrCodeImgs/201803271546249201.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 15:46:24', null, '2018-03-30 14:05:24', null);
INSERT INTO `t_user` VALUES ('336', '嘻哈盒', '687c4cdb79874ecf9ee6a468b16f4681', '1321eaad52eb49918575435acb3624fb', '2', null, 'oLyuwxAwGRF5ODh6D0EfsLFIrKaY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/CXFZVMTfG3U4iaibvq7jnLaS9z7ZjsNDwTd5JgQJ66EHWL9XicB9xA1nJdqsVZyiaibIOicckZM9RsNQoUD8Ywd3K5ZQ/132', 'ueditor/upload/qrCodeImgs/201803271623329016.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 16:23:32', null, '2018-03-27 16:28:08', null);
INSERT INTO `t_user` VALUES ('337', '田二姐', '4bd8409b490f439e956d4d126a61b125', 'd4caaed83f874dd1ba13789cbcbfee7c', '0', null, 'oLyuwxJFiUBgiWxKryx_Er75E_gQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLicy0x4gnyq5vyz3aBt8tiaeu9KgKliapg9VsvWyfM1jcSGSaZhiaPcWMvAPfQibib6DZ38Wlr1JM7B1FA/132', 'ueditor/upload/qrCodeImgs/201803271627039111.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 16:27:03', null, '2018-03-27 16:27:03', null);
INSERT INTO `t_user` VALUES ('338', '许碧娟', '40d32cc09d0a4808ba7eb1b2bef4e7d8', '546775ed8e5c4d5aa88620411e4dd3a3', '2', null, 'oLyuwxH9L4ypz99Lq21HlPsXUcYc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLeHuIR5ibMIqhO40dZP4WWjWlEaTCw3DzrCuKEF9B6ciaf4IekBO2O6ccOnB34qTjc6cIXY7vkLCYA/132', 'ueditor/upload/qrCodeImgs/201803271648175974.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 16:48:17', null, '2018-03-27 16:50:14', null);
INSERT INTO `t_user` VALUES ('339', 'potato0', '2a7e416fc3304c85a90c5a8c467c76ed', '43ce36803e42456794bd6ff69b73c5c3', '2', null, 'oLyuwxP-wccCtc-6XNO9ZfbqFLoo', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqAhmAWzP6Vic6uqws0BWhpeVEHjJY06E1d0GVnQkwCiaictaL9bGOdkRy38GvKa0YhqkibGGSwfySPNQ/132', 'ueditor/upload/qrCodeImgs/201803271651274606.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 16:51:27', null, '2018-04-01 01:13:34', null);
INSERT INTO `t_user` VALUES ('340', 'NULL', '7a3327030c014eaaaf8c871c3e05ad9b', '890a98f55b8345d8a315a9dac74c8c9a', '2', null, 'oLyuwxK4Zn7GTJQOTOetTZFnvaec', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIiczhmib6c9rEDCe1H4kLMQqlB8g2sqkgU8zstq6rEibibKT9Ukgm0IAKGjfXKtABcx1q7OpdgcC9y1w/132', 'ueditor/upload/qrCodeImgs/201803271721467528.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 17:21:46', null, '2018-03-27 17:21:46', null);
INSERT INTO `t_user` VALUES ('341', '成长-QJP', 'b0be686e321f42c5be77a9dc12e1d9a8', '1e80993d4bb849f8b160509a668b7627', '1', null, 'oLyuwxMJqu3alOgsudhy0ev6BxRc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI0bWr4cFvOhmJqcDCKACOr6cBzqibxgiaUN3ys6jsKBr33NTHxgNRc28DRIv8KU5yjsTbP11Hia8elQ/132', 'ueditor/upload/qrCodeImgs/201803271743589375.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 17:43:58', null, '2018-03-27 17:46:36', null);
INSERT INTO `t_user` VALUES ('342', '乐', '0a8e0c87fd064a7f8a0cbd661adc5333', '3df41b99ceb345d4b651d10e4ece5e70', '0', null, 'oLyuwxEgpO1CtkJosC_qBsCPt7T8', 'http://thirdwx.qlogo.cn/mmopen/vi_32/qjR3CicwhKDzgUe4tsgsZenWuxVqkKe4UJVkCrR62Hj7OicloxeeIg392X2ia1juUgFTrhXPxicpiaqZAECFVP9dib8Q/132', 'ueditor/upload/qrCodeImgs/201803271750108728.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 17:50:10', null, '2018-03-27 17:56:24', null);
INSERT INTO `t_user` VALUES ('343', '白鹭', '610f34e72f9846568c77c733be91788c', '49d5a517f4d94baf9438d2b43bc3672a', '1', null, 'oLyuwxF2gmabm7_IHVgqGO5J6ccY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/9KBexE8cX9enXwEARiaKG2BSm3kAXcqyRXJm4d1uhiaibo6icHHmKFzNAURNWphWxmNibNgtBibHOrrkKUz6EEhZ72tA/132', 'ueditor/upload/qrCodeImgs/201803271903029269.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 19:03:02', null, '2018-03-27 19:05:58', null);
INSERT INTO `t_user` VALUES ('344', '娟', '6faffb3e89884e53a5ca2fd25b44a76c', 'a980dfd53bdc453ba38fd7da0eefb325', '2', null, 'oLyuwxMwIzLx19Uu_6I-CTqyLlCY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLfdgqbibiamQXFd9xibosfOvFdGH9D33pFoQq2icpCnyvNQ4Y5AJqicM1hoNlLYPSQOnemL1PXSHFZE3g/132', 'ueditor/upload/qrCodeImgs/201803272151043808.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 21:51:04', null, '2018-03-29 23:14:53', null);
INSERT INTO `t_user` VALUES ('345', 'lmc', '8b0628021f71475685323ec51f157aab', 'cc27f9ddb8b74d0fa47dde77d3b96aa6', '1', null, 'oLyuwxO30JOCpmCEUAvNXw03GG4E', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIicYd6VhHscYqGfJia6BkkeBwhxG8PKWjW23nDiaobt4vn1oKicRp3aiag2FURRmNgic8ibicYmkZJ4yGoaA/132', 'ueditor/upload/qrCodeImgs/201803272258152318.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 22:58:15', null, '2018-03-27 22:58:15', null);
INSERT INTO `t_user` VALUES ('346', 'singlo_lam', '0b25306086aa41998aa737c371f7571c', '86b5fb652285441e82f272dfbf38cd79', '1', null, 'oLyuwxH0jmYWNxKe3u_FItSBGDA0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/0g4hxNeghDH5pSpicibLT4Qt4f5fLWnGFfKyNcibK8AS1EZPwQwS1M2GcHMXaQMvc5yjia47U1jIEEicXM3OMZFJlPw/132', 'ueditor/upload/qrCodeImgs/201803272318254466.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-27 23:18:25', null, '2018-03-29 16:35:03', null);
INSERT INTO `t_user` VALUES ('347', '迷途屋', '2d3a05d4f5724cd8af853c5fee74a0ec', '475f9a013080466baa565762116ae80a', '0', null, 'oLyuwxPBDYv4o3iemyZ5QUgMBuuc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqk7zaqpGsU31uiaQLlC9oCM34lB02o0nNNhqw2BpqX6YejSaRicDV7Nhqjmt6Urkicf0zzf2ibTRrTaw/132', 'ueditor/upload/qrCodeImgs/201803280012113420.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 00:12:10', null, '2018-03-29 16:15:40', null);
INSERT INTO `t_user` VALUES ('348', 'jared', '84aae05bf5fd463fac39210b97ab3db4', 'dcbc2b78114c4d3fba6d9ef9f0a43d09', '1', null, 'oLyuwxL8WZUCHEUX0PY4vS7G3U2c', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo6rYC8Yu4pnWlX83OClESmgppW7z9vpco465omQhwg5jVSC1MDJibUibQA0l4BrIjR7vnv4C8BpvrQ/132', 'ueditor/upload/qrCodeImgs/201803280851488247.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 08:51:48', null, '2018-03-28 08:51:48', null);
INSERT INTO `t_user` VALUES ('349', 'Y_IceTears', '65e06659283543f19ccf14428b34878d', '4e736a70ab40448f94c8efcb7d586c63', '2', null, 'oLyuwxDlJrZVnOLd8bwQ5e5ByKgo', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIg5UbDvGTO4QN3MeVJStYWlnDvrhu4VUuIF74LnVXDU6UsmqnrxYdPs2hu9kFQubNxWx6EdA77mg/132', 'ueditor/upload/qrCodeImgs/201803280901323739.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 09:01:32', null, '2018-03-28 09:01:32', null);
INSERT INTO `t_user` VALUES ('350', '//--', '7642f14fca9445719f15f4a710fc8065', '92cf02c2649248359ae2d5897fdaf291', '1', null, 'oLyuwxGGGqR61NkkHpw3LrAqRrxM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqNFFicEd8aBYleRHhmdquZYybefHj1iaoWEIMNUiaqsStEgo42wjmP4JsRDP8eO3ic5lswe7lX9VaibTg/132', 'ueditor/upload/qrCodeImgs/201803281017092717.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 10:17:09', null, '2018-03-28 10:17:09', null);
INSERT INTO `t_user` VALUES ('351', '陈彬', '7f8b8115443e4899ace584295646b7cb', 'aba1f763a4374ab18d858c2e990b3443', '1', null, 'oLyuwxMmvs356977CL0b8dq7lzms', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Ugc3Md3aNVb3mGGZJXdIBicHXeHtaz8VuTUIjUYNDhxhjqnyNYB3FFjVnkdslRia5DF8e7LwflsjjGJzoOvG4aNQ/132', 'ueditor/upload/qrCodeImgs/201803281249144133.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 12:49:14', null, '2018-03-28 12:49:14', null);
INSERT INTO `t_user` VALUES ('352', '张文章', '39e39c84dc944500a1f09e093a07bc59', 'bfc13b040a464f42b6dcc4d33b1fdac9', '1', null, 'oLyuwxHJ2omyg7XQ9blzvBu-v8Ng', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJe2dJTCGabCV0f3aISpBmMbyibf8kol69h933nXLXhUtHydVeMQMarFmAtPYZ40G4lJxicEqHyMJUA/132', 'ueditor/upload/qrCodeImgs/201803281302077781.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 13:02:07', null, '2018-03-28 13:02:07', null);
INSERT INTO `t_user` VALUES ('353', '蓝精灵', '4681d47917fb4ce1a56f3d6699c47247', '6f87045c6123455b9f6d2c92f6e5b3cf', '1', null, 'oLyuwxPkK4E_teA3hn4x73JaqCM0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI2OuGib3UJqEMmxSXxsFh5vgVoj4a2W3GGWDVQo38flgd1BhUjJf4u6LepjNfNlzUMKyYuFFWBg0g/132', 'ueditor/upload/qrCodeImgs/201803282044533359.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-28 20:44:53', null, '2018-03-30 11:55:45', null);
INSERT INTO `t_user` VALUES ('354', 'EVERBODY', '54d886fc56fe4347b11961143b770f95', 'ad0425ab22ec41519c3792d6e5ca0272', '1', null, 'oLyuwxHP9myjIDo8uYcqnxkPlfQI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoBKbayJeSYu5icwcBHTPIB81MI9GrKUcY5CicvzCz3lT3e1GNteiaopaMX2DSKXWvTTKDSPFUkZHJ6g/132', 'ueditor/upload/qrCodeImgs/201803291121469566.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 11:21:46', null, '2018-03-29 11:21:46', null);
INSERT INTO `t_user` VALUES ('355', 'LA蕊', '5f4906b60cf249f9a81fcae4a0df5b12', '388ed813386a4fe19fa1f1779d17317f', '2', null, 'oLyuwxDaRP5-PTJkug51qdkQrsCg', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLDHvPiaGuC7UeEdibgDicwZOwnuKzTVwQqrYjFkKzVFFdQfroL9iczsdrmDKwcjOrlib5FibzDSxUria8lg/132', 'ueditor/upload/qrCodeImgs/201803291232393964.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 12:32:39', null, '2018-03-29 12:32:39', null);
INSERT INTO `t_user` VALUES ('356', '萧志勤', 'da97377d4a7e4c5ba6cf9dddcda0c642', 'a8dfb8678607496a94b91903fea1f8ed', '1', null, 'oLyuwxCc8AAAROATCibSL0PvnhRA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erb11l2hVNib9qQFeouS1vsqMy2oQFDUTnFN1zC4VUjL19tUuk86DHf6LSz76dVFlh0drtNSD3icqJg/132', 'ueditor/upload/qrCodeImgs/201803291236362851.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 12:36:36', null, '2018-03-29 12:36:36', null);
INSERT INTO `t_user` VALUES ('357', '123', 'ea9ae0b48443400abd1b04dd75f16647', 'a73093688a3f452f86ae7dc3c7a5b9df', '1', null, 'oLyuwxPFm--uXQGK1bu_kaU9TGow', 'http://thirdwx.qlogo.cn/mmopen/vi_32/oTibKaayyw76e3q5J2rpFibEcBc5yicjYUiaxlicrZkTcPjcic3aTutUcqBEp9wnckiavCicvIPnadpAmCmibgUDFA7sxJw/132', 'ueditor/upload/qrCodeImgs/201803291308564841.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 13:08:56', null, '2018-03-29 13:08:56', null);
INSERT INTO `t_user` VALUES ('358', '小鱼', 'a8e1c66d32a1417bb0fde6d464063f2d', '6557a9e77f22435baa8a2c3d5463e91a', '2', null, 'oLyuwxAAPTYu1pVa2pnaJSudjXrU', 'http://thirdwx.qlogo.cn/mmopen/vi_32/uSdKGpGGH7uVuZ6O6A03aRND5kqbcI7Y1czwWLibHuuAHKqZ4HRgNp6icEFHy2RCdxLTDFbM8CJGMeWNRupBXp6Q/132', 'ueditor/upload/qrCodeImgs/201803291346564290.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 13:46:56', null, '2018-03-29 13:46:56', null);
INSERT INTO `t_user` VALUES ('359', '一切皆修行', '626fdaeaf90c4149bafaaa2a859de725', 'b8d78961bd094b6d9b18d1d7884af443', '1', null, 'oLyuwxO-mkbezmNSuJ6Pkp6-w6dI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLHMNVzsFj4oDQtyATMQo48fbCvM4Sz2FzTib2vXBlw2oAb9mdpxR8hGicnludT2Q8m26zgp0ZQQcHA/132', 'ueditor/upload/qrCodeImgs/20180329134821460.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 13:48:21', null, '2018-03-29 13:48:21', null);
INSERT INTO `t_user` VALUES ('360', '淡然', '93633715be1147d18d92e0a6768cf606', 'a875035cfb4e443f884713c19ba95f6d', '1', null, 'oLyuwxNIqYc-5fjFrQ8oSEl3TogA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epAAyrP5icEfTMMtDiaUJicaugqSM0ibhFEpoZAibu9laQ6iaibkfR9rSC2fCKWsr2npLcFFD0k0f0Bg7jmg/132', 'ueditor/upload/qrCodeImgs/201803291349385811.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 13:49:38', null, '2018-03-29 13:49:38', null);
INSERT INTO `t_user` VALUES ('361', 'TinaLin', '83dfdf588a284171976d1bb325b6d538', '9a85211778dd4b31a7b8e78317b5ffd8', '2', null, 'oLyuwxCNGKpfYafcclVdenOKI6t0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKRWNGj7GTFFcogn1qGknjh3rZQh8JZ1ZCUrDrLFCFKwAyg93WjrorZaKTrrAdsfXFvT6DRoqicz6g/132', 'ueditor/upload/qrCodeImgs/201803291405545298.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 14:05:54', null, '2018-03-29 15:08:26', null);
INSERT INTO `t_user` VALUES ('362', '抹香鲸', 'ac2530f0fa2e4553b3bbdb57594edf8b', 'bb5d210decbf417f8bbaa61b3bd6eba1', '1', null, 'oLyuwxFq1wezG85dT_9s1ZOE-m9c', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Y7Jwr3lK1y3Uia3QUzYNAXaNPsRZ7GJALNXqmzmlUw3O8dnicuFf0ZqDjic9GSYBv9pLwlb1NGHOicE8Zp2peaoWGQ/132', 'ueditor/upload/qrCodeImgs/201803291409559991.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 14:09:55', null, '2018-03-29 14:18:15', null);
INSERT INTO `t_user` VALUES ('363', 'chestnut', '3efe92f003134aa19034e6ebe204082a', '913475cf8802411e80d7cfcad184f858', '2', null, 'oLyuwxGz6bUJy7Q9Eo1lVqHVG9ro', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLp3LCVTTsj2D061wO8esdDOwiaOY0YFXpgSsUyG2UIZj7GNqUBVbSIBxEvmABwOw5M1aNM3u9iaemA/132', 'ueditor/upload/qrCodeImgs/201803291520482719.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 15:20:48', null, '2018-03-29 15:20:48', null);
INSERT INTO `t_user` VALUES ('364', 'Tina&Nina', 'ac751ded799d4d88a4f85081f3c4155d', '6b523a7841c64e48af10f98e7fc51649', '2', null, 'oLyuwxHwnUiQvsbV-UV2JhjFwJQA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ94CQE90lF6ArU5IibZkicOHliaqjfzWianMIhyAgSQ1ibib5hiaRwo6ibop0fH6FgV7jcmib7fH8ofsWicCHg/132', 'ueditor/upload/qrCodeImgs/201803291521335953.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 15:21:33', null, '2018-03-29 15:50:13', null);
INSERT INTO `t_user` VALUES ('365', 'Amy', 'b46249179086469fade16acfc38b69a7', '1ec828e71ea640e887d4bbe3e8e999b3', '2', null, 'oLyuwxDeJ3qkQngqb-ZXOQSiu_JE', 'http://thirdwx.qlogo.cn/mmopen/vi_32/u9Ij9vTLtBCmGw3J5Qumu61zzppGhuKmFicb377GBHric1RvVrpaklvo6VaIK1pluyWzsLDdeoCZ9GveMDD7PkIw/132', 'ueditor/upload/qrCodeImgs/201803291522117071.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 15:22:11', null, '2018-03-29 16:37:35', null);
INSERT INTO `t_user` VALUES ('366', '枫叶🍁飘零', '98cd1dd1d0b2473c967760512a925c97', '7cffe458f1854f37ae0cec6b637a0dd2', '2', null, 'oLyuwxIoU81-UCDI50lDDBacMwzQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWQVeTqM8HCwDLu1h1h3MscoL5AaMu5YvZE4QicrShVR1JIwCoxvLDZ7YfbSjoEdjEMbP6fSSJRzg/132', 'ueditor/upload/qrCodeImgs/201803291538032311.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 15:38:03', null, '2018-03-29 15:38:03', null);
INSERT INTO `t_user` VALUES ('367', 'Sam Su', '2f38380b734045deb281aa342ad47ee2', '94277dcede2548aa8ccc737b90eaea04', '1', null, 'oLyuwxJAoqAU7SsG6Dr6siaX4PiI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLns60AgRWAiaagnnytO6xnuIcoyg4J9XulQk7siaqoJF8Jrm9xFrCJ6IXBrlPeqYIaicQ0zG1DtIdibg/132', 'ueditor/upload/qrCodeImgs/201803291542255167.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 15:42:25', null, '2018-03-29 15:42:25', null);
INSERT INTO `t_user` VALUES ('368', '飛魚', '300968f61dbb493489ab79e6e924154c', '1de695c12c6f4ba5b72bece3a22d3e89', '1', null, 'oLyuwxN3AJIwetKQPX3xLYrFfQ6c', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJV8uqHiavia0CO6n97FUBr2JOOibG2fkyksdNticD7BNDuPu4lia7cQ2ia08bZKafuyGzNd5olrV43G08g/132', 'ueditor/upload/qrCodeImgs/201803291548502048.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 15:48:49', null, '2018-03-30 01:21:39', null);
INSERT INTO `t_user` VALUES ('369', 'Henry', 'e2a59bdfe8214883ad9def86def428df', 'f4483dca4a4743ce9c90d90ad480e9d4', '1', null, 'oLyuwxFIcP9d-njlwCJM77uutXbA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epLztYvJXqrFQTAHu4BRrSqeJYO7eHHOKnS0ulydFomucx87RuSE02fROLcS1WdSuVj4Nn3PQiayjg/132', 'ueditor/upload/qrCodeImgs/201803291601552725.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 16:01:55', null, '2018-03-29 16:01:55', null);
INSERT INTO `t_user` VALUES ('370', 'Donjon', '32175e80fc0a40fd80a00d955e18bba5', 'dfeada543c0f4a9b853255cc74e800cb', '1', null, 'oLyuwxEtCtRv4YOZn_pmCNNvUEsk', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIhdWvaENZGT4y1GnA8t4BRmaDTRQBhkpnQmQSvY8L1zqoUtdDXAlD9q3RAkQoI2tBD5MOy0icXosg/132', 'ueditor/upload/qrCodeImgs/201803291623206726.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 16:23:20', null, '2018-03-29 17:16:33', null);
INSERT INTO `t_user` VALUES ('371', 'Moon', '116d6dd227394c6dbab786246ae027f9', '393c95fddbe8435780aabc9db5f91146', '1', null, 'oLyuwxOGJQsqBSppgRKTJuL7Bm9E', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLxGM5qOoveOup7Oexntxo07GVksialVdPH6x0FMjTBuFjGKGjCeFuQIq9QIp7pD0lElwvSoSIBjbQ/132', 'ueditor/upload/qrCodeImgs/2018032916234090.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 16:23:40', null, '2018-03-29 16:26:21', null);
INSERT INTO `t_user` VALUES ('372', '建伟', '5fa6f68d4cb1433baa3c13162641ba6d', '1bab0a6df5e2403188c93da52003f145', '1', null, 'oLyuwxEYRqehY80BvCr5Rr7AdMb0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKWd249rx2pkLWpicq8iaEPYeOhTsjf41wIx23fZOtvOw77FyTYS7P5sER0I6mibvNP5UlW9w1MRKWfQ/132', 'ueditor/upload/qrCodeImgs/201803291633008419.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 16:33:00', null, '2018-03-29 16:33:00', null);
INSERT INTO `t_user` VALUES ('373', '悠悠', '4ca2b8fffeec498f8c227cce8b67e98d', '176f7ffcd012494c948c37a40d9cba7b', '2', null, 'oLyuwxJWbw5xnqxN4deeUF2AQNwc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLDmHwIf5D75tP8A9DFXoUwQrYEhj4e0U52riaibPy40mZrBW0wFk7tiaOKzEDic1lUDIpF8hg6lqXEX6A/132', 'ueditor/upload/qrCodeImgs/201803291634059591.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 16:34:05', null, '2018-03-29 16:37:21', null);
INSERT INTO `t_user` VALUES ('374', 'qinxiu', 'a3c5f2accdf54a5e90fdfd633cdd9351', '7bdc4ba905444728a90f7c4c6be2ab73', '0', null, 'oLyuwxBT3LcLfmguoFkc4Y3FoEiY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/u9p3ibvl90RoByYYL63MjPz7p2kPHjFia7icv2HKgg6iaUDGfibBuey4KGNcDjCWWDr1S0VQibl5FPlFHfxZNgr9aLUQ/132', 'ueditor/upload/qrCodeImgs/201803291634366512.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 16:34:36', null, '2018-03-29 16:38:09', null);
INSERT INTO `t_user` VALUES ('375', 'Cheris', '114b52330e714d16ab4682681f6f51e0', 'd356bc0c37074361a422f3e33176b438', '2', null, 'oLyuwxHgq-gsh1Hr7Otkb4Gt3zvQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q3auHgzwzM6F22n1sFvgy4vJM3dq2N7bJE0V3DPiceOVWxkBbOR4uRflCPcHxXKmvwL2SWAZf3BMO9CNziaVWuKg/132', 'ueditor/upload/qrCodeImgs/201803291720186695.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 17:20:18', null, '2018-03-29 17:21:26', null);
INSERT INTO `t_user` VALUES ('376', 'Alicez', '922c00e3d74f4b43aeea72fab37761ad', '42554b5e156645ddb47e7a3bd5cc677d', '0', null, 'oLyuwxNtt3hxb2s8zNselq_fvwAI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIedfqI3BtnG0WP7icq2Kiccaah26DBpUz5nw1JX7LNFJicYvnzUFibdTjTnDaickNNMGt7TfLskDEH6JA/132', 'ueditor/upload/qrCodeImgs/201803291728253919.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 17:28:25', null, '2018-03-29 17:28:25', null);
INSERT INTO `t_user` VALUES ('377', 'Molly', '7afb99958885430081d993f68ec25d7d', '8531d4726643402ba528ea66b76759ae', '2', null, 'oLyuwxGnWvjqosTy5vCx0ISrH3Qw', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ760hLusozEySMROX6aZia1hUm6gXcusRyGkcrZiaQrdMyCd7Xk6EuRtanJnjq9hVzuxN1UHbrXsgw/132', 'ueditor/upload/qrCodeImgs/201803291812463520.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 18:12:46', null, '2018-03-29 18:12:46', null);
INSERT INTO `t_user` VALUES ('378', '琦', '8f393d7f52fe49c0bab58dc8b22bcea9', 'c2787a3892244cb78e12d5275508dab9', '1', null, 'oLyuwxAQbvygC1RVoE6y65qbNVVg', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJKZiapalRJv7hvF8lHGAj3zFLxbIR2OwHjOu4jCxc7szMHByaj9nXBsf0K6AoXibIh3zk37GGJqySQ/132', 'ueditor/upload/qrCodeImgs/201803291831304298.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 18:31:30', null, '2018-03-29 18:31:30', null);
INSERT INTO `t_user` VALUES ('379', 'Easy_🎀', '7da22d1d42fc4e33a16cdd2a27c59fe4', '219f43fe66634629b5882101009c48c9', '2', null, 'oLyuwxHWi1OscPBjueqclZtXEdVQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ern3ibLbQ4aIjrAeqlbpEdiapkkkiaeOSWbJmialokLCfKNQRuceblYXq0wxjcMtPpLJNw12ba1yEGq4g/132', 'ueditor/upload/qrCodeImgs/201803292015247009.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-29 20:15:24', null, '2018-03-29 20:15:24', null);
INSERT INTO `t_user` VALUES ('381', '小T', '4767a09df83d4c37ade3c45ba87d228b', '92cf01284df6498db5cbfbac7e556f31', '2', null, 'oLyuwxF0RqsGRIEBua5PKc9oJfSs', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epPVpJics0GuByjvmoobsvFjiaGUzVS45dYJRxv62yiaIjCo8jbTNWICXiaGc9AUicJVbiaickRLaCN1O8bA/132', 'ueditor/upload/qrCodeImgs/201803300904341545.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:04:34', null, '2018-03-30 09:04:34', null);
INSERT INTO `t_user` VALUES ('382', 'we-thinking', '5f2dbf6d19be4297ad89577b7974fd13', '14c589eff4c04c2bb83a3caba2e66dca', '1', null, 'oLyuwxMG4DpVybO3-2YnsZ0OV5lM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/GXHyxbZKc3CpUboaPr5oib5Ogw6icKMDqMslyfEm2RhhWrLrKbg6qNKSFZOdBCIYeHrkLl2MukqUAYI3t48Q2E9A/132', 'ueditor/upload/qrCodeImgs/201803300912593315.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:12:59', null, '2018-03-30 09:12:59', null);
INSERT INTO `t_user` VALUES ('383', 'SulaBoer', '04dfd192b1694fcdabdd21eb32d80f77', '0cbeed8e1cfe4cd59618bd28009318f1', '2', null, 'oLyuwxEi1OhfXpcDPFW462-OXYs4', 'http://thirdwx.qlogo.cn/mmopen/vi_32/CKQd0WmYmnicKMUNvHQGyjspF3ibm1OtOEfD3aYZOVIDK9WW3rt6FaYj7QPbnQhcSr8p12gY7beRgaaBJUEybYKQ/132', 'ueditor/upload/qrCodeImgs/201803300913059401.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:13:05', null, '2018-03-30 09:13:05', null);
INSERT INTO `t_user` VALUES ('384', '天空空', 'f8aa79fba2c644a8b0f314e2b14799bd', 'd515debabb874811b4347d5d857aa392', '2', null, 'oLyuwxH0xcbNxhicWDB0md6CarhM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo8sWDqvceibVZGZibedZrB3HbLsV9hUuzHLcOp4hQE7a9w7kakGDmlpL8iarQjOsPvSnRW83VGwhXIw/132', 'ueditor/upload/qrCodeImgs/201803300929546583.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:29:53', null, '2018-03-30 09:29:54', null);
INSERT INTO `t_user` VALUES ('385', '鹹味牙膏', '675fce9fed87431191df0d0e875d3121', '8cecbb94cda14ae5b426f952786f7168', '1', null, 'oLyuwxDBuYEJHzJZUPsTiRB3L4tE', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoUicswquEIxTJTh9UpbMBAiaaQPREUge0GWQVMsHzusO0ZFvTsEBUrbbxzUrbibGamucDV3eRPIIdYQ/132', 'ueditor/upload/qrCodeImgs/201803300935248978.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:35:24', null, '2018-03-30 09:35:24', null);
INSERT INTO `t_user` VALUES ('386', 'Aaong_z🌞', '3be87aad89134ccf86d6102358f0da3a', 'c48137d93ddb49039bf337604c728e37', '2', null, 'oLyuwxCiIAbDou_yJ9k79HLtFfb8', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epajWYAQTzw7qjsDC95vetjWp9WMxFGLoLetYBZ7GDicdX2jLaBbGnWtQvOEVlK8ERcdj9YxVsnmYA/132', 'ueditor/upload/qrCodeImgs/201803300938229472.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:38:22', null, '2018-03-30 09:43:52', null);
INSERT INTO `t_user` VALUES ('387', 'solace', 'f2dfdc754fc745649d49379179491cc5', '5a9f60906982478694c745319eba863a', '1', null, 'oLyuwxDpg16niHUFYxU_qs_w-0OM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIQm7wEmvQEQPcp4Mwlzrxnkicz6x3Y5JIicyeibSS5Hibk4CALnCKUk74lRdtA99ayafZu8lacXdSlwQ/132', 'ueditor/upload/qrCodeImgs/201803300940464736.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 09:40:46', null, '2018-03-30 09:40:46', null);
INSERT INTO `t_user` VALUES ('388', '艾沫', '3dc235195712405795e8414749e4eaf0', '8f9bdf5507134af5a4ad0298a093b678', '2', null, 'oLyuwxIS9KGFkKUpt8_NOh72YsL8', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJSt6SSXWGEXWcJlWOCCcquvj6RB8M8fvcdNiatV9WNOpHc1ehvTOvHO4U95uz3wfFBTUpE74UzjQA/132', 'ueditor/upload/qrCodeImgs/201803301049049365.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 10:49:04', null, '2018-03-30 10:50:13', null);
INSERT INTO `t_user` VALUES ('389', 'weiwei', '3878131894e948d59512997737738419', '9356233ef6c34bf6994ccb0e46f9f76f', '2', null, 'oLyuwxCgi0w8uCbzox7ejFF--CMg', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep1YgZmQickG0AmvScSiaDs3tKKpS3RVW6H9n4B5DsSyoupxLSKRl2oBPGhfWicuvtO3zqxsZqAEWjKg/132', 'ueditor/upload/qrCodeImgs/20180330152843727.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 15:28:43', null, '2018-03-30 15:28:43', null);
INSERT INTO `t_user` VALUES ('390', '可可', '42c83aa328e145afb84731c42a713429', '432a912fa0734f4f9eb1e611b677519a', '2', null, 'oLyuwxNDR2_Ioz2BkycJpjjkvf-Y', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKkPiaRZJntlKZ4kZ9AGah0m0Tm8icrGDYZLw9TrQf6ZTqd9qB4KySnXobx79wLM1vAfKhmlRoX3SnQ/132', 'ueditor/upload/qrCodeImgs/201803301544341324.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 15:44:34', null, '2018-03-30 15:59:07', null);
INSERT INTO `t_user` VALUES ('391', 'hong', 'f58dfb90435a4fe1a0f880b7ca9688df', '31263ed6e2104b07a61aef5fe968d007', '1', null, 'oLyuwxCYArAi8s-t0JzXjSpXlizc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJ6fYibAAgQqglHx9LLgGibDlHIT28V2cw7SXVmPuMnkFTUGEgf2E65oreNcdw3MY3acANS7UgpLISg/132', 'ueditor/upload/qrCodeImgs/201803301634292851.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 16:34:29', null, '2018-03-30 16:34:29', null);
INSERT INTO `t_user` VALUES ('392', '随行de单车', '398bfef0af834684a30c413ac7c806a7', '573eed2c5e7545d893bf878064979a68', '1', null, 'oLyuwxPVYGV0JiM4I_NJVZ44irAc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIradvotK0Q7ZBUeNooslNIvg6NRW6quGGBXPiaV3KENDAib2No7KPfTniatmmW5GaTwbaicJQhg6gt1A/132', 'ueditor/upload/qrCodeImgs/201803301920305569.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-30 19:20:30', null, '2018-03-30 19:20:30', null);
INSERT INTO `t_user` VALUES ('393', '孙云峰', '6bd37367cc79490ba775358e1af37c74', '97842aafaf3f432a9048b003fc368fe5', '1', null, 'oLyuwxDEillXu-54AcVEcADkq53o', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL5QXw1TpubcGkxtyDFb09jEx0noqM2BlicITKobHHpMx6zO2qr7G5e4LLptqOhHYcoCgbmdWMr2UA/132', 'ueditor/upload/qrCodeImgs/20180331165901909.png', null, '0', '4', 'y', '0.00', '0.05', '2018-03-31 16:59:01', null, '2018-03-31 16:59:01', null);
INSERT INTO `t_user` VALUES ('394', '剑仙', '4f55180a015b4d82be2042ebf35e8352', 'e901e81a73c946b7af37ada0224463c8', '1', null, 'oLyuwxJeH8KXZtLgZCPxUnKTNEFw', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJIy2ibzdngVLdD2bT0iawD8zHLiavWgMFjl4lNtl0nQGzAvpTOxXaibsuzfWY38ZTOSgB5BCib0ThePdw/132', 'ueditor/upload/qrCodeImgs/201804011002107268.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-01 10:02:10', null, '2018-04-01 10:02:10', null);
INSERT INTO `t_user` VALUES ('395', '暖暖 ', '3ecb116df88b43f082f9879a34f53c15', '9b8e991638a44d19b936e3e01d41f50b', '2', null, 'oLyuwxJIzO6O9zX9dqmqAbksC0e4', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTITuVqiaGrGzIZ2aSuyw3iaAuCdlDkqlPm8EcgZ5HR9tLkCPiczwkhMycn5UCqjqSbNWqXH1zHC37JnQ/132', 'ueditor/upload/qrCodeImgs/201804011142443599.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-01 11:42:44', null, '2018-04-01 11:42:44', null);
INSERT INTO `t_user` VALUES ('396', '清欢', '60d62962c1d340dea76a201c41badba4', 'b492cb1025d048ceb13e4b28eb37bac5', '2', null, 'oLyuwxGR9II9EHEf7kY-FWcyKkbY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLicy0x4gnyq5vY3wJngheo8r9p9xkINu77LQFbXb5LXxYFtwu3ESAAqZ6AyX3aE5qr0cvN3MxFnKA/132', 'ueditor/upload/qrCodeImgs/201804020918532921.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-02 09:18:53', null, '2018-04-02 09:18:53', null);
INSERT INTO `t_user` VALUES ('397', 'lyz幼字', 'ba9a30e077704884b20c9f3bb90d8ba8', '095bdc9d90fd47b98c80b3e49828bec9', '2', null, 'oLyuwxClYeTXs7-pyCuT8zmHL0yM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJulibWCqMBetHLIMay7OOkSbU8xjGczticIiaY1qVzkzrx16sdvYEiczBR0B6VIhjuR7h20qz9pyMnpQ/132', 'ueditor/upload/qrCodeImgs/201804021022106890.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-02 10:22:10', null, '2018-04-02 10:22:10', null);
INSERT INTO `t_user` VALUES ('398', '甘雅梅', '45de06509188465db03968beafac9bb8', '8f8d2511965a42efad082f08bf687e4f', '2', null, 'oLyuwxFWMiMrHwZK39v7ls6P3XGA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKRibeg9xMAXeyCKaytjorp2bGv6iama3jJqcZP0FBpx9uEplMhCicZem0ib2EAqsBFPC8yMbL3icqJ7gg/132', 'ueditor/upload/qrCodeImgs/201804021348239322.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-02 13:48:23', null, '2018-04-02 13:48:23', null);
INSERT INTO `t_user` VALUES ('399', '珊', '073caf56e856455db5b64dc4103f39df', '55411603792a4bf0ad78ebce76e0e570', '2', null, 'oLyuwxIb9gV7Y6-eZo4gQVa9_DoI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/iazBiaHjMuVkbS4bmZvCxSibrMycYF95D7OTXHPCPhBrTibXB4cZzNsCYNI13ibn9AG84bcuauxSFjsOeW4te54PicLw/132', 'ueditor/upload/qrCodeImgs/201804022051455469.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-02 20:51:45', null, '2018-04-02 20:51:45', null);
INSERT INTO `t_user` VALUES ('400', '以沫', 'c1cbaac594fb40149e91c99cbe6b518f', 'af5e5f4f7da249108b0cc4915a8a12fe', '0', null, 'oLyuwxNnSOt25_BaIF5wH0PHvxcI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/J8dav3qA9Dom6F7VRibvAkaMibnuSC3oNj30Q7UG7pZWar8sFzTmzEGqbepSzSetSibMvibicWOyJGXsYKhsibVu9skw/132', 'ueditor/upload/qrCodeImgs/201804022054046098.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-02 20:54:04', null, '2018-04-02 20:54:05', null);
INSERT INTO `t_user` VALUES ('401', '阿芝', 'a5978a8cbeef4d85b2a09ea75b61aa16', 'e40cd1f7f974454898a93d8148dd6d4c', '2', null, 'oLyuwxFhZImWjKADXxqZSJoceUfw', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJufXiaVlObbAjKvWJ6wEDqzk8JV1GVuMJctWPVEn2YN4G1Jx2aLJlGplcwvNqyb7QRYRicJ76JvXTA/132', 'ueditor/upload/qrCodeImgs/201804031048302.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-03 10:48:30', null, '2018-04-03 10:48:30', null);
INSERT INTO `t_user` VALUES ('402', 'w', '4d248eb1813941dfb5f9b5e2c1517848', '34a7a0acbd75457bb56c0e4b307c2ed0', '0', null, 'oLyuwxDGd9c3pSlXikvIhQZMW_uQ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLON9WNgBHOld2XdWPIJXCiazZQ2bicLUo2UYPiaYLq8N3SNMEEDm50mwhhbA1d1DRmfrYpXVkckUXnA/132', 'ueditor/upload/qrCodeImgs/201804031242096802.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-03 12:42:09', null, '2018-04-03 12:42:09', null);
INSERT INTO `t_user` VALUES ('403', '芳小姐', 'f5242bbb6d244e04a9b4fc0eef376a23', '095b55e4e6814a628b5a4e6d709cc367', '2', null, 'oLyuwxNVMNYmgH_ybVHw7O8UzqPg', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLqmib6ibmQW4ccLR3s5hQ3gZNeYFW0e7TAFe5L1DAx84vqJ8jIW4fuOl07xFZKCwmfRan0w3qTAIbw/132', 'ueditor/upload/qrCodeImgs/201804052106293757.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-05 21:06:29', null, '2018-04-05 21:06:29', null);
INSERT INTO `t_user` VALUES ('404', '弥撒', '8ab0a9c3044b4067aa047267f1ea53e1', 'ed3902fcdb914b85822a12f80bae6557', '2', null, 'oLyuwxETc-yJ76lnVTaWdrT-H_Fc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLcxBKDUib2eJNj0yiaFKBuQgkSoib8Ma4YTqX8x0ax10AlPGyAmKeIj8rzyrqAsTicqDfgYgdBTl6zmg/132', 'ueditor/upload/qrCodeImgs/201804251125131227.png', null, '0', '4', 'y', '0.00', '0.05', '2018-04-25 11:25:13', null, '2018-04-25 11:25:13', null);
INSERT INTO `t_user` VALUES ('405', '肉泥', '3256e55fa299479e8896483e69562ca4', '3f605136e81147018683e2afe907264c', '1', null, 'oLyuwxNPjb_HKFJU8SceVFTDSlOY', 'http://thirdwx.qlogo.cn/mmopen/vi_32/3EB7dFdNRKkwLvPgUGASXD7dp8BoVgdXOojibe1aFx9Rl7ickCcjL5fHLYHd0iby0UdlNFbkdds9Whn2ZqHObNu8A/132', 'ueditor/upload/qrCodeImgs/201805251615574480.png', null, '0', '4', 'y', '0.00', '0.05', '2018-05-25 16:15:57', null, '2018-05-25 16:15:57', null);
INSERT INTO `t_user` VALUES ('406', '志文', '7b9b9d2ee8fa44a386969bd4481d071f', '105805079a0b4fadbbc97ffe1e587410', '1', null, 'oLyuwxE0KGEMc4oQ3VF4aQ7m6kUc', 'http://thirdwx.qlogo.cn/mmopen/vi_32/vQPjPfJqQKPmEZnbn1OcUm0tzyaqrLckTsS667ibia89tjeSSQaO5oMvQ32picZbaRtnb7IdTSYOAOoUM0liaDiaYiaw/132', 'ueditor/upload/qrCodeImgs/201805281527592681.png', null, '0', '4', 'y', '0.00', '0.05', '2018-05-28 15:27:59', null, '2018-05-28 15:27:59', null);
INSERT INTO `t_user` VALUES ('407', '阿兰', 'd0f5c606d0534d1282628c74a34b00d3', '0f04b006cdee4e38b44e06030e9276aa', '0', null, 'oLyuwxLDXuRYoV0yVXJOoBpT_EHM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/K2s3oylDn3XGIgUXgh5no0v7glAiacppvicaibRc5FKeUo2Ip7AzxaXmFWdOP5rDkFr1IJqWjiatWkfNcic34O2tqpA/132', 'ueditor/upload/qrCodeImgs/201806022134082958.png', null, '0', '4', 'y', '0.00', '0.05', '2018-06-02 21:34:08', null, '2018-06-02 21:34:48', null);
INSERT INTO `t_user` VALUES ('408', '卖柚子的菜菜', 'af84c5bbcf1e4850a6d017cc26a6ead3', '53bb2dfaa42b4a108adf106c3512d2e9', '2', null, 'oLyuwxEU1NuFBJS0_LJU3uvCFqc4', 'http://thirdwx.qlogo.cn/mmopen/vi_32/lFql3XFM1IJGiadhiagJtG3icmicYxTh2IkPl1TBLmIbE7hic0O2IS8iaThVibzR1U7Yz3KFr8EBuXmrSKFZQRyboHC9A/132', 'ueditor/upload/qrCodeImgs/20180603122135910.png', null, '0', '4', 'y', '0.00', '0.05', '2018-06-03 12:21:35', null, '2018-06-03 12:21:35', null);
INSERT INTO `t_user` VALUES ('409', '肖坤安', '44e30f0e727f44678e6406a3836330af', '091ced36940b4ca3ba835751972de787', '1', null, 'oLyuwxMsNH3tB6GbjDNe4bCZ-Q_0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIlIgibZHWyblb8Rhic1VU98FKTLssTSMGR1pFlqJEV348lEh81U6QibX6LGTrmz5kf9IsuFHpsAROaQ/132', null, null, '0', '4', 'y', '0.00', '0.05', '2018-06-19 14:45:45', null, '2018-06-19 15:51:34', null);

-- ----------------------------
-- Table structure for t_wallet
-- ----------------------------
DROP TABLE IF EXISTS `t_wallet`;
CREATE TABLE `t_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `payee` varchar(255) DEFAULT NULL COMMENT '收款人',
  `payAccount` varchar(255) DEFAULT NULL COMMENT '收款账户',
  `accountType` varchar(10) DEFAULT NULL COMMENT '账户类型（1支付宝、2微信、3银行卡 ）',
  `applicant` varchar(255) DEFAULT NULL COMMENT '申请人',
  `money` decimal(11,2) DEFAULT NULL COMMENT '提现金额',
  `status` varchar(4) DEFAULT '1' COMMENT '状态(1待审核、2已通过、3不通过）',
  `applicantName` varchar(255) DEFAULT NULL COMMENT '申请人姓名',
  `payeePhone` varchar(255) DEFAULT NULL COMMENT '收款人手机',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户提现申请表';

-- ----------------------------
-- Records of t_wallet
-- ----------------------------

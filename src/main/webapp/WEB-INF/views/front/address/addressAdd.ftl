<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
    <title>味猫商城</title>
</head>
<body class="add-body">
<div class="page-group">
    <div class="page">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="ToBack()">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">新增收货地址</h1>
        </header>
        <div class="content editAddress-content">
            <div class="list-block">
                <ul>
                    <!-- Text inputs -->
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">收货人</div>
                                <div class="item-input">
                                    <input type="text" class="r-name" placeholder="请填写收货人姓名">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">联系电话</div>
                                <div class="item-input">
                                    <input type="tel" class="r-tel" placeholder="请填写手机号码">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="item-link item-content">
                            <div class="item-media"><i class="icon icon-form-password"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">所在地区</div>
                                <div class="item-input">
                                    <input type="text" class="r-area" readonly="readonly" id="city-picker"
                                           placeholder="请选择所在地区">
                                </div>
                            </div>

                        </a>
                    </li>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">所在小区</div>
                                <div class="item-input">
                                    <input type="text" class="r-location" placeholder="请填写所在小区名称">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">楼号-门牌号</div>
                                <div class="item-input">
                                    <input type="text" class="r-floor" placeholder="请填写楼号-门牌号">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div id="editBtn">
                    <a href="#" class="button">保存该地址</a>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
    <script type="text/javascript">$.config = {router: false}</script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
    <script type="text/javascript" src="http://g.alicdn.com/msui/sm/0.6.2/js/sm-city-picker.min.js"
            charset="utf-8"></script>
    <script>
        $("#city-picker").cityPicker({
            toolbarTemplate: '<header class="bar bar-nav">\
        <button class="button button-link pull-right close-picker cell-right l">确定</button>\
        <h1 class="title">选择收货地址</h1>\
        </header>'
        });
    </script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
    <script src='${staticpath}/front/dist/js/address/addressAdd.js'></script>
    <script type="text/javascript">$.init();</script>
</div>
</body>

</html>

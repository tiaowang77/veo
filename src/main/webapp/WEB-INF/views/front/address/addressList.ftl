<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_bg2kkzkp58eel8fr.css">
    <title>味猫商城</title>
    <style>
        .page {
            background: #efeff4;}
    </style>
</head>
<body>
<div class="page-group manageAddress">
    <div class="page">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" id="back">
                <span class="icon icon-left"></span>返回
            </button>
            <#--<span class="iconfont icon-jiantouright ico pull-left cell-left"  id="back"></span>-->
            <h1 class="title">选择地址</h1>
        </header>
        <div class="content ">
            <ul data-url="">
                <div class="lack">
                    <img src="${staticpath}/front/dist/images/lack_addr.png" alt="" class="pic-lack">
                    <div class="text-lack">亲，您还没有添加地址呢~</div>
                </div>
            </ul>

        </div>
        <a data-url="" class="bar bar-footer addBtn">添加新地址</a>
    </div>
    <script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/address/addressList.js' charset='utf-8'></script>
</body>

</html>

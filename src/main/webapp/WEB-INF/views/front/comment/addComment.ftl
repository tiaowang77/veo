<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/base/webUpload/webuploader.css">
    <link rel="stylesheet" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
</head>
<body>

<div class="page-group">
    <div class="page page-current">
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="back()">
                <span class="icon icon-left"></span>返回
            </button>
            <button class="right button button-link button-nav pull-right">
                <span class="saveBtn">提交</span>
            </button>
            <h1 class="title">发布评论</h1>
        </header>
        <div class="content">
            <textarea class="comment-text" placeholder="请输入您的评价"></textarea>
            <ul class="pic-box flex ">
            <div id="filePicker" class="upload">
                <img src="${staticpath}/front/dist/images/addPicture.png" alt="" class="pic-item" ></li>

                </div>
            </ul>
            <div class="underline"></div>
            <li class="pic-check">
                <input type="checkbox" class="check" checked="checked">
                <span class="pic-select">匿名评价</span>
            </li>
            <input type="text" id="picurl" class="hide">
            <div class="underline"></div>
            <ul class="check-comment">
                <p>选择评价评分</p>
                <li class="selectComment">好评</li>
                <li>中评</li>
                <li>差评</li>
            </ul>
    </div>
    </div>

</div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script> jQuery.noConflict();</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/comment/addComment.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/base/webUpload/webuploader.min.js' charset='utf-8'></script>
<script>
    $.config = {
        router: false
    }
</script>
<script>
    var imgArr = new Array();
    jQuery(function ($) {
        var uploader = WebUploader.create({
            auto: true,
            swf: '${staticpath}/common/ueditor/third-party/webuploader/Uploader.swf',
            server: '${basepath}/rest/front/ued/config?action=uploadimage',
            pick: '#filePicker',
            fileNumLimit: 3,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });
        //上传成功后
        uploader.on('uploadSuccess', function (file, response) {
            uploader.removeFile(file);
            imgArr.push(response.url);
            var length=jQuery(".pic-box").find("li").length;
            if (length<= 2) {
                jQuery("#filePicker").before("<li ><img class='upimg' src='/" + response.url + "'><i class='iconfont icon-close2 del-img ' data-url='" + response.url + "'></i></li>");
                jQuery("#picurl").val(imgArr.join(","));
                length < 2?$('#filePicker').show():$('#filePicker').hide();
            }
        });
        jQuery(".pic-box").on('click','.del-img',function(){
            var data = $(this).attr("data-url");
            for(var i in imgArr){
                imgArr[i]==data&&imgArr.splice(i, 1);
            }
            jQuery("#picurl").val(imgArr.join(","));
            $(this).parents('li').remove();
            jQuery(".pic-box").find("li").length < 3?$('#filePicker').show():$('#filePicker').hide();
        });
    });
</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
</body>

</html>

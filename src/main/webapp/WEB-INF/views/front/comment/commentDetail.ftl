<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_eg16ytxjphc9pb9.css">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
    <style>
        .bar .button-nav.pull-left {
             margin-left: 0px!important;
        }
        .bar .icon {
            margin-left: 6px!important;
        }
    </style>
</head>
<body>

<div class="page-group">
    <div class="page page-current">
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="back()">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">查看评论</h1>
        </header>
        <div class="content">
            <textarea class="comment-text" placeholder="请输入您的评价" readonly></textarea>
            <ul class="pic-box flex ">
            </ul>
            <div class="underline"></div>
            <li class="pic-check">
                <input type="checkbox" class="check" readonly="readonly" disabled="disabled">
                <span class="pic-select">匿名评价</span>
            </li>
            <div class="underline"></div>
            <ul class="check-comment">
                <p>选择评价评分</p>
                <li>好评</li>
                <li>中评</li>
                <li>差评</li>
            </ul>
        </div>
    </div>

</div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/comment/CommentDetail.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
</body>

</html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_up1w8xn3gak2zkt9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <style>
        .page {
            background-color: #f5f6f7;
        }
    </style>
</head>

<body>
<div class="page-group">
    <div class="page page-current" id="pas">
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="back()">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">我的评价</h1>
        </header>
        <div class="content ">
            <div class="buttons-row tab-btn">
                <a href="#all" class="tab-link active cell-main" data-status="all">全部</a>
                <a href="#n" class="tab-link  cell-main" data-status="n" >待评价</a>
                <a href="#y" class="tab-link cell-main" data-status="y" >已评价</a>
            </div>
            <div id="all" class="tab ">
                <div class="lack">
                    <img src="${staticpath}/front/dist/images/lack_comment.png" alt="" class="pic-lack">
                    <div class="text-lack">亲，没有查询到可评价订单哦~</div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/comment/commentlist.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>

</html>

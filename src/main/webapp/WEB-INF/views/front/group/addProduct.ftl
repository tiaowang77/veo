<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/base/webUpload/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/swiper-3.4.2.min.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_507155_3xhv7n8e0kny7gb9.css">
    <title>味猫商城</title>
    <style>
        .editAddress-content .list-block li:after{
            height:0px;
        }
        .page{
            background: #efeff4!important;
        }
        .swiper-container {
            height: 16rem;

        }

    </style>
</head>
<body id="product-body">
<div class="page-group">
    <div class="page  page-current" style="z-index: 1" id="pageOne">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <header class="bar bar-nav">
                <button class="back-button button button-link button-nav pull-left" onclick="pBack()">
                    <span class="icon icon-left"></span>返回
                </button>
                <h1 class="title">添加商品</h1>
            </header>
        </header>
        <div class="content editAddress-content addProduct-content">

            <div class="list-block">
                <ul>
                    <li class="addr">
                        <i class="cell-left cell-right iconfont icon-dingwei ico" style="float: left"></i>
                        <div style="float: left" class="addressMain">
                            <span class="name cell-right">暂无配送地址</span>
                            <div class="address overhide">还没有配送地址，赶紧去添加吧</div>
                        </div>
                        <span class="iconfont icon-fanhuijianyou addSpec" style="margin-right: 10px;"></span>
                    </li>
                    <img src="${staticpath}/front/dist/images/line.png" alt="" class="colorline">
                    <!-- Text inputs -->

                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">商品名称</div>
                                <div class="item-input">
                                    <input type="text" class="p-name" placeholder="请填写商品名称">
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">商品介绍</div>
                                <div class="item-input">
                                    <textarea class="p-textarea" placeholder="请填写商品介绍"></textarea>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">开团日期</div>
                                <div class="item-input">
                                    <input type="text" id='datetime-picker' placeholder="请填写开团日期"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <div class="spec-li">
                        <li>
                            <div class="item-content">
                                <div class="item-media"><i class="icon icon-form-email"></i></div>
                                <div class="item-inner">
                                    <div class="item-title label">规格组1</div>
                                    <div class="item-input" style="float: left;">
                                        <input type="text" class="specInput" placeholder="请填写规格组" readonly="readonly">
                                    </div>
                                    <span class="iconfont icon-fanhuijianyou addSpec"></span>
                                </div>
                            </div>
                        </li>
                    </div>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">自提区</div>
                                <div class="item-input">
                                    <input type="text" class="p-address" placeholder="请填写自提名地名">
                                </div>
                            </div>
                        </div>
                    </li>
                  <div class="underline"></div>
                  <li class="checkBtn" style="display: none;">
                    <div class="item-content">
                      <div class="item-media"><i class="icon icon-form-email"></i></div>
                      <div class="item-inner">
                        <div class="item-title label">开团循环</div>
                          <input type="checkbox" class="check"  style="float: right">
                      </div>
                    </div>
                  </li>
                </ul>
            </div>
            <div class="img-card card underline">
                <p class="p-name">商品主图</p>

                <div class="card-content">
                    <ul class="card-content-inner imgList">
                        <li id="filePicker"><img src="${staticpath}/front/dist/images/addPicture.png"></li>
                    </ul>
                </div>
                <input type="hidden" id="picurl"/>
            </div>
            <div id="editBtn">
                <a href="#" class="previewBtn button">预览</a>
            </div>
            <div id="editBtn">
                <a href="#" class="button saveBtn">保存并提交申请</a>
            </div>
        </div>
    </div>
    <div class="page  page-current" style="z-index: 1;display: none" id="pageTwo">
        <!--头部内容 !-->
            <header class="bar bar-nav">
                <button class="back-button button button-link button-nav pull-left spec-left">
                    <span class="icon icon-left"></span>返回
                </button>
                <h1 class="title">添加商品规格</h1>
                <span class=" pull-right cell-right xl apply hui" id="saveSpec">保存</span>
            </header>

        <div class="content editAddress-content addProduct-content">
            <div class="list-block">
                <ul>
                    <!-- Text inputs -->
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">规格组1</div>
                                <span class="addBtn">添加规格</span>

                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">商品规格</div>
                                <div class="item-input">
                                    <input type="text" class="p-color" placeholder="请填写商品规格颜色">
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">市场价</div>
                                <div class="item-input">
                                    <input type="text" class="p-purchase" placeholder="请填写市场价"
                                           onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">现价</div>
                                <div class="item-input">
                                    <input type="text" class="p-now" placeholder="请填写商品现价"
                                           onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">团购价</div>
                                <div class="item-input">
                                    <input type="text" class="p-group" placeholder="请填写商品团购价"
                                           onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li class="text-input">
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">库存</div>
                                <div class="item-input">
                                    <input type="text" class="p-stock" placeholder="请填写商品库存"
                                           onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page" id="pageThree">
        <div class="content">
            <!-- 轮播 -->
            <div class="swiper-container">
                <div class="back">
                    <i class="iconfont icon-more1 ico"></i>
                </div>
                <div class="share">
                    <i class="iconfont icon-fenxiang ico"></i>
                </div>
                <div class="swiper-wrapper">
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <!-- 标题 -->
            <li class="goods-det clearfix">
                <h1 class="xl over-h1"></h1>
                <p class="high-line l over-p"></p>
                <div style="line-height: 1.5rem;">
                    <span class="price red xl"><b>158.00</b></span>
                    <s class="l hui price ">158.00</s>

                    <div class="pull-right m"><span class="hui ">限时团购</span>&nbsp;<span
                            class="time ">13天12小时21分44秒</span></div>
                </div>
            </li>
            <!-- 参团 -->
            <ul class="group">
                <ul class="underline">
                    <li class="flex header">
                        <h1 class="xl cell-left cell-main">17人在参团</h1>

                        <div class="more hui l">查看更多</div>
                        <i class="iconfont icon-more arrow cell-right"></i>
                    </li>
                </ul>
                <ul class="underline">
                    <div class="inf-box flex middle">
                        <img src="${staticpath}/front/dist/images/A.png" alt="" class="pic ">

                        <div class="name xl">林志伟</div>
                        <div class="cell-main right fn inf-a">
                            <span class="l">还差<span class="pnum">1</span>人拼成</span>

                            <div class="addre m">宁海社区17号楼1103</div>
                        </div>
                        <div class="join cell-right xl">去拼团</div>
                    </div>
                </ul>
                <ul class="underline">
                    <div class="inf-box flex middle">
                        <img src="${staticpath}/front/dist/images/A.png" alt="" class="pic cell-left">

                        <div class="name xl">林志伟</div>
                        <div class="cell-main right fn inf-a">
                            <span class="l">还差<span class="pnum">1</span>人拼成</span>

                            <div class="addre m">宁海社区17号楼1103</div>
                        </div>
                        <div class="join cell-right xl">去拼团</div>
                    </div>
                </ul>
            </ul>
            <!-- 选择 -->
            <ul class="select-group">
                <li class="flex select-box">
                    <div class=" cell-main l">所在地区</div>
                    <div class="hui l cell-right adrs areaBtn">宁海社区17楼</div>
                </li>
                <div class="underline"></div>
                <li class="flex select-box">
                    <div class=" cell-main l">所在小区</div>
                    <div class="hui l cell-right adrs address-div">宁海社区17楼</div>
                </li>
                <div class="underline"></div>
                <li class="flex select-box">
                    <div class=" cell-main l">已选</div>
                    <div class="hui l cell-right "><span class="goodsnumb">1</span>件</div>
                    <i class="iconfont icon-more arrow"></i>
                </li>
            </ul>
            <!-- 评价 -->
            <ul class="assess">
                <li class="header flex">
                    <div class=" cell-main l">用户评价<span class="assnum">（12000）</span></div>
                    <div class="hui l cell-right adrs">查看全部</div>
                    <i class="iconfont icon-more arrow"></i>
                </li>
                <div class="underline"></div>
                <ul class="grade-wrapper flex">
                    <div class="btn-grade cell-main l">好评(10)</div>
                    <div class="btn-grade cell-main l">中评(10)</div>
                    <div class="btn-grade cell-main l">差评(10)</div>
                    <div class="btn-grade cell-main l">有图(10)</div>
                </ul>
                <div class="assess-box">
                    <li class="head flex middle">
                        <img src="${staticpath}/front/dist/images/c-head.png" alt="" class="pic">

                        <div class="cell-main">
                            <span class="name xl">林青晴</span>

                            <div class="hui l"><span class="time">2017-09-25</span>&nbsp; 规格：<span
                                    class="spec">白色 M码</span></div>
                        </div>
                    </li>
                    <div class="assess-det xl">总体不错，就是没有黑色的。以前都是用黑色的，然后鼠 标底部的感应有点太灵敏了，抬起来晃动鼠标。指针还是 跟着动，
                    </div>
                    <li class="flex pic-box">
                        <img src="${staticpath}/front/dist/images/c-img.png" alt="" class="img">
                        <img src="${staticpath}/front/dist/images/c-img.png" alt="" class="img">
                        <img src="${staticpath}/front/dist/images/c-img.png" alt="" class="img">
                    </li>
                </div>
            </ul>
            <!-- 推荐 -->
            <div class="Recommend">
                <img src="${staticpath}/front/dist/images/push.png" alt="" class="img">
                <li class="flex goods-box">
                    <img src="${staticpath}/front/dist/images/productImg.png" alt="" class="pic cell-right">

                    <div class="cell-main">
                        <div class="name over">山东特产 青州蜜桃冬雪蜜桃 冬桃 小毛桃新鲜水果 5斤装</div>
                        <div class="price orange xl ">79.00</div>
                        <div class="timetip">剩余时间</div>
                        <div class="time m">6天8时59分22秒</div>
                        <div class="person m">200参与</div>
                        <div class="join">去拼团</div>
                    </div>
                </li>
            </div>
        </div>
        <nav class="bar bar-tab" id="tool-bar">
            <a class="tab-item external home fn">
                <span class="ic iconfont icon-shouye "></span>

                <div class="tab-label fn">首页</div>
            </a>
            <a class="tab-item external coll fn">
                <span class="ic iconfont icon-kongxinxing collect"></span>

                <div class="tab-label fn">收藏</div>
            </a>
            <a class="tab-item fn" id="singer" data-state="1">
                <div class="price l">158.00</div>
                单独购买</a>
            <a href="#" class="tab-item fn" id="joinus">
                <div class="price l">158.00</div>
                立即购买</a>
        </nav>
    </div>
</div>

<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script> jQuery.noConflict();</script>
<script type="text/javascript">$.config = {router: false}</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/swiper-3.4.2.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/group/addProduct.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/base/webUpload/webuploader.min.js' charset='utf-8'></script>
<script>
    var imgArr = new Array();
    jQuery(function ($) {
        var uploader = WebUploader.create({
            auto: true,
            swf: '${staticpath}/common/ueditor/third-party/webuploader/Uploader.swf',
            server: '${basepath}/rest/front/ued/config?action=uploadimage',
            pick: '#filePicker',
            fileNumLimit: 5,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });
        //上传成功后
        uploader.on('uploadSuccess', function (file, response) {
            uploader.removeFile(file);
            imgArr.push(response.url);
            var length = jQuery(".imgList").find("li").length;
            if (length <= 5) {
                jQuery("#filePicker").before("<li ><img class='upimg' src='/" + response.url + "'><i class='iconfont icon-close2 del-img ' data-url='" + response.url + "'></i></li>");
                jQuery("#picurl").val(imgArr.join(","));
                length < 5 ? $('#filePicker').show() : $('#filePicker').hide();
            }
        });
        jQuery(".imgList").on('click', '.del-img', function () {
            var data = $(this).attr("data-url");
            for (var i in imgArr) {
                imgArr[i] == data && imgArr.splice(i, 1);
            }
            jQuery("#picurl").val(imgArr.join(","));
            $(this).parents('li').remove();
            jQuery(".imgList").find("li").length < 6 ? $('#filePicker').show() : $('#filePicker').hide();
        });
    });
</script>
</body>

</html>

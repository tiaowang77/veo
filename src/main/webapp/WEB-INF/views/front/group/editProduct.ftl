<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/base/webUpload/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/swiper-3.4.2.min.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_507155_3xhv7n8e0kny7gb9.css">
    <title>味猫商城</title>
    <style>
        .editAddress-content .list-block li:after{
            height:0!important;
        }
        .page{
            background: #efeff4!important;
        }
        .swiper-container {
            height: 16rem;

        }
        .bar .button.pull-right{
            margin-right: 15px;
        }
    </style>
</head>
<body>
<div class="page-group">
    <div class="page  page-current" style="z-index: 1" id="pageOne">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <header class="bar bar-nav">
                <button class="back-button button button-link button-nav pull-left" onclick="pBack()">
                    <span class="icon icon-left"></span>返回
                </button>

                <h1 class="title">编辑商品</h1>
            </header>
        </header>
        <div class="content editAddress-content addProduct-content">
            <div class="list-block">
                <ul>
                    <li class="addr">
                        <i class="cell-left cell-right iconfont icon-dingwei ico" style="float: left"></i>
                        <div style="float: left" class="addressMain">
                            <span class="name cell-right">暂无配送地址</span>
                            <div class="address overhide">还没有配送地址，赶紧去添加吧</div>
                        </div>
                        <span class="iconfont icon-fanhuijianyou addSpec" style="margin-right: 10px;"></span>
                    </li>
                    <img src="${staticpath}/front/dist/images/line.png" alt="" class="colorline">
                    <!-- Text inputs -->
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">商品名称</div>
                                <div class="item-input">
                                    <input type="text" class="p-name" placeholder="请填写商品名称">
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">商品介绍</div>
                                <div class="item-input">
                                    <textarea class="p-textarea" placeholder="请填写商品介绍"></textarea>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">开团日期</div>
                                <div class="item-input">
                                    <input type="text" id='datetime-picker' placeholder="请填写开团日期"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="underline"></div>
                    <div class="spec-li">

                    </div>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">自提区</div>
                                <div class="item-input">
                                    <input type="text" class="p-address" placeholder="请填写自提名地名">
                                </div>
                            </div>
                        </div>
                    </li>
                  <div class="underline"></div>
                  <li class="checkBtn" style="display: none;">
                    <div class="item-content">
                      <div class="item-media"><i class="icon icon-form-email"></i></div>
                      <div class="item-inner">
                        <div class="item-title label">开团循环</div>
                        <input type="checkbox" class="check"  style="float: right">
                      </div>
                    </div>
                  </li>
                </ul>
            </div>
            <div class="img-card card underline">
                <p class="p-name">商品主图</p>

                <div class="card-content">
                    <ul class="card-content-inner imgList">
                        <li id="filePicker"><img src="${staticpath}/front/dist/images/addPicture.png"></li>
                    </ul>
                </div>
                <input type="hidden" id="picurl"/>
            </div>
            <div id="editBtn">
                <a href="#" class="previewBtn button">预览</a>
            </div>
            <div id="editBtn">
                <a href="#" class="button saveBtn">保存并提交申请</a>
            </div>
        </div>
    </div>
    <div class="page  page-current" style="z-index: 1;display: none" id="pageTwo">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left spec-left">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">添加商品规格</h1>
            <span class=" pull-right cell-right xl apply hui" id="saveSpec">保存</span>
        </header>
        <div class="content editAddress-content addProduct-content">
            <div class="list-block">
                <ul>

                </ul>
            </div>
        </div>
    </div>
    <div class="page" id="pageThree">
        <div class="content">
            <!-- 轮播 -->
            <div class="swiper-container" >
                <div class="back">
                    <i class="iconfont icon-more1 ico"></i>
                </div>
                <div class="share">
                    <i class="iconfont icon-fenxiang ico"></i>
                </div>
                <div class="swiper-wrapper">
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <!-- 标题 -->
            <li class="goods-det clearfix">
                <h1 class=" xl over-h1"></h1>
                <p class="high-line l over-p"></p>
                <div style="line-height: 1.5rem;">
                    <span class="price red xl"><b>158.00</b></span>
                    <s class="l hui price ">158.00</s>

                    <div class="pull-right m"><span class="hui ">限时团购</span>&nbsp;<span
                            class="time ">13天12小时21分44秒</span></div>
                </div>
            </li>
            <!-- 参团 -->
            <ul class="group">
                <ul class="underline">
                    <li class="flex header">
                        <h1 class="xl cell-left cell-main">17人在参团</h1>

                        <div class="more hui l">查看更多</div>
                        <i class="iconfont icon-more arrow cell-right"></i>
                    </li>
                </ul>
                <ul class="underline">
                    <div class="inf-box flex middle">
                        <img src="${staticpath}/front/dist/images/A.png" alt="" class="pic ">

                        <div class="name xl">林志伟</div>
                        <div class="cell-main right fn inf-a">
                            <span class="l">还差<span class="pnum">1</span>人拼成</span>

                            <div class="addre m">宁海社区17号楼1103</div>
                        </div>
                        <div class="join cell-right xl">去拼团</div>
                    </div>
                </ul>
                <ul class="underline">
                    <div class="inf-box flex middle">
                        <img src="${staticpath}/front/dist/images/A.png" alt="" class="pic cell-left">

                        <div class="name xl">林志伟</div>
                        <div class="cell-main right fn inf-a">
                            <span class="l">还差<span class="pnum">1</span>人拼成</span>

                            <div class="addre m">宁海社区17号楼1103</div>
                        </div>
                        <div class="join cell-right xl">去拼团</div>
                    </div>
                </ul>
            </ul>
            <!-- 选择 -->
            <ul class="select-group">
                <li class="flex select-box">
                    <div class=" cell-main l">所在地区</div>
                    <div class="hui l cell-right adrs areaBtn">宁海社区17楼</div>
                </li>
                <div class="underline"></div>
                <li class="flex select-box">
                    <div class=" cell-main l">所在小区</div>
                    <div class="hui l cell-right adrs address-div">宁海社区17楼</div>
                </li>
                <div class="underline"></div>
                <li class="flex select-box">
                    <div class=" cell-main l">已选</div>
                    <div class="hui l cell-right "><span class="goodsnumb">1</span>件</div>
                    <i class="iconfont icon-more arrow"></i>
                </li>
            </ul>
            <!-- 评价 -->
            <ul class="assess">
                <li class="header flex">
                    <div class=" cell-main l">用户评价<span class="assnum">（12000）</span></div>
                    <div class="hui l cell-right adrs">查看全部</div>
                </li>
                <div class="underline"></div>
                <ul class="grade-wrapper flex">
                    <div class="btn-grade cell-main l">好评(10)</div>
                    <div class="btn-grade cell-main l">中评(10)</div>
                    <div class="btn-grade cell-main l">差评(10)</div>
                    <div class="btn-grade cell-main l">有图(10)</div>
                </ul>
                <div class="assess-box">
                    <li class="head flex middle">
                        <img src="${staticpath}/front/dist/images/c-head.png" alt="" class="pic">

                        <div class="cell-main">
                            <span class="name xl">林青晴</span>

                            <div class="hui l"><span class="time">2017-09-25</span>&nbsp; 规格：<span
                                    class="spec">白色 M码</span></div>
                        </div>
                    </li>
                    <div class="assess-det xl">总体不错，就是没有黑色的。以前都是用黑色的，然后鼠 标底部的感应有点太灵敏了，抬起来晃动鼠标。指针还是 跟着动，
                    </div>
                    <li class="flex pic-box">
                        <img src="${staticpath}/front/dist/images/c-img.png" alt="" class="img">
                        <img src="${staticpath}/front/dist/images/c-img.png" alt="" class="img">
                        <img src="${staticpath}/front/dist/images/c-img.png" alt="" class="img">
                    </li>
                </div>
            </ul>
            <!-- 推荐 -->
            <div class="Recommend">
                <img src="${staticpath}/front/dist/images/push.png" alt="" class="img">
                <li class="flex goods-box">
                    <img src="${staticpath}/front/dist/images/productImg.png" alt="" class="pic cell-right">

                    <div class="cell-main">
                        <div class="name over">山东特产 青州蜜桃冬雪蜜桃 冬桃 小毛桃新鲜水果 5斤装</div>
                        <div class="price orange xl ">79.00</div>
                        <div class="timetip">剩余时间</div>
                        <div class="time m">6天8时59分22秒</div>
                        <div class="person m">200参与</div>
                        <div class="join">去拼团</div>
                    </div>
                </li>
            </div>
        </div>
        <nav class="bar bar-tab" id="tool-bar">
            <a class="tab-item external home fn">
                <span class="ic iconfont icon-shouye "></span>

                <div class="tab-label fn">首页</div>
            </a>
            <a class="tab-item external coll fn">
                <span class="ic iconfont icon-kongxinxing collect"></span>

                <div class="tab-label fn">收藏</div>
            </a>
            <a class="tab-item fn" id="singer" data-state="1">
                <div class="price l">158.00</div>
                单独购买</a>
            <a href="#" class="tab-item fn" id="joinus">
                <div class="price l">158.00</div>
                立即购买</a>
        </nav>
    </div>
</div>

<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script> jQuery.noConflict();</script>
<script type="text/javascript">$.config = {router: false}</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/swiper-3.4.2.min.js' charset='utf-8'></script>
<#--<script type='text/javascript' src='${staticpath}/front/dist/js/group/editProduct.js' charset='utf-8'></script>-->
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/base/webUpload/webuploader.min.js' charset='utf-8'></script>
<script>
    var imgArr = new Array();

    jQuery(function ($) {
        var uploader = WebUploader.create({
            auto: true,
            swf: '${staticpath}/common/ueditor/third-party/webuploader/Uploader.swf',
            server: '${basepath}/rest/front/ued/config?action=uploadimage',
            pick: '#filePicker',
            fileNumLimit: 5,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });
        //上传成功后
        uploader.on('uploadSuccess', function (file, response) {
            uploader.removeFile(file);
            imgArr.push(response.url);
            var length = jQuery(".imgList").find("li").length;
            if (length <= 5) {
                jQuery("#filePicker").before("<li ><img class='upimg' src='/" + response.url + "'><i class='iconfont icon-close2 del-img ' data-url='" + response.url + "'></i></li>");
                jQuery("#picurl").val(imgArr.join(","));
                length < 5 ? $('#filePicker').show() : $('#filePicker').hide();
            }
        });
        jQuery(".imgList").on('click', '.del-img', function () {
            var data = $(this).attr("data-url");
            $.each(imgArr, function (index, item) {
                if (item == data) {
                    imgArr.splice(index, 1);
                }
            });
            jQuery("#picurl").val(imgArr.join(","));
            $(this).parents('li').remove();
            jQuery(".imgList").find("li").length < 6 ? $('#filePicker').show() : $('#filePicker').hide();
        });
    });
</script>
<script>
    var id = getQueryString('id');
    //只能输入数字
    function clearNoNum(obj) {
        obj.value = obj.value.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符
        if (obj.value.indexOf(".") < 0 && obj.value != "") {//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            obj.value = parseFloat(obj.value);
        }
    }
    //声明数组
    var specCombination = '',//商品规格的颜色
            specPurchasePrice = '',//商品规格的进价
            specNowPrice = '',//商品规格的现价
            specGroupPrice = '',//商品规格的团购价
            specStock = '';//商品规格库存
    //获取第一次的规格数据
    var specCombinationHidden = "",
            specPurchasePriceHidden = '',//商品规格的进价
            specNowPriceHidden = '',//商品规格的现价
            specGroupPriceHidden = '',//商品规格的团购价
            specStockHidden = '';//商品规格库存
    //查询地址
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/address/selectDefault",
        success: function (data) {
            var list = data.data;
            if (list) {
                var html = '<span class="addrname cell-right">' + list.name + '</span><span class="tel">' + list.phone + '</span>' +
                        '<div class="address overhide"><span class="area">' + list.area + '</span>&nbsp;<span class="addressDetail">' + list.address + ''+list.houseNumber+'</span></div>';
                $('.addressMain').html(html);
                $('.addr').click(function () {
                    window.location.href = '/rest/front/address/toAddressList?addressType='+2+'&editID='+id;
                })
            } else {
                $('.addr').click(function () {
                    window.location.href = '/rest/front/address/toAddressAdd?addressType='+2+'&editID='+id;
                })
            }
        }
    });
    //初始化查询商品编辑页面
    $.ajax({
        type: "GET",
        url: "/rest/front/product/selectProductDetail",
        data: {
            id: id//传活动id
        },
//        url: "/rest/front/product/selectProductDetailCS",
//        data: {
//            id: 10443,//传活动id
//            userID: 220
//        },
        dataType: "json",
        success: function (data) {
            var product = data.data;
            /**该商品的详情页面内容部分**/
            $(".p-name").val(product.name);
            $("#datetime-picker").val(product.startDate);
            $(".p-address").val(product.address);
            $(".p-textarea").val(product.introduce);
            //自提区显示循环开团
          if(sessionStorage.role==2){
            $(".checkBtn").show();
            product.isSelf == "y"?$(".check").attr('checked', true):$(".check").attr('checked', false);
          }
            //关于商品规格
            var specHtml = "";//规格数组
            var specsListHtml = "";
            $.each(product.specs, function (z, o) {
                if (z == 0) {
                    specHtml += '<li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-name"></i></div> <div class="item-inner"> <div class="item-title label">规格组1</div> <span class="addBtn">添加规格</span> </div>' +
                            '</div></li> <div class="underline"></div> <li><div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">商品规格</div> <div class="item-input"> <input type="text" class="p-color specName" placeholder="请填写商品规格颜色" value="' + o.specCombination + '"> </div> </div> </div> </li> <div class="underline"></div>' +
                            '<li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">市场价</div> <div class="item-input"> <input type="text" class="p-purchase specPurchase" placeholder="请填写市场价" value="' + o.specPurchasePrice + '" onkeyup="clearNoNum(this)"/></div></div>' +
                            '</div> </li> <div class="underline"></div><li><div class="item-content"><div class="item-media"><i class="icon icon-form-email"></i></div><div class="item-inner"> <div class="item-title label">现价</div> <div class="item-input"> <input type="text" class="p-now specNow" placeholder="请填写商品现价" value="' + o.specNowPrice + '" onkeyup="clearNoNum(this)"/> </div> </div>' +
                            '</div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div><div class="item-inner">' +
                            '<div class="item-title label">团购价</div><div class="item-input"><input type="text" class="p-group specGroup" placeholder="请填写商品团购价" value="' + o.specGroupPrice + '" onkeyup="clearNoNum(this)" /> </div> </div>' +
                            '</div></li> <div class="underline"></div><li class="text-input"> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div><div class="item-inner"> <div class="item-title label">库存</div> <div class="item-input"> <input type="text" class="p-stock specStock" placeholder="请填写商品库存" value="' + o.specStock + '" onkeyup="clearNoNum(this)"/>' +
                            '</div></div></div></li> <div class="underline"></div>';
                    specsListHtml += '<div class="specs-lis"><li ><div class="item-content"><div class="item-media"><i class="icon icon-form-email"></i></div>' +
                            '<div class="item-inner"><div class="item-title label">规格组1</div><div class="item-input" style="float: left;">' +
                            '<input type="text" class="specInput" placeholder="" readonly="readonly" value="' + o.specCombination + '"> </div> <span class="iconfont icon-fanhuijianyou addSpec"></span> </div> </div> </li> <div class="underline"></div></div>';

                } else {
                    specHtml += '<div class="add-li"><li style="margin-top: 10px;"> <div class="item-content"> <div class="item-media"><i class="icon icon-form-name"></i></div> <div class="item-inner"> <div class="item-title label">规格组' + (z + 1) + '</div><span class="delBtn">删除规格</span></div>' +
                            '</div></li><div class="underline"></div> <li><div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">商品规格</div> <div class="item-input"> <input type="text" class="p-color specName" placeholder="请填写商品规格颜色" value="' + o.specCombination + '"> </div> </div> </div> </li> <div class="underline"></div>' +
                            '<li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">市场价</div> <div class="item-input"> <input type="text" class="p-purchase specPurchase" placeholder="请填写市场价" value="' + o.specPurchasePrice + '" onkeyup="clearNoNum(this)"/></div></div>' +
                            '</div> </li> <div class="underline"></div><li><div class="item-content"><div class="item-media"><i class="icon icon-form-email"></i></div><div class="item-inner"> <div class="item-title label">现价</div> <div class="item-input"> <input type="text" class="p-now specNow" placeholder="请填写商品现价" value="' + o.specNowPrice + '" onkeyup="clearNoNum(this)"/> </div> </div>' +
                            '</div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div><div class="item-inner">' +
                            '<div class="item-title label">团购价</div><div class="item-input"><input type="text" class="p-group specGroup" placeholder="请填写商品团购价" value="' + o.specGroupPrice + '" onkeyup="clearNoNum(this)"/> </div> </div>' +
                            '</div></li> <div class="underline"></div><li class="text-input"> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div><div class="item-inner"> <div class="item-title label">库存</div> <div class="item-input"> <input type="text" class="p-stock specStock" placeholder="请填写商品库存" value="' + o.specStock + '" onkeyup="clearNoNum(this)"/>' +
                            '</div></div></div></li> <div class="underline"></div></div>';
                    specsListHtml += '<div class="specs-lis"><li><div class="item-content"><div class="item-media"><i class="icon icon-form-email"></i></div>' +
                            '<div class="item-inner"><div class="item-title label">规格组' + (z + 1) + '</div><div class="item-input" style="float: left;">' +
                            '<input type="text"  placeholder="" readonly="readonly" value="' + o.specCombination + '"> </div> <span class="iconfont icon-fanhuijianyou addSpec"></span> </div> </div> </li> <div class="underline"></div></div>';
                }
            });
            $("#pageTwo ul").html(specHtml);

            $(".spec-li").append(specsListHtml);
            $(".delBtn").click(function () {
                $(this).parents('.add-li').remove();
            });
            $("#datetime-picker").datetimePicker({
                minDate: new Date()
            });
            //点击规格跳转到添加规格的页面
            $(".spec-li").click(function () {
                $("#pageOne").hide();
                $("#pageTwo").show();
            });

            //点击规格跳转到添加规格的页面
            $(".addSpec").click(function () {
                $("#pageOne").hide();
                $("#pageTwo").show();
            });
            var j = $(".specs-lis").length==0? $(".add-liSpec").length : $(".specs-lis").length ;//获取条数
            //点击添加规格数组，在添加一行规格数组
            $(".addBtn").click(function () {
                j++;
                $("#pageTwo ul").append('<div class="add-li"><li style="margin-top: 10px;"> <div class="item-content"> <div class="item-media"><i class="icon icon-form-name"></i></div> <div class="item-inner"> <div class="item-title label">规格组' + j + '</div> <span class="delBtn">删除规格</span> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">商品规格</div> <div class="item-input"> <input type="text" class="p-color" placeholder="请填写商品规格颜色"> </div> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">市场价</div> <div class="item-input"> <input type="number" class="p-purchase" placeholder="请填写市场价" onkeyup="clearNoNum(this)"/> </div> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">现价</div> <div class="item-input"> <input type="number" class="p-now" placeholder="请填写商品现价" onkeyup="clearNoNum(this)"/> </div></div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">团购价</div> <div class="item-input"> <input type="text" class="p-group" placeholder="请填写商品团购价" onkeyup="clearNoNum(this)"/> </div> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">库存</div> <div class="item-input"> <input type="number" class="p-stock" placeholder="请填写商品库存" onkeyup="clearNoNum(this)"/ > </div> </div> </div> </li></div>');
                //点击删除规格数组，在删除一行规格数组
                $(".delBtn").click(function () {
                    j--;
                    $(this).parents('.add-li').remove();
                });
            });

            //点击规格的保存或者返回键跳转到添加商品的页面
            $("#saveSpec").click(function () {

                querySpec();
                eachInput();
            });
            $(".spec-left").click(function () {
                $("#pageOne").show();
                $("#pageTwo").hide();
            });

            $(".specName").each(function () {
                specCombinationHidden += $(this).val() + ",";
            });
            $(".specPurchase").each(function () {
                specPurchasePriceHidden += $(this).val() + ",";
            });
            $(".specNow").each(function () {
                specNowPriceHidden += $(this).val() + ",";
            });
            $(".specGroup").each(function () {
                specGroupPriceHidden += $(this).val() + ",";
            });
            $(".specStock").each(function () {
                specStockHidden += $(this).val() + ",";
            });
            //关于图片上传
            var pictureList = product.images;
            imgArr = pictureList.split(',');
            for (var i = 0; i < imgArr.length; i++) {
                $(".imgList").prepend("<li ><img class='upimg' src='/" + imgArr[i] + "'><i class='iconfont icon-close2 del-img ' data-url='" + imgArr[i] + "'></i></li>");
            }
            jQuery("#picurl").val(pictureList);
            jQuery(".imgList").find("li").length < 6 ? $('#filePicker').show() : $('#filePicker').hide();
            //绑定删除按钮事件
            jQuery(".del-img").click(function () {
                //更新数组
                var data = $(this).attr("data-url");
                //splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。
                $.each(imgArr, function (index, item) {
                    if (item == data) {
                        imgArr.splice(index, 1);
                    }
                });

                //更新input值
                jQuery("#picurl").val(imgArr.join(","));
                //移除图片显示
                $(this).parents('li').remove();
                jQuery(".imgList").find("li").length < 6 ? $('#filePicker').show() : $('#filePicker').hide();
            });

        }
    });
    //规格数组的方法
    function querySpec() {
        specCombination = '';//商品规格的颜色
        specPurchasePrice = '';//商品规格的进价
        specNowPrice = '';//商品规格的现价
        specGroupPrice = '';//商品规格的团购价
        specStock = '';//商
        $(".p-color").each(function () {
            specCombination += $(this).val() + ",";
        });
        $(".p-purchase").each(function () {
            specPurchasePrice += $(this).val() + ",";
        });
        $(".p-now").each(function () {
            specNowPrice += $(this).val() + ",";
        });
        $(".p-group").each(function () {
            specGroupPrice += $(this).val() + ",";
        });
        $(".p-stock").each(function () {
            specStock += $(this).val() + ",";
        });
    }
    //判断所有input是否为空
    function eachInput() {
        var x = 0;//判断input是否为空
        $("#pageTwo input").each(function () {
            if ($(this).val() == "") {
                x++;
            }
        });
        if (x > 0) {
            $.alert("请填写完整信息")
        } else {
            var specCombinationArr = specCombination.substr(0, specCombination.length - 1).split(",");

            var dp=0;
            $("#pageOne").show();
            $("#pageTwo").hide();
            $(".specs-lis").remove();
            $(".add-liSpec").remove();
            for (var o = 0; o < specCombinationArr.length; o++) {

                dp++;

                    $(".spec-li").append('<div class="add-liSpec"><li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">规格组' + dp + '</div> <div class="item-input" style="float: left;"> <input type="text"  value="' + specCombinationArr[o] + '" readonly="readonly"> </div><span class="iconfont icon-fanhuijianyou addSpec"></span></div> </div> </li><div class="underline"></div></div>');
            }
            $.toast("保存成功");
            setTimeout(function () {
//                $("#pageOne").show();
//                $("#pageTwo").hide();
            }, 100);

        }
    }
    var params;
    //保存并提交申请
    $('.saveBtn').click(function () {
      var isSelf="";
      if(sessionStorage.role==2){
        isSelf= $(".check").prop("checked") == true ? "y" : "n";
      }else{
        isSelf="n";
      }
        //保存并提交申请
        var name = $('.p-name').val(),//商品名称
                images = $('#picurl').val(),//商品图片
                time = $('#datetime-picker').val(),//商品开团时间
                garden = $('.addressDetail').text(),//小区
                address = $('.p-address').val(),//自提点
                linkMan = $('.addrname').text(),//联系人
                linkPhone = $('.tel').text(),//联系电话
                reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/,
                area = $('.area').text(),//省市区
                introduce = $('.p-textarea').val();//商品详细信息
        if (!name || !images || !time || !garden || !address || !linkMan || !linkPhone || !area || !introduce) {
            $.alert('请填写完整信息!');
            return false;
        }
      if (specCombination == "" || specPurchasePrice == "" || specNowPrice == "" || specGroupPrice == "" || specStock == "") {
        params = {
          id: id,
          name: name,
          images: images,
          isSelf:isSelf,
          specCombination: specCombinationHidden.substr(0, specCombinationHidden.length - 1),
          specPurchasePrice: specPurchasePriceHidden.substr(0, specPurchasePriceHidden.length - 1),
          specNowPrice: specNowPriceHidden.substr(0, specNowPriceHidden.length - 1),
          specGroupPrice: specGroupPriceHidden.substr(0, specGroupPriceHidden.length - 1),
          specStock: specStockHidden.substr(0, specStockHidden.length - 1),
          startDate: time,
          garden: garden,
          address: address,
          linkMan: linkMan,
          linkPhone: linkPhone,
          area: area,
          introduce: introduce
        };
      } else {
        params = {
          id: id,
          name: name,
          isSelf:isSelf,
          images: images,
          specCombination: specCombination.substr(0, specCombination.length - 1),
          specPurchasePrice: specPurchasePrice.substr(0, specPurchasePrice.length - 1),
          specNowPrice: specNowPrice.substr(0, specNowPrice.length - 1),
          specGroupPrice: specGroupPrice.substr(0, specGroupPrice.length - 1),
          specStock: specStock.substr(0, specStock.length - 1),
          startDate: time,
          garden: garden,
          address: address,
          linkMan: linkMan,
          linkPhone: linkPhone,
          area: area,
          introduce: introduce
        };
      }
      return reg.test(linkPhone) ? Submit(params) : $.toast('手机格式不正确');
    });
    function Submit(params) {
      $.ajax({
        type: 'POST',
        url: "/rest/front/product/updateProduct",
        data: params,
        dataType: 'json',
        success: function (data) {
          if (data.success) {
            $.toast("保存成功");
            setTimeout(function () {
              window.location.href = "/rest/front/product/toProductList";
            }, 1000);
          } else {
            $.alert(data.message);
          }

        }
      })
    }
    //点击预览跳转到预览的商品详情页面
    $(".previewBtn").click(function () {
    //保存并提交申请
        $("#pageThree").show();
        $("#pageOne").hide();
        //显示商品详情的图片
        var imgArr = $("#picurl").val().split(',');
        var imgHtml = '';
        if (imgArr.length == 1) {
            $(".swiper-wrapper").html('<div class="swiper-slide"><img src="/' + imgArr[0] + '"></div>');
        } else {
            for (var q in imgArr) {
                imgHtml += '<div class="swiper-slide"><img src="/' + imgArr[q] + '"></div>';
            }
            $(".swiper-wrapper").html(imgHtml);
            var mySwiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                autoplay: 5000,
                loop: true,
                paginationClickable: true
            });
        }
        //点击单独购买欲立即购买给出提示
        $("#singer,#joinus").click(function () {
            $.alert("预览商品不支持购买");
        });
        //点击去拼团给出提示
        $(".join").click(function () {
            $.alert("预览商品不支持去拼团");
        });
        //点击去拼团给出提示
        $(".share").click(function () {
            $.alert("预览商品不支持分享");
        });
        //点击首页给出提示
        $(".home").click(function () {
            $.alert("预览商品不支持跳转到首页");
        });
        //点击收藏给出提示
        $(".coll").click(function () {
            $.alert("预览商品不支持收藏");
        });
        //显示商品名称
        $(".goods-det .over-h1").text($('.p-name').val());
        //显示商品详情
        $(".goods-det .over-p").text($('.p-textarea').val());
        //显示地区
        $(".areaBtn").text($('.area').text());
        //显示小区
        $(".address-div").text($('.addressDetail').text());
    });
    //点击预览商品详情的地方返回
    $(".back").click(function () {
        $("#pageOne").show();
        $("#pageThree").hide();
    });
    function pBack() {
        window.location.href = "/rest/front/product/toProductList";
    }
</script>
</body>

</html>

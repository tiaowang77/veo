<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_up1w8xn3gak2zkt9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
</head>
<style>
    .page {
        background-color: #f5f6f7;
    }

    .bar:after {
        height: 0;
    }
    ul{
        margin-bottom: 7px;
    }
</style>
<body>
<div class="page-group">
    <div class="page">
        <header class="bar bar-nav ">
            <input type="text" class="inp  cell-left" placeholder="搜索商品">

            <div class="search pull-right">搜索</div>
        </header>
        <div class="content">
            <div class="buttons-row tab-group middle">
                <a href="#new" class="tab-link active cell-main new" data-type="0">最新</a>
                <a href="#hot" class="tab-link  cell-main hot" data-type="0">热销</a>
                <#--<a href="#hot" class="tab-link  cell-main wait">即将开团</a>-->
            </div>
            <div class="good-wrapper"></div>
            <#--<div class="good-wait"></div>-->
            <div class="lack lack-top">
                <img src="${staticpath}/front/dist/images/lack_goods.png" alt="" class="pic-lack">
                <div class="text-lack">亲，没有查询到商品哦~</div>
            </div>
        </div>
        <!-- 工具栏 -->
        <nav class="footer-bar bar bar-tab ">
            <a class="tab-item external active fn" >
                <img src="${staticpath}/front/dist/images/groupBuy-1.png" class="ico">

                <p class="tab-label">团购</p>
            </a>
            <#--<a class="tab-item external fn" href="/rest/front/product/toProductList">-->
                <#--<img src="${staticpath}/front/dist/images/openGroupBtn-1.png" class="ico">-->

                <#--<p class="tab-label">开团</p>-->
            <#--</a>-->
            <a class="tab-item external fn " href="/rest/front/user/toMyPage">
                <img src="${staticpath}/front/dist/images/userBtn-1.png" class="user-ico ico">

                <p class="tab-label">个人</p>
            </a>
        </nav>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script>
    $.config = {
        router: false
    }
</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/group/star.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/group/groupPurchase.js' charset='utf-8'></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/vconsole.min.js' charset='utf-8'></script>
<script> jQuery.noConflict();</script>
<script>
    $.init();
    var vConsole = new VConsole();
</script>
<script>
    jQuery(function ($) {
        $('.search').click(function () {
            var inp = $('.inp').val();
            if(inp){
                $(".good").hide();
                $("a:contains('" + inp + "')").parents('.good').show()
            }
        });
        $('.inp').keyup(function () {
            $(this).val() == '' && $(".good").show();
        })
    })
</script>
</body>

</html>

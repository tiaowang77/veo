<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
</head>
<body>
<div class="page-group">
    <div class="page">
        <div class="content openGroup-content">
            <!--头部内容 !-->
            <div class="searchbar">
                <div class="add-div" id="addProduct">
                    <img src="${staticpath}/front/dist/images/addBtn.png">

                    <p>添加</p>
                </div>
                <input type="text" placeholder="搜索商品" class="search">
                <span class="search-span">搜索</span>
            </div>
            <div class="underline">
            </div>
            <div class="tabs">
                <div id="all-tab" class="tab active" data-url="">
                    <ul class="o-list">
                        <#--<li class="row row-list" data-oid="' + o.id + '" data-status="' + o.checkStatus + '">-->
                            <#--<div class="col-20"><img src="/' + img + '"/></div>-->
                            <#--<div class="col-80">-->
                                <#--<a class="p-list">文化途锐我很快</a>-->

                                <#--<div class="pspec">-->
                                    <#--<div class="join-div"><span class="pe-span">55人参加&nbsp;</span> <span-->
                                            <#--class="time-span time' + i + '""></span></div>-->
                                <#--</div>-->
                                <#--<div class="price-div"><span class="price s-price orange">&nbsp;88</span> <span-->
                                        <#--class="price time-div x line-span">88</span>-->
                                    <#--<span class="detailOrder">查看订单</span>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</li>-->
                        <#--<li class="row row-list" data-oid="' + o.id + '" data-status="' + o.checkStatus + '">-->
                            <#--<div class="col-20"><img src="/' + img + '"/></div>-->
                            <#--<div class="col-80">-->
                                <#--<a class="p-list">文化途锐我很快</a>-->

                                <#--<div class="join-div pel-span div-p">20人参与</div>-->
                                <#--<div class="price1-div">-->
                                    <#--<div class="detailOrder">查看订单</div>-->
                                    <#--<div class="openGroup">再次开团</div>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</li>-->
                        <#--<li class="row row-list" data-oid="' + o.id + '" data-status="' + o.checkStatus + '">-->
                            <#--<div class="col-20"><img src="/' + img + '"/></div>-->
                            <#--<div class="col-80">-->
                                <#--<a class="p-list">文化途锐我很快</a>-->

                                <#--<div class="join-div status-span">审核中</div>-->

                                <#--<div class="price1-div openGroup">编辑商品</div>-->
                            <#--</div>-->
                        <#--</li>-->
                        <#--<li class="row row-list" data-oid="' + o.id + '" data-status="' + o.checkStatus + '">-->
                            <#--<div class="col-20"><img src="/' + img + '"/></div>-->
                            <#--<div class="col-80">-->
                                <#--<a class="p-list">文化途锐我很快</a>-->

                                <#--<div class="pspec">-->
                                    <#--<div class="join-div"><span class="pe-span">55人参加&nbsp;</span> <span-->
                                            <#--class="time-span time' + i + '""></span></div>-->
                                <#--</div>-->
                                <#--<div class="price-div"><span class="price s-price orange">&nbsp;88</span> <span-->
                                        <#--class="price time-div x line-span">88</span>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</li>-->
                        <#--<li class="row row-list" data-oid="' + o.id + '" data-status="' + o.checkStatus + '">-->
                            <#--<div class="col-20"><img src="/' + img + '"/></div>-->
                            <#--<div class="col-80">-->
                                <#--<a class="p-list">文化途锐我很快</a>-->

                                <#--<div class="pspec">-->

                                    <#--<div class="join-div"><span class="status-span">未通过</span></div>-->
                                    <#--<div class="price1-div openGroup">再次开团</div>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</li>-->
                    <div class="lack lack-top">
                    <img src="${staticpath}/front/dist/images/lack_cang.png" alt="" class="pic-lack">
                    <div class="text-lack">亲，还没有上架到商品哦~</div>
                      <div class="addProductBtn">添加商品</div>
                    </div>
                    </ul>
                </div>
            </div>
        </div>
        <nav class="footer-bar bar bar-tab">
            <a class="tab-item external" href="/rest/front/product/toGroupPurchase">
                <img src="${staticpath}/front/dist/images/groupBuy.png" class="ico">

                <p class="tab-label">团购</p>
            </a>
            <a class="tab-item external active">
                <img src="${staticpath}/front/dist/images/openGroupBtn.png" class="ico">

                <p class="tab-label">开团</p>
            </a>
            <a class="tab-item external " href="/rest/front/user/toMyPage">
                <img src="${staticpath}/front/dist/images/userBtn-1.png" class="user-ico ico">

                <p class="tab-label">个人</p>
            </a>
        </nav>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script>    $.config = {router: false}    </script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/group/openGroup.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script> jQuery.noConflict();</script>
<script>
    $.init();
</script>
<script>
    jQuery(function ($) {
        $('.search-span').click(function () {
            var inp = $('.search').val();
            if (inp) {
                $(".row").hide();
                $("a:contains('" + inp + "')").parents('.row').show()
            }
        });
        $('.search').keyup(function () {
            $(this).val() == '' && $(".row").show();
        })
    })
</script>
</body>

</html>

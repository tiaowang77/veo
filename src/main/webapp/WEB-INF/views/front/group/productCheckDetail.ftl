<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_1xs8m5vedep3z0k9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/swiper-3.4.2.min.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
</head>
<style>
    .page {
        background-color: #f5f6f7;
    }
</style>

<body>
<div class="page-group">
    <div class="page page-current">
        <div class="content">
            <!-- 轮播 -->
            <div class="swiper-container">
                <div class="back">
                    <i class="iconfont icon-more1 ico"></i>
                </div>
                <div class="share">
                    <i class="iconfont icon-fenxiang ico"></i>
                </div>
                <div class="swiper-wrapper">
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <!-- 参团 -->
            <ul class="group">
                <ul class="underline">
                    <li class="flex header groupmore">
                        <h1 class="xl cell-left cell-main"><span class="text">开团日期</span></h1>

                        <div class="more hui l openTime-li"></div>
                    </li>
                    <div class="upline"></div>
                    <li class="flex header groupmore">
                        <h1 class="xl cell-left cell-main"><span class="text">所在小区</span></h1>

                        <div class="more hui l add-li"></div>
                    </li>
                    <div class="upline"></div>
                    <li class="flex header groupmore">
                        <h1 class="xl cell-left cell-main"><span class="text">自提点</span></h1>

                        <div class="more hui l self-li"></div>
                    </li>
                    <div class="upline"></div>
                    <li class="flex header groupmore">
                        <h1 class="xl cell-left cell-main"><span class="text">名字</span></h1>

                        <div class="more hui l name-li"></div>
                    </li>
                    <div class="upline"></div>
                    <li class="flex header groupmore">
                        <h1 class="xl cell-left cell-main"><span class="text">电话</span></h1>

                        <div class="more hui l phone-li"></div>
                    </li>
                </ul>
            </ul>

            <div id="editBtn">
                <a href="#" class="saveBtn button">通过</a>
            </div>
            <div id="editBtn">
                <a href="#" class="previewBtn button ">不通过</a>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/swiper-3.4.2.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/group/productCheckDetail.js' charset='utf-8'></script>
<script>
    $.init()
</script>
</body>

</html>

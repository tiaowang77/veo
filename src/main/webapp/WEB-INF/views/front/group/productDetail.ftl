<html>

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <title>味猫商城</title>
  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_1xs8m5vedep3z0k9.css">
  <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
  <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/swiper-3.4.2.min.css">
  <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
  <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
</head>
<style>
  .page {
    background-color: #f5f6f7;
  }
</style>

<body>
<div class="page-group">
  <div class="page page-current">
    <div class="content">
      <!-- 轮播 -->
      <div class="swiper-container">
        <div class="back" onclick="indexBack()">
          <i class="iconfont icon-more1 ico"></i>
        </div>
        <div class="share">
          <i class="iconfont icon-fenxiang ico"></i>
        </div>
        <div class="swiper-wrapper">

        </div>
        <div class="swiper-pagination"></div>
      </div>
      <#--<!-- 参团 &ndash;&gt;-->
      <#--<ul class="group">-->
        <#--<ul class="underline">-->
          <#--<li class="flex header groupmore">-->
            <#--<h1 class="xl cell-left cell-main"><span class="groupnum">0</span><span class="text">人在参团</span></h1>-->

            <#--<div class="more hui l">查看更多</div>-->
            <#--<i class="iconfont icon-more arrow cell-right"></i>-->
          <#--</li>-->
        <#--</ul>-->
      <#--</ul>-->
      <!-- 选择 -->
      <ul class="select-group">
        <div class="underline"></div>
        <li class="flex select-box select-spec">
          <div class=" cell-main l">已选</div>
          <div class="hui l cell-right "><span class="goodsnumb">1</span>件</div>
          <i class="iconfont icon-more arrow"></i>
        </li>
      </ul>
      <!-- 评价 -->
      <#--<ul class="assess">-->
        <#--<li class="header flex">-->
          <#--<div class=" cell-main l">用户评价（<span class="assnum">0</span>）</div>-->
          <#--<a href="#pageTwo" class="hui l cell-right">查看全部</a>-->
          <#--<i class="iconfont icon-more arrow"></i>-->
        <#--</li>-->
        <#--<div class="underline"></div>-->
        <#--<ul class="grade-wrapper flex">-->
          <#--<div class="btn-grade cell-main l" data-type="1"><a href="#pageTwo">好评(<span class="i">0</span>)</a>-->
          <#--</div>-->
          <#--<div class="btn-grade cell-main l" data-type="2"><a href="#pageTwo">中评(<span class="j">0</span>)</a>-->
          <#--</div>-->
          <#--<div class="btn-grade cell-main l" data-type="3"><a href="#pageTwo">差评(<span class="k">0</span>)</a>-->
          <#--</div>-->
          <#--<div class="btn-grade cell-main l" data-url="y"><a href="#pageTwo">有图(<span class="x">0</span>)</a>-->
          <#--</div>-->
        <#--</ul>-->
        <#--<div class="assess-box">-->

        <#--</div>-->
      <#--</ul>-->
      <div id="goodsDet-tab" style="margin:15px 0 10px 0;">

      </div>
      <!-- 详情 -->

      <!-- 推荐 -->
      <#--<div class="Recommend">-->
        <#--<img src="${staticpath}/front/dist/images/push.png" alt="" class="img">-->

      <#--</div>-->
    </div>
    <nav class=" flex" id="tool-bar">
      <div class="home">
        <span class="ic iconfont icon-shouye "></span>
        <div class=" fn">首页</div>
      </div>
      <div class="  coll">
        <span class="ic iconfont icon-kongxinxing collect"></span>
        <div class=" fn">收藏</div>
      </div>
      <div class=" fn cell-main flex middle " id="share" data-state="1">
        <span class="auto">分享</span>
      </div>
      <div class=" fn cell-main flex middle" id="joinus">
        <span class="auto">购买</span>
      </div>
    </nav>
  </div>
  <div class="expand"></div>
  <img src="" alt="" class="bigpic">
  <!-- 拼团弹窗 -->
  <div class="jointeam">
    <div class="close">&times;</div>
    <div class="header ">
      <h1 class="title">正在拼团</h1></div>
    <ul class="box-team upline">

    </ul>
  </div>
  <!-- 规格 -->
  <ul class="spec-box">
    <img src="" alt="" class="goodpic">
    <ul class="mb">
      <li class="spec-main">

      </li>
      <div class="flex flex-wrap spec-btn">

      </div>
      <li class="upline underline">
        <div class="flex numsel">
          <div class="text">购买数量</div>
          <div class="cell-main right">
            <div class="reduce"><i class="iconfont icon-jian"></i></div>
            <span class="number hui">1</span>

            <div class="add"><i class="iconfont icon-jia"></i></div>
          </div>
        </div>
      </li>
    </ul>
    <div class="bottom">
      <div class="flex">
        <div class="cell-main joingroup">去付款</div>
      </div>
    </div>
    <i class="close-box iconfont icon-close2"></i>
  </ul>


<img src="${staticpath}/front/dist/images/tip.png" alt="" class="pictip">


<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/swiper-3.4.2.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
    <script type='text/javascript' src='${staticpath}/front/dist/js/group/productDetail.js' charset='utf-8'></script>

    <script>
  $.init()
</script>
</body>

</html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_up1w8xn3gak2zkt9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <style>
        .page {
            background-color: #f5f6f7;
        }
    </style>

</head>

<body>
<div class="page-group">
    <header class="bar bar-nav ">
        <button class="back-button button button-link button-nav pull-left" onclick="ToBack()">
            <span class="icon icon-left"></span>返回
        </button>
        <input type="text" class="inp  cell-left sendsear-bar" placeholder="搜索商品">
        <div class="search pull-right">搜索</div>
    </header>
    <div class="content ">
        <div id="all" class="tab ">
            <ul class="group-box">

            </ul>
        </div>
    </div>
</div>
</div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/group/sendGood.js' charset='utf-8'></script>
</body>

</html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/print.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <link rel="stylesheet" href="//at.alicdn.com/t/font_508245_5a1y3g2n5ti0ms4i.css"/>
</head>
<style>
    .page {
        background-color: #f5f6f7;
    }
    .bar .iconfont {
        font-size: 1.25rem!important;
        line-height: 2.25rem;
    }

</style>
<body>
<div class="page-group">
    <div class="page">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <button class="button button-link button-nav pull-left" onclick="back()">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">全部订单</h1>
        </header>
        <div class="content allOrder-content">
            <!--tab栏 !-->
            <div class="buttons-tab tab-mr">
                <a id="all-tab-btn" class="tab-link  button active" data-status="all">全部</a>
                <a id="payment-tab" class="tab-link  button" data-status="init">待付款</a>
                <a id="send-tab" class="tab-link button" data-status="pass">待发货</a>
                <a id="harvest-tab" class="tab-link button" data-status="send">待收货</a>
                <a id="complete-tab" class="tab-link button" data-status="finish">已完成</a>
                <a id="share-tab" class="tab-link button" data-status="share">分享</a>
            </div>
            <div class="tabs">
                <div id="all-tab" class="active">

                </div>
                <div id="share" class="tab"></div>
                <div class="lack lack-top">
                    <img src="${staticpath}/front/dist/images/lack_order.png" alt="" class="pic-lack">
                    <div class="text-lack">亲，没有查询到订单哦~</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/order/allOrder.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>

</body>

</html>

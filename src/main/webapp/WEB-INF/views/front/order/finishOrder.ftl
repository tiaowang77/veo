<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_up1w8xn3gak2zkt9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <style>
        .page{background-color: #f5f6f7;}
    </style>
</head>

<body>
<div class="page-group">
    <div class="page page-current" id="pas">
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="back()">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">订单详情</h1>
        </header>
        <div class="content ">
            <li class="addr flex middle">
                <i class="cell-left cell-right iconfont icon-dingwei ico"></i>
                <div class="cell-main addr-main">
                    <span class="name cell-right">暂无配送地址</span>
                    <div class="address overhide">还没有配送地址，赶紧去添加吧</div>
                </div>
            </li>
            <img src="${staticpath}/front/dist/images/line.png" alt="" class="colorline">
            <ul class="orderlist">
                <div class="title-box goodlist">
                    <span class="tit cell-left">商品清单</span>
                    <span class="pull-right cell-right state"></span>
                </div>

                <li class="link-box right ">

                    <!--  <span class="watch">取消订单</span> -->
                </li>
            </ul>
            <ul class="inf-wrapper">
                <div class="title-box">
                    <span class="tit cell-left">基本信息</span>
                </div>
                <div class="underline"></div>
                <div class="column baseinf">


                    <li class="flex inf remark-box">
                        <div class="inf-l cell-right">买家留言</div>
                        <div class="inf-r cell-main  right remark">未填写</div>
                    </li>
                </div>
            </ul>
            <ul class="inf-wrapper">
                <div class="title-box">
                    <span class="tit cell-left">价格信息</span>
                </div>
                <div class="underline"></div>
                <div class="column">
                    <li class="flex inf">
                        <div class="inf-l">商品总价</div>
                        <div class="inf-r cell-main right"><span class="price totalprice">55.80</span>元</div>
                    </li>
                    <li class="flex inf">
                        <div class="inf-l">余额支付</div>
                        <div class="inf-r cell-main right"><span class="price wallet">0.00</span>元</div>
                    </li>
                    <li class="flex inf">
                        <div class="inf-l">实付金额</div>
                        <div class="inf-r cell-main right"><span class="price actual ">55.80</span>元</div>
                    </li>
                </div>
            </ul>
        </div>
        <nav class="bar bar-tab payment-bar-tab" id="tool-bar">
            <div class="evaluate tocomment hide">去评价</div>
            <div class="evaluate paynew hide">立即支付</div>
            <div class="evaluate tosure hide">确认收货</div>
        </nav>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/order/finishOrder.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>

</html>

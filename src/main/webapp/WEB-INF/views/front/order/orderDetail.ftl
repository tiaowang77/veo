<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_vrfd3m7xch3kgldi.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <style>
        .page {
            background-color: #f5f6f7;
        }
    </style>
</head>

<body>
<div class="page-group">
    <div class="page page-current" id="pas">
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" id="back">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">订单详情</h1>
        </header>
        <div class="content ">
            <li class="addr flex middle">
                <i class="cell-left cell-right iconfont icon-dingwei ico"></i>
                <div class="cell-main addr-main">
                    <span class="name cell-right">暂无配送地址</span>
                    <div class="address overhide">还没有配送地址，赶紧去添加吧</div>
                </div>
                <i class="cell-left cell-right iconfont icon-more ico"></i>
            </li>
            <img src="${staticpath}/front/dist/images/line.png" alt="" class="colorline">
            <ul class="orderlist">

            </ul>
            <div class="orderinf">
                <div class="underline"></div>
                <li class="inf flex">
                    <div class="text cell-left">使用余额</div>
                    <input type="number" id="money" class="inp cell-main money" placeholder="可用余额为0.00">
                </li>
                <div class="underline"></div>
                <li class="remark">
                    <div>用户备注（50字）</div>
                    <textarea class="texta" placeholder="选填"></textarea>
                </li>
            </div>
        </div>
        <nav class="bar bar-tab payment-bar-tab">
            <div class="total ">
                <span class="totaltip">合计:</span>
                <span class="totalprice price orange">55.8</span>
            </div>
            <div class="evaluate">去结算</div>
        </nav>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/order/orderDetail.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>

</html>

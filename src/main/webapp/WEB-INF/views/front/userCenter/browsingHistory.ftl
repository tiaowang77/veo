<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">

    <style>

    </style>
</head>

<body>
<div class="page-group">
    <div class="page">
        <!--头部内容 !-->
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="ToBack()">
                <span class="icon icon-left"></span>返回
            </button>
            <div class=" pull-right cell-right z-in saveBtn" data-state="0">
                <span>编辑</span>
            </div>
            <h1 class="title">我的足迹</h1>
        </header>
        <!--内容部分!-->
        <div class="content ">
            <ul class="collect">
                <div class="coll-goods">
                    <div class="lack lack-top">
                        <img src="${staticpath}/front/dist/images/lack_foot.png" alt="" class="pic-lack">
                        <div class="text-lack">亲，先逛逛商城吧~</div>
                    </div>
                </div>
            </ul>
        </div>
        <nav class="bar bar-tab payment-bar-tab hide" id="tool-bar">
            <span data-url="" class="delBtn">删除</span>
            <div class="allCheck" >
                全选
                <label for="">
                    <input type="checkbox" class="checkbox"  />
                    <i class="select iconfont  pull-right CheckAll" data-state="0"></i></label></div>
        </nav>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script>    $.config = {        router: false    }    </script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/userCenter/history.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>

<script>
    $.init();
</script>
</body>

</html>

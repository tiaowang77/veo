<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_up1w8xn3gak2zkt9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <style>
        .page{background-color: #f5f6f7;}
    </style>
</head>

<body>
<div class="page-group">
    <div class="page page-current" id="pas">
        <header class="bar bar-nav">
            <span class="iconfont icon-jiantouright ico pull-left cell-left" onclick="ToBack()"></span>
            <h1 class="title">密码</h1>
        </header>
        <div class="content ">
            <li class="psw-box flex">
                <span class="cell-left cell-right text">新密码</span>
                <input type="number" class="pws inp cell-main" placeholder="请填写新密码" >
            </li>
            <div class="underline"></div>
            <li class="psw-box flex">
                <span class="cell-left cell-right text">确定密码</span>
                <input type="number" class="pws-new inp cell-main" placeholder="重新输入新密码" >
            </li>
            <div class="underline"></div>
            <div class="btn auto psw-up">确定</div>
        </div>
    </div>
    <div class="page " id="tel">
        <header class="bar bar-nav">
            <span class="iconfont icon-jiantouright ico pull-left cell-left" onclick="ToBack()"></span>
            <h1 class="title">绑定手机</h1>
        </header>
        <div class="content ">
            <li class="psw-box flex">
                <span class="cell-left cell-right text">手机号</span>
                <input type="number" class="tel inp cell-main" placeholder="请填写手机号" >
                <input type="hidden"  id="SmsId" value="">
                <div class="confirm yan">验证码</div>
            </li>
            <div class="underline"></div>
            <li class="psw-box flex">
                <span class="cell-left cell-right text">验证码</span>
                <input type="number" class="yan-inp inp cell-main" placeholder="请填写验证码" >
            </li>
            <div class="underline"></div>
            <div class="btn auto tel-up">确定</div>
        </div>
    </div>
    <div class="page " id="name">
        <header class="bar bar-nav">
            <span class="iconfont icon-jiantouright ico pull-left cell-left" onclick="ToBack()"></span>
            <h1 class="title">修改用户名</h1>
        </header>
        <div class="content ">
            <li class="psw-box flex">
                <span class="cell-left cell-right text">用户名</span>
                <input type="text" class="name-inp inp cell-main" placeholder="请填写用户名" >
            </li>
            <div class="underline"></div>
            <div class="btn auto name-up">确定</div>
        </div>
    </div>
    <div class="page " id="mail">
        <header class="bar bar-nav">
            <span class="iconfont icon-jiantouright ico pull-left cell-left" onclick="ToBack()"></span>
            <h1 class="title">修改邮箱</h1>
        </header>
        <div class="content ">
            <li class="psw-box flex">
                <span class="cell-left cell-right text">邮箱</span>
                <input type="title" class="mail-inp inp cell-main" placeholder="请填写邮箱" >
            </li>
            <div class="underline"></div>
            <div class="btn auto mail-up">确定</div>
        </div>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script>    $.config = {        router: false    }</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/userCenter/revise.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>

</html>
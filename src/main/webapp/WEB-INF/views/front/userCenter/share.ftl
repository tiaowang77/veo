<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_re1nubji8k2wqaor.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
</head>
<style>
    #sharePic {
        position: absolute;
        top: 0;
        width: 100%;
        bottom: 0;
        background: url('${staticpath}/front/dist/images/share.jpg') no-repeat;
        background-size: 100% 100%;
    }
    #createImg{
        width: 100%;
        bottom: 0;
        z-index: 3;
    }
    .codeImg{
        position: absolute;
        right: 20px;
        bottom: 20px;
        width: 100px;
        height: 100px;
    }
</style>
<body>
<div class="page-group">
    <div class="page">
        <div class="content " id="sharePic"->
                <img src="" alt="" class="codeImg">
        </div>
        <img class="content " id="createImg" />
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script>    $.config = {router: false}    </script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/html2canvas.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/canvas2image.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/base64.js' charset='utf-8'></script>

<script>
    $.init();
    var vConsole = new VConsole();
</script>
<script>
    var id=getQueryString('id');
    var userId=sessionStorage.id;
    var shareID=userId+'_'+id;
    (function (shareID) {
       $.ajax({
           url:'/rest/front/product/createProductQRcode',
           type:'POST',
           dataType:'json',
           data:{shareID:shareID},
           success:function (data) {
               $('.codeImg').attr('src','/'+data.data);
               return createCanvus()
           }
       })
    })(shareID);
    function createCanvus() {
        html2canvas($('#sharePic'), {
            onrendered: function(canvas) {
                canvas.setAttribute('id','thecanvas');
                var img=Canvas2Image.saveAsPNG(canvas, true).getAttribute('src');
                $('#createImg').attr('src',img);
            }
        });
    }
</script>
</body>

</html>

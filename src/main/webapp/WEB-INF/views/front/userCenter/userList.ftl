<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/mend.css">

</head>

<body>
<div class="page-group">
    <div class="page">
        <div class="content shopping-content ub">
            <div class="myheader">
                <img src="" alt="头像" class="mypic">
                <div class="myname">用户昵称</div>
                <a href="/rest/front/user/toUserInfo">
                <img src="${staticpath}/front/dist/images/seting.png" id="settingBtn"></a>
                <#--<img src="${staticpath}/front/dist/images/orCode.png" id="messageBtn">-->
            </div>
            <a href="${basepath}/rest/front/order/toOrderList" class="underline">
                    <li class="myitem myorder">
                        <img src="${staticpath}/front/dist/images/my-order.png" alt="" class="myitem-ico cell-left">

                        <div class="myitem-title">我的订单</div>
                        <div class="pull-right">
                            <div class="myitem-detail">查看全部订单</div>
                            <img src="${staticpath}/front/dist/images/arrowRight.png" alt=""
                                 class="myitem-arrow-small cell-right"></div>
                    </li>
            </a>
            <li class="order-inf flex-box">
                <div class="detail-box cell-main">
                    <a href="${basepath}/rest/front/order/toOrderList?status=init">
                        <img src="${staticpath}/front/dist/images/myico1.png" alt="" class="order-ico">

                        <div class="order-text">待付款</div>
                    </a>
                </div>
                <div class="detail-box cell-main">
                    <a href="${basepath}/rest/front/order/toOrderList?status=pass">
                        <img src="${staticpath}/front/dist/images/myico2.png" alt="" class="order-ico">

                        <div class="order-text">待发货</div>
                    </a>
                </div>
                <div class="detail-box cell-main">
                    <a href="${basepath}/rest/front/order/toOrderList?status=send">
                        <img src="${staticpath}/front/dist/images/evaluated.png" alt="" class="order-ico">

                        <div class="order-text">待收货</div>
                    </a>
                </div>
                <div class="detail-box cell-main">
                    <a href="${basepath}/rest/front/order/toOrderList?status=share">
                        <img src="${staticpath}/front/dist/images/shared.png" alt="" class="order-ico">

                        <div class="order-text">待分享</div>
                    </a>
                </div>
            </li>
            <ul class="underline" id="commentBtn">
                <a href="/rest/front/wallet/toWalletList">
                    <li class="myitem   box-flex">
                        <img src="${staticpath}/front/dist/images/wo-money.png" alt="" class="myitem-ico cell-left">

                        <div class="myitem-title cell-main">我的钱包</div>
                        <img src="${staticpath}/front/dist/images/arrowRight.png" alt=""
                             class="myitem-arrow cell-right">
                    </li>
                </a>
            </ul>
            <#--<ul class="underline" id="warehouseBtn">-->
                <#--<a href="/rest/front/product/toProductList">-->
                    <#--<li class="myitem   box-flex">-->
                        <#--<img src="${staticpath}/front/dist/images/openGroup.png" alt="" class="myitem-ico cell-left">-->

                        <#--<div class="myitem-title cell-main">我要开团</div>-->
                        <#--<img src="${staticpath}/front/dist/images/arrowRight.png" alt=""-->
                             <#--class="myitem-arrow cell-right">-->
                    <#--</li>-->
                <#--</a>-->
            <#--</ul>-->
            <#--<ul class="underline" id="walletBtn">-->
                <#--<a href="/rest/front/comment/toCommentList">-->
                    <#--<li class="myitem   box-flex">-->
                        <#--<img src="${staticpath}/front/dist/images/evaluation.png" alt="" class="myitem-ico cell-left">-->

                        <#--<div class="myitem-title cell-main">我的评价</div>-->
                        <#--<img src="${staticpath}/front/dist/images/arrowRight.png" alt=""-->
                             <#--class="myitem-arrow cell-right">-->

                    <#--</li>-->
                <#--</a>-->
            <#--</ul>-->
            <ul class="underline" id="teamBtn">
                <a href="/rest/front/user/toMyFavorite">
                    <li class="myitem   box-flex">
                        <img src="${staticpath}/front/dist/images/collection.png" alt="" class="myitem-ico cell-left"
                             style="width: 23px; ">

                        <div class="myitem-title cell-main">我的收藏</div>
                        <img src="${staticpath}/front/dist/images/arrowRight.png" alt=""
                             class="myitem-arrow cell-right">
                    </li>
                </a>
            </ul>

            <ul class="underline">
                <a href="/rest/front/user/toMyFoot">
                    <li class="myitem   box-flex">
                        <img src="${staticpath}/front/dist/images/browsingHistory.png" alt=""
                             class="myitem-ico cell-left">

                        <div class="myitem-title cell-main">浏览历史</div>
                        <img src="${staticpath}/front/dist/images/arrowRight.png" alt=""
                             class="myitem-arrow cell-right">
                    </li>
                </a>
            </ul>

            <ul class="underline">
                <a href="/rest/front/address/toAddressList">
                    <li class="myitem   box-flex">
                        <img src="${staticpath}/front/dist/images/harvestAddress.png" alt=""
                             class="myitem-ico cell-left">

                        <div class="myitem-title cell-main">我的地址</div>
                        <img src="${staticpath}/front/dist/images/arrowRight.png" alt=""
                             class="myitem-arrow cell-right">
                    </li>
                </a>
            </ul>
            <#--<ul class="underline" id="openCheckBtn">-->
                <#--<a href="/rest/front/product/toProductCheck">-->
                    <#--<li class="myitem   box-flex">-->
                        <#--<img src="${staticpath}/front/dist/images/check-icon.png" alt=""-->
                             <#--class="myitem-ico cell-left">-->

                        <#--<div class="myitem-title cell-main">开团审核</div>-->
                        <#--<img src="${staticpath}/front/dist/images/arrowRight.png" alt=""-->
                             <#--class="myitem-arrow cell-right">-->
                    <#--</li>-->
                <#--</a>-->
            <#--</ul>-->
        </div>
        <!-- 工具栏 -->
        <nav class="footer-bar bar bar-tab">
            <a class="tab-item external" href="/rest/front/product/toGroupPurchase">
                <img src="${staticpath}/front/dist/images/groupBuy.png" class="ico">
                <p class="tab-label">团购</p>
            </a>
            <#--<a class="tab-item external" href="/rest/front/product/toProductList">-->
                <#--<img src="${staticpath}/front/dist/images/openGroupBtn-1.png" class="ico">-->
                <#--<p class="tab-label">开团</p>-->
            <#--</a>-->
            <a class="tab-item external active" >
                <img src="${staticpath}/front/dist/images/userBtn.png" class="user-ico ico">

                <p class="tab-label">个人</p>
            </a>
        </nav>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script>
    $.config = {
        router: false
    }
</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/userCenter/userList.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>

</html>

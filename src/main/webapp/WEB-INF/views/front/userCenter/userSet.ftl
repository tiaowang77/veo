<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="${staticpath}/base/webUpload/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm-extend.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">

    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_507155_s1pkeywbq08uxr.css">
</head>
<body>
<div class="page-group">
    <div class="page page-current">
        <!--头部部分 !-->
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="ToBack()">
                <span class="icon icon-left"></span>返回
            </button>
            <button class="saveBtn button button-link button-nav pull-right">
                保存
            </button>
            <h1 class="title">设置</h1>
        </header>
        <div class="content account-content">
            <div class="card img-card sex-card">
                <div class="card-content">
                    <div class="list-block">
                        <ul id="filePicker">
                            <li>
                                <div class="item-link item-content">
                                    <div class="item-inner">
                                        <div class="item-title" style="color:#333;">头像</div>
                                        <div class="item-after img-div">
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card sex-card">
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li class="item-link item-content">
                                <a href="/rest/front/user/toRevise#name" class="item-inner ">
                                    <div class="item-title label">昵称</div>
                                    <div class="item-input">
                                        <input type="text" placeholder="" class="name-div">
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card sex-card">
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li>
                                <a href="#" class="item-link item-content" id="sexBtn">
                                    <div class="item-inner">
                                        <div class="item-title cell-main">性别</div>
                                        <input type="text" class="sex mr0" value="男" readonly="readonly">

                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card sex-card" style="border: none;">
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li>
                                <div class="item-link item-content phone-item-link" >
                                    <div class="item-inner">
                                        <div class="item-title">手机号码</div>
                                        <div class="item-after phone-div">未绑定</div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="expand"></div>
    <div class="select">
        <div class="select-sex">
            <h1 class="cell-left">性别</h1>
        </div>
        <li class="select-sex flex middle">
            <h1 class="cell-left cell-main">男</h1>
            <input type="checkbox" class=" manRadio Radiosex cell-right" value="男">
        </li>
        <div class="upline"></div>
        <li class="select-sex flex middle">
            <h1 class="cell-left cell-main">女</h1>
            <input type="checkbox" class=" womenRadio Radiosex cell-right" value="女">
        </li>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script>jQuery.noConflict();</script>
<script>    $.config = {        router: false    }</script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/base/webUpload/webuploader.min.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/userCenter/userSet.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/common.js' charset='utf-8'></script>
<script type="text/javascript">$.init();</script>
<script>
    var uploader=WebUploader.create({
        auto:true,
        swf: '${staticpath}/common/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/front/ued/config?action=uploadimage',
        pick: '#filePicker',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });
    uploader.on('uploadSuccess', function (file, response) {
        uploader.removeFile(file);
        var headImg="/"+response.url;
        var imgHtml='<img src="' + headImg + '">';
        $(".img-div").html(imgHtml);
    })
</script>
</body>
</html>

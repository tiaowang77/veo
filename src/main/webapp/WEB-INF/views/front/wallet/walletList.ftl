<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>味猫商城</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_508245_up1w8xn3gak2zkt9.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/lib/sm.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/module.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/reset.css">
    <style>
        .page {
            background-color: #f5f6f7;
        }
    </style>
</head>

<body>
<div class="page-group">
    <div class="page page-current" id="pas">
        <header class="bar bar-nav">
            <button class="back-button button button-link button-nav pull-left" onclick="back()">
                <span class="icon icon-left"></span>返回
            </button>
            <h1 class="title">我的钱包</h1>
            <span class=" pull-right cell-right xl apply hui">提现</span>
        </header>
        <div class="content ">
            <div class="money-wrapper">
                <img src="${staticpath}/front/dist/images/record.png" alt="" class="money-ico">
                <div class="text xl">账户余额</div>
                <div class="price hui xl">0.00</div>
            </div>
            <div class="record">
                <div class="head xl">交易记录</div>
                <div class="underline"></div>
            </div>
            <div class="lack ">
                <img src="${staticpath}/front/dist/images/lack_goods.png" alt="" class="pic-lack">
                <div class="text-lack">亲，没有查询到商品哦~</div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/lib/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='${staticpath}/front/dist/js/wallet/walletList.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>

</html>

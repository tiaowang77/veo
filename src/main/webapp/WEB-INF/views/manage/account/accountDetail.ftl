<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="通讯录">
<style>
    .row {
        margin-left: -70px;
    }
    .form-horizontal{
        margin-bottom: -20px;
    }
    hr {
        color:#5fb878;
        border:1px solid;
    }
</style>
<div class="panel">
    <div class="returnBtn panel-header">
        <a href="${basepath}/rest/manage/account/toList">返回上一级</a>
    </div>
</div>
<form class="form-horizontal">
    <div class="row">
        <div class="control-group span16">
            <label class="control-label">昵称：</label>
            <div class="controls">
                <input type="text" class="input-normal control-text" name="sales" value="${e.nickname!}" disabled="disabled">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="control-group span16">
            <label class="control-label">上级：</label>

            <div class="controls">
                <input type="text" class="input-normal control-text" name="sales" value="${e.parentname!}" disabled="disabled">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="control-group span16">
            <label class="control-label">下级：</label>
        </div>
    </div>
</form>
<!--suppress ALL -->
<hr/>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title : '序号',dataIndex :'id', elCls: 'center', width:'17%'},
                {title : '昵称',dataIndex :'nickname', elCls: 'center', width:'33%'},
                {title: '头像', dataIndex: 'icon', elCls: 'center', width: '33%', renderer: function (value) {
                    return '<img src="' + value + '" style="width:110px;height:110px"/>';
                }
                }
            ];
    var store = new Store({
                url : '/rest/manage/account/loadDataDetail',
                autoLoad:true, //自动加载数据
                params : { //配置初始请求的参数
                    length : '10',
                    id:'${e.id!}'
                },
                pageSize:10,	// 配置分页数目
                root : 'list',
                totalProperty : 'total'
            }),
            grid = new Grid.Grid({
                render:'#grid',
                columns : columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins : [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar:{
                    pagingBar:true
                }
            });
    grid.render();
    //搜索框表单
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
</script>

</@page.pageBase>

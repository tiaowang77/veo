<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="用户管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
        margin-left: 3px;
    }

    .button-primary {
        margin-left: -10px;
    }

    .button-danger {
        margin-left: -25px;
    }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/account/loadDataAccount">
    <ul class="panel-content">
        <li>
            <div class="control-group span5">
                <div class="search-controls  controls">
                    <input type="text" name="nickname" id="title" value="" placeholder="搜索关键字">
                </div>
            </div>
            <div class="form-actions span2">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
            </div>
            <div class="form-actions span20">
                <a class="button button-success" href="javascript:addPartner()" style="margin-right: 40px;">设为合伙人</a>
                <a class="button button-danger" id="delPartner" style="margin-right: 20px;">取消合伙人</a>
                <a class="button button-primary" href="javascript:addSelfOne()" style="margin-right: 40px;">设为自营用户</a>
                <a class="button button-danger" id="delSelfOne" style="margin-right: 40px;">取消自营用户</a>
                <a class="button button-danger" href="javascript:delFunction()">批量删除</a>
            </div>
        </li>
    </ul>
</form>
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/account/editPercent" method="post">
        <input type="hidden" class="input-normal control-text inputID" name="id" value=""/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label" style="width: 150px;margin-top: 6px;">金额占订单金额比例：</label>

                <div class="controls">
                    <input type="text" id="num1" class="input-normal control-text" name="percent"
                           data-rules="{number:true,maxlength:2}" />%
                </div>
            </div>
        </div>
    </form>
</div>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '10%'},
                {title: '昵称', dataIndex: 'nickname', elCls: 'center', width: '10%'},
                {
                    title: '头像', dataIndex: 'icon', elCls: 'center', width: '20%', renderer: function (value) {
                    return '<img src="' + value + '" style="width:110px;height:110px"/>';
                }
                },
                {
                    title: '性别', dataIndex: 'sex', elCls: 'center', width: '10%', renderer: function (value) {
                    switch (value) {
                        case "2":
                            return '<p>女</p>';
                        case "1":
                            return '<p>男</p>';
                        default:
                            return '<p>未知</p>';
                    }
                }
                },
                {
                    title: '手机号', dataIndex: 'phone', elCls: 'center', width: '10%', renderer: function (value) {
                    if (value == null) {
                        return '<p>未绑定</p>';
                    } else {
                        return '<p>' + value + '</p>';
                    }
                }
                },
                {
                    title: '通讯录', dataIndex: 'id', elCls: 'center', width: '10%', renderer: function (value, data) {
                    var parentname = '';
                    if (data.parentname == null) {
                        parentname = '无';
                    } else {
                        parentname = data.parentname;
                    }
                    return '<a href="${basepath}/rest/manage/account/toDetailList?id=' + value + '&nickname=' + data.nickname
                            + '&parentname=' + parentname + '">查看</a>';
                }
                },
                {
                    title: '是否为合伙人', dataIndex: 'rid', elCls: 'center', width: '10%', renderer: function (value, data) {
                    if (data.rid == '3') {
                        return '<p>是</p>';
                    } else {
                        return '<p>否</p>';
                    }

                }
                },
                {
                    title: '返利分配比例', dataIndex: 'percent', elCls: 'center', width: '10%', renderer: function (value, data) {
                    if (data.rid == '3') {
                        return '<a href="javascript:setPercent(' + value*100 + ','+data.id+')"><span>'+value*100+'</span>%</a>';
                    } else {
                        return '<p>—— ——</p>';
                    }

                }
                },
                {
                    title: '是否为自营用户',
                    dataIndex: 'rid',
                    elCls: 'center',
                    width: '12%',
                    renderer: function (value, data) {
                        if (data.rid == '2') {
                            return '<p>是</p>';
                        } else {
                            return '<p>否</p>';
                        }

                    }
                },
                {
                    title: '操作', dataIndex: 'id', elCls: 'center', width: '8%', renderer: function (value) {
                    return '<a href="javascript:deleteOne(' + value + ')">删除</a>';
                }
                }
            ];

    var store = new Store({
                url: '/rest/manage/account/loadDataAccount',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10'
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    //搜索框表单
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //设为合伙人
    function addPartner() {
        BUI.Message.Confirm('确认设置为合伙人？', function () {
            var selections = grid.getSelection();
            var ids = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].id.toString()
            }
            $.ajax({
                type: "POST",
                url: "/rest/manage/account/setPartner",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    //取消合伙人
    $("#delPartner,#delSelfOne").click(function () {
        BUI.Message.Confirm('确认取消合伙人？', function () {
            var selections = grid.getSelection();
            var ids = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].id.toString()
            }
            $.ajax({
                type: "POST",
                url: "/rest/manage/account/setCommon",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    });
    //设为自营用户
    function addSelfOne() {
        BUI.Message.Confirm('确认设置为自营用户？', function () {
            var selections = grid.getSelection();
            var ids = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].id.toString()
            }
            $.ajax({
                type: "POST",
                url: "/rest/manage/account/setManager",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    //删除选中的记录
    function delFunction() {
        BUI.Message.Confirm('确认要删除吗？', function () {
            var selections = grid.getSelection();
            var ids = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].id.toString()
            }
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/account/deletesAccount",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    //删除单个用户
    function deleteOne(value) {
        BUI.Message.Confirm('确认要删除吗？', function () {
            $.ajax({
                type: 'POST',
                url: '${basepath}/rest/manage/account/deleteOne',
                dataType: 'json',
                data: {
                    id: value
                },
                success: function () {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    /*------------------设置弹出框----------------------------*/
    var editForm = new BUI.Form.HForm({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            if(data.success==false){
                BUI.Message.Alert("返利占比需小于100%！");
            }else {
                var obj = new Object();
                obj.start = 0; //返回第一页
                store.load(obj);
                editDialog.close();
            }
        }
    });
    editForm.render();
    var editDialog = new BUI.Overlay.Dialog({
        title: '返利设置',
        width: 400,
        height: 150,
        contentId: 'editContent',
        success: function () {
            if($("#num1").val().length>=3){
                BUI.Message.Alert('返利占比需小于100%!','error');
            }else {
                editForm.submit();
            }
        }
    });
    //编辑按钮事件
    function setPercent(value,id){
        //让返利比例在设置弹出框显示
        $("#num1").val(value);
        $(".inputID").val(id);
        editForm.clearErrors();
        editDialog.show();
    }
</script>

</@page.pageBase>

<#import "../tpl/pageTep.ftl" as page>
    <@page.pageBase currentMenu="返利明细">
    <style>
        .form-horizontal .controls {
            line-height: 40px;
            height: 40px;
            margin-left: 3px;
        }
        .button-primary {
            margin-left: -10px;
        }
        .button-danger{
            margin-left: -25px;
        }
    </style>
    <!--suppress ALL -->
    <form id="searchForm" class="form-panel">
        <ul class="panel-content">
            <li>
                <div class="control-group span5">
                    <div class="search-controls  controls">
                        <input type="text" name="orderPerson" id="title" value="" placeholder="搜索下单人、订单号">
                    </div>
                </div>
                <div class="form-actions span2">
                    <button type="submit" class="button  button-primary">
                        搜索
                    </button>
                </div>
            </li>
        </ul>
    </form>
    <div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title : '序号',dataIndex :'id', elCls: 'center', width:'15%'},
                {title : '下单人',dataIndex :'nickname', elCls: 'center',width:'15%'},
                {title : '下单日期',dataIndex :'createTime', elCls: 'center',width:'20%'},
                {title : '获利方',dataIndex :'gainer', elCls: 'center',width:'15%'},
                {title : '返利金额',dataIndex :'rebate', elCls: 'center',width:'15%'}
            ];

    var store = new Store({
                url : '/rest/manage/order/selectBrokerageOrder',
                autoLoad:true, //自动加载数据
                params : { //配置初始请求的参数
                    length : '10'
                },
                pageSize:10,	// 配置分页数目
                root : 'data',
                totalProperty : 'total'
            }),
            grid = new Grid.Grid({
                render:'#grid',
                columns : columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins : [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar:{
                    pagingBar:true
                }
            });
    grid.render();
    //搜索框表单
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;//不用form表单提交
    });
</script>

</@page.pageBase>

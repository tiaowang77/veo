<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="分类管理">
<style>
    .button-success, .button-danger {
        margin-left: 5px;
    }

    .row p {
        text-align: center;
        font-size: 12px;
        color: #999;
        -webkit-transform-origin-x: 0;
        -webkit-transform: scale(0.85);
    }

    .form-horizontal .control-group-img {
        height: 40px;
    }

    .webuploader-pick {
        background: none;
        padding: 0;
    }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
            <div class="form-actions">
                <a href="javascript:add()" class="button button-success">
                     新增
                </a>
                <a class="button button-danger" onclick="return delFunction();">
                     批量删除
                </a>
            </div>
        </li>
    </ul>
</form>
<#----------------------------------------------新增表单 start---------------------------------------------------------->
<div id="addContent" style="display:none;">
    <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/catalog/insertJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">分类名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="name" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">分类图标：</label>

                <div class="control-group-img  controls ">
                    <input type="text" class="input-normal control-text" name="icon" data-rules="{required : true}"
                           readonly>
                    <a id="addImgBtn" class="button button-min button-primary pull-right"
                       style="height:20px;margin-left: 10px;">上传</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">排序：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="order1"
                           data-rules="{required : true,number:true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">是否显示：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="showInNav">
                        <option value="">-请选择-</option>
                        <option value="y">显示</option>
                        <option value="n">不显示</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<#----------------------------------------------新增表单 end---------------------------------------------------------->
<#----------------------------------------------编辑表单start--------------------------------------------------------->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/catalog/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">分类名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="name" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">分类图标：</label>

                <div class="control-group-img controls">
                    <input type="text" class="input-normal control-text" name="icon" data-rules="{required : true}"
                           readonly>
                    <a id="editImgBtn" class="button button-min button-primary pull-right"
                       style="height:20px;margin-left: 10px;">上传</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">排序：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="order1" id="order"
                           data-rules="{required : true,number:true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">是否显示：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="showInNav">
                        <option value="">-请选择-</option>
                        <option value="y">显示</option>
                        <option value="n">不显示</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="grid"></div>
<script>
    <#----------------------------------------------列表处理---------------------------------------------------------->
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '0%'},
                {title: '分类名称', dataIndex: 'name', elCls: 'center', width: '25%'},
                {
                    title: '分类图标', dataIndex: 'icon', elCls: 'center', width: '30%', renderer: function (value) {
                    return '<img src="${basepath}/' + value + '" width="100px" height="100px"/>'
                }
                },
                {title: '排序', dataIndex: 'order1', elCls: 'center', width: '10%'},
                {
                    title: '是否在导航栏中显示', dataIndex: 'showInNav', width: '15%',elCls: 'center', renderer: function (data) {
                        if (data == "y") {
                            return '<img src="${basepath}/resource/manage/images/action_check.gif">';
                        } else {
                            return '<img src="${basepath}/resource/manage/images/action_delete.gif">';
                        }
                    }
                },
                {
                    title: '操作', dataIndex: 'id', width: '20%', elCls: 'center', renderer: function (value, data) {

                        return '<a href="javascript:edit(' + value + ')">编辑</a>' +
                                '<a style="margin-left:10px;" href="javascript:delOneFunction(' + value + ')">删除</a>';
                    }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    /*------------------------------------------------新增表单处理start------------------------------------------------*/
    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addDialog.close();
        }
    }).render();

    var addDialog = new BUI.Overlay.Dialog({
        title: '新增图片',
        width: 500,
        height: 320,
        contentId: 'addContent',
        success: function () {
            if (addForm.isValid()) {
                addForm.submit();
            } else {
                addForm.valid();
            }
        }
    });
    //添加上传图片按钮事件
    function add() {
        addForm.clearFields();          //添加的时候要清楚原来的数据
        addDialog.show();
        var addUploader = WebUploader.create({
            auto: true,
            swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
            server: '${basepath}/rest/manage/ued/config?action=uploadimage',
            pick: '#addImgBtn',
            resize: false,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
            }
        });
        addUploader.on('uploadSuccess', function (file, response) {
            addUploader.removeFile(file);
            addForm.setFieldValue("icon", response.url)
        });
    }
    /*------------------------------------------------新增表单处理 end-------------------------------------------------*/
    /*------------------------------------------------编辑表单处理 start-----------------------------------------------*/
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();

    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑图片',
        width: 500,
        height: 320,
        contentId: 'editContent',
        success: function () {
            if (editForm.isValid()) {
                editForm.submit();
            } else {
                editForm.valid();
            }
        }
    });
    var nowOrder='';            //原来的排序值
    //编辑按钮事件
    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/catalog/toEditJson",
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                nowOrder = data.data.order1;           //获取原来的值
                editDialog.show();
                var editUploader = WebUploader.create({
                    auto: true,
                    swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
                    server: '${basepath}/rest/manage/ued/config?action=uploadimage',
                    pick: '#editImgBtn',
                    resize: false,
                    accept: {
                        title: 'Images',
                        extensions: 'gif,jpg,jpeg,bmp,png',
                        mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
                    }
                });

                editUploader.on('uploadSuccess', function (file, response) {
                    editUploader.removeFile(file);
                    editForm.setFieldValue("icon", response.url)
                });
            }
        });
    }
    /*------------------------------------------------编辑表单处理 end------------------------------------------------*/
    <#----------------------------------------------删除操作 start---------------------------------------------------------->
    //删除单个记录
    function delOneFunction(value) {
        BUI.Message.Confirm('确认要删除吗？', function () {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/catalog/deleteByIdJson",
                dataType: "json",
                data: {
                    id: value
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }

    //删除选中的记录--批量删除
    function delFunction() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        BUI.Message.Confirm('确认要删除吗？', function () {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/catalog/deletesJson",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    <#----------------------------------------------删除操作 end---------------------------------------------------------->
    <#----------------------------------------------排序操作 start---------------------------------------------------------->
    //以下是异步表单验证--新增
    var orderField = addForm.getField('order1');

    orderField.set('remote', {
        url: '${basepath}/rest/manage/catalog/toOrder',
        dataType: 'json',//默认为字符串
        callback: function (data) {
            if (data.data == null) {
                return '';
            } else {
                return "序号已存在";
            }
        }
    });

    //以下是异步表单验证--编辑接口
    var order2Field = editForm.getField('order1');

    order2Field.set('remote', {
        url: '${basepath}/rest/manage/catalog/toOrder',
        dataType: 'json',//默认为字符串
        callback: function (data) {
            var oldOrder = $("#order").val();
            if (data.data == null || oldOrder == nowOrder) {
                return '';
            } else {
                return "序号已存在";
            }
        }
    });
    <#----------------------------------------------排序操作 end---------------------------------------------------------->
</script>

</@page.pageBase>

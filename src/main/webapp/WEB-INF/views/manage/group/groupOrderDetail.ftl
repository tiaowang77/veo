<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="订单详情">
<style>
    .control-group p {
        margin: 0 0 0 20px;
        float: left;
        font-size: 10px;
        text-align: left;
        line-height: 30px;
    }

    .order-details {
        width: 100%;
    }

    .order {
        width: 60%;
        float: left;
    }

    .panel-header {
        color: #333333;
        background-color: #f5f5f5;
        border-color: #dddddd;
    }

    .button-large {
        padding: 10px 30px;
        font-size: 16px;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="form-horizontal">
                <div class="panel-header">
                    <a href="${basepath}/rest/manage/order/toList">返回上一级</a>
                </div>
                <div class="panel-body">

                    <h3>订单详情</h3>
                    <hr>
                    <div class="order-details">
                        <div class="order">
                            <div class="control-group">
                                <label class="control-label">
                                    订单号：
                                </label>

                                <p>${e.orderID!}</p>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    订单时间：
                                </label>

                                <p>${e.createTime!}</p>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    买家帐号：
                                </label>

                                <p>${e.nickname!}</p>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    订单状态：
                                </label>

                                <p>${e.status!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <h3>商品信息</h3>
                    <hr>
                    <div id="grid"></div>
                </div>
                <div class="panel-body">
                    <h3>物流信息</h3>
                    <hr>
                    <form id="detailForm" action="${basepath}/rest/manage/order/sendProduct" class="form-horizontal"
                          method="post">
                        <#--<input type="hidden" class="control-text span-width span8 hidden" name="addressID"
                               value="${e.addressID!}"/>-->
                        <input type="hidden" class="control-text span-width span8 hidden" name="id" value="${e.id!}"/>
                        <input type="hidden" class="control-text span-width span8 hidden" name="orderID"
                               value="${e.orderID!}"/>

                        <div class="control-group">
                            <label class="control-label">
                                <s>*</s>
                                收货人：
                            </label>

                            <div class="controls">
                                <#if e.name??>
                                    <input type="text" class="control-text span-width span8" value="${e.name!}"
                                           disabled/>
                                <#else>
                                    <input type="text" class="control-text span-width span8" value=""
                                           disabled/>
                                </#if>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <s>*</s>
                                收货地址：
                            </label>

                            <div class="controls">
                                <#if e.address??>
                                    <input type="text" class="control-text span-width span8" value="${e.province!}${e.address!}"
                                           disabled>
                                <#else>
                                    <input type="text" class="control-text span-width span8" value=""
                                           disabled>
                                </#if>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <s>*</s>
                                联系电话：
                            </label>

                            <div class="controls">
                                <input type="text" class="control-text span-width span8" value="${e.phone!}"
                                       disabled>
                            </div>
                        </div>
                        <#if e.status=="待发货">
                            <input type="hidden" name="status" value="${e.status!}"/>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    快递公司：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="expressCompany"
                                           value="" data-rules="{required : true}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    快递单号：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="expressNum"
                                           value="" data-rules="{required : true,number : true}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    发货人：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="consigner"
                                           value="" data-rules="{required : true}">
                                </div>
                            </div>
                            <div class="centered">
                                <button type="submit" class="button  button-large  button-success" id="sendProduct">发货</button>
                            </div>
                        <#elseif e.status=="已发货">
                            <input type="hidden" name="status" value="${e.status!}"/>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    快递公司：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="expressCompany"
                                           value="${e.expressCompany!}" disabled>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    快递单号：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="expressNum"
                                           value="${e.expressNum!}" disabled>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    发货人：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="consigner"
                                           value="${e.consigner!}" disabled>
                                </div>
                            </div>
                            <div class="centered">
                                <button type="submit" class="button  button-large  button-success">确认收货</button>
                            </div>
                        <#elseif e.status=="已完成">
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    快递公司：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="expressCompany"
                                           value="${e.expressCompany!}" disabled>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    快递单号：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="expressNum"
                                           value="${e.expressNum!}" disabled>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <s>*</s>
                                    发货人：
                                </label>

                                <div class="controls">
                                    <input type="text" class="control-text span-width span8" name="consigner"
                                           value="${e.consigner!}" disabled>
                                </div>
                            </div>
                        <#else>

                        </#if>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title : '商品名称',dataIndex :'productName', elCls: 'center', width: '20%'},
                {title : '商品图片',dataIndex :'picture', elCls: 'center', width: '20%',renderer:function(value){
                    return '<img src="/' + value + '" style="width:110px;height:110px"/>';
                }},
                {title : '商品规格',dataIndex :'specInfo', elCls: 'center', width: '20%'},
                {title : '下单数量',dataIndex :'number', elCls: 'center', width: '20%'},
                {title : '商品单价',dataIndex :'price', elCls: 'center', width: '20%'}
            ];

    var store = new Store({
                url : '/rest/manage/order/loadProduct',
                autoLoad:true, //自动加载数据
                params : { //配置初始请求的参数
                    length : '10',
                    orderID: '${e.orderID!}'
                },
                pageSize:10,	// 配置分页数目
                root : 'list',
                totalProperty : 'total'
            }),
            grid = new Grid.Grid({
                render:'#grid',
                columns : columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins : [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar:{
                    pagingBar:true
                }
            });
    grid.render();

    var form = new BUI.Form.Form({
        srcNode: '#detailForm'
    }).render();
    form.on('beforesubmit', function (ev) {
        if (!form.isValid()) {
            alert(1);
        }
    });
</script>
</@page.pageBase>

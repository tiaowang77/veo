<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="开团审核">
<style>
    hr {
        color: #5fb878;
        border: 1px solid;
    }

    textarea {
        width: 200px;
        height: 100px !important;
    }

    .checkBtn {
        margin-top: 20px;
    }
</style>
<div class="xm-product">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="${basepath}/rest/manage/product/toCheckList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="addForm" class="form-horizontal" action="">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品名称：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="name" disabled="disabled"
                                   value="${e.name!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            开团日期：
                        </label>

                        <div class="controls">

                            <input type="text" class="control-text span-width span10" name="startDate"
                                   disabled="disabled" value="${e.startDate!}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在小区：
                        </label>

                        <div class="controls">

                            <input type="text" class="control-text span-width span10" name="garden"
                                   disabled="disabled" value="${e.garden!}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            自提点：
                        </label>

                        <div class="controls">

                            <input type="text" class="control-text span-width span10" name="address"
                                   disabled="disabled" value="${e.address!}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            名字：
                        </label>

                        <div class="controls">

                            <input type="text" class="control-text span-width span10" name="linkMan"
                                   disabled="disabled" value="${e.linkMan!}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            电话：
                        </label>

                        <div class="controls">

                            <input type="text" class="control-text span-width span10" name="linkPhone"
                                   disabled="disabled" value="${e.linkPhone!}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label organization-img">
                            <s>*</s>
                            商品图片：
                        </label>
                    </div>
                    <h3 class="offset2">商品规格
                    </h3>
                    <hr>
                    <div id="grid"></div>
                    <h3 class="offset2">历史订单
                    </h3>
                    <hr>
                    <div id="orderGrid"></div>
                    <div class="checkBtn">
                        <a class="button button-success passBtn" href="javascript:pass()" style="margin-left: 30%">
                            通过审核
                        </a>
                        <a class="button button-success noPassBtn" href="javascript:refuse()" style="margin-left: 5%">
                            拒绝
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<#-------------------------------------------------------编辑审核状态 begin-------------------------------------------------------->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/product/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id" value="${e.id!}"/>
        <input type="hidden" class="input-normal control-text" name="checkStatus" value="3"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">拒绝理由：</label>

                <div class="controls control-row4">
                    <textarea name="reason"></textarea>
                </div>
            </div>
        </div>
    </form>
<#-------------------------------------------------------审核状态 end-------------------------------------------------------->
    <script>
        function getQueryString(name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return unescape(r[2]);
            }
            return null;
        }
        var id = getQueryString('id'),
                checkStatus=getQueryString('checkStatus');
        /*--------------------------产品规格列表  begin--------------------*/
        if(checkStatus==2){
            $(".passBtn").hide();
        }else if(checkStatus==3){
            $(".noPassBtn").hide();
        }else if(checkStatus==4||checkStatus==5){
            $(".noPassBtn").hide();
            $(".passBtn").hide();
        }
        var Grid = BUI.Grid,
                Store = BUI.Data.Store,
                columns = [
                    {title: '商品规格组合', dataIndex: 'specCombination', elCls: 'center', width: '20%'},
                    {title: '商品团购价', dataIndex: 'specPurchasePrice', elCls: 'center', width: '20%'},
                    {title: '商品现价', dataIndex: 'specNowPrice', elCls: 'center', width: '20%'},
                    {title: '商品团购价', dataIndex: 'specGroupPrice', elCls: 'center', width: '20%'},
                    {title: '商品库存', dataIndex: 'specStock', elCls: 'center', width: '20%'}
                ];

        var store = new Store({
                    url: '/rest/manage/spec/selectSpecList',
                    autoLoad: true, //自动加载数据
                    params: { //配置初始请求的参数
                        length: '10',
                        productID: '${e.id!}'
                    },
                    pageSize: 10,	// 配置分页数目
                    root: 'data',
                    totalProperty: 'total'
                }),
                grid = new Grid.Grid({
                    render: '#grid',
                    columns: columns,
                    loadMask: true, //加载数据时显示屏蔽层
                    store: store,
                    plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                    // 底部工具栏
                    bbar: {
                        pagingBar: true
                    }
                });
        grid.render();

        /*--------------------------产品规格列表  end---------------------------*/
        //获取商品的id/*--------------------------；历史订单列表  begin--------------------*/
        var orderColumns = [
            {title: '订单号', dataIndex: 'id', elCls: 'center', width: '10%'},
            {title: '买家账号', dataIndex: 'nickname', elCls: 'center', width: '30%'},
            {title: '开团时间', dataIndex: 'productStartTime', elCls: 'center', width: '20%'},
            {title: '下单时间', dataIndex: 'createTime', elCls: 'center', width: '20%'},
            {
                title: '订单状态', dataIndex: 'status', elCls: 'center', width: '10%', renderer: function (value) {
                if (value == 'init') {
                    return '<p>待付款</p>';
                } else if (value == 'pass') {
                    return '<p>待发货</p>';
                } else if (value == 'send') {
                    return '<p>待收货</p>';
                } else if (value == 'finish') {
                    return '<p>已完成</p>';
                } else {
                    return '<p>已关闭</p>';
                }
            }
            },
            {
                title: '操作',
                dataIndex: 'orderDetailID',
                elCls: 'center',
                width: '10%',
                renderer: function () {
                    var option1 = '<p>查看</p>';
                    return option1;
                }
            }
        ];

        var orderStore = new Store({
                    url: '/rest/manage/order/selectHistoryOrder',
                    autoLoad: true, //自动加载数据
                    params: { //配置初始请求的参数
                        length: '10',
                        id: '${e.id!}',
                        startDate:'${e.startDate!}'
                    },
                    pageSize: 10,	// 配置分页数目
                    root: 'data',
                    totalProperty: 'total'
                }),
                orderGrid = new Grid.Grid({
                    render: '#orderGrid',
                    columns: orderColumns,
                    loadMask: true, //加载数据时显示屏蔽层
                    store: orderStore,
                    plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                    // 底部工具栏
                    bbar: {
                        pagingBar: true
                    }
                });
        orderGrid.render();

        /*--------------------------产品规格列表  end---------------------------*/
        //获取商品的id
        var editForm = new BUI.Form.Form({
            srcNode: '#editForm',
            submitType: 'ajax',
            callback: function (data) {
                if (data.success == true) {
                    window.location.href = "${basepath}/rest/manage/product/toCheckList"
                } else {

                }
            }
        }).render();
        var editDialog = new BUI.Overlay.Dialog({
            title: '编辑审核状态',
            width: 500,
            height: 220,
            contentId: 'editContent',
            success: function () {
                editForm.ajaxSubmit();
            }
        });
        //编辑按钮事件
        function refuse() {
            editDialog.show();
        };
        <#---------------------------------------------通过操作------------------------------------>
        /**
         * 审批通过
         */
        function pass() {
            BUI.Message.Confirm('是否通过审核？', function () {
                $.ajax({
                    type: "POST",
                    url: "/rest/manage/product/updateJson",
                    dataType: "json",
                    data: {
                        id: id,
                        checkStatus: 2
                    },
                    success: function (data) {
                        window.location.href = "/rest/manage/product/toCheckList";
                    }
                });
            }, 'question');
        }
        <#---------------------------------------------通过操作------------------------------------>
        /*--------------------------多张机构图片分割  begin--------------------*/
        var imgArr = "${e.images!}".split(",");
        var imgHtml = "";
        for (var i = 0; i < imgArr.length; i++) {
            imgHtml += '<img src="${basepath}/' + imgArr[i] + '"/ style="height: 100px;width: 100px;margin:0 0 10px 10px" />';//创建img标签，图片地址赋值src
        }
        $(".organization-img").after(imgHtml);
        /*--------------------------多张机构图片分割  end--------------------*/
    </script>

</@page.pageBase>

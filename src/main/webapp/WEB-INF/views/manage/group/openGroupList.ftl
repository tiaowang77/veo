<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="开团审核">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger, .shelves-button {
        margin-left: 5px;
    }

    p {
        margin: 0 !important;
    }
    .bui-grid-cell-text{
        min-height: 0px;
        min-width: 0px;
        height: 20px;
        white-space: nowrap; overflow: hidden; text-overflow: ellipsis
    }
</style>
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/product/selectAllList">
    <ul class="panel-content ">
        <li>
            <div class="control-group span5">
                <div class="controls">
                    <input type="text" id="name" name="name" placeholder="输入关键字搜索"/>
                </div>
            </div>
            <div class="form-actions span16">
                <button type="submit" class="button  button-primary">
                    查询
                </button>
                <a class="button button-danger" href="javascript:pass()">
                    批量通过
                </a>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<#---------------------------------------------列表操作------------------------------------>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '商品编号', dataIndex: 'id', elCls: 'center'},
                {title: '商品名称', dataIndex: 'name', elCls: 'center',width: 300 },
                {title: '开团日期', dataIndex: 'startDate', elCls: 'center',width: 150},
                {title: '所在小区', dataIndex: 'garden', elCls: 'center',width: 200 },
                {title: '自提点', dataIndex: 'address', elCls: 'center'},
                {title: '名字', dataIndex: 'linkMan', elCls: 'center'},
                {title: '电话', dataIndex: 'linkPhone', elCls: 'center'},
                {
                    title: '状态', dataIndex: 'id', elCls: "center", renderer: function (value, data) {
                    switch (data.checkStatus) {
                        case '1':
                            return '<a href="${basepath}/rest/manage/product/toCheckDetail?id=' + value + '">未审核</a>';
                            break;
                        case '2':
                            return '<a href="${basepath}/rest/manage/product/toCheckDetail?id=' + value + '&checkStatus=' + data.checkStatus + '">已通过</a>';
                            break;
                        case '3':
                            return '<a href="${basepath}/rest/manage/product/toCheckDetail?id=' + value + '&checkStatus=' + data.checkStatus + '">不通过</a>';
                            break;
                        case '4':
                            return '<a href="${basepath}/rest/manage/product/toCheckDetail?id=' + value + '&checkStatus=' + data.checkStatus + '">开团中</a>';
                            break;
                        case '5':
                            return '<a href="${basepath}/rest/manage/product/toCheckDetail?id=' + value + '&checkStatus=' + data.checkStatus + '">已结束</a>';
                            break;
                    }
                }
                }
            ];

    var store = new Store({
                url: '/rest/manage/product/selectAllList',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'data',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                forceFit: true,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    <#---------------------------------------------列表操作------------------------------------>
    <#---------------------------------------------模糊查询------------------------------------>
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    <#---------------------------------------------模糊查询------------------------------------>
    <#---------------------------------------------批量通过操作------------------------------------>
    /**
     * 批量通过选中记录
     */
    function pass() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString();
        }
        BUI.Message.Confirm('确认要批量通过吗？', function () {
            $.ajax({
                type: "POST",
                url: "/rest/manage/product/updateProducts",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = form.serializeToObject();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    <#---------------------------------------------批量通过操作------------------------------------>
</script>
</@page.pageBase>

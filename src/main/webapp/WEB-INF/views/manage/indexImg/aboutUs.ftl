<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="关于我们">
<style>
    .button-large {
        font-size: 16px;
    }

</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-body">
                <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/systemSetting/updateAboutJson"
                      method="post">
                    <input type="hidden" class="control-text span-width span10" name="id" value="1">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            发现内容：
                        </label>

                        <div class="controls control-row12" style="height: 350px;initialFrameWidth: 100%;">
                            <textarea type="text" data-rules="{required:true}" class="input-large" name="introduce"
                                      id="content"></textarea>
                        </div>
                    </div>
                    <div class="centered">
                        <button type="submit" class="button  button-large  button-success">发布</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var editor;
    setTimeout(function () {
        editor = UE.getEditor('content');
        var editForm = new BUI.Form.Form({
            srcNode: '#editForm',
            submitType: 'ajax',
            callback: function (data) {
                if (data.success == true) {
                    BUI.Message.Alert('发布成功', function () {
                        window.location.href = "#";
                    }, 'info');
                } else {

                }
            }
        }).render();
    }, 1000);

    $.ajax({
        type: "GET",
        url: "${basepath}/rest/manage/systemSetting/selectContent",
        dataType: "json",
        success: function (data) {
            $(".input-large").val(data.data.introduce);
        }
    });

</script>

</@page.pageBase>
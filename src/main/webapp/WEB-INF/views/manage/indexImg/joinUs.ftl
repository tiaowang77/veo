<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="加入我们">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .span16 {
        width: 880px;
    }

    .form-horizontal .time-label {
        width: 60px;
        line-height: 40px;
        margin-left: 20px;
    }

    .button-primary {
        margin-left: 5px;
    }

    .form-panel .time-input {
        width: 120px;
    }

    .form-panel select {
        margin-left: 10px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/members/loadData">
    <ul class="panel-content">
        <li>
            <div>
                <div class="control-group span5">
                    <div class="search-controls  controls">
                        <input type="text" name="name" id="name" value="" placeholder="关键字搜索">
                    </div>
                </div>
                <div class="form-actions span2">
                    <button type="submit" class="button  button-primary">搜索</button>
                </div>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<script>
    /*--------------------------------------------------------表格数据渲染 start-----------------------------------*/
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '10%'},
                {title: '上传者微信昵称', dataIndex: 'nickname', elCls: 'center', width: '18%'},
                {title: '姓名', dataIndex: 'name', elCls: 'center', width: '18%'},
                {title: '电话', dataIndex: 'phone', elCls: 'center', width: '18%'},
                {title: 'QQ', dataIndex: 'qq', elCls: 'center', width: '18%'},
                {title: '微信号', dataIndex: 'weChat', elCls: 'center', width: '18%'}
            ];
    var store = new Store({
                url: '${basepath}/rest/manage/members/loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: '1'
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格，//就是开头的多选框
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    //搜索
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        ///序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

</script>

</@page.pageBase>

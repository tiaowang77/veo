<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="订单管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary,.button-danger {
        margin-left: 5px;
    }

    .form-horizontal .time-label {
        width: 60px;
        line-height: 40px;
        margin-left: 20px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
    .form-horizontal .money-label {
        width: 60px;
        line-height: 40px;
        margin-left: 5px;
    }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/order/loadData">
    <ul class="panel-content">
        <li>
            <div class="control-group span5">
                <div class="search-controls  controls">
                    <input type="text" name="id" id="title" value="" placeholder="查询订单号、买家帐号">
                </div>
            </div>
            <div>
                <label class="time-label control-label">订单状态：</label>

                <div class="time-controls controls">
                    <select name="status" class="input-normal" id="status">
                        <option value="">全部</option>
                        <option value="init">待付款</option>
                        <option value="pass">待发货</option>
                        <option value="send">待收货</option>
                        <option value="finish">已完成</option>
                        <option value="cancel">已关闭</option>
                    </select>
                </div>
                <label class="time-label control-label">起始日期：</label>

                <div class="time-controls controls">
                    <input name="startDate" class="input-small calendar" type="text" style="width: 80px;"><label>
                    &nbsp;-&nbsp;</label><input
                        name="endDate" class="input-small calendar" type="text" style="width: 80px;">
                </div>

                <button type="submit" class="button  button-primary">
                    查询
                </button>
                <a class="button button-danger" href="javascript:delFunction()">
                    批量删除
                </a>
            </div>
        </li>

    </ul>
</form>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '订单号', dataIndex: 'orderID', elCls: 'center', width: '20%'},
                {title: '买家账号', dataIndex: 'nickname', elCls: 'center', width: '20%'},
                {title: '下单时间', dataIndex: 'createTime', elCls: 'center', width: '20%'},
                {title: '订单状态', dataIndex: 'status', elCls: 'center', width: '20%', renderer: function (value) {
                    if (value == 'init') {
                        return '<p>待付款</p>';
                    } else if (value == 'pass') {
                        return '<p>待发货</p>';
                    } else if (value == 'send') {
                        return '<p>待收货</p>';
                    } else if (value == 'finish') {
                        return '<p>已完成</p>';
                    } else {
                        return '<p>已关闭</p>';
                    }
                }
                },
                {title: '所属合伙人', dataIndex: 'gainer', elCls: 'center', width: '20%'},
                {title: '操作', dataIndex: 'orderDetailID', elCls: 'center', width: '20%', renderer: function (value, data) {
                    var option1 = '<a href="${basepath}/rest/manage/order/toEditOrder?id=' + value + '">查看</a>';
                    var option2 = '<a href="javascript:deleteOne(' + value + ',' + data.orderID + ')">删除</a>';
                    return option1 + "&nbsp;&nbsp;&nbsp;&nbsp;" + option2;
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10'
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    //查询框
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    //删除选中的记录
    function delFunction() {
        BUI.Message.Confirm('确认要删除吗？', function () {
            var selections = grid.getSelection();
            var ids = new Array();
            var orderIDs = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].orderDetailID.toString();
                orderIDs[i] = selections[i].orderID.toString();
            }
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/order/deletesOrderDetail",
                dataType: "json",
                data: {
                    ids: ids,
                    orderIDs: orderIDs
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    //删除单个订单记录
    function deleteOne(value, orderID) {
        BUI.Message.Confirm('确认要删除吗？', function () {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/order/deleteOne",
                dataType: "json",
                data: {
                    id: value,
                    orderID: orderID
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
</script>

</@page.pageBase>

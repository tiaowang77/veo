<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="添加商品">
<style>
    .button-large {
        font-size: 16px;
    }

    .webuploader-pick {
        background: none;
        padding: 0;
    }

    input.calendar {
        width: 160px;
    }
</style>
<div class="xm-product">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="${basepath}/rest/manage/product/toList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/product/insertAllJson"
                      method="post">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品名称：
                        </label>

                        <div class="controls">

                            <input type="text" class="control-text span-width span8" name="name" placeholder="请输入商品名称"
                                   data-rules="{required : true}">
                        </div>
                    </div>
                <#--<div class="control-group">-->
                <#--<label class="control-label">-->
                <#--<s>*</s>-->
                <#--开团时间：-->
                <#--</label>-->

                <#--<div class="controls span8">-->
                <#--<input type="text" class="calendar  calendar-time" name="startDate"-->
                <#--placeholder="请输入商品开团时间"-->
                <#--data-rules="{required : true}" id="startTime">-->
                <#--</div>-->
                <#--</div>-->
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在地址：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="area" placeholder="请输入所在地址"
                                   data-rules="{required : true}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在小区：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="garden" placeholder="请输入所在小区"
                                   data-rules="{required : true}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            自提区：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="address" placeholder="请输入自提区"
                                   data-rules="{required : true}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            联系人：
                        </label>
                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="linkMan" placeholder="请输入联系人"
                                   data-rules="{required : true}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            联系电话：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="linkPhone"
                                   placeholder="请输入联系电话"
                                   data-rules="{required:true,number:true}" id="linkPhone" onkeyup="value=value.replace(/[^\d.]/g,'')">
                        </div>
                    </div>
                <#--<div class="control-group">-->
                <#--<label class="control-label">-->
                <#--<s>*</s>-->
                <#--开团循环：-->
                <#--</label>-->

                <#--<div class="controls">-->
                <#--<select data-rules="{required:true}" name="isSelf">-->
                <#--<option value="">--请选择--</option>-->
                <#--<option value="y">是</option>-->
                <#--<option value="n">否</option>-->
                <#--</select>-->
                <#--</div>-->
                <#--</div>-->
                    <div class="control-group">
                        <label class="control-label">  <s>*</s>第一单提成：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="firstPercent"
                                   data-rules="{required : true,number:true}" value="5"/>%
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">  <s>*</s>第二单提成：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="secondPercent"
                                   data-rules="{required : true,number:true}" value="10"/>%
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">  <s>*</s>第三单提成：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="thirdPercent"
                                   data-rules="{required : true,number:true}" value="20"/>%
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品海报：
                        </label>
                        <div class="controls">
                            <input type="text" class="control-text span8" name="background"
                                   data-rules="{required : true}" readonly>
                            <a id="backgroundBtn" class="button button-min button-primary pull-right"
                               style="height:20px;margin-left: 10px;">上传</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品主图：
                        </label>
                        <div class="controls">
                            <input type="text" class="control-text span8" name="picture"
                                   data-rules="{required : true}" readonly>
                            <a id="pictureBtn" class="button button-min button-primary pull-right"
                               style="height:20px;margin-left: 10px;">上传</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品简介：
                        </label>
                        <div class="controls  control-row-auto mar0">
                            <textarea data-rules="{required:true}" name="introduce" placeholder="请输入内容"
                                      aria-disabled="false" aria-pressed="false" style="width: 400px;"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品详情：
                        </label>

                        <div class="controls control-row4" style="height: 350px;">
                            <textarea type="text" class="input-large" name="productDetail" id="productHTML"
                                      data-rules="{required:true}"></textarea>
                        </div>
                    </div>
                    <h3 class="offset2">商品图片
                        <a class="button button-primary pull-right" id="imagesBtn" style="height:20px">新增图片</a>
                    </h3>
                    <hr>
                    <input type="hidden" class="control-text span-width " name="images" id="productImgs"
                           data-rules="{required:true}">
                    <div id="imagesGrid" class="xm-grid">
                    </div>
                    <div id="specList">
                        <h3 class="offset2">商品规格
                            <a class="button button-primary pull-right" href="javascript:addSpec()">新增规格</a>
                        </h3>
                        <hr>
                        <input type="hidden" class="input-normal control-text" name="specCombination"
                               data-rules="{required : true}"/>
                        <input type="hidden" class="input-normal control-text" name="specNowPrice"/>
                        <input type="hidden" class="input-normal control-text" name="specPurchasePrice"/>
                        <#--<input type="hidden" class="input-normal control-text" name="specGroupPrice"/>-->
                        <input type="hidden" class="input-normal control-text" name="specStock"/>

                        <div id="specGrid" class="xm-grid">
                        </div>
                        <div class="centered">
                            <a class="button  button-large  button-success" id="saveBtn">立即上架</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<#-----------------------------------------------添加商品规格的弹出框------------------------------------------->
<div id="addSpecContent" style="display:none;">
    <form id="addSpecForm" class="form-horizontal">
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">规格组合：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" id="specCombination"
                           data-rules="{required : true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">市场价：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" id="specPurchasePrice"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">现价：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" id="specNowPrice"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
        <#--<div class="control-group span16">-->
        <#--<label class="control-label">团购价：</label>-->

        <#--<div class="controls">-->
        <#--<input type="text" class="input-normal control-text" id="specGroupPrice"-->
        <#--data-rules="{required : true,number:true}"/>-->
        <#--</div>-->
        <#--&lt;#&ndash;</div>&ndash;&gt;-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第一单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" id="firstCommission"-->
                           <#--data-rules="{required : true,number:true}" value="5"/>%-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第二单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" id="secondCommission"-->
                           <#--data-rules="{required : true,number:true}" value="10"/>%-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第三单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" id="thirdCommission"-->
                           <#--data-rules="{required : true,number:true}" value="20"/>%-->
                <#--</div>-->
            <#--</div>-->
            <div class="control-group span16">
                <label class="control-label">库存：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" id="specStock"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
        </div>
    </form>
</div>
<#-----------------------------------------------添加商品规格的弹出框------------------------------------------->
<script type="text/javascript">
    <#-----------------------------商品上架跳转----------------------------->
    var editor = UE.getEditor('productHTML');
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Data = BUI.Data;
    var Select = BUI.Select;
    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            if (data.success == true) {
                window.location.href = "${basepath}/rest/manage/product/toList";
            } else {
                alert(data.message);
            }
        }
    }).render();
    //点击保存按钮，判断
    $("#saveBtn").click(function () {
        var linkPhone = $("#linkPhone").val(),
                reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        if (addForm.isValid()) {
            if (!reg.test(linkPhone)) {
                BUI.Message.Alert('手机格式不正确', function () {
                }, 'info');
            }
            else {
                addForm.ajaxSubmit();
            }
        } else {
            addForm.valid();
        }
    });

    //商品分类获取--获取商品分类名称
    var pStore = new Store({
        url: '${basepath}/rest/manage/catalog/selectAllList',
        autoLoad: true,
        root: 'data'
    });
    var pSelect = new Select.Select({
        render: '#p-catalog',
        valueField: '#p-value',
        list: {
            itemTpl: '<li>{name}</li>',
            idField: 'id'
        },
        store: pStore
    }).render();

    /*--------------------------产品图片列表  begin--------------------*/
    /**
     * 图片信息
     * @type {*[]}
     */
    var imagesColumns = [
        {
            title: '图片', dataIndex: 'url', elCls: "center", width: '30%', renderer: function (data) {
                return '<img src="${basepath}/' + data + '" width="100px" height="100px"/>'
            }
        },
        {
            title: '状态', dataIndex: 'state', elCls: "center", width: '30%', renderer: function (data) {
                if (data == "SUCCESS") {
                    return "上传成功";
                }
                return "上传失败";
            }
        },
        {
            title: '操作', dataIndex: 'title', elCls: "center", width: '40%', renderer: function (data, obj, index) {
                return '<a href="javascript:delImages(' + index + ')">删除</a>';
            }
        }
    ];

    /**
     * 删除缓存图片信息
     * @param index
     */
    function delImages(index) {
        var record = imagesStore.findByIndex(index);
        imagesStore.remove(record);
        var imgs = imagesStore.getResult();
        var imgArr = new Array();
        for (var i = 0; i < imgs.length; i++) {
            imgArr[i] = imgs[i].url
        }
        $("#productImgs").val(imgArr.join(","));
    }

    var imagesStore = new Store({});
    var imagesGrid = new Grid.Grid({
        render: '#imagesGrid',
        width: '100%',//如果表格使用百分比，这个属性一定要设置
        columns: imagesColumns,
        idField: 'title',
        store: imagesStore
    });
    imagesGrid.render();
    /*--------------------------产品图片列表  end--------------------*/

    /*--------------------------产品规格列表  begin--------------------*/
    /**
     * 规格信息
     * @type {*[]}
     */
    var specColumns = [
        {title: '规格组合', dataIndex: 'specCombination', elCls: "center"},
        {title: '市场价', dataIndex: 'specPurchasePrice', elCls: "center"},
        {title: '现价', dataIndex: 'specNowPrice', elCls: "center"},
        // {title: '团购价', dataIndex: 'specGroupPrice', elCls: "center"},
        // {title: '第一单提成', dataIndex: 'firstPercent', elCls: "center"},
        // {title: '第二单提成', dataIndex: 'secondPercent', elCls: "center"},
        // {title: '第三单提成', dataIndex: 'thirdPercent', elCls: "center"},
        {title: '库存', dataIndex: 'specStock', elCls: "center"},
        {
            title: '操作', dataIndex: 'specCombination', elCls: "center", renderer: function (data) {
                return '<a href="javascript:delSpec(\'' + data + '\')">删除</a>';
            }
        }
    ];
    var specStore = new Store({});
    var specGrid = new Grid.Grid({
        render: '#specGrid',
        forceFit: true,
        columns: specColumns,
        store: specStore
    });
    specGrid.render();

    /**
     * 删除规格信息--不是放在数据库中，而是显示在列表上的用bui的remove实现
     * @param index
     */
    function delSpec(specCombination) {
        var record = specStore.find('specCombination', specCombination);
        specStore.remove(record);
        var specs1 = specStore.getResult();
        var specCombinationArr = [],
                specNowPriceArr = [],
                specPurchasePriceArr = [],
                specStockArr = [];
                // specGroupArr = [];
                // firstPercentArr=[],
                // secondPercentArr=[],
                // thirdPercentArr=[];
        for (var i = 0; i < specs1.length; i++) {
            specCombinationArr[i] = specs1[i].specCombination;
            specNowPriceArr[i] = specs1[i].specNowPrice;
            specPurchasePriceArr[i] = specs1[i].specPurchasePrice;
            specStockArr[i] = specs1[i].specStock;
            // specGroupArr[i] = specs1[i].specGroupPrice;
            // firstPercentArr[i]=specs1[i].firstPercent;
            // secondPercentArr[i]=specs1[i].secondPercent;
            // thirdPercentArr[i]=specs1[i].thirdPercent;
        }
        $("input[name='specCombination']").val(specCombinationArr.join(";"));
        $("input[name='specNowPrice']").val(specNowPriceArr.join(";"));
        $("input[name='specPurchasePrice']").val(specPurchasePriceArr.join(";"));
        $("input[name='specStock']").val(specStockArr.join(";"));
        // $("input[name='firstPercent']").val(firstPercentArr.join(";"));
        // $("input[name='secondPercent']").val(secondPercentArr.join(";"));
        // $("input[name='thirdPercent']").val(thirdPercentArr.join(";"));
    }

    /*--------------------------产品规格列表  end---------------------------*/
    /*------------------------图片上传插件配置  begin-----------------------*/
    //商品海报上传
    var backgroundUploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#backgroundBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });
    //获取一张海报并传入url
    backgroundUploader.on('uploadSuccess', function (file, response) {
        backgroundUploader.removeFile(file);
        addForm.setFieldValue("background", response.url)
    });
    //商品主图上传
    var pictureUploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#pictureBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });
    //获取一张主图并传入url
    pictureUploader.on('uploadSuccess', function (file, response) {
        pictureUploader.removeFile(file);
        addForm.setFieldValue("picture", response.url)
    });


    //商品详情图片上传
    var uploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#imagesBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });
    var imgs = imagesStore.getResult();
    uploader.on('uploadSuccess', function (file, response) {
        uploader.removeFile(file);
        if (imgs.length > 4) {
            BUI.Message.Alert('不能多于五张图片', function () {
            }, 'info');
        } else {
            imagesStore.add(response);
            var imgArr = new Array();
            for (var i = 0; i < imgs.length; i++) {
                imgArr[i] = imgs[i].url;
            }
            $("#productImgs").val(imgArr.join(","));
        }
    });
    /*------------------------图片上传插件配置  end-----------------------*/
    /*--------------------------添加商品规格方法 begin---------------------------------*/
    var specs = specStore.getResult();

    //新增商品规格--先存入表单中，不直接存入数据库
    function addSpecs() {
        var spec = {
            "specCombination": $('#specCombination').val(),
            "specNowPrice": $('#specNowPrice').val(),
            "specPurchasePrice": $('#specPurchasePrice').val(),
            // "specGroupPrice": $('#specGroupPrice').val(),
            "specStock": $('#specStock').val(),
            // "firstPercent": $('#firstCommission').val(),
            // "secondPercent": $('#secondCommission').val(),
            // "thirdPercent": $('#thirdCommission').val()
        };
        specStore.add(spec);
        var specCombinationArr = [],
                specNowPriceArr = [],
                specPurchasePriceArr = [],
                specStockArr = [];
                // specGroupArr = [];
                // firstPercentArr=[],
                // secondPercentArr=[],
                // thirdPercentArr=[];
        for (var i = 0; i < specs.length; i++) {
            specCombinationArr[i] = specs[i].specCombination;
            specNowPriceArr[i] = specs[i].specNowPrice;
            specPurchasePriceArr[i] = specs[i].specPurchasePrice;
            specStockArr[i] = specs[i].specStock;
            // specGroupArr[i] = specs[i].specGroupPrice;
            // firstPercentArr[i]=specs[i].firstPercent;
            // secondPercentArr[i]=specs[i].secondPercent;
            // thirdPercentArr[i]=specs[i].thirdPercent;
        }
        $("input[name='specCombination']").val(specCombinationArr.join(";"));
        $("input[name='specNowPrice']").val(specNowPriceArr.join(";"));
        $("input[name='specPurchasePrice']").val(specPurchasePriceArr.join(";"));
        $("input[name='specStock']").val(specStockArr.join(";"));
        // $("input[name='firstPercent']").val(firstPercentArr.join(";"));
        // $("input[name='secondPercent']").val(secondPercentArr.join(";"));
        // $("input[name='thirdPercent']").val(thirdPercentArr.join(";"));
        // $("input[name='specGroupPrice']").val(specGroupArr.join(";"));
    }

    /*--------------------------添加商品规格方法 end---------------------------------*/

    /*----------------------------------------------新增规格表单处理 begin------------------------------------*/
    var addSpecForm = new BUI.Form.Form({
        srcNode: '#addSpecForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addSpecForm.close();
        }
    }).render();
    var addSpecDialog = new BUI.Overlay.Dialog({
        title: '新增产品规格',
        width: 500,
        height: 280,
        contentId: 'addSpecContent',
        success: function () {
            if (addSpecForm.isValid()) {
                addSpecs();
                this.close();
            } else {
                addSpecForm.valid();
            }
        }
    });

    /**
     *规格新增按钮事件
     */
    function addSpec() {
        //新增规格的时候获取的表单为空
        $('#specCombination').val("");
        $('#specNowPrice').val("");
        $('#specPurchasePrice').val("");
        // $('#specGroupPrice').val("");

        $('#specStock').val("");
        addSpecDialog.show();
    }

    /*----------------------------------------------新增规格表单处理 end------------------------------------*/
</script>

</@page.pageBase>

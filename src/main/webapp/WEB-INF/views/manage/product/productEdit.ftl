<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="编辑商品">
<style>
    .button-large {
        font-size: 16px;
    }

    .webuploader-pick {
        background: none;
        padding: 0;
    }
</style>
<div class="xm-product">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="${basepath}/rest/manage/product/toList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/product/updateProductJson"
                      method="post">
                    <input type="hidden" class="control-text span-width span10" name="id" value="${e.id!}">

                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品名称：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="name" placeholder="请输入商品名称"
                                   value="${e.name!}" data-rules="{required : true}">
                        </div>
                    </div>
                    <#--<div class="control-group">-->
                        <#--&lt;#&ndash;<label class="control-label">&ndash;&gt;-->
                            <#--&lt;#&ndash;<s>*</s>&ndash;&gt;-->
                            <#--&lt;#&ndash;商品编码：&ndash;&gt;-->
                        <#--&lt;#&ndash;</label>&ndash;&gt;-->

                        <#--<div class="controls">-->
                            <#--<input type="text" class="control-text span-width span10" name="name" placeholder="请输入商品名称"-->
                                   <#--value="${e.id!}" disabled="disabled">-->
                        <#--</div>-->
                    <#--</div>-->
                    <#--<div class="control-group">-->
                        <#--<label class="control-label">-->
                            <#--<s>*</s>-->
                            <#--开团时间：-->
                        <#--</label>-->

                        <#--<div class="controls span8">-->
                            <#--<input type="text" class="calendar  calendar-time" name="startDate"-->
                                   <#--placeholder="请输入商品开团时间"-->
                                   <#--data-rules="{required : true}" value="${e.startDate!}">-->
                        <#--</div>-->
                    <#--</div>-->
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在地址：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="area" placeholder="请输入所在地址"
                                   data-rules="{required : true}" value="${e.area!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在小区：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="garden" placeholder="请输入所在小区"
                                   data-rules="{required : true}" value="${e.garden!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            自提区：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="address" placeholder="请输入自提区"
                                   data-rules="{required : true}" value="${e.address!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            联系人：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="linkMan" placeholder="请输入联系人"
                                   data-rules="{required : true}" value="${e.linkMan!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            联系电话：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="linkPhone"
                                   placeholder="请输入联系电话" value="${e.linkPhone!}"
                                   data-rules="{required:true,number:true}" id="linkPhone" onkeyup="value=value.replace(/[^\d.]/g,'')">
                        </div>
                    </div>
                  <#--<div class="control-group">-->
                    <#--<label class="control-label">-->
                      <#--<s>*</s>-->
                      <#--开团循环：-->
                    <#--</label>-->

                    <#--<div class="controls">-->
                      <#--<select data-rules="{required:true}" name="isSelf" value="${e.isSelf!}">-->
                        <#--<option value="">--请选择--</option>-->
                        <#--<option value="y">是</option>-->
                        <#--<option value="n">否</option>-->
                      <#--</select>-->
                    <#--</div>-->
                  <#--</div>-->
                    <div class="control-group">
                        <label class="control-label">  <s>*</s>第一单提成：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="firstPercent"
                                   data-rules="{required : true,number:true}" value="${e.firstPercent!}"/>%
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">  <s>*</s>第二单提成：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="secondPercent"
                                   data-rules="{required : true,number:true}" value="${e.secondPercent!}"/>%
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">  <s>*</s>第三单提成：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span8" name="thirdPercent"
                                   data-rules="{required : true,number:true}" value="${e.thirdPercent!}"/>%
                        </div>
                    </div>
                    <div class="control-group" style="margin-bottom: 10px;">
                        <label class="control-label">
                            <s>*</s>
                            商品海报：
                        </label>
                        <div class="controls" style="height: auto">
                            <input type="hidden" name="background"
                                   data-rules="{required : true}"  value="${e.background!}"/>
                            <img id="mainBackground" src="${basepath}/${e.background!}"
                                 style="max-width: 250px;width: 100%;max-height: 250px;">
                            <a id="backgroundBtn" class="button button-min button-primary pull-right"
                               style="height:20px;margin-left: 10px;">上传</a>
                        </div>
                    </div>
                    <div class="control-group" style="margin-bottom: 10px;">
                        <label class="control-label">
                            <s>*</s>
                            商品主图：
                        </label>

                        <div class="controls" style="height: auto">
                            <input type="hidden" name="picture" class="hiddenImg"/>
                            <img id="mainPicture" src=""
                                 style="max-width: 250px;width: 100%;max-height: 250px;">
                            <a id="pictureBtn" class="button button-min button-primary pull-right hidden"
                               style="height:20px;margin-left: 10px;">上传</a>
                        </div>
                    </div>
                  <div class="control-group">
                    <label class="control-label">
                      <s>*</s>
                      商品简介：
                    </label>
                    <div class="controls  control-row-auto mar0">
                            <textarea data-rules="{required:true}"  name="introduce" placeholder="请输入内容"
                                      aria-disabled="false" style="width: 400px;margin-top: 20px;">${e.introduce!}</textarea>
                    </div>
                  </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            商品详情：
                        </label>

                        <div class="controls control-row4" style="height: 350px;">
                            <textarea type="text" class="input-large" data-rules="{required:true}" name="productDetail"
                                      id="productHTML">${e.productDetail!}</textarea>
                        </div>
                    </div>
                    <h3 class="offset2">商品图片
                        <a class="button button-primary pull-right" id="imagesBtn" style="height:20px">新增图片</a>
                    </h3>
                    <hr>
                    <input type="hidden" class="control-text span-width " name="images" id="productImgs"
                           value="${e.images!}"
                           data-rules="{required:true}">

                    <div id="imagesGrid" class="xm-grid">
                    </div>
                <#----------------------------------------------------商品规格------------------------------------->
                    <div id="specList">
                        <h3 class="offset2">商品规格
                            <a class="button button-primary pull-right" href="javascript:addSpec()">新增规格</a>
                        </h3>
                        <hr>
                        <div id="specGrid" class="xm-grid">
                        </div>
                        <div class="centered">
                            <a class="button  button-large  button-success" id="saveBtn">立即上架</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<#-----------------------------------------------添加商品规格 start------------------------------------------->
<div id="addSpecContent" style="display:none;">
    <form id="addSpecForm" class="form-horizontal" action="${basepath}/rest/manage/spec/insertPJson?productID=${e.id}"
          method="post">
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">规格组合：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specCombination"
                           data-rules="{required : true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">市场价：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specPurchasePrice"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">现价：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specNowPrice"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>

            <#--<div class="control-group span16">-->
                <#--<label class="control-label">团购价：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="specGroupPrice" id="specGroupPrice"-->
                           <#--data-rules="{required : true,number:true}"/>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第一单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="firstPercent"-->
                           <#--data-rules="{required : true,number:true}" id="firstCommission"/>%-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第二单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="secondPercent"-->
                           <#--data-rules="{required : true,number:true}" id="secondCommission"/>%-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第三单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="thirdPercent"-->
                           <#--data-rules="{required : true,number:true}" id="thirdCommission"/>%-->
                <#--</div>-->
            <#--</div>-->
            <div class="control-group span16">
                <label class="control-label">库存：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specStock"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
        </div>
    </form>
</div>
<#-----------------------------------------------添加商品规格end------------------------------------------->

<#-----------------------------------------------编辑商品规格start------------------------------------------->
<div id="editSpecContent" style="display:none;">
    <form id="editSpecForm" class="form-horizontal" action="${basepath}/rest/manage/spec/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">规格组合：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specCombination"
                           data-rules="{required : true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">市场价：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specPurchasePrice"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">现价：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specNowPrice"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">团购价：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="specGroupPrice" id="specGroupPrice"-->
                           <#--data-rules="{required : true,number:true}"/>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第一单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="firstPercent"-->
                           <#--data-rules="{required : true,number:true}" />%-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第二单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="secondPercent"-->
                           <#--data-rules="{required : true,number:true}" />%-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="control-group span16">-->
                <#--<label class="control-label">第三单提成：</label>-->

                <#--<div class="controls">-->
                    <#--<input type="text" class="input-normal control-text" name="thirdPercent"-->
                           <#--data-rules="{required : true,number:true}" />%-->
                <#--</div>-->
            <#--</div>-->
            <div class="control-group span16">
                <label class="control-label">库存：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="specStock"
                           data-rules="{required : true,number:true}"/>
                </div>
            </div>
        </div>
    </form>
</div>
<#-----------------------------------------------编辑商品规格end------------------------------------------->

<script type="text/javascript">
    var editor = UE.getEditor('productHTML');
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Data = BUI.Data;
    var Select = BUI.Select;
    var imagesStore = new Store({});
    //判断是否有主图，没有主图那图片的第一张
    var mainImg ="${e.picture!}";
    var mainArr = "${e.images!}".split(",");
    if (mainImg == "" || mainImg == null) {
        for (var x = 0; x < mainArr.length; x++) {
            if (x == 0) {
                $(".hiddenImg").val(mainArr[x]);
                $("#mainPicture").attr("src", "${basepath}/" + mainArr[x]);
            }
        }
    } else {
        $(".hiddenImg").val(mainImg);
        $("#mainPicture").attr("src", "${basepath}/" + mainImg);
    }
    <#-----------------------------商品图片编辑 start----------------------------->
    var pictureUploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#pictureBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });

    pictureUploader.on('uploadSuccess', function (file, response) {
        pictureUploader.removeFile(file);
        editForm.setFieldValue("picture", response.url);
        $("#mainPicture").attr("src", "${basepath}/" + response.url);
    });
    //商品海报上传
    var backgroundUploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#backgroundBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });
    //获取一张海报并传入url
    backgroundUploader.on('uploadSuccess', function (file, response) {
        backgroundUploader.removeFile(file);
        editForm.setFieldValue("background", response.url);
        $("#mainBackground").attr("src", "${basepath}/" + response.url);
    });
    /*--------------------------商品图片列表  begin--------------------*/
    /**
     * 图片信息
     * @type {*[]}
     */
    var imagesColumns = [
        {
            title: '图片', dataIndex: 'url', width: '30%', renderer: function (data) {
            return '<img src="${basepath}/' + data + '" width="100px" height="100px"/>'
        }
        },
        {
            title: '状态', dataIndex: 'state', width: '30%', renderer: function (data) {
            if (data == "SUCCESS") {
                return "上传成功";
            }
            return "上传失败";
        }
        },
        {
            title: '操作', dataIndex: 'title', width: '40%', renderer: function (data, obj, index) {
            return '<a href="javascript:delImages(' + index + ')">删除</a>';
        }
        }
    ];
    /**
     * 删除缓存图片信息
     * @param index
     */
    function delImages(index) {
        var record = imagesStore.findByIndex(index);
        imagesStore.remove(record);
        var imgStr = imagesStore.getResult();
        var imgArr = new Array();
        for (var i = 0; i < imgStr.length; i++) {
            imgArr[i] = imgStr[i].url
        }
        $("#productImgs").val(imgArr.join(","));
    }
    /**
     * 初始化图片列表数据源
     * */
    function initImagesStore() {
        var imgArr = $("#productImgs").val().split(',');
        for (var i = 0; i < imgArr.length; i++) {
            var obj = new Object();
            obj.url = imgArr[i];
            obj.state = "SUCCESS";
            imagesStore.add(obj);
        }
    }

    initImagesStore();
    var imagesGrid = new Grid.Grid({
        render: '#imagesGrid',
        width: '100%',//如果表格使用百分比，这个属性一定要设置
        columns: imagesColumns,
        idField: 'title',
        store: imagesStore
    }).render();
    /*--------------------------活动图片列表  end--------------------*/

    /*------------------------图片上传插件配置  begin-----------------------*/
    var uploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/base/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#imagesBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });

    uploader.on('uploadSuccess', function (file, response) {
        uploader.removeFile(file);
        var imgs = imagesStore.getResult();
        if (imgs.length > 5) {
            BUI.Message.Alert('不能多于五张图片', function () {
            }, 'info');
        } else {
            imagesStore.add(response);
            var imgArr = new Array();
            for (var i = 0; i < imgs.length; i++) {
                imgArr[i] = imgs[i].url;
            }
            $("#productImgs").val(imgArr.join(","));
        }
    });
    /*--------------------------商品图片列表  end--------------------*/

    <#-----------------------------商品图片编辑 end----------------------------->
    <#-----------------------------商品编辑保存跳转----------------------------->
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            if (data.success == true) {
                window.location.href = "${basepath}/rest/manage/product/toList";
            } else {
                BUI.Message.Alert(data.message, 'error');
            }
        }
    }).render();
    //点击保存按钮，判断
    $("#saveBtn").click(function () {
        var linkPhone = $("#linkPhone").val(),
                reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        if (editForm.isValid()) {
            if (!reg.test(linkPhone)) {
                BUI.Message.Alert('手机格式不正确', function () {
                }, 'info');
            }
            else {
              editForm.ajaxSubmit();
            }
        } else {
            editForm.valid();
        }
    });
    <#-----------------------------商品编辑保存跳转----------------------------->
    //商品分类获取--获取商品分类名称
    var pStore = new Store({
        url: '${basepath}/rest/manage/catalog/selectAllList',
        autoLoad: true,
        root: 'data'
    });
    var pSelect = new Select.Select({
        render: '#p-catalog',
        valueField: '#p-value',
        list: {
            itemTpl: '<li>{name}</li>',
            idField: 'id'
        },
        store: pStore
    }).render();

    /*--------------------------产品规格列表  begin--------------------*/
    /**
     * 规格信息
     * @type {*[]}
     */
    var specColumns = [
        {title: '规格组合', dataIndex: 'specCombination', elCls: "center"},
        {title: '市场价', dataIndex: 'specPurchasePrice', elCls: "center"},
        {title: '现价', dataIndex: 'specNowPrice', elCls: "center"},
        // {title: '团购价', dataIndex: 'specGroupPrice', elCls: "center"},
        // {title: '第一单提成', dataIndex: 'firstPercent', elCls: "center"},
        // {title: '第二单提成', dataIndex: 'secondPercent', elCls: "center"},
        // {title: '第三单提成', dataIndex: 'thirdPercent', elCls: "center"},
        {title: '库存', dataIndex: 'specStock', elCls: "center"},
        {title: '利润', dataIndex: 'profit', elCls: 'center'},
        {
            title: '操作', dataIndex: 'specCombination', elCls: "center", renderer: function (value, data) {
            return '<a href="javascript: editSpec(\'' + data.id + '\')">编辑</a>' +
                    '<a style="margin-left:10px;"  href="javascript:delSpec(\'' + value + '\')">删除</a>';
        }
        }
    ];
    var specStore = new Store({
        url: '/rest/manage/spec/getSpec?productID=' +${e.id},
        autoLoad: true, //自动加载数据
        params: { //配置初始请求的参数
            length: '10',
            status: $("#status").val()
        },
        pageSize: 10,    // 配置分页数目
        root: 'list',
        totalProperty: 'total'
    });
    var specGrid = new Grid.Grid({
        render: '#specGrid',
        columns: specColumns,
        plugins: [Grid.Plugins.CheckSelection],
        forceFit: true,
        loadMask: true, //加载数据时显示屏蔽层
        store: specStore
    }).render();
    /*--------------------------产品规格列表  end---------------------------*/
    /*----------------------------------------------新增规格表单处理 begin------------------------------------*/
    var addSpecForm = new BUI.Form.Form({
        srcNode: '#addSpecForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            specStore.load(obj);
            addSpcDialog.close();
        }
    }).render();

    var addSpcDialog = new BUI.Overlay.Dialog({
        title: '新增规格',
        width: 500,
        height: 280,
        contentId: 'addSpecContent',
        success: function () {
            if (addSpecForm.isValid()) {
                addSpecForm.submit();
            } else {
                addSpecForm.valid();
            }
        }
    });
    function addSpec() {
        addSpecForm.clearFields();
        addSpcDialog.show();
    }
    /*----------------------------------------------新增规格表单处理 end------------------------------------*/
    /*------------------------------------------------编辑规格表单处理 start-----------------------------------------------*/
    var editSpecForm = new BUI.Form.Form({
        srcNode: '#editSpecForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            specStore.load(obj);
            editSpecDialog.close();
        }
    }).render();

    var editSpecDialog = new BUI.Overlay.Dialog({
        title: '编辑商品规格',
        width: 500,
        height: 280,
        contentId: 'editSpecContent',
        success: function () {
            if (editSpecForm.isValid()) {
                BUI.Message.Confirm('是否保存该规格', function () {
                    editSpecForm.submit();
                }, 'info');
            } else {
                editSpecForm.valid();
            }
        }
    });

    //编辑按钮事件
    function editSpec(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/rest/manage/spec/toEditJson",//弹窗编辑
            data: {
                id: id
            },
            success: function (data) {
                var form = $("#editSpecForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                specStore.load();
                editSpecDialog.show();
            }
        });
    }
    /*------------------------------------------------编辑规格表单处理 end------------------------------------------------*/

    /*--------------------删除规格操作----------------------------*/
    function delSpec(com) {
        BUI.Message.Confirm('确认要删除吗？', function () {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/spec/deleteByComJson",
                dataType: "json",
                data: {
                    specCombination: com
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    specStore.load(obj);
                }
            });
        }, 'question');
    }
    /*------------------商品规格编辑 end-------------------------------*/
</script>
</@page.pageBase>

<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="商品管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger, .shelves-button {
        margin-left: 5px;
    }
    .bui-grid-cell-text{
        min-height: 0px;
        min-width: 0px;
        height: 20px;
        white-space: nowrap; overflow: hidden; text-overflow: ellipsis
    }
</style>
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/product/loadData">
    <ul class="panel-content ">
        <li>
            <div class="control-group span5">
                <div class="controls">
                    <input type="text" id="name" name="name" placeholder="输入关键字搜索"/>
                </div>
            </div>
            <div class="form-actions span16">
                <button type="submit" class="button  button-primary">
                    查询
                </button>
                <a href="${basepath}/rest/manage/product/toAdd" class="button button-success">添加</a>
                <a class="button button-danger" href="javascript:delFunction()">
                    批量删除
                </a>
                <a class="shelves-button button" href="javascript:changeProductStatus(1)">
                    上架
                </a>
                <a class="shelves-button button" href="javascript:changeProductStatus(0)">
                    下架
                </a>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<#---------------------------------------------列表操作------------------------------------>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '商品编号', dataIndex: 'id', elCls: 'center'},
                {title: '商品名称', dataIndex: 'name', elCls: 'center', width: 300},
                {title: '库存', dataIndex: 'number', elCls: 'center'},
                {
                    title: '商品状态', dataIndex: 'status', elCls: 'center', renderer: function (data) {
                    if (data == "0") {
                        return '已上架';
                    }
                    else if (data == "1") {
                        return '已下架';
                    }
                }
                },
                {
                    title: '操作', dataIndex: 'id', elCls: "center", renderer: function (data, obj) {
                    var option1 = '<a href="${basepath}/rest/manage/product/toEdit?id='
                            + data + '">编辑</a>';
                    var option2;
                    var option3 = '<a href="javascript:delOneFunction('+data+');">删除</a>';
                    if (obj.status == 1) {
                        option2 = '<a href="javascript:changeOneStatus('+1+','+data+');">上架</a>';
                    } else if (obj.status == 0) {
                        option2 = '<a href="javascript:changeOneStatus('+0+','+data+');">下架</a>';
                    }
                    return option1 + "&nbsp;&nbsp;&nbsp;&nbsp;" + option2 + "&nbsp;&nbsp;&nbsp;&nbsp;" + option3;
                }
                }
            ];
    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                forceFit: true,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    <#---------------------------------------------列表操作------------------------------------>
    <#---------------------------------------------模糊查询------------------------------------>
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    <#---------------------------------------------模糊查询------------------------------------>
    <#---------------------------------------------删除操作------------------------------------>
    /**
     * 批量删除选中记录
     */
    function delFunction() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString();
        }
        BUI.Message.Confirm('确认要删除吗？',function() {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/product/deletesJson",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = form.serializeToObject();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        },'question');
    }

        /**
         * 删除当个当前记录
         */
        function delOneFunction(id) {
            BUI.Message.Confirm('确认要删除吗？',function() {
                $.ajax({
                    type: "POST",
                    url: "${basepath}/rest/manage/product/deleteByIdJson",
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success: function (data) {
                        var obj = form.serializeToObject();
                        obj.start = 0; //返回第一页
                        store.load(obj);
                    }
                });
            },'question');
        }
    <#---------------------------------------------删除操作------------------------------------>
    <#---------------------------------------------改变状态------------------------------------>
        /**
         *改变单个商品状态
         * */
        function changeOneStatus(type,id) {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/product/changeOneStatus",
                dataType: "json",
                data: {
                    id:id,
                    type: type
                },
                success: function (data) {
                    var obj = form.serializeToObject();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }

        /**
         * 批量更改商品状态
         * */
        function changeProductStatus(type) {
            var selections = grid.getSelection();
            var ids = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].id.toString()
            }
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/product/changeStatus",
                dataType: "json",
                data: {
                    idArr: ids,
                    type: type
                },
                success: function (data) {
                    var obj = form.serializeToObject();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }
    <#---------------------------------------------改变状态------------------------------------>
</script>
</@page.pageBase>

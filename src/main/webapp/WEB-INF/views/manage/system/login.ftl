<#import "../tpl/htmlTep.ftl" as html>

<@html.htmlBase checkLogin=false >
<style type="text/css">
    html,body{width: 100%;height:100%;min-width: 1024px;font-family: '微软雅黑';font-size: 16px;}
    h3{font-family: '微软雅黑';}
    body{background: url("${staticpath}/manage/images/loginBg.jpg") no-repeat;background-size: cover;}
    .login-box{margin-right: 10%;}
    .login-box h3{display: block; font-size: 26px;color: #c8d4df; z-index: 1;margin: 20% auto 20px; text-align: center; }
    .well{background: #fff;box-shadow: none;-webkit-box-shadow:none;margin:0;padding-bottom: 35px;}
    .mb20{margin-bottom:20px!important;}
    .error-tips{color: red;margin-left: 30px;padding-top: 10px;}
    .center{text-align: center;}
    .login-box .panel,.login-box .panel-header{background: #f5f5f5;border-bottom: 0;padding: 0;}
    .login-box input{height: 40px;width: 80%;line-height: 40px;padding: 0; font-size: 20px;}
    .panel-body{padding: 0;}
    .panel-title{font-size: 22px;font-weight: bold ;margin:0 auto;height:50px;line-height: 50px;  text-align: center; }
    .panel-body .row .name{ text-align: left; font-size: 16px;margin-left: 34px; margin-bottom: 10px;}
    .panel-body .btn{display: block; width: 80%;margin: 20px auto 0;height:45px;line-height: 45px; text-decoration: none;
    text-align:center;color: #fff; border-radius: 4px;font-size:17px; background-color: #335397; border-style: none;}
    .panel-body .checkbox{width: 15px; height:15px;display: inline-block;margin-left: 30px;}
    .checkbox-tip{font-size: 15px;color:#afafaf;}
</style>
<body>
<#--header-->

<div class="row span10 pull-right login-box">
    <h3>味猫后台系统</h3>
    <div class="panel">
        <div class="panel-header" >
            <div class="panel-title">登录</div>
        </div>
        <div class="panel-body">
            <form class="form-horizontal well" action="${basepath}/rest/manage/user/login" method="post">
                <div class="row mb20 center ">
                   <div class="name">用户名</div>
                      <input type="text" value="${e.username!""}"  name="username" class="control-text" id="username" autofocus/>
                </div>
                <div class="row center mb20 ">
                    <div class="name">密码</div>
                    <input type="text" onfocus="this.type='password'" name="password"  class="control-text" label="密码"/>
                </div>
                <div class="control-group">
                    <label class="checkbox-tip">
                        <input type="checkbox" class=" checkbox"> 下次自动登录</label>
                </div>
                <#if errorMsg??>
                    <div class="row">
                        <div class="error-tips">*${errorMsg}</div>
                    </div>
                </#if>
                <div class="row">
                    <button type="submit" class="btn">点击登录</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
</body>
</@html.htmlBase>

<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="任务管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger {
        margin-left: 5px;
    }

    .form-horizontal .time-label {
        width: 50px;
        line-height: 40px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
    [class*="span"] {
      margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
            <label class="time-label control-label">状态：</label>

            <div class="time-controls controls">
                <select name="status" class="input-normal" id="status">
                    <option value="">全部</option>
                    <option value="y">启用</option>
                    <option value="n">禁用</option>
                </select>
            </div>

            <div class="form-actions span8">
                <button type="submit" class="button  button-primary">
                    查询
                </button>
                <a href="javascript:add()" class="button button-success">添加</a>
                <button class="button button-danger" onclick="return delFunction()">
                    删除
                </button>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<div id="addContent" style="display:none;">
    <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/task/insertJson" method="post">
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">任务名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="name" data-rules="{required : true}">
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">任务组名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="groupName"
                           data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">触发器：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="triggerName"
                           data-rules="{required : true}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">表达式：</label>

                <div class="controls ">
                    <input type="text" class="input-normal control-text" name="cron" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">状态：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="state">
                        <option value="">-请选择-</option>
                        <option value="1">启用</option>
                        <option value="0">禁用</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/task/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">任务名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="name" data-rules="{required : true}">
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">任务组名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="groupName"
                           data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">触发器：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="triggerName"
                           data-rules="{required : true}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">表达式：</label>

                <div class="controls ">
                    <input type="text" class="input-normal control-text" name="cron" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">状态：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="state">
                        <option value="">-请选择-</option>
                        <option value="1">启用</option>
                        <option value="0">禁用</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '任务ID', dataIndex: 'id', width: '10%',elCls: 'center'},
                {title: '任务名称', dataIndex: 'name', width: '10%',elCls: 'center'},
                {title: '任务组名称', dataIndex: 'groupName', width: '40%',elCls: 'center'},
                {title: '触发器', dataIndex: 'triggerName',width: '10%',elCls: 'center'},
                {title: '表达式', dataIndex: 'cron', width: '10%',elCls: 'center'},
                {title: '当前状态', dataIndex: 'state', width: '10%',elCls: 'center'},
                {
                    title: '操作', dataIndex: 'id', width: '10%',elCls: 'center', renderer: function (value, obj, index) {
                    return '<a href="javascript:runTask(' + value + ')">立即执行</a>';
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    //删除选中的记录
    function delFunction() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/task/deletesJson",
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = form.serializeToObject();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });

    }
    /*----------------------------------------------新增表单处理 begin------------------------------------*/

    var Data = BUI.Data;


    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addDialog.close();
        }
    }).render();

    var addDialog = new BUI.Overlay.Dialog({
        title: '新增任务',
        width: 500,
        height: 380,
        contentId: 'addContent',
        success: function () {
            if (addForm.isValid()) {
                addForm.ajaxSubmit();
            } else {
                addForm.valid();
            }
        }
    });
    //添加按钮事件
    function add() {
        addDialog.show();
    }
    /*----------------------------------------------新增表单处理 end------------------------------------*/

    /*----------------------------------------------编辑表单处理 begin------------------------------------*/
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();

    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑任务',
        width: 500,
        height: 320,
        contentId: 'editContent',
        success: function () {
            editForm.ajaxSubmit();
        }
    });
    //编辑按钮事件
    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/task/toEditJson",
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                editDialog.show();
            }
        });
    }
    /*----------------------------------------------编辑表单处理 end------------------------------------*/

    function runTask(id){
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/task/runTask",
            data: {id: id},
            success: function (data) {
                BUI.Message.Alert('执行成功！',function(){
                },'info');
            }
        });
    }
</script>

</@page.pageBase>
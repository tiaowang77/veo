<#macro htmlBase title=""  checkLogin=true>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="${systemSetting().description}"/>
    <meta name="keywords" content="${systemSetting().keywords}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${(title?? && title!="")?string("${systemSetting().systemCode} - "+ title , "${systemSetting().systemCode}")}</title>
    <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
    <link href="${staticpath}/base/bui/css/bs3/dpl.css" rel="stylesheet">
    <link href="${staticpath}/base/bui/css/bs3/bui.css" rel="stylesheet">
    <link rel="stylesheet" href="${staticpath}/base/bui-admin/index.css">
    <link rel="stylesheet" href="${staticpath}/base/bui/css/base.css">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_513012_ji3xs5d5kc81if6r.css">
    <link rel="stylesheet" href="${staticpath}/base/ueditor/third-party/webuploader/webuploader.css">


   <script src="${staticpath}/base/jquery-1.8.1.min.js"></script>
   <script src="${staticpath}/base/bui/bui.js"></script>
    <script type="text/javascript" src="${staticpath}/base/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="${staticpath}/base/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="${staticpath}/base/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script src="${staticpath}/base/ueditor/third-party/webuploader/webuploader.js"></script>
</head>
    <#nested />
</html>
</#macro>

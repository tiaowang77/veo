<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="提现审批">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary,.button-danger {
        margin-left: 5px;
    }

    .form-horizontal .time-label {
        width: 60px;
        line-height: 40px;
        margin-left: 20px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
    .form-horizontal .money-label {
        width: 60px;
        line-height: 40px;
        margin-left: 5px;
    }
    .money-row{
        padding:8px 22px;
        width: 100%;
        height: auto;
    }
    .money-row .money-box{
        display: inline-block;
        width: 250px;
        height: 100px;
        border-radius: 10px;
        color: #fff;
        margin-right: 40px;
    }
    .color-green{
        background-color: #32cc7f;
    }
    .color-blue{
        background-color: #47a9b7;
    }
    .color-purple{
        background-color: #7d659a;
    }
    .money-row .pic{
        float: left;
        width: 54px;
        height: 60px;
        margin: 28px  0 0 23px;
        opacity: 0.3;
    }
    .money-row .right-inp{
        display: inline-block;
        height: auto;
        margin:30px 0 0 50px;
    }
    .money-row .money-input{
        height: 30px;
        width:100px ;
        font-size: 20px;
        background-color: inherit!important;
        outline: none;
        border: none;
        box-shadow: none;
        padding: 0;
    }
    .money-row .money-text{
    font-size:12px ;
    }
</style>
<!--suppress ALL -->
<div class="money-row">
    <div class="money-box color-green">
        <img src="${staticpath}/manage/images/moneybag.png" alt="" class="pic">
        <div class="right-inp">
            <input type="text" class="money-input"id="totalMoney" value="0.00" disabled/>
            <div class="money-text">总金额</div>
        </div>
    </div>
    <div class="money-box color-blue">
        <img src="${staticpath}/manage/images/moneybag.png" alt="" class="pic">
        <div class="right-inp">
            <input type="text" class="money-input" id="passMoney" value="0.00" disabled/>
            <div class="money-text">已提现金额</div>
        </div>
    </div>
    <div class="money-box color-purple">
        <img src="${staticpath}/manage/images/moneybag.png" alt="" class="pic">
        <div class="right-inp">
            <input type="text" class="money-input" id="stayMoney" value="0.00" disabled/>
            <div class="money-text">未提现金额</div>
        </div>
    </div>
</div>
<form id="searchForm" class="form-panel" action="">
    <ul class="panel-content">
        <li>
            <div class="control-group span5">
                <div class="search-controls  controls">
                    <input type="text" name="applicantName" id="title" value="" placeholder="查询昵称、收款账号、收款人">
                </div>
            </div>
            <div>
                <label class="time-label control-label">起始日期：</label>

                <div class="time-controls controls">
                    <input name="startDate" class="input-small calendar" type="text" style="width: 80px;"><label>
                    &nbsp;-&nbsp;</label>
                    <input name="endDate" class="input-small calendar" type="text" style="width: 80px;">
                </div>

                <button type="submit" class="button  button-primary">
                    查询
                </button>

            </div>
        </li>
    </ul>
</form>
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/wallet/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id" id="statusID" value=""/>
        <input type="hidden" class="input-normal control-text" name="applicant" id="applicantID" value=""/>
        <input type="hidden" class="input-normal control-text" name="money" id="retMoney" value=""/>
        <div class="row">
            <label class="time-label control-label" style="width: 100px;">审批状态：</label>

            <div class="time-controls controls">
                <select name="status" class="input-normal" id="status">
                    <option value="">请选择状态</option>
                    <option value="2">通过</option>
                    <option value="3">不通过</option>
                </select>
            </div>
        </div>
    </form>
</div>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title : '序号',dataIndex :'id', elCls: 'center', width:'5%'},
                {title : '申请微信昵称',dataIndex :'applicantName', elCls: 'center', width:'10%'},
                {title : '申请日期',dataIndex :'createTime', elCls: 'center', width:'15%'},
                {title : '提现类型',dataIndex :'accountType', elCls: 'center', width:'10%',renderer:function(value){
                    return '<span>微信</span>'
                }},
                {title : '转账账号',dataIndex :'payAccount', elCls: 'center', width:'15%'},
                {title : '提现人',dataIndex :'payee', elCls: 'center', width:'10%'},
                {title : '提现金额',dataIndex :'money', elCls: 'center', width:'10%'},
                {title : '预留手机号',dataIndex :'payeePhone', elCls: 'center', width:'10%'},
                {title : '提现状态',dataIndex :'status', elCls: 'center', width:'8%',renderer:function(value){
                    if(value==3){
                        return '<span>不通过</span>';
                    }else if(value==2){
                        return '<span>已通过</span>';
                    }else{
                        return '<span>待审核</span>';
                    }
                }},
                {title : '操作',dataIndex :'status', elCls: 'center', width:'7%',renderer:function(value , data){
                    var stayMoney=data.totalMoney-data.passMoney;
                    $("#totalMoney").val(data.totalMoney);
                    $("#passMoney").val(data.passMoney);
                    $("#stayMoney").val(Math.round(stayMoney*100)/100);
                    if(value==3||value==2){
                        return '----';
                    }else{
                        return '<a href="javascript:changeStatus(' + data.id + ','+data.applicant+','+data.money+');">审批</a>'
                    }
                }}
            ];

    var store = new Store({
                url : 'loadData',
                autoLoad:true, //自动加载数据
                params : { //配置初始请求的参数
                    length : '10'
                },
                pageSize:10,	// 配置分页数目
                root : 'list',
                totalProperty : 'total'
            }),
            grid = new Grid.Grid({
                render:'#grid',
                columns : columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins : [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar:{
                    pagingBar:true
                }
            });

    grid.render();

    //查询框
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
/*-------------------------审批弹出框------------------------*/
    var editForm = new BUI.Form.HForm({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();
    editForm.setInternal('initRecord',editForm.serializeToObject());
    var editDialog = new BUI.Overlay.Dialog({
        title: '审批',
        width: 380,
        height: 180,
        contentId: 'editContent',
        success: function () {
            editForm.submit();
        }
    });
    //审批按钮事件
    function changeStatus(id,applicant,money) {
        editForm.reset();
        $("#statusID").val(id);
        $("#applicantID").val(applicant);
        $("#retMoney").val(money);
        editDialog.show();
    }
    //总金额单独方法
    function totalMoney(){
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/wallet/selectTotalMoney",
            dataType: "json",
            success:function(data){
                if(data.data==0){
                    $("#totalMoney").val("0.00");
                }else {
                    $("#totalMoney").val(data.data);
                    var stayMoney=data.data- $("#passMoney").val();
                    $("#stayMoney").val(Math.round(stayMoney*100)/100);
                }
            }
        })
    }
    totalMoney();
</script>

</@page.pageBase>

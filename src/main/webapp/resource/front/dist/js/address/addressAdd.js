function saveInf(data){
    $.ajax({
        type:'POST',
        dataType:'json',
        url:'/rest/front/address/insertAddress',
        data:data,
        success: function () {
            $('#editBtn').attr("state", "1");
            window.location.href=document.referrer;
        }
    })
}
$(function () {
   $('#editBtn').click(function () {
       if(!$(this).attr('state')) {
           var name = $('.r-name').val(),
               tel = $('.r-tel').val(),
               province = $('.r-area').val(),
               addressDetail = $('.r-location').val(),
               isdefault = $("#orderSubComplete").prop('checked') ? 'y' : 'n',
               houseNumber=$('.r-floor').val();
           var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
           if (name && tel && province && addressDetail && houseNumber) {
              var data={name:name,phone:tel, area: province, address: addressDetail,houseNumber:houseNumber};
               reg.test(tel) ?  saveInf(data) : $.toast('手机格式不正确')
           }else{
               $.toast('信息未填写完整');
           }
       }
   })
});

function getAddr(aid){
    $.ajax({
    type: 'GET',
    url: '/rest/front/address/selectDetail',
    dataType: 'json',
    data: {
        id: aid
    },
    success: function (data) {
        $('.r-name').val(data.data.name);
        $('.r-tel').val(data.data.phone);
        $('.r-area').val(data.data.area);
        $('.r-location').val(data.data.address);
        $('.r-floor').val(data.data.houseNumber);
        if (data.data.isdefault == 'y') {
            $("#orderSubComplete").attr('checked', true);
        }
    }
});
}
function saveInf(data){
    $.ajax({
        type:'POST',
        dataType:'json',
        url:"/rest/front/address/updateJson",
        data:data,
        success: function () {
            $('#editBtn').attr("state", "1");
            $.toast("编辑成功");
            setTimeout(function () {
                window.location.href= document.referrer;
            },100);
        }
    })
}
$(function () {
    var aid=getQueryString('aid');
    getAddr(aid);
    $('#editBtn').click(function () {
        if(!$(this).attr('state')) {
            var name = $('.r-name').val(),
            tel = $('.r-tel').val(),
            province = $('.r-area').val(),
            addressDetail = $('.r-location').val(),
            isdefault = $("#orderSubComplete").prop('checked') ? 'y' : 'n',
            houseNumber=$('.r-floor').val();
            var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
            if (name && tel && province && addressDetail && houseNumber) {
                var data={name:name,phone:tel, area: province, address: addressDetail,houseNumber:houseNumber,id:aid};
                reg.test(tel) ?  saveInf(data) : $.toast('手机格式不正确')
            }else{
                $.toast('信息未填写完整');
            }
        }
    })
});


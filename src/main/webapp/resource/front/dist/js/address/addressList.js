//添加新地址
function getAddr() {
    $.ajax({
        type: 'GET',
        url: "/rest/front/address/selectAddressList",
        dataType: 'json',
        success: function (data) {
            var list = data.data, html = '';
            if (list!=null&&list!='') {
                $('.lack').hide();
                $.each(list, function (i, e) {
                    html += ' <li class="row" data-id="' + e.id + '"> <div class="col-100"> <span class="o-name">' + e.name + '</span>' + '<span class="o-tel">' + e.phone + '</span><p class="o-address">' + e.area + e.address + " " + e.houseNumber + '</p> ' + '</div> <div class="address-fn col-100"> <span class="c-box">';
                    html += e.isdefault == 'y' ? '<input type="radio" checked="checked"/>' : '<input type="radio" />';  //是否默认地址
                    html += '</span>';
                    html += '<span class="fn-btn l"><a   onclick="editAddr(' + e.id + ')"><i class="iconfont icon-bianji"></i>编辑</a> ' + '<a class="a-delete confirm-del l"><i class="iconfont icon-weibiaoti--"></i>删除</a> </span>';
                    html += '</div> </li>';
                });
                $("ul").html(html);
            }
        }
    })
}

function delAddr(dele) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/rest/front/address/deleteJson',
        data: {id: dele},
        success: function () {
            $.toast("删除成功");
            setTimeout(function () {
                window.location.href = "/rest/front/address/toAddressList";
            }, 100);
        }
    })
}
function editAddr(id) {
    var url = '/rest/front/address/toAddressEdit';
    window.location.href = url + '?aid=' + id;
}
$(function () {
    var spec=getQueryString('addressType'),
        editID=getQueryString('editID');
    getAddr();
    $(document).on('click', '.confirm-del', function (e) {
        var b = e.target;
        $.confirm('确认删除？', function () {
            var e = $(b).parents('.row');
            var dele = e.attr('data-id');
            e.remove();
            return delAddr(dele);
        })
    });
    $(".addBtn").click(function () {
        window.location.href = "/rest/front/address/toAddressAdd";
    });
    //切换默认地址
    $(document).on("click", ".c-box,.col-100", function () {
        $('ul').find('input').prop('checked', false);
        var id = $(this).parents('.row').attr('data-id');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/rest/front/address/setDefault',
            data: {
                id: id
            },
            success: function () {
                if (spec == 1) {//返回添加商品页面
                    window.location.href = "/rest/front/product/toAddProduct";
                } else if (spec == 2) {//返回编辑商品页面
                    window.location.href = "/rest/front/product/toEditProduct?id=" + editID;
                } else if (spec == 3) {//返回订单详情的订单详情页面
                    window.location.href=document.referrer;
                } else {
                    window.location.reload();
                }
            }
        })
    });
    //返回键
    $("#back").click(function () {
        if (spec == 1) {//返回添加商品页面
            window.location.href = "/rest/front/product/toAddProduct";
        } else if (spec == 2) {//返回编辑商品页面
            window.location.href = "/rest/front/product/toEditProduct?id=" + editID;
        } else if (spec == 3) {//返回订单详情的订单详情页面
            window.location.href=document.referrer
        }  else {
            window.location.href = "/rest/front/user/toMyPage";

        }

    });

});





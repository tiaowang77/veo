/**
 * Created by cxw on 2017/12/16.
 */
function getComment(oID,src) {
    $.ajax({
        type: "GET",
        url: "/rest/front/comment/selectCommentDetail",
        data: {
            oID: oID
        },
        dataType: "json",
        success: function (data) {
            var datalist = data.data;
            var imgHtml='';
            var html = '<ul class="underline"><li class="comment-header"><img src="' +src + '"  class="comment-left cell-left ">' +
                '<div class="comment-main"><div class="comment-title cell-right">' + datalist.productName + '</div>' +
                '<div class="comment-num">x<span>' + datalist.productNumber + '</span></div></li></ul>';
            $('.comment-text').text(datalist.content);
            //评论等级
            switch (datalist.rank) {
                case '好评':
                    $(".check-comment li:nth-last-child(3)").addClass('selectComment');
                    break;
                case '中评':
                    $(".check-comment li:nth-last-child(2)").addClass('selectComment');
                    break;
                case '差评':
                    $(".check-comment li:nth-last-child(1)").addClass('selectComment');
                    break;
            }
            if (datalist.picture) {
                var imgArr = data.data.picture.split(",");
                for (var i in imgArr) {
                    imgHtml+= '<li ><img class="upimg" src="/' + imgArr[i] + '"></li>'
                }
            }
            datalist.isHide == "y"?$(".check").attr('checked', true):$(".check").attr('checked', false);
            $('.content').prepend(html);
            $('.pic-box').prepend(imgHtml);
        }
    })
}
function back(){
    window.location.href='/rest/front/comment/toCommentList';
}

$(function () {
    var oID = getQueryString('oID');
    var src=getQueryString('src');
    getComment(oID,src);

});

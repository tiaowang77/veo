/**
 * Created by cxw on 2017/12/15.
 */
function getComment(oID) {
    $.ajax({
        type: "GET",
        url: '/rest/front/orderdetail/selectOrderdetailList',
        dataType: "json",
        success: function (data) {
            var datalist = data.data;
            var html='';
            $.each(datalist, function (i, e) {
                if (e.id == oID) {
                    var img=e.picture|| e.images.split(',')[0];
                     html += '<ul class="underline"><li class="comment-header" data-id="'+ e.id+'"><img src="/' +img+ '"  class="comment-left cell-left ">' +
                        '<div class="comment-main"><div class="comment-title cell-right">' + e.productName + '</div>' +
                        '<div class="comment-num">x<span>' + e.number + '</span></div></li></ul>';
                }
            });
            $('.content').prepend(html);
        }
    })
}

function Submit(data){
    $.ajax({
        type:'POST',
        url: "/rest/front/comment/insertComment",
        data:data,
        dataType:'json',
        success: function () {
            setTimeout(function () {
                window.location.href = "/rest/front/comment/toCommentList";
            }, 1000);
        }
    })
}
function back(){
    window.location.href='/rest/front/comment/toCommentList';
}
$(function () {
    var oID = getQueryString('oID');
    getComment(oID);
    //点击是否好评

    $('.check-comment').on('click', 'li', function () {
        $(this).addClass('selectComment');
        $(this).siblings('li').removeClass('selectComment');
    });
    $('.saveBtn').click(function () {
        var content = $('.comment-text').val(),
            picurl = $('#picurl').val(),
            isHide = $(".check").prop("checked") == true ? "y" : "n",
            id=$('.comment-header').attr('data-id'),
            commentRank=$('.selectComment').text();
        var data={userID: sessionStorage.id,oID: id,isHide: isHide,content: content,picture: picurl,rank:commentRank
        };
        if(!content){
            $.alert('请填写评价!');
            return false;
        }
       return  Submit(data);
    })
});

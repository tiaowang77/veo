/**
 * Created by cxw on 2017/12/15.
 */

function getComment() {
    $.ajax({
        type: "GET",
        //url: '/rest/front/orderdetail/selectOrderdetailListCS',
        //data: {
        //    userID: 234
        //},
        url: '/rest/front/orderdetail/selectOrderdetailList',
        dataType: "json",
        success: function (data) {
            var datalist = data.data,
                html = '';
            if(datalist!=null&&datalist!='') {
                $('.lack').hide();
                $.each(datalist, function (i, e) {
                    var img = e.picture || e.images.split(',')[0];
                    html += ' <ul class=" underline"  data-status="' + e.isComment + '"  data-id="' + e.id + '"><li class="flex goods-box"><img src="/' + img + '" alt="" class="commentpic cell-right">' +
                        '<div class="cell-main"><div class="name over">' + e.productName + '</div>' +
                        '<div class="timetip commentt">购买时间：<span class="buytime">' + e.createTime + '</span></div>';
                    html += e.isComment == "n" ? '<a  class="join comment " >评价商品</a>' : '<div class="join watchcomment">查看评价</div>'
                    html += '</div></li></ul>';
                });
            }
            $('#all').html(html)
        }
    })
}
function back(){
window.location.href='/rest/front/user/toMyPage'
}
$(function () {
    $('.buttons-row').on('click', 'a', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var status = $(this).attr('data-status');
        $('ul').show();
        if ($(this).attr('data-status') != 'all') {
            $('ul:not([data-status="' + status + '"])').hide();
        }
    });
    $(document).on('click','.comment',function(){
        var oID=$(this).parents('ul').attr('data-id');
        window.location.href='/rest/front/comment/toCommentAdd'+'?oID='+oID;
    });
    $(document).on('click','.watchcomment',function(){
        var oID=$(this).parents('ul').attr('data-id');
        var src=$(this).parents('ul').find('img').attr('src');
        window.location.href='/rest/front/comment/toCommentDetail'+'?oID='+oID+'&src='+src;
    });
    getComment();
});

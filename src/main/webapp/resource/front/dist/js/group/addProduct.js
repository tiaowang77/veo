/**
 * Created by cxw on 2017/12/15.
 */
function queryAddr() {
  $.ajax({
    type: 'GET',
    dataType: 'json',
    url: "/rest/front/address/selectDefault",
    success: function (data) {
      var list = data.data;
      if (list) {
        var html = '<span class="addrname cell-right">' + list.name + '</span><span class="tel">' + list.phone + '</span>' +
          '<div class="address overhide"><span class="area">' + list.area + '</span>&nbsp;<span class="addressDetail">' + list.address + ''+list.houseNumber+'</span></div>';
        $('.addressMain').html(html);
        $('.addr').click(function () {
          window.location.href = '/rest/front/address/toAddressList?addressType=1';
        })
      } else {
        $('.addr').click(function () {
          window.location.href = '/rest/front/address/toAddressAdd?addressType=1';
        })
      }
    }
  })
}
function Submit(params) {
  $.ajax({
    type: 'POST',
    url: "/rest/front/product/insertProduct",
    data: params,
    dataType: 'json',
    success: function (data) {
      if (data.success == true) {
        $.toast("保存成功");
        setTimeout(function () {
          window.location.href = "/rest/front/product/toProductList";
        }, 1000);
      } else {
        $.alert(data.message);
      }

    }
  })
}
var specCombination = '',//商品规格的颜色
  specPurchasePrice = '',//商品规格的进价
  specNowPrice = '',//商品规格的现价
  specGroupPrice = '',//商品规格的团购价
  specStock = '';//商品规格库存
//规格数组的方法
function querySpec() {
  specCombination = '';//商品规格的颜色
  specPurchasePrice = '';//商品规格的进价
  specNowPrice = '';//商品规格的现价
  specGroupPrice = '';//商品规格的团购价
  specStock = '';//商
  $(".p-color").each(function () {
    specCombination += $(this).val() + ",";
  });
  $(".p-purchase").each(function () {
    specPurchasePrice += $(this).val() + ",";
  });
  $(".p-now").each(function () {
    specNowPrice += $(this).val() + ",";
  });
  $(".p-group").each(function () {
    specGroupPrice += $(this).val() + ",";
  });
  $(".p-stock").each(function () {

    specStock += $(this).val() + ",";
  });
}
function eachInput() {
  var x = 0;//判断input是否为空
  $("#pageTwo input").each(function () {
    if ($(this).val() == "") {
      x++;
    }
  });
  if (x > 0) {
    $.alert("请填写完整信息");
  } else {
    var specCombinationArr = specCombination.substr(0, specCombination.length - 1).split(",");
    for (var j = 0; j < specCombinationArr.length; j++) {
      if (j == 0) {
        $(".specInput").val(specCombinationArr[j]);
      } else {
        $(".spec-li").append('<li class="add-liSpec"> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">规格组' + (j + 1) + '</div> <div class="item-input" style="float: left;"> <input type="text"  value="' + specCombinationArr[j] + '" readonly="readonly"> </div><span class="iconfont icon-fanhuijianyou addSpec"></span></div> </div> </li> <div class="underline"></div>');
      }
    }
    $.toast("保存成功");
    setTimeout(function () {
      $("#pageOne").show();
      $("#pageTwo").hide();

    }, 100);

  }
}
$(function () {
  queryAddr();//查询地址
  $("#datetime-picker").datetimePicker({
    minDate: new Date()
  });
  //点击规格跳转到添加规格的页面
  $(".spec-li").click(function () {
    $("#pageOne").hide();
    $("#pageTwo").show();
  });
  var i = 1;//声明一个i标签
  //点击添加规格数组，在添加一行规格数组
  $(".addBtn").click(function () {
    i++;
    $("#pageTwo ul").append('<div class="add-li"><li style="margin-top: 10px;"> <div class="item-content"> <div class="item-media"><i class="icon icon-form-name"></i></div> <div class="item-inner"> <div class="item-title label">规格组' + i + '</div> <span class="delBtn">删除规格</span> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">商品规格</div> <div class="item-input"> <input type="text" class="p-color" placeholder="请填写商品规格颜色"> </div> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">市场价</div> <div class="item-input"> <input type="number" class="p-purchase" placeholder="请填写市场价" onkeyup="clearNoNum(this)"/> </div> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">现价</div> <div class="item-input"> <input type="number" class="p-now" placeholder="请填写商品现价" onkeyup="clearNoNum(this)"/> </div></div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">团购价</div> <div class="item-input"> <input type="text" class="p-group" placeholder="请填写商品团购价" onkeyup="clearNoNum(this)"/> </div> </div> </div> </li> <div class="underline"></div> <li> <div class="item-content"> <div class="item-media"><i class="icon icon-form-email"></i></div> <div class="item-inner"> <div class="item-title label">库存</div> <div class="item-input"> <input type="number" class="p-stock" placeholder="请填写商品库存" onkeyup="clearNoNum(this)"/ > </div> </div> </div> </li></div>');
    //点击删除规格数组，在删除一行规格数组
    $(".delBtn").click(function () {
      $(this).parents('.add-li').remove();
    });
  });

  //只能输入数字
  function clearNoNum(obj) {
    obj.value = obj.value.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符
    if (obj.value.indexOf(".") < 0 && obj.value != "") {//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
      obj.value = parseFloat(obj.value);
    }
  }
  //点击规格的保存或者返回键跳转到添加商品的页面
  $("#saveSpec").click(function () {
    querySpec();
    eachInput();
  });
  $(".spec-left").click(function () {
    $("#pageOne").show();
    $("#pageTwo").hide();
  });
  if(sessionStorage.role==2){
    $(".checkBtn").show();
  }
  $('.saveBtn').click(function () {
    var isSelf="";
    if(sessionStorage.role==2){
      isSelf= $(".check").prop("checked") == true ? "y" : "n";
    }else{
      isSelf="n";
    }
    //保存并提交申请
    var name = $('.p-name').val(),//商品名称
      images = $('#picurl').val(),//商品图片
      //picture=images.split(',')[0];
      time = $('#datetime-picker').val(),//商品开团时间
      garden = $('.addressDetail').text(),//小区
      address = $('.p-address').val(),//自提点
      linkMan = $('.addrname').text(),//联系人
      reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/,
      linkPhone = $('.tel').text(),//联系电话
      area = $('.area').text(),//省市区
      introduce = $('.p-textarea').val();//商品详细信息
    if (!name || !images || !specCombination || !specPurchasePrice || !specNowPrice || !specStock || !time || !garden || !address || !linkMan || !linkPhone || !area || !introduce) {
      $.alert('请填写完整信息!');
      return false;
    }
    var params = {
      name: name,
      isSelf:isSelf,
      images: images,
      specCombination: specCombination.slice(0, -1),
      specPurchasePrice: specPurchasePrice.slice(0, -1),
      specNowPrice: specNowPrice.slice(0, -1),
      specGroupPrice: specGroupPrice.slice(0, -1),
      specStock: specStock.slice(0, -1),
      startDate: time,
      garden: garden,
      address: address,
      linkMan: linkMan,
      linkPhone: linkPhone,
      area: area,
      introduce: introduce
    };
    return reg.test(linkPhone) ? Submit(params) : $.toast('手机格式不正确');

  });
  //点击预览跳转到预览的商品详情页面
  $(".previewBtn").click(function () {
    $("#pageThree").show();
    $("#pageOne").hide();
    //显示商品详情的图片
    var imgArr = $("#picurl").val().split(',');
    var imgHtml = '';
    if (imgArr.length == 1) {
      $(".swiper-wrapper").html('<div class="swiper-slide"><img src="/' + imgArr[0] + '"></div>');
    } else {
      for (var q in imgArr) {
        imgHtml += '<div class="swiper-slide"><img src="/' + imgArr[q] + '"></div>';
      }
      $(".swiper-wrapper").html(imgHtml);
      var mySwiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        autoplay: 5000,
        loop: true,
        paginationClickable: true
      });
    }
    //点击单独购买欲立即购买给出提示
    $("#singer,#joinus").click(function () {
      $.alert("预览商品不支持购买");
    });
    //点击去拼团给出提示
    $(".join").click(function () {
      $.alert("预览商品不支持去拼团");
    });
    //点击去拼团给出提示
    $(".share").click(function () {
      $.alert("预览商品不支持分享");
    });
    //点击首页给出提示
    $(".home").click(function () {
      $.alert("预览商品不支持跳转到首页");
    });
    //点击收藏给出提示
    $(".coll").click(function () {
      $.alert("预览商品不支持收藏");
    });
    //显示商品名称
    $(".goods-det .over-h1").text($('.p-name').val());
    //显示商品详情
    $(".goods-det .over-p").text($('.p-textarea').val());
    //显示地区
    $(".areaBtn").text($('.area').text());
    //显示小区
    $(".address-div").text($('.addressDetail').text());
  });

  //点击预览商品详情的地方返回
  $(".back").click(function () {
    $("#pageOne").show();
    $("#pageThree").hide();
  });
});

function pBack() {
  window.location.href = "/rest/front/product/toProductList";
}

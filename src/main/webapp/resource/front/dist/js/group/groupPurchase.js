/**
 * Created by cxw on 2017/12/18.
 */
function getGoods(state) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/product/selectGroupProductList",
        success: function (data) {
            var list = data.data, html = '', time;
            if (list != null && list != '') {
                $('.lack').hide();
                state == 0 ? ($('.new').attr('data-type', '1').children('.sort').attr('src', '/resource/front/dist/images/sort2.png'), list = list.reverse()) : ($('.new').attr('data-type', '0').children('.sort').attr('src', '/resource/front/dist/images/sort1.png'));
                $.each(list, function (i, e) {
                    var img = e.picture || e.images.split(',')[0];
                    var starHtml = querypirce(e.id);
                    html += '<ul class="underline good" data-id="' + e.id + '"  ><li class="flex goods-box" ><img src="/' + img + '" alt="" class="pic cell-right">' +
                        '<div class="cell-main"><a class="name over">' + e.name + '</a>' +
                        '<div class="hui flex middle cell-main">收益:<div class="flex star-box center">' + starHtml + '</div></div>' +
                        '<div class="timetip"><span class="time-div-after time-div l" >' + e.sellcount + '人付款</span></div><div ><span class="price xxxl spec-money orange" >&nbsp;' + e.specs[0].specGroupPrice + '</span></div>' +
                        '<div class="join" ><span class="share"  data-id="' + e.id + '" >分享</span><span class="buy" data-id="' + e.id + '">购买</span></div></div></li></ul>';
                });
            }
            $('.good-wrapper').html(html)
        }
    })
}

function queryHot() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/product/selectProductListBySellCount",
        success: function (data) {
            var list = data.data, html = '', time;
            if (list != null && list != '') {
                $('.lack').hide();
                $.each(list, function (i, e) {
                        var img = e.picture || e.images.split(',')[0];
                        var starHtml = querypirce(e.id);
                        html += '<ul class="underline good" data-id="' + e.id + '"  ><li class="flex goods-box" ><img src="/' + img + '" alt="" class="pic cell-right">' +
                            '<div class="cell-main"><a class="name over">' + e.name + '</a>' +
                            '<div class="hui flex middle cell-main">收益:<div class="flex star-box center">' + starHtml + '</div></div>' +
                            '<div class="timetip"><span class="time-div-after time-div l" >' + e.sellcount + '人付款</span></div><div ><span class="price xxxl spec-money orange" >&nbsp;' + e.specs[0].specGroupPrice + '</span></div>' +
                            '<div class="join" ><span class="share" data-id="' + e.id + '"  >分享</span><span class="buy" data-id="' + e.id + '">购买</span></div></div></li></ul>';
                });
            }
            $('.good-wrapper').html(html)
        }
    })
}

function queryWait() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/product/selectPassProduct",
        data: {checkStatus: 2},
        success: function (data) {
            var list = data.data, html = '', time;
            if (list != null && list != '') {
                $('.lack').hide();
                $.each(list, function (i, e) {
                    var img = e.picture || e.images.split(',')[0];
                    html += ' <ul class="underline good" data-id="' + e.id + '" data-type="1"><li class="flex goods-box" ><img src="/' + img + '" alt="" class="pic cell-right">' +
                        '<div class="cell-main"><a class="name over">' + e.name + '</a>' +
                        '<div class="timetip"><span class="time-div-after time-div l" >' + e.reserveNumber + '人预定&nbsp;&nbsp;&nbsp;</span><div class="person-div "><span class="price xxxl spec-money orange" >&nbsp;' + e.specs[0].specGroupPrice + '</span><span class="price time-div x" style="text-decoration:line-through;">' + e.specs[0].specPurchasePrice + '</span></div>' +
                        '<div class="join">去预定</div></div></li></ul>';
                    getTime(i, e.startDate, 0);
                });
            }
            $('.good-wait').html(html)
        }
    })
}

$(function () {
    $('.content').on('click', '.good', function () {
        var id = $(this).data('id');
        window.location.href = '/rest/front/product/toProductDetail?id=' + id;
    });
    $('.content').on('click', '.share', function (event) {
        var id = $(this).data('id');
        window.location.href = "/rest/front/user/toShare?id=" + id;
        event.stopPropagation()
    });
    getGoods(0);
    $('.new').click(function () {
        $('.good-wrapper,.good-wait').empty();
        getGoods();
    });
    $('.hot').click(function () {
        $('.good-wrapper,.good-wait').empty();
        queryHot();
    });
    $('.wait').click(function () {
        $('.good-wait').show();
        $('.good-wrapper,.good-wait').empty();
        queryWait();
    });
    $('.buttons-row').on('click', 'a', function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
});
/**获取缓存用户信息**/
$.ajax({
    type: "GET",
    url: "/rest/front/user/getSession",
    dataType: "json",
    success: function (data) {
        sessionStorage.id = data.data.user.id;//获取缓存用户的id
        sessionStorage.role = data.data.user.rid;//获取缓存用户的角色
        sessionStorage.rankID = data.data.user.rankID;//获取缓存用户的等级ID
        sessionStorage.pid = data.data.user.pid;//获取缓存用户的pid确定是否有上级
    }
});

$(function () {
    queryOpenGroup();
    //点击添加，到添加商品列表页面
    $("#addProduct").click(function () {
        window.location.href = "/rest/front/product/toAddProduct"
    });
    $('.o-list').on('click','.openGroup', function () {
        var id=$(this).parents('.row').attr('data-oid');
        var state=$(this).parents('.row').attr('data-status');
        //if($(this).attr('data-status')!=4){
            window.location.href='/rest/front/product/toEditProduct?id='+id;
        //}
    });
    $('.o-list').on('click','.detailOrder', function () {
            window.location.href='/rest/front/order/toSendGoods'
    });
    //跳转到添加商品页面
  $('.lack').on('click','.addProductBtn', function () {
    window.location.href='/rest/front/product/toAddProduct'
  });
});
//查询列表
function queryOpenGroup() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/product/selectProductList",
        success: function (data) {
            var list = data.data,
                OpenGroupHtml = "",
                stateHtml = '',
                type,img;
            if(list!=null&&list!='') {
                $('.lack').hide();
                $.each(list, function (i, o) {
                    switch (o.checkStatus) {     //根据状态，设置订单状态及对应的按钮
                        case '4':   //开团中
                            stateHtml = '<div class="pspec"><div class="join-div"><span class="pe-span">' + o.sellcount + '人参加&nbsp;</span> <span class="time-span time' + i + '""></span></div></div><div class="price-div"><span class="price s-price orange">&nbsp;' + o.specs[0].specGroupPrice + '</span><span class="price time-div x line-span">' + o.specs[0].specPurchasePrice + '</span><span class="detailOrder">查看订单</span></div>';
                            type = 1;
                            break;
                        case '5':   //已经结束
                            stateHtml = '<div class="join-div pel-span div-p">' + o.sellcount + '参与</div> <div class="price1-div"><span class="detailOrder">查看订单 </span> <span class="openGroup">再次开团 </span></div>';
                            break;
                        case '1': //审核中
                            stateHtml = '<div class="join-div status-span">审核中</div><div class="price1-div openGroup">编辑商品 </div>';
                            break;
                        case '2'://已通过
                            stateHtml = '<div class="pspec"><div class="join-div"><span class="pe-span">' + o.sellcount + '人参加&nbsp;</span> <span class="time-span time' + i + '""></span></div></div><div class="price-div"><span class="price s-price orange">&nbsp;' + o.specs[0].specGroupPrice + '</span><span class="price time-div x line-span">' + o.specs[0].specPurchasePrice + '</span></div>';
                            type = 0;
                            break;
                        case '3':
                            stateHtml = '<div class="join-div"><span class="status-span">未通过</span></div><div class="price1-div openGroup">再次开团</div>';
                            break;
                    }
                    img = o.picture ? o.picture : o.images.split(',')[0];
                    OpenGroupHtml += '<li class="row row-list" data-oid="' + o.id + '" data-status="' + o.checkStatus + '"><div class="col-20"><img src="/' + img + '"/></div><div class="col-80">' +
                        ' <a class="p-list">' + o.name + '</a>';
                    OpenGroupHtml += stateHtml + '</div></li>';
                    o.isSelf=='n'&&getTime(i, o.startDate, type);
                });
            }
            $(".o-list").html(OpenGroupHtml);
        }
    })
}

function queryOrder() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/product/selectAllList',
        success: function (data) {
            var list = data.data, html = '', time, timeArr = [];
            if (list != '' || list != null) {
                $.each(list, function (i, e) {
                    if (e.checkStatus == '1') {
                        if (time != e.dayTime) {
                            time = e.dayTime;
                            timeArr = time.split('-');
                            html += '<div class="underline good-head" ><div class="header"><h1 class="" style="text-align: center;color:#eb392f;">' + timeArr[0] + '年' + timeArr[1] + '月' + timeArr[2] + '号</h1></div></div>'
                        }
                        html += '<ul class="underline good-body"  data-id="' + e.id + '"><li class="flex order "><label class="pull-left cell-left check-b">';
                        html += '</label><div class="cell-main  l"><div>商品名称: <span>' + e.name + '</span></div><div>开团日期: <span>' + e.startDate + '</span></div>' +
                            '<div>名字: <span class="name">' + e.linkMan + '</span></div>' +
                            '<div>收货地址: <span class="name">' + e.area + '&nbsp;' + e.garden + '</span></div><div>联系电话：<span class="tel">' + e.linkPhone + '</span></div></div>';
                        html +='<div class="send  cell-right">未审核</div></li></ul>';
                    }
                });

            }

            $('.group-box').html(html)
        }
    })
}

$(function () {
    queryOrder();
    //跳转到详情页面
    $('.group-box').on('click','.send', function () {
        var id=$(this).parents('.good-body').attr('data-id');
        window.location.href='/rest/front/product/toProductCheckDetail' + '?id=' + id;
    });
    //点击返回
    $("#back").click(function(){
        window.location.href = "/rest/front/user/toMyPage";
    })
});
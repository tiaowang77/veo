$(function () {
    var id = getQueryString('id');
    var merchantID="";
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {
            id: id
        },
        url: '/rest/front/product/selectDetail',
        success: function (data) {
            var list = data.data;
            var imgArr = list.images.split(',');
            var html = '';
            html += '<li class="goods-det clearfix"><h1 class="over xl goodsname"><b>' + list.name + '</b></h1></li>';
            $('.swiper-container').after(html);
            $(".openTime-li").text(list.startDate);
            $(".add-li").text(list.area + list.garden);
            $(".self-li").text(list.address);
            $(".name-li").text(list.linkMan);
            $(".phone-li").text(list.linkPhone);
            merchantID=list.merchantID;
            getPic(imgArr);
        }
    });

    //通过点击
    $(".saveBtn").click(function () {
        $.confirm('是否通过', function () {
            setPassStatus(id,merchantID);
        });
    });
    //点击不通过
    $(".previewBtn").click(function () {
        $.confirm('是否不通过', function () {
            noPassStatus(id,merchantID);
        });
    });
    //点击返回
    $(".back").click(function () {
        window.location.href = "/rest/front/product/toProductCheck";
    })

});
//查询图片
function getPic(arr) {
    var imgHtml = '';
    if (arr.length == 1) {
        $(".swiper-wrapper").html('<div class="swiper-slide"><img class="big" src="/' + arr[0] + '"></div>');
    } else {
        for (var i in arr) {
            imgHtml += '<div class="swiper-slide"><img class="big" src="/' + arr[i] + '"></div>';
        }
        $(".swiper-wrapper").html(imgHtml);
        var mySwiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            autoplay: 5000,
            loop: true,
            paginationClickable: true
        });
    }
}
//设置通过状态
function setPassStatus(id,merchantID) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {
            id: id,
            merchantID:merchantID,
            checkStatus: 2
        },
        url: '/rest/front/product/CheckProduct',
        success: function (data) {
            $.toast("成功通过");
            setTimeout(function () {
                window.location.href = "/rest/front/product/toProductCheck";
            }, 1000);
        }
    })
}

//设置不通过状态

function noPassStatus(id,merchantID) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {
            id: id,
            merchantID:merchantID,
            checkStatus: 3
        },
        url: '/rest/front/product/CheckProduct',
        success: function (data) {
            $.toast("不通过");
            setTimeout(function () {
                window.location.href = "/rest/front/product/toProductCheck";
            }, 1000);
        }
    })
}

/**
 * Created by cxw on 2017/12/18.
 */
function getProduct(id) {
    $.ajax({
        type: 'GET',
        url: "/rest/front/product/selectProductDetail",
        // url: "/rest/front/product/selectProductDetailCS",
        dataType: 'json',
        data: {
            id: id,
            // userID:285
        },
        success: function (data) {
            var list = data.data;
            var imgArr = list.images.split(',');
            var html = '';
            var specHtml = '', img;
            getPic(imgArr);
            html += '<li class="goods-det clearfix"><h1 class="over xl goodsname"><b>' + list.name + '</b></h1>' +
                '<div><h1 class="high-line l intro" >' + list.introduce + '</h1>' +
                '<div style="line-height: 1.5rem;"><span class="price red " style="font-size: 22px;"><b>' + list.specs[0].specGroupPrice + '</b>&nbsp;</span>' +
                '<s class="l hui price ">' + list.specs[0].specPurchasePrice + '</s>';
            html +=list.isSelf== "n"?'<div class="pull-right xl red"><span class=" ">剩余时间</span>&nbsp;<span class="time time-1 "></span></div>': '';
            html +='</div></li>';
            getTime(-1, list.startDate, 1);
            if(list.checkStatus==5){
                $('.buynow,.joingroup').unbind();
                $(document).off('click');
                $('#singer').hide();
                $('#joinus').css("background","#999")
            }
            (list.isSelf=='n')&&$('.select-group').prepend('<li class="flex select-box"><div class=" cell-main l">所在小区</div><div class="hui l cell-right adrs">'+list.address+'</div></li>');
            $('.spec-box').attr('addr', list.garden);
            $('.swiper-container').after(html);
            specHtml += '<div class="name over xl">' + list.name + ' </div>' +
                '<span class="red price xl">' + list.specs[0].specGroupPrice + '</span>' +
                '<div class="l">库存<span class="specnum">' + list.specs[0].specStock + '</span>件</div>';
            $('.spec-main').html(specHtml);
            img = list.picture ? '/' + list.picture : '/' + imgArr[0];
            $('.share').attr('data-src', img);
            $('.goodpic').attr('src', img);
            list.favoriteStatus == 'y' && $(".collect").removeClass('icon-kongxinxing').addClass('icon-xingxing').parent().css("color", "#eb392f").attr("data-stutus", "1");
            $('.page').attr('merchant', list.merchantID);
          $("#goodsDet-tab").html(list.productDetail);//商品详情显示
        }
    })
}
function getPic(arr) {
    var imgHtml = '';
    if (arr.length == 1) {
        $(".swiper-wrapper").html('<div class="swiper-slide"><img class="big" src="/' + arr[0] + '"></div>');
    } else {
        for (var i in arr) {
            imgHtml += '<div class="swiper-slide"><img class="big" src="/' + arr[i] + '"></div>';
        }
        $(".swiper-wrapper").html(imgHtml);
        var mySwiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            autoplay: 5000,
            loop: true,
            paginationClickable: true
        });
    }
}
function addFoot(id) {
    $.ajax({
        type: "POST",
        url: "/rest/front/foot/insertFoot",
        data: {
            productID: id,
            userID: sessionStorage.id
        },
        dataType: "json"
    });
}
function querySpec(id) {
    $.ajax({
        type: "GET",
        url: "/rest/front/spec/selectAllList",
        dataType: "json",
        data: {
            productID: id
        },
        success: function (data) {
            var list = data.data;
            var html = '';
            $.each(list, function (i, e) {
                html += '<div class="spec" specid=' + e.id + ' spec=' + e.specStock + ' price=' + e.specGroupPrice + ' >' + e.specCombination + '</div>'
            });
            $('.spec-btn').html(html).children().eq(0).addClass('active').siblings().removeClass('active');
        }
    })
}
function cancelColl(id) {
    $.ajax({
        type: "POST",
        url: "/rest/front/favorite/deleteJson",
        data: {
            userID: sessionStorage.id,
            productID: id
        },
        dataType: "json",
        success: function () {
            $(".collect").removeClass('icon-xingxing').addClass('icon-kongxinxing').parent().css("color", "#929292").attr("data-stutus", "0");
        }
    });
}
function addColl(id) {
    $.ajax({
        type: "POST",
        url: "/rest/front/favorite/insertJson",
        data: {
            userID: sessionStorage.id,
            productID: id
        },
        dataType: "json",
        success: function () {
            $(".collect").removeClass('icon-kongxinxing').addClass('icon-xingxing').parent().css("color", "#eb392f").attr("data-stutus", "1");
        }
    });
}
//
// function Recommend(id) {
//     $.ajax({
//         type: "GET",
//         url: "/rest/front/product/selectGroupProductList",
//         dataType: "json",
//         success: function (data) {
//             var list = data.data;
//             var html = '';
//             var i = 0;
//             var img;
//             if (list == '' || list == null) {
//                 $('.Recommend').hide()
//             } else {
//                 $.each(list, function (i, e) {
//                     img = e.picture || e.images.split(',')[0];
//                     html += '<li class="flex goods-box" data-id="' + e.id + '"><img src="/' + img + '" alt="" class="pic cell-right">' +
//                         '<div class="cell-main"><div class="name over">' + e.name + '</div>' +
//                         '<div class="timetip"><span class="time-div-after time-div l" >' + e.sellcount + '人参与&nbsp;&nbsp;&nbsp;</span><span class="time time' + i + '""></span><div class="person-div "><span class="price xxxl spec-money orange" >&nbsp;' + e.specs[0].specGroupPrice + '</span><span class="price time-div x" style="text-decoration:line-through;">' + e.specs[0].specPurchasePrice + '</span></div></div></li>';
//                     (e.isSelf=='n')&&getTime(i, e.startDate, 1);
//                 });
//                 $('.Recommend').append(html);
//             }
//         }
//     });
// }
// function getComment(id) {
//     $.ajax({
//         type: "GET",
//         url: "/rest/front/comment/selectCommentList",
//         data: {
//             productID: id,
//             rank:''
//         },
//         dataType: "json",
//         success: function (data) {
//             var list = data.data;
//             var html = '', imgArr;
//             var x = 0, y = 0, z = 0, k = 0;
//             if (list != null && list != '') {
//                 html += '<li class="head flex middle"><div class="cell-main"><span class="name xl">匿名昵称</span>';
//                 html +='<div class="hui l"><span class="time">' + list[0].createTime + '</span>&nbsp; 规格：<span class="spec">' + list[0].productSpecInfo + '</span></div></div></li>' +
//                     '<div class="assess-det xl">' + list[0].content + '</div>';
//                 if (list[0].picture) {
//                     imgArr = list[0].picture.split(",");
//                     html += '<li class="flex pic-box">';
//                     for (var i in imgArr) {
//                         html += '<img src="/' + imgArr[i] + '" alt="" class="img big">';
//                     }
//                     html += '</li>';
//                 }
//                 $('.assess-box').html(html);
//                 $.each(list, function (i,e) {
//                     e.rank == '好评' ? x++ : e.rank == '中评' ? y++ : z++;
//                     $('.assnum,.all').text(i + 1);
//                     if(e.picture){
//                         k++;
//                     }
//                 });
//                 $('.i').text(x);
//                 $('.j').text(y);
//                 $('.k').text(z);
//                 $('.x').text(k);
//             }
//         }
//     })
// }
// function getCommentAll(id,rank) {
//     $.ajax({
//         type: "GET",
//         url: "/rest/front/comment/selectCommentList",
//         data: {
//             rank:rank,
//             productID: id
//         },
//         dataType:'json',
//         success: function (data) {
//             var list = data.data;
//             var html = '', imgArr;
//             if (list != null && list != '') {
//                 $.each(list, function (i, e) {
//                     html += '<div class="comment-body"><ul class="underline"><li class="head flex middle"><div class="cell-main"><span class="name xl">匿名用户</span>';
//                     html +='<div class="hui l"><span class="time">' + e.createTime + '</span>&nbsp; 规格：<span class="spec">' + e.productSpecInfo + '</span></div></div></li>' +
//                         '<div class="assess-det xl">' + e.content + '</div></ul>';
//                     if (e.picture) {
//                         imgArr = e.picture.split(",");
//                         html += '<li class="flex pic-box">';
//                         for (var j in imgArr) {
//                             html += '<img src="/' + imgArr[j] + '" alt="" class="img">';
//                         }
//                         html += '</li></div>';
//                     }
//                 });
//                 $('.assess-all').html(html);
//             } else {
//                 $('.header a').removeAttr('href');
//             }
//         }
//     })
// }
function queryGroup(id,url,type) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: url,
        data: {
            id: id
        },
        success: function (data) {
            var list = data.data, html = '', showHtml = '', num,k=0,name,addr;
            $.each(list, function (i, e) {
                num = 2 - e.groupNumber * 1;
                if(num!=0){
                    name=e.userName.length<6?e.userName:e.userName.slice(0,6)+'…';
                    addr=e.addressDetail.length<16?e.addressDetail:e.addressDetail.slice(0,16)+'…';
                    showHtml += '<ul class="underline"><div class="inf-box flex middle item" data-group=' + e.id + '><img src="' + e.userIcon + '" alt="" class="pic ">' ;
                    showHtml += type==0?'<div class="cell-main  fn inf-a"><span class="name l">' +name + '</span><span class="l pull-right">还差<span class="pnum">' + num + '</span>人</span><div class="addre m">' + addr + '</div></div><div class="join  xl togroup">去拼团</div>' :'<div class="cell-right cell-main right fn inf-a"><span class="l">已预定</span><div class="addre m">' + e.addressDetail + '</div></div>';
                    showHtml +='</div></ul>';
                }

                if (i == 1) {
                    return false
                }
            });
            $.each(list, function (j, o) {
                k++;
                num = 2 - o.groupNumber * 1;
                if(num!=0){
                    name=o.userName.length<5?o.userName:o.userName.slice(0,4)+'…';
                    addr=o.addressDetail.length<13?o.addressDetail:o.addressDetail.slice(0,11)+'…';
                    html += '<ul class="underline"><div class="team-meb flex middle" data-group=' + o.id + '><img class="avatar pic mr4" src="' + o.userIcon + '" alt="" >';
                    html += type==0?'<div class="cell-main  fn "><span class="name l">' + name + '</span><span class="l pull-right cell-right">还差<span class="pnum">' + num + '</span>人</span><div class="addre m oneline mt4">' +addr + '</div></div><div class=" xl togroup btn">去拼团</div>' :'<div class="cell-right cell-main right fn inf-a"><span class="l">已预定</span><div class="addre m">' + o.addressDetail + '</div></div>';
                    html +='</div></ul>';
                }
                $('.groupnum').text(j + 1);
            });
            if (k<3) {
                $('.groupmore').unbind();
                $('.groupmore h1').siblings().hide();
            }
            $('.group').append(showHtml);
            $('.box-team').html(html);
        }
    })
}
$(function () {
    var id = getQueryString('id');
    var type = getQueryString('Type');
    var state=getQueryString('state');
    var input = $('.number');
    var num = input.text(),
        url='/rest/front/order/selectGroupOrder',
        rank='',
        way=0;
    // if(state!=0){
    //     $('#singer').hide();
    //     $('#joinus span').text('团购价预定');
    //     $('.groupmore .text').text('人预定');
    //     url='/rest/front/order/selectReserveGroupOrder';
    //     way=1;
    // }

    getProduct(id);
    addFoot(id);
    querySpec(id);
    // getComment(id);
    // Recommend(id);
    // getCommentAll(id,rank);
    queryGroup(id,url,way);
    setTimeout(function () {
        if (type == 1) {
            toshare()
        }
    }, 1000);
    $('.share').click(function () {
        toshare()
    });
    // $('.grade-wrapper').on('click','.btn-grade', function () {
    //     rank=$(this).attr('data-type');
    //     $('.comment-body').hide();
    //     $(this).attr('data-url')?$('.pic-box').parents('.comment-body').show():getCommentAll(id,rank);
    //
    // });
    $('.groupmore').click(function () {
        $('.expand,.jointeam').show()
    });
    $('.expand,.close,.close-box').click(function () {
        $('.goodsnumb').text($('.number').text());
        $('.expand,.jointeam,.spec-box,.pictip,.bigpic').hide()
    });
    $(document).on('click', '.select-spec,#joinus,.togroup', function () {
        var group = $(this).parents('.inf-box').attr('data-group');
        $('.joingroup').attr('group', group);
        $('.expand,.spec-box').show();
    });
    $('.spec-btn').on('click', '.spec', function () {
        var price = $(this).attr('price');
        var spec = $(this).attr('spec');
        $(this).addClass('active').siblings().removeClass('active');
        $('.specnum').text(spec);
        $('.spec-main .price').text(price)
         });
    $(".coll").click(function () {
        $(this).attr("data-stutus") == 1 && id ? cancelColl(id) : addColl(id);
    });
    $('.home').click(function () {
        window.location.href = '/rest/front/product/toGroupPurchase';
    });
    $('.numsel').on("click", ".reduce", function () {
        num--;
        num = 1 > num ? 1 : num;
        input.text(num);
    });
    $('.numsel').on("click", ".add", function () {
        num++;
        var spec = $('.specnum').text();
        if (spec < num) {
            num = spec;
            $.toast("库存不足");
        }
        input.text(num)
    });
    $('.joingroup').click(function () {
        var specId = $('.spec-btn').children('.active').attr('specid');
        var price =  $('.spec-main .price').text();
        var addr = $('.spec-box').attr('addr');
        var isGroup = $(this).attr('type') ? 'n' : 'y';
        var group = $(this).attr('group')||0;
        var merchantID = $('.page').attr('merchant');
        window.location.href = '/rest/front/order/toProductOrderDetail?id=' + id + '&num=' + num + '&specId=' + specId + '&price=' + price + '&addr=' + addr + '&isGroup=' + isGroup + '&groupOrderID=' + group + '&merchantID=' + merchantID+'&state='+state;
    });
    $(document).on('click', '.goods-box', function () {
        var id = $(this).attr('data-id');
        window.location.href = '/rest/front/product/toProductDetail?id=' + id;
    });
});
//返回键
function indexBack(){
    window.location.href="/rest/front/product/toGroupPurchase";
}

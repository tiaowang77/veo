/**
 * Created by cxw on 2017/12/21.
 */
function queryOrder() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url:'/rest/front/order/selectSendOrderByDay',
        success: function (data) {
            var list = data.data, html = '', time, timeArr = [];
            if(list!=''&&list!=null){
                $.each(list, function (i, e) {
                    if (e.status == 'pass' || e.status == 'finish'||e.status == 'send') {
                    if (time != e.dayTime) {
                        time = e.dayTime;
                        timeArr = time.split('-');
                        html += '<div class="underline good-head" ><div class="header clearfix overh"><h1 class=" pull-left cell-left xl group-t overh">' + timeArr[0] + '年' + timeArr[1] + '月' + timeArr[2] + '号团</h1><div class="sendall pull-right cell-right" data-date="'+ e.dayTime+'">一键发货</div></div></div>'
                    }
                        html += '<ul class="underline good-body" data-state="'+e.status+'" data-id="'+e.id+'" data-date="'+e.dayTime+'"><li class="flex order "><label class="pull-left cell-left check-b">' ;
                        html += '</label><div class="cell-main  l"><div>商品名称: <span>' + e.orderdetails[0].productName + '</span></div><div>规格: <span>' + e.orderdetails[0].specInfo + '</span> &nbsp; 数量: <span>' + e.orderdetails[0].number + '</span></div>' +
                            '<div>收货人: <span class="name">' + e.addressName + '</span></div>' +
                            '<div>收货地址: <span class="name">' + e.addressArea + '&nbsp;' + e.addressDetail + '</span></div><div>联系电话：<span class="tel">' + e.addressPhone + '</span></div></div>';
                        html += e.status == 'pass'?'<div class="send  cell-right">发货</div>':'';
                        html +='</li></ul>';
                    }
                });
            }
            $('.group-box').html(html)
        }
    })
}
function sendOne(id){
    $.ajax({
        type:'POST',
        dataType:'json',
        url:'/rest/front/order/updateJson',
        data:{
            id:id,
            status:"send"
            },
        success: function () {
            location.reload()
        }
    })
}
function sendAll(ids){
    $.ajax({
        type:'POST',
        dataType:'json',
        url:'/rest/front/order/updateOrders',
        data:{
            ids:ids
        },
        success: function () {
            location.reload()
        }
    })
}
$(function () {
    queryOrder();
    $('.tab-btn').on('click','a',function(){
        $(this).addClass('active').siblings().removeClass('active');
        var status = $(this).attr('data-state');
        var head=$('.good-head');
        $('.good-body,.good-head').show();
        switch (status){
            case 'over':   $('.good-body[data-state="pass"]').hide();break;
            case 'pass':  $('.good-body:not([data-state="pass"])').hide();break;
        }
        for(var i=0; i<head.length;i++){
            if($(head[i]).next(".good-body").css("display") == "none"){
                $(head[i]).hide()
            }
        }
    });
    $('.group-box').on('click','.send', function () {
       var id=$(this).parents('.good-body').attr('data-id');
        sendOne(id);
    });
    $('.group-box').on('click','.sendall', function () {
        var date=$(this).attr('data-date');
        var dateArr=$('.good-body[data-date="'+date+'"]'),ids=[];
        for(var i=0;i<dateArr.length;i++){
            ids[i]=$(dateArr[i]).attr('data-id')
        }
        sendAll(ids);
    })
});
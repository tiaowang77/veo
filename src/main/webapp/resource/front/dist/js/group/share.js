//微信接口初始化
/**获取缓存用户信息**/
$.ajax({
    type: "GET",
    url: "/rest/front/user/getSession",
    dataType: "json",
    success: function (data) {
        sessionStorage.id = data.data.user.id;//获取缓存用户的id
        sessionStorage.role = data.data.user.rid;//获取缓存用户的角色
    }
});
function toshare(){
    var price = $('#joinus').find('.price').text(),
        name = $('.goodsname').text(),
        introduce = $('.intro').text(),
        img =  $('.share').attr('data-src'),
        titleText = '[仅剩下1个名额]我花了' + price + '元拼了' + name,
        id=getQueryString('id');
    $(".expand,.pictip").show();
    getShare(id,titleText,introduce,img)
}
function getShare(id,titleText,introduce,img) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/user/toScan',
        data: {
            pageUrl: location.href.split('#')[0]
        },
        success: function (res) {
            if (res.success) {
                wx.config({
                    debug: false, // 开启调试模式
                    appId: res.data.appId, // 必填，公众号的唯一标识
                    timestamp: res.data.timestamp, // 必填，生成签名的时间戳
                    nonceStr: res.data.noncestr, // 必填，生成签名的随机串
                    signature: res.data.signature,// 必填，签名
                    jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']// 功能列表，我们要使用JS-SDK的什么功能
                });
                wx.ready(function () {
                    var link = '/rest/front/product/toProductDetail' + '?id=' + id;
                    var protocol = window.location.protocol;
                    var host = window.location.host;
                    var imgShare = '/' + img;
                    //分享朋友圈
                    wx.onMenuShareTimeline({
                        title: titleText, // 分享标题
                        desc: introduce, // 分享描述
                        link: protocol + '//' + host + link,
                        imgUrl: protocol + '//' + host + imgShare, // 自定义图标
                        trigger: function (res) {
                            // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回.
                        },
                        success: function (res) {
                            $(".pictip").hide();//阴影部分显示
                            $(".share-body").show();//分享成功弹出框显示
                            //点击分享成功后跳转点击前往商城首页
                            $(".continue-share").click(function () {
                                $(".share-body").hide()
                            });
                            $(".span-go").click(function () {
                                window.location.href = '/rest/front/product/toGroupPurchase';
                            })
                        },
                        cancel: function (res) {
                        },
                        fail: function (res) {
                            $.alert('失败');
                        }
                    });
                    wx.onMenuShareAppMessage({
                        title: titleText, // 分享标题
                        desc: introduce, // 分享描述
                        link: protocol + '//' + host + link,
                        imgUrl: protocol + '//' + host + imgShare, // 自定义图标
                        success: function (res) {
                            $(".pictip").hide();//阴影部分显示
                            $(".share-body").show();//分享成功弹出框显示
                            //点击分享成功后跳转点击前往商城首页
                            $(".continue-share").click(function () {
                                $(".share-body").hide()
                            });
                            $(".span-go").click(function () {
                                window.location.href = '/rest/front/product/toGroupPurchase';
                            })
                        },
                        cancel: function (res) {
                        },
                        fail: function (res) {
                            $.alert('失败');
                        }
                    });
                })

            } else {
                $.alert(res.message);
            }
        }
    });
}

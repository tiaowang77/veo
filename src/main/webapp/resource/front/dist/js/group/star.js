function querypirce(id) {
    var ret='';
    $.ajax({
        type:'GET',
        data:{userID:sessionStorage.id,productID:id},
        async:false,
        dataType:'json',
        url:'  /rest/front/product/selectProductRank',
        success:function (data) {
            var arr=data.data.priceList;
            var num=data.data.userRank*1;
            for(var i=0;i<3;i++){
                var className=num>i?'star-on':'star-off';
                ret+='<span class="cell-main "><div class="'+className+' auto"></div><div class="mt4 profit red">'+arr[i]+'</div></span>'
            }
        }
    });
    return ret;
}

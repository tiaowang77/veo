/**
 * Created by cxw on 2017/12/17.
 */
function queryOrder(type) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/order/selectOrderList",
        success: function (data) {
            var list = data.data;
            var orderHtml = "";
            var stateHtml = '';
            var state = "";
            var num = 0;
            if (list != '' && list != null) {
                $.each(list, function (i, o) {
                    switch (o.status) {     //根据状态，设置订单状态及对应的按钮
                        case 'init':
                            state = "等待买家付款";
                            stateHtml = '<div class="o-list-total upline"><div class="o-list-div"><span data-url="" class="paymentBtn">付款</span><span data-url="" class="cancelBtn cancelBtn1" data-id="' + o.id + '">取消订单</span></div></div>';
                            break;
                        case 'pass':
                            state = "等待卖家发货";
                            stateHtml = '';
                            break;
                        case 'send':
                            state = "等待买家收货";
                            stateHtml = '<div class="o-list-total upline"><div class="o-list-div"><span data-url="" class=" sBtn">确认收货</span></div></div>';
                            break;
                        case 'finish':
                            state = "订单已完成";
                            stateHtml = '';
                            break;
                    }
                    orderHtml += '<div class="o-list-content underline" product="'+ o.productCheckStatus+'" data-oid="' + o.id + '" data-status="' + o.status + '" style="margin-bottom:6px;"> <div class="o-list-item" remark="' + o.remark + '"> <div class="o-s-head"><i class="iconfont icon-dingdan"></i><span>订单号:' + o.id + '</span><span class="status" style="float: right;font-size: 14px;color: #ff5000">' + state + '</span></div><ul class="o-list upline">';
                    $.each(o.orderdetails, function (j, e) {
                        var img = e.picture || e.images.split(',')[0];
                        orderHtml += '  <li class="row product-row" data-productID="' + e.productID + '"><div class="col-20"><img class="goods-pic" src="/' + img + '"> </div><div class="col-80"><div class="p-list">' + e.productName + '</div>' + e.specInfo + ' <div class="pspec"><div class="s-price">' + e.price + '</div><div class="s-count">' + e.number + '</div> </div> </div> </li> ';
                        //数量相加
                        num = e.number;

                    });
                    orderHtml += stateHtml + '</div></div>';
                });
            }
            $("#all-tab").html(orderHtml);
            setType(type);
        }
    })
}
function getShare(type) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/order/selectShareOrder",
        success: function (data) {
            var list = data.data;
            var shareHtml = "";
            if (list != '' && list != null) {
                $('.lack').hide();
                $.each(list, function (a, b) {
                    if (b.status == 'pass' || b.status == 'send' || b.status == 'finish') {
                        shareHtml +='<div class="o-list-content "  data-oid="' + b.id + '" data-status="' + b.status + '" remark="' + b.remark + '"><li class="head flex middle"><img src="'+ b.userIcon+'" alt="" class="pic cell-right"><div class="cell-main"><span class="name xl">'+ b.userName+'</span></div></li>'
                        $.each(b.orderdetails, function (c, d) {
                            var img = d.picture || d.images.split(',')[0];
                            shareHtml += '  <li class="row product-row upline" data-productID="' + d.productID + '"><div class="col-20"><img class="goods-pic" src="/' + img + '"> </div>' +
                                '<div class="col-80"><div class="p-list">' + d.productName + '</div>' + d.specInfo + ' <div class="pspec"><div class="s-price">' + d.price + '</div><div class="s-count">' + d.number + '</div> </div> </div> </li> ';
                            shareHtml += '<div class="o-list-total upline"><div class="o-list-div"><span data-url="" class="sharefriend pull-right cell-right" data-id="' + d.productID + '" data-img="' + d.picture + '" data-price="' + d.price + '" data-name="' + d.productName + '" data-introduce="' + d.introduce + '">邀请好友拼单</span></div></div></div></div>';
                        });
                    }
                });
            }
            $("#share").html(shareHtml);
            setType(type);
        }
    })
}
function setType(type) {   //从我的页面带入的type
    switch (type) {
        case 'pass':
            $('#send-tab').trigger("click");
            break;
        case 'init':
            $('#payment-tab').trigger("click");
            break;
        case 'send':
            $('#harvest-tab').trigger("click");
            break;
        case 'finish':
            $('#complete-tab').trigger("click");
            break;
        case 'share':
            $('#share-tab').trigger("click");
            break;
        default:
            $("#all-tab-btn").trigger("click");
            break;
    }
}
function cancelOrd(id) {
    $.ajax({
        type: 'POST',
        url: '/rest/front/order/deleteJson',
        dataType: 'json',
        data: {id: id},
        success: function () {
            window.location.reload();
        }
    })
}
function back() {
    window.location.href = '/rest/front/user/toMyPage';
}
$(function () {
    var type = getQueryString('status');
    queryOrder(type);
    getShare(type);
    $('.buttons-tab').on('click', '.button', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var status = $(this).attr('data-status');
        $('.o-list-content').show();
        if ($(this).attr('data-status') != 'share') {
            $('#share').hide();
            $('#all-tab').show()
        }
        if ($(this).attr('data-status') != 'all' && $(this).attr('data-status') != 'share') {
            $('.o-list-content:not([data-status="' + status + '"])').hide();
        }
    });
    $('#all-tab').on('click', '.paymentBtn,.sBtn,.product-row', function () {
        var oid = $(this).parents('.o-list-content').attr('data-oid');
        var id = $(this).parents('.o-list-total').siblings('.product-row').attr('data-productID');
        var remark = $(this).parents('.o-list-item').attr('remark');
        var product=$(this).parents('.o-list-content').attr('product');
        var status = $(this).parents('.o-list-content').attr('data-status');
        if (!remark || remark == 'null') {
            remark = 0
        }
        window.location.href = '/rest/front/order/toOrderDetail' + '?orderID=' + oid + '&remark=' + remark + "&id=" + id + "&status=" + status + "&product=" +product;
    });
    $(document).on('click', '.cancelBtn1', function () {  //id传后端
        var id = $(this).attr('data-id');
        $.confirm('是否取消订单', function () {
            cancelOrd(id)
        })
    });
    $('#share-tab').click(function () {
        $('#share').show();
        $('#all-tab').hide()
    });
    //点击邀请好友拼单跳转到商品详情页面
    $(document).on('click', '.sharefriend', function () {
        var id = $(this).attr("data-id");
        window.location.href = '/rest/front/product/toProductDetail' + '?id=' + id + "&Type=" + 1;
    });

});

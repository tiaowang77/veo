/**
 * Created by cxw on 2017/12/17.
 */
function queryOrder(orderID) {
    $.ajax({
        type: "GET",
        url: "/rest/front/order/selectOrderDetail",
        dataType: "json",
        data: {
            orderID: orderID
        },
        success: function (data) {
            var list = data.data;
            if (list) {
                $('.lack').hide();
                getOrder(list);
            }
        }
    })
}
function getOrder(list) {
    var html = '';
    var infHtml = '',price=0;
    $.each(list, function (i, e) {
        var img= e.picture|| e.images.split(',')[0];
        html += ' <div class="underline"><li class="goods flex middle cell-left" data-product=' + e.productID + '>' +
            '<img src="/' +img+ '" alt="" class="pic"><div class="cell-main">' +
            '<div class="name">' + e.productName + '</div><div class="spec">' + e.specInfo + '</div>' +
            '<div class="price orange">' + e.price + '</div></div>' +
            '<div class="goods-num">&times;<span class="num">' + e.number + '</span></div> </li></div>';
        price+=e.price;
    });
    $('.goodlist').after(html);
    $('.totalprice').text(price);
    infHtml += '<li class="flex inf"><div class="inf-l">订单编号</div><div class="inf-r cell-main right">' + list[0].orderID + '</div></li>' +
        '<li class="flex inf"><div class="inf-l">下单时间</div><div class="inf-r cell-main right">' + list[0].createTime + '</div></li>';
    $('.baseinf').prepend(infHtml);
   return getState(list[0].orderStatus,list[0].isComment);
}
function getState(type,check) {
    var html = '';
    switch (type) {
        case 'init':
            $('.state').text('待付款');
            $('.paynew').show();
            html += '<span class="cancel btn-box">取消订单</span>';
            break;
        case 'pass':
            $('#tool-bar').hide();
            $('.state').text('待发货');
            html += '';
            break;
        case 'send':
            $('.tosure').show();
            $('.state').text('待收货');
            html += '';
            break;
        case 'finish':
            check=='y'? ($('.state').text('已评价'),$('#tool-bar').hide())
                :($('.state').text('待评价'), $('.tocomment').show());
            html += '';
            break;
    }
    $('.link-box').html(html);
}
function queryAddr(id, status) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/order/selectOrderAddress",
        data: {
            id: id
        },
        success: function (data) {
            var list = data.data;
            var inf = '';
            var totalpirce = list.ptotal;
            var wallet = list.deduction;
            if (list) {
                var html = '<span class="addrname cell-right">' + list.addressName + '</span><span class="tel">' + list.addressPhone + '</span>' +
                    '<div class="address overhide"><span class="area">' + list.addressArea + '</span>&nbsp;<span class="addressDetail">' + list.addressDetail + '</span></div>';
                $('.addr-main').html(html);
            }
            if (status != 'init'&&list.expressNum&&list.expressCompany) {
                inf += '<li class="flex inf"><div class="inf-l">快递单号</div><div class="inf-r cell-main right">' + list.expressNum + '</div></li>' +
                    '<li class="flex inf"> <div class="inf-l">配送方式</div> <div class="inf-r cell-main right">' + list.expressCompany + '</div></li>';
                $('.remark-box').before(inf);
            }
            $('.wallet').text(wallet);
            $('.actual').text(totalpirce);
        }
    })
}
function cancelOrd(id) {
    $.ajax({
        type: "POST",
        url: "/rest/front/order/deleteJson",
        dataType: "json",
        data: {
            id: id
        },
        success: function () {
            $.toast("删除成功");
            setTimeout(function () {
                window.location.href = "/rest/front/order/toOrderList";
            }, 100);
        }
    })
}
function finishOrd(id) {
    $.ajax({
        type: "POST",
        url: "/rest/front/order/updateJson",
        dataType: "json",
        data: {
            id: id,
            status: "finish"
        },
        success: function () {
            $.toast("确认成功");
            setTimeout(function () {
                window.location.href = "/rest/front/order/toOrderList";
            }, 100);
        }
    })
}
function back() {
    window.location.href = "/rest/front/order/toOrderList?status=" + status;
}
$(function () {
    var postData = GetRequest(),
        orderID = postData.orderID,
        status = postData.status,
        remark = postData.remark,
        product=postData.product;
    queryOrder(orderID);
    remark!=0&& $('.remark').text(remark);
    queryAddr(orderID, status);
    $('.link-box').on('click', '.cancel', function () {
        $.confirm("是否取消订单", function () {
            cancelOrd(orderID);
        })
    });
    $('.tosure').click(function () {
        $.confirm("是否确认收货", function () {
            finishOrd(orderID);
        })
    });
    $(document).on('click','.goods', function () {
      var id= $(this).attr('data-product');
        window.location.href='/rest/front/product/toProductDetail?state=0&id='+id;
    });
    $('.tocomment').click(function () {
        window.location.href = '/rest/front/comment/toCommentList';
    });
    $('.paynew').click(function () {
        var product = $('.goods');
        var num = $('.num');
        var spec = $('.spec');
        var data;
        var productID = '', numList = '', specList = '';
        for (var i = 0; i < product.length; i++) {
            productID += $(product[i]).attr('data-product') + ",";
        }
        productID = productID.slice(0, -1);
        for (var i = 0; i < num.length; i++) {
            numList += $(num[i]).text() + ",";
        }
        numList = numList.slice(0, -1);
        for (var i = 0; i < spec.length; i++) {
            specList = specList + $(spec[i]).text() + ",";
        }
        specList = specList.slice(0, -1);
        data={
            id: orderID,
            productIDs: productID,
            productAmount: numList,
            specIDs: specList,
            remark: remark,
            transType: "1",
            ptotal: $('.actual').text(),//获取商品总价格
            addressName: $('.addrname').text(),//获取购物车地址收货人姓名
            addressPhone: $('.tel').text(),//获取购物车地址收货人手机号
            addressArea: $('.area').text(),//获取购物车地址
            addressDetail: $(".addressDetail").text(),//获取购物车地址具体地址
            type: "order",
            submitType: "order",
            pid:sessionStorage.pid
        };
       toPay(data);
    })
    if(product==5){
        $('.paynew').unbind().css("background","#999");
    }
});
function toPay(data){
    $.ajax({
        type: "GET",
        url: "/rest/front/orderpay/getPrepayId",
        dataType: "json",
        data:data,
        success: function (data) {
            var appData = data.data;
            if (data.success) {
                function onBridgeReady() {
                    WeixinJSBridge.invoke(
                        'getBrandWCPayRequest', {
                            "appId": appData.appId,
                            "timeStamp": appData.timeStamp,
                            "nonceStr": appData.nonceStr,
                            "package": appData.package,
                            "signType": appData.signType,
                            "paySign": appData.paySign
                        },
                        function (res) {
                            if (res.err_msg == "get_brand_wcpay_request:ok") {
                                    window.location.href = "/rest/front/order/toOrderList?status=pass";//支付成功后跳转到我的订单页面，状态是待发货
                            } else if (res.err_msg == "get_brand_wcpay_request:cancel") {
                                window.location.href = "/rest/front/order/toOrderList?status=init";//取消支付后跳转到我的订单页面，状态是待付款
                            } else if (res.err_msg == "get_brand_wcpay_request:fail") {
                                alert("调用支付异常，响应为：get_brand_wcpay_request:fail");
                            }
                        }
                    );
                }

                if (typeof WeixinJSBridge == "undefined") {
                    if (document.addEventListener) {
                        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                    } else if (document.attachEvent) {
                        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
                    }
                } else {
                    onBridgeReady();
                }
            } else {
                $.alert(data.message);
            }
        }
    })
}

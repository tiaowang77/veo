/**
 * Created by cxw on 2017/12/19.
 */
function queryAddr() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/rest/front/address/selectDefault",
        success: function (data) {
            var list = data.data;
            if (list) {
                var html = '<span class="addrname cell-right">' + list.name + '</span><span class="tel">' + list.phone + '</span>' +
                    '<div class="address overhide"><span class="area">' + list.area + '</span>&nbsp;<span class="addressDetail">' + list.address  + " " + list.houseNumber + '</span></div>';
                $('.addr-main').html(html);
                $('.addr').click(function () {
                    window.location.href = "/rest/front/address/toAddressList?addressType=" + 3;
                })
            } else {
                $('.addr').click(function () {
                    window.location.href = '/rest/front/address/toAddressAdd';
                })
            }
        }
    })
}
function queryOrder(id, num, price,addr) {
    $.ajax({
        type: 'GET',
        url: '/rest/front/product/selectOrderDetail',
        dataType: 'json',
        data: {
            id: id
        },
        success: function (data) {
            var list = data.data, html = '',img;
            if (list.productID) {
                img=list.picture||list.productImages.split(',')[0];
                html += '<div class="underline"> <li class="goods flex middle cell-left" data-product="' + list.productID + '"><img src="/' + img + '" alt="" class="pic">' +
                    '<div class="cell-main"><div class="name">' + list.productName + '</div><div class="spec">' + list.specCombination + '</div>' +
                    '<div class="price orange goodspirce">' + price + '</div></div><div class="goods-num">&times;<span class="num">' + num + '</span></div></li> </div>';
                $('.orderlist').html(html);
                list.productIsSelf=='n'&&$('.orderinf').prepend('<li class="inf flex"><div class="text cell-left">自提点</div><div class="cell-main xl addrone">'+addr+'</div></li>');
                totalPrice();
            }
        }
    })
}
function queryMoney() {
    $.ajax({
        type: 'GET',
        url: '/rest/front/user/selectUser',
        dataType: 'json',
        success: function (data) {
            if (data.data && data.data.walletMoney) {
                var wallet = Number(data.data.walletMoney).toFixed(2);
                $('.money').attr('placeholder', '可用余额为' + wallet);
                $('.title').attr('data-money', wallet);
            }
        }
    });
}
function totalPrice() {
    var pList = $('.goodspirce');
    var nList = $('.num');
    var money=$('.money').val();
    var totalPrice = '';
    for (var i = 0; i < pList.length; i++) {
        totalPrice += $(pList[i]).text() * $(nList[i]).text();
    }
    totalPrice = Number(totalPrice-money).toFixed(2);
    $('.totalprice').text(totalPrice);
}
function getPercent(){
    $.ajax({
        type: 'GET',
        url: '/rest/front/user/selectPercent',
        dataType: 'json',
        success: function (data) {
            $('.title').attr('money',data.data);
        }
    });
}

$(function () {
    var postData = GetRequest(),
        specId = postData.specId,
        num = postData.num,
        price = postData.price,
        addr = postData.addr,
        state=postData.state,
        isGroup = postData.isGroup,
        id=postData.id,
        groupOrderID = postData.groupOrderID,
        merchantID = postData.merchantID;
    queryAddr();
    //查询
    queryMoney();
    getPercent();
    queryOrder(specId, num, price,addr);

    $(document).on('click','.goods', function () {
        var id= $(this).attr('data-product');
        window.location.href='/rest/front/product/toProductDetail?state=0&id='+id;
    });
    $('.money').blur(function () {
        var wallet = $('.title').attr('data-money');
        var money=$('.totalprice').text();
        if ($(this).val() > wallet) {
            $.toast('请输入合适金额');
            $(this).val('');
        }
        if($(this).val() > money){
            $.toast('使用余额不能超过总金额');
            $(this).val('');
        }
        totalPrice();
    });
    $('.texta').keyup(function () {
        var content = $('.texta').val();
        if (content.length > 50) {
            $('.texta').val(content.substr(0, 50));
        }
    });
//返回键，到商品详情
    $("#back").click(function(){
        if(type!=null||type!=""){
            window.location.href='/rest/front/product/toProductDetail?id=' + id + '&type=' + type;
        }else{
            window.location.href='/rest/front/product/toGroupPurchase';
        }

    });
    $('.evaluate').click(function () {
        if(!$('.addrname').text()||!$('.tel').text()||!$('.area').text()||!$(".addressDetail").text()){
            $.toast("请填写地址信息");
            return false;
        }
        var product = $('.goods');
        var num = $('.num');
        var spec = $('.spec');
        var money=$('.money').val()||0;
        var percent=$('.title').attr('money');
        var ptotal= $('.totalprice').text();
        var rebate=Number(ptotal*percent).toFixed(2);
        var type=0;
        var data;
        var productID = '', numList = '', specList = '';
        for (var i = 0; i < product.length; i++) {
            productID += $(product[i]).attr('data-product') + ",";
        }
        productID = productID.slice(0, -1);
        for (var i = 0; i < num.length; i++) {
            numList += $(num[i]).text() + ",";
        }
        numList = numList.slice(0, -1);
        for (var i = 0; i < spec.length; i++) {
            specList = specList + $(spec[i]).text() + ",";
        }
        specList = specList.slice(0, -1);
        if(ptotal==0){
            ptotal=money;
            type=1;
        }
        state=state==1? 'y': 'n';
        data = {
            productIDs: productID,
            productAmount: numList,
            specIDs: specList,
            transType: "1",
            remark: $('.texta').val(),
            ptotal: ptotal,
            addressName: $('.addrname').text(),
            addressPhone: $('.tel').text(),
            addressArea: $('.area').text(),
            addressDetail: $(".addressDetail").text(),
            type: "order",
            submitType: "order",
            pid: sessionStorage.pid,
            supUserID:sessionStorage.pid,
            isGroup: isGroup,
            groupOrderID: groupOrderID,
            merchantID:merchantID,
            deduction:money,
            rebate:rebate,
            isReserve:state
            };
        if(type==0){
            toPay(data)
        }else{

           todeduction(data)
        }
    })
});
function todeduction(data){
    $.ajax({
        type: "POST",
        url: "/rest/front/order/deductionOrder",
        dataType: "json",
        data: data,
        success: function () {
            var id=$('.goods').attr('data-product');
            window.location.href = '/rest/front/product/toProductDetail?Type=1&id=' + id;
        }
    })
}
function toPay(data) {
    $.ajax({
        type: "GET",
        url: "/rest/front/orderpay/getPrepayId",
        dataType: "json",
        data: data,
        success: function (data) {
            var appData = data.data;
            var id=$('.goods').attr('data-product');
            if (data.success) {
                function onBridgeReady() {
                    WeixinJSBridge.invoke(
                        'getBrandWCPayRequest', {
                            "appId": appData.appId,
                            "timeStamp": appData.timeStamp,
                            "nonceStr": appData.nonceStr,
                            "package": appData.package,
                            "signType": appData.signType,
                            "paySign": appData.paySign
                        },
                        function (res) {
                            if (res.err_msg == "get_brand_wcpay_request:ok") {
                                window.location.href = '/rest/front/product/toProductDetail?Type=1&id=' + id;//支付成功后跳转到我的订单页面，状态是待发货
                            } else if (res.err_msg == "get_brand_wcpay_request:cancel") {
                                window.location.href = "/rest/front/order/toOrderList?status=init";//取消支付后跳转到我的订单页面，状态是待付款
                            } else if (res.err_msg == "get_brand_wcpay_request:fail") {
                                alert("调用支付异常，响应为：get_brand_wcpay_request:fail");
                            }
                        }
                    );
                }

                if (typeof WeixinJSBridge == "undefined") {
                    if (document.addEventListener) {
                        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                    } else if (document.attachEvent) {
                        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
                    }
                } else {
                    onBridgeReady();
                }
            } else {
                $.alert(data.message);
            }
        }
    })
}

/**
 * Created by cxw on 2017/12/16.
 */
function getCollection(){
    $.ajax({
        type:'GET',
        url: "/rest/front/favorite/selectFavoriteList",
        data:{
            userID: sessionStorage.id
        },
        dataType:'json',
        success: function (data) {
           var list=data.data,
                html='';
            if (list!=null&&list!='') {
                $('.lack').hide();
                $.each(list, function (i, e) {
                    var img=e.productPicture|| e.productImages.split(',')[0];
                html+='<li class="flex coll" data-pro="' + e.productID + '"><img class="pic cell-left" src="/'+img+'"  /><div class="cell-main cell-right">'+
                        '<div class="over p-list ">' + e.productName + '</div><div class="pspec"><span class="price xl orange">' + e.productPrice + '</span>'+
                        '<span class="s-payment">' + e.productSellCount + '人付款</span><label class="hide" >'+
                        '<input type="checkbox" class="checkbox" ><i class="select iconfont  pull-right"></i></label>'+
                        '</div></div></li><div class="underline"></div>'
                })
                $('.coll-goods').prepend(html);
            }
        }
    })
}

function delGoods(id){
    $.ajax({
        type:'POST',
        url: '/rest/front/favorite/deleteFavorites',
        dataTpye: 'json',
        data: {
            userID: sessionStorage.id,
            productIDs: id
        },
        success: function () {
            $.toast('删除成功');
            $('.coll-goods').empty();
            getCollection(userID);
            $('#tool-bar').hide();
            $('.checkbox').prop('checked', false);
            $('.saveBtn').attr("data-state", "0").text("编辑");
        }
    })
}

$(function () {
    getCollection();
    $('.saveBtn').click(function(){
        $(this).attr('data-state')==0?$(this).attr("data-state", "1").text("完成"):$(this).attr("data-state", "0").text("编辑");
        $('label.hide,#tool-bar').toggle();
    });
    $(".CheckAll").click(function(){
        if($(this).attr('data-state')==0) {
            $(this).attr("data-state", "1").siblings('.checkbox').prop('checked', true);
            $('.collect').find('.checkbox').prop('checked', true)
        }else{
            $(this).attr("data-state", "0").siblings('.checkbox').prop('checked', false);
            $('.collect').find('.checkbox').prop('checked', false)
        }
    });
    $(".delBtn").click(function () {
        var delCheck =$('.collect').find('input:checked');
        var delArr=[];
        if(!delCheck){
            $.alert('请选择要删除的记录', function () {return false; });
        }else{
            for (var i = 0; i < delCheck.length; i++) {
                delArr[i]= $(delCheck[i]).parents(".coll").attr("data-pro");
            }
            delGoods(delArr)
        }
    });
    $('.coll-goods').on('click', '.coll', function () {
        if ($('.saveBtn').attr('data-state') == 0) {
            var id = $(this).attr("data-pro");
            window.location.href = '/rest/front/product/toProductDetail?state=0&id=' + id;
        }
    });
    $('.collect').on('change','.checkbox',function(){
        if($('.collect').find('input:checked').length== $('.collect').find('.checkbox').length){
            $(".CheckAll").siblings('.checkbox').prop('checked', true);
        }
    })
});
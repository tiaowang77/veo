/**
 * Created by cxw on 2017/12/17.
 */
function sendRequest(phone){
    $.ajax({
        type:'POST',
        dataType:'json',
        url: '/rest/front/user/getSMSCode',
        data:{
            mobile:phone
        },
        success: function (res) {
            if(res.success){
                $.toast('发送成功');
                $('#SmsId').val(res.data.sessionId);
                console.log(res.data.sessionId);
                console.log($('#SmsId').val())
            }else {
                $.alert(res.message);
            }
        }
    })
}
function setTel(phone,SmsId,code){
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "/rest/front/user/checkCode",
        data: {
            SmsId: SmsId,
            SmsContent: code,
            phone: phone
        },
        success: function (data) {
            if (data.success) {
                $('.tel-up').attr('state',1);
                $.toast('绑定成功');
                setTimeout(function () {
                    window.location.href = '/rest/front/user/toUserInfo';
                }, 1000);

            } else {
                $.toast('验证码错误');
            }
        }
    })
}
function setName(name){
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '/rest/front/user/updateUser',
        data: {
            nickname: name
        },
        success: function (data) {
            if (data.success) {
                window.location.href = '/rest/front/user/toMyPage';
            } else {
                $.alert(data.message);
            }
        }
    });
}

$(function () {
    $('.name-up').click(function () {
      var name=  $('.name-inp').val();
        setName(name)
    });
    $('.yan').click(function(){
        var reg = /^1[356789]\d{9}$/;
        var phone = $('.tel').val();
        var num=30;
        if (!reg.test(phone)) {$.toast('手机格式错误');return false;}
        $(this).addClass("code").text('30秒').removeClass('yan');
        var time=setInterval(function(){
            num--;
            num>0?$('.code').html(num+'秒'):( clearInterval(time),$('.code').text('验证码').addClass("yan").removeClass('code'))
        },1000);
        return sendRequest(phone);
    });
    $('.tel-up').click(function () {
        var phone= $('.tel').val();
        var SmsId=$('#SmsId').val();
        var code=$('.yan-inp').val();
        if(!$(this).attr('state')){
            if(!phone&&!code){  $.toast('请填写完整信息'); return false;}
           return setTel(phone,SmsId,code)
        }
    })
});

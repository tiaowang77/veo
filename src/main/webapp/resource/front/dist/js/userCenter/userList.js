//查询个人信息
$.ajax({
    type: 'GET',
    url: '/rest/front/user/selectUser',
    dataType: "json",
    success: function (data) {
        $(".mypic").attr('src', data.data.icon);//取用户的头像图片
        $(".myname").text(data.data.nickname);//取用户名称
    }
});
//点击头像跳转到个人信息页面
$(".mypic").click(function () {
    window.location.href = '/rest/front/user/toUserInfo'
});

// $("#openCheckBtn").hide();
// if (sessionStorage.role == 2) {//自营用户有开团审核
//     $("#openCheckBtn").show();
// }

// //点击二维码出现，个人信息的二维码
// $("#messageBtn").click(function (){
//     $.ajax({
//         type: "GET",
//         url: '/rest/front/user/selectUser',
//         dataType: "json",
//         success: function (data) {
//             var informationList = data.data;
//             $.modal({
//                 title: '<div class="user-item item-media"><div class="img-div"> <span class="img-box"><img src="' + informationList.icon + '" style="border: 0.1rem solid #999;"></span><div class="item-inner"> <div class="information"> <div class="item-title">' + informationList.nickname + '</div></div></div><span class="iconfont icon-iconfont31guanbi" style="float: right;"></span></div><img src="/' + informationList.qrCode + '" class="qr-code"><h5></h5></div>'
//             });
//             //点击二维码或者打叉关闭弹出框
//             $(".qr-code,.icon-iconfont31guanbi,.modal-overlay").click(function () {
//                 $.closeModal();
//             });
//         }
//     });
// });


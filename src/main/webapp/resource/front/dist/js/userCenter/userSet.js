/**
 * Created by cxw on 2017/12/17.
 */
function getInf(id,sex){
    $.ajax({
        type:'GET',
        url: "/rest/front/user/selectUser",
        dataType: "json",
        success: function (data) {
            var list=data.data;
            if(list) {
                sex = list.sex;
                $(".img-div").html('<img src="' + list.icon + '">');
                $(".name-div").val(list.nickname);
                $(".weChart-input").val(list.weChat);
                $(".r-area").val(list.city);
                $(".sex").val(sex == 1 ? '男' : sex == 2 ? '女' : '未知');
                $(".phone-div").text(list.phone ? list.phone : '未绑定');
            }
        }
    })
}
function saveInf(sex,headImg){
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '/rest/front/user/updateUser',
        data: {
            sex: sex,
            icon: headImg
        },
        success: function (data) {
            if (data.success) {
                window.location.href = '/rest/front/user/toMyPage';
            } else {
                $.alert(data.message);
            }
        }
    });
}
$(function () {
    var userID=217,sex;
    getInf(userID,sex);
    $("#sexBtn").click(function () {
        $(".expand,.select").show();
        $('.sex').val()=='男'?$(".manRadio").prop("checked", "true"):$(".womenRadio").prop("checked", "true");
    });
    $('.select-sex').click(function () {
        if ($(this).children(".Radiosex").prop('checked')==false) {
            $(this).children(".Radiosex").prop('checked', true);
            $(this).siblings("li").children(".Radiosex").prop('checked', false);
        }
        $(".mr0").val($(this).children(".Radiosex").val());
        $(".expand,.select").hide();
    });
    $('input[type=checkbox]').click(function () {  //点击checkbox可以选中
        $(document).find('input').not($(this)).prop('checked', false);
        $(this).parent().trigger("click");
    });
    $('.saveBtn').click(function () {
        var headImg= $(".img-div img").attr('src');
        sex=$('.sex').val()=='男'?1:2;
        saveInf(sex,headImg);
    });
    $('.phone-item-link').click(function(){
        if($(this).find('.phone-div').text()=='未绑定'){
            window.location.href='/rest/front/user/toRevise#tel'
        }
    })
});
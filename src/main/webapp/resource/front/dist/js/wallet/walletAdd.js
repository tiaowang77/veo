/**
 * Created by cxw on 2017/12/17.
 */
function queryMoney() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        //data: {userID: userID},
        //url: '/rest/front/user/selectUserCS',
        url: '/rest/front/user/selectUser',
        success: function (data) {
            if (data.data && data.data.walletMoney) {
                $(".money-wallet").html('<span>可提现金额：<span class="money">' + data.data.walletMoney + '</span>元<span>')
            }
        }
    })
}
function back(){
    window.location.href='/rest/front/wallet/toWalletList';
}
function toWallet(data) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/front/wallet/insertWallet",
        data: data,
        success: function (data) {
            if (data.success) {
                $.toast('发起成功，等待审批');
                setTimeout(function () {
                    window.location.href = '/rest/front/wallet/toWalletList';
                }, 1000);
            } else {
                $.alert(data.message);
            }
        }
    })
}
$(function () {
    queryMoney();
    $('.towallet').click(function () {
        var phone = $(".input-payeePhone").val();
        var account = $(".input-payAccount").val();
        var name = $(".input-payee").val();
        var money = $("#aliPayMoney").val();
        if (name && account && phone && money) {
            var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
            var data = {payeePhone: phone, payAccount: account, payee: name, money: money, accountType: 2};
            reg.test(phone) ? toWallet(data) : $.toast('手机格式不正确')
        } else {
            $.toast('信息未填写完整')
        }
    })
    $("#aliPayMoney").blur(function () {
         var money=$('.money').text();
        if ($(this).val() <= 0||$(this).val()>money) {
            $.toast('请输入正确的提现金额');
            $(this).val("")
        }
    });
});
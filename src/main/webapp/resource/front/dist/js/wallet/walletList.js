/**
 * Created by cxw on 2017/12/16.
 */
function queryMoney() {
    $.ajax({
        type: 'GET',
        url: '/rest/front/user/selectUser',
        dataType: 'json',
        success: function (data) {
            data.data && data.data.walletMoney && $('.price').text(Number(data.data.walletMoney).toFixed(2))
        }
    })
}
function back(){
    window.location.href='/rest/front/user/toMyPage';
}
function queryList() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/rest/front/record/selectRecordList",
        success: function (data) {
            var list = data.data;
            getList(list);
        }
    })
}
function getList(obj) {
    var html = '';
    if (obj!=null&&obj!='') {
        $('.lack').hide();
        $.each(obj, function (i, e) {
           var state=e.cashStatus == 1 ? '待审核' : e.cashStatus == 2 ? '已通过' : '未通过'
            html += '<ul class="underline"><li class="flex record-inf" data-id="' + e.transID + '" data-transType="' + e.transType + '" type="' + e.cashType + '" data-name="' + e.buyerName + '">';
            switch (e.transType) {
                case '0':
                    html += '<img src="/resource/front/dist/images/WeChat.png" alt="" class="pic">' +
                        '<div class="cell-main"><span class="type xl">提现至微信</span>' +
                        '<div class="time hui">' + e.createTime + '</div></div>' +
                        '<div class="cell-right"><span class="recordnum l">-&nbsp;' + Number(e.money).toFixed(2) + '</span><div class="state orange">'+state+'</div></div></li></ul>';
                    break;
                case '1':
                    html += '<img src="/resource/front/dist/images/shop.png" alt="" class="pic">' +
                        '<div class="cell-main"><span class="type xl">下级购买商品返利</span>' +
                        '<div class="time hui">' + e.createTime + '</div></div>' +
                        '<div class="cell-right"><span class="recordnum l">+&nbsp;' + Number(e.money).toFixed(2) + '</span></div></li></ul>';
                    break;
                case '2':
                    html += '<img src="/resource/front/dist/images/shop.png" alt="" class="pic">' +
                        '<div class="cell-main"><span class="type xl">开团订单收入</span>' +
                        '<div class="time hui">' + e.createTime + '</div></div>' +
                        '<div class="cell-right"><span class="recordnum l">+&nbsp;' + Number(e.money).toFixed(2) + '</span></div></li></ul>';
                    break;
                case '3':
                    html += '<img src="/resource/front/dist/images/shop.png" alt="" class="pic">' +
                        '<div class="cell-main"><span class="type xl">购买商品抵扣</span>' +
                        '<div class="time hui">' + e.createTime + '</div></div>' +
                        '<div class="cell-right"><span class="recordnum l">-&nbsp;' + Number(e.money).toFixed(2) + '</span></div></li></ul>';
                    break;
            }
        });
        $(".record").append(html);
    }
}
$(function () {
    queryMoney();
    queryList();
    $('.apply').click(function () {
        window.location.href = '/rest/front/wallet/toWalletAdd'
    })
})